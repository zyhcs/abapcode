<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM008_DATA</h2>
<h3> Description: Include ZQM008_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM008_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES:qals,qamv,matdoc,ausp,mara.<br/>
<br/>
DATA BEGIN OF gs_alv1.<br/>
DATA:prueflos   TYPE qals-prueflos,  "检验批<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-mjahr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-mblnr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zeile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-zeile,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证项目号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herkunft&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-herkunft,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检验批来源<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-werk,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t001w-name1,&nbsp;&nbsp;&nbsp;&nbsp;"工厂名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;art&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-art,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检验类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kurztext&nbsp;&nbsp;&nbsp;TYPE&nbsp;tq30t-kurztext,&nbsp;&nbsp;"检验类型描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;plko-ktext,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检验计划描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plnty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-plnty,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"任务清单类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zaehl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-zaehl,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"内部计数器<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plnnr_alt&nbsp;&nbsp;TYPE&nbsp;plko-plnnr_alt,&nbsp;&nbsp;&nbsp;"检验依据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plnnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-plnnr,&nbsp;&nbsp;&nbsp;&nbsp;"检验计划组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plnal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-plnal,&nbsp;&nbsp;&nbsp;&nbsp;"检验计划组计数器<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-budat,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pastrterm&nbsp;&nbsp;TYPE&nbsp;qals-pastrterm,&nbsp;&nbsp;"检验开始日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vdatum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-vfdat,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检验完成日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzqyjsrq&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-zzqyjsrq,&nbsp;&nbsp;&nbsp;"取样结束日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zpdate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-zpdate,&nbsp;&nbsp;&nbsp;&nbsp;"生产日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-matnr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"物料编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;makt-maktx,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"物料描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-lifnr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1_org&nbsp;&nbsp;TYPE&nbsp;but000-name_org1,&nbsp;"供应商名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-charg,&nbsp;"批次号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"品牌<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"规格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"产品名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"食用盐/工业盐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"轻碱/重碱<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"普通农铵/颗粒铵<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"颗粒/出口颗粒<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"干/湿散盐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"是否低钠<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt10&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"碘含量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt11&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;"包装规格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mengeneinh&nbsp;TYPE&nbsp;qals-mengeneinh,&nbsp;"单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmengeist&nbsp;&nbsp;TYPE&nbsp;qals-lmengeist,&nbsp;"检验批数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;losmenge&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-losmenge,&nbsp;"检验批数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbewertung&nbsp;TYPE&nbsp;qave-vbewertung,&nbsp;"检验状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzjyjl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zqmt001-zzjyjl,&nbsp;"检验结论<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vcode,&nbsp;"使用决策<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kurztext1&nbsp;&nbsp;TYPE&nbsp;qpct-kurztext,&nbsp;"使用决策描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vorglfnr&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-vorglfnr,&nbsp;"节点编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;merknr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-merknr,&nbsp;"特性编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;satzstatus&nbsp;TYPE&nbsp;qamr-satzstatus,&nbsp;"状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kurztext2&nbsp;&nbsp;TYPE&nbsp;tq76t-kurztext,&nbsp;"状态描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pmethode&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamv-pmethode,&nbsp;"检验方法<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjyffms&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char40,&nbsp;"检验方法描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kurztext3&nbsp;&nbsp;TYPE&nbsp;qmtt-kurztext,&nbsp;"检验方法描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;verwmerkm&nbsp;&nbsp;TYPE&nbsp;qamv-verwmerkm,&nbsp;"检验特性<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kurztext4&nbsp;&nbsp;TYPE&nbsp;qamv-kurztext,&nbsp;"检验特性描述,同时作为动态搜索字段<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mbewertg&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-mbewertg,&nbsp;"单项评估结果<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zdxjg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char10,&nbsp;"单项评估结果<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;masseinhsw&nbsp;TYPE&nbsp;qamv-masseinhsw,&nbsp;&nbsp;"缺计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjldw&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char6,&nbsp;&nbsp;"缺计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pruefbemkt&nbsp;TYPE&nbsp;qamr-pruefbemkt,&nbsp;"检验描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vcodegrp&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vcodegrp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vcode1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vcode,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;versionam&nbsp;&nbsp;TYPE&nbsp;qave-versionam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qmtb_werks&nbsp;TYPE&nbsp;qamv-qmtb_werks,&nbsp;"工厂<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pmethode2&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamv-pmethode,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pmtversion&nbsp;TYPE&nbsp;qamv-pmtversion,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objek&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-objek,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sortfeld&nbsp;&nbsp;&nbsp;TYPE&nbsp;qpmk-sortfeld,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjytx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char8,&nbsp;&nbsp;"转化显示后&nbsp;确定搜索字段的唯一性<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qpmk_werks&nbsp;TYPE&nbsp;qamv-qpmk_werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mkversion&nbsp;&nbsp;TYPE&nbsp;qamv-mkversion,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pruefdatuv&nbsp;TYPE&nbsp;qamr-pruefdatuv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pruefdatub&nbsp;TYPE&nbsp;qamr-pruefdatub,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;code1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-code1,&nbsp;"检验结果<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;katalgart1&nbsp;TYPE&nbsp;qamr-katalgart1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gruppe1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-gruppe1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;version1&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamr-version1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mittelwert&nbsp;TYPE&nbsp;qamr-mittelwert,&nbsp;"检验结果<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zdefult&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char50,&nbsp;"最终显示检验结果字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stellen&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamv-stellen,&nbsp;"最终显示检验结果字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sel.<br/>
DATA END OF gs_alv1.<br/>
<br/>
DATA gt_alv1 LIKE TABLE OF gs_alv1.<br/>
<br/>
DATA:wt_alv LIKE TABLE OF gs_alv1.<br/>
DATA:ws_alv LIKE LINE OF wt_alv.<br/>
<br/>
DATA:lt_alv LIKE TABLE OF gs_alv1.<br/>
DATA:ls_alv LIKE LINE OF lt_alv.<br/>
<br/>
DATA:BEGIN OF ls_zdt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjytx&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qamv-verwmerkm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sortfeld&nbsp;TYPE&nbsp;qpmk-sortfeld,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zdefult&nbsp;&nbsp;TYPE&nbsp;char20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ls_zdt.<br/>
<br/>
DATA:lt_zdt LIKE TABLE OF ls_zdt.<br/>
<br/>
DATA gs_qamv TYPE qamv.<br/>
DATA gt_qamv LIKE TABLE OF gs_qamv.<br/>
<br/>
DATA gs_MATDOC TYPE matdoc.<br/>
DATA gt_MATDOC LIKE TABLE OF gs_MATDOC.<br/>
<br/>
DATA gs_AUSP TYPE ausp.<br/>
DATA gt_AUSP LIKE TABLE OF gs_AUSP.<br/>
<br/>
DATA: lt_alv_cat TYPE TABLE OF lvc_s_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_alv_cat&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_alv_cat.<br/>
<br/>
DATA: wt_alv_cat TYPE TABLE OF lvc_s_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_alv_cat&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;wt_alv_cat.<br/>
<br/>
DATA: lwa_layout       TYPE lvc_s_layo.<br/>
DATA: la_layout       TYPE lvc_s_layo.<br/>
<br/>
FIELD-SYMBOLS:<br/>
&nbsp;&nbsp;&lt;fs_table2&gt;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE,<br/>
&nbsp;&nbsp;&lt;fs_workarea2&gt;&nbsp;TYPE&nbsp;any,<br/>
&nbsp;&nbsp;&lt;fs_value2&gt;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;any.<br/>
DATA: lcl_table2    TYPE REF TO data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lcl_workarea2&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fname(30)&nbsp;&nbsp;&nbsp;TYPE&nbsp;c.<br/>
<br/>
DATA:BEGIN OF tS_serch_help1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt&nbsp;TYPE&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;tS_serch_help1.<br/>
<br/>
DATA:t_serch_help1 LIKE TABLE OF tS_serch_help1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>