<table class="outerTable">
<tr>
<td><h2>Table: ZQMT005A</h2>
<h3>Description: 检验项目配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZCPMC</td>
<td>3</td>
<td>X</td>
<td>ZE_ZCPMC</td>
<td>ZDM_ZCPMC</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>产品名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZQPOS</td>
<td>4</td>
<td>X</td>
<td>ZE_ZQPOS</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZJYXM</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZE_ZJYXM</td>
<td>ZDM_ZJYXM</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>检验项目</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZMSEHI</td>
<td>6</td>
<td>&nbsp;</td>
<td>QMGEH6</td>
<td>MSEC6</td>
<td>CHAR</td>
<td>6</td>
<td>X</td>
<td>数量数据被维护的计量单位</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZUCL</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZE_ZUCL</td>
<td>&nbsp;</td>
<td>DEC</td>
<td>10</td>
<td>&nbsp;</td>
<td>上限值</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZLCL</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZE_ZLCL</td>
<td>&nbsp;</td>
<td>DEC</td>
<td>10</td>
<td>&nbsp;</td>
<td>下限值</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZXSW</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZE_ZXSW</td>
<td>&nbsp;</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>小数位</td>
</tr>
<tr class="cell">
<td>10</td>
<td>BCLR</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZE_BCLR</td>
<td>ZDM_BCLR</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>班次录入标记</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZCPMCPX</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZE_CPMCPX</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>产品名称排序</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>