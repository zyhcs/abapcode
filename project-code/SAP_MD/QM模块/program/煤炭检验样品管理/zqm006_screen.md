<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM006_SCREEN</h2>
<h3> Description: Include ZQM006_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM006_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B2&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-002.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:P_WERKS&nbsp;LIKE&nbsp;ZQMT006-WERKS&nbsp;MEMORY&nbsp;ID&nbsp;wrk&nbsp;&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_MATNR&nbsp;LIKE&nbsp;ZQMT006-MATNR&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_CRDAT&nbsp;like&nbsp;ZQMT006-ERSTELLDAT&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;sy-datum&nbsp;MODIF&nbsp;ID&nbsp;m1.<br/>
&nbsp;&nbsp;&nbsp;SELECT-OPTIONS&nbsp;S_CRDAT&nbsp;FOR&nbsp;ZQMT006-ERSTELLDAT&nbsp;MODIF&nbsp;ID&nbsp;m2.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B2.<br/>
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B1&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;P_RB1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;G1&nbsp;USER-COMMAND&nbsp;UC1&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;G1.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>