<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM006_F01</h2>
<h3> Description: Include ZQM006_F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM006_F01<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;SUB_SET_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM SUB_SET_SCREEN .<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;P_RB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;SCREEN-GROUP1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;&nbsp;'M2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-ACTIVE&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'M1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-ACTIVE&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;P_RB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;SCREEN-GROUP1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;&nbsp;'M2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-ACTIVE&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'M1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-ACTIVE&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_F4_GET_MAKTX<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;LBL_MAKTX<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_GET_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_GET_DATA .<br/>
&nbsp;&nbsp;DATA:LT_BUT000&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;BUT000&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_MAKTX&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;MAKT&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_T001W&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;T001W&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;GT_DATA<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ZQMT006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;MATNR&nbsp;=&nbsp;P_MATNR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ERSTELLDAT&nbsp;IN&nbsp;S_CRDAT.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;GT_DATA&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;TABLE&nbsp;LT_BUT000<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;BUT000<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;GT_DATA<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;PARTNER&nbsp;=&nbsp;GT_DATA-LIFNR.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;LT_MAKTX<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;MAKT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;GT_DATA<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;MATNR&nbsp;=&nbsp;GT_DATA-MATNR.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;LT_T001W<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;T001W<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;GT_DATA<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;GT_DATA-WERKS.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;LT_BUT000&nbsp;BY&nbsp;PARTNER.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;&nbsp;LT_MAKTX&nbsp;BY&nbsp;MATNR.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;LT_T001W&nbsp;BY&nbsp;WERKS.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_DATA&nbsp;INTO&nbsp;GW_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_MAKTX&nbsp;WITH&nbsp;KEY&nbsp;MATNR&nbsp;=&nbsp;GW_DATA-MATNR&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA-MAKTX&nbsp;=&nbsp;LT_MAKTX-MAKTX.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_T001W&nbsp;WITH&nbsp;KEY&nbsp;WERKS&nbsp;=&nbsp;GW_DATA-WERKS&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA-WERKS_T&nbsp;=&nbsp;LT_T001W-NAME1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;&nbsp;LT_BUT000&nbsp;WITH&nbsp;KEY&nbsp;PARTNER&nbsp;=&nbsp;GW_DATA-LIFNR&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA-NAME1&nbsp;=&nbsp;LT_BUT000-NAME_ORG1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;GT_DATA&nbsp;FROM&nbsp;GW_DATA.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_DELE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;GW_DATA_ZQNUM<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_DELE_DATA  USING P_ZYPBH TYPE ZE_ZYPBH.<br/>
&nbsp;&nbsp;DELETE&nbsp;GT_DATA&nbsp;WHERE&nbsp;ZYPBH&nbsp;=&nbsp;P_ZYPBH.<br/>
&nbsp;&nbsp;DELETE&nbsp;FROM&nbsp;ZQMT006&nbsp;WHERE&nbsp;ZYPBH&nbsp;=&nbsp;P_ZYPBH.<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK&nbsp;AND&nbsp;WAIT.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'删除成功!'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_data_save<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_DATA_SAVE .<br/>
&nbsp;&nbsp;DATA:&nbsp;OBJECT&nbsp;TYPE&nbsp;CHAR10&nbsp;VALUE&nbsp;'ZYPBH',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_TIID&nbsp;TYPE&nbsp;CHAR10.<br/>
&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;=&nbsp;'N'&nbsp;AND&nbsp;ZQMT006-ZYPBH&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'NUMBER_GET_NEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NR_RANGE_NR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'01'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;OBJECT<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'1'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUBOBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOYEAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'0000'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IGNORE_BUFFER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;E_TIID<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURNCODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTERVAL_NOT_FOUND&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUMBER_RANGE_NOT_INTERN&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJECT_NOT_FOUND&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY_IS_0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY_IS_NOT_1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTERVAL_OVERFLOW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUFFER_OVERFLOW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-ZYPBH&nbsp;=&nbsp;E_TIID.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;ZQMT006-ZYPBH&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;=&nbsp;'N'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-ERSTELLER&nbsp;=&nbsp;SY-UNAME.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-ERSTELLDAT&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-ERSTELLTIM&nbsp;=&nbsp;SY-UZEIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;GV_MODE&nbsp;=&nbsp;'M'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-AENDERER&nbsp;=&nbsp;SY-UNAME.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-AENDERDAT&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMT006-AENDERTIM&nbsp;=&nbsp;SY-UZEIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;ZQMT006&nbsp;TO&nbsp;GW_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;ZQMT006&nbsp;FROM&nbsp;ZQMT006.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'保存成功！'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'样品编号不能为空！'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;CHECK_AUTH<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM CHECK_AUTH .<br/>
&nbsp;&nbsp;DATA:LV_ACTVT(2)&nbsp;TYPE&nbsp;C.<br/>
&nbsp;&nbsp;IF&nbsp;P_RB1&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LV_ACTVT&nbsp;=&nbsp;'01'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LV_ACTVT&nbsp;=&nbsp;'03'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;AUTHORITY-CHECK&nbsp;OBJECT&nbsp;'ZQM003'<br/>
&nbsp;&nbsp;&nbsp;ID&nbsp;'ACTVT'&nbsp;FIELD&nbsp;LV_ACTVT<br/>
&nbsp;&nbsp;&nbsp;ID&nbsp;'WERKS'&nbsp;FIELD&nbsp;&nbsp;P_WERKS.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;TEXT-W02&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>