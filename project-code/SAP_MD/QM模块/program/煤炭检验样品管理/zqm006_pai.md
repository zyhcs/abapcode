<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM006_PAI</h2>
<h3> Description: Include ZQM006_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM006_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;EXIT_PROGGAM&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE EXIT_PROGGAM INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F02'&nbsp;OR&nbsp;'&amp;F05'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F12'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_MAKTX&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*MODULE&nbsp;F4_GET_MAKTX&nbsp;INPUT.<br/>
*<br/>
*&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;MAKTX&nbsp;INTO&nbsp;LBL_MAKTX<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;MAKT<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;MATNR&nbsp;=&nbsp;ZQMT006-MATNR<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;SPRAS&nbsp;=&nbsp;'1'.<br/>
*ENDMODULE.<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_NAME&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_NAME INPUT.<br/>
if ZQMT006-LIFNR is NOT INITIAL.<br/>
CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ZQMT006-LIFNR<br/>
&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_PARTNER.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;NAME_ORG1&nbsp;INTO&nbsp;LBL_NAME<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;BUT000<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;PARTNER&nbsp;=&nbsp;lv_PARTNER.<br/>
endif.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>