<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM005_PBO</h2>
<h3> Description: 表控制输出模块（已生成）的包含文件</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Include&nbsp;ZQM005_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_JYXM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;UPDATE&nbsp;LINES&nbsp;FOR&nbsp;EQUIVALENT&nbsp;SCROLLBAR<br/>
</div>
<div class="code">
MODULE TC_JYXM_CHANGE_TC_ATTR OUTPUT.<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;GT_ZQMT005&nbsp;LINES&nbsp;TC_JYXM-LINES.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE STATUS_0100 OUTPUT.<br/>
&nbsp;&nbsp;REFRESH&nbsp;EXCLTAB.<br/>
&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;EQ&nbsp;''&nbsp;OR&nbsp;GV_MODE&nbsp;EQ&nbsp;'M'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCLTAB-FCODE&nbsp;=&nbsp;'CREATE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;EXCLTAB.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI_STATUS_100'&nbsp;EXCLUDING&nbsp;EXCLTAB.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'GUI_TITLE_0100'&nbsp;WITH&nbsp;'生产过程检验数据管理'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_CONTAINER_OBJECT&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE INIT_CONTAINER_OBJECT OUTPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;CREATE_CONTAINER_OBJECT&nbsp;USING&nbsp;EDITOR_CONTAINER&nbsp;'EDITOR'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;CREATE_EDITOR_OBJECT&nbsp;USING&nbsp;EDITOR&nbsp;EDITOR_CONTAINER&nbsp;.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_DATA&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE INIT_DATA OUTPUT.<br/>
&nbsp;&nbsp;DATA:LV_CHAR_FIELD&nbsp;TYPE&nbsp;QSOLLWERTC,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;QSOLLWERTE.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;LV_CHAR_FIELD,&nbsp;LV_VALUE.<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;GV_MODE&nbsp;EQ&nbsp;''&nbsp;or&nbsp;(&nbsp;SY-UCOMM&nbsp;eq&nbsp;'SAVE'&nbsp;and&nbsp;ZQMT005-ZQNUM&nbsp;is&nbsp;NOT&nbsp;INITIAL&nbsp;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"AND&nbsp;SY-UCOMM&nbsp;NE&nbsp;'CREATE'&nbsp;and&nbsp;GV_MODE&nbsp;ne&nbsp;'M'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;(&nbsp;GV_MODE&nbsp;EQ&nbsp;'N'&nbsp;OR&nbsp;GV_MODE&nbsp;EQ&nbsp;'M')&nbsp;AND&nbsp;SY-UCOMM&nbsp;NE&nbsp;'CREATE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SCREEN-NAME&nbsp;=&nbsp;'ZQMT005-ZCPMC'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;SY-UCOMM&nbsp;EQ&nbsp;'CREATE'&nbsp;OR&nbsp;GV_MODE&nbsp;EQ&nbsp;'C'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SCREEN-NAME&nbsp;=&nbsp;'ZQMT005-ZCPMC'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;EQ&nbsp;'N'&nbsp;AND&nbsp;SY-UCOMM&nbsp;NE&nbsp;'SAVE'&nbsp;AND&nbsp;&nbsp;GV_LINE&nbsp;NE&nbsp;'X'&nbsp;AND&nbsp;SY-UCOMM&nbsp;NE&nbsp;'CREATE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ZQMT005-ZCPMC&nbsp;=&nbsp;P_ZCPMC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ZQMT005-ZQDAT&nbsp;=&nbsp;P_ZQDAT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ZQMT005-WERKS&nbsp;=&nbsp;&nbsp;P_WERKS.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;GT_ZQMT005&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_ZQMT005A&nbsp;INTO&nbsp;GW_ZQMT005A.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;GW_ZQMT005A&nbsp;TO&nbsp;GW_ZQMT005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;GT_T006A&nbsp;INTO&nbsp;GW_T006A&nbsp;WITH&nbsp;KEY&nbsp;MSEHI&nbsp;=&nbsp;GW_ZQMT005-ZMSEHI&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-MSEH6&nbsp;=&nbsp;GW_T006A-MSEH6.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;GW_ZQMT005&nbsp;TO&nbsp;GT_ZQMT005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-UCOMM&nbsp;NE&nbsp;'SAVE'&nbsp;AND&nbsp;GV_LINE&nbsp;NE&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*读取长文本<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;EQ&nbsp;'C'&nbsp;AND&nbsp;WA_THEAD&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WA_THEAD-TDNAME&nbsp;&nbsp;=&nbsp;GV_OLDZQNUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_READTEXT&nbsp;USING&nbsp;EDITOR&nbsp;WA_THEAD.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REFRESH&nbsp;GT_ZQMT005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ZQMT005-ZQNUM&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_DATA&nbsp;INTO&nbsp;GW_DATA&nbsp;WHERE&nbsp;ZQNUM&nbsp;=&nbsp;ZQMT005-ZQNUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-ZQPOS&nbsp;=&nbsp;GW_DATA-ZQPOS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-ZJYXM&nbsp;=&nbsp;GW_DATA-ZJYXM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-ZMSEHI&nbsp;=&nbsp;GW_DATA-ZMSEHI.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;GT_T006A&nbsp;INTO&nbsp;GW_T006A&nbsp;WITH&nbsp;KEY&nbsp;MSEHI&nbsp;=&nbsp;GW_ZQMT005-ZMSEHI&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-MSEH6&nbsp;=&nbsp;GW_T006A-MSEH6.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-ZJYSJ&nbsp;=&nbsp;GW_DATA-JYZ.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005-ZWJC&nbsp;=&nbsp;GW_DATA-ZWJC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;GW_ZQMT005&nbsp;TO&nbsp;GT_ZQMT005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;GW_ZQMT005,GW_T006A.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WA_THEAD-TDNAME&nbsp;&nbsp;=&nbsp;&nbsp;ZQMT005-ZQNUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_READTEXT&nbsp;USING&nbsp;EDITOR&nbsp;WA_THEAD.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZZJYY&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZZJYY INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZZJYY&nbsp;USING&nbsp;'ZQMT005-ZZJYY'&nbsp;CHANGING&nbsp;ZQMT005-ZZJYY.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZBANCI&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZBANCI INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZBANCI&nbsp;USING&nbsp;'ZQMT005-ZBANCI'&nbsp;CHANGING&nbsp;ZQMT005-ZBANCI.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZQYDD&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZQYDD INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZQYDD&nbsp;USING&nbsp;'ZQMT005-ZQYDD'&nbsp;CHANGING&nbsp;ZQMT005-ZQYDD.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;ZCHECK_ZJYSJ&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE ZCHECK_ZJYSJ INPUT.<br/>
&nbsp;&nbsp;CLEAR:OK_CODE.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;GT_ZQMT005A&nbsp;INTO&nbsp;GW_ZQMT005A&nbsp;WITH&nbsp;KEY&nbsp;ZJYXM&nbsp;=&nbsp;GW_ZQMT005-ZJYXM<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQPOS&nbsp;=&nbsp;GW_ZQMT005-ZQPOS.<br/>
&nbsp;&nbsp;GV_LINE&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;GW_ZQMT005-ZJYSJ&nbsp;BETWEEN&nbsp;GW_ZQMT005A-ZLCL&nbsp;AND&nbsp;GW_ZQMT005A-ZUCL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;GW_ZQMT005-ZJYXM&nbsp;&amp;&amp;&nbsp;'输入值应小于等于上限值'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&amp;&amp;&nbsp;GW_ZQMT005A-ZUCL&nbsp;&amp;&amp;&nbsp;'大于等于'&nbsp;&amp;&amp;&nbsp;GW_ZQMT005A-ZLCL&nbsp;TYPE&nbsp;'I'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'W'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;TC_JYXM_CHANGE_FIELD_ATTR&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE TC_JYXM_CHANGE_FIELD_ATTR OUTPUT.<br/>
&nbsp;&nbsp;IF&nbsp;GV_MODE&nbsp;EQ&nbsp;''&nbsp;OR&nbsp;(&nbsp;SY-UCOMM&nbsp;&nbsp;EQ&nbsp;'SAVE'&nbsp;and&nbsp;ZQMT005-ZQNUM&nbsp;is&nbsp;NOT&nbsp;INITIAL&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SCREEN-NAME&nbsp;=&nbsp;'GW_ZQMT005-ZJYSJ'&nbsp;OR&nbsp;SCREEN-NAME&nbsp;=&nbsp;'GW_ZQMT005-ZWJC'&nbsp;OR&nbsp;SCREEN-NAME&nbsp;=&nbsp;'GW_ZQMT005-BCLR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZHLBZ&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZHLBZ INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZHLBZ&nbsp;USING&nbsp;'ZQMT005-ZHLBZ'&nbsp;CHANGING&nbsp;ZQMT005-ZHLBZ.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZBUMEN&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZBUMEN INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZBUMEN&nbsp;USING&nbsp;'ZQMT005-ZBUMEN'&nbsp;CHANGING&nbsp;ZQMT005-ZBUMEN.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZZHIBI&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZZHIBI INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZZHIBI&nbsp;USING&nbsp;'ZQMT005-ZZHIBI'&nbsp;CHANGING&nbsp;ZQMT005-ZZHIBI.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZPP&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZPP INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZPP&nbsp;USING&nbsp;'ZQMT005-ZPP'&nbsp;CHANGING&nbsp;ZQMT005-ZPP.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZGG&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZGG INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZGG&nbsp;USING&nbsp;'ZQMT005-ZGG'&nbsp;CHANGING&nbsp;ZQMT005-ZGG.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;F4_GET_ZCPMC&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE F4_GET_ZCPMC INPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_F4_GET_ZCPMC&nbsp;USING&nbsp;'ZQMT005-ZCPMC'&nbsp;CHANGING&nbsp;ZQMT005-ZCPMC.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>