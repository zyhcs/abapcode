<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM005_TOP</h2>
<h3> Description: Include ZQM005_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM005_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES: ZQMT005,ZQMT005D,ZQMT005A.<br/>
TYPES:BEGIN OF TY_ITEM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELT&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQPOS&nbsp;&nbsp;LIKE&nbsp;ZQMT005-ZQPOS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJYXM&nbsp;&nbsp;LIKE&nbsp;ZQMT005-ZJYXM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMSEHI&nbsp;LIKE&nbsp;ZQMT005-ZMSEHI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MSEH6&nbsp;&nbsp;like&nbsp;T006A-MSEH6,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJYSJ&nbsp;&nbsp;LIKE&nbsp;ZQMT005-ZJYSJ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZWJC&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZQMT005-ZWJC,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BCLR&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZQMT005A-BCLR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_ITEM.<br/>
<br/>
DATA:BEGIN OF TY_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include&nbsp;structure&nbsp;<a&nbsp;href&nbsp;="zqmt005 dictionary-zqmt005.html"="">ZQMT005.<br/>
DATA:  NAME1 LIKE T001W-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jyz&nbsp;type&nbsp;char20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MSEH6&nbsp;type&nbsp;MSEH6,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZZJYY_T&nbsp;TYPE&nbsp;ZE_ZZNAME,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJYSJ_T&nbsp;TYPE&nbsp;char20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BCLR&nbsp;like&nbsp;ZQMT005A-BCLR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_DATA.<br/>
DATA: GV_MODE  TYPE C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_line&nbsp;TYPE&nbsp;C.&nbsp;"用于判断是否table&nbsp;control&nbsp;触发<br/>
DATA:GT_JYY LIKE TABLE OF ZQMT002 WITH HEADER LINE.<br/>
DATA:GT_ZQMT005 TYPE TABLE OF TY_ITEM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005&nbsp;TYPE&nbsp;TY_ITEM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_T006A&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;T006A,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gw_T006A&nbsp;TYPE&nbsp;T006A,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_DATA&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;TY_DATA.<br/>
<br/>
DATA:BEGIN OF EXCLTAB OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FCODE&nbsp;LIKE&nbsp;SY-UCOMM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;EXCLTAB.<br/>
<br/>
</a&nbsp;href&nbsp;="zqmt005></div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;显示用到的全局参数<br/>
*&nbsp;定义FIELDCAT&nbsp;内表和工作区<br/>
</div>
<div class="code">
DATA: GT_FIELDCAT TYPE LVC_T_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_FIELDCAT&nbsp;TYPE&nbsp;LVC_S_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_ZQMT005A&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ZQMT005A,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005A&nbsp;TYPE&nbsp;ZQMT005A.<br/>
&nbsp;&nbsp;DATA:gT_T001W&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;T001W&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;DATA:GV_OLDZQNUM&nbsp;LIKE&nbsp;ZQMT005-ZQNUM.<br/>
</div>
<div class="codeComment">
*&nbsp;定义布局<br/>
</div>
<div class="code">
DATA: GW_LAYOUT        TYPE LVC_S_LAYO.<br/>
</div>
<div class="codeComment">
*&nbsp;设置字段排序<br/>
</div>
<div class="code">
DATA: GT_SORT TYPE LVC_T_SORT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_SORT&nbsp;TYPE&nbsp;LVC_S_SORT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;对象,ALV回调的时候用，一般不用<br/>
</div>
<div class="code">
DATA: GR_GRID TYPE REF TO CL_GUI_ALV_GRID.<br/>
DATA: OK_CODE LIKE SY-UCOMM.<br/>
</div>
<div class="codeComment">
*&nbsp;为FIELDCAT&nbsp;定义宏&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其中&amp;1表示占位符<br/>
*&nbsp;以下含有“是否”的值范围为空和X<br/>
</div>
<div class="code">
DEFINE DE_FIELDCAT.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gw_FIELDCAT.<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.&nbsp;"字段名<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段长描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段中描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段短描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.&nbsp;"主键，蓝底显示，默认冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-NO_ZERO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.&nbsp;"不显示0值<br/>
&nbsp;&nbsp;gw_FIELDCAT-EDIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;"是否编辑<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"参考字段&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_TABLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;"参考表&nbsp;&nbsp;&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIX_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;8.&nbsp;"冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-CONVEXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9.&nbsp;"转换例程<br/>
&nbsp;&nbsp;IF&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;EQ&nbsp;'CK'.<br/>
&nbsp;&nbsp;gw_FIELDCAT-CHECKBOX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'."是否复选框<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;gw_FIELDCAT&nbsp;TO&nbsp;GT_FIELDCAT.<br/>
END-OF-DEFINITION.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_JYXM'&nbsp;ITSELF<br/>
</div>
<div class="code">
CONTROLS: TC_JYXM TYPE TABLEVIEW USING SCREEN 0100.<br/>
<br/>
DATA:EDITOR_CONTAINER TYPE REF TO CL_GUI_CUSTOM_CONTAINER ,  " 定义容器<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EDITOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;CL_GUI_TEXTEDIT&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;定义文本编辑器对象<br/>
<br/>
CONSTANTS:C_LINE_LENGTH TYPE I VALUE 72 .                    " 控制文本编辑器每行最多显示的字符数<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;定义用于存放长文本的数据类型<br/>
</div>
<div class="code">
TYPES:BEGIN OF ST_TEXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LINE&nbsp;TYPE&nbsp;C&nbsp;LENGTH&nbsp;C_LINE_LENGTH,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ST_TEXT&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;定义一个长文本表结构类型<br/>
</div>
<div class="code">
TYPES :TT_TEXT TYPE STANDARD TABLE OF ST_TEXT .<br/>
<br/>
DATA: WA_LINE     TYPE TLINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_TAB_TEXT&nbsp;TYPE&nbsp;ST_TEXT&nbsp;OCCURS&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;定义一个按行存放编辑器数据的内表<br/>
</div>
<div class="code">
DATA: IT_LINES TYPE STANDARD TABLE OF TLINE WITH HEADER LINE .<br/>
</div>
<div class="codeComment">
*&nbsp;定义存放SAVE_TEXT的表头数据<br/>
</div>
<div class="code">
DATA: WA_THEAD LIKE THEAD .<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;初始化表头信息的内容<br/>
</div>
<div class="code">
WA_THEAD-TDOBJECT  =  'ZT001' .<br/>
WA_THEAD-TDID      =  'ZT01' .<br/>
"WA_THEAD-TDNAME    =  SY-REPID .<br/>
WA_THEAD-TDSPRAS   =  SY-LANGU .<br/>
ranges:rs_ZCPMC FOR ZQMT005-ZCPMC.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>