<table class="outerTable">
<tr>
<td><h2>Table: ZQMT005F</h2>
<h3>Description: 规格</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZCODE</td>
<td>2</td>
<td>X</td>
<td>ZE_ZCODE</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZGG</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZE_ZGG</td>
<td>ZDM_ZGG</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>规格</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>