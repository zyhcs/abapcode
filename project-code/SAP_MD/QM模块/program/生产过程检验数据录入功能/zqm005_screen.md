<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM005_SCREEN</h2>
<h3> Description: Include ZQM005_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM005_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B2&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-002.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:P_WERKS&nbsp;LIKE&nbsp;ZQMT005-WERKS&nbsp;MEMORY&nbsp;ID&nbsp;WRK&nbsp;&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZCPMC&nbsp;LIKE&nbsp;ZQMT005-ZCPMC,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZQDAT&nbsp;LIKE&nbsp;ZQMT005-ZQDAT&nbsp;DEFAULT&nbsp;SY-DATUM&nbsp;MODIF&nbsp;ID&nbsp;M1.<br/>
&nbsp;&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_ZQDAT&nbsp;FOR&nbsp;ZQMT005-ZQDAT&nbsp;MODIF&nbsp;ID&nbsp;M2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ZBUMEN&nbsp;FOR&nbsp;ZQMT005D-ZBUMEN&nbsp;MODIF&nbsp;ID&nbsp;M2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ZJYXM&nbsp;FOR&nbsp;ZQMT005A-ZJYXM&nbsp;MODIF&nbsp;ID&nbsp;M2.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B2.<br/>
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B1&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;P_RB1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;G1&nbsp;USER-COMMAND&nbsp;UC1&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;G1.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B1.<br/>
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B3&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-003.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;P_CB1&nbsp;AS&nbsp;CHECKBOX&nbsp;USER-COMMAND&nbsp;UC1&nbsp;MODIF&nbsp;ID&nbsp;M2.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B3.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>