<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM007_TOP</h2>
<h3> Description: Include ZQM007_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQM007_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES ZQMT006.<br/>
<br/>
DATA:LBL_MAKTX LIKE MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LBL_NAME&nbsp;&nbsp;LIKE&nbsp;LFA1-NAME1.<br/>
<br/>
DATA:BEGIN OF TY_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include&nbsp;structure&nbsp;<a&nbsp;href&nbsp;="zqmt006 dictionary-zqmt006.html"="">ZQMT006.<br/>
DATA:  MAKTX   LIKE MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME1&nbsp;&nbsp;&nbsp;LIKE&nbsp;LFA1-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS_T&nbsp;LIKE&nbsp;T001W-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;TYPE&nbsp;CHAR4,&nbsp;"执行标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESS&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STRING,&nbsp;"执行消息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ck&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_DATA.<br/>
DATA:GT_DATA LIKE TABLE OF TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA&nbsp;LIKE&nbsp;TY_DATA.<br/>
</a&nbsp;href&nbsp;="zqmt006></div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;显示用到的全局参数<br/>
*&nbsp;定义FIELDCAT&nbsp;内表和工作区<br/>
</div>
<div class="code">
DATA: GT_FIELDCAT TYPE LVC_T_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_FIELDCAT&nbsp;TYPE&nbsp;LVC_S_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_ZQMT005A&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ZQMT005A,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZQMT005A&nbsp;TYPE&nbsp;ZQMT005A.<br/>
DATA:GT_T001W    LIKE TABLE OF T001W WITH HEADER LINE.<br/>
</div>
<div class="codeComment">
*&nbsp;定义布局<br/>
</div>
<div class="code">
DATA: GW_LAYOUT        TYPE LVC_S_LAYO.<br/>
</div>
<div class="codeComment">
*&nbsp;设置字段排序<br/>
</div>
<div class="code">
DATA: GT_SORT TYPE LVC_T_SORT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_SORT&nbsp;TYPE&nbsp;LVC_S_SORT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;对象,ALV回调的时候用，一般不用<br/>
</div>
<div class="code">
DATA: GR_GRID TYPE REF TO CL_GUI_ALV_GRID.<br/>
DATA: OK_CODE LIKE SY-UCOMM.<br/>
</div>
<div class="codeComment">
*&nbsp;为FIELDCAT&nbsp;定义宏&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其中&amp;1表示占位符<br/>
*&nbsp;以下含有“是否”的值范围为空和X<br/>
</div>
<div class="code">
DEFINE DE_FIELDCAT.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gw_FIELDCAT.<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.&nbsp;"字段名<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段长描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段中描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段短描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.&nbsp;"主键，蓝底显示，默认冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-NO_ZERO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.&nbsp;"不显示0值<br/>
&nbsp;&nbsp;gw_FIELDCAT-EDIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;"是否编辑<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"参考字段&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_TABLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;"参考表&nbsp;&nbsp;&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIX_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;8.&nbsp;"冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-CONVEXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9.&nbsp;"转换例程<br/>
&nbsp;&nbsp;IF&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;EQ&nbsp;'CK'.<br/>
&nbsp;&nbsp;gw_FIELDCAT-CHECKBOX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'."是否复选框<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;EQ&nbsp;'PRUEFLOS'.<br/>
&nbsp;&nbsp;gw_FIELDCAT-HOTSPOT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;gw_FIELDCAT&nbsp;TO&nbsp;GT_FIELDCAT.<br/>
END-OF-DEFINITION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>