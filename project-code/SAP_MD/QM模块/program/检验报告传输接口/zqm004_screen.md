<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZQM004_SCREEN</h2>
<h3> Description: Include ZQMIF001_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMIF001_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_WERK&nbsp;LIKE&nbsp;QALS-WERK&nbsp;MEMORY&nbsp;ID&nbsp;wrk&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_JYPC&nbsp;FOR&nbsp;QALS-PRUEFLOS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_JYPY&nbsp;FOR&nbsp;QALS-HERKUNFT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_CJRQ&nbsp;FOR&nbsp;QALS-ENSTEHDAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_MATNR&nbsp;FOR&nbsp;QALS-MATNR.<br/>
<br/>
&nbsp;&nbsp;"&nbsp;s_ZSTATUS&nbsp;for.<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK BLK2 WITH FRAME .<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;&nbsp;RB1&nbsp;&nbsp;&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;S01&nbsp;.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;&nbsp;RB2&nbsp;&nbsp;&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;S01&nbsp;.<br/>
SELECTION-SCREEN END OF BLOCK  BLK2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>