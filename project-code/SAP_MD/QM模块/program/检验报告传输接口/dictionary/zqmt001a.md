<table class="outerTable">
<tr>
<td><h2>Table: ZQMT001A</h2>
<h3>Description: 检验结论</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZZBGMB</td>
<td>2</td>
<td>X</td>
<td>ZE_ZZBGMB</td>
<td>ZDM_ZZBGMB</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>检验报告模板</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZZCODE</td>
<td>3</td>
<td>X</td>
<td>ZE_ZZCODE</td>
<td>ZDM_ZZCODE</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>结论编码</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZZJYJL</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZZJYJL</td>
<td>ZDM_ZZJYJL</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>检验结论</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>VCODE</td>
<td>5</td>
<td>&nbsp;</td>
<td>QVCODE</td>
<td>QCODE</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>使用决策代码</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>0</td>
<td>&nbsp;</td>
<td>LV1</td>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>1</td>
<td>&nbsp;</td>
<td>LV2</td>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>2</td>
<td>&nbsp;</td>
<td>LV3</td>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>3</td>
<td>&nbsp;</td>
<td>LV4</td>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>8</td>
<td>&nbsp;</td>
<td>LV5</td>
</tr>
<tr class="cell">
<td>ZDM_ZZCODE</td>
<td>9</td>
<td>&nbsp;</td>
<td>NG</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>