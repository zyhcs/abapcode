<table class="outerTable">
<tr>
<td><h2>Table: ZQMS_JYBG</h2>
<h3>Description: 检验报告传输接口结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>PRUEFLOS</td>
<td>1</td>
<td>&nbsp;</td>
<td>QPLOS</td>
<td>QPLOS</td>
<td>NUMC</td>
<td>12</td>
<td>&nbsp;</td>
<td>检验批次编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERK</td>
<td>2</td>
<td>&nbsp;</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>WERKNAME1</td>
<td>3</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BURKS</td>
<td>4</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BUTXT</td>
<td>5</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>KTEXT</td>
<td>6</td>
<td>&nbsp;</td>
<td>PLANTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZZJYFF</td>
<td>7</td>
<td>&nbsp;</td>
<td>CP_PLNNR_A</td>
<td>CP_PLNNR_A</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>检验依据</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZZBGMB</td>
<td>8</td>
<td>&nbsp;</td>
<td>VAGRP</td>
<td>VAGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>检验报告模板</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MATNR</td>
<td>9</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>10</td>
<td>KTEXTMAT</td>
<td>10</td>
<td>&nbsp;</td>
<td>QKTEXTOBJ</td>
<td>QKURZTEXT</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>检验对象的短文本</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZGG</td>
<td>11</td>
<td>&nbsp;</td>
<td>ATWRT30</td>
<td>ATWRT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>特征值</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZPP</td>
<td>12</td>
<td>&nbsp;</td>
<td>ATWRT30</td>
<td>ATWRT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>特征值</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZCPMC</td>
<td>13</td>
<td>&nbsp;</td>
<td>ATWRT30</td>
<td>ATWRT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>特征值</td>
</tr>
<tr class="cell">
<td>14</td>
<td>GESSTICHPR</td>
<td>14</td>
<td>&nbsp;</td>
<td>QGESTICH</td>
<td>QMENGEPLOS</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>检验样本数量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>LOSMENGE</td>
<td>15</td>
<td>&nbsp;</td>
<td>QLOSMENGE</td>
<td>QMENGEPLOS</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>检验批数量</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZHSDAT</td>
<td>16</td>
<td>&nbsp;</td>
<td>HSDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>生产日期</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZRKDAT</td>
<td>17</td>
<td>&nbsp;</td>
<td>LWEDT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>最近的收货日期</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZSJBM</td>
<td>18</td>
<td>&nbsp;</td>
<td>ZESJBM</td>
<td>CHAR40</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>送检部门</td>
</tr>
<tr class="cell">
<td>19</td>
<td>PASTRTERM</td>
<td>19</td>
<td>&nbsp;</td>
<td>QPRSTART</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>本地时区中检验的开始日期</td>
</tr>
<tr class="cell">
<td>20</td>
<td>PAENDTERM</td>
<td>20</td>
<td>&nbsp;</td>
<td>QPRENDE</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>本地时区中检验的结束日期</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZZJYY</td>
<td>21</td>
<td>&nbsp;</td>
<td>ZE_ZZJYY</td>
<td>ZDM_ZZJYY</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>检验员</td>
</tr>
<tr class="cell">
<td>22</td>
<td>CHARG</td>
<td>22</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td>23</td>
<td>LIFNR</td>
<td>23</td>
<td>&nbsp;</td>
<td>ELIFN</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商帐户号</td>
</tr>
<tr class="cell">
<td>24</td>
<td>LIFNRNAME1</td>
<td>24</td>
<td>&nbsp;</td>
<td>NAME1_GP</td>
<td>NAME</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>名称 1</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZZJYJL</td>
<td>25</td>
<td>&nbsp;</td>
<td>ZE_ZZJYJL</td>
<td>ZDM_ZZJYJL</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>检验结论</td>
</tr>
<tr class="cell">
<td>26</td>
<td>VCODE</td>
<td>26</td>
<td>&nbsp;</td>
<td>QVCODE</td>
<td>QCODE</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>使用决策代码</td>
</tr>
<tr class="cell">
<td>27</td>
<td>KURZTEXT</td>
<td>27</td>
<td>&nbsp;</td>
<td>QTXT_CODE</td>
<td>QKURZTEXT</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>代码的短文本</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZBANCI</td>
<td>28</td>
<td>&nbsp;</td>
<td>ZEBANCI</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>班次</td>
</tr>
<tr class="cell">
<td>29</td>
<td>MENGENEINH</td>
<td>29</td>
<td>&nbsp;</td>
<td>QLOSMENGEH</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>检验批次数量的基本计量单位</td>
</tr>
<tr class="cell">
<td>30</td>
<td>EINHPROBE</td>
<td>30</td>
<td>&nbsp;</td>
<td>QPROBEMGEH</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>样本单位的基本计量单位</td>
</tr>
<tr class="cell">
<td>31</td>
<td>HERKUNFT</td>
<td>31</td>
<td>&nbsp;</td>
<td>QHERK</td>
<td>QHERK</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>检验批源</td>
</tr>
<tr class="cell">
<td>32</td>
<td>IF_ITEMS</td>
<td>32</td>
<td>&nbsp;</td>
<td>ZTQMIF001_ITEMS</td>
<td>&nbsp;</td>
<td>TTYP</td>
<td>0</td>
<td>&nbsp;</td>
<td>检验报告传输接口行项目表类型</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>