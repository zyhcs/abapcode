<table class="outerTable">
<tr>
<td><h2>Table: ZSQM_JYPC</h2>
<h3>Description: 检验批次结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>PRUEFLOS</td>
<td>1</td>
<td>&nbsp;</td>
<td>QPLOS</td>
<td>QPLOS</td>
<td>NUMC</td>
<td>12</td>
<td>&nbsp;</td>
<td>检验批次编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZZBGMB</td>
<td>2</td>
<td>&nbsp;</td>
<td>VAGRP</td>
<td>VAGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>检验报告模板</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>HERKUNFT</td>
<td>3</td>
<td>&nbsp;</td>
<td>QHERK</td>
<td>QHERK</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>检验批源</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>