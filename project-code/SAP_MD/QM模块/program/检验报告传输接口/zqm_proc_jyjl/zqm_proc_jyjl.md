<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZQM_PROC_JYJL</h2>
<h3> Description: 检验结论处理函数</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZQM_PROC_JYJL.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  TABLES<br/>
*"      IT_DATA TYPE  ZTQM_JYPC<br/>
*"      OT_RETURN TYPE  ZTQM_JYPCMSG<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zqm_proc_jyjl.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:LT_JCJL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_JCJL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TY_JCJL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_JCJL_L&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_JCJL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LS_DATA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZSQM_JYPC,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LS_ZQMT001A&nbsp;TYPE&nbsp;ZQMT001A.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(LT_ZQMT001A)&nbsp;FROM&nbsp;ZQMT001A.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;LT_ZQMT001A&nbsp;BY&nbsp;ZZBGMB&nbsp;ZZCODE.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*检验结论计算逻辑<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;IT_DATA[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~PRUEFLOS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~VORGLFNR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~MERKNR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMR~MBEWERTG<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~PRUEFQUALI<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMR~MITTELWERT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~GRENZEOB1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~GRENZEOB2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~GRENZEUN2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~GRENZEUN1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~TOLERANZUN<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QAMV~TOLERANZOB<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;LT_JCJL<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;QAMV<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEFT&nbsp;JOIN&nbsp;QAMR&nbsp;ON&nbsp;QAMV~PRUEFLOS&nbsp;=&nbsp;QAMR~PRUEFLOS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;QAMV~VORGLFNR&nbsp;=&nbsp;QAMR~VORGLFNR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;QAMV~MERKNR&nbsp;=&nbsp;QAMR~MERKNR<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;IT_DATA[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;QAMV~PRUEFLOS&nbsp;=&nbsp;IT_DATA-PRUEFLOS.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;LT_JCJL&nbsp;BY&nbsp;PRUEFLOS&nbsp;VORGLFNR&nbsp;MERKNR.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;IT_DATA&nbsp;INTO&nbsp;LS_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_JCJL&nbsp;INTO&nbsp;LW_JCJL&nbsp;WHERE&nbsp;PRUEFLOS&nbsp;=&nbsp;LS_DATA-PRUEFLOS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-PRUEFLOS&nbsp;=&nbsp;LS_DATA-PRUEFLOS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LW_JCJL-MBEWERTG&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCTOK&nbsp;=&nbsp;OT_RETURN-ZZCTOK&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MBEWERTG&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCTNG&nbsp;=&nbsp;OT_RETURN-ZZCTNG&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*检验结论计算逻辑<br/>
*      if LS_DATA-HERKUNFT eq '04'.<br/>
*          OT_RETURN-ZZJYJL = '不合格'.<br/>
*          OT_RETURN-ZZCODE = '9'.<br/>
*      else.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_JCJL&nbsp;INTO&nbsp;LW_JCJL&nbsp;WITH&nbsp;KEY&nbsp;PRUEFLOS&nbsp;=&nbsp;LS_DATA-PRUEFLOS&nbsp;MBEWERTG&nbsp;&nbsp;=&nbsp;''&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCODE&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_JCJL&nbsp;INTO&nbsp;LW_JCJL&nbsp;WHERE&nbsp;KATALGART1&nbsp;=&nbsp;'1'&nbsp;AND&nbsp;MBEWERTG&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZJYJL&nbsp;=&nbsp;'不合格'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCODE&nbsp;=&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OT_RETURN-ZZCODE&nbsp;NE&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_JCJL&nbsp;INTO&nbsp;LW_JCJL&nbsp;WHERE&nbsp;KATALGART1&nbsp;&lt;&gt;&nbsp;'1'&nbsp;AND&nbsp;PRUEFQUALI&nbsp;&lt;&gt;&nbsp;'1'&nbsp;AND&nbsp;PRUEFQUALI&nbsp;&lt;&gt;&nbsp;'2'&nbsp;&nbsp;AND&nbsp;MBEWERTG&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZJYJL&nbsp;=&nbsp;'不合格'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCODE&nbsp;=&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*4）检查定量检验特性的结果（分级），取所有指标中最低的分级作为检验结论，<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OT_RETURN-ZZCODE&nbsp;NE&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REFRESH&nbsp;LT_JCJL_L.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_JCJL&nbsp;INTO&nbsp;LW_JCJL&nbsp;WHERE&nbsp;KATALGART1&nbsp;&lt;&gt;&nbsp;'1'&nbsp;AND&nbsp;(&nbsp;PRUEFQUALI&nbsp;EQ&nbsp;'1'&nbsp;OR&nbsp;PRUEFQUALI&nbsp;EQ&nbsp;'2').<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LW_JCJL-PRUEFQUALI&nbsp;EQ&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LW_JCJL-MITTELWERT&nbsp;&gt;=&nbsp;LW_JCJL-GRENZEOB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&gt;=&nbsp;LW_JCJL-GRENZEOB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&gt;=&nbsp;LW_JCJL-GRENZEUN2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&gt;=&nbsp;LW_JCJL-GRENZEUN1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'3'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&gt;=&nbsp;LW_JCJL-TOLERANZUN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'8'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-PRUEFQUALI&nbsp;EQ&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LW_JCJL-MITTELWERT&nbsp;&nbsp;&lt;=&nbsp;LW_JCJL-GRENZEUN1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&lt;=&nbsp;LW_JCJL-GRENZEUN2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&lt;=&nbsp;LW_JCJL-GRENZEOB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&lt;=&nbsp;LW_JCJL-GRENZEOB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'3'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;LW_JCJL-MITTELWERT&nbsp;&lt;=&nbsp;LW_JCJL-TOLERANZOB.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'8'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LW_JCJL-ZZCODE&nbsp;=&nbsp;'9'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;LW_JCJL&nbsp;TO&nbsp;LT_JCJL_L.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LT_JCJL_L[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;LT_JCJL_L&nbsp;DESCENDING&nbsp;BY&nbsp;PRUEFLOS&nbsp;ZZCODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_JCJL_L&nbsp;INTO&nbsp;LW_JCJL&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_ZQMT001A&nbsp;INTO&nbsp;LS_ZQMT001A&nbsp;WITH&nbsp;KEY&nbsp;ZZBGMB&nbsp;=&nbsp;LS_DATA-ZZBGMB&nbsp;ZZCODE&nbsp;=&nbsp;LW_JCJL-ZZCODE&nbsp;&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZJYJL&nbsp;=&nbsp;LS_ZQMT001A-ZZJYJL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZCODE&nbsp;=&nbsp;LW_JCJL-ZZCODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:OT_RETURN-ZZJYJL,&nbsp;OT_RETURN-ZZCODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OT_RETURN-ZZCODE&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;OT_RETURN-ZZJYJL&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;LT_ZQMT001A&nbsp;INTO&nbsp;LS_ZQMT001A&nbsp;WITH&nbsp;KEY&nbsp;ZZBGMB&nbsp;=&nbsp;LS_DATA-ZZBGMB&nbsp;ZZCODE&nbsp;=&nbsp;OT_RETURN-ZZCODE&nbsp;&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN-ZZJYJL&nbsp;=&nbsp;LS_ZQMT001A-ZZJYJL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*      ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;OT_RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;OT_RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>