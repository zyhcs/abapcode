<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZQMG04TOP</h2>
<h3> Description: 检验报告传输接口（SAP-OA）</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZQMG04.                       "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZQMG04D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
</div>
<div class="code">
TYPES: BEGIN OF TY_JCJL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRUEFLOS&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;QAMV-PRUEFLOS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VORGLFNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;QAMV-VORGLFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MERKNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;QAMV-MERKNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBEWERTG&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;QAMR-MBEWERTG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KATALGART1&nbsp;LIKE&nbsp;QAMR-KATALGART1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRUEFQUALI&nbsp;LIKE&nbsp;QAMV-PRUEFQUALI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MITTELWERT&nbsp;LIKE&nbsp;QAMR-MITTELWERT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRENZEOB1&nbsp;&nbsp;LIKE&nbsp;QAMV-GRENZEOB1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRENZEOB2&nbsp;&nbsp;LIKE&nbsp;QAMV-GRENZEOB2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRENZEUN2&nbsp;&nbsp;LIKE&nbsp;QAMV-GRENZEUN2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRENZEUN1&nbsp;&nbsp;LIKE&nbsp;QAMV-GRENZEUN1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOLERANZUN&nbsp;LIKE&nbsp;QAMV-TOLERANZUN,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOLERANZOB&nbsp;LIKE&nbsp;QAMV-TOLERANZOB,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZZCODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZE_ZZCODE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_JCJL.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;gt_bdcdata&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bdcdata,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_bdcdata&nbsp;TYPE&nbsp;bdcdata,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_option&nbsp;&nbsp;TYPE&nbsp;ctu_params,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bdcmsgcoll,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bdcmsgcoll.<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_msg&nbsp;TYPE&nbsp;char200.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>