<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMMC_MAINTAIN_MRPAREA_TOP</h2>
<h3> Description: Include ZMMC_MAINTAIN_MRPAREA_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMC_MAINTAIN_MRPAREA_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TYPE-POOLS : slis ,icon .<br/>
TABLES: sscrfields.<br/>
<br/>
TYPES : BEGIN OF ty_upload_mrp ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;18&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;物料号(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;4&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;工厂&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;berid&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;10,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;MRP&nbsp;范围(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dismm&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;2&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dispo&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;3&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;disls&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;2&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;批量大小&nbsp;&nbsp;必填<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sobsl&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;2&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;特殊采购类型&nbsp;&nbsp;选填<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werksg&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;4&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;供货工厂&nbsp;&nbsp;选填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgfsb&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;4,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;外部采购仓储地点<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plifz&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;3&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;计划交货时间&nbsp;&nbsp;(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;&nbsp;OF&nbsp;ty_upload_mrp&nbsp;.<br/>
TYPES : ty_tab_upload_mrp TYPE ty_upload_mrp OCCURS 0 .<br/>
<br/>
<br/>
TYPES : BEGIN OF ty_out_mrp ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;icon&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;4,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;200,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-matnr&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;物料号(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;TYPE&nbsp;&nbsp;makt-maktx,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;物料描述<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzchjc&nbsp;TYPE&nbsp;&nbsp;mara-zzchjc,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;存货简称<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-werks&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;工厂&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;berid&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-berid,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;MRP&nbsp;范围(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dismm&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-dismm&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dispo&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-dispo&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;disls&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-disls&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;批量大小&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werksg&nbsp;TYPE&nbsp;&nbsp;mdma-werks&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;供货工厂&nbsp;&nbsp;选填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sobsl&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-sobsl&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;特殊采购类型&nbsp;&nbsp;选填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgfsb&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-lgfsb,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;外部采购仓储地点<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plifz&nbsp;&nbsp;TYPE&nbsp;&nbsp;mdma-plifz,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;计划交货时间&nbsp;&nbsp;(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;&nbsp;OF&nbsp;ty_out_mrp&nbsp;.<br/>
TYPES : ty_tab_out_mrp TYPE ty_out_mrp OCCURS 0 .<br/>
<br/>
DATA : gt_upload_mrp TYPE ty_tab_upload_mrp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp&nbsp;TYPE&nbsp;ty_upload_mrp.<br/>
DATA : gt_out_mrp TYPE ty_tab_out_mrp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp&nbsp;TYPE&nbsp;ty_out_mrp.<br/>
<br/>
</div>
<div class="codeComment">
****----ALV<br/>
</div>
<div class="code">
DATA: gt_fieldcat      TYPE slis_t_fieldcat_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_fieldcat_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_layout_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_repid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-repid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_t_event,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_title,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_list_comments&nbsp;TYPE&nbsp;slis_t_listheader.<br/>
</div>
<div class="codeComment">
****----<br/>
</div>
<div class="code">
SELECTION-SCREEN: FUNCTION KEY 1.<br/>
<br/>
</div>
<div class="codeComment">
****----<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK block1 WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;f_file&nbsp;LIKE&nbsp;rfpdo1-febumsf&nbsp;.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK block1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>