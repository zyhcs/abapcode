<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMMC_MAINTAIN_MRPAREA_F01</h2>
<h3> Description: Include ZMMC_MAINTAIN_MRPAREA_F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMC_MAINTAIN_MRPAREA_F01<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_DOWNLOAD_TEMPLATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_download_template .<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;lc_file&nbsp;&nbsp;TYPE&nbsp;&nbsp;string&nbsp;,&nbsp;"rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lc_key&nbsp;&nbsp;&nbsp;TYPE&nbsp;wwwdatatab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lc_desc&nbsp;&nbsp;TYPE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lc_subrc&nbsp;TYPE&nbsp;sy-subrc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lc_temp&nbsp;&nbsp;TYPE&nbsp;c.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;l_def_file&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;30&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;l_objid&nbsp;TYPE&nbsp;wwwdatatab-objid&nbsp;.<br/>
&nbsp;&nbsp;DATA:c_objid1&nbsp;TYPE&nbsp;wwwdatatab-objid&nbsp;VALUE&nbsp;'ZPP015'.<br/>
&nbsp;&nbsp;IF&nbsp;sscrfields-ucomm&nbsp;=&nbsp;'FC01'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_def_file&nbsp;=&nbsp;'MRP区域维护模版.xlsx'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_objid&nbsp;=&nbsp;c_objid1&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CHECK&nbsp;sscrfields-ucomm+0(2)&nbsp;=&nbsp;'FC'&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'WS_FILENAME_GET'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;def_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_def_file&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设置前文件名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;def_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C:\'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设置前路径<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mask&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;',Excel&nbsp;Files,*.xls;All&nbsp;Files,*.*.'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"S&nbsp;=&nbsp;保存，O&nbsp;=&nbsp;打开<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'Save&nbsp;File'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lc_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inv_winsys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_batch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selection_cancel&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selection_error&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lc_desc&nbsp;=&nbsp;lc_file.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;lc_key<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;wwwdata&nbsp;WHERE&nbsp;srtf2&nbsp;=&nbsp;0&nbsp;AND&nbsp;relid&nbsp;=&nbsp;'MI'&nbsp;AND&nbsp;objid&nbsp;=&nbsp;l_objid&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lc_key<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;lc_desc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lc_subrc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;temp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lc_temp.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lc_subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'没有相应的数据模板下载'&nbsp;TYPE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_FILENAME<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_filename .<br/>
&nbsp;&nbsp;DATA:&nbsp;l_files&nbsp;TYPE&nbsp;filetable,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;h_files&nbsp;TYPE&nbsp;file_table,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_rc&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-subrc.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_open_dialog&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'XLSX'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_files&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_open_dialog_failed&nbsp;=&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;OR&nbsp;l_rc&nbsp;&lt;&nbsp;0.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;ENDIF.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mo271101<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;l_files&nbsp;INDEX&nbsp;1&nbsp;INTO&nbsp;h_files.<br/>
&nbsp;&nbsp;f_file&nbsp;=&nbsp;h_files-filename.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_data .<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SAPGUI_PROGRESS_INDICATOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;percentage&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'正在上传数据……'.<br/>
<br/>
</div>
<div class="codeComment">
***********************************************************************************************************<br/>
*&nbsp;&nbsp;&nbsp;excel&nbsp;导入<br/>
***********************************************************************************************************<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;lt_fname&nbsp;&nbsp;TYPE&nbsp;filetable,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fname&nbsp;&nbsp;TYPE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_subrc&nbsp;&nbsp;TYPE&nbsp;sy-subrc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lwa_fname&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_fname.<br/>
&nbsp;&nbsp;DATA:&nbsp;BEGIN&nbsp;OF&nbsp;lt_excel&nbsp;OCCURS&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCLUDE&nbsp;STRUCTURE&nbsp;alsmex_tabline.<br/>
&nbsp;&nbsp;DATA:&nbsp;END&nbsp;OF&nbsp;lt_excel.<br/>
&nbsp;&nbsp;IF&nbsp;f_file&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_open_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'*.xls'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_fname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_subrc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_open_dialog_failed&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_fname&nbsp;INTO&nbsp;lwa_fname&nbsp;INDEX&nbsp;1.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_fname&nbsp;=&nbsp;lwa_fname-filename.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_fname&nbsp;=&nbsp;&nbsp;f_file.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;lv_fname&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'请输入文件上传路径'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_fname&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ALSM_EXCEL_TO_INTERNAL_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_fname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;=&nbsp;15<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;=&nbsp;65536<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_excel[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCONSISTENT_PARAMETERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPLOAD_OLE&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_excel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;lt_excel-col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-matnr&nbsp;=&nbsp;lt_excel-value."*matnr&nbsp;TYPE&nbsp;&nbsp;c&nbsp;LENGTH&nbsp;&nbsp;18&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;物料号(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;2."存货简称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;3."物料描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;4."&nbsp;&nbsp;工厂&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-werks&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;5."MRP&nbsp;范围(必填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-berid&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;6."&nbsp;&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-dismm&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;7."&nbsp;&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-dispo&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;8."&nbsp;&nbsp;批量大小&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-disls&nbsp;=&nbsp;lt_excel-value.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;9."&nbsp;&nbsp;特殊采购类型&nbsp;&nbsp;选填<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-sobsl&nbsp;=&nbsp;lt_excel-value.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;9."&nbsp;&nbsp;供货工厂&nbsp;&nbsp;选填<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-werksg&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;10."外部采购仓储地点<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-lgfsb&nbsp;=&nbsp;lt_excel-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;11."&nbsp;&nbsp;计划交货时间&nbsp;&nbsp;(选填)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-plifz&nbsp;=&nbsp;lt_excel-value.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;wa_upload_mrp&nbsp;TO&nbsp;gt_upload_mrp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:wa_upload_mrp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_PROCESS_MRP<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_process_mrp .<br/>
&nbsp;&nbsp;DATA:l_msg&nbsp;TYPE&nbsp;char200.<br/>
&nbsp;&nbsp;DATA:l_where&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_upload_mrp&nbsp;INTO&nbsp;wa_upload_mrp&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;wa_upload_mrp&nbsp;TO&nbsp;wa_out_mrp&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_sy_conversion_no_number&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-icon&nbsp;=&nbsp;icon_led_red&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"数值字段不能输入字符<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_field_numc&nbsp;USING:&nbsp;"wa_upload_mrp-disls&nbsp;'批量大小'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_upload_mrp-plifz&nbsp;'计划交货时间'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_msg&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'数值字段：'&nbsp;l_msg&nbsp;'不能包含字符'&nbsp;INTO&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;&nbsp;wa_out_mrp-msg&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-msg&nbsp;&nbsp;=&nbsp;'数据填写错误'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSLATE&nbsp;wa_out_mrp-matnr&nbsp;TO&nbsp;UPPER&nbsp;CASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;wa_out_mrp&nbsp;TO&nbsp;gt_out_mrp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;wa_out_mrp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSLATE&nbsp;wa_out_mrp-matnr&nbsp;TO&nbsp;UPPER&nbsp;CASE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-icon&nbsp;=&nbsp;icon_led_yellow&nbsp;.<br/>
</div>
<div class="codeComment">
*必填字段不能为空<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_field_empty&nbsp;USING&nbsp;:&nbsp;wa_out_mrp-matnr&nbsp;'物料编码'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-werks&nbsp;'工厂'&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-berid&nbsp;'MRP范围'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-dismm&nbsp;'MRP类型'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-dispo&nbsp;'MRP控制者'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-disls&nbsp;'批量大小'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-sobsl&nbsp;'特殊采购类型'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-lgfsb&nbsp;'外部采购仓储地点'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-plifz&nbsp;'计划交货时间'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;l_msg<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_msg&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'必输字段：'&nbsp;l_msg&nbsp;'不能为空'&nbsp;INTO&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;&nbsp;wa_out_mrp-msg&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"判断是否已经维护基本视图。<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_basis_view&nbsp;USING&nbsp;wa_out_mrp-matnr&nbsp;wa_out_mrp-werks&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
</div>
<div class="codeComment">
*&nbsp;MRP控制着，加前导零<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;wa_out_mrp-dispo<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;wa_out_mrp-dispo.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;物料&nbsp;&nbsp;matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-matnr&nbsp;'物料'&nbsp;'MARA'&nbsp;'MATNR'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;工厂&nbsp;&nbsp;WERKS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-werks&nbsp;'工厂'&nbsp;'T001W'&nbsp;'WERKS'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"供货工厂&nbsp;&nbsp;WERKSG<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-werksg&nbsp;'供货工厂'&nbsp;'T001W'&nbsp;'WERKS'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;MRP范围&nbsp;berid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-berid&nbsp;'MRP范围'&nbsp;'MDLV'&nbsp;'BERID'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"MRP类型&nbsp;&nbsp;DISMM<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-dismm&nbsp;'MRP类型'&nbsp;'T438A'&nbsp;'DISMM'&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"MRP控制者&nbsp;&nbsp;DISPO<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_where.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'WERKS&nbsp;=&nbsp;'''&nbsp;wa_out_mrp-werks&nbsp;&nbsp;'''&nbsp;AND&nbsp;DISPO'&nbsp;INTO&nbsp;l_where.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-dispo&nbsp;'MRP控制者'&nbsp;'T024D'&nbsp;l_where&nbsp;&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"批量大小&nbsp;&nbsp;DISLS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-disls&nbsp;'批量大小'&nbsp;'T439A'&nbsp;'DISLS'&nbsp;&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"外部采购仓储地点<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_where.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'WERKS&nbsp;=&nbsp;'''&nbsp;wa_out_mrp-werks&nbsp;'''&nbsp;AND&nbsp;LGORT'&nbsp;''&nbsp;INTO&nbsp;l_where.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-lgfsb&nbsp;'外部采购仓储地点'&nbsp;'T001L'&nbsp;l_where&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;"特殊采购类型<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_where.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'WERKS&nbsp;=&nbsp;'''&nbsp;wa_out_mrp-werks&nbsp;'''&nbsp;AND&nbsp;SOBSL'&nbsp;''&nbsp;INTO&nbsp;l_where.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_single_input&nbsp;USING&nbsp;wa_out_mrp-sobsl&nbsp;'特殊采购类型'&nbsp;'T460A'&nbsp;l_where&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
<br/>
*&lt;START&gt;------「Delete&nbsp;By&nbsp;Zhangyh&nbsp;Time:&nbsp;29.07.2021&nbsp;09:57:57」-------&lt;START&gt;*<br/>
****----取物料描述存货简称<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_mara_maktx&nbsp;USING&nbsp;wa_out_mrp-matnr&nbsp;CHANGING&nbsp;wa_out_mrp-maktx&nbsp;wa_out_mrp-zzchjc.<br/>
*&lt;END&gt;--------「Delete&nbsp;By&nbsp;Zhangyh&nbsp;Time:&nbsp;29.07.2021&nbsp;09:57:57」---------&lt;END&gt;*<br/>
<br/>
****----取特殊采购类型<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;sobsl&nbsp;INTO&nbsp;wa_out_mrp-sobsl&nbsp;FROM&nbsp;t460a<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;wa_out_mrp-werks&nbsp;AND&nbsp;wrk02&nbsp;=&nbsp;wa_out_mrp-werksg.<br/>
*&lt;START&gt;------「Delete&nbsp;By&nbsp;Zhangyh&nbsp;Time:&nbsp;29.07.2021&nbsp;09:58:44」-------&lt;START&gt;*<br/>
****----取物流周期<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;wlzq&nbsp;INTO&nbsp;wa_out_mrp-plifz&nbsp;FROM&nbsp;zdm_wuliu<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;drgcbm&nbsp;=&nbsp;wa_out_mrp-werks&nbsp;AND&nbsp;ghgcbm&nbsp;=&nbsp;wa_out_mrp-werksg.<br/>
*&lt;END&gt;--------「Delete&nbsp;By&nbsp;Zhangyh&nbsp;Time:&nbsp;29.07.2021&nbsp;09:58:44」---------&lt;END&gt;*<br/>
****----供货工厂不为空时物流周期必须大于0<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;maktx&nbsp;INTO&nbsp;wa_out_mrp-maktx&nbsp;FROM&nbsp;makt&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;wa_out_mrp-matnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;wa_out_mrp-werksg&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;wa_out_mrp-plifz&nbsp;=&nbsp;0&nbsp;OR&nbsp;wa_out_mrp-plifz&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;wa_out_mrp-msg&nbsp;'没有计划交货时间！'&nbsp;INTO&nbsp;wa_out_mrp-msg&nbsp;SEPARATED&nbsp;BY&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out_mrp-icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;wa_out_mrp&nbsp;TO&nbsp;gt_out_mrp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;wa_out_mrp&nbsp;.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_FIELD_EMPTY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP_WERKS&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0386&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_field_empty USING p_field_value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_field_desc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg.<br/>
&nbsp;&nbsp;IF&nbsp;p_field_value&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_msg&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;p_field_desc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;p_msg&nbsp;','&nbsp;p_field_desc&nbsp;INTO&nbsp;p_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_BASIS_VIEW<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP_MATNR&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_basis_view USING p_matnr p_werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg.<br/>
&nbsp;&nbsp;DATA:l_vpsta&nbsp;TYPE&nbsp;mara-vpsta,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_pstat&nbsp;TYPE&nbsp;marc-pstat.<br/>
&nbsp;&nbsp;CHECK&nbsp;p_matnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vpsta&nbsp;INTO&nbsp;l_vpsta&nbsp;FROM&nbsp;mara&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;p_matnr.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;'基本视图未维护'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_vpsta&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;l_vpsta&nbsp;NS&nbsp;'K'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;'基本视图未维护'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;pstat&nbsp;INTO&nbsp;l_pstat&nbsp;FROM&nbsp;marc&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;p_matnr&nbsp;AND&nbsp;werks&nbsp;=&nbsp;p_werks.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;'工厂'&nbsp;&amp;&amp;&nbsp;p_werks&nbsp;&amp;&amp;&nbsp;'视图不存在'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_pstat&nbsp;NS&nbsp;'D'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;'工厂'&nbsp;&amp;&amp;&nbsp;p_werks&nbsp;&amp;&amp;&nbsp;'MRP视图未维护'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_FIELD_NUMC<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_UPLOAD_MRP_PLIFZ&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0328&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_field_numc USING p_field_value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_field_desc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg.<br/>
&nbsp;&nbsp;DATA:l_value&nbsp;TYPE&nbsp;char50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_type&nbsp;&nbsp;TYPE&nbsp;dd01v-datatype.<br/>
&nbsp;&nbsp;IF&nbsp;p_field_value&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_value&nbsp;=&nbsp;p_field_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;REPLACE&nbsp;','&nbsp;IN&nbsp;l_value&nbsp;WITH&nbsp;&nbsp;'&nbsp;'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;REPLACE&nbsp;'.'&nbsp;IN&nbsp;l_value&nbsp;WITH&nbsp;&nbsp;'&nbsp;'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;l_value&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'NUMERIC_CHECK'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;string_in&nbsp;=&nbsp;l_value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STRING_OUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;htype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_type.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_type&nbsp;NE&nbsp;'NUMC'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_msg&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg&nbsp;=&nbsp;p_field_desc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;p_msg&nbsp;','&nbsp;p_field_desc&nbsp;INTO&nbsp;p_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_SINGLE_INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP_MATNR&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0476&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0477&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0478&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_single_input USING p_value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_desc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_tablename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_l_where<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_msg.<br/>
&nbsp;&nbsp;DATA:l_where&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;p_l_where&nbsp;'&nbsp;=&nbsp;'''&nbsp;p_value&nbsp;''''&nbsp;INTO&nbsp;l_where.<br/>
&nbsp;&nbsp;IF&nbsp;p_value&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(&nbsp;*&nbsp;)&nbsp;FROM&nbsp;(p_tablename)&nbsp;WHERE&nbsp;(l_where).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;p_msg&nbsp;p_desc&nbsp;&nbsp;p_value&nbsp;'不存在'&nbsp;INTO&nbsp;p_msg&nbsp;SEPARATED&nbsp;BY&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_ALV_DISPLAY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_alv_display .<br/>
&nbsp;&nbsp;g_repid&nbsp;=&nbsp;sy-repid&nbsp;.<br/>
&nbsp;&nbsp;g_title&nbsp;=&nbsp;'MRP区域维护清单'&nbsp;.<br/>
&nbsp;&nbsp;REFRESH&nbsp;gt_fieldcat[]&nbsp;.<br/>
&nbsp;&nbsp;gs_layout-colwidth_optimize&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-box_fieldname&nbsp;=&nbsp;'SEL'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_field&nbsp;USING&nbsp;:"&nbsp;'SEL'&nbsp;&nbsp;'选择'&nbsp;''&nbsp;'4',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ICON'&nbsp;&nbsp;'标识'&nbsp;''&nbsp;'4',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MSG'&nbsp;&nbsp;'消息'&nbsp;''&nbsp;'200',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MATNR'&nbsp;&nbsp;'物料号'&nbsp;''&nbsp;'20',<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZZCHJC'&nbsp;&nbsp;'存货简称'&nbsp;''&nbsp;'20',<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MAKTX'&nbsp;&nbsp;'物料描述'&nbsp;''&nbsp;'40',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'WERKS'&nbsp;&nbsp;'工厂'&nbsp;''&nbsp;'6',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'BERID'&nbsp;&nbsp;'MRP范围'&nbsp;''&nbsp;'20',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'DISMM'&nbsp;&nbsp;'MRP类型'&nbsp;''&nbsp;'20',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'DISPO'&nbsp;&nbsp;'MRP控制者'&nbsp;''&nbsp;'20',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'DISLS'&nbsp;&nbsp;'批量大小'&nbsp;''&nbsp;'20',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'WERKSG'&nbsp;&nbsp;'供货工厂'&nbsp;''&nbsp;'6',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'LGFSB'&nbsp;&nbsp;'外部采购仓储地点'&nbsp;''&nbsp;'20',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'PLIFZ'&nbsp;&nbsp;'计划交货时间'&nbsp;''&nbsp;'20'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'SOBSL'&nbsp;&nbsp;'特殊采购类型'&nbsp;''&nbsp;'20'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_events<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_grid_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_title<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'FRM_STATUS_SET'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'FRM_USERCOMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_out_mrp[].<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILL_FIELD<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0870&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0871&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0872&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0873&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_field USING p_l_field<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_l_fieldtxt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_l_qfieldname&nbsp;p_l_lengh.<br/>
&nbsp;&nbsp;CLEAR:wa_fieldcat.<br/>
&nbsp;&nbsp;MOVE&nbsp;p_l_field&nbsp;TO&nbsp;wa_fieldcat-fieldname.<br/>
&nbsp;&nbsp;MOVE&nbsp;p_l_fieldtxt&nbsp;TO&nbsp;:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_fieldcat-seltext_l,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_fieldcat-seltext_m,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_fieldcat-seltext_s.<br/>
&nbsp;&nbsp;"&nbsp;&nbsp;wa_fieldcat-qfieldname&nbsp;=&nbsp;p_l_qfieldname.<br/>
&nbsp;&nbsp;"&nbsp;&nbsp;wa_fieldcat-decimals_out&nbsp;=&nbsp;0.&nbsp;&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;wa_fieldcat-outputlen&nbsp;=&nbsp;p_l_lengh.<br/>
&nbsp;&nbsp;APPEND&nbsp;wa_fieldcat&nbsp;TO&nbsp;gt_fieldcat.<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_status_set USING extab TYPE slis_t_extab.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'ZSTATUS'.<br/>
ENDFORM. "frm_FRM_STATUS_SET<br/>
<br/>
FORM frm_usercommand USING ucomm LIKE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lr_grid&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid.<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;ls_stable&nbsp;TYPE&nbsp;lvc_s_stbl.<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_GLOBALS_FROM_SLVC_FULLSCR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_grid&nbsp;=&nbsp;lr_grid.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lr_grid-&gt;check_changed_data.<br/>
<br/>
&nbsp;&nbsp;ls_stable-row&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_stable-col&nbsp;=&nbsp;'X'.<br/>
<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;ucomm&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_goto_mm03&nbsp;USING&nbsp;&nbsp;selfield-tabindex.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SEND'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_save_data.<br/>
&nbsp;&nbsp;ENDCASE&nbsp;.<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lr_grid-&gt;refresh_table_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_stable&nbsp;=&nbsp;ls_stable<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;finished&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.&nbsp;ENDIF.<br/>
<br/>
ENDFORM . "user_command<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_SAVE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_save_data .<br/>
&nbsp;&nbsp;DATA:lt_mdma&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;mdma&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tabix&nbsp;TYPE&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;CHECK&nbsp;gt_out_mrp[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_out_mrp&nbsp;TRANSPORTING&nbsp;NO&nbsp;FIELDS&nbsp;WHERE&nbsp;icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'存在错误数据，请排除后再操作'&nbsp;TYPE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_mdma&nbsp;FROM&nbsp;mdma<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_out_mrp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;gt_out_mrp-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;berid&nbsp;=&nbsp;gt_out_mrp-berid.<br/>
&nbsp;&nbsp;SORT&nbsp;lt_mdma&nbsp;BY&nbsp;matnr&nbsp;berid.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_out_mrp&nbsp;INTO&nbsp;wa_out_mrp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_tabix&nbsp;=&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_mdma&nbsp;WITH&nbsp;KEY&nbsp;matnr&nbsp;=&nbsp;wa_out_mrp-matnr&nbsp;berid&nbsp;=&nbsp;wa_out_mrp-berid&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_mrparea_maintain&nbsp;USING&nbsp;wa_out_mrp&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_mrparea_create&nbsp;USING&nbsp;wa_out_mrp&nbsp;CHANGING&nbsp;wa_out_mrp-icon&nbsp;wa_out_mrp-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_out_mrp&nbsp;FROM&nbsp;wa_out_mrp&nbsp;INDEX&nbsp;l_tabix.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_MARA_MAKTX<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP_MATNR&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_MAKTX&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ZZCHJC&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_mara_maktx USING p_wa_out_mrp_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_wa_out_mrp_maktx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_zzchjc.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zzchjc&nbsp;INTO&nbsp;p_wa_out_mrp_zzchjc&nbsp;FROM&nbsp;mara&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;p_wa_out_mrp_matnr.<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;maktx&nbsp;INTO&nbsp;p_wa_out_mrp_maktx&nbsp;FROM&nbsp;makt&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;p_wa_out_mrp_matnr&nbsp;AND&nbsp;spras&nbsp;=&nbsp;sy-langu.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_MRPAREA_MAINTAIN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_mrparea_maintain USING p_wa_out_mrp TYPE ty_out_mrp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_wa_out_mrp_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_msg.<br/>
&nbsp;&nbsp;DATA:&nbsp;s_mdma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;mdma,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_dpop&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;dpop,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_selfields&nbsp;LIKE&nbsp;sdibe_massfields,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_return&nbsp;&nbsp;&nbsp;LIKE&nbsp;bapireturn1.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;s_mdma&nbsp;FROM&nbsp;mdma&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;&nbsp;p_wa_out_mrp-matnr&nbsp;AND&nbsp;berid&nbsp;=&nbsp;&nbsp;p_wa_out_mrp-berid.<br/>
&nbsp;&nbsp;CLEAR&nbsp;s_selfields.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_selfields&nbsp;CHANGING&nbsp;s_selfields.<br/>
&nbsp;&nbsp;s_mdma-dismm&nbsp;=&nbsp;p_wa_out_mrp-dismm."&nbsp;&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-dispo&nbsp;=&nbsp;p_wa_out_mrp-dispo."&nbsp;&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-disls&nbsp;=&nbsp;p_wa_out_mrp-disls."&nbsp;&nbsp;批量大小&nbsp;&nbsp;必填<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;s_mdma-sobsl&nbsp;=&nbsp;p_wa_out_mrp-sobsl."&nbsp;&nbsp;特殊采购类型&nbsp;&nbsp;必填<br/>
</div>
<div class="code">
&nbsp;&nbsp;s_mdma-lgfsb&nbsp;=&nbsp;p_wa_out_mrp-lgfsb."&nbsp;&nbsp;外部采购仓储地点&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-plifz&nbsp;=&nbsp;p_wa_out_mrp-plifz."&nbsp;&nbsp;计划交货时间&nbsp;&nbsp;必填<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MD_MRP_LEVEL_CHANGE_DATA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_werk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mrp_area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-berid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_berty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'02'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_selfields&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_selfields<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mdma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_mdma<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_dpop&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_dpop<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_QUEUE_FLAG&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SAVE_FLAG&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_EXTERNAL_COMMIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_error_return&nbsp;=&nbsp;ls_return.<br/>
&nbsp;&nbsp;IF&nbsp;ls_return-type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_msg&nbsp;=&nbsp;ls_return-message.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_icon&nbsp;=&nbsp;icon_led_green.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_MRPAREA_CREATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_WA_OUT_MRP&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_ICON&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_WA_OUT_MRP_MSG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_mrparea_create USING p_wa_out_mrp TYPE ty_out_mrp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_wa_out_mrp_icon<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_msg.<br/>
&nbsp;&nbsp;DATA:&nbsp;s_mdma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;mdma,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_dpop&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;dpop,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_selfields&nbsp;LIKE&nbsp;sdibe_massfields,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_return&nbsp;&nbsp;&nbsp;LIKE&nbsp;bapireturn1.<br/>
&nbsp;&nbsp;CLEAR&nbsp;s_selfields.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_selfields&nbsp;CHANGING&nbsp;s_selfields.<br/>
&nbsp;&nbsp;s_mdma-matnr&nbsp;=&nbsp;p_wa_out_mrp-matnr."&nbsp;&nbsp;物料<br/>
&nbsp;&nbsp;s_mdma-werks&nbsp;=&nbsp;p_wa_out_mrp-werks."&nbsp;&nbsp;工厂<br/>
&nbsp;&nbsp;s_mdma-berid&nbsp;=&nbsp;p_wa_out_mrp-berid."&nbsp;&nbsp;MRP区域<br/>
&nbsp;&nbsp;s_mdma-dismm&nbsp;=&nbsp;p_wa_out_mrp-dismm."&nbsp;&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-dispo&nbsp;=&nbsp;p_wa_out_mrp-dispo."&nbsp;&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-disls&nbsp;=&nbsp;p_wa_out_mrp-disls."&nbsp;&nbsp;批量大小&nbsp;&nbsp;必填<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;s_mdma-sobsl&nbsp;=&nbsp;p_wa_out_mrp-sobsl."&nbsp;&nbsp;特殊采购类型&nbsp;&nbsp;必填<br/>
</div>
<div class="code">
&nbsp;&nbsp;s_mdma-lgfsb&nbsp;=&nbsp;p_wa_out_mrp-lgfsb."&nbsp;&nbsp;外部采购仓储地点&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;s_mdma-plifz&nbsp;=&nbsp;p_wa_out_mrp-plifz."&nbsp;&nbsp;计划交货时间&nbsp;&nbsp;必填<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MD_MRP_LEVEL_CREATE_DATA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_werk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mrp_area&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_wa_out_mrp-berid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_selfields&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_selfields<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mdma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_mdma<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_dpop&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;s_dpop<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_QUEUE_FLAG&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SAVE_FLAG&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_EXTERNAL_COMMIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_error_return&nbsp;=&nbsp;ls_return.<br/>
&nbsp;&nbsp;IF&nbsp;ls_return-type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_msg&nbsp;=&nbsp;ls_return-message.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_wa_out_mrp_icon&nbsp;=&nbsp;icon_led_green.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_SET_SELFIELDS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_S_SELFIELDS&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_selfields CHANGING p_s_selfields TYPE sdibe_massfields.<br/>
<br/>
<br/>
&nbsp;&nbsp;p_s_selfields-xdismm&nbsp;=&nbsp;'X'."&nbsp;&nbsp;MRP类型&nbsp;必填<br/>
&nbsp;&nbsp;p_s_selfields-xdispo&nbsp;=&nbsp;'X'."&nbsp;&nbsp;MRP控制者&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;p_s_selfields-xdisls&nbsp;=&nbsp;'X'."&nbsp;&nbsp;批量大小&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;p_s_selfields-xsobsl&nbsp;=&nbsp;'X'."&nbsp;&nbsp;特殊采购类型&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;p_s_selfields-xlgfsb&nbsp;=&nbsp;'X'."&nbsp;&nbsp;外部采购仓储地点&nbsp;&nbsp;必填<br/>
&nbsp;&nbsp;p_s_selfields-xplifz&nbsp;=&nbsp;'X'."&nbsp;&nbsp;计划交货时间&nbsp;&nbsp;必填<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GOTO_MM03<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_RS_SELFIELD_TABINDEX&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_goto_mm03 USING p_rs_selfield_tabindex.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_out_mrp&nbsp;INDEX&nbsp;p_rs_selfield_tabindex&nbsp;INTO&nbsp;wa_out_mrp.&nbsp;"双击所在行<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'MAT'&nbsp;FIELD&nbsp;wa_out_mrp-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'MM03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>