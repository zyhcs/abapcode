<table class="outerTable">
<tr>
<td><h2>Table: ZPPT005</h2>
<h3>Description: 热电厂负责人基础表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZVERAN</td>
<td>3</td>
<td>X</td>
<td>ZE_ZVERAN</td>
<td>ZDM_ZVERAN</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>负责人</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZDEFAULT</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZDEFAULT</td>
<td>ZDM_ZDEFAULT</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>默认值</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZDM_ZDEFAULT</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>ZDM_ZDEFAULT</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>