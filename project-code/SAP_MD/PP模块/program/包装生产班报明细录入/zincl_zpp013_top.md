<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP013_TOP</h2>
<h3> Description: Include ZINCL_ZPP013_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP013_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA: gw_zppt024 TYPE zppt024,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_zppt024&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;zppt024.<br/>
<br/>
DATA:BEGIN OF ty_condition_sql OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;condition(80),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_condition_sql.<br/>
<br/>
DATA gv_name1 TYPE name1. "工厂名称<br/>
<br/>
DATA:      ok_code LIKE sy-ucomm.<br/>
TABLES:ZPPT007.<br/>
<br/>
DATA: wa_layo TYPE lvc_s_layo.<br/>
DATA: wa_fcat TYPE lvc_s_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fcat&nbsp;TYPE&nbsp;lvc_t_fcat.<br/>
<br/>
DATA  it_scol TYPE lvc_t_scol.<br/>
<br/>
DATA: g_grid TYPE REF TO cl_gui_alv_grid.<br/>
<br/>
FIELD-SYMBOLS: &lt;dyn_table&gt; TYPE STANDARD TABLE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;dyn_wa&gt;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;dyn_field&gt;.<br/>
<br/>
DATA: dy_table TYPE REF TO data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dy_line&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;data.<br/>
<br/>
DATA: colname(30).<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>