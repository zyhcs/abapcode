<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP002_UPDOWN</h2>
<h3> Description: Include ZINCL_ZPPB001_UPDOWN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPPB001_UPDOWN<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
DATA c_excel TYPE string VALUE 'Excel文件(*.XLS)|*.XLS|Excel文件(*.xlsx)|*.xlsx|全部文件 (*.*)|*.*|'.<br/>
DATA c_txt TYPE string VALUE 'TXT文件(*.TXT)|*.TXT|全部文件 (*.*)|*.*|' .<br/>
DATA gt_alsmex TYPE issr_alsmex_tabline.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;包含文件&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_CMUPDOWN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_F4_FILENAME<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;文件f4帮助<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;E_FILE&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_filename CHANGING e_file TYPE ibipparms-path.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4_FILENAME'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_name&nbsp;&nbsp;=&nbsp;sy-cprog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpro_number&nbsp;=&nbsp;sy-dynnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;field_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;e_file.<br/>
ENDFORM. " FRM_F4_FILENAME<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILE_SAVETEMPLET<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*FORM&nbsp;FRM_FILE_SAVETEMPLET&nbsp;.<br/>
*&nbsp;&nbsp;DATA&nbsp;LS_FILE&nbsp;TYPE&nbsp;LOCALFILE.<br/>
*&nbsp;&nbsp;PERFORM&nbsp;FRM_FILE_SAVE_DIALOG&nbsp;USING&nbsp;SPACE&nbsp;&nbsp;&nbsp;C_EXCEL&nbsp;SPACE&nbsp;CHANGING&nbsp;LS_FILE.<br/>
*&nbsp;&nbsp;CHECK&nbsp;LS_FILE&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
*&nbsp;&nbsp;PERFORM&nbsp;FRM_DOWNLOAD_TEMPLATE&nbsp;USING&nbsp;C_TEMPLATE&nbsp;ls_FILE.<br/>
*<br/>
*ENDFORM.<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILE_SAVE_DIALOG<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_file_save_dialog USING i_dfname TYPE any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_filter&nbsp;TYPE&nbsp;any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_overwrite&nbsp;TYPE&nbsp;flag<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;e_fullpath&nbsp;TYPE&nbsp;clike.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'保存文件'.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_default_file_name&nbsp;TYPE&nbsp;string&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fullpath&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'C:\*\*\Desktop\'&nbsp;."默认桌面<br/>
&nbsp;&nbsp;DATA&nbsp;ls_user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;i_dfname&nbsp;&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;i_dfname&nbsp;TO&nbsp;ls_default_file_name&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;sy-title&nbsp;'_'&nbsp;sy-datum&nbsp;&nbsp;INTO&nbsp;ls_default_file_name&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_window_title<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;space<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_default_file_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prompt_on_overwrite&nbsp;&nbsp;=&nbsp;i_overwrite<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_user_action<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Clear&nbsp;fullpath&nbsp;if&nbsp;cancel<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;ls_user_action&nbsp;=&nbsp;cl_gui_frontend_services=&gt;action_cancel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;e_fullpath.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_fullpath&nbsp;=&nbsp;ls_fullpath.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GETPATH<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_P_FILE&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_getpath USING i_filter TYPE any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;e_fullpath&nbsp;TYPE&nbsp;any&nbsp;.<br/>
<br/>
&nbsp;&nbsp;"""""""""""""""''function<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;'F4_FILENAME'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_name&nbsp;&nbsp;=&nbsp;syst-cprog<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpro_number&nbsp;=&nbsp;syst-dynnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;field_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;I_FILE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;"""""""""""""""""""""""""""""""""""""<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;f_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;filetable&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;ls_initname&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_user_action&nbsp;TYPE&nbsp;i&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_wtitle&nbsp;TYPE&nbsp;string&nbsp;VALUE&nbsp;'文件选择'&nbsp;.<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;f_table&gt;&nbsp;TYPE&nbsp;file_table&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;i_filter&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;c_excel&nbsp;TO&nbsp;i_filter&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;concatenate&nbsp;&nbsp;'XXXX'&nbsp;&nbsp;'-'&nbsp;SY-DATUM&nbsp;SY-UZEIT'.xls'&nbsp;into&nbsp;&nbsp;L_INITNAME.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_open_dialog&nbsp;"打开<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;call&nbsp;method&nbsp;cl_gui_frontend_services=&gt;file_save_dialog&nbsp;"保存<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选择文件'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_initname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C:\*\*\Desktop\'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;multiselection&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;f_table<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;rc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_user_action<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_open_dialog_failed&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_cfw=&gt;flush<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_system_error&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;ls_user_action&nbsp;EQ&nbsp;cl_gui_frontend_services=&gt;action_cancel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;e_fullpath&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;f_table&nbsp;ASSIGNING&nbsp;&lt;f_table&gt;&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_fullpath&nbsp;=&nbsp;&lt;f_table&gt;-filename.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
&nbsp;&nbsp;""""""""""""""""""""''文件夹选择<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'TMP_GUI_BROWSE_FOR_FOLDER'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选择文件夹'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_folder&nbsp;&nbsp;=&nbsp;''<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selected_folder&nbsp;=&nbsp;p_file<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
</div>
<div class="code">
&nbsp;&nbsp;"""""""""""""""""""""""""""""""""""""""""""""""""""<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'WS_FILENAME_GET'<br/>
*&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'O'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选取路径'<br/>
*&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gv_file<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inv_winsys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_batch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selection_cancel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selection_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
</div>
<div class="codeComment">
*PERFORM&nbsp;FRM_GETFILE&nbsp;CHANGING&nbsp;FILE&nbsp;.<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GETFILE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_打开文件夹<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_getfilepath CHANGING i_filepath TYPE any .<br/>
&nbsp;&nbsp;DATA&nbsp;ls_selected_folder(100)&nbsp;TYPE&nbsp;c&nbsp;.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'TMP_GUI_BROWSE_FOR_FOLDER'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选择文件夹'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_folder&nbsp;&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;selected_folder&nbsp;=&nbsp;ls_selected_folder<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;MOVE&nbsp;ls_selected_folder&nbsp;TO&nbsp;i_filepath&nbsp;.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_FILE_EXIST<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;检查模板是否存在<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_FILENAME&nbsp;&nbsp;&nbsp;文件名<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;E_EXIST&nbsp;&nbsp;是否存在<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_file_exist USING i_filename TYPE any .<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_file&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_exist&nbsp;&nbsp;TYPE&nbsp;abap_bool.<br/>
&nbsp;&nbsp;ls_file&nbsp;=&nbsp;i_filename.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_exist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;result&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_exist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_parameter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;ls_exist&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;8&nbsp;TO&nbsp;sy-subrc&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;0&nbsp;TO&nbsp;sy-subrc&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
ENDFORM. "FRM_CHECK_FILE_EXIST<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_DOWNLOAD_TEMPLATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下载模版<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_OBJID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;模版对象名称<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_FILENAME&nbsp;&nbsp;下载文件名<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_download_template USING i_objid TYPE wwwdatatab-objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_file&nbsp;&nbsp;&nbsp;TYPE&nbsp;any&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA&nbsp;LS_FILENAME&nbsp;TYPE&nbsp;STRING&nbsp;&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_exist&nbsp;TYPE&nbsp;abap_bool.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_wwwdata&nbsp;TYPE&nbsp;wwwdatatab.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA&nbsp;LT_MINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;W3MIME&nbsp;OCCURS&nbsp;10&nbsp;.<br/>
*&nbsp;&nbsp;DATA&nbsp;LS_TITLE&nbsp;TYPE&nbsp;STRING&nbsp;.<br/>
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_rc&nbsp;&nbsp;&nbsp;TYPE&nbsp;sy-subrc.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fullpath&nbsp;TYPE&nbsp;string.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;判断模版文件是否存在<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_check_file_exist&nbsp;USING&nbsp;i_file&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.&nbsp;"已存在<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'模版'&nbsp;i_file&nbsp;'已存在'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'W'.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_wwwdata-relid&nbsp;=&nbsp;'MI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_wwwdata-objid&nbsp;=&nbsp;i_objid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;i_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_rc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_rc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e001(00)&nbsp;WITH&nbsp;'下载模版失败!'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'成功下载模版,下载目录:'&nbsp;i_file&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CONCATENATE&nbsp;&nbsp;I_TITLLE&nbsp;'_'&nbsp;SY-DATUM&nbsp;INTO&nbsp;LS_TITLE.<br/>
*<br/>
*&nbsp;&nbsp;LS_WWWDATA-RELID&nbsp;=&nbsp;'MI'.<br/>
*&nbsp;&nbsp;LS_WWWDATA-OBJID&nbsp;=&nbsp;I_OBJID.<br/>
*<br/>
**模板下载<br/>
*&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'WWWDATA_IMPORT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_WWWDATA<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_MINE<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRONG_OBJECT_TYPE&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORT_ERROR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
*<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;CL_GUI_FRONTEND_SERVICES=&gt;FILE_SAVE_DIALOG<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_TITLE<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEFAULT_EXTENSION&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'xls'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEFAULT_FILE_NAME&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_TITLE<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILE_FILTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;CL_GUI_FRONTEND_SERVICES=&gt;FILETYPE_EXCEL<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILENAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_FILENAME<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PATH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_PATH<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FULLPATH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LS_FULLPATH<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CNTL_ERROR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ERROR_NO_GUI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOT_SUPPORTED_BY_GUI&nbsp;=&nbsp;3<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
*&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;E000&nbsp;WITH&nbsp;'错误！'.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;IF&nbsp;LS_FILENAME&nbsp;=&nbsp;''.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;E000&nbsp;WITH&nbsp;'已取消导出！'&nbsp;.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GUI_DOWNLOAD'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILENAME&nbsp;=&nbsp;LS_FULLPATH<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILETYPE&nbsp;=&nbsp;'BIN'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA_TAB&nbsp;=&nbsp;LT_MINE.<br/>
</div>
<div class="code">
ENDFORM. " download_template<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_EXCEL_TO_ITAB<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEL上传到sap<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--ET_UPLOAD&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_excel_to_tabline USING i_filename TYPE clike<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sheet_name&nbsp;&nbsp;&nbsp;TYPE&nbsp;clike<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sheet_number&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;et_upload&nbsp;TYPE&nbsp;issr_alsmex_tabline.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;0.&nbsp;清除之前的数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;et_upload[].<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_file_exist&nbsp;USING&nbsp;i_filename&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'上传文件'&nbsp;i_filename&nbsp;'不存在!'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;2.&nbsp;显示进度<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SAPGUI_PROGRESS_INDICATOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;=&nbsp;'文件上传中,&nbsp;请稍等......'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;3.&nbsp;读取上传文件内的数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ALSM_EXCEL_TO_INTERNAL_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_begin_col<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_begin_row<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_end_col<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_end_row<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SHNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'Sheet1'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SHEET_NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;I_SHEET_NUMBER<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;et_upload<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inconsistent_parameters&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;upload_ole&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'文件'&nbsp;i_filename&nbsp;'上传不成功,请检查!'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM. " FRM_EXCEL_TO_ITAB<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CONVERTDATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GT_OUT&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_convertdata TABLES it_data TYPE STANDARD TABLE .<br/>
&nbsp;&nbsp;"为&nbsp;&lt;...&gt;&nbsp;插入正确的名称.<br/>
&nbsp;&nbsp;DATA&nbsp;cl_descr&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_abap_structdescr.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_comp&gt;&nbsp;TYPE&nbsp;abap_compdescr.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_alsmex&gt;&nbsp;TYPE&nbsp;alsmex_tabline&nbsp;.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_field&gt;&nbsp;&nbsp;TYPE&nbsp;any.<br/>
&nbsp;&nbsp;cl_descr&nbsp;?=&nbsp;cl_abap_typedescr=&gt;describe_by_data(&nbsp;it_data&nbsp;)."&nbsp;内表数据结构<br/>
&nbsp;&nbsp;DELETE&nbsp;gt_alsmex&nbsp;WHERE&nbsp;value&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alsmex&nbsp;ASSIGNING&nbsp;&lt;ls_alsmex&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;cl_descr-&gt;components&nbsp;ASSIGNING&nbsp;&lt;ls_comp&gt;&nbsp;INDEX&nbsp;&lt;ls_alsmex&gt;-col&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;&lt;ls_comp&gt;-name&nbsp;OF&nbsp;STRUCTURE&nbsp;it_data&nbsp;TO&nbsp;&lt;ls_field&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_field&gt;&nbsp;=&nbsp;&lt;ls_alsmex&gt;-value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;it_data&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;it_data&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDAT&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_SHEETS_NAME<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;获取Excel所有工作表名称<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;T_NAMES&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;I_FILENAME&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_sheets_name TABLES t_names<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USING&nbsp;i_filename&nbsp;TYPE&nbsp;clike.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;application&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;workbook&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;sheets&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;sheet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;sheet_name&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;count&nbsp;TYPE&nbsp;i.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Makro&nbsp;für&nbsp;Fehlerbehandlung&nbsp;der&nbsp;Methods<br/>
</div>
<div class="code">
&nbsp;&nbsp;DEFINE&nbsp;m_message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;sy-subrc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.&nbsp;RAISE&nbsp;upload_ole.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;END-OF-DEFINITION.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;open&nbsp;file&nbsp;in&nbsp;Excel<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;application-header&nbsp;=&nbsp;space&nbsp;OR&nbsp;application-handle&nbsp;=&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;application&nbsp;'Excel.Application'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;m_message.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF<br/>
&nbsp;&nbsp;&nbsp;&nbsp;application<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Workbooks'&nbsp;=&nbsp;workbook.<br/>
&nbsp;&nbsp;m_message.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF<br/>
&nbsp;&nbsp;&nbsp;&nbsp;workbook<br/>
&nbsp;&nbsp;&nbsp;&nbsp;'Open'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#1&nbsp;=&nbsp;i_filename.<br/>
&nbsp;&nbsp;m_message.<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;application&nbsp;'sheets'&nbsp;=&nbsp;sheets.<br/>
&nbsp;&nbsp;m_message.<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;sheets&nbsp;'Count'&nbsp;=&nbsp;count.<br/>
&nbsp;&nbsp;m_message.<br/>
<br/>
&nbsp;&nbsp;DO&nbsp;count&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;sheets&nbsp;'Item'&nbsp;=&nbsp;sheet<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;sy-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;m_message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;sheet&nbsp;'Name'&nbsp;=&nbsp;sheet_name.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;m_message.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;sheet_name&nbsp;TO&nbsp;t_names.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;FREE&nbsp;sheet.<br/>
&nbsp;&nbsp;FREE&nbsp;sheets.<br/>
&nbsp;&nbsp;FREE&nbsp;workbook.<br/>
&nbsp;&nbsp;FREE&nbsp;application.<br/>
ENDFORM. " FRM_GET_SHEETS_NAME<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>