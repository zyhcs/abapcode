<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP004_SELSCR</h2>
<h3> Description: Include ZINCL_ZPP004_SELSCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP004_SELSCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES sscrfields.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FUNCTXT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_ICON_CHANGE&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_0173&nbsp;&nbsp;&nbsp;text<br/>
*PERFORM&nbsp;FRM_FUNCTXT&nbsp;USING&nbsp;ICON_CHANGE&nbsp;'下载模板'&nbsp;CHANGING&nbsp;&nbsp;SSCRFIELDS-FUNCTXT_01.<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_functxt USING i_icon TYPE icon_d<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_text&nbsp;TYPE&nbsp;clike<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;i_functxt&nbsp;TYPE&nbsp;rsfunc_txt&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_dyntxt&nbsp;TYPE&nbsp;smp_dyntxt.<br/>
&nbsp;&nbsp;ls_dyntxt-icon_id&nbsp;&nbsp;&nbsp;=&nbsp;i_icon.<br/>
&nbsp;&nbsp;ls_dyntxt-icon_text&nbsp;=&nbsp;i_text.<br/>
&nbsp;&nbsp;i_functxt&nbsp;=&nbsp;ls_dyntxt&nbsp;.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>