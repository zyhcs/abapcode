<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP009_SSCR</h2>
<h3> Description: Include ZINCL_ZPP009_SSCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP009_SSCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES: mapl,plpo,crhd.<br/>
SELECT-OPTIONS: s_matnr FOR mapl-matnr,<br/>
<br/>
s_werks FOR mapl-werks OBLIGATORY MEMORY ID wrk,<br/>
s_plnnr FOR mapl-plnnr,<br/>
s_plnal FOR mapl-plnal,<br/>
s_vornr FOR plpo-vornr,<br/>
s_steus FOR plpo-steus,<br/>
</div>
<div class="codeComment">
*s_vbeln&nbsp;FOR&nbsp;mapl-vbeln,<br/>
*s_posnr&nbsp;FOR&nbsp;mapl-posnr,<br/>
*&nbsp;s_usr01&nbsp;&nbsp;&nbsp;for&nbsp;&nbsp;plpo-usr01,<br/>
</div>
<div class="code">
s_arbpl FOR crhd-arbpl MATCHCODE OBJECT CRAM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>