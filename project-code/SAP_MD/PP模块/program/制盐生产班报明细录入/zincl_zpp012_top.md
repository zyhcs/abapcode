<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP012_TOP</h2>
<h3> Description: Include ZINCL_ZPP012_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP012_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA: gw_zppt023 TYPE zppt023,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_zppt023&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;zppt023.<br/>
<br/>
DATA:BEGIN OF ty_condition_sql OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;condition(80),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_condition_sql.<br/>
<br/>
DATA gv_name1 TYPE name1. "工厂名称<br/>
<br/>
DATA:      ok_code LIKE sy-ucomm.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>