<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP012_PBO</h2>
<h3> Description: Include ZINCL_ZPP012_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP012_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE frm_init_data OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;vid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vrm_id&nbsp;VALUE&nbsp;'GW_ZPPT023-ZZHIBIE',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vlist&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vrm_values,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;vlist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zppt002&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;zppt002&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;gw_zppt023-werks&nbsp;=&nbsp;p_werks.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;name1&nbsp;INTO&nbsp;gv_name1<br/>
&nbsp;&nbsp;FROM&nbsp;t001w<br/>
&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;p_werks.<br/>
&nbsp;&nbsp;gw_zppt023-zpdate&nbsp;=&nbsp;p_zpdate.<br/>
&nbsp;&nbsp;gw_zppt023-zbanci&nbsp;=&nbsp;p_zbanci.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;lt_zppt002<br/>
&nbsp;&nbsp;FROM&nbsp;zppt002<br/>
&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;p_werks.<br/>
&nbsp;&nbsp;REFRESH:vlist.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_zppt002.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;lt_zppt002-zzhibie&nbsp;TO&nbsp;value-key&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;lt_zppt002-zzhibie&nbsp;TO&nbsp;value-text&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;value&nbsp;TO&nbsp;vlist&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vlist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id_illegal_name&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_0100 OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_extab_line&nbsp;TYPE&nbsp;slis_t_extab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_extab_line&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_extab_line.<br/>
&nbsp;&nbsp;IF&nbsp;p_rb3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_extab_line-fcode&nbsp;=&nbsp;'@SAVE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_extab_line&nbsp;TO&nbsp;lt_extab_line.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI_STATUS_100'&nbsp;EXCLUDING&nbsp;lt_extab_line.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'GUI_TITLE_0100'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;MODIFY_SCREEN_100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE modify_screen_100 OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_rb3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;FRM_INIT_ZVERAN&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE frm_init_zveran OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;vid_02&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vrm_id&nbsp;VALUE&nbsp;'GW_ZPPT023-ZVERAN',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vlist_02&nbsp;&nbsp;&nbsp;TYPE&nbsp;vrm_values,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_02&nbsp;&nbsp;&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;vlist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zppt005&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;zppt005&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;lt_zppt005<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zppt005<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;p_werks.<br/>
<br/>
&nbsp;&nbsp;REFRESH:vlist_02.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_zppt005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;value_02.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;lt_zppt005-zveran&nbsp;TO&nbsp;value_02-key&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;lt_zppt005-zveran&nbsp;TO&nbsp;value_02-text&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;value_02&nbsp;TO&nbsp;vlist_02&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vid_02<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vlist_02<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id_illegal_name&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>