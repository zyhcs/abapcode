<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP024_FUNC</h2>
<h3> Description: Include ZPP024_FUNC</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP024_FUNC<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_set_values<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_values .<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;CONV&nbsp;vrm_id(&nbsp;'S_LSBOX'&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;=&nbsp;VALUE&nbsp;vrm_values(&nbsp;(&nbsp;key&nbsp;=&nbsp;''&nbsp;text&nbsp;=&nbsp;'')<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;key&nbsp;=&nbsp;'A1'&nbsp;text&nbsp;=&nbsp;'产量')<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;key&nbsp;=&nbsp;'A2'&nbsp;text&nbsp;=&nbsp;'消耗'&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;).<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_query_ctrl<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;筛选条件选择<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_query_ctrl .<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'R1'&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---查询显示隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;r_radio1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'R2'&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---查询显示隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;r_radio1&nbsp;=&nbsp;'X'&nbsp;OR&nbsp;r_radio2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'R3'&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---查询显示隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;r_radio3&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_fieldcat<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fieldcat1 .<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_fieldcat.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*调用生产ALV展示字段<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_fields&nbsp;USING:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'WERKS'&nbsp;'生产工厂'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'BUDAT'&nbsp;'过账日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZPDATE'&nbsp;'生产日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZBANCI'&nbsp;'班次'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZZHIBIE'&nbsp;'值别'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZTYPE'&nbsp;'产量/消耗'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MBLNR'&nbsp;'物料凭证'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZEILE'&nbsp;'物料凭证项目'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'AUFNR'&nbsp;'生产订单'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'AUART'&nbsp;'生产订单类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'PLNBEZ'&nbsp;'生产订单物料编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'MATDOC'&nbsp;'MATNR',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'PLNBEZ_TXT'&nbsp;'生产订单物料描述'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'BWART'&nbsp;'移动类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MATNR'&nbsp;'物料编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'MATDOC'&nbsp;'MATNR',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MAKTX'&nbsp;'物料描述'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'CHARG'&nbsp;'批次'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GROES'&nbsp;'规格'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ERFMG'&nbsp;'数量(录入单位)'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ERFME'&nbsp;'录入单位'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MENGE'&nbsp;'数量(基本单位)'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MEINS'&nbsp;'基本单位'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GAMNG'&nbsp;'计划数量'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GMEIN'&nbsp;'计划单位'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'LGORT'&nbsp;'存储地点'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'LGOBE'&nbsp;'存储地点描述'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_fieldcat<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fieldcat2 .<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_fieldcat.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*调用生产ALV展示字段<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_fields&nbsp;USING:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'WERKS'&nbsp;'生产工厂'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'BUDAT'&nbsp;'过账日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZTYPE'&nbsp;'产量/消耗'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MATNR'&nbsp;'物料编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'MATDOC'&nbsp;'MATNR',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MAKTX'&nbsp;'物料描述'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GROES'&nbsp;'规格'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MENGE'&nbsp;'数量(基本单位)'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MEINS'&nbsp;'基本单位'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_fieldcat<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fieldcat3 .<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_fieldcat.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*调用生产ALV展示字段<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_fields&nbsp;USING:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'WERKS'&nbsp;'生产工厂'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZYEAR'&nbsp;'过账年度'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZMONTH'&nbsp;'过账月度'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZTYPE'&nbsp;'产量/消耗'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MATNR'&nbsp;'物料编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'MATDOC'&nbsp;'MATNR',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MAKTX'&nbsp;'物料描述'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GROES'&nbsp;'规格'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MENGE'&nbsp;'数量(基本单位)'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;'',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'MEINS'&nbsp;'基本单位'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_set_fields<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_fields  USING  pv_fieldname pv_reptext pv_zero pv_out pv_key pv_column pv_checkbox pv_edit pv_ref_table pv_ref_field.<br/>
&nbsp;&nbsp;INSERT&nbsp;VALUE&nbsp;#(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fieldname&nbsp;=&nbsp;pv_fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reptext&nbsp;=&nbsp;pv_reptext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_zero&nbsp;=&nbsp;pv_zero<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_out&nbsp;=&nbsp;pv_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;=&nbsp;pv_key<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fix_column&nbsp;=&nbsp;pv_column<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;checkbox&nbsp;=&nbsp;pv_checkbox<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;edit&nbsp;=&nbsp;pv_edit<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ref_table&nbsp;=&nbsp;pv_ref_table<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ref_field&nbsp;=&nbsp;pv_ref_field<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br/>
&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;gt_fieldcat.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_layout<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_layout .<br/>
&nbsp;&nbsp;gs_layout-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-sel_mode&nbsp;&nbsp;&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;gs_layout-cwidth_opt&nbsp;=&nbsp;'X'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_layout-box_fname&nbsp;=&nbsp;'SEL'.<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_display<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_display USING lp_radio TYPE char1 .<br/>
&nbsp;&nbsp;IF&nbsp;r_radio1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_PF_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'ALV_USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_data1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;r_radio2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_PF_STATUS'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'ALV_USER_COMMAND'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_data2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_PF_STATUS'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'ALV_USER_COMMAND'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_data3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;SET_PF_STATUS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;定义工具栏按钮状态<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM set_pf_status USING pt_exclude TYPE kkblo_t_extab .<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STANDARD'&nbsp;.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;ALV_USER_COMMAND<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;定义ALV按钮事件<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM alv_user_command USING r_ucomm LIKE sy-ucomm rs_selfield TYPE slis_selfield .<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;r_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'.&nbsp;&nbsp;&nbsp;"双击&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"对于热点链接，所对应的动作码为"&amp;IC1"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data1&nbsp;INTO&nbsp;DATA(ls_out)&nbsp;INDEX&nbsp;rs_selfield-tabindex.&nbsp;"索引到内表的第几行<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;rs_selfield-fieldname&nbsp;EQ&nbsp;'AUFNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'ANR'&nbsp;FIELD&nbsp;ls_out-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'CO03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;rs_selfield-fieldname&nbsp;EQ&nbsp;'MBLNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MIGO_DIALOG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A04'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_refdoc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'R02'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_notree&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_skip_first_screen&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_okcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'OK_GO'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_out-mblnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_out-zpdate(4)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;illegal_combination&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_f4_month USING i_status TYPE help_info-dynprofld.<br/>
</div>
<div class="codeComment">
*s_region&nbsp;FOR&nbsp;t005s-bland,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"省份<br/>
*&nbsp;&nbsp;RANGES&nbsp;lt_status&nbsp;FOR&nbsp;ztplm002-status.<br/>
</div>
<div class="code">
&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ts_month,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;month&nbsp;TYPE&nbsp;zmonth,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ts_month.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lw_month&nbsp;TYPE&nbsp;ts_month.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_month&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ts_month.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;calc_month&nbsp;TYPE&nbsp;char2.<br/>
<br/>
&nbsp;&nbsp;DO&nbsp;12&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;calc_month&nbsp;=&nbsp;calc_month&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;strlen(&nbsp;calc_month&nbsp;)&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'0'&nbsp;calc_month&nbsp;INTO&nbsp;calc_month.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_month-month&nbsp;=&nbsp;&nbsp;calc_month.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lw_month&nbsp;TO&nbsp;lt_month.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lt_return&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;ddshretval.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_maping&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dselc&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;lt_maping-fldname&nbsp;=&nbsp;'F0002'.&nbsp;&nbsp;"如果没有使用结构,则需要通过是否F加字段在内表中的位置来指定map的值<br/>
*&nbsp;&nbsp;lt_maping-dyfldname&nbsp;=&nbsp;'ZTPLM002-VERSION'.<br/>
*&nbsp;&nbsp;APPEND&nbsp;lt_maping.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=&nbsp;'T005U'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'MONTH'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_status<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_month<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_f4_zbanci<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_zbanci  USING i_status TYPE help_info-dynprofld.<br/>
&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ts_zbanci,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbanci&nbsp;TYPE&nbsp;matdoc-zbanci,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ts_zbanci.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lw_zbanci&nbsp;TYPE&nbsp;ts_zbanci.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_zbanci&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ts_zbanci.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lt_return&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;ddshretval.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_maping&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dselc&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;s_zpdate,s_zpdate[].<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_screen_input.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;DISTINCT&nbsp;zbanci&nbsp;FROM&nbsp;zppt001&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;@p_werks&nbsp;INTO&nbsp;TABLE&nbsp;@lt_zbanci.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;select&nbsp;single&nbsp;zbanci&nbsp;from&nbsp;@lt_zbanci&nbsp;as&nbsp;zbanci&nbsp;into&nbsp;@p_zbanci.<br/>
<br/>
*&nbsp;&nbsp;lt_maping-fldname&nbsp;=&nbsp;'F0002'.&nbsp;&nbsp;"如果没有使用结构,则需要通过是否F加字段在内表中的位置来指定map的值<br/>
*&nbsp;&nbsp;lt_maping-dyfldname&nbsp;=&nbsp;'ZTPLM002-VERSION'.<br/>
*&nbsp;&nbsp;APPEND&nbsp;lt_maping.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=&nbsp;'T005U'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'ZBANCI'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_status<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_zbanci<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_f4_zhibie<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_zhibie  USING i_status TYPE help_info-dynprofld.<br/>
&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ts_zhibie,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zhibie&nbsp;TYPE&nbsp;matdoc-zzhibie,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ts_zhibie.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lw_zhibie&nbsp;TYPE&nbsp;ts_zhibie.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_zhibie&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ts_zhibie.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lt_return&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;ddshretval.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_maping&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dselc&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;s_zpdate,s_zpdate[].<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_screen_input.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;DISTINCT&nbsp;zzhibie&nbsp;FROM&nbsp;zppt002<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;@p_werks<br/>
&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@lt_zhibie.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=&nbsp;'T005U'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'ZHIBIE'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_status<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_zhibie<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_f4_zhibie<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_auart  USING i_status TYPE help_info-dynprofld.<br/>
&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ts_t003o,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;auart&nbsp;TYPE&nbsp;t003o-auart,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;autyp&nbsp;TYPE&nbsp;t003o-autyp,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt&nbsp;&nbsp;&nbsp;TYPE&nbsp;t003p-txt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ts_t003o.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lw_t003o&nbsp;TYPE&nbsp;ts_t003o.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_t003o&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ts_t003o.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lt_return&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;ddshretval.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_maping&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dselc&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_screen_input.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;DISTINCT&nbsp;t003o~auart,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t003o~autyp,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t003p~txt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t003o<br/>
&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;t399x<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;t003o~auart&nbsp;=&nbsp;t399x~auart<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;t399x~autyp&nbsp;=&nbsp;'10'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEFT&nbsp;JOIN&nbsp;t003p<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;t003p~spras&nbsp;=&nbsp;@sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;t003o~auart&nbsp;=&nbsp;t003p~auart<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;t399x~werks&nbsp;=&nbsp;@p_werks<br/>
&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@lt_t003o.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=&nbsp;'T005U'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'AUART'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_status<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_t003o<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
FORM frm_get_screen_input.<br/>
&nbsp;&nbsp;DATA:&nbsp;dynpfields&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dynpread&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;dynpfields,&nbsp;dynpfields[].<br/>
&nbsp;&nbsp;dynpfields-fieldname&nbsp;=&nbsp;'P_WERKS'.&nbsp;"填入需要读值的字段名<br/>
&nbsp;&nbsp;APPEND&nbsp;dynpfields.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DYNP_VALUES_READ'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dyname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynumb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;translate_to_upper&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfields&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;dynpfields<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;9.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;dynpfields&nbsp;WITH&nbsp;KEY&nbsp;fieldname&nbsp;=&nbsp;'P_WERKS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_werks&nbsp;=&nbsp;dynpfields-fieldvalue.&nbsp;"备注<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>