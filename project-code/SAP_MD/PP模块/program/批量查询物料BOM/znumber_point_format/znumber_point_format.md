<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZNUMBER_POINT_FORMAT</h2>
<h3> Description: 金额、数量字段小数点位数保留格式化</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION znumber_point_format.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_DATA)<br/>
*"     VALUE(I_NUM) TYPE  I OPTIONAL<br/>
*"     VALUE(I_FLAG_INTEGER) TYPE  C OPTIONAL<br/>
*"  EXPORTING<br/>
*"     VALUE(I_OUTPUT)<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-znumber_point_format.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:i_length&nbsp;&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_index&nbsp;&nbsp;&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_count_f&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_count_l&nbsp;TYPE&nbsp;i.<br/>
<br/>
&nbsp;&nbsp;DATA:i_input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_str_front&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_str_last&nbsp;&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;i_input&nbsp;=&nbsp;i_data.<br/>
&nbsp;&nbsp;CONDENSE&nbsp;i_input&nbsp;NO-GAPS&nbsp;.<br/>
</div>
<div class="codeComment">
* 分别获取小数点前后字符长度<br/>
</div>
<div class="code">
&nbsp;&nbsp;SPLIT&nbsp;i_input&nbsp;AT&nbsp;'.'&nbsp;INTO&nbsp;i_str_front&nbsp;i_str_last.<br/>
&nbsp;&nbsp;i_count_f&nbsp;=&nbsp;strlen(&nbsp;i_str_front&nbsp;).<br/>
&nbsp;&nbsp;i_count_l&nbsp;=&nbsp;strlen(&nbsp;i_str_last&nbsp;).<br/>
</div>
<div class="codeComment">
*  获取数据字符总长度<br/>
</div>
<div class="code">
&nbsp;&nbsp;i_length&nbsp;=&nbsp;strlen(&nbsp;i_input&nbsp;).<br/>
&nbsp;&nbsp;i_index&nbsp;=&nbsp;i_count_l&nbsp;-&nbsp;i_num.<br/>
</div>
<div class="codeComment">
* 补长零<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;i_index&nbsp;&lt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_index&nbsp;=&nbsp;i_num&nbsp;-&nbsp;i_count_l.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;i_input.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIND&nbsp;'.'&nbsp;IN&nbsp;I_DATA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;i_output&nbsp;'.'&nbsp;INTO&nbsp;i_output.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;i_index&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;i_output&nbsp;'0'&nbsp;INTO&nbsp;i_output.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_index&nbsp;=&nbsp;i_length&nbsp;-&nbsp;i_index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;i_input+0(i_index).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_index&nbsp;&gt;&nbsp;i_length.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_index&nbsp;=&nbsp;i_index&nbsp;-&nbsp;i_length.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;i_index&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;i_output&nbsp;'0'&nbsp;INTO&nbsp;i_output.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 取整<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;i_flag_integer&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;i_str_front&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>