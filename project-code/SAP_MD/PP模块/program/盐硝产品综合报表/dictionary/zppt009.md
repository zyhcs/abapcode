<table class="outerTable">
<tr>
<td><h2>Table: ZPPT009</h2>
<h3>Description: 锅炉运行情况明细表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZPDATE</td>
<td>3</td>
<td>X</td>
<td>ZEPDATE</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>生产日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZBANCI</td>
<td>4</td>
<td>X</td>
<td>ZEBANCI</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>班次</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZGTEXT</td>
<td>5</td>
<td>X</td>
<td>ZE_ZGTEXT</td>
<td>ZDM_ZGTEXT</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>锅炉名称</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZCQL</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZE_ZCQL</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>产汽量</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZHYM</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZE_ZHYM</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>耗原煤</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZYXSJ</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZE_ZYXSJ</td>
<td>TIME</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>运行时间</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZTCYY</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZE_ZTCYY</td>
<td>ZDM_ZTCYY</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>停产原因</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZTCSJ</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZE_ZTCSJ</td>
<td>TIMS</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>停产时间</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZPWL</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZE_ZPWL</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>排污率</td>
</tr>
<tr class="cell">
<td>12</td>
<td>LIFNR</td>
<td>12</td>
<td>&nbsp;</td>
<td>LIFNR</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商或债权人的帐号</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZBZ</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZE_ZBZ</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>备注</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>