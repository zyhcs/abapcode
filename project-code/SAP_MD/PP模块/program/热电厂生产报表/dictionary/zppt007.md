<table class="outerTable">
<tr>
<td><h2>Table: ZPPT007</h2>
<h3>Description: 热电厂投入产出抬头信息表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZPDATE</td>
<td>3</td>
<td>X</td>
<td>ZEPDATE</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>生产日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZBANCI</td>
<td>4</td>
<td>X</td>
<td>ZEBANCI</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>班次</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZZHIBIE</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZEZHIBIE</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>值别</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZWSD</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZE_ZWSD</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>外送电</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZWGD</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZE_ZWGD</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>外购电</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZWSQ</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZE_ZWSQ</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>外送汽</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZWGQ</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZE_ZWGQ</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>外购汽</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZVERAN</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZE_ZVERAN</td>
<td>ZDM_ZVERAN</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>负责人</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZLSA</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZE_ZLSA</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>硫酸铵</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZHSL</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZE_ZHSL</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>回水量</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZYQTLYA</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZE_ZYQTLYA</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>烟气脱硫用氨</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZYYSL</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZE_ZYYSL</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>用盐酸量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZYSJL</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZE_ZYSJL</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>用烧碱量</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZSCQK</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZE_ZSCQK</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>生产情况</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZCJRQ</td>
<td>17</td>
<td>&nbsp;</td>
<td>ZE_ZCJRQ</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZCJSJ</td>
<td>18</td>
<td>&nbsp;</td>
<td>ZE_ZCJSJ</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>创建时间</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZXGRQ</td>
<td>19</td>
<td>&nbsp;</td>
<td>ZE_ZXGRQ</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>修改日期</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZXGSJ</td>
<td>20</td>
<td>&nbsp;</td>
<td>ZE_ZXGSJ</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>修改时间</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZCJZ</td>
<td>21</td>
<td>&nbsp;</td>
<td>ZE_ZCJZ</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>创建者</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZXGZ</td>
<td>22</td>
<td>&nbsp;</td>
<td>ZE_ZXGZ</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>修改者</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>