<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP023_DATA</h2>
<h3> Description: Include ZPP023_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP023_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES:aufk.<br/>
DATA: wt_fieldcat   TYPE lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
TYPE-POOLS:slis,icon.<br/>
DATA:BEGIN OF gs_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;TYPE&nbsp;auFK-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzt&nbsp;&nbsp;&nbsp;TYPE&nbsp;char4,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zmsg&nbsp;&nbsp;TYPE&nbsp;char200,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;TYPE&nbsp;sel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gs_alv.<br/>
<br/>
DATA:gt_alv LIKE TABLE OF gs_alv.<br/>
DATA: gt_bdcdata TYPE STANDARD TABLE OF bdcdata,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_bdcdata&nbsp;TYPE&nbsp;bdcdata,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_option&nbsp;&nbsp;TYPE&nbsp;ctu_params,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bdcmsgcoll,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bdcmsgcoll.<br/>
DATA: lv_msg TYPE char200.<br/>
<br/>
gs_option-dismode = 'N'.        "N 代表后台一步执行，A代表按照录屏步骤一步步执行，A一般用于调试<br/>
gs_option-updmode = 'S'.<br/>
gs_option-defsize = 'X'.          "代表分辨率<br/>
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;SELECT-OPTIONS:s_AUFNR&nbsp;FOR&nbsp;aufk-aufnr.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:p_IDAT2&nbsp;LIKE&nbsp;aufk-idat2&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_r1&nbsp;&nbsp;&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1&nbsp;USER-COMMAND&nbsp;ucomm01&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_r2&nbsp;&nbsp;&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1.<br/>
<br/>
<br/>
SELECTION-SCREEN END OF BLOCK blk1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>