<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP011_F01</h2>
<h3> Description: Include ZINCL_ZPP011_F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP011_F01<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
FORM form_zbanci .<br/>
&nbsp;&nbsp;DATA:&nbsp;wa_dynpfields&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;dynpread,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfields&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;dynpread,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zppt001&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;zppt001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lw_zppt001&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zppt001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_condition_sql&nbsp;LIKE&nbsp;ty_condition_sql,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_condition_sql&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ty_condition_sql.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;lw_zppt001,ls_condition_sql.<br/>
&nbsp;&nbsp;REFRESH:&nbsp;lt_zppt001,lt_condition_sql.<br/>
<br/>
&nbsp;&nbsp;wa_dynpfields-fieldname&nbsp;=&nbsp;'P_WERKS'.<br/>
&nbsp;&nbsp;APPEND&nbsp;wa_dynpfields&nbsp;TO&nbsp;dynpfields.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;通过该函数,获取PAI事件之前的屏幕字段的值<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DYNP_VALUES_READ'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dyname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"&nbsp;当前程序<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynumb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;"&nbsp;当前屏幕<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfields&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;dynpfields&nbsp;"&nbsp;获取指定屏幕字段的值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_abapworkarea&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_dynprofield&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_dynproname&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_dynpronummer&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_request&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_fielddescription&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_parameter&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;undefind_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;double_conversion&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;9<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stepl_not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;10<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;11.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;取出指定屏幕字段的值<br/>
</div>
<div class="code">
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;dynpfields&nbsp;INTO&nbsp;wa_dynpfields&nbsp;WITH&nbsp;KEY&nbsp;fieldname&nbsp;=&nbsp;'P_WERKS'.<br/>
&nbsp;&nbsp;p_werks&nbsp;=&nbsp;wa_dynpfields-fieldvalue&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;动态SQL<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_condition_sql-condition&nbsp;=&nbsp;'&nbsp;WERKS&nbsp;=&nbsp;P_WERKS&nbsp;'.<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_condition_sql&nbsp;TO&nbsp;lt_condition_sql.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;FROM&nbsp;zppt001&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_zppt001<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;(lt_condition_sql).<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'ZBANCI'&nbsp;&nbsp;&nbsp;&nbsp;"选择后显示的字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;&nbsp;"程序名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"屏幕号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'P_ZBANCI'&nbsp;&nbsp;"屏幕上需要检索help的控件名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'班次检索'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_zppt001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;parameter_error&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_values_found&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;MESSAGE&nbsp;ID&nbsp;SY-MSGID&nbsp;TYPE&nbsp;SY-MSGTY&nbsp;NUMBER&nbsp;SY-MSGNO<br/>
<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;SY-MSGV1&nbsp;SY-MSGV2&nbsp;SY-MSGV3&nbsp;SY-MSGV4.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_get_data.<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SINGLE&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;gw_zppt022<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zppt022<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;p_werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;zpdate&nbsp;=&nbsp;p_zpdate<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;zbanci&nbsp;=&nbsp;p_zbanci<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
ENDFORM.<br/>
FORM frm_authority_check.<br/>
&nbsp;&nbsp;"验证消息"<br/>
&nbsp;&nbsp;DATA:lv_msg&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA:lv_actvt(2)&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;"选择屏幕条件，权限控制"<br/>
&nbsp;&nbsp;IF&nbsp;p_rb1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_actvt&nbsp;=&nbsp;'01'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_rb2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_actvt&nbsp;=&nbsp;'02'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_rb3&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_actvt&nbsp;=&nbsp;'03'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;AUTHORITY-CHECK&nbsp;OBJECT&nbsp;'Z_WERKS'<br/>
&nbsp;&nbsp;&nbsp;ID&nbsp;'WERKS'&nbsp;FIELD&nbsp;p_werks<br/>
&nbsp;&nbsp;&nbsp;ID&nbsp;'ACTVT'&nbsp;FIELD&nbsp;lv_actvt.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'您没有工厂'&nbsp;p_werks&nbsp;&nbsp;'的权限!'&nbsp;INTO&nbsp;lv_msg&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;lv_msg&nbsp;TYPE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>