<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPP011_POV</h2>
<h3> Description: Include ZINCL_ZPP011_POV</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPP011_POV<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;FRM_CHOOSE_TIME1&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE frm_choose_time1 INPUT.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'HELP_VALUES_GET'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fieldname&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'LASTSTRTTM'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INPUT_VALUE&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tabname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'BTCH1010'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SHOW_ALL_VALUES_AT_FIRST_TIME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;select_value&nbsp;=&nbsp;gw_zppt022-zyxsj<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT_INDEX&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_HELP_FOR_FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;MESSAGE&nbsp;ID&nbsp;SY-MSGID&nbsp;TYPE&nbsp;SY-MSGTY&nbsp;NUMBER&nbsp;SY-MSGNO<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;SY-MSGV1&nbsp;SY-MSGV2&nbsp;SY-MSGV3&nbsp;SY-MSGV4.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>