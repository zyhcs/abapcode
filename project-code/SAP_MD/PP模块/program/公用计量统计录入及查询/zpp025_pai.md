<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP025_PAI</h2>
<h3> Description: Include ZPP025_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP025_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
MODULE user_command_0100 INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;ok_code.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F03'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F15'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F12'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SAVE_DATA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_save_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;DEL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_delete_data.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;CLEAR&nbsp;ok_code.<br/>
ENDMODULE.<br/>
<br/>
MODULE tc_itab1_modify INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;tc_itab1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;wa_tc_itab1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;tc_itab1_0100-current_line.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>