<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP025_SCREEN</h2>
<h3> Description: Include ZPP025_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP025_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP024_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES:matdoc,zppt026.<br/>
SELECTION-SCREEN BEGIN OF BLOCK block1 WITH FRAME TITLE TEXT-002.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_werks&nbsp;&nbsp;TYPE&nbsp;matdoc-werks&nbsp;MEMORY&nbsp;ID&nbsp;wrk,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"生产工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_zyear&nbsp;&nbsp;TYPE&nbsp;zppt026-zscnd&nbsp;DEFAULT&nbsp;sy-datum+0(4),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"生产年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_zmonth&nbsp;TYPE&nbsp;zppt026-zscyd&nbsp;MODIF&nbsp;ID&nbsp;r1&nbsp;DEFAULT&nbsp;sy-datum+4(2).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"生产月度<br/>
&nbsp;&nbsp;SELECT-OPTIONS&nbsp;s_zmonth&nbsp;FOR&nbsp;zppt026-ZSCYD&nbsp;MODIF&nbsp;ID&nbsp;r2.&nbsp;&nbsp;&nbsp;&nbsp;"生产月度<br/>
SELECTION-SCREEN END OF BLOCK block1.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK block2 WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;r_radio1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1&nbsp;USER-COMMAND&nbsp;mm&nbsp;MODIF&nbsp;ID&nbsp;rad&nbsp;DEFAULT&nbsp;'X',&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"发出商品结转物料帐差异<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_radio2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1&nbsp;MODIF&nbsp;ID&nbsp;rad,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_radio3&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1&nbsp;MODIF&nbsp;ID&nbsp;rad.<br/>
SELECTION-SCREEN END OF BLOCK block2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>