<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP025_PBO</h2>
<h3> Description: Include ZPP025_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPP025_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_0100 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI_STATUS'.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'GUI_TITLE'.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_SXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;UPDATE&nbsp;LINES&nbsp;FOR&nbsp;EQUIVALENT&nbsp;SCROLLBAR<br/>
</div>
<div class="code">
MODULE tc_gyjltj_change_tc_attr OUTPUT.<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;gt_zppt026&nbsp;LINES&nbsp;tc_itab1_0100-lines.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>