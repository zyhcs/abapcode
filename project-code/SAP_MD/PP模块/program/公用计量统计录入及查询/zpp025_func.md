<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP025_FUNC</h2>
<h3> Description: Include ZPP025_FUNC</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
***INCLUDE&nbsp;ZPP025_FUNC.<br/>
*----------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_query_ctrl<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_query_ctrl .<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'R1'&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---查询显示隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;r_radio1&nbsp;=&nbsp;'X'&nbsp;OR&nbsp;r_radio2&nbsp;=&nbsp;'X'..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'R2'&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---查询显示隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;r_radio3&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;frm_add_fcat<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;VALUE1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;VALUE2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;VALUE3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;VALUE4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_add_fcat USING value1 value2 value3 value4.<br/>
&nbsp;&nbsp;wa_fcat-fieldname&nbsp;=&nbsp;value1.<br/>
&nbsp;&nbsp;wa_fcat-inttype&nbsp;=&nbsp;value2.<br/>
&nbsp;&nbsp;wa_fcat-reptext&nbsp;=&nbsp;value3.<br/>
&nbsp;&nbsp;wa_fcat-intlen&nbsp;&nbsp;&nbsp;=&nbsp;value4.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;wa_fcat-decimals&nbsp;=&nbsp;value5.<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;wa_fcat&nbsp;TO&nbsp;it_fcat.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;wa_fcat.<br/>
ENDFORM.                    "frm_add_fcat<br/>
<br/>
FORM frm_add_fcat1 USING value1 value2 value3 value4.<br/>
&nbsp;&nbsp;wa_fcat-fieldname&nbsp;=&nbsp;value1.<br/>
&nbsp;&nbsp;wa_fcat-inttype&nbsp;=&nbsp;value2.<br/>
&nbsp;&nbsp;wa_fcat-reptext&nbsp;=&nbsp;value3.<br/>
&nbsp;&nbsp;wa_fcat-decimals&nbsp;&nbsp;&nbsp;=&nbsp;value4.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;wa_fcat-decimals&nbsp;=&nbsp;value5.<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;wa_fcat&nbsp;TO&nbsp;it_fcat.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;wa_fcat.<br/>
ENDFORM.                    "frm_add_fcat<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;SET_PF_STATUS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;定义工具栏按钮状态<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM set_pf_status USING pt_exclude TYPE kkblo_t_extab .<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STANDARD'&nbsp;.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>