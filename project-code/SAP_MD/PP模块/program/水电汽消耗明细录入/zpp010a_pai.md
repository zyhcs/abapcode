<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010A_PAI</h2>
<h3> Description: Include ZPPD054A_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD054A_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;EXIT_PROGGAM&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE EXIT_PROGGAM INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;EXIT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_0100&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE USER_COMMAND_0100 INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;BACK'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;EXIT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;CANCEL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SAVE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_SAVE_DATA.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_SXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_SXHMXB_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT014<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT014<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_SXHMXB-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_SXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_SXHMXB_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_SXHMXB_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT014.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_SXHMXB-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GS_ZPPT014-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT014&nbsp;into&nbsp;g_TC_SXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_SXHMXB_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT014<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_SXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT014<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT014<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_SXHMXB-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_DXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_DXHMXB_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT015<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT015<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_DXHMXB-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_DXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_DXHMXB_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_DXHMXB_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT015.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_DXHMXB-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GS_ZPPT015-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT015&nbsp;into&nbsp;g_TC_DXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_DXHMXB_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT015<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_DXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT015<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT015<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_DXHMXB-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_ZQXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_ZQXHMXB_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT016<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT016<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_ZQXHMXB-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_ZQXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_ZQXHMXB_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_ZQXHMXB_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT016.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_ZQXHMXB-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GS_ZPPT016-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT016&nbsp;into&nbsp;g_TC_ZQXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_ZQXHMXB_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT016<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_ZQXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT016<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT016<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_ZQXHMXB-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_TSYXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_TSYXHMXB_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT017<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT017<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_TSYXHMXB-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_TSYXHMXB'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_TSYXHMXB_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_TSYXHMXB_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT017.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_TSYXHMXB-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GS_ZPPT017-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT017&nbsp;into&nbsp;g_TC_TSYXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_TSYXHMXB_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT017<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_TSYXHMXB_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT017<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GS_ZPPT017<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_TSYXHMXB-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>