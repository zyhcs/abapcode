<table class="outerTable">
<tr>
<td><h2>Table: ZPPT010</h2>
<h3>Description: 淡水用水单位基础表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZYSDW</td>
<td>3</td>
<td>X</td>
<td>ZE_ZYSDW</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>用水单位</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZYONGTU</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZYONGTU</td>
<td>ZDM_ZYONGTU</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>用途</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZINDEX</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZINDEX</td>
<td>ZINDEX</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>01</td>
<td>&nbsp;</td>
<td>制水</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>02</td>
<td>&nbsp;</td>
<td>热电</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>03</td>
<td>&nbsp;</td>
<td>采卤(及净化)</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>04</td>
<td>&nbsp;</td>
<td>卤水净化</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>05</td>
<td>&nbsp;</td>
<td>大颗粒卤水净化</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>06</td>
<td>&nbsp;</td>
<td>制盐</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>07</td>
<td>&nbsp;</td>
<td>(大)包装</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>08</td>
<td>&nbsp;</td>
<td>小包装</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>09</td>
<td>&nbsp;</td>
<td>烟气脱硫</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>10</td>
<td>&nbsp;</td>
<td>外供</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>11</td>
<td>&nbsp;</td>
<td>生活办公</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>12</td>
<td>&nbsp;</td>
<td>氯碱</td>
</tr>
<tr class="cell">
<td>ZDM_ZYONGTU</td>
<td>13</td>
<td>&nbsp;</td>
<td>双氧水</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>