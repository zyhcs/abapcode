<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010A_SCREEN</h2>
<h3> Description: Include ZPPD054A_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD054A_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_WERKS&nbsp;&nbsp;LIKE&nbsp;ZPPT001-WERKS&nbsp;MEMORY&nbsp;ID&nbsp;WRK,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZPDATE&nbsp;LIKE&nbsp;ZPPT007-ZPDATE&nbsp;MODIF&nbsp;ID&nbsp;m1.<br/>
&nbsp;&nbsp;&nbsp;SELECT-OPTIONS&nbsp;S_ZPDATE&nbsp;FOR&nbsp;ZPPT007-ZPDATE&nbsp;MODIF&nbsp;ID&nbsp;m2.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PARAMETERS:&nbsp;P_ZBANCI&nbsp;LIKE&nbsp;TQ01B-QMATAUTH&nbsp;VISIBLE&nbsp;LENGTH&nbsp;6&nbsp;OBLIGATORY.<br/>
</div>
<div class="code">
&nbsp;&nbsp;PARAMETERS:&nbsp;P_ZBANCI&nbsp;LIKE&nbsp;TQ01B-QMATAUTH&nbsp;VISIBLE&nbsp;LENGTH&nbsp;6.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_ZHIBIE&nbsp;LIKE&nbsp;ZPPT007-ZZHIBIE&nbsp;VISIBLE&nbsp;LENGTH&nbsp;6&nbsp;MODIF&nbsp;ID&nbsp;m2.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
SELECTION-SCREEN BEGIN OF BLOCK BLK2 WITH FRAME TITLE TEXT-T02.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;LINE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;12.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;cr1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1&nbsp;DEFAULT&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;14(10)&nbsp;textcr1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;25.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;cr2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;27(10)&nbsp;textcr2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;38.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;cr3&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;40(10)&nbsp;textcr3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;51.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;cr4&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;54(10)&nbsp;textcr4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;65.<br/>
&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;LINE.<br/>
SELECTION-SCREEN END OF BLOCK BLK2.<br/>
SELECTION-SCREEN BEGIN OF BLOCK BLK3 WITH FRAME  TITLE TEXT-T03.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_RB1&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G&nbsp;&nbsp;USER-COMMAND&nbsp;UC1&nbsp;DEFAULT&nbsp;'X',&nbsp;&nbsp;"新建<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB2&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"修改<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB3&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"查询<br/>
SELECTION-SCREEN END OF BLOCK BLK3.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>