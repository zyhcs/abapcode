<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010_SCREEN</h2>
<h3> Description: Include ZPPD001_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD001_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_WERKS&nbsp;&nbsp;LIKE&nbsp;ZPPT001-WERKS&nbsp;MEMORY&nbsp;ID&nbsp;WRK&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZPDATE&nbsp;LIKE&nbsp;ZPPT007-ZPDATE&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZBANCI&nbsp;LIKE&nbsp;TQ01B-QMATAUTH&nbsp;VISIBLE&nbsp;LENGTH&nbsp;6&nbsp;OBLIGATORY.<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
SELECTION-SCREEN BEGIN OF BLOCK BLK2 WITH FRAME .<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_RB1&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G&nbsp;DEFAULT&nbsp;'X',&nbsp;&nbsp;"新建<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB2&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"修改<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_RB3&nbsp;TYPE&nbsp;C&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R_G&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"查询<br/>
SELECTION-SCREEN END OF BLOCK BLK2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>