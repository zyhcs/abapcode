<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010_TOP</h2>
<h3> Description: Include ZPPD001_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD001_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES:ZPPT007.<br/>
TYPE-POOLS:SILS,ICON.<br/>
DATA:BEGIN OF TY_ZPPT009.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include&nbsp;structure&nbsp;<a&nbsp;href&nbsp;="zppt009 dictionary-zppt009.html"="">ZPPT009.<br/>
DATA:  SELT  TYPE C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME1&nbsp;LIKE&nbsp;LFA1-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_ZPPT009.<br/>
<br/>
DATA:BEGIN OF TY_ZPPT008.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include&nbsp;structure&nbsp;<a&nbsp;href&nbsp;="zppt008 dictionary-zppt008.html"="">ZPPT008.<br/>
DATA:  SELT TYPE C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_ZPPT008.<br/>
<br/>
&nbsp;&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;TY_LFA1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIFNR&nbsp;LIKE&nbsp;LFA1-LIFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME1&nbsp;LIKE&nbsp;LFA1-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_LFA1.<br/>
<br/>
DATA:BEGIN OF TY_CONDITION_SQL OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDITION(80),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_CONDITION_SQL.<br/>
<br/>
DATA:LBL_NAME1 TYPE NAME1.<br/>
DATA:GW_ZPPT007 TYPE ZPPT007,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_ZPPT008&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;TY_ZPPT008,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZPPT008&nbsp;LIKE&nbsp;TY_ZPPT008,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_ZPPT009&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;TY_ZPPT009,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZPPT009&nbsp;LIKE&nbsp;TY_ZPPT009.<br/>
<br/>
</a&nbsp;href&nbsp;="zppt008></a&nbsp;href&nbsp;="zppt009></div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;FUNCTION&nbsp;CODES&nbsp;FOR&nbsp;TABSTRIP&nbsp;'TAB_100'<br/>
</div>
<div class="code">
CONSTANTS: BEGIN OF C_TAB_100,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAB1&nbsp;LIKE&nbsp;SY-UCOMM&nbsp;VALUE&nbsp;'TAB_100_FC1',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAB2&nbsp;LIKE&nbsp;SY-UCOMM&nbsp;VALUE&nbsp;'TAB_100_FC2',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAB3&nbsp;LIKE&nbsp;SY-UCOMM&nbsp;VALUE&nbsp;'TAB_100_FC3',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;C_TAB_100.<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;DATA&nbsp;FOR&nbsp;TABSTRIP&nbsp;'TAB_100'<br/>
</div>
<div class="code">
CONTROLS:  TAB_100 TYPE TABSTRIP.<br/>
DATA:      BEGIN OF G_TAB_100,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUBSCREEN&nbsp;&nbsp;&nbsp;LIKE&nbsp;SY-DYNNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;SY-REPID&nbsp;VALUE&nbsp;'ZPP010',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRESSED_TAB&nbsp;LIKE&nbsp;SY-UCOMM&nbsp;VALUE&nbsp;C_TAB_100-TAB1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;G_TAB_100.<br/>
DATA:      OK_CODE LIKE SY-UCOMM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_FDJYXQK'&nbsp;ITSELF<br/>
*CONTROLS:&nbsp;TC_FDJYXQK&nbsp;TYPE&nbsp;TABLEVIEW&nbsp;USING&nbsp;SCREEN&nbsp;0101.<br/>
<br/>
*&amp;SPWIZARD:&nbsp;LINES&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_FDJYXQK'<br/>
*DATA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TC_FDJYXQK_LINES&nbsp;&nbsp;LIKE&nbsp;SY-LOOPC.<br/>
<br/>
*&amp;SP*WIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_GLYXQK'&nbsp;ITSELF<br/>
*CONTROLS:&nbsp;TC_GLYXQK&nbsp;TYPE&nbsp;TABLEVIEW&nbsp;USING&nbsp;SCREEN&nbsp;0102.<br/>
<br/>
*&amp;SPWIZARD:&nbsp;LINES&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_GLYXQK'<br/>
*DATA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TC_GLYXQK_LINES&nbsp;&nbsp;LIKE&nbsp;SY-LOOPC.<br/>
<br/>
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_FDJYXQK'&nbsp;ITSELF<br/>
</div>
<div class="code">
CONTROLS: TC_FDJYXQK TYPE TABLEVIEW USING SCREEN 0101.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC_GLYXQK'&nbsp;ITSELF<br/>
</div>
<div class="code">
CONTROLS: TC_GLYXQK TYPE TABLEVIEW USING SCREEN 0102.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>