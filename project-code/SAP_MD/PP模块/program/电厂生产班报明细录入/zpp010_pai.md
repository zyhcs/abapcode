<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010_PAI</h2>
<h3> Description: Include ZPPD001_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD001_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TS&nbsp;'TAB_100'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;GETS&nbsp;ACTIVE&nbsp;TAB<br/>
</div>
<div class="code">
MODULE TAB_100_ACTIVE_TAB_GET INPUT.<br/>
&nbsp;&nbsp;OK_CODE&nbsp;=&nbsp;SY-UCOMM.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-PRESSED_TAB&nbsp;=&nbsp;C_TAB_100-TAB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-PRESSED_TAB&nbsp;=&nbsp;C_TAB_100-TAB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-PRESSED_TAB&nbsp;=&nbsp;C_TAB_100-TAB3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;NOTHING<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
*MODULE&nbsp;TC_FDJYXQK_MODIFY&nbsp;INPUT.<br/>
*&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT008<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT008<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_FDJYXQK-CURRENT_LINE.<br/>
*ENDMODULE.<br/>
<br/>
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
*MODULE&nbsp;TC_FDJYXQK_MARK&nbsp;INPUT.<br/>
*&nbsp;&nbsp;DATA:&nbsp;G_TC_FDJYXQK_WA2&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;GT_ZPPT008.<br/>
**&nbsp;&nbsp;IF&nbsp;TC_FDJYXQK-LINE_SEL_MODE&nbsp;=&nbsp;1<br/>
**&nbsp;AND&nbsp;GW_ZPPT008-SELT&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_ZPPT008&nbsp;INTO&nbsp;G_TC_FDJYXQK_WA2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TC_FDJYXQK_WA2-SELT&nbsp;=&nbsp;''.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT008<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;G_TC_FDJYXQK_WA2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
**&nbsp;&nbsp;ENDIF.<br/>
**&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT008<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT008<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_FDJYXQK-CURRENT_LINE<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
*ENDMODULE.<br/>
*<br/>
**&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
**&amp;SPWIZARD:&nbsp;PROCESS&nbsp;USER&nbsp;COMMAND<br/>
</div>
<div class="code">
MODULE TC_FDJYXQK_USER_COMMAND INPUT.<br/>
&nbsp;&nbsp;OK_CODE&nbsp;=&nbsp;SY-UCOMM.<br/>
&nbsp;&nbsp;PERFORM&nbsp;USER_OK_TC&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;'TC_FDJYXQK'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GT_ZPPT008'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'SELT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;SY-UCOMM&nbsp;=&nbsp;OK_CODE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_GLYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;PROCESS&nbsp;USER&nbsp;COMMAND<br/>
</div>
<div class="code">
MODULE TC_GLYXQK_USER_COMMAND INPUT.<br/>
&nbsp;&nbsp;OK_CODE&nbsp;=&nbsp;SY-UCOMM.<br/>
&nbsp;&nbsp;PERFORM&nbsp;USER_OK_TC&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;'TC_GLYXQK'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GT_ZPPT009'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'SELT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;SY-UCOMM&nbsp;=&nbsp;OK_CODE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_0100&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE USER_COMMAND_0100 INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;BACK'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;EXIT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;CANCEL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SAVE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_SAVE_DATA.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_SAVE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_SAVE_DATA .<br/>
&nbsp;&nbsp;DATA:&nbsp;LT_007&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT007&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_008&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT008&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_009&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT009&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;if&nbsp;p_rb1&nbsp;eq&nbsp;'X'.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZCJRQ&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZCJSJ&nbsp;=&nbsp;SY-UZEIT.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZCJZ&nbsp;=&nbsp;&nbsp;SY-UNAME.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_rb2&nbsp;eq&nbsp;'X'.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZXGRQ&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZXGSJ&nbsp;=&nbsp;SY-UZEIT.<br/>
&nbsp;&nbsp;GW_ZPPT007-ZXGZ&nbsp;=&nbsp;SY-UNAME.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_ZPPT008&nbsp;INTO&nbsp;GW_ZPPT008&nbsp;WHERE&nbsp;ZFTEXT&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;GW_ZPPT008&nbsp;TO&nbsp;LT_008&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_008-WERKS&nbsp;=&nbsp;GW_ZPPT007-WERKS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_008-ZPDATE&nbsp;=&nbsp;GW_ZPPT007-ZPDATE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_008-ZBANCI&nbsp;=&nbsp;GW_ZPPT007-ZBANCI&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;LT_008&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;LT_008&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;GT_ZPPT009&nbsp;INTO&nbsp;GW_ZPPT009&nbsp;WHERE&nbsp;ZGTEXT&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;GW_ZPPT009&nbsp;TO&nbsp;LT_009&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GW_ZPPT009-LIFNR<br/>
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_009-LIFNR.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_009-WERKS&nbsp;&nbsp;=&nbsp;GW_ZPPT007-WERKS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_009-ZPDATE&nbsp;=&nbsp;GW_ZPPT007-ZPDATE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_009-ZBANCI&nbsp;=&nbsp;GW_ZPPT007-ZBANCI&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;LT_009&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;LT_009&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;MODIFY&nbsp;ZPPT007&nbsp;FROM&nbsp;GW_ZPPT007&nbsp;.<br/>
&nbsp;&nbsp;MODIFY&nbsp;ZPPT008&nbsp;FROM&nbsp;TABLE&nbsp;LT_008&nbsp;.<br/>
&nbsp;&nbsp;MODIFY&nbsp;ZPPT009&nbsp;FROM&nbsp;TABLE&nbsp;LT_009&nbsp;.<br/>
<br/>
&nbsp;&nbsp;COMMIT&nbsp;&nbsp;WORK&nbsp;AND&nbsp;WAIT&nbsp;.<br/>
&nbsp;&nbsp;MESSAGE&nbsp;'保存成功.'&nbsp;TYPE&nbsp;'S'&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;ZCHECK_LIFNR&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE ZCHECK_LIFNR INPUT.<br/>
DATA:LV_LIFNR TYPE LIFNR.<br/>
CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GW_ZPPT009-LIFNR<br/>
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LV_LIFNR.<br/>
<br/>
SELECT SINGLE NAME1 INTO GW_ZPPT009-NAME1<br/>
&nbsp;&nbsp;FROM&nbsp;LFA1<br/>
&nbsp;&nbsp;WHERE&nbsp;LIFNR&nbsp;=&nbsp;LV_LIFNR<br/>
&nbsp;&nbsp;AND&nbsp;LAND1&nbsp;=&nbsp;'CN'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;EXIT_PROGGAM&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE EXIT_PROGGAM INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;EXIT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_FDJYXQK_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT008<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT008<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_FDJYXQK-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_FDJYXQK_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_FDJYXQK_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT008.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_FDJYXQK-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GW_ZPPT008-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT008&nbsp;into&nbsp;g_TC_FDJYXQK_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_FDJYXQK_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT008<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_FDJYXQK_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT008<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT008<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_FDJYXQK-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_GLYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_GLYXQK_MODIFY INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_GLYXQK-CURRENT_LINE.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TC_GLYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE TC_GLYXQK_MARK INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_TC_GLYXQK_wa2&nbsp;like&nbsp;line&nbsp;of&nbsp;GT_ZPPT009.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;TC_GLYXQK-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;GW_ZPPT009-SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;GT_ZPPT009&nbsp;into&nbsp;g_TC_GLYXQK_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;SELT&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_TC_GLYXQK_wa2-SELT&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;GT_ZPPT009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;g_TC_GLYXQK_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transporting&nbsp;SELT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;MODIFY&nbsp;GT_ZPPT009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;GW_ZPPT009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;TC_GLYXQK-CURRENT_LINE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;SELT.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>