<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZPP010_PBO</h2>
<h3> Description: Include ZPPD001_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZPPD001_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TS&nbsp;'TAB_100'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;SETS&nbsp;ACTIVE&nbsp;TAB<br/>
</div>
<div class="code">
MODULE TAB_100_ACTIVE_TAB_SET OUTPUT.<br/>
&nbsp;&nbsp;TAB_100-ACTIVETAB&nbsp;=&nbsp;G_TAB_100-PRESSED_TAB.<br/>
&nbsp;&nbsp;CASE&nbsp;G_TAB_100-PRESSED_TAB.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-SUBSCREEN&nbsp;=&nbsp;'0101'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-SUBSCREEN&nbsp;=&nbsp;'0102'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;C_TAB_100-TAB3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G_TAB_100-SUBSCREEN&nbsp;=&nbsp;'0103'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;NOTHING<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE STATUS_0100 OUTPUT.<br/>
&nbsp;DATA:&nbsp;LT_extab_line&nbsp;TYPE&nbsp;slis_t_extab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LS_extab_line&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;LT_extab_line.<br/>
&nbsp;&nbsp;if&nbsp;P_RB3&nbsp;eq&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LS_extab_line-fcode&nbsp;=&nbsp;'@SAVE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;LS_extab_line&nbsp;TO&nbsp;LT_extab_line.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI_STATUS_100'&nbsp;EXCLUDING&nbsp;LT_extab_line.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'GUI_TITLE_0100'.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;MODIFY_101_SCREEN&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE MODIFY_101_SCREEN OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;iF&nbsp;SCREEN-NAME(7)&nbsp;NE&nbsp;'TAB_100'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;P_RB3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;MODIFY_103_SCREEN&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE MODIFY_103_SCREEN OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SCREEN-NAME(7)&nbsp;NE&nbsp;'TAB_100'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;P_RB3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_QTSCQK_DATA&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE INIT_QTSCQK_DATA OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;VID_02&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;VRM_ID&nbsp;VALUE&nbsp;'GW_ZPPT007-ZVERAN',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VLIST_02&nbsp;&nbsp;&nbsp;TYPE&nbsp;VRM_VALUES,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE_02&nbsp;&nbsp;&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;VLIST,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_ZPPT005&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT005&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;LT_ZPPT005<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ZPPT005<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS.<br/>
<br/>
&nbsp;&nbsp;REFRESH:VLIST_02.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_ZPPT005.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;VALUE_02.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_ZPPT005-ZVERAN&nbsp;TO&nbsp;VALUE_02-KEY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_ZPPT005-ZVERAN&nbsp;TO&nbsp;VALUE_02-TEXT&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;VALUE_02&nbsp;TO&nbsp;VLIST_02&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VID_02<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VLIST_02<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID_ILLEGAL_NAME&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_ZPPT005&nbsp;WHERE&nbsp;ZDEFAULT&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_ZPPT007-ZVERAN&nbsp;=&nbsp;LT_ZPPT005-ZVERAN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_FDJQK_DATA&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE INIT_FDJQK_DATA OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;VID_03&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;VRM_ID&nbsp;VALUE&nbsp;'GW_ZPPT008-ZTCYY',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VLIST_03&nbsp;&nbsp;&nbsp;TYPE&nbsp;VRM_VALUES,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE_03&nbsp;&nbsp;&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;VLIST,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_ZPPT006&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT006&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;LT_ZPPT006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ZPPT006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS.<br/>
<br/>
&nbsp;&nbsp;REFRESH:VLIST_03.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_ZPPT006.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;VALUE_03.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_ZPPT006-ZTCYY&nbsp;TO&nbsp;VALUE_03-KEY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_ZPPT006-ZTCYY&nbsp;TO&nbsp;VALUE_03-TEXT&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;VALUE_03&nbsp;TO&nbsp;VLIST_03.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VID_03<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VLIST_03<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID_ILLEGAL_NAME&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_GLDATA&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE INIT_GLDATA OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;VID_04&nbsp;&nbsp;&nbsp;TYPE&nbsp;VRM_ID&nbsp;VALUE&nbsp;'GW_ZPPT009-ZTCYY',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VLIST_04&nbsp;TYPE&nbsp;VRM_VALUES,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE_04&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;VLIST,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_T006&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;ZPPT006&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;LT_T006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ZPPT006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS.<br/>
<br/>
&nbsp;&nbsp;REFRESH:VLIST_04.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;LT_T006.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;VALUE_04.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_T006-ZTCYY&nbsp;TO&nbsp;VALUE_04-KEY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;LT_T006-ZTCYY&nbsp;TO&nbsp;VALUE_04-TEXT&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;VALUE_04&nbsp;TO&nbsp;VLIST_04.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VID_04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;VLIST_04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID_ILLEGAL_NAME&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_FDJYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;UPDATE&nbsp;LINES&nbsp;FOR&nbsp;EQUIVALENT&nbsp;SCROLLBAR<br/>
</div>
<div class="code">
MODULE TC_FDJYXQK_CHANGE_TC_ATTR OUTPUT.<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;GT_ZPPT008&nbsp;LINES&nbsp;TC_FDJYXQK-lines.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TC_GLYXQK'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;UPDATE&nbsp;LINES&nbsp;FOR&nbsp;EQUIVALENT&nbsp;SCROLLBAR<br/>
</div>
<div class="code">
MODULE TC_GLYXQK_CHANGE_TC_ATTR OUTPUT.<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;GT_ZPPT009&nbsp;LINES&nbsp;TC_GLYXQK-lines.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;TC_FDJYXQK_CHANGE_FIELD_ATTR&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE TC_FDJYXQK_CHANGE_FIELD_ATTR OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;P_RB3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;TC_GLYXQK_CHANGE_FIELD_ATTR&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE TC_GLYXQK_CHANGE_FIELD_ATTR OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;P_RB3&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-INPUT&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>