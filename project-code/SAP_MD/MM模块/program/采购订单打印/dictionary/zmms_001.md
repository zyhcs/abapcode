<table class="outerTable">
<tr>
<td><h2>Table: ZMMS_001</h2>
<h3>Description: 采购订单打印参考结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>NAME_V</td>
<td>1</td>
<td>&nbsp;</td>
<td>NAME_1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>公司名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>NAME_L</td>
<td>2</td>
<td>&nbsp;</td>
<td>NAME1_GP</td>
<td>NAME</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>名称 1</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AEDAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>ERDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>记录创建日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BUKRS</td>
<td>4</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BSART</td>
<td>5</td>
<td>&nbsp;</td>
<td>BSART</td>
<td>BSART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>订单类型（采购）</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>KNUMV</td>
<td>6</td>
<td>&nbsp;</td>
<td>KNUMV</td>
<td>KNUMV</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>凭证条件号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>EBELN</td>
<td>7</td>
<td>&nbsp;</td>
<td>EBELN</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购凭证编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>BATXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>BATXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>采购凭证类型的简短描述</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>EBELP</td>
<td>9</td>
<td>&nbsp;</td>
<td>EBELP</td>
<td>EBELP</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>采购凭证的项目编号</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MATNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MAKTX</td>
<td>11</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MENGE</td>
<td>12</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>13</td>
<td>MEINS</td>
<td>13</td>
<td>&nbsp;</td>
<td>BSTME</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>采购订单计量单位</td>
</tr>
<tr class="cell">
<td>14</td>
<td>EINDT</td>
<td>14</td>
<td>&nbsp;</td>
<td>EINDT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>项目交货日期</td>
</tr>
<tr class="cell">
<td>15</td>
<td>KBETR_P</td>
<td>15</td>
<td>&nbsp;</td>
<td>VFPRC_ELEMENT_AMOUNT</td>
<td>VFPRC_DEC_VALUE</td>
<td>DEC</td>
<td>24</td>
<td>&nbsp;</td>
<td>条件金额或百分比</td>
</tr>
<tr class="cell">
<td>16</td>
<td>KBETR</td>
<td>16</td>
<td>&nbsp;</td>
<td>KBETR</td>
<td>WERTV6</td>
<td>CURR</td>
<td>11</td>
<td>&nbsp;</td>
<td>条件金额或百分比</td>
</tr>
<tr class="cell">
<td>17</td>
<td>KWERT</td>
<td>17</td>
<td>&nbsp;</td>
<td>KWERT</td>
<td>WERTV7</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>定价值</td>
</tr>
<tr class="cell">
<td>18</td>
<td>SCHPR</td>
<td>18</td>
<td>&nbsp;</td>
<td>SCHPR</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>标识：估算价格</td>
</tr>
<tr class="cell">
<td>19</td>
<td>AFNAM</td>
<td>19</td>
<td>&nbsp;</td>
<td>AFNAM</td>
<td>AFNAM</td>
<td>CHAR</td>
<td>12</td>
<td>X</td>
<td>申请人姓名</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZKBETR</td>
<td>20</td>
<td>&nbsp;</td>
<td>CHAR12</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>长度 12 的字符字段</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZBZ</td>
<td>21</td>
<td>&nbsp;</td>
<td>CHAR200</td>
<td>CHAR200</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>文本字段长度 200</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZCGY</td>
<td>22</td>
<td>&nbsp;</td>
<td>AD_NAME2_P</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>人员出生时的名字</td>
</tr>
<tr class="cell">
<td>23</td>
<td>TEL_NUMBER</td>
<td>23</td>
<td>&nbsp;</td>
<td>AD_TLNMBR</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>电话号码: 拨区号 + 号码</td>
</tr>
<tr class="cell">
<td>24</td>
<td>MSEH6</td>
<td>24</td>
<td>&nbsp;</td>
<td>MSEHL</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>度量单位文本(最多30个字符）</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZKBETR2</td>
<td>25</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>24</td>
<td>&nbsp;</td>
<td>含税单价</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZKWERT</td>
<td>26</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>13</td>
<td>&nbsp;</td>
<td>价税合计</td>
</tr>
<tr class="cell">
<td>27</td>
<td>TXZ01</td>
<td>27</td>
<td>&nbsp;</td>
<td>TXZ01</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>短文本</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>