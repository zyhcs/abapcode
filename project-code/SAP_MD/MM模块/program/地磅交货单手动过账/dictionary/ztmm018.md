<table class="outerTable">
<tr>
<td><h2>Table: ZTMM018</h2>
<h3>Description: 发货过账接口手工处理记录表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>UUID</td>
<td>2</td>
<td>X</td>
<td>SYSUUID_C32</td>
<td>SYSUUID_C32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 32 Characters (Hexadecimal Encoded)</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>.INCLUDE</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZMMS003</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>0</td>
<td>&nbsp;</td>
<td>交货单过账传入参数</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>VBELN</td>
<td>4</td>
<td>&nbsp;</td>
<td>VBELN_VL</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>交货</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>POSNR</td>
<td>5</td>
<td>&nbsp;</td>
<td>POSNR_VL</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>交货项目</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ERMNG</td>
<td>6</td>
<td>&nbsp;</td>
<td>LFIMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际已交货量（按销售单位）</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MEINS</td>
<td>7</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>LGORT</td>
<td>8</td>
<td>&nbsp;</td>
<td>LGORT_D</td>
<td>LGORT</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>存储地点</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>CHARG</td>
<td>9</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZCPHM</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZESDCPHM</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>车牌号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZJZXH1</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZESDJZXH1</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>集装箱号1</td>
</tr>
<tr class="cell">
<td>12</td>
<td>BRGEW</td>
<td>12</td>
<td>&nbsp;</td>
<td>BRGEW_15</td>
<td>MENG15</td>
<td>QUAN</td>
<td>15</td>
<td>&nbsp;</td>
<td>毛重</td>
</tr>
<tr class="cell">
<td>13</td>
<td>TRGEW</td>
<td>13</td>
<td>&nbsp;</td>
<td>TARAG</td>
<td>MENG15</td>
<td>QUAN</td>
<td>15</td>
<td>&nbsp;</td>
<td>皮重装运单位</td>
</tr>
<tr class="cell">
<td>14</td>
<td>GEWEI</td>
<td>14</td>
<td>&nbsp;</td>
<td>GEWEI</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>重量单位</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZZGBDAT</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>过磅日期</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZGBTIM</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>过磅时间</td>
</tr>
<tr class="cell">
<td>17</td>
<td>MATNR</td>
<td>17</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>18</td>
<td>MAKTX</td>
<td>18</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>19</td>
<td>WERKS</td>
<td>19</td>
<td>&nbsp;</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td>20</td>
<td>SENDER</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>发送系统</td>
</tr>
<tr class="cell">
<td>21</td>
<td>RECEIVER</td>
<td>21</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>接收系统</td>
</tr>
<tr class="cell">
<td>22</td>
<td>BUSTYP</td>
<td>22</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>消息类型</td>
</tr>
<tr class="cell">
<td>23</td>
<td>MESSAGEID</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>消息ID</td>
</tr>
<tr class="cell">
<td>24</td>
<td>SEND_DATE</td>
<td>24</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>发送日期</td>
</tr>
<tr class="cell">
<td>25</td>
<td>SEND_TIME</td>
<td>25</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>发送时间</td>
</tr>
<tr class="cell">
<td>26</td>
<td>SEND_USER</td>
<td>26</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>发送用户</td>
</tr>
<tr class="cell">
<td>27</td>
<td>GZFLAG</td>
<td>27</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>已过账标识</td>
</tr>
<tr class="cell">
<td>28</td>
<td>XCHPF</td>
<td>28</td>
<td>&nbsp;</td>
<td>XCHPF</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>批次管理需求标识</td>
</tr>
<tr class="cell">
<td>29</td>
<td>CHARG_MENGE</td>
<td>29</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>