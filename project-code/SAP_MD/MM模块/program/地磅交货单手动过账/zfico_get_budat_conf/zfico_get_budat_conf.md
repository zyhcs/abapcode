<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFICO_GET_BUDAT_CONF</h2>
<h3> Description: 获取过账日期配置数据</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zfico_get_budat_conf.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(BUKRS) TYPE  BUKRS<br/>
*"     REFERENCE(ZPLDAT) TYPE  BUDAT<br/>
*"  EXPORTING<br/>
*"     REFERENCE(ZBUDAT) TYPE  BUDAT<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zfico_get_budat_conf.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lw_t029&nbsp;TYPE&nbsp;ztfico029.<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ztfico029<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;@bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;zyear&nbsp;=&nbsp;@zpldat+0(4)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;zmonth&nbsp;=&nbsp;@zpldat+4(2)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;@lw_t029.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;zpldat&nbsp;&gt;=&nbsp;lw_t029-zstart&nbsp;AND&nbsp;zpldat&nbsp;&lt;=&nbsp;lw_t029-zend.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbudat&nbsp;=&nbsp;zpldat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbudat&nbsp;=&nbsp;lw_t029-zbudat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;zbudat&nbsp;=&nbsp;zpldat.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>