<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM027_SCREEN</h2>
<h3> Description: Include ZMM027_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM027_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES: bseg,matdoc.<br/>
SELECTION-SCREEN BEGIN OF BLOCK block1 WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;s_belnr&nbsp;FOR&nbsp;bseg-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_gjahr&nbsp;&nbsp;&nbsp;FOR&nbsp;bseg-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_budat&nbsp;FOR&nbsp;bseg-h_budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lifnr&nbsp;&nbsp;&nbsp;FOR&nbsp;bseg-lifnr&nbsp;&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_werks&nbsp;&nbsp;&nbsp;FOR&nbsp;bseg-werks&nbsp;OBLIGATORY&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ebeln&nbsp;&nbsp;&nbsp;FOR&nbsp;bseg-ebeln&nbsp;&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matnr&nbsp;&nbsp;&nbsp;FOR&nbsp;bseg-matnr&nbsp;&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_mblnr&nbsp;&nbsp;&nbsp;FOR&nbsp;matdoc-mblnr&nbsp;&nbsp;.<br/>
SELECTION-SCREEN END OF BLOCK block1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>