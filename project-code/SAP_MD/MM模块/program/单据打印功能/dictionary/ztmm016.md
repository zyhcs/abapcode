<table class="outerTable">
<tr>
<td><h2>Table: ZTMM016</h2>
<h3>Description: 业务类型配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>YWLX</td>
<td>1</td>
<td>X</td>
<td>ZEYWLX</td>
<td>ZDYWLX</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>业务类型</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>TEXT</td>
<td>2</td>
<td>&nbsp;</td>
<td>ZETYWLX</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>29</td>
<td>&nbsp;</td>
<td>业务类型描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>