<table class="outerTable">
<tr>
<td><h2>Table: ZSMM011</h2>
<h3>Description: 单据打印-销售出库单</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUTXT</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MBLNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>MBLNR</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>物料凭证编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZZLTXBH</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>质量体系编号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZZDR</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>制单人</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BUDAT</td>
<td>5</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>NAME1</td>
<td>6</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZSHDZ</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>送货地址</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>BEZEI</td>
<td>8</td>
<td>&nbsp;</td>
<td>BEZEI</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>控制范围名称</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZLXFS</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>联系方式</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZCYWL</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZESDCYWL</td>
<td>CHAR40</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>承运物流</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZCPHM</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZESDCPHM</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>车牌号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZCZGK</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>车站/港口</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZDYCH</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>短运车号</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MAKTX</td>
<td>14</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>15</td>
<td>LGOBE</td>
<td>15</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td>16</td>
<td>LGOBE2</td>
<td>16</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td>17</td>
<td>CHARG</td>
<td>17</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td>18</td>
<td>MENGE1</td>
<td>18</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>19</td>
<td>MENGE2</td>
<td>19</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>20</td>
<td>SGTXT</td>
<td>20</td>
<td>&nbsp;</td>
<td>SGTXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>项目文本</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZZLTXBHWB</td>
<td>21</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>质量体系编号文本</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>