<table class="outerTable">
<tr>
<td><h2>Table: ZSMM010</h2>
<h3>Description: 单据打印-委外发料单</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUTXT</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MBLNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>MBLNR</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>物料凭证编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUDAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MCOD1</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>合作单位</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZZLTXBH</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>质量体系编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZNUM</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MATNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MAKTX</td>
<td>8</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>LGOBE</td>
<td>9</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MEINS</td>
<td>10</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MENGE1</td>
<td>11</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MENGE2</td>
<td>12</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZPRICE</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DEC</td>
<td>13</td>
<td>&nbsp;</td>
<td>单价</td>
</tr>
<tr class="cell">
<td>14</td>
<td>DMBTR</td>
<td>14</td>
<td>&nbsp;</td>
<td>DMBTR_CS</td>
<td>WERT7</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>以本币表示的金额</td>
</tr>
<tr class="cell">
<td>15</td>
<td>CHARG</td>
<td>15</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZLTXBHWB</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>质量体系编号文本</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>