<table class="outerTable">
<tr>
<td><h2>Table: ZSMM003</h2>
<h3>Description: 单据打印-产品入库单</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUTXT</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MBLNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>MBLNR</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>物料凭证编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUDAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZZLTXBH</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>质量体系编号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZBANCI</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>班次</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZZHIBIE</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>值别</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ABLAD</td>
<td>7</td>
<td>&nbsp;</td>
<td>ABLAD</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>卸货点</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>WEMPF</td>
<td>8</td>
<td>&nbsp;</td>
<td>WEMPF</td>
<td>WEMPF</td>
<td>CHAR</td>
<td>12</td>
<td>X</td>
<td>收货方</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZNUM</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
<tr class="cell">
<td>10</td>
<td>AUFNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MATNR</td>
<td>11</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MAKTX</td>
<td>12</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZDHL</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>碘含量</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MENGE1</td>
<td>14</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>MENGE2</td>
<td>15</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>16</td>
<td>KTEXT</td>
<td>16</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td>17</td>
<td>LGOBE</td>
<td>17</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZLEVEL</td>
<td>18</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>等级</td>
</tr>
<tr class="cell">
<td>19</td>
<td>SGTXT</td>
<td>19</td>
<td>&nbsp;</td>
<td>SGTXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>项目文本</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZZLTXBHWB</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>质量体系编号文本</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>