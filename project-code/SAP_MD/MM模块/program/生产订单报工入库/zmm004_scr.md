<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM004_SCR</h2>
<h3> Description: Include ZMM004_SCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM004_SCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*---------------------------------------------------------------------*<br/>
*&nbsp;Selection-screen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_WERKS&nbsp;TYPE&nbsp;AUFK-WERKS&nbsp;MODIF&nbsp;ID&nbsp;M1.&nbsp;&nbsp;"&nbsp;工厂<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_AUART&nbsp;FOR&nbsp;AUFK-AUART,&nbsp;&nbsp;"&nbsp;生产订单类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_AUFNR&nbsp;FOR&nbsp;AUFK-AUFNR,&nbsp;&nbsp;"&nbsp;生产订单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ABLAD&nbsp;FOR&nbsp;AFPO-ABLAD&nbsp;VISIBLE&nbsp;LENGTH&nbsp;5&nbsp;NO-EXTENSION&nbsp;NO&nbsp;INTERVALS.&nbsp;&nbsp;"&nbsp;班次<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_ZDDRQ&nbsp;TYPE&nbsp;AUFK-ERDAT&nbsp;DEFAULT&nbsp;SY-DATUM&nbsp;MODIF&nbsp;ID&nbsp;M1.&nbsp;&nbsp;&nbsp;"&nbsp;订单日期<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_MATNR&nbsp;FOR&nbsp;AFPO-MATNR.&nbsp;&nbsp;"&nbsp;物料<br/>
&nbsp;&nbsp;PARAMETERS:P1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R1&nbsp;DEFAULT&nbsp;'X'&nbsp;USER-COMMAND&nbsp;CREATE&nbsp;&nbsp;MODIF&nbsp;ID&nbsp;M2,&nbsp;"&nbsp;生产收货<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;R1&nbsp;&nbsp;MODIF&nbsp;ID&nbsp;M2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;副产品收货<br/>
SELECTION-SCREEN END OF BLOCK BLK.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>