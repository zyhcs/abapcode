<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM004_PAI</h2>
<h3> Description: Include ZMM004_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM004_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_9001&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE USER_COMMAND_9001 INPUT.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F03'&nbsp;OR&nbsp;'&amp;F12'.&nbsp;&nbsp;&nbsp;&nbsp;"后退<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F15'.&nbsp;&nbsp;"关闭<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'POST'.&nbsp;&nbsp;"&nbsp;收货确认<br/>
<br/>
</div>
<div class="codeComment">
**&amp;=====&nbsp;&nbsp;数据检查校验<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_CHECK_DATA.<br/>
*<br/>
**&amp;=====&nbsp;&nbsp;报工入库&nbsp;收货确认<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_BAPI_POST.&nbsp;&nbsp;"&nbsp;报工入库<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;CHECK_ZZHIBIE&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;值别&nbsp;检查<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE CHECK_ZZHIBIE INPUT.<br/>
&nbsp;&nbsp;IF&nbsp;GS_HEAD-ZZHIBIE&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F03'&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F12'&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F15'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;ZPPT002<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ZZHIBIE&nbsp;=&nbsp;GS_HEAD-ZZHIBIE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;E000(ZMM01)&nbsp;WITH&nbsp;'【值别】请输入正确的值，参考表：ZPPT002'.&nbsp;&nbsp;"&nbsp;【值别】请输入正确的值，参考表：ZPPT002<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;CHECK_ZBANCI&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;班次&nbsp;检查<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE CHECK_ZBANCI INPUT.<br/>
&nbsp;&nbsp;IF&nbsp;GS_HEAD-ZBANCI&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F03'&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F12'&nbsp;AND&nbsp;OK_CODE&nbsp;&lt;&gt;&nbsp;'&amp;F15'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;ZPPT001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;WERKS&nbsp;=&nbsp;P_WERKS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ZBANCI&nbsp;=&nbsp;GS_HEAD-ZBANCI.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;E000(ZMM01)&nbsp;WITH&nbsp;'【班次】请输入正确的值，参考表：ZPPT001'.&nbsp;&nbsp;"&nbsp;【值别】请输入正确的值，参考表：ZPPT002<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;EXIT_PROGGAM&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE EXIT_PROGGAM INPUT.<br/>
&nbsp;&nbsp;CASE&nbsp;OK_CODE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F03'&nbsp;OR&nbsp;'&amp;F12'.&nbsp;&nbsp;&nbsp;&nbsp;"后退<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F15'.&nbsp;&nbsp;"关闭<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>