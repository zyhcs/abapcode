<table class="outerTable">
<tr>
<td><h2>Table: ZTMM019</h2>
<h3>Description: 生产入库产品等级配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WERKS</td>
<td>2</td>
<td>X</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MATKL</td>
<td>3</td>
<td>X</td>
<td>MATKL</td>
<td>MATKL</td>
<td>CHAR</td>
<td>9</td>
<td>&nbsp;</td>
<td>物料组</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZCODE</td>
<td>4</td>
<td>X</td>
<td>ZCODE</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>等级编码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZLEVEL</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZLEVEL</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>产品等级</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>