<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM004_PBO</h2>
<h3> Description: Include ZMM004_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM004_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_9001&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_9001 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STATUS_9001'.<br/>
&nbsp;&nbsp;IF&nbsp;p1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TITLEBAR_9001'&nbsp;WITH&nbsp;TEXT-026.&nbsp;"&nbsp;生产收货<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TITLEBAR_9001'&nbsp;WITH&nbsp;TEXT-027.&nbsp;"&nbsp;副产品收货<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;屏幕上的按钮，通过下面代码隐藏<br/>
</div>
<div class="code">
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;EQ&nbsp;'BTN'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-invisible&nbsp;=&nbsp;1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;EQ&nbsp;'GS_HEAD-ZFLAG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_werks&nbsp;=&nbsp;'6300'&nbsp;OR&nbsp;p_werks&nbsp;=&nbsp;'6310'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;gv_item_zfklx&nbsp;=&nbsp;'Z01'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;EQ&nbsp;'GV_ITEM_ZCZDA'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;0.&nbsp;"0代表不能输入&nbsp;1代表可以输入！<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;SET_ALV&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE set_alv OUTPUT.<br/>
<br/>
</div>
<div class="codeComment">
*--·生成alv和注册事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frem_gre_alv&nbsp;USING&nbsp;ref_wa_custom_container&nbsp;gv_table&nbsp;ref_alv_grid&nbsp;''.<br/>
<br/>
</div>
<div class="codeComment">
*--·设置字段属性<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_fieldcat.<br/>
<br/>
</div>
<div class="codeComment">
*--·设置输出格式<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_layout.<br/>
<br/>
</div>
<div class="codeComment">
*--·排除ALV工具栏上的标准按钮<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;gt_exclude[].<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_exclude_toolbar&nbsp;CHANGING&nbsp;gt_exclude.<br/>
<br/>
</div>
<div class="codeComment">
*--·显示ALV<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_display_alv.<br/>
<br/>
</div>
<div class="codeComment">
*--·搜索帮助<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;register_f4&nbsp;CHANGING&nbsp;ref_alv_grid.<br/>
<br/>
</div>
<div class="codeComment">
*--·刷新ALV<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_refresh.<br/>
<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;LISTBOX_9001&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE listbox_9001 OUTPUT.<br/>
&nbsp;&nbsp;TYPE-POOLS&nbsp;vrm.<br/>
&nbsp;&nbsp;DATA:&nbsp;vid&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vrm_id&nbsp;VALUE&nbsp;'GS_HEAD-ZFLAG',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vlist&nbsp;&nbsp;TYPE&nbsp;vrm_values,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;vlist.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;vlist.<br/>
&nbsp;&nbsp;CLEAR&nbsp;values.<br/>
&nbsp;&nbsp;MOVE&nbsp;'T'&nbsp;TO&nbsp;values-key.<br/>
&nbsp;&nbsp;MOVE&nbsp;'当天'&nbsp;TO&nbsp;values-text.<br/>
&nbsp;&nbsp;APPEND&nbsp;values&nbsp;TO&nbsp;vlist.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;values.<br/>
&nbsp;&nbsp;MOVE&nbsp;'Y'&nbsp;TO&nbsp;values-key.<br/>
&nbsp;&nbsp;MOVE&nbsp;'昨天'&nbsp;TO&nbsp;values-text.<br/>
&nbsp;&nbsp;APPEND&nbsp;values&nbsp;TO&nbsp;vlist.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CLEAR&nbsp;values.<br/>
*&nbsp;&nbsp;MOVE&nbsp;''&nbsp;TO&nbsp;values-key.<br/>
*&nbsp;&nbsp;MOVE&nbsp;''&nbsp;TO&nbsp;values-text.<br/>
*&nbsp;&nbsp;APPEND&nbsp;values&nbsp;TO&nbsp;vlist.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'VRM_SET_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;vlist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id_illegal_name&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'下拉框出错'&nbsp;TYPE&nbsp;'I'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>