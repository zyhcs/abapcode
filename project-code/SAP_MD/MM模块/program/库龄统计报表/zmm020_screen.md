<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM020_SCREEN</h2>
<h3> Description: Include ZMM015_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM015_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;s_matnr&nbsp;for&nbsp;MARD-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_WERKS&nbsp;FOR&nbsp;MARC-WERKS&nbsp;OBLIGATORY,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_LGORT&nbsp;FOR&nbsp;MARD-LGORT,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_CHARG&nbsp;FOR&nbsp;MCSD-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_MTART&nbsp;FOR&nbsp;MARA-MTART,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_MATKL&nbsp;FOR&nbsp;&nbsp;MARA-MATKL.<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
SELECTION-SCREEN BEGIN OF BLOCK BLK2 WITH FRAME TITLE TEXT-T02.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;cr1&nbsp;AS&nbsp;CHECKBOX.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_DGC&nbsp;FOR&nbsp;MARC-WERKS.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;cr2&nbsp;AS&nbsp;CHECKBOX.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_KUNNR2&nbsp;FOR&nbsp;MCSD-KUNNR.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;cr3&nbsp;AS&nbsp;CHECKBOX.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_KUNNR3&nbsp;FOR&nbsp;MCSD-KUNNR.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;cr4&nbsp;AS&nbsp;CHECKBOX.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_KUNNR4&nbsp;FOR&nbsp;MCSD-KUNNR.<br/>
SELECTION-SCREEN END OF BLOCK BLK2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>