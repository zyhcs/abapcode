<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZLTL001</h2>
<h3> Description: 领退料单查询</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*---------------------------------------------------------*<br/>
*&nbsp;程序名称：领退料清单<br/>
*&nbsp;程序名：&nbsp;&nbsp;ZLTL001<br/>
*&nbsp;开发日期：20210802<br/>
*&nbsp;创建者：&nbsp;&nbsp;WEIXP<br/>
*---------------------------------------------------------*<br/>
*&nbsp;概要说明<br/>
*---------------------------------------------------------*<br/>
*&nbsp;&nbsp;领退料清单<br/>
*---------------------------------------------------------*<br/>
*&nbsp;变更记录<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;日期&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改者&nbsp;&nbsp;&nbsp;&nbsp;传输请求号&nbsp;&nbsp;&nbsp;&nbsp;修改内容及原因<br/>
*---------------------------------------------------------*<br/>
*&nbsp;yyyy-mm-dd&nbsp;&nbsp;&nbsp;&nbsp;张三&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEVK90000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因为.......所以修改了.....<br/>
*&nbsp;yyyy-mm-dd&nbsp;&nbsp;&nbsp;&nbsp;李四&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEVK90010&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因为.......所以修改了.....<br/>
*---------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
REPORT zltl001 MESSAGE-ID zmm001.<br/>
TABLES: ztmm014,ztmm015,aufk.<br/>
TYPE-POOLS icon.<br/>
PARAMETERS p_werks TYPE ztmm014-werks OBLIGATORY MEMORY ID wrk.<br/>
SELECT-OPTIONS:s_docnr FOR ztmm014-docnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ywlx&nbsp;FOR&nbsp;ztmm014-ywlx&nbsp;MATCHCODE&nbsp;OBJECT&nbsp;zsh_ywlx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lllx&nbsp;FOR&nbsp;ztmm014-lllx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_status&nbsp;FOR&nbsp;ztmm014-status,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matnr&nbsp;FOR&nbsp;ztmm015-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_cdate&nbsp;FOR&nbsp;ztmm014-zz_crt_dat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_cusr&nbsp;FOR&nbsp;ztmm014-zz_crt_usr&nbsp;MATCHCODE&nbsp;OBJECT&nbsp;user_comp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_appl&nbsp;FOR&nbsp;ztmm014-applicant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kostl&nbsp;FOR&nbsp;ztmm014-kostl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_aufnr2&nbsp;FOR&nbsp;ztmm014-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_auart&nbsp;FOR&nbsp;aufk-auart&nbsp;MATCHCODE&nbsp;OBJECT&nbsp;zsh_auart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_aufnr&nbsp;FOR&nbsp;ztmm014-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_banci&nbsp;&nbsp;FOR&nbsp;ztmm014-zbanci,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_zhibie&nbsp;FOR&nbsp;ztmm014-zzhibie&nbsp;&nbsp;.<br/>
<br/>
DATA gt_data TYPE TABLE OF  zltl_view.<br/>
<br/>
</div>
<div class="codeComment">
*TYPES:&nbsp;BEGIN&nbsp;OF&nbsp;gs_data.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCLUDE&nbsp;TYPE&nbsp;zltl_view.<br/>
*TYPES:&nbsp;&nbsp;&nbsp;bdmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;resb-bdmng,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"物料预留数量<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins_jb&nbsp;TYPE&nbsp;resb-meins,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"基本单位字段<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gs_data.<br/>
<br/>
*DATA&nbsp;gt_data&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;gs_data.<br/>
*&nbsp;定义ALV数据选择内表<br/>
</div>
<div class="code">
DATA: gt_checked TYPE TABLE OF zltl_view,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_checked&nbsp;TYPE&nbsp;zltl_view.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;定义SmartForms抬头内表<br/>
</div>
<div class="code">
DATA: gt_head TYPE TABLE OF zpps001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head&nbsp;TYPE&nbsp;zpps001.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;定义SmartForms行项目内表<br/>
</div>
<div class="code">
DATA: gt_item TYPE TABLE OF zpps001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_item&nbsp;TYPE&nbsp;zpps001.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*&nbsp;定义SMART&nbsp;FORMS变量<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA: ssf_name        TYPE tdsfname,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fm_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rs38l_fnam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;control&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ssfctrlop,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output_options&nbsp;&nbsp;TYPE&nbsp;ssfcompop,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;job_output_info&nbsp;TYPE&nbsp;ssfcrescl.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*ALV00相关变量<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA go_alv TYPE REF TO cl_gui_alv_grid.<br/>
DATA go_container TYPE REF TO cl_gui_custom_container.<br/>
DATA ok_code TYPE sy-ucomm.<br/>
DATA save_ok TYPE sy-ucomm.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*定义类<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
CLASS lcl_event_receiver DEFINITION.<br/>
&nbsp;&nbsp;PUBLIC&nbsp;SECTION.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLASS-METHODS:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"自定义工具栏<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_toolbar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;toolbar&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_object&nbsp;e_interactive,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"自定义菜单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_menu_button&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;menu_button&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_object&nbsp;e_ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"自定义按钮<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_user_command&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;user_command&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单击事件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_hotspot_click1&nbsp;&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;hotspot_click&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_row_id&nbsp;e_column_id,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"数据改变触发<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_data_changed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;data_changed&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;er_data_changed,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"双击<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_double_click&nbsp;FOR&nbsp;EVENT&nbsp;double_click&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_row<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_column<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;es_row_no.<br/>
&nbsp;&nbsp;PRIVATE&nbsp;SECTION.<br/>
ENDCLASS.<br/>
<br/>
<br/>
INITIALIZATION.<br/>
&nbsp;&nbsp;s_cdate-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;s_cdate-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;s_cdate-high&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;s_cdate-low&nbsp;=&nbsp;sy-datum&nbsp;-&nbsp;7.<br/>
&nbsp;&nbsp;APPEND&nbsp;s_cdate.<br/>
&nbsp;&nbsp;DATA(gv_title)&nbsp;=&nbsp;sy-title.<br/>
&nbsp;&nbsp;s_status-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;s_status-option&nbsp;=&nbsp;'NE'.<br/>
&nbsp;&nbsp;s_status-low&nbsp;=&nbsp;'30'.<br/>
&nbsp;&nbsp;APPEND&nbsp;s_status.<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_lllx-low.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_f4_lllx&nbsp;USING&nbsp;'S_LLLX-LOW'.<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_appl-low.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_f4_applicant&nbsp;USING&nbsp;'S_APPL-LOW'.<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_lllx-high.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_f4_lllx&nbsp;USING&nbsp;'S_LLLX-HIGH'.<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_appl-high.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_f4_applicant&nbsp;USING&nbsp;'S_APPL-HIGH'.<br/>
<br/>
<br/>
<br/>
<br/>
START-OF-SELECTION.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;IF&nbsp;gt_data&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s017&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;9001.<br/>
<br/>
<br/>
END-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_DISPLAY_ALVOO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_display_alvoo .<br/>
<br/>
&nbsp;&nbsp;DATA:ls_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_fieldcat&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_seting&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_glay,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_excluding&nbsp;&nbsp;TYPE&nbsp;&nbsp;slis_t_extab.<br/>
&nbsp;&nbsp;lv_seting-edt_cll_cb&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_layout_lvc-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_layout_lvc-cwidth_opt&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_layout_lvc-sel_mode&nbsp;=&nbsp;'A'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout_lvc-sel_mode&nbsp;=&nbsp;'D'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_layout_lvc-i_save&nbsp;=&nbsp;'A'<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout_lvc-box_fname&nbsp;&nbsp;=&nbsp;'ZZ_SEL'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_layout_lvc-edit&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;ls_layout_lvc-no_rowmark&nbsp;=&nbsp;'X'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_variant&nbsp;TYPE&nbsp;&nbsp;disvariant.&nbsp;&nbsp;"布局<br/>
&nbsp;&nbsp;ls_variant-report&nbsp;=&nbsp;sy-repid.&nbsp;&nbsp;"布局<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'LVC_FIELDCATALOG_MERGE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BUFFER_ACTIVE&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_structure_name&nbsp;=&nbsp;'ZLTL_VIEW'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CLIENT_NEVER_DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BYPASSING_BUFFER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_INTERNAL_TABNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"也可用内表<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ct_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_fieldcat<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCONSISTENT_INTERFACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROGRAM_ERROR&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_fieldcat&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_fieldcat&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;&lt;ls_fieldcat&gt;-fieldname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'.NODE1'&nbsp;OR&nbsp;'MTYPE'&nbsp;OR&nbsp;'REFITEM'&nbsp;OR&nbsp;'ZZ_SEL'&nbsp;OR&nbsp;'PEINH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ISTH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'退货标识'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'STATUS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-edit_mask&nbsp;=&nbsp;'==LLSTA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'YW_TEXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'业务类型描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'LL_TEXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'领料类型描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'BKTXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'抬头文本'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'OA_RNAME'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'OA审批人'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'OA_ZRETXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'OA拒绝原因'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'T_APPL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'申请人描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'LIFNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'合作伙伴'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'T_LIFNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'合作伙伴描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'KOSTL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'成本中心'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'T_KOSTL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'成本中心描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'T_WERKS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'工厂名称'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'HEAD_AUFNR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'内部订单'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'T_AUFNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'内部描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'AUFNR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'生产订单'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SEND_OA'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'OA审核'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MATNR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'生产订单抬头物料'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MAKTX'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'生产订单抬头物料描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MAKTX'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'生产订单抬头物料描述'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MENGE_JH'&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'计划数量'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MENGE_YL'&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'已领数量'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PP_MENGE_POST'&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'已过账数量'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'MENGE'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'计划数量'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ACT_MENGE'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'过账数量'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'BDMNG'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'预留数量'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'MEINS_JB'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'基本单位'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PRICE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'单价'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'AMOUNT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'金额'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'DMBTR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_fieldcat&gt;-coltext&nbsp;=&nbsp;'凭证金额'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;go_container<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;parent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;container_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C1'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;style&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifetime&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lifetime_default<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_autodef_progid_dynnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_system_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifetime_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifetime_dynpro_dynpro_link&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6.<br/>
&nbsp;&nbsp;ASSERT&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;go_alv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_shellstyle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_lifetime&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_parent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;go_container<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_appl_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;space<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_parentdbg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_applogparent&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_graphicsparent&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_fcat_complete&nbsp;&nbsp;&nbsp;=&nbsp;SPACE<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_cntl_create&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_cntl_init&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_cntl_link&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_dp_create&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;MESSAGE&nbsp;ID&nbsp;SY-MSGID&nbsp;TYPE&nbsp;SY-MSGTY&nbsp;NUMBER&nbsp;SY-MSGNO<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;SY-MSGV1&nbsp;SY-MSGV2&nbsp;SY-MSGV3&nbsp;SY-MSGV4.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;DATA(lo_handler)&nbsp;=&nbsp;NEW&nbsp;lcl_event_receiver(&nbsp;).<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_toolbar&nbsp;FOR&nbsp;go_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_menu_button&nbsp;FOR&nbsp;go_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_user_command&nbsp;FOR&nbsp;go_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_data_changed&nbsp;FOR&nbsp;go_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_hotspot_click1&nbsp;FOR&nbsp;go_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;lo_handler-&gt;handle_double_click&nbsp;FOR&nbsp;go_alv.<br/>
<br/>
&nbsp;&nbsp;go_alv-&gt;register_edit_event(&nbsp;cl_gui_alv_grid=&gt;mc_evt_enter&nbsp;).&nbsp;"回车时触发<br/>
&nbsp;&nbsp;go_alv-&gt;register_edit_event(&nbsp;cl_gui_alv_grid=&gt;mc_evt_modified&nbsp;).&nbsp;"单元格更改触发<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;go_alv-&gt;set_table_for_first_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_buffer_active&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_bypassing_buffer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_consistency_check&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_structure_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_variant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_variant<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_layout_lvc<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_print&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_special_groups&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_toolbar_excluding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_hyperlink&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_alv_graphics&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_except_qinfo&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ir_salv_adapter&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcatalog&nbsp;=&nbsp;lt_fieldcat<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_sort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_parameter_combination&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;too_many_lines&nbsp;&nbsp;=&nbsp;3<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;others&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;EXIT&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE exit INPUT.<br/>
&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_9001&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE user_command_9001 INPUT.<br/>
&nbsp;&nbsp;CLEAR&nbsp;save_ok.<br/>
&nbsp;&nbsp;save_ok&nbsp;=&nbsp;ok_code.<br/>
&nbsp;&nbsp;CLEAR&nbsp;ok_code.<br/>
&nbsp;&nbsp;CASE&nbsp;save_ok.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'BACK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'PRINT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"在此次添加打印代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_for_print.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_9001&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_9001 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'ALV'.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'ALV'&nbsp;WITH&nbsp;&nbsp;gv_title.<br/>
&nbsp;&nbsp;IF&nbsp;go_alv&nbsp;IS&nbsp;NOT&nbsp;BOUND.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_display_alvoo.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_GET_LOG<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_data .<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zltl_view&nbsp;&nbsp;AS&nbsp;view<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;docnr&nbsp;IN&nbsp;@s_docnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~ywlx&nbsp;IN&nbsp;@s_ywlx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~lllx&nbsp;IN&nbsp;@s_lllx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~werks&nbsp;=&nbsp;@p_werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~status&nbsp;IN&nbsp;@s_status<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~zz_crt_dat&nbsp;IN&nbsp;@s_cdate<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~zz_crt_usr&nbsp;IN&nbsp;@s_cusr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~applicant&nbsp;IN&nbsp;@s_appl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~matnr&nbsp;IN&nbsp;@s_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~kostl&nbsp;IN&nbsp;@s_kostl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~head_aufnr&nbsp;IN&nbsp;@s_aufnr2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~pp_auart&nbsp;IN&nbsp;@s_auart<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~aufnr&nbsp;IN&nbsp;@s_aufnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~zbanci&nbsp;IN&nbsp;@s_banci<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~zzhibie&nbsp;IN&nbsp;@s_zhibie<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;@gt_data<br/>
&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~mblnr&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~mjahr&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~matnr&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"物料<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~charg&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"批次<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view~act_menge,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;doc~dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;@gt_data&nbsp;AS&nbsp;view<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEFT&nbsp;JOIN&nbsp;matdoc&nbsp;AS&nbsp;doc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;view~mblnr&nbsp;=&nbsp;doc~mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~mjahr&nbsp;=&nbsp;doc~mjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~matnr&nbsp;=&nbsp;doc~matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"物料<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~charg&nbsp;=&nbsp;doc~charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"批次<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~act_menge&nbsp;=&nbsp;doc~erfmg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_matdoc).<br/>
&nbsp;&nbsp;SORT&nbsp;lt_matdoc&nbsp;BY&nbsp;mblnr&nbsp;mjahr&nbsp;matnr&nbsp;charg&nbsp;act_menge.<br/>
&nbsp;&nbsp;SORT&nbsp;gt_data&nbsp;BY&nbsp;docnr&nbsp;item&nbsp;.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;resb~rsnum,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;resb~rspos,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;resb~bdmng,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;resb~meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;@gt_data&nbsp;AS&nbsp;view<br/>
&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;resb<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;view~rsnum&nbsp;=&nbsp;resb~rsnum<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;view~rspos&nbsp;=&nbsp;resb~rspos<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;view~aufnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_resb).<br/>
&nbsp;&nbsp;SORT&nbsp;lt_resb&nbsp;BY&nbsp;rsnum&nbsp;rspos.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_data&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_data&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-price&nbsp;=&nbsp;&lt;ls_data&gt;-price&nbsp;/&nbsp;&lt;ls_data&gt;-peinh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-amount&nbsp;=&nbsp;&lt;ls_data&gt;-price&nbsp;*&nbsp;&lt;ls_data&gt;-act_menge.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_matdoc&nbsp;WITH&nbsp;KEY&nbsp;mblnr&nbsp;=&nbsp;&lt;ls_data&gt;-mblnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mjahr&nbsp;=&nbsp;&lt;ls_data&gt;-mjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;=&nbsp;&lt;ls_data&gt;-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;=&nbsp;&lt;ls_data&gt;-charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;act_menge&nbsp;=&nbsp;&lt;ls_data&gt;-act_menge&nbsp;INTO&nbsp;DATA(lw_matdoc)&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-dmbtr&nbsp;=&nbsp;lw_matdoc-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_resb&nbsp;WITH&nbsp;KEY&nbsp;rsnum&nbsp;=&nbsp;&lt;ls_data&gt;-rsnum&nbsp;rspos&nbsp;=&nbsp;&lt;ls_data&gt;-rspos&nbsp;INTO&nbsp;DATA(lw_resb)&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;ls_data&gt;-werks&nbsp;=&nbsp;'6300'&nbsp;OR&nbsp;&lt;ls_data&gt;-werks&nbsp;=&nbsp;'6310'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;ls_data&gt;-ywlx&nbsp;=&nbsp;'P1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-bdmng&nbsp;=&nbsp;lw_resb-bdmng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;ls_data&gt;-ywlx&nbsp;=&nbsp;'P2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-bdmng&nbsp;=&nbsp;lw_resb-bdmng&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-meins_jb&nbsp;=&nbsp;lw_resb-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*---------------------------------------------------------------------*<br/>
*&nbsp;实现类<br/>
*---------------------------------------------------------------------*<br/>
</div>
<div class="code">
CLASS lcl_event_receiver IMPLEMENTATION.<br/>
&nbsp;&nbsp;METHOD&nbsp;handle_toolbar.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;line_exists(&nbsp;&nbsp;e_object-&gt;mt_toolbar[&nbsp;function&nbsp;=&nbsp;'&amp;MB_SUBTOT'&nbsp;]&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_object-&gt;mt_toolbar[&nbsp;function&nbsp;=&nbsp;'&amp;MB_SUBTOT'&nbsp;]-disabled&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;e_object-&gt;mt_toolbar&nbsp;WHERE&nbsp;function&nbsp;=&nbsp;'&amp;PRINT_BACK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_object-&gt;mt_toolbar&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;BASE&nbsp;e_object-&gt;mt_toolbar&nbsp;(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;function&nbsp;=&nbsp;'SENDOA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;icon&nbsp;=&nbsp;icon_mail<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;=&nbsp;'发送OA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;quickinfo&nbsp;=&nbsp;'发送到OA审核'&nbsp;)&nbsp;).<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_object-&gt;mt_toolbar&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;BASE&nbsp;e_object-&gt;mt_toolbar&nbsp;(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;function&nbsp;=&nbsp;'REFRESH'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;icon&nbsp;=&nbsp;icon_refresh<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;=&nbsp;'刷新'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;quickinfo&nbsp;=&nbsp;''&nbsp;)&nbsp;).<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handle_toolbar<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_menu_button&nbsp;.&nbsp;&nbsp;"自定义菜单实现方法<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handle_menu_button<br/>
</div>
<div class="codeComment">
*---------------------------------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_user_command.&nbsp;&nbsp;"自定义按钮实现方法<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_msg&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_mtype&nbsp;TYPE&nbsp;bapi_mtype.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lt_docnr&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;zelldocnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;e_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SENDOA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;go_alv-&gt;get_selected_rows(&nbsp;IMPORTING&nbsp;et_index_rows&nbsp;=&nbsp;DATA(lt_index)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;et_row_no&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;DATA(lt_nos)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_nos&nbsp;&nbsp;BY&nbsp;row_id.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_nos&nbsp;INTO&nbsp;DATA(ls_nos).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_data&gt;)&nbsp;INDEX&nbsp;ls_nos-row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_docnr&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;BASE&nbsp;lt_docnr&nbsp;(&nbsp;&lt;ls_data&gt;-docnr&nbsp;)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;ADJACENT&nbsp;DUPLICATES&nbsp;FROM&nbsp;lt_docnr&nbsp;COMPARING&nbsp;ALL&nbsp;FIELDS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_docnr&nbsp;INTO&nbsp;DATA(lv_docnr).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;="zif_mm057_ltldoc_send_to_oa zif_mm057_ltldoc_send_to_oa.html"="">'ZIF_MM057_LTLDOC_SEND_TO_OA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_docnr&nbsp;=&nbsp;lv_docnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_mtype<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_data&nbsp;ASSIGNING&nbsp;&lt;ls_data&gt;&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;lv_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-icon&nbsp;=&nbsp;icon_led_red.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-message&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-icon&nbsp;=&nbsp;icon_led_green.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-message&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_data&gt;-status&nbsp;=&nbsp;'40'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_mtype.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;go_alv-&gt;refresh_table_display(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'REFRESH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;go_alv-&gt;refresh_table_display(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;ENDMETHOD.<br/>
<br/>
&nbsp;&nbsp;METHOD&nbsp;handle_data_changed.&nbsp;&nbsp;"数据变化实现方法<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.<br/>
<br/>
&nbsp;&nbsp;METHOD&nbsp;handle_hotspot_click1.&nbsp;"单击事件实现方法<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"HANDLE_HOTSPOT_CLICK<br/>
<br/>
&nbsp;&nbsp;METHOD&nbsp;handle_double_click.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;&nbsp;INTO&nbsp;DATA(ls_data)&nbsp;INDEX&nbsp;e_row-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'ZLTLDOC'&nbsp;FIELD&nbsp;ls_data-docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'ZLL003'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.<br/>
ENDCLASS.<br/>
</a&nbsp;href&nbsp;="zif_mm057_ltldoc_send_to_oa></div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_for_print<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;领料单打印<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_for_print .<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_deal_data.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;数据预处理<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_data_print.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;SmartForms数据打印<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_DEAL_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;数据预处理<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_deal_data .<br/>
</div>
<div class="codeComment">
**&nbsp;&nbsp;清空勾选内表数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;gt_checked.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;将勾选的数据放到临时内表中<br/>
</div>
<div class="code">
&nbsp;&nbsp;go_alv-&gt;get_selected_rows(&nbsp;IMPORTING&nbsp;et_index_rows&nbsp;=&nbsp;DATA(lt_index)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;et_row_no&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;DATA(lt_nos)&nbsp;).<br/>
&nbsp;&nbsp;SORT&nbsp;lt_nos&nbsp;&nbsp;BY&nbsp;row_id.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_nos&nbsp;INTO&nbsp;DATA(ls_nos).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_data&gt;)&nbsp;INDEX&nbsp;ls_nos-row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;&lt;ls_data&gt;&nbsp;TO&nbsp;gs_checked.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_checked&nbsp;TO&nbsp;gt_checked.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_data&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;fs_opt&gt;)&nbsp;WHERE&nbsp;zz_sel&nbsp;=&nbsp;'X'&nbsp;.<br/>
**&nbsp;&nbsp;&nbsp;如果被勾选，则将数据添加到勾选内表中"<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;&lt;fs_opt&gt;&nbsp;TO&nbsp;gs_checked.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_checked&nbsp;TO&nbsp;gt_checked.<br/>
**&nbsp;&nbsp;&nbsp;清空结构<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_checked.<br/>
*&nbsp;&nbsp;ENDLOOP.<br/>
*&nbsp;当有勾选的数据时才调用打印<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;gt_checked&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;遍历勾选内表&nbsp;获取打印抬头数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gt_head.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_checked&nbsp;INTO&nbsp;gs_checked&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;根据条件获取抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_head&nbsp;WITH&nbsp;KEY&nbsp;docnr&nbsp;=&nbsp;gs_checked-docnr&nbsp;INTO&nbsp;gs_head.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;抬头表为空,肯定获取不到数据,所以状态不为0<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;将需要的数据加入到抬头内表<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_checked&nbsp;TO&nbsp;gs_head.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gs_checked-send_oa&nbsp;=&nbsp;'Y'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;申请人<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-name_last&nbsp;=&nbsp;gs_checked-t_appl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;name_last&nbsp;INTO&nbsp;gs_head-name_last&nbsp;FROM&nbsp;user_addr&nbsp;WHERE&nbsp;bname&nbsp;=&nbsp;gs_checked-zz_crt_usr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-ktext_k&nbsp;=&nbsp;gs_checked-t_kostl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-zywlx&nbsp;=&nbsp;gs_checked-yw_text&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-ktext_a&nbsp;=&nbsp;gs_checked-t_aufnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-zlllx&nbsp;=&nbsp;gs_checked-ll_text&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-t_lifnr&nbsp;=&nbsp;gs_checked-t_lifnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gs_checked-werks&nbsp;=&nbsp;'6000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_head-zzltxbh&nbsp;=&nbsp;'质量体系编号:HY质控024B'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_head&nbsp;TO&nbsp;gt_head.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清空勾选结构<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_checked.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;没有勾选数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'请勾选数据！'&nbsp;TYPE&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_DATA_PRINT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;SmartForms数据打印<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_data_print .<br/>
</div>
<div class="codeComment">
*&nbsp;定义smartforms参数，实现假脱机<br/>
</div>
<div class="code">
&nbsp;&nbsp;control-preview&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;control-no_dialog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;control-no_open&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;control-no_close&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;output_options-tdimmed&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;output_options-tdarmod&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;output_options-tdiexit&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;开启假脱机"<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_open_ssf.<br/>
</div>
<div class="codeComment">
*&nbsp;开启循环打印<br/>
*&nbsp;遍历抬头表&nbsp;根据指定条件从勾选内表中获取行项目数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:lv_ywlx&nbsp;TYPE&nbsp;ztmm016a-ywlx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_isth&nbsp;TYPE&nbsp;ztmm016a-isth.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_head&nbsp;INTO&nbsp;gs_head&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_ywlx,lv_isth.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;清空行项目表<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gt_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lv_num&nbsp;TYPE&nbsp;i&nbsp;VALUE&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;遍历勾选内表,根据抬头表的条件<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_data&nbsp;INTO&nbsp;DATA(gs_data)&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;gs_head-docnr&nbsp;AND&nbsp;refitem&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_num&nbsp;=&nbsp;lv_num&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_ywlx&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gs_data-ywlx&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_ywlx&nbsp;=&nbsp;gs_data-ywlx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_item-zxh&nbsp;=&nbsp;lv_num&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_item-zllsl&nbsp;=&nbsp;gs_data-menge.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;将需要的行项目数据放入数据表内<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_data&nbsp;TO&nbsp;gs_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;lgobe&nbsp;INTO&nbsp;gs_item-lgobe<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t001l&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;gs_data-werks&nbsp;AND&nbsp;lgort&nbsp;=&nbsp;gs_data-lgort.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_item&nbsp;TO&nbsp;gt_item.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;清空勾选结构<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;调用SmartForms、生成函数接口"<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;isth&nbsp;INTO&nbsp;lv_isth&nbsp;FROM&nbsp;ztmm016a&nbsp;WHERE&nbsp;ywlx&nbsp;=&nbsp;lv_ywlx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_isth&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssf_name&nbsp;=&nbsp;'ZPPF002'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssf_name&nbsp;=&nbsp;'ZPPF001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SSF_FUNCTION_MODULE_NAME'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ssf_name<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ariant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;direct_call&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fm_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_fm_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_form&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_function_module&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;调用SMARTFORMS生成接口<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;l_fm_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;传递需要打印的数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_head<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;设置假脱机参数<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output_options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;output_options<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;control_parameters&nbsp;=&nbsp;control<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;传入打印数据参数"<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;itab1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_head.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;打印结束，关闭假脱机"<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_close_ssf.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_OPEN_SSF<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;定义假脱机开始参数设置<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_open_ssf .<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SSF_OPEN'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_settings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;control_parameters&nbsp;=&nbsp;control<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output_options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;output_options<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formatting_error&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;internal_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;send_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_canceled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_CLOSE_SSF<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;定义假脱机结束参数设置<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_close_ssf .<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SSF_CLOSE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;job_output_info&nbsp;&nbsp;=&nbsp;job_output_info<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formatting_error&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;internal_error&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;send_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_f4_lllx<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_lllx  USING    VALUE(iv_field).<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_lllx)<br/>
&nbsp;&nbsp;FROM&nbsp;ztmm016a<br/>
&nbsp;&nbsp;WHERE&nbsp;&nbsp;&nbsp;ywlx&nbsp;IN&nbsp;@s_ywlx.<br/>
<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_field&nbsp;TYPE&nbsp;help_info-dynprofld.<br/>
&nbsp;&nbsp;lv_field&nbsp;=&nbsp;iv_field.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=&nbsp;'ZTPLM002'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'LLLX'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;=&nbsp;lv_field<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;=&nbsp;lt_lllx<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;=&nbsp;lt_return<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_F4_APPLICANT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_f4_applicant USING  VALUE(iv_field) .<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;but000~partner,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;but000~name_org1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;but000~bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000<br/>
&nbsp;&nbsp;WHERE&nbsp;but000~bu_group&nbsp;=&nbsp;'Z006'<br/>
&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_but000).<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lt_return&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;ddshretval.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_maping&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;dselc&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_field&nbsp;TYPE&nbsp;help_info-dynprofld.<br/>
&nbsp;&nbsp;lv_field&nbsp;=&nbsp;iv_field.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4IF_INT_TABLE_VALUE_REQUEST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ddic_structure&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retfield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'PARTNER'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVALKEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpprog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid&nbsp;&nbsp;"必输字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-dynnr&nbsp;&nbsp;&nbsp;"必输字段,否则无法带入到屏幕字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynprofield&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_field<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STEPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WINDOW_TITLE&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MULTIPLE_CHOICE&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISPLAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_FORM&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALLBACK_METHOD&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MARK_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_RESET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_but000<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD_TAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpfld_mapping&nbsp;=&nbsp;lt_maping[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETER_ERROR&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_VALUES_FOUND&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*GUI&nbsp;Texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;ALV&nbsp;--&gt;&nbsp;&amp;1&nbsp;&amp;2<br/>
<br/>
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;001&nbsp;重推<br/>
*&nbsp;002&nbsp;没有需要重推的日志!<br/>
*&nbsp;003&nbsp;重推成功<br/>
*&nbsp;004&nbsp;重推失败<br/>
*&nbsp;005&nbsp;条<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_WERKS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_APPL&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_AUART&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_AUFNR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_AUFNR2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;内部订单<br/>
*&nbsp;S_BANCI&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_CDATE&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_CUSR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_DOCNR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_KOSTL&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_LLLX&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_MATNR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_STATUS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_YWLX&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_ZHIBIE&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;请勾选数据！<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;ZMM001<br/>
*017&nbsp;&nbsp;&nbsp;未查询到符合条件的数据!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>