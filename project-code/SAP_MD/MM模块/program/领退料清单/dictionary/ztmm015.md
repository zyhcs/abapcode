<table class="outerTable">
<tr>
<td><h2>Table: ZTMM015</h2>
<h3>Description: 领退料明细数据</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>DOCNR</td>
<td>2</td>
<td>X</td>
<td>ZELLDOCNR</td>
<td>ZDLLDOCNR</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>领退料单号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ITEM</td>
<td>3</td>
<td>X</td>
<td>ZEITEM</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>行号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>REF_ITEM</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>参考行号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>LGORT</td>
<td>5</td>
<td>&nbsp;</td>
<td>LGORT_D</td>
<td>LGORT</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>存储地点</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MATNR</td>
<td>6</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>CHARG</td>
<td>7</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MEINS</td>
<td>8</td>
<td>&nbsp;</td>
<td>ERFME</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>条目单位</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MENGE</td>
<td>9</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ACT_MENGE</td>
<td>10</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>11</td>
<td>XCHAR</td>
<td>11</td>
<td>&nbsp;</td>
<td>XCHAR</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>批量管理标识(内部)</td>
</tr>
<tr class="cell">
<td>12</td>
<td>KUNNR</td>
<td>12</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>13</td>
<td>BWART</td>
<td>13</td>
<td>&nbsp;</td>
<td>BWART</td>
<td>BWART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>移动类型(库存管理)</td>
</tr>
<tr class="cell">
<td>14</td>
<td>AUFNR</td>
<td>14</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>15</td>
<td>RSNUM</td>
<td>15</td>
<td>&nbsp;</td>
<td>RSNUM</td>
<td>RSNUM</td>
<td>NUMC</td>
<td>10</td>
<td>&nbsp;</td>
<td>预留/相关需求的编号</td>
</tr>
<tr class="cell">
<td>16</td>
<td>RSPOS</td>
<td>16</td>
<td>&nbsp;</td>
<td>RSPOS</td>
<td>RSPOS</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>预留 / 相关需求的项目编号</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>