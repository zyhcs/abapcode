<table class="outerTable">
<tr>
<td><h2>Table: ZSMM005</h2>
<h3>Description: 领退料单行项目</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>.INCLUDE</td>
<td>1</td>
<td>&nbsp;</td>
<td>ZTMM015</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>0</td>
<td>&nbsp;</td>
<td>领退料明细数据</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MANDT</td>
<td>2</td>
<td>&nbsp;</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>DOCNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZELLDOCNR</td>
<td>ZDLLDOCNR</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>领退料单号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ITEM</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZEITEM</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>行号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>REF_ITEM</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>参考行号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>LGORT</td>
<td>6</td>
<td>&nbsp;</td>
<td>LGORT_D</td>
<td>LGORT</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>存储地点</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MATNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>CHARG</td>
<td>8</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MEINS</td>
<td>9</td>
<td>&nbsp;</td>
<td>ERFME</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>条目单位</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MENGE</td>
<td>10</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ACT_MENGE</td>
<td>11</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>12</td>
<td>XCHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>XCHAR</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>批量管理标识(内部)</td>
</tr>
<tr class="cell">
<td>13</td>
<td>KUNNR</td>
<td>13</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>14</td>
<td>BWART</td>
<td>14</td>
<td>&nbsp;</td>
<td>BWART</td>
<td>BWART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>移动类型(库存管理)</td>
</tr>
<tr class="cell">
<td>15</td>
<td>AUFNR</td>
<td>15</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>16</td>
<td>RSNUM</td>
<td>16</td>
<td>&nbsp;</td>
<td>RSNUM</td>
<td>RSNUM</td>
<td>NUMC</td>
<td>10</td>
<td>&nbsp;</td>
<td>预留/相关需求的编号</td>
</tr>
<tr class="cell">
<td>17</td>
<td>RSPOS</td>
<td>17</td>
<td>&nbsp;</td>
<td>RSPOS</td>
<td>RSPOS</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>预留 / 相关需求的项目编号</td>
</tr>
<tr class="cell">
<td>18</td>
<td>SEL</td>
<td>18</td>
<td>&nbsp;</td>
<td>FLAG</td>
<td>FLAG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>一般标记</td>
</tr>
<tr class="cell">
<td>19</td>
<td>PP_AUART</td>
<td>19</td>
<td>&nbsp;</td>
<td>AUFART</td>
<td>AUFART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>订单类型</td>
</tr>
<tr class="cell">
<td>20</td>
<td>PP_MATNR</td>
<td>20</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>21</td>
<td>PP_MAKTX</td>
<td>21</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>22</td>
<td>PP_MENGE_JH</td>
<td>22</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>23</td>
<td>PP_MENGE_YL</td>
<td>23</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>24</td>
<td>PP_MENGE_POST</td>
<td>24</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>25</td>
<td>PP_MEINS</td>
<td>25</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>26</td>
<td>PP_GSTRP</td>
<td>26</td>
<td>&nbsp;</td>
<td>PM_ORDGSTRP</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基本开始日期</td>
</tr>
<tr class="cell">
<td>27</td>
<td>PP_LGORT</td>
<td>27</td>
<td>&nbsp;</td>
<td>ZEPP_LGORT</td>
<td>LGORT</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>入库存储地点</td>
</tr>
<tr class="cell">
<td>28</td>
<td>PP_LGOBE</td>
<td>28</td>
<td>&nbsp;</td>
<td>ZEPP_LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>入库存储地点描述</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ICON</td>
<td>29</td>
<td>&nbsp;</td>
<td>ICON_D</td>
<td>ICON</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>文本字段中的图标（替换显示，别名）</td>
</tr>
<tr class="cell">
<td>30</td>
<td>MAKTX</td>
<td>30</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>31</td>
<td>CHARG_SPLIT</td>
<td>31</td>
<td>&nbsp;</td>
<td>CHAR10</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>字符字段长度为 10</td>
</tr>
<tr class="cell">
<td>32</td>
<td>LABST</td>
<td>32</td>
<td>&nbsp;</td>
<td>LABST</td>
<td>MENG13V</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>非限制使用的估价的库存</td>
</tr>
<tr class="cell">
<td>33</td>
<td>VERID</td>
<td>33</td>
<td>&nbsp;</td>
<td>VERID</td>
<td>VERID</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>生产版本</td>
</tr>
<tr class="cell">
<td>34</td>
<td>ABLAD</td>
<td>34</td>
<td>&nbsp;</td>
<td>ABLAD</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>卸货点</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>X</td>
<td>&nbsp;</td>
<td>标志已设置（事件引发）</td>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>未设置标记</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>