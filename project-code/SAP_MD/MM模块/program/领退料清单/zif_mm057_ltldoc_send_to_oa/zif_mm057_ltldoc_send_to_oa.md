<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM057_LTLDOC_SEND_TO_OA</h2>
<h3> Description: 发送oa审核</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm057_ltldoc_send_to_oa.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IV_DOCNR) TYPE  ZELLDOCNR<br/>
*"  EXPORTING<br/>
*"     REFERENCE(MTYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(MESSAGE) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_mm057_ltldoc_send_to_oa.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zfm_ltldoc_auth_check="" zfm_ltldoc_auth_check.html"="">'ZFM_LTLDOC_AUTH_CHECK'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_docnr&nbsp;&nbsp;=&nbsp;iv_docnr<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*     IV_WERKS  =<br/>
*     IV_YWLX   =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_action&nbsp;=&nbsp;'SENDOA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;mtype<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;&nbsp;&nbsp;=&nbsp;message.<br/>
&nbsp;&nbsp;IF&nbsp;(&nbsp;mtype&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;AND&nbsp;message&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;)&nbsp;AND&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;message&nbsp;TYPE&nbsp;mtype.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;CHECK&nbsp;mtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zfm_lock_ltldoc="" zfm_lock_ltldoc.html"="">'ZFM_LOCK_LTLDOC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docnr&nbsp;&nbsp;&nbsp;=&nbsp;iv_docnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;message<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;&nbsp;&nbsp;=&nbsp;mtype.<br/>
&nbsp;&nbsp;IF&nbsp;(&nbsp;mtype&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;AND&nbsp;message&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;)&nbsp;AND&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;message&nbsp;TYPE&nbsp;mtype.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DEQUEUE_EZLTLDOC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docnr&nbsp;=&nbsp;iv_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*初始化日志类.<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;interface&nbsp;=&nbsp;'ZIF_MM057_LTLDOC_SEND_TO_OA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;=&nbsp;|{&nbsp;iv_docnr&nbsp;}|&nbsp;).<br/>
&nbsp;&nbsp;DO&nbsp;1&nbsp;TIMES&nbsp;.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*检查接口是否启用<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'接口未启用.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*排重<br/>
*--------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;check_repeat(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;mtype<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;message&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*处理数据<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_in&nbsp;TYPE&nbsp;zmt_erp_mm057_materialrequest1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_out&nbsp;TYPE&nbsp;zmt_erp_mm057_materialrequest.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;lv_in-mt_erp_mm057_materialrequest_r-message_header&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;ls_msg&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;lv_in-mt_erp_mm057_materialrequest_r-zdata-header&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;ls_header&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;lv_in-mt_erp_mm057_materialrequest_r-zdata-items&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;lt_items&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;lv_out-mt_erp_mm057_materialrequest_r-zdata-header&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;ls_reheader&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-bustyp&nbsp;=&nbsp;'MM057'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-messageid&nbsp;=&nbsp;iv_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;@DATA(ls_header)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zltl_head_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;@iv_docnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_msg&gt;-send_user&nbsp;=&nbsp;ls_header-bpext.<br/>
</div>
<div class="codeComment">
*    &lt;ls_header&gt;-workflowid = '317'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-workflowid&nbsp;=&nbsp;zcl_eas_assist=&gt;get_workflowid(&nbsp;'MM057'&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zdjno&nbsp;=&nbsp;ls_header-docnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zllrq&nbsp;=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcl_assist01=&gt;conv_date_int_to_ext(&nbsp;ls_header-bldat&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zllbm&nbsp;=&nbsp;ls_header-t_kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zywlx&nbsp;=&nbsp;ls_header-yw_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zxmmc&nbsp;=&nbsp;ls_header-t_aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zyongtu&nbsp;=&nbsp;ls_header-bktxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-zlllx&nbsp;=&nbsp;ls_header-lllx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-ztext&nbsp;=&nbsp;ls_header-ll_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-name1&nbsp;=&nbsp;ls_header-t_lifnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_itmes)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zltl_item_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;docnr&nbsp;&nbsp;=&nbsp;@iv_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lines(&nbsp;lt_itmes&nbsp;)&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_header&gt;-idnrk&nbsp;=&nbsp;lt_itmes[&nbsp;1&nbsp;]-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_itmes&nbsp;INTO&nbsp;DATA(ls_item).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;lt_items&gt;&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;BASE&nbsp;&lt;lt_items&gt;&nbsp;(&nbsp;zhxm&nbsp;=&nbsp;ls_item-item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;idnrk&nbsp;=&nbsp;ls_item-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;=&nbsp;ls_item-maktx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;erfme&nbsp;=&nbsp;ls_item-msehl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zllsl&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;ls_item-menge&nbsp;&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgobe&nbsp;=&nbsp;ls_item-lgobe&nbsp;)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当ERP为发送方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lo_proxy)&nbsp;=&nbsp;NEW&nbsp;&nbsp;zco_si_erp_mm057_materialreque(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_proxy-&gt;si_erp_mm057_materialrequest_s(&nbsp;EXPORTING&nbsp;output&nbsp;=&nbsp;lv_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;input&nbsp;=&nbsp;lv_out&nbsp;).<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;&lt;ls_reheader&gt;-header_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;&lt;ls_reheader&gt;-header_status.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;lo_proxy&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;ENDDO.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存到自定义表.<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;mtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;UPDATE&nbsp;ztmm014&nbsp;SET&nbsp;status&nbsp;=&nbsp;'40'&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;iv_docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK&nbsp;AND&nbsp;WAIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'发送OA成功!'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DEQUEUE_EZLTLDOC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docnr&nbsp;=&nbsp;iv_docnr.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;mtype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;message<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;|{&nbsp;iv_docnr&nbsp;}|<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;key3&nbsp;=&nbsp;''&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
&nbsp;&nbsp;IF&nbsp;(&nbsp;mtype&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;AND&nbsp;message&nbsp;IS&nbsp;NOT&nbsp;SUPPLIED&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;message&nbsp;TYPE&nbsp;mtype.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*004   单据被用户&amp;1锁定!<br/>
*005   请维护必输字段:&amp;1!<br/>
*006   请维护行项目数据!<br/>
*007   OK!<br/>
*008   删除领退料单:&amp;1成功!<br/>
*009   删除领退料单&amp;1失败.<br/>
*010   请输入正确的领退料单据号!<br/>
*011   请选择要删除的行!<br/>
*012   请选择要复制的行!<br/>
*013   未查询到相关批次数据!<br/>
*014   单据&amp;1不存在!<br/>
*015   缺少权限:对象ZLTL;工厂:&amp;1;业务类型:&amp;2;活动:&amp;3.<br/>
*016   领退料单&amp;1&amp;2.<br/>
*017   未查询到符合条件的数据!<br/>
*018   查询到数据&amp;1行,请选择更精确的选择条件!<br/>
*019   单据&amp;1保存成功!<br/>
*020   请输入正确的内部订单编号!<br/>
*026   申请总数量不能超过工单需求数量!<br/>
*027   退货数量不能超过已过账数量!<br/>
*028   过账数量不能大于计划领用数量!<br/>
*031   成本中心所属公司与工厂所属公司&amp;1不一致!<br/>
*032   请输入订单类型!<br/>
*033   领退料单据尚未过账,不需要冲销!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>