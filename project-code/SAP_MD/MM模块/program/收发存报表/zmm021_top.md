<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM021_TOP</h2>
<h3> Description: Include ZMM021_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMM021_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES:mchb,mara,marc,mbew,mch1,mkpf,t148,t001l.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;输出表<br/>
</div>
<div class="code">
TYPES:BEGIN OF ty_list,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;mara-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl_txt&nbsp;LIKE&nbsp;t023t-wgbez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-groes,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bismt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-bismt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbzgg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;LIN&nbsp;20211001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zpp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;LIN&nbsp;20211001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zgg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;LIN&nbsp;20211001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;marc-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mard-lgort,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgobe&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t001l-lgobe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-charg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseht&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseht,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msehf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseht,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mskuh-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name_spe&nbsp;&nbsp;TYPE&nbsp;kna1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;imeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"期初数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fimng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"期初数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;imbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"期初金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"采购入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"采购入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"采购入库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"生产入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fsmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"生产入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"生产入库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;snetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;omeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其他入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fomng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其他入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ombtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"其他入库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;onetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;供应商寄售结算<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sceng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"供应商寄售结算数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;scmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"供应商寄售结算数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;scbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"供应商寄售结算金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;scetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"生产领料数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"生产领料数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"生产领料金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"销售出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fvmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"销售出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"销售出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其他出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fnmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其他出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"其他出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;emeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"结存数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;femng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"结存数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;embtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"结存金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;enetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*start&nbsp;add&nbsp;by&nbsp;chenchao&nbsp;20190923<br/>
*&nbsp;&nbsp;转储调拨出库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fdmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"转储调拨出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*转储调拨入库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;frmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"转储调拨入库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;寄售出库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"寄售出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fjmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"寄售出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"寄售出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"寄售退货数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ftmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"寄售退货数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"寄售退货金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tnetp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"销售退货入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ftrmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"销售退货入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"销售退货入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tcmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"采购退货出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ftcmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"采购退货出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tcmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"采购退货出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tcnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ccmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"成本费用出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fccmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"成本费用出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ccmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"成本费用出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ccnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pdmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"盘盈/盘亏数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pdmng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"盘盈/盘亏数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pdmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"盘盈/盘亏金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pdnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;crmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"成本费用人库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcrmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"成本费用人库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;crmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"成本费用人库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;crnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;srmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;&nbsp;"物料收入数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fsrmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;&nbsp;"物料收入数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;srmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;&nbsp;"物料收入金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;srnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;&nbsp;"物料发出数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ffcmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;&nbsp;"物料发出数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;&nbsp;"物料发出金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
***end&nbsp;add<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-lifnr,&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;"供应商名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-hsdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vfdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-vfdat,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;START&nbsp;EDITED&nbsp;BY&nbsp;CHENCHAO&nbsp;20190924<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cspem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"结存冻结库存数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;csmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"金额<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;labst&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"结存冻结库存数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lambtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"金额<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;insme&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"结存冻结库存数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"金额<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;END&nbsp;EDITED<br/>
*&nbsp;ADD&nbsp;BY&nbsp;LIN&nbsp;20211001&nbsp;START<br/>
*&nbsp;形态转换出库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xcmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨出库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fxcmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨出库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xcmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"转储调拨出库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xcnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*形态转换入库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xrmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨入库数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fxrmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"转储调拨入库数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xrmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"转储调拨入库金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xrnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*其它领料数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qlmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其它领料数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fqlmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"其它领料数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qlmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"其它领料金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qlnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*&nbsp;ADD&nbsp;BY&nbsp;LIN&nbsp;20211001&nbsp;END<br/>
*&nbsp;ADD&nbsp;BY&nbsp;LIN&nbsp;20211001&nbsp;START<br/>
*&nbsp;库存初始化<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kcmeng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"库存初始化数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fkcmng&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"库存初始化数量-辅助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kcmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"库存初始化金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kcnetp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
</div>
<div class="codeComment">
*&nbsp;ADD&nbsp;BY&nbsp;LIN&nbsp;20211001&nbsp;END<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ba_alt&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"库存差异调整<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cancel&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_list,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料信息<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_marc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;TYPE&nbsp;marc-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;TYPE&nbsp;marc-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xchpf&nbsp;TYPE&nbsp;marc-xchpf,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;TYPE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;TYPE&nbsp;mara-groes,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;TYPE&nbsp;mara-meins,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseht&nbsp;TYPE&nbsp;t006a-mseht,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bismt&nbsp;TYPE&nbsp;mara-bismt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort&nbsp;TYPE&nbsp;mard-lgort,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umlmc&nbsp;TYPE&nbsp;marc-umlmc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_marc,<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;物料单位换算<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_marm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;TYPE&nbsp;marm-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umrez&nbsp;TYPE&nbsp;marm-umrez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umren&nbsp;TYPE&nbsp;marm-umren,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_marm,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;期初库存<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_mdoc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-mjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-mblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zeile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-zeile,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-sjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smbln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-smbln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smblp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-smblp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfbja&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-lfbja,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfbnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-lfbnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfpos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-lfpos,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-lgort,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-bwart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shkzg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-shkzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-kostl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-charg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vgart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-vgart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-vbeln_im,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjper&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-gjper,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wempf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-wempf,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xauto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-xauto,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-hsdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vfdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-vfdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sobkz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-sobkz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kzbew&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-kzbew,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kzzug&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-kzzug,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kzvbr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-kzvbr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cancl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-cancelled,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ctype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-cancellation_type,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;movet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-reversal_movement,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ztype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;btext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t156t-btext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msehf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseht,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netpr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-hsl,&nbsp;"单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr_sid&nbsp;TYPE&nbsp;matdoc-kunnr_sid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cancel&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_mdoc,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历史库存<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_hist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-lgort,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clabs&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-clabs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cinsm&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-cinsm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cspem&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-cspem,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mchb-charg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsdat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-hsdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vfdat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-vfdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mskuh-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name_spe&nbsp;TYPE&nbsp;kna1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trame&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;march-trame,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_hist,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成本估算号<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_ckml,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kalnr&nbsp;TYPE&nbsp;ckmlhd-kalnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;TYPE&nbsp;ckmlhd-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwkey&nbsp;TYPE&nbsp;ckmlhd-bwkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_ckml,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凭证明细金额<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_mldoc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docref&nbsp;&nbsp;TYPE&nbsp;mldoc-docref,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;curtp&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-curtp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kalnr&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-kalnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jahrper&nbsp;TYPE&nbsp;mldoc-jahrper,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;quant&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-quant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stval&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-stval,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-prd,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;categ&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-categ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ptyp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-ptyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;awref&nbsp;&nbsp;&nbsp;TYPE&nbsp;mldoc-awref,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;awitem&nbsp;&nbsp;TYPE&nbsp;mldoc-awitem,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_mldoc,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商名称<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_lfa1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;TYPE&nbsp;lfa1-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;TYPE&nbsp;lfa1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_lfa1.<br/>
</div>
<div class="codeComment">
*&nbsp;库存差异调整<br/>
<br/>
</div>
<div class="code">
TYPES:BEGIN OF ty_ba_alt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;TYPE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;h_blart&nbsp;TYPE&nbsp;bseg-h_blart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr_s&nbsp;TYPE&nbsp;bseg-dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr_h&nbsp;TYPE&nbsp;bseg-dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_ba_alt.<br/>
<br/>
TYPES:BEGIN OF lty_item,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mark&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCLUDE&nbsp;TYPE&nbsp;ty_mdoc.<br/>
TYPES END OF lty_item.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA:gt_list  TYPE STANDARD TABLE OF ty_list, "输出内表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_marc&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_marc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_mdoc&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_mdoc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_hist&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_hist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_now&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_hist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_ckml&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_ckml,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_lfa1&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_lfa1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_mldoc&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_mldoc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_zt18&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;zmmt0018,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_marm&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_marm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_t156t&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t156t.<br/>
DATA gv_lastm TYPE sydatum.<br/>
DATA:gv_noauth.<br/>
DATA:gv_costauth.<br/>
</div>
<div class="codeComment">
*&nbsp;Define&nbsp;ALV&nbsp;attribute<br/>
<br/>
</div>
<div class="code">
DATA:go_grid TYPE REF TO cl_gui_alv_grid.<br/>
DATA:gs_layout   TYPE lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_fieldcat&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_variant&nbsp;&nbsp;TYPE&nbsp;disvariant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcat&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_fieldcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_exclude&nbsp;&nbsp;TYPE&nbsp;slis_t_extab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_repid&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;syrepid.<br/>
DATA:gt_ba_alt TYPE TABLE OF ty_ba_alt.<br/>
DATA:gt_ba_alt_item TYPE TABLE OF ty_ba_alt.<br/>
DATA:gt_ba_alt_item2 TYPE TABLE OF ty_ba_alt.<br/>
DATA:gv_special,"特殊库存为空标识X<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_display."特殊库存B/K不显示金额数据<br/>
DATA lt_item TYPE TABLE OF lty_item.<br/>
DATA wa_item TYPE lty_item.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;SELECTION-SCREEN<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk_1 WITH FRAME TITLE TEXT-t01.<br/>
&nbsp;&nbsp;PARAMETERS:p_werks&nbsp;TYPE&nbsp;t001l-werks&nbsp;MEMORY&nbsp;ID&nbsp;wrk&nbsp;MODIF&nbsp;ID&nbsp;m3.<br/>
&nbsp;&nbsp;"s_werks&nbsp;FOR&nbsp;marc-werks&nbsp;MODIF&nbsp;ID&nbsp;m3,<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;s_matnr&nbsp;FOR&nbsp;mchb-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lgort&nbsp;FOR&nbsp;t001l-lgort&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matkl&nbsp;FOR&nbsp;mara-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_charg&nbsp;FOR&nbsp;mchb-charg&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_bklas&nbsp;FOR&nbsp;mbew-bklas,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lifnr&nbsp;FOR&nbsp;mch1-lifnr&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_budat&nbsp;FOR&nbsp;mkpf-budat&nbsp;NO-EXTENSION&nbsp;MODIF&nbsp;ID&nbsp;m3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_sobkz&nbsp;FOR&nbsp;t148-sobkz&nbsp;NO-EXTENSION&nbsp;NO-DISPLAY.<br/>
SELECTION-SCREEN END OF BLOCK blk_1.<br/>
SELECTION-SCREEN BEGIN OF BLOCK blk_2 WITH FRAME TITLE TEXT-t02.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;blk_3&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-t03.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:rd_werks&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1&nbsp;DEFAULT&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER-COMMAND&nbsp;usr1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rd_lgort&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;blk_3.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;blk_4&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-t04.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:rd_matnr&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp2&nbsp;DEFAULT&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER-COMMAND&nbsp;usr2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rd_charg&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp2.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;blk_4.<br/>
SELECTION-SCREEN END OF BLOCK blk_2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>