<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM011_TOP</h2>
<h3> Description: Include ZMMIF001_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMIF001_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TYPE-POOLS:SILS,ICON.<br/>
TABLES: EBAN,RLGRAP,SSCRFIELDS.<br/>
CONSTANTS:CON_FRGKZ TYPE EBAN-FRGKZ VALUE 'N'.<br/>
TYPES:BEGIN OF TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BANFN&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-BANFN,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BNFPO&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-BNFPO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJJ1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZEBAN-ZJJ1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZWZLB&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZEBAN-ZWZLB,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZXQLX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZEBAN-ZXQLX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZBM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ZEBAN-ZBM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZCJZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-ERNAM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KNTTP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-KNTTP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KNTTX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T163I-KNTTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EPSTP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-PSTYP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PTEXT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T163Y-PTEXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAKTX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKORG&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-EKORG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKOTX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T024E-EKOTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKGRP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-EKGRP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKNAM&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T024-EKNAM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME1&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;V_T001W-NAME1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EEIND&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-LFDAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKGRP1&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-EKGRP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKNAM1&nbsp;&nbsp;&nbsp;LIKE&nbsp;T024-EKNAM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZKBETR&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-NETPR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ANLN1&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBKN-ANLN1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TXT50&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ANLA-TXT50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJJ(2)&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSQR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-AFNAM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BADAT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-BADAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRGKZ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EBAN-FRGKZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZOAZH&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZEOAZH,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MSG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;BAPI_MSG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PURPS&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZEPURPS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF_ITEMS&nbsp;TYPE&nbsp;ZT_ITEMS.<br/>
TYPES:END OF TY_DATA.<br/>
<br/>
DATA:GT_DATA TYPE TABLE OF TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA&nbsp;TYPE&nbsp;TY_DATA.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;显示用到的全局参数<br/>
*&nbsp;定义FIELDCAT&nbsp;内表和工作区<br/>
</div>
<div class="code">
DATA: GT_FIELDCAT TYPE LVC_T_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_FIELDCAT&nbsp;TYPE&nbsp;LVC_S_FCAT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义布局<br/>
</div>
<div class="code">
DATA: GW_LAYOUT        TYPE LVC_S_LAYO.<br/>
</div>
<div class="codeComment">
*&nbsp;设置字段排序<br/>
</div>
<div class="code">
DATA: GT_SORT TYPE LVC_T_SORT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_SORT&nbsp;TYPE&nbsp;LVC_S_SORT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;对象,ALV回调的时候用，一般不用<br/>
</div>
<div class="code">
DATA: GR_GRID TYPE REF TO CL_GUI_ALV_GRID.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;为FIELDCAT&nbsp;定义宏&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其中&amp;1表示占位符<br/>
*&nbsp;以下含有“是否”的值范围为空和X<br/>
</div>
<div class="code">
DEFINE DE_FIELDCAT.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gw_FIELDCAT.<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.&nbsp;"字段名<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段长描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段中描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段短描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.&nbsp;"主键，蓝底显示，默认冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-NO_ZERO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.&nbsp;"不显示0值<br/>
&nbsp;&nbsp;gw_FIELDCAT-EDIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;"是否编辑<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"参考字段&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_TABLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;"参考表&nbsp;&nbsp;&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIX_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;8.&nbsp;"冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-CONVEXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9.&nbsp;"转换例程<br/>
&nbsp;&nbsp;IF&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;EQ&nbsp;'CK'.<br/>
&nbsp;&nbsp;gw_FIELDCAT-CHECKBOX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'."是否复选框<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;gw_FIELDCAT&nbsp;TO&nbsp;GT_FIELDCAT.<br/>
END-OF-DEFINITION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>