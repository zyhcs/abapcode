<table class="outerTable">
<tr>
<td><h2>Table: ZEBAN</h2>
<h3>Description: 采购申请抬头</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BANFN</td>
<td>2</td>
<td>X</td>
<td>BANFN</td>
<td>BANFN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购申请编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZJJ1</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZEJJ</td>
<td>CFAUSWAHL</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>加急</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZWZLB</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZEWZLB</td>
<td>ZDWZLB</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>物资类别</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZXQLX</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZEXQLX</td>
<td>ZDXQLX</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>需求类型</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZBM</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZEBM</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>7</td>
<td>&nbsp;</td>
<td>部门编码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>PURPS</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZEPURPS</td>
<td>BU_PARTNER</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购员</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>CFAUSWAHL</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>未选择</td>
</tr>
<tr class="cell">
<td>CFAUSWAHL</td>
<td>X</td>
<td>&nbsp;</td>
<td>选择的</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>