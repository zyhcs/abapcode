<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM011_SCR</h2>
<h3> Description: Include ZMMIF001_SCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMIF001_SCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_WERKS&nbsp;LIKE&nbsp;EBAN-WERKS&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;'6000'.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_BANFN&nbsp;FOR&nbsp;EBAN-BANFN,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ERNAM&nbsp;FOR&nbsp;EBAN-ERNAM,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ERDAT&nbsp;FOR&nbsp;EBAN-ERDAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_STATU&nbsp;FOR&nbsp;EBAN-STATU&nbsp;NO-EXTENSION&nbsp;NO&nbsp;INTERVALS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_FRGKZ&nbsp;FOR&nbsp;EBAN-FRGKZ&nbsp;DEFAULT&nbsp;'N'&nbsp;NO-EXTENSION&nbsp;NO&nbsp;INTERVALS&nbsp;MODIF&nbsp;ID&nbsp;A.<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>