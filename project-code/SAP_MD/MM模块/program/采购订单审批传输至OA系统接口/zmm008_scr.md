<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM008_SCR</h2>
<h3> Description: Include ZMMIF001_SCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMIF001_SCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_BUKRS&nbsp;LIKE&nbsp;EKKO-BUKRS&nbsp;OBLIGATORY,&nbsp;"&nbsp;公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_EKORG&nbsp;LIKE&nbsp;EKKO-EKORG&nbsp;OBLIGATORY."&nbsp;DEFAULT&nbsp;'6000'&nbsp;&nbsp;MODIF&nbsp;ID&nbsp;A.&nbsp;"&nbsp;采购组织<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_EBELN&nbsp;FOR&nbsp;EKKO-EBELN,&nbsp;&nbsp;&nbsp;"&nbsp;采购订单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_EKGRP&nbsp;FOR&nbsp;EKKO-EKGRP,&nbsp;&nbsp;&nbsp;"&nbsp;采购组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_LIFNR&nbsp;FOR&nbsp;EKKO-LIFNR,&nbsp;&nbsp;&nbsp;"&nbsp;供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_BEDAT&nbsp;FOR&nbsp;EKPO-CREATIONDATE.&nbsp;&nbsp;&nbsp;"&nbsp;创建日期<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>