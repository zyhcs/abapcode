<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZMM008_TOP</h2>
<h3> Description: Include ZMMIF001_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMMIF001_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TYPE-POOLS:SILS,ICON.<br/>
TABLES: EBAN, EKKO, EKPO, RLGRAP, SSCRFIELDS.<br/>
<br/>
CONSTANTS:CON_FRGKZ TYPE EKKO-FRGKE VALUE 'N'.<br/>
TYPES:BEGIN OF TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EBELN&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-EBELN,&nbsp;&nbsp;&nbsp;"&nbsp;采购凭证编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EBELP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-EBELP,&nbsp;&nbsp;&nbsp;"&nbsp;采购凭证的项目编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIFNR&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-LIFNR,&nbsp;&nbsp;&nbsp;"&nbsp;供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME1&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;LFA1-NAME1,&nbsp;&nbsp;&nbsp;"&nbsp;供应商名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUKRS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-BUKRS,&nbsp;&nbsp;&nbsp;"&nbsp;公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;V_T880-NAME1,&nbsp;"&nbsp;公司代码描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKORG&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-EKORG,&nbsp;&nbsp;&nbsp;"&nbsp;采购组织<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKOTX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T024E-EKOTX,&nbsp;&nbsp;"&nbsp;采购组织描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKGRP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-EKGRP,&nbsp;&nbsp;&nbsp;"&nbsp;采购组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EKNAM&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T024-EKNAM,&nbsp;&nbsp;&nbsp;"&nbsp;采购组描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KNTTP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-KNTTP,&nbsp;&nbsp;&nbsp;"&nbsp;科目分配类别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KNTTX&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T163I-KNTTX,&nbsp;&nbsp;"&nbsp;科目分配类别描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PSTYP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-PSTYP,&nbsp;&nbsp;&nbsp;"&nbsp;项目类别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PTEXT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T163Y-PTEXT,&nbsp;&nbsp;"&nbsp;项目类别描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-MATNR,&nbsp;&nbsp;&nbsp;"&nbsp;物料号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TXZ01&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-TXZ01,&nbsp;&nbsp;&nbsp;"&nbsp;物料描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-MATKL,&nbsp;&nbsp;&nbsp;"&nbsp;物料组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T023T-WGBEZ,&nbsp;&nbsp;"&nbsp;物料组描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-MENGE,&nbsp;&nbsp;&nbsp;"&nbsp;数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-MEINS,&nbsp;&nbsp;&nbsp;"&nbsp;单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EINDT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKET-EINDT,&nbsp;&nbsp;&nbsp;"&nbsp;计划交货日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KBETR&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-KBETR,&nbsp;&nbsp;&nbsp;"&nbsp;含税单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZHSZJ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-KBETR,&nbsp;&nbsp;&nbsp;"&nbsp;含税总价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WAERS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-WAERS,&nbsp;&nbsp;&nbsp;"&nbsp;货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZKBETR&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-KBETR,&nbsp;&nbsp;&nbsp;"&nbsp;税率<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ANLN1&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKN-ANLN1,&nbsp;&nbsp;&nbsp;"&nbsp;资产号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TXT50&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ANLA-TXT50,&nbsp;&nbsp;&nbsp;"&nbsp;资产号描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AFNAM&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-AFNAM,&nbsp;&nbsp;&nbsp;"&nbsp;申请人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEDAT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-CREATIONDATE,&nbsp;&nbsp;&nbsp;"&nbsp;创建日期<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BSTYP&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-BSTYP,&nbsp;&nbsp;&nbsp;"&nbsp;采购类别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BSART&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-BSART,&nbsp;&nbsp;&nbsp;"&nbsp;采购类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BATXT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;V_T161-BATXT,&nbsp;"&nbsp;采购类型描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSJG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-KWERT,&nbsp;"&nbsp;总价格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZJXS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-KWERT,&nbsp;"&nbsp;进项税<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WAERK&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;PRCD_ELEMENTS-WAERK,&nbsp;"&nbsp;销售和分销凭证货币<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZHT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-ZHT,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;采购合同号<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZCGFS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-ZCGFS,&nbsp;&nbsp;&nbsp;"&nbsp;采购方式<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCHPR&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKPO-SCHPR,&nbsp;&nbsp;&nbsp;"&nbsp;估算价格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETPO&nbsp;&nbsp;&nbsp;&nbsp;like&nbsp;ekpo-RETPO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRGKE&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;EKKO-FRGKE,&nbsp;&nbsp;&nbsp;"&nbsp;审批状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MSG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;BAPI_MSG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZCGLX&nbsp;&nbsp;&nbsp;&nbsp;like&nbsp;ekko-ZCGLX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PURPS&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ekko-PURPS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF_ITEMS&nbsp;TYPE&nbsp;ZT_ITEMS.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;项目的组件信息<br/>
TYPES:END OF TY_DATA.<br/>
<br/>
DATA:GT_DATA TYPE TABLE OF TY_DATA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_DATA&nbsp;TYPE&nbsp;TY_DATA.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;显示用到的全局参数<br/>
*&nbsp;定义FIELDCAT&nbsp;内表和工作区<br/>
</div>
<div class="code">
DATA: GT_FIELDCAT TYPE LVC_T_FCAT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_FIELDCAT&nbsp;TYPE&nbsp;LVC_S_FCAT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义布局<br/>
</div>
<div class="code">
DATA: GW_LAYOUT TYPE LVC_S_LAYO.<br/>
</div>
<div class="codeComment">
*&nbsp;设置字段排序<br/>
</div>
<div class="code">
DATA: GT_SORT TYPE LVC_T_SORT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_SORT&nbsp;TYPE&nbsp;LVC_S_SORT.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;对象,ALV回调的时候用，一般不用<br/>
</div>
<div class="code">
DATA: GR_GRID TYPE REF TO CL_GUI_ALV_GRID.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;为FIELDCAT&nbsp;定义宏&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其中&amp;1表示占位符<br/>
*&nbsp;以下含有“是否”的值范围为空和X<br/>
</div>
<div class="code">
DEFINE DE_FIELDCAT.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gw_FIELDCAT.<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.&nbsp;"字段名<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段长描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段中描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-SCRTEXT_S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段短描述<br/>
&nbsp;&nbsp;gw_FIELDCAT-KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.&nbsp;"主键，蓝底显示，默认冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-NO_ZERO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.&nbsp;"不显示0值<br/>
&nbsp;&nbsp;gw_FIELDCAT-EDIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;"是否编辑<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"参考字段&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-REF_TABLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;"参考表&nbsp;&nbsp;&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;gw_FIELDCAT-FIX_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;8.&nbsp;"冻结列<br/>
&nbsp;&nbsp;gw_FIELDCAT-CONVEXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9.&nbsp;"转换例程<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;gw_FIELDCAT-FIELDNAME&nbsp;EQ&nbsp;'CK'.<br/>
*&nbsp;&nbsp;gw_FIELDCAT-CHECKBOX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'."是否复选框<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;gw_FIELDCAT&nbsp;TO&nbsp;GT_FIELDCAT.<br/>
END-OF-DEFINITION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>