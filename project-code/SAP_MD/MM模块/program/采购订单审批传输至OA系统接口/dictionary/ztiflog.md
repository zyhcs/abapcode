<table class="outerTable">
<tr>
<td><h2>Table: ZTIFLOG</h2>
<h3>Description: 接口日志表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>UUID</td>
<td>2</td>
<td>X</td>
<td>SYSUUID_X16</td>
<td>SYSUUID_X16</td>
<td>RAW</td>
<td>16</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 16 Bytes (Raw Format)</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ID</td>
<td>3</td>
<td>&nbsp;</td>
<td>SYSUUID_C32</td>
<td>SYSUUID_C32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 32 Characters (Hexadecimal Encoded)</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>INTERFACE</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>120</td>
<td>&nbsp;</td>
<td>接口名称</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>STIME</td>
<td>5</td>
<td>&nbsp;</td>
<td>TIMESTAMPL</td>
<td>TZNTSTMPL</td>
<td>DEC</td>
<td>21</td>
<td>&nbsp;</td>
<td>长格式的 UTC 时戳 (YYYYMMDDhhmmssmmmuuun)</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ETIME</td>
<td>6</td>
<td>&nbsp;</td>
<td>TIMESTAMPL</td>
<td>TZNTSTMPL</td>
<td>DEC</td>
<td>21</td>
<td>&nbsp;</td>
<td>长格式的 UTC 时戳 (YYYYMMDDhhmmssmmmuuun)</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>KEY1</td>
<td>7</td>
<td>&nbsp;</td>
<td>CHAR50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>注释</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>KEY2</td>
<td>8</td>
<td>&nbsp;</td>
<td>CHAR50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>注释</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>KEY3</td>
<td>9</td>
<td>&nbsp;</td>
<td>CHAR50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>注释</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MTYPE</td>
<td>10</td>
<td>&nbsp;</td>
<td>BAPI_MTYPE</td>
<td>SYCHAR01</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型: S 成功,E 错误,W 警告,I 信息,A 中断</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MSG</td>
<td>11</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td>12</td>
<td>REPUSHABLE</td>
<td>12</td>
<td>&nbsp;</td>
<td>CHAR1</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>单字符标记</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ERNAM</td>
<td>13</td>
<td>&nbsp;</td>
<td>ERNAM</td>
<td>USNAM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>创建对象的人员名称</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ERDAT</td>
<td>14</td>
<td>&nbsp;</td>
<td>ERDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>记录创建日期</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ERZET</td>
<td>15</td>
<td>&nbsp;</td>
<td>ERZET</td>
<td>UZEIT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>输入时间</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>