<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM073_DELIVERY_ORDER_RESP</h2>
<h3> Description: 交货单排车系统接口1</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm073_delivery_order_resp.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(YWRQ) TYPE  ZYWRQ<br/>
*"     VALUE(WERKS) TYPE  WERKS_D<br/>
*"  EXPORTING<br/>
*"     VALUE(VBELN) TYPE  VBELN<br/>
*"     VALUE(ZCPHM) TYPE  ZESDCPHM<br/>
*"     VALUE(ZCYWL) TYPE  ZESDCYWL<br/>
*"     VALUE(ZSTAUS) TYPE  ZSTAUS<br/>
*"     VALUE(ZYWRQ) TYPE  ZYWRQ<br/>
*"     VALUE(ZDYCS) TYPE  ZDYCS<br/>
*"     VALUE(ZWERKS) TYPE  WERKS_D<br/>
*"     VALUE(ZRSEB1) TYPE  ZRSEB<br/>
*"     VALUE(ZRSEB2) TYPE  ZRSEB<br/>
*"     VALUE(ZRSEB3) TYPE  ZRSEB<br/>
*"     VALUE(ZRSEB4) TYPE  ZRSEB<br/>
*"     VALUE(ZRSEB5) TYPE  ZRSEB<br/>
*"  TABLES<br/>
*"      ITEMS STRUCTURE  ZSMM073 OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
*  TYPES: BEGIN OF ls_likp,<br/>
*           vbeln   TYPE likp-vbeln,<br/>
*           zstatus TYPE c LENGTH 2,<br/>
*         END OF ls_likp.<br/>
*<br/>
*  DATA: lt_likp TYPE TABLE OF ls_likp,<br/>
*        lw_likp TYPE ls_likp.<br/>
**--------------------------------------------------------------------*<br/>
**初始化日志类.<br/>
**--------------------------------------------------------------------*<br/>
*  DATA(lo_log) = NEW zcl_if_log( interface = 'ZIF_MM073_DELIVERY_ORDER_RESP'<br/>
*                                         id = |{ wadat }| ).<br/>
*  DO 1 TIMES .<br/>
**--------------------------------------------------------------------*<br/>
**检查接口是否启用<br/>
**--------------------------------------------------------------------*<br/>
*    IF lo_log-&gt;check_active( ) &lt;&gt; 'X'.<br/>
*      message = '接口未启用.'.<br/>
*      mtype = 'E'.<br/>
*      EXIT.<br/>
*    ENDIF.<br/>
**--------------------------------------------------------------------*<br/>
**排重<br/>
**--------------------------------------------------------------------*<br/>
*<br/>
*    lo_log-&gt;check_repeat(<br/>
*      IMPORTING<br/>
*        msgtype = mtype<br/>
*        message = message ).<br/>
*    IF mtype = 'E'.<br/>
*<br/>
*      EXIT.<br/>
*    ENDIF.<br/>
*<br/>
*    SELECT DISTINCT vbeln,<br/>
*           '' AS zstatus<br/>
*      FROM likp<br/>
*     WHERE likp~wadat IN iv_wadat<br/>
*       AND werks = iv_werks<br/>
*      INTO TABLE @lt_likp.<br/>
*    SORT lt_likp BY vbeln.<br/>
*<br/>
*    SELECT DISTINCT vbeln,<br/>
*           zstatus<br/>
*      FROM zmmt020<br/>
*     WHERE wadat IN iv_wadat<br/>
*       AND zstatus IS INITIAL<br/>
*      INTO TABLE @DATA(lt_t020).<br/>
*<br/>
*    LOOP AT lt_t202 INTO lw_t020.<br/>
*      READ TABLE lt_likp WITH KEY vbeln = lw_t020-vbeln INTO DATA(lw_likp) BINARY SEARCH.<br/>
*      "如果上次已传的查询日交货单不存在与本次查询日交货单中，则表示该交货单已在系统删除，需要将删除的交货单及状态传输给CQS<br/>
*      IF sy-subrc &lt;&gt; 0.<br/>
*        lw_likp-vbeln = lw_t020-vbeln.<br/>
*        lw_likp-zstatus = 'L'.<br/>
*        APPEND lw_t020 TO lt_likp.<br/>
*        UPDATE zmmt020 SET zstatus = 'L' WHERE vbeln = lw_t020-vbeln.<br/>
*      ENDIF.<br/>
*      COMMIT WORK.<br/>
*    ENDLOOP.<br/>
*<br/>
*<br/>
*<br/>
*  ENDDO.<br/>
<div class="codeComment">*       <a href="global-zif_mm073_delivery_order_resp.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>