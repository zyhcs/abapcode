<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function CONVERSION_EXIT_LLSTA_OUTPUT</h2>
<h3> Description: Conversion exit for commercial (3-char) measurement unit OUTPUT</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION CONVERSION_EXIT_LLSTA_OUTPUT .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(INPUT)<br/>
*"  EXPORTING<br/>
*"     VALUE(OUTPUT)<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-conversion_exit_llsta_output.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ddtext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;output<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;dd07t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;domname&nbsp;=&nbsp;'ZDLLDSTATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;ddlanguage&nbsp;=&nbsp;sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;as4local&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;domvalue_l&nbsp;=&nbsp;input&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;input.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 领退料平台<br/>
* 002 是否保存领退料单?<br/>
* A01 创建<br/>
* A02 修改<br/>
* A03 显示<br/>
* A04 过账<br/>
* B01 生产订单选择条件<br/>
* B02 维修工单选择条件<br/>
* F01 工厂<br/>
* F02 业务类型<br/>
* F03 领料类型<br/>
* F04 输入生产订单获取行项目<br/>
* F05 主资产号<br/>
* F06 资产次级编号<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*004   单据被用户&amp;1锁定!<br/>
*005   请维护必输字段:&amp;1!<br/>
*006   请维护行项目数据!<br/>
*007   OK!<br/>
*008   删除领退料单:&amp;1成功!<br/>
*009   删除领退料单&amp;1失败.<br/>
*010   请输入正确的领退料单据号!<br/>
*011   请选择要删除的行!<br/>
*012   请选择要复制的行!<br/>
*013   未查询到相关批次数据!<br/>
*014   单据&amp;1不存在!<br/>
*015   缺少权限:对象ZLTL;工厂:&amp;1;业务类型:&amp;2;活动:&amp;3.<br/>
*016   领退料单&amp;1&amp;2.<br/>
*017   未查询到符合条件的数据!<br/>
*018   查询到数据&amp;1行,请选择更精确的选择条件!<br/>
*019   单据&amp;1保存成功!<br/>
*020   请输入正确的内部订单编号!<br/>
*026   申请总数量不能超过工单需求数量!<br/>
*027   退货数量不能超过已过账数量!<br/>
*028   过账数量不能大于计划领用数量!<br/>
*031   成本中心所属公司与工厂所属公司&amp;1不一致!<br/>
*032   请输入订单类型!<br/>
*033   领退料单据尚未过账,不需要冲销!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>