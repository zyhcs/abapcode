<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_POST</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_POST.<br/>
</div>
<div class="codeComment">
*"--------------------------------------------------------------------<br/>
*"*"局部接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(IM_EBELN) TYPE  EBELN<br/>
*"--------------------------------------------------------------------<br/>
<br/>
*  DATA: ls_data LIKE LINE OF gt_data,<br/>
*        lt_data_new TYPE STANDARD TABLE OF mepo_badi_exampl,<br/>
*        lt_data_old TYPE STANDARD TABLE OF mepo_badi_exampl.<br/>
*<br/>
** prepare customers data for posting<br/>
*<br/>
*  CHECK NOT im_ebeln IS INITIAL.<br/>
*<br/>
*  lt_data_new[] = gt_data.<br/>
*  lt_data_old[] = gt_persistent_data.<br/>
*  ls_data-mandt = sy-mandt.<br/>
*  ls_data-ebeln = im_ebeln.<br/>
*  MODIFY lt_data_new FROM ls_data TRANSPORTING mandt ebeln WHERE ebeln IS initial.<br/>
*<br/>
*  CALL FUNCTION 'MEPOBADIEX_COMMIT' IN UPDATE TASK<br/>
*    TABLES<br/>
*      imt_data_new = lt_data_new<br/>
*      imt_data_old = lt_data_old.<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_post.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>