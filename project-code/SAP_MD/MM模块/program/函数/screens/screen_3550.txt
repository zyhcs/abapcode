****************************************************************																																
*   This file was generated by Direct Download Enterprise.     *																																
*   Please do not change it manually.                          *																																
****************************************************************																																
%_DYNPRO																																
SAPLZMMG04																																
3550																																
755																																
                40																																
%_HEADER																																
SAPLZMMG04                              3550I0000DATE  3 83192 37  0  0  3 83  0G 1                              20210615205601																																
%_DESCRIPTION																																
ECC-DIMP: New Confirmation Profile																																
%_FIELDS																																
%#AUTOTEXT001		CHAR	 83	00	00	00	30	00	  1	  2		  0	  0	  0		  3	R				  0	  0	101							DI 产品确认		
MARC-PROFIL	2	CHAR	 15	30	00	04	30	00	  2	  3		  0	  0	  0		  0					  0	  0								反冲参数文件	      DIMP_GENERAL                  D                                                                                                                                                                                                                   X	
MARC-PROFIL	C	CHAR	  4	A0	00	84	00	08	  2	 19		  0	  0	  0		  0					  8	  0								____	X     DIMP_GENERAL                  D  00	
	0	CHAR	 20	80	10	00	00	00	255	  1	O	  0	  0	  0		  0					  0	  0								____________________		
%_FLOWLOGIC																																
* IS2ERP: By DIMP industries owned, retrofitted screen																																
* Collected in piecelist DIMP_DYNP_OWN (software component SAP_APPL)																																
																																
																																
PROCESS BEFORE OUTPUT.																																
*                      Verarbeitung vor der Ausgabe																																
																																
  MODULE INIT_SUB.																																
  MODULE GET_DATEN_SUB.																																
  MODULE FELDAUSWAHL.																																
  MODULE SONDERFAUS.																																
  MODULE SONFAUSW_IN_FGRUPPEN.																																
  MODULE FELDHISTORIE. " Aenderungsdienst																																
  MODULE BILDSTATUS. " Nach Feldauswahl vor Vorlagehand.																																
  MODULE ZUSREF_VORSCHLAGEN_B.																																
  MODULE REFDATEN_VORSCHLAGEN.																																
  MODULE ZUSREF_VORSCHLAGEN_A.																																
  MODULE SET_DATEN_SUB.																																
																																
																																
PROCESS AFTER INPUT.																																
*                      Verarbeitung nach der Eingabe																																
																																
  MODULE GET_DATEN_SUB.																																
  CHAIN.																																
    Field MARC-PROFIL.																																
*       MODULE MARC-PROFIL SWITCH DIMP_GENERAL.																																
* module does not exist																																
  ENDCHAIN.																																
																																
*																																
  MODULE SET_DATEN_SUB.																																
*																																
