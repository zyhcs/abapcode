<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM049_MATERIALREQ</h2>
<h3> Description: 物料主数据同步外围系统</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm049_materialreq.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_MATNR) TYPE  MARA-MATNR<br/>
*"     VALUE(I_MAKT) TYPE  MAKT OPTIONAL<br/>
*"     VALUE(I_MARM) TYPE  MARM OPTIONAL<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_MSG_TYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(E_MSG) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_mm049_materialreq.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:materialreq&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zco_si_erp_mm049_materialdata.<br/>
&nbsp;&nbsp;DATA:lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_erp_mm049_materialdata_req,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header&nbsp;TYPE&nbsp;zdt_erp_mm049_meterialdata_re1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm049_meterialdata_req.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_zdata_header&nbsp;TYPE&nbsp;zdt_erp_mm049_meterialdata_re3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm049_meterialdata_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm049_meterialdata_re2.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_meinh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;marm-meinh,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_umrez&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;marm-umrez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_umren&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;marm-umren,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_lbtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t024x-lbtxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_maktx2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fenzi(10)&nbsp;TYPE&nbsp;p&nbsp;DECIMALS&nbsp;5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mseh3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zkmeins&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm011-zkmeins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_cplb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm008-zcplb.<br/>
&nbsp;&nbsp;DATA:lt_marm&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;marm&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;DATA:i_mara&nbsp;TYPE&nbsp;mara.<br/>
</div>
<div class="codeComment">
* 定义长文本参数"<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;lv_id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;thead-tdid,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"id"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_language&nbsp;LIKE&nbsp;&nbsp;thead-tdspras,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"language"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;thead-tdname,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"name"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_object&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;thead-tdobject,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"object"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_lines&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;tline&nbsp;WITH&nbsp;HEADER&nbsp;LINE.&nbsp;&nbsp;"长文本内容返回结果"<br/>
<br/>
&nbsp;&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_ausp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atinn&nbsp;TYPE&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atwrt&nbsp;TYPE&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_ausp.<br/>
&nbsp;&nbsp;DATA:lt_ausp&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ty_ausp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ausp&nbsp;TYPE&nbsp;ty_ausp.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_zyyyhg&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zbzgg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zsyygys&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcltjj&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdhl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_ztx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhzd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zxd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgssy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcky&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhsj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zfy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zmy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspsx&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspdw&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlzm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhzh&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zptnakla&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zddbzgg&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zzj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgymy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_znyfny&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zklckkl&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zldf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zkhdq&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atwrt,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zyyyhg1&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zbzgg1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zsyygys1&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcltjj1&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdhl1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpp1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_ztx1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdn1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhzd1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zxd1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgg1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgssy1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcky1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdg1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhsj1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zfy1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zmy1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspsx1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspdw1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpz1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlh1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlzm1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcc1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhzh1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zptnakla1&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zddbzgg1&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zzj1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgymy1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhl1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_znyfny1&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zklckkl1&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zldf1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zkhdq1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;ausp-atinn.<br/>
<br/>
</div>
<div class="codeComment">
* 获取UUID<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;lv_uuid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
<br/>
<br/>
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_MM049_MATERIALREQ'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'.<br/>
</div>
<div class="codeComment">
*   发送信息抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-receiver&nbsp;=&nbsp;'ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-bustyp&nbsp;=&nbsp;'MM-049'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_user&nbsp;=&nbsp;sy-uname.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;i_mara&nbsp;FROM&nbsp;mara&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;i_matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_matnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZYYYHG&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zyyyhg1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZBZGG&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zbzgg1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZSYYGYS&nbsp;'&nbsp;CHANGING&nbsp;lv_zsyygys1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZCLTJJ&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zcltjj1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZDHL&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zdhl1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZPP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zpp1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZTX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_ztx1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZDN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zdn1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZHZD&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zhzd1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZXD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zxd1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZGG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zgg1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZGSSY&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zgssy1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZCKY&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zcky1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZDG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zdg1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZHSJ&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zhsj1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZFY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zfy1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZMY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zmy1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZSPSX&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zspsx1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZSPDW&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zspdw1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZPZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zpz1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZLH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zlh1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZLZM&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zlzm1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZCC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zcc1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZQHZH&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zqhzh1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZPTNAKLA'&nbsp;CHANGING&nbsp;lv_zptnakla1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZDDBZGG&nbsp;'&nbsp;CHANGING&nbsp;lv_zddbzgg1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZZJ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zzj1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZGYMY&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zgymy1&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZQHL&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zqhl1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZNYFNY&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_znyfny1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZKLCKKL&nbsp;'&nbsp;CHANGING&nbsp;lv_zklckkl1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZLDF&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zldf1&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_trans_atinn&nbsp;USING&nbsp;'ZKHDQ&nbsp;&nbsp;&nbsp;'&nbsp;CHANGING&nbsp;lv_zkhdq1&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;atinn&nbsp;atwrt&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_ausp&nbsp;FROM&nbsp;ausp&nbsp;WHERE&nbsp;objek&nbsp;=&nbsp;i_mara-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_ausp&nbsp;INTO&nbsp;ls_ausp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zyyyhg1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zyyyhg&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zbzgg1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zbzgg&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zsyygys1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zsyygys&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zcltjj1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcltjj&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zdhl1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdhl&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zpp1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpp&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_ztx1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_ztx&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zdn1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdn&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zhzd1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhzd&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zxd1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zxd&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zgg1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgg&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zgssy1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgssy&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zcky1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcky&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zdg1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zdg&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zhsj1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zhsj&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zfy1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zfy&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zmy1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zmy&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zspsx1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspsx&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zspdw1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zspdw&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zpz1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zpz&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zlh1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlh&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zlzm1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zlzm&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zcc1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zcc&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zqhzh1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhzh&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zptnakla1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zptnakla&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zddbzgg1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zddbzgg&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zzj1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zzj&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zgymy1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zgymy&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zqhl1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zqhl&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_znyfny1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_znyfny&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zklckkl1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zklckkl&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zldf1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zldf&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_ausp-atinn&nbsp;=&nbsp;lv_zkhdq1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zkhdq&nbsp;=&nbsp;ls_ausp-atwrt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;maktx&nbsp;INTO&nbsp;lv_maktx&nbsp;FROM&nbsp;makt&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;i_mara-matnr.<br/>
</div>
<div class="codeComment">
*      SELECT SINGLE lbtxt INTO lv_lbtxt FROM t024x WHERE labor = i_mara-labor AND spras = '1'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zcplb&nbsp;INTO&nbsp;lv_cplb&nbsp;FROM&nbsp;ztmm008&nbsp;WHERE&nbsp;labor&nbsp;=&nbsp;i_mara-labor&nbsp;.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;TABLE&nbsp;lt_marm&nbsp;FROM&nbsp;marm&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;i_mara-matnr.<br/>
<br/>
</div>
<div class="codeComment">
*  补前导零<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_MATN1_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;length_error&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设置参数值"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'GRUN'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_language&nbsp;=&nbsp;sy-langu.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_object&nbsp;&nbsp;&nbsp;=&nbsp;'MATERIAL'.<br/>
<br/>
</div>
<div class="codeComment">
* 获取长文本<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'READ_TEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*         CLIENT                  = SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_id<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_language<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_object<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_lines<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reference_check&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_access_to_archive&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_lines.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;lv_maktx2&nbsp;lt_lines-tdline&nbsp;INTO&nbsp;lv_maktx2&nbsp;SEPARATED&nbsp;BY&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_mara-mstae&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_mara-mstae&nbsp;=&nbsp;'Y'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-guid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-matnr&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bismt&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-bismt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-mtart&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-mtart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-mstae&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-mstae.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-matkl&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-matkl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-spart&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_cplb.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_zyyyhg&nbsp;=&nbsp;'盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-meins&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'TO'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkmeins&nbsp;INTO&nbsp;lv_zkmeins&nbsp;FROM&nbsp;ztmm011&nbsp;WHERE&nbsp;meins&nbsp;=&nbsp;'TO'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-meins&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkmeins&nbsp;INTO&nbsp;lv_zkmeins&nbsp;FROM&nbsp;ztmm011&nbsp;WHERE&nbsp;meins&nbsp;=&nbsp;i_mara-meins&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zkmeins&nbsp;&nbsp;=&nbsp;lv_zkmeins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maktx&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_maktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maktx2&nbsp;&nbsp;&nbsp;=&nbsp;lv_maktx2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-brgew&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-brgew.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-brgew&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ntgew&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-ntgew.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-ntgew&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_zyyyhg&nbsp;=&nbsp;'盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-gewei&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'TO'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-gewei&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-gewei.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-volum&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-volum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-volum&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-voleh&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-voleh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-groes&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-groes.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ean11&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-ean11.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ssfl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-zflbm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-labor&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_cplb.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;&nbsp;&nbsp;=&nbsp;lv_zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zbzgg&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zbzgg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zsyygys&nbsp;&nbsp;=&nbsp;lv_zsyygys.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zcltjj&nbsp;&nbsp;&nbsp;=&nbsp;lv_zcltjj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zdhl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zdhl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zpp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zpp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ztx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_ztx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zdn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zdn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zhzd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zhzd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zxd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zxd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zgg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zgg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zgssy&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zgssy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zcky&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zcky.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zdg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zdg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zhsj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zhsj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zfy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zfy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zmy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zmy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zspsx&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zspsx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zspdw&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zspdw.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zpz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zpz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zlh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zlh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zlzm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zlzm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zcc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zcc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zqhzh&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zqhzh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zptnakla&nbsp;=&nbsp;lv_zptnakla.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zddbzgg&nbsp;&nbsp;=&nbsp;lv_zddbzgg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zzj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zzj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zgymy&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zgymy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zqhl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zqhl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-znyfny&nbsp;&nbsp;&nbsp;=&nbsp;lv_znyfny.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zklckkl&nbsp;&nbsp;=&nbsp;lv_zklckkl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zldf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zldf.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zkhdq&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zkhdq.<br/>
</div>
<div class="codeComment">
* 添加抬头数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-header&nbsp;=&nbsp;ls_zdata_header.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_marm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lt_marm-meinh&nbsp;=&nbsp;i_mara-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_mseh3,lv_fenzi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;mseh3&nbsp;INTO&nbsp;lv_mseh3&nbsp;FROM&nbsp;t006a&nbsp;WHERE&nbsp;msehi&nbsp;=&nbsp;lt_marm-meinh&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_zyyyhg&nbsp;=&nbsp;'盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fenzi&nbsp;=&nbsp;lt_marm-umrez.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fenzi&nbsp;=&nbsp;lv_fenzi&nbsp;/&nbsp;1000&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zumrez&nbsp;&nbsp;&nbsp;=&nbsp;lv_fenzi.<br/>
</div>
<div class="codeComment">
*      ELSEIF ( lt_marm-meinh = 'PC' OR lt_marm-meinh = 'ST' ) AND i_mara-meins = 'KG' .<br/>
*        lv_fenzi = lt_marm-umrez.<br/>
*        lv_fenzi = lv_fenzi / 1000 / lt_marm-umren.<br/>
*        ls_zdata_item-zumrez   = lv_fenzi.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zumrez&nbsp;&nbsp;=&nbsp;lt_marm-umrez&nbsp;/&nbsp;lt_marm-umren.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-zumrez&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-guid&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-matnr&nbsp;&nbsp;&nbsp;=&nbsp;i_mara-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-meinh&nbsp;&nbsp;&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zumren&nbsp;&nbsp;=&nbsp;lv_mseh3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_zdata_item&nbsp;TO&nbsp;lt_zdata_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
* 添加item数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-items&nbsp;=&nbsp;lt_zdata_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_mm049_materialdata_req-message_header&nbsp;=&nbsp;ls_msg_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_mm049_materialdata_req-zdata&nbsp;=&nbsp;ls_zdata.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;materialreq&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;materialreq.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;materialreq-&gt;si_erp_mm049_materialdata_asyn<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'发送成功！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;materialreq&nbsp;).<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;e_msg_type&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;e_msg&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
<br/>
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;i_mara-matnr&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>