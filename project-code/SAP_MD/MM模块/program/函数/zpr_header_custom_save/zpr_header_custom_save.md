<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPR_HEADER_CUSTOM_SAVE</h2>
<h3> Description: 保存采购申请数据</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zpr_header_custom_save.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IM_BANFN) TYPE  BANFN<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zpr_header_custom_save.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;@DATA(ls_custom_header)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zeban<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;banfn&nbsp;=&nbsp;@zeban-banfn.<br/>
&nbsp;&nbsp;IF&nbsp;ls_custom_header-zbm&nbsp;&lt;&gt;&nbsp;zeban-zbm&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zjj1&nbsp;&lt;&gt;&nbsp;zeban-zjj1&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zwzlb&nbsp;&lt;&gt;&nbsp;zeban-zwzlb&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zxqlx&nbsp;&lt;&gt;&nbsp;zeban-zxqlx&nbsp;or<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-purps&nbsp;&lt;&gt;&nbsp;zeban-purps.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;zeban-banfn&nbsp;=&nbsp;im_banfn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zpr_header_custom_save_core="" zpr_header_custom_save_core.html"="">'ZPR_HEADER_CUSTOM_SAVE_CORE'IN&nbsp;UPDATE&nbsp;TASK<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_s_header&nbsp;=&nbsp;zeban.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>