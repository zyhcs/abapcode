<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_GET_FROM_FUNC</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_GET_FROM_FUNC .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  EXPORTING<br/>
*"     REFERENCE(EX_DYNP_DATA) TYPE  CI_EKKODB<br/>
*"----------------------------------------------------------------------<br/>
<br/>
*get dynpro data<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_get_from_func.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;ex_dynp_data&nbsp;=&nbsp;CI_EKKODB.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>