<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMM_GET_NEXT_BATCH</h2>
<h3> Description: 获取批次号</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zmm_get_next_batch.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(MATNR) TYPE  MATNR<br/>
*"     REFERENCE(TIMES) TYPE  I DEFAULT 20<br/>
*"  CHANGING<br/>
*"     REFERENCE(NEW_CHARG)<br/>
*"  EXCEPTIONS<br/>
*"      BLOCKED<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zmm_get_next_batch.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lv_charg&nbsp;TYPE&nbsp;mch1-charg.<br/>
&nbsp;&nbsp;lv_charg&nbsp;=&nbsp;sy-datum+2(6)&nbsp;&amp;&amp;&nbsp;'%'.<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_charg)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;mch1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;@matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;charg&nbsp;LIKE&nbsp;@lv_charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_charg&nbsp;=&nbsp;sy-datum+2(6)&nbsp;&amp;&amp;&nbsp;'0000'.<br/>
&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*排除不规则的含有字母的序列号<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_charg&nbsp;INTO&nbsp;DATA(ls_charg).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;strlen(&nbsp;ls_charg-charg&nbsp;)&nbsp;&lt;&gt;&nbsp;10&nbsp;OR&nbsp;ls_charg-charg&nbsp;&lt;&gt;&nbsp;&nbsp;match(&nbsp;val&nbsp;=&nbsp;ls_charg-charg&nbsp;regex&nbsp;=&nbsp;'/d*'&nbsp;occ&nbsp;=&nbsp;1&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_charg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lt_charg[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_charg&nbsp;=&nbsp;sy-datum+2(6)&nbsp;&amp;&amp;&nbsp;'0000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_charg&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_charg&nbsp;=&nbsp;lt_charg[&nbsp;lines(&nbsp;lt_charg[]&nbsp;)&nbsp;].<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_charg&nbsp;&nbsp;=&nbsp;ls_charg-charg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_n&nbsp;TYPE&nbsp;n&nbsp;LENGTH&nbsp;4.<br/>
&nbsp;&nbsp;lv_n&nbsp;=&nbsp;ls_charg-charg+6(4).<br/>
&nbsp;&nbsp;DO&nbsp;times&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_n&nbsp;=&nbsp;lv_n&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_charg+6(4)&nbsp;=&nbsp;lv_n.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ENQUEUE_EMMCH1E'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*       MODE_MCH1      = 'E'<br/>
*       MANDT          = SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_charg<br/>
</div>
<div class="codeComment">
*       X_MATNR        = ' '<br/>
*       X_CHARG        = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_scope&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'3'<br/>
</div>
<div class="codeComment">
*       _WAIT          = ' '<br/>
*       _COLLECT       = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;foreign_lock&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;system_failure&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new_charg&nbsp;&nbsp;=&nbsp;lv_charg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>