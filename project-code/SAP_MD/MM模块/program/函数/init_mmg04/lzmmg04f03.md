<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04F03</h2>
<h3> Description: 物料主数据：消耗/预测值的表例程I</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0G&nbsp;.&nbsp;&nbsp;"&nbsp;TPROWF_ERWEITERN<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0F&nbsp;.&nbsp;&nbsp;"&nbsp;VERBRAUCH_ERWEITERN<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0E&nbsp;.&nbsp;&nbsp;"&nbsp;OK_CODE_VERBRAUCH<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0D&nbsp;.&nbsp;&nbsp;"&nbsp;OK_CODE_PROGNOSE<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0C&nbsp;.&nbsp;&nbsp;"&nbsp;PRDAT_ERMITTELN<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>