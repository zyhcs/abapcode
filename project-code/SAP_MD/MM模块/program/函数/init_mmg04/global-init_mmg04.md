<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04TOP</h2>
<h3> Description: 初始化</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZMMG04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE-ID&nbsp;m3.<br/>
<br/>
</div>
<div class="codeComment">
*DATA&nbsp;gv_isbatch(1)&nbsp;TYPE&nbsp;c.<br/>
<br/>
<br/>
*TF&nbsp;4.6C&nbsp;Materialfixierung=================================<br/>
</div>
<div class="code">
INCLUDE &lt;icon&gt;.<br/>
</div>
<div class="codeComment">
*TF&nbsp;4.6C&nbsp;Materialfixierung=================================<br/>
<br/>
</div>
<div class="code">
INCLUDE mmmgtrbb.<br/>
INCLUDE mmmgbbau.<br/>
</div>
<div class="codeComment">
*-----------------------------<br/>
</div>
<div class="code">
INCLUDE wstr_definition. "Holds BADI global definition<br/>
<br/>
INCLUDE lmgd1iv0.   "IS2ERP<br/>
<br/>
"{ Begin ENHO /NFM/CA_LMGD1TOP IS-MP-NF /NFM/GENERAL }<br/>
</div>
<div class="codeComment">
*&nbsp;Global&nbsp;Dates&nbsp;nNE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/NFM/<br/>
</div>
<div class="code">
INCLUDE /nfm/global_data.                                        "/NFM/<br/>
</div>
<div class="codeComment">
*&nbsp;Constants&nbsp;NE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/NFM/<br/>
</div>
<div class="code">
INCLUDE /nfm/constants.                                          "/NFM/<br/>
</div>
<div class="codeComment">
*&nbsp;Routines&nbsp;NE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/NFM/<br/>
</div>
<div class="code">
INCLUDE /nfm/mgd1.                                               "/NFM/<br/>
"{ End ENHO /NFM/CA_LMGD1TOP IS-MP-NF /NFM/GENERAL }<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
INCLUDE /cwm/mgd1i01.<br/>
INCLUDE /cwm/mgd1o01.<br/>
<br/>
ENHANCEMENT-POINT lmgd1top_01 SPOTS es_lmgd1top STATIC.<br/>
<br/>
ENHANCEMENT-POINT ehp603_lmgd1top_01 SPOTS es_lmgd1top STATIC .<br/>
<br/>
<br/>
LOAD-OF-PROGRAM.<br/>
&nbsp;&nbsp;IF&nbsp;1&nbsp;=&nbsp;2.&nbsp;ENDIF.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Note&nbsp;2668968<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>