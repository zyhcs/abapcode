<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04F01</h2>
<h3> Description: 物料主数据：描述的表例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
************************************************************************<br/>
*&nbsp;Include&nbsp;LMGD1F01&nbsp;-&nbsp;Formroutinen&nbsp;Kurztexthandling<br/>
************************************************************************<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0B&nbsp;.&nbsp;&nbsp;"&nbsp;MODIF_ZEILE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F0A&nbsp;.&nbsp;&nbsp;"&nbsp;FELDER_ANZEIGEN<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F09&nbsp;.&nbsp;&nbsp;"&nbsp;PRUEFEN_EINTRAG<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F08&nbsp;.&nbsp;&nbsp;"&nbsp;OK_CODE_KTEXT<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F07&nbsp;.&nbsp;&nbsp;"&nbsp;PRUEFEN_DOPEINTRAG<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>