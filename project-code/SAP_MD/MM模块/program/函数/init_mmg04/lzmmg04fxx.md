<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04FXX</h2>
<h3> Description: 物料主数据: 子屏幕的集中 FORM 例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2B&nbsp;.&nbsp;&nbsp;"&nbsp;INIT_BAUSTEIN<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2A&nbsp;.&nbsp;&nbsp;"&nbsp;MAKT_GET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F29&nbsp;.&nbsp;&nbsp;"&nbsp;MARM_GET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F28&nbsp;.&nbsp;&nbsp;"&nbsp;MLAN_GET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F27&nbsp;.&nbsp;&nbsp;"&nbsp;MAKT_SET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F26&nbsp;.&nbsp;&nbsp;"&nbsp;MARM_SET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F25&nbsp;.&nbsp;&nbsp;"&nbsp;MLAN_SET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F24&nbsp;.&nbsp;&nbsp;"&nbsp;ZUSATZDATEN_GET_SUB<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F23&nbsp;.&nbsp;&nbsp;"&nbsp;ZUSATZDATEN_SET_SUB<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>