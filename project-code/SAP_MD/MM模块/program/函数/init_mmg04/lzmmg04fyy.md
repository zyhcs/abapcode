<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04FYY</h2>
<h3> Description: 物料主数据: 主程序/子屏幕的集中例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2E&nbsp;.&nbsp;&nbsp;"&nbsp;MAIN_PARAMETER_GET<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2D&nbsp;.&nbsp;&nbsp;"&nbsp;T130F_LESEN_KOMPLETT<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2C&nbsp;.&nbsp;&nbsp;"&nbsp;EANDATEN_BME<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>