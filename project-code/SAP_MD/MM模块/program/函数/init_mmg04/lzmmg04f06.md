<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04F06</h2>
<h3> Description: 物料主数据：表例程II-消耗/预测值</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1W&nbsp;.&nbsp;&nbsp;"&nbsp;ERSTER_TAG_PERIODE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1V&nbsp;.&nbsp;&nbsp;"&nbsp;VORIGE_PERIODE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1U&nbsp;.&nbsp;&nbsp;"&nbsp;NAECHSTE_PERIODE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1T&nbsp;.&nbsp;&nbsp;"&nbsp;FACTORYDATE_TO_DATE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1S&nbsp;.&nbsp;&nbsp;"&nbsp;DATE_TO_FACTORYDATE_PLUS<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1R&nbsp;.&nbsp;&nbsp;"&nbsp;DATE_TO_FACTORYDATE_MINUS<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1Q&nbsp;.&nbsp;&nbsp;"&nbsp;DATE_COMPUTE_DAY<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1P&nbsp;.&nbsp;&nbsp;"&nbsp;DATUMSAUFBEREITUNG<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>