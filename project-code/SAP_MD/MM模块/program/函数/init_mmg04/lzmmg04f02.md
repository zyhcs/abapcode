<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04F02</h2>
<h3> Description: 物料主数据：税收的表例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
************************************************************************<br/>
*&nbsp;Include&nbsp;LMGD1F01&nbsp;-&nbsp;Formroutinen&nbsp;Steuerabwicklung<br/>
************************************************************************<br/>
<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;OK_CODE_STEUERN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM OK_CODE_STEUERN.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;RMMZU-OKCODE.<br/>
</div>
<div class="codeComment">
*-----&nbsp;Verlassen&nbsp;des&nbsp;Bildes&nbsp;------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_BABA.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;RMMG2-FLGSTEUER.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Zurücksetzen&nbsp;Steuerflag<br/>
</div>
<div class="codeComment">
*-----&nbsp;Erste&nbsp;Seite&nbsp;-&nbsp;Steuern&nbsp;First&nbsp;Page&nbsp;------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_STFP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FIRST_PAGE&nbsp;USING&nbsp;ST_ERSTE_ZEILE.<br/>
</div>
<div class="codeComment">
*-----&nbsp;Seite&nbsp;vor&nbsp;-&nbsp;Steuern&nbsp;Next&nbsp;Page&nbsp;---------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_STNP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;NEXT_PAGE&nbsp;USING&nbsp;ST_ERSTE_ZEILE&nbsp;ST_ZLEPROSEITE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST_LINES.<br/>
</div>
<div class="codeComment">
*-----&nbsp;Seite&nbsp;zurueck&nbsp;-&nbsp;Steuern&nbsp;Previous&nbsp;Page&nbsp;-------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_STPP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;PREV_PAGE&nbsp;USING&nbsp;ST_ERSTE_ZEILE&nbsp;ST_ZLEPROSEITE.<br/>
</div>
<div class="codeComment">
*-----&nbsp;Bottom&nbsp;-&nbsp;Steuern&nbsp;Last&nbsp;Page&nbsp;------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_STLP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;LAST_PAGE&nbsp;USING&nbsp;ST_ERSTE_ZEILE&nbsp;ST_LINES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ST_ZLEPROSEITE&nbsp;SPACE.<br/>
</div>
<div class="codeComment">
*-----&nbsp;SPACE&nbsp;-&nbsp;Enter&nbsp;-------------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;FCODE_SPACE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;RMMG2-FLGSTEUER.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Zurücksetzen&nbsp;Steuerflag<br/>
</div>
<div class="codeComment">
*&nbsp;----&nbsp;Sonstige&nbsp;Funktionen&nbsp;wie&nbsp;Springen&nbsp;etc.&nbsp;--------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;RMMG2-FLGSTEUER.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Zurücksetzen&nbsp;Steuerflag<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
ENDFORM.                    " OK_CODE_STEUERN<br/>
<br/>
<br/>
<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>