<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04F05</h2>
<h3> Description: 物料主数据：EAN的表例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*-------------------------------------------------------------------<br/>
***INCLUDE&nbsp;LMGD1F05&nbsp;.<br/>
*-------------------------------------------------------------------<br/>
<br/>
*---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORM&nbsp;MEAN_ME_TAB_AKT<br/>
*---------------------------------------------------------------------<br/>
*&nbsp;&nbsp;&nbsp;Aktualisieren&nbsp;der&nbsp;int.&nbsp;Tabelle&nbsp;MEAN_ME_TAB&nbsp;wg.&nbsp;Benutzereingabe<br/>
*---------------------------------------------------------------------<br/>
*&nbsp;&nbsp;&nbsp;keine&nbsp;USING-Parameter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1O&nbsp;.&nbsp;&nbsp;"&nbsp;MEAN_ME_TAB_AKT<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1N&nbsp;.&nbsp;&nbsp;"&nbsp;TMLEA_AKT<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1M&nbsp;.&nbsp;&nbsp;"&nbsp;TMLEA_AKT_MEINH<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1L&nbsp;.&nbsp;&nbsp;"&nbsp;OK_CODE_EAN_ZUS<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1K&nbsp;.&nbsp;&nbsp;"&nbsp;EAN_SET_ZEILE<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1J&nbsp;.&nbsp;&nbsp;"&nbsp;EAN_SET_ZEILE_LFEAN<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1I&nbsp;.&nbsp;&nbsp;"&nbsp;SET_UPDATE_TAB<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1H&nbsp;.&nbsp;&nbsp;"&nbsp;DEL_EAN_LIEF<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1G&nbsp;.&nbsp;&nbsp;"&nbsp;DEL_EAN_LIEF_MEINH<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1F&nbsp;.&nbsp;&nbsp;"&nbsp;UPD_EAN_LIEF<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1E&nbsp;.&nbsp;&nbsp;"&nbsp;UPD_EAN_LIEF_MEINH<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1D&nbsp;.&nbsp;&nbsp;"&nbsp;SET_SCREEN_FIELD_VALUE<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Start:&nbsp;EAN.UCC&nbsp;Functionality<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;EAN_UCC_ROUTINES&nbsp;IF&nbsp;FOUND.&nbsp;"&nbsp;EAN.UCC&nbsp;Functionality<br/>
</div>
<div class="codeComment">
*&nbsp;End:<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>