<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG04FSC</h2>
<h3> Description: 物料主数据：屏幕模块的中心滚动例程</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*-------------------------------------------------------------------<br/>
***INCLUDE&nbsp;LMGXXF01&nbsp;.&nbsp;&nbsp;&nbsp;Bl#tterroutinen&nbsp;für&nbsp;Materialstamm<br/>
*-------------------------------------------------------------------<br/>
*&nbsp;&nbsp;Bedeutungen&nbsp;der&nbsp;Parameter&nbsp;:<br/>
*<br/>
*&nbsp;&nbsp;ERSTE_ZEILE&nbsp;&nbsp;:&nbsp;Position&nbsp;des&nbsp;ersten&nbsp;sichtbaren&nbsp;Eintrages&nbsp;-&nbsp;1&nbsp;(!!),<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.h.&nbsp;wenn&nbsp;der&nbsp;erste&nbsp;Eintrag&nbsp;der&nbsp;Tabelle&nbsp;am&nbsp;Anfang&nbsp;zu&nbsp;sehen&nbsp;ist,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ist&nbsp;ERSTE_ZEILE&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;ZLE_PROSEITE&nbsp;:&nbsp;Anzahl&nbsp;der&nbsp;Step-Loop&nbsp;-&nbsp;Zeilen&nbsp;im&nbsp;Dynpro<br/>
*&nbsp;&nbsp;LINES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;Anzahl&nbsp;der&nbsp;Eintr#ge&nbsp;in&nbsp;der&nbsp;internen&nbsp;Tabelle<br/>
*<br/>
*-------------------------------------------------------------------<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F22&nbsp;.&nbsp;&nbsp;"&nbsp;FIRST_PAGE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F21&nbsp;.&nbsp;&nbsp;"&nbsp;PREV_PAGE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F20&nbsp;.&nbsp;&nbsp;"&nbsp;NEXT_PAGE<br/>
<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1Z&nbsp;.&nbsp;&nbsp;"&nbsp;NEXT_PAGE_VW<br/>
</div>
<div class="codeComment">
*&nbsp;AHE:&nbsp;02.06.98&nbsp;-&nbsp;E<br/>
<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1Y&nbsp;.&nbsp;&nbsp;"&nbsp;LAST_PAGE<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F1X&nbsp;.&nbsp;&nbsp;"&nbsp;PARAM_SET<br/>
<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LMGD1F2H.&nbsp;&nbsp;&nbsp;"TC_LONGTEXT_INIT<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>