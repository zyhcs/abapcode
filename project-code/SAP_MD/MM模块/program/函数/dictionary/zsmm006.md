<table class="outerTable">
<tr>
<td><h2>Table: ZSMM006</h2>
<h3>Description: 领退料平台辅助字段</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>CHARG</td>
<td>5</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>CLABS</td>
<td>6</td>
<td>&nbsp;</td>
<td>LABST</td>
<td>MENG13V</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>非限制使用的估价的库存</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ERSDA</td>
<td>3</td>
<td>&nbsp;</td>
<td>ERSDA</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>HSDAT</td>
<td>2</td>
<td>&nbsp;</td>
<td>HSDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>生产日期</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>LGORT</td>
<td>4</td>
<td>&nbsp;</td>
<td>LGORT_D</td>
<td>LGORT</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>存储地点</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>LYSL</td>
<td>7</td>
<td>&nbsp;</td>
<td>LABST</td>
<td>MENG13V</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>非限制使用的估价的库存</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MATNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>UNAME</td>
<td>8</td>
<td>&nbsp;</td>
<td>SYST_UNAME</td>
<td>SYCHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>ABAP 系统字段：当前用户的名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>