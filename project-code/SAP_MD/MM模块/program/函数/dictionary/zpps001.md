<table class="outerTable">
<tr>
<td><h2>Table: ZPPS001</h2>
<h3>Description: l领料单打印参考结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BKTXT</td>
<td>15</td>
<td>&nbsp;</td>
<td>BKTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>凭证抬头文本</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BLDAT</td>
<td>2</td>
<td>&nbsp;</td>
<td>BLDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的凭证日期</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>DOCNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>ZELLDOCNR</td>
<td>ZDLLDOCNR</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>领退料单号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>KTEXT_A</td>
<td>5</td>
<td>&nbsp;</td>
<td>AUFTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>KTEXT_K</td>
<td>3</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>LGOBE</td>
<td>14</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MAKTX</td>
<td>11</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MATNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MEINS</td>
<td>12</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>10</td>
<td>NAME_LAST</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>申请人</td>
</tr>
<tr class="cell">
<td>11</td>
<td>OA_RNAME</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>审批人</td>
</tr>
<tr class="cell">
<td>12</td>
<td>T_LIFNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>NAME1_GP</td>
<td>NAME</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>名称 1</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZLLLX</td>
<td>6</td>
<td>&nbsp;</td>
<td>TEXT</td>
<td>FL_TEXT</td>
<td>CHAR</td>
<td>29</td>
<td>&nbsp;</td>
<td>文本字段</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZLLSL</td>
<td>13</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZXH</td>
<td>9</td>
<td>&nbsp;</td>
<td>INT1</td>
<td>INT1</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>1 字节无符号整数</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZYWLX</td>
<td>4</td>
<td>&nbsp;</td>
<td>TEXT</td>
<td>FL_TEXT</td>
<td>CHAR</td>
<td>29</td>
<td>&nbsp;</td>
<td>文本字段</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZZLTXBH</td>
<td>8</td>
<td>&nbsp;</td>
<td>TEXT30</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>文本 (30 个字符)</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>