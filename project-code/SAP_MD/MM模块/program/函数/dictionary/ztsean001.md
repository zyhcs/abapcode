<table class="outerTable">
<tr>
<td><h2>Table: ZTSEAN001</h2>
<h3>Description: sean测试</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>AGE</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>INT1</td>
<td>3</td>
<td>&nbsp;</td>
<td>年纪</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>NAME</td>
<td>2</td>
<td>X</td>
<td>ZE_NAME</td>
<td>ZD_NAME</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>人员名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>TEL</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_SEANTEL</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>电话号码</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>