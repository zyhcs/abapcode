<table class="outerTable">
<tr>
<td><h2>Table: ZDT_ERP_MM049_METERIALDATA_RE2</h2>
<h3>Description: </h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>CONTROLLER</td>
<td>1</td>
<td>&nbsp;</td>
<td>PRXCTRLTAB</td>
<td>&nbsp;</td>
<td>TTYP</td>
<td>0</td>
<td>&nbsp;</td>
<td>Control Flags for Fields of a Structure</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>GUID</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MATNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MEINH</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZUMREN</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZUMREZ</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>