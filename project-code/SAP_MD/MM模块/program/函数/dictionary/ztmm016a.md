<table class="outerTable">
<tr>
<td><h2>Table: ZTMM016A</h2>
<h3>Description: 领料类型配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BWART</td>
<td>3</td>
<td>&nbsp;</td>
<td>BWART</td>
<td>BWART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>移动类型(库存管理)</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ISTH</td>
<td>5</td>
<td>&nbsp;</td>
<td>FLAG</td>
<td>FLAG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>一般标记</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>LLLX</td>
<td>2</td>
<td>X</td>
<td>ZELLLX</td>
<td>ZDLLLX</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>领料类型</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>TEXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZETLLLX</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>29</td>
<td>&nbsp;</td>
<td>领料类型描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>YWLX</td>
<td>1</td>
<td>X</td>
<td>ZEYWLX</td>
<td>ZDYWLX</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>业务类型</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>X</td>
<td>&nbsp;</td>
<td>标志已设置（事件引发）</td>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>未设置标记</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>