<table class="outerTable">
<tr>
<td><h2>Table: ZTMM008</h2>
<h3>Description: SAP与EAS产品类别明细映射表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>LABOR</td>
<td>2</td>
<td>X</td>
<td>LABOR</td>
<td>LABOR</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>实验室/设计室</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>LBTXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>LBTXT</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>实验室/工程办公室的描述</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZCPLB</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZE_CPLB</td>
<td>ZD_CPLB</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>EAS产品类别明细</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>