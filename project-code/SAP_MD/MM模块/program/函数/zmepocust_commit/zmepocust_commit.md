<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_COMMIT</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_COMMIT.<br/>
</div>
<div class="codeComment">
*"--------------------------------------------------------------------<br/>
*"*"更新函数模块：<br/>
*"<br/>
*"*"局部接口：<br/>
*"  TABLES<br/>
*"      IMT_DATA_NEW STRUCTURE  MEPO_BADI_EXAMPL<br/>
*"      IMT_DATA_OLD STRUCTURE  MEPO_BADI_EXAMPL<br/>
*"--------------------------------------------------------------------<br/>
<br/>
*  DATA: ls_data_new      LIKE LINE OF gt_data,<br/>
*        ls_data_old      LIKE LINE OF gt_data,<br/>
*        data_ins         TYPE STANDARD TABLE OF mepo_badi_exampl,<br/>
*        data_upd         TYPE STANDARD TABLE OF mepo_badi_exampl,<br/>
*        data_del         TYPE STANDARD TABLE OF mepo_badi_exampl.<br/>
*<br/>
** new state<br/>
*  LOOP AT imt_data_new INTO ls_data_new.<br/>
*    READ TABLE imt_data_old INTO ls_data_old WITH KEY<br/>
*                                           mandt = sy-mandt<br/>
*                                           ebeln = ls_data_new-ebeln<br/>
*                                           ebelp = ls_data_new-ebelp.<br/>
*    IF sy-subrc IS INITIAL.<br/>
*      DELETE imt_data_old INDEX sy-tabix.<br/>
*      IF ls_data_new NE ls_data_old.<br/>
** existing entry was changed<br/>
*        APPEND ls_data_new TO data_upd.<br/>
*      ENDIF.<br/>
*    ELSE.<br/>
** a new entry was added<br/>
*      APPEND ls_data_new TO data_ins.<br/>
*    ENDIF.<br/>
*  ENDLOOP.<br/>
*<br/>
** remaining old state: can be deleted<br/>
*  APPEND LINES OF imt_data_old TO data_del.<br/>
*<br/>
**---------------------------------------------------------------------*<br/>
** actual update operations<br/>
**---------------------------------------------------------------------*<br/>
*<br/>
** insert<br/>
*  IF NOT data_ins[] IS INITIAL.<br/>
*    INSERT mepo_badi_exampl FROM TABLE data_ins.<br/>
*    IF sy-subrc NE 0.<br/>
*      MESSAGE a807(me) WITH 'MEPO_BADI_EXAMPL'.<br/>
*    ENDIF.<br/>
*  ENDIF.<br/>
** update<br/>
*  IF NOT data_upd[] IS INITIAL.<br/>
*    UPDATE mepo_badi_exampl FROM TABLE data_upd.<br/>
*    IF sy-subrc NE 0.<br/>
*      MESSAGE a808(me) WITH 'MEPO_BADI_EXAMPL'.<br/>
*    ENDIF.<br/>
*  ENDIF.<br/>
** delete<br/>
*  IF NOT data_del[] IS INITIAL.<br/>
*    DELETE mepo_badi_exampl FROM TABLE data_del.<br/>
*    IF sy-subrc NE 0.<br/>
*      MESSAGE a809(me) WITH 'MEPO_BADI_EXAMPL'.<br/>
*    ENDIF.<br/>
*  ENDIF.<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_commit.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>