<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_GET_ABLAD</h2>
<h3> Description: 获取卸货点</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZFM_GET_ABLAD.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  EXPORTING<br/>
*"     REFERENCE(EV_ABLAD) TYPE  ABLAD<br/>
*"     REFERENCE(EV_WEMPF) TYPE  WEMPF<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zfm_get_ablad.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;EV_ABLAD&nbsp;=&nbsp;GV_ABLAD.<br/>
&nbsp;&nbsp;EV_WEMPF&nbsp;=&nbsp;GV_WEMPF.<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>