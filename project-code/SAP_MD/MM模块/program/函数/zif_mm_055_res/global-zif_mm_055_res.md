<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG008TOP</h2>
<h3> Description: 供应商采购订单预付状态回传接口(EAS→SAP)</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZMMG008.                      "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZMMG008D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>