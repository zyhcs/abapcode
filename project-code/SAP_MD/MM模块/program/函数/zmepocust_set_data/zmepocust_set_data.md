<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_SET_DATA</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_SET_DATA.<br/>
</div>
<div class="codeComment">
*"--------------------------------------------------------------------<br/>
*"*"局部接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IM_DATA) TYPE  MEPO_BADI_EXAMPL<br/>
*"     REFERENCE(IM_PHYSICAL_DELETE_REQUEST) TYPE  MMPUR_BOOL<br/>
*"         OPTIONAL<br/>
*"--------------------------------------------------------------------<br/>
<br/>
* update customers data<br/>
<br/>
*  DATA: ls_data LIKE LINE OF gt_data.<br/>
*<br/>
*  FIELD-SYMBOLS: &lt;data&gt; LIKE LINE OF gt_data.<br/>
*<br/>
*  CHECK NOT im_data-ebelp IS INITIAL.<br/>
*<br/>
*  IF NOT im_physical_delete_request IS INITIAL.<br/>
** delete a line from gt_data<br/>
*    DELETE TABLE gt_data WITH TABLE KEY mandt = sy-mandt<br/>
*                                        ebeln = im_data-ebeln<br/>
*                                        ebelp = im_data-ebelp.<br/>
*  ELSE.<br/>
** update customer data<br/>
*    READ TABLE gt_data ASSIGNING &lt;data&gt; WITH TABLE KEY<br/>
*                                        mandt = sy-mandt<br/>
*                                        ebeln = im_data-ebeln<br/>
*                                        ebelp = im_data-ebelp.<br/>
*    IF sy-subrc IS INITIAL.<br/>
** update existing data<br/>
*      &lt;data&gt;-badi_bsgru = im_data-badi_bsgru.<br/>
*      &lt;data&gt;-badi_afnam = im_data-badi_afnam.<br/>
*    ELSE.<br/>
** make a new entry into the data table<br/>
*      ls_data = im_data.<br/>
*      ls_data-mandt = sy-mandt.<br/>
*      INSERT ls_data INTO TABLE gt_data.<br/>
*    ENDIF.<br/>
*  ENDIF.<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_set_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>