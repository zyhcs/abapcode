<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG001TOP</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZMMG001.                   "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;persistent&nbsp;data<br/>
<br/>
*&nbsp;dynpro&nbsp;output&nbsp;structure<br/>
</div>
<div class="code">
TABLES: CI_EKKODB.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;definitions&nbsp;required&nbsp;for&nbsp;dynpro/framework&nbsp;integration<br/>
</div>
<div class="code">
DATA: ok-code TYPE sy-ucomm.<br/>
INCLUDE lmeviewsf01.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>