<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPR_OPEN</h2>
<h3> Description: 初始化</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zpr_open.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IO_HEADER) TYPE REF TO  IF_PURCHASE_REQUISITION<br/>
*"     VALUE(I_TRTYP) TYPE  TRTYP OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zpr_open.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;ls_header&nbsp;TYPE&nbsp;mereq_header.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_count&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;CLEAR&nbsp;zeban.<br/>
&nbsp;&nbsp;CLEAR&nbsp;g_original_changed_flag.<br/>
&nbsp;&nbsp;IF&nbsp;io_header&nbsp;IS&nbsp;BOUND.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;go_header&nbsp;=&nbsp;io_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header&nbsp;=&nbsp;go_header-&gt;get_data(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(&nbsp;*&nbsp;)&nbsp;INTO&nbsp;l_count&nbsp;FROM&nbsp;eban<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;banfn&nbsp;=&nbsp;ls_header-banfn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;l_count&nbsp;&gt;=&nbsp;1.<br/>
</div>
<div class="codeComment">
*THE PROCESSED PR IS EXISTED IN DB<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;FROM&nbsp;zeban<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;banfn&nbsp;=&nbsp;ls_header-banfn.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>