<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_INTERFACE_00001421</h2>
<h3> Description: Interface Description for Event 00001421 (Vendor)</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zfm_interface_00001421.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_LFA1) LIKE  LFA1 STRUCTURE  LFA1 OPTIONAL<br/>
*"     VALUE(I_LFA1_OLD) LIKE  LFA1 STRUCTURE  LFA1 OPTIONAL<br/>
*"     VALUE(I_LFB1) LIKE  LFB1 STRUCTURE  LFB1 OPTIONAL<br/>
*"     VALUE(I_LFB1_OLD) LIKE  LFB1 STRUCTURE  LFB1 OPTIONAL<br/>
*"     VALUE(I_LFM1) LIKE  LFM1 STRUCTURE  LFM1 OPTIONAL<br/>
*"     VALUE(I_LFM1_OLD) LIKE  LFM1 STRUCTURE  LFM1 OPTIONAL<br/>
*"     VALUE(UPD_LFA1) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFAS) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFAT) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFB1) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFB5) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFBK) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFBK_IBAN) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFBW) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFEI) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFLR) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFM1) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFM2) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_LFZA) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_WYT1) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_WYT1T) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(UPD_WYT3) LIKE  CDPOS-CHNGIND OPTIONAL<br/>
*"     VALUE(I_ADD_ON_DATA) LIKE  VEND_ADD_ON_DATA STRUCTURE<br/>
*"        VEND_ADD_ON_DATA OPTIONAL<br/>
*"  TABLES<br/>
*"      T_XLFAS STRUCTURE  FLFAS OPTIONAL<br/>
*"      T_YLFAS STRUCTURE  FLFAS OPTIONAL<br/>
*"      T_XLFAT STRUCTURE  FLFAT OPTIONAL<br/>
*"      T_YLFAT STRUCTURE  FLFAT OPTIONAL<br/>
*"      T_XLFB5 STRUCTURE  FLFB5 OPTIONAL<br/>
*"      T_YLFB5 STRUCTURE  FLFB5 OPTIONAL<br/>
*"      T_XLFBK STRUCTURE  FLFBK OPTIONAL<br/>
*"      T_YLFBK STRUCTURE  FLFBK OPTIONAL<br/>
*"      T_XLFBK_IBAN STRUCTURE  FLFBK_IBAN OPTIONAL<br/>
*"      T_YLFBK_IBAN STRUCTURE  FLFBK_IBAN OPTIONAL<br/>
*"      T_XLFBW STRUCTURE  FLFBW OPTIONAL<br/>
*"      T_YLFBW STRUCTURE  FLFBW OPTIONAL<br/>
*"      T_XLFEI STRUCTURE  FLFEI OPTIONAL<br/>
*"      T_YLFEI STRUCTURE  FLFEI OPTIONAL<br/>
*"      T_XLFLR STRUCTURE  FLFLR OPTIONAL<br/>
*"      T_YLFLR STRUCTURE  FLFLR OPTIONAL<br/>
*"      T_XLFM2 STRUCTURE  FLFM2 OPTIONAL<br/>
*"      T_YLFM2 STRUCTURE  FLFM2 OPTIONAL<br/>
*"      T_XLFZA STRUCTURE  FLFZA OPTIONAL<br/>
*"      T_YLFZA STRUCTURE  FLFZA OPTIONAL<br/>
*"      T_XWYT1 STRUCTURE  FWYT1 OPTIONAL<br/>
*"      T_YWYT1 STRUCTURE  FWYT1 OPTIONAL<br/>
*"      T_XWYT1T STRUCTURE  FWYT1T OPTIONAL<br/>
*"      T_YWYT1T STRUCTURE  FWYT1T OPTIONAL<br/>
*"      T_XWYT3 STRUCTURE  FWYT3 OPTIONAL<br/>
*"      T_YWYT3 STRUCTURE  FWYT3 OPTIONAL<br/>
*"      T_UPD_TXT STRUCTURE  FLIFTXT OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zfm_interface_00001421.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;i_lfa1-lifnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zif_mm050_suppliersave="" zif_mm050_suppliersave.html"="">'ZIF_MM050_SUPPLIERSAVE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_lifnr&nbsp;=&nbsp;i_lfa1-lifnr.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFUNCTION.<br/>
</a&nbsp;href&nbsp;="..></div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>