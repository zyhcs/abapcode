<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_DELETE_LTLDOC</h2>
<h3> Description: 删除领退料单</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zfm_delete_ltldoc.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(DOCNR) TYPE  ZELLDOCNR<br/>
*"  EXPORTING<br/>
*"     REFERENCE(MTYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(MESSAGE) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zfm_delete_ltldoc.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zfm_lock_ltldoc="" zfm_lock_ltldoc.html"="">'ZFM_LOCK_LTLDOC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docnr&nbsp;&nbsp;&nbsp;=&nbsp;docnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;message<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;&nbsp;&nbsp;=&nbsp;mtype.<br/>
<br/>
&nbsp;&nbsp;&nbsp;CHECK&nbsp;mtype&nbsp;=&nbsp;'S'.<br/>
<br/>
&nbsp;&nbsp;UPDATE&nbsp;ztmm014&nbsp;SET&nbsp;status&nbsp;=&nbsp;'30'&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;docnr.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK&nbsp;AND&nbsp;WAIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s008&nbsp;WITH&nbsp;docnr&nbsp;INTO&nbsp;message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s009&nbsp;WITH&nbsp;docnr&nbsp;INTO&nbsp;message.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 领退料平台<br/>
* 002 是否保存领退料单?<br/>
* A01 创建<br/>
* A02 修改<br/>
* A03 显示<br/>
* A04 过账<br/>
* B01 生产订单选择条件<br/>
* B02 维修工单选择条件<br/>
* F01 工厂<br/>
* F02 业务类型<br/>
* F03 领料类型<br/>
* F04 输入生产订单获取行项目<br/>
* F05 主资产号<br/>
* F06 资产次级编号<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*004   单据被用户&amp;1锁定!<br/>
*005   请维护必输字段:&amp;1!<br/>
*006   请维护行项目数据!<br/>
*007   OK!<br/>
*008   删除领退料单:&amp;1成功!<br/>
*009   删除领退料单&amp;1失败.<br/>
*010   请输入正确的领退料单据号!<br/>
*011   请选择要删除的行!<br/>
*012   请选择要复制的行!<br/>
*013   未查询到相关批次数据!<br/>
*014   单据&amp;1不存在!<br/>
*015   缺少权限:对象ZLTL;工厂:&amp;1;业务类型:&amp;2;活动:&amp;3.<br/>
*016   领退料单&amp;1&amp;2.<br/>
*017   未查询到符合条件的数据!<br/>
*018   查询到数据&amp;1行,请选择更精确的选择条件!<br/>
*019   单据&amp;1保存成功!<br/>
*020   请输入正确的内部订单编号!<br/>
*026   申请总数量不能超过工单需求数量!<br/>
*027   退货数量不能超过已过账数量!<br/>
*028   过账数量不能大于计划领用数量!<br/>
*031   成本中心所属公司与工厂所属公司&amp;1不一致!<br/>
*032   请输入订单类型!<br/>
*033   领退料单据尚未过账,不需要冲销!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>