<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM054_SUPCUSTDATA_RETURN</h2>
<h3> Description: 客户供应商回传接口</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm054_supcustdata_return.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(PARTNER) TYPE  BU_PARTNER<br/>
*"     VALUE(LIFNR) TYPE  STRING OPTIONAL<br/>
*"     VALUE(KUNNR) TYPE  STRING OPTIONAL<br/>
*"     VALUE(GUID) TYPE  SYSUUID_C32 OPTIONAL<br/>
*"  EXPORTING<br/>
*"     REFERENCE(MSGTYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(MESSAGE) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
*--------------------------------------------------------------------*<br/>
*初始化日志类.<br/>
*--------------------------------------------------------------------*<br/>
<div class="codeComment">*       <a href="global-zif_mm054_supcustdata_return.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;|{&nbsp;guid&nbsp;}|<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_MM054_SUPCUSTDATA_RETURN'&nbsp;).<br/>
&nbsp;&nbsp;DO&nbsp;1&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;partner&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'业务伙伴编号不能为空.'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;kunnr&nbsp;IS&nbsp;INITIAL&nbsp;AND&nbsp;lifnr&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'外部客户编码和外部供应商编码不能同时为空.'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;partner&nbsp;=&nbsp;|{&nbsp;partner&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;partner.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'业务伙伴编号不存在.'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ENQUEUE_EBU_PARTNR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*       MODE_BUT000    = 'E'<br/>
*       CLIENT         = SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;partner<br/>
</div>
<div class="codeComment">
*       X_PARTNER      = ' '<br/>
*       _SCOPE         = '2'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_wait&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*       _COLLECT       = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;foreign_lock&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;system_failure&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|业务伙伴编号被用户{&nbsp;sy-msgv1&nbsp;}锁定.|&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当接口出现错误时,获取函数传入传出参数,以便重推<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;paras&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;get_func_paras(&nbsp;IMPORTING&nbsp;paras&nbsp;=&nbsp;DATA(lt_paras)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_paras&nbsp;&nbsp;ASSIGNING&nbsp;&nbsp;FIELD-SYMBOL(&lt;ls_paras&gt;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;(&lt;ls_paras&gt;-parameter)&nbsp;TO&nbsp;&lt;paras&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_paras&gt;-value&nbsp;=&nbsp;REF&nbsp;#(&nbsp;&lt;paras&gt;&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_func_msg(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;paras&nbsp;&nbsp;=&nbsp;lt_paras<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repush&nbsp;=&nbsp;'X'&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;kunnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPDATE&nbsp;bp001&nbsp;SET&nbsp;unw_remark&nbsp;=&nbsp;kunnr&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;partner.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lifnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPDATE&nbsp;but000&nbsp;SET&nbsp;bpext&nbsp;=&nbsp;lifnr&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;partner.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK&nbsp;AND&nbsp;WAIT.<br/>
&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;IF&nbsp;msgtype&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当ERP为接收方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lo_log-&gt;set_proxy_receiver_msgid(&nbsp;)&nbsp;.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;|{&nbsp;msgtype&nbsp;}|<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;|{&nbsp;message&nbsp;}|<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;|{&nbsp;partner&nbsp;}|<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;|{&nbsp;kunnr&nbsp;}|<br/>
&nbsp;&nbsp;key3&nbsp;=&nbsp;|{&nbsp;lifnr&nbsp;}|&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>