<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function F4IF_SHLP_EXIT_WZLB</h2>
<h3> Description: Beispiel für ein Suchhilfe-Exit eine Suchhilfe</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION f4if_shlp_exit_wzlb.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  TABLES<br/>
*"      SHLP_TAB TYPE  SHLP_DESCT<br/>
*"      RECORD_TAB STRUCTURE  SEAHLPRES<br/>
*"  CHANGING<br/>
*"     VALUE(SHLP) TYPE  SHLP_DESCR<br/>
*"     VALUE(CALLCONTROL) LIKE  DDSHF4CTRL STRUCTURE  DDSHF4CTRL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
* EXIT immediately, if you do not want to handle this step<br/>
<div class="codeComment">*       <a href="global-f4if_shlp_exit_wzlb.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;callcontrol-step&nbsp;&lt;&gt;&nbsp;'SELONE'&nbsp;AND<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;callcontrol-step&nbsp;&lt;&gt;&nbsp;'SELECT'&nbsp;AND<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;AND&nbsp;SO&nbsp;ON<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;callcontrol-step&nbsp;&lt;&gt;&nbsp;'DISP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
* STEP SELONE  (Select one of the elementary searchhelps)<br/>
*"----------------------------------------------------------------------<br/>
* This step is only called for collective searchhelps. It may be used<br/>
* to reduce the amount of elementary searchhelps given in SHLP_TAB.<br/>
* The compound searchhelp is given in SHLP.<br/>
* If you do not change CALLCONTROL-STEP, the next step is the<br/>
* dialog, to select one of the elementary searchhelps.<br/>
* If you want to skip this dialog, you have to return the selected<br/>
* elementary searchhelp in SHLP and to change CALLCONTROL-STEP to<br/>
* either to 'PRESEL' or to 'SELECT'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;callcontrol-step&nbsp;=&nbsp;'SELONE'.<br/>
</div>
<div class="codeComment">
*   PERFORM SELONE .........<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
* STEP PRESEL  (Enter selection conditions)<br/>
*"----------------------------------------------------------------------<br/>
* This step allows you, to influence the selection conditions either<br/>
* before they are displayed or in order to skip the dialog completely.<br/>
* If you want to skip the dialog, you should change CALLCONTROL-STEP<br/>
* to 'SELECT'.<br/>
* Normaly only SHLP-SELOPT should be changed in this step.<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;callcontrol-step&nbsp;=&nbsp;'PRESEL'.<br/>
</div>
<div class="codeComment">
*   PERFORM PRESEL ..........<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
* STEP SELECT    (Select values)<br/>
*"----------------------------------------------------------------------<br/>
* This step may be used to overtake the data selection completely.<br/>
* To skip the standard seletion, you should return 'DISP' as following<br/>
* step in CALLCONTROL-STEP.<br/>
* Normally RECORD_TAB should be filled after this step.<br/>
* Standard function module F4UT_RESULTS_MAP may be very helpfull in this<br/>
* step.<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;callcontrol-step&nbsp;=&nbsp;'SELECT'.<br/>
</div>
<div class="codeComment">
*   PERFORM STEP_SELECT TABLES RECORD_TAB SHLP_TAB<br/>
*                       CHANGING SHLP CALLCONTROL RC.<br/>
*   IF RC = 0.<br/>
*     CALLCONTROL-STEP = 'DISP'.<br/>
*   ELSE.<br/>
*     CALLCONTROL-STEP = 'EXIT'.<br/>
*   ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.&nbsp;"Don't&nbsp;process&nbsp;STEP&nbsp;DISP&nbsp;additionally&nbsp;in&nbsp;this&nbsp;call.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
* STEP DISP     (Display values)<br/>
*"----------------------------------------------------------------------<br/>
* This step is called, before the selected data is displayed.<br/>
* You can e.g. modify or reduce the data in RECORD_TAB<br/>
* according to the users authority.<br/>
* If you want to get the standard display dialog afterwards, you<br/>
* should not change CALLCONTROL-STEP.<br/>
* If you want to overtake the dialog on you own, you must return<br/>
* the following values in CALLCONTROL-STEP:<br/>
* - "RETURN" if one line was selected. The selected line must be<br/>
*   the only record left in RECORD_TAB. The corresponding fields of<br/>
*   this line are entered into the screen.<br/>
* - "EXIT" if the values request should be aborted<br/>
* - "PRESEL" if you want to return to the selection dialog<br/>
* Standard function modules F4UT_PARAMETER_VALUE_GET and<br/>
* F4UT_PARAMETER_RESULTS_PUT may be very helpfull in this step.<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;callcontrol-step&nbsp;=&nbsp;'DISP'.<br/>
</div>
<div class="codeComment">
*   PERFORM AUTHORITY_CHECK TABLES RECORD_TAB SHLP_TAB<br/>
*                           CHANGING SHLP CALLCONTROL.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lt_auth_values&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;us335.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_AUTH_VALUES'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object1&nbsp;=&nbsp;'M_BANF_WRK'<br/>
</div>
<div class="codeComment">
*       OBJECT2 = ' '<br/>
*       OBJECT3 = ' '<br/>
*       OBJECT4 = ' '<br/>
*       OBJECT5 = ' '<br/>
*       OBJECT6 = ' '<br/>
*       OBJECT7 = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-uname<br/>
</div>
<div class="codeComment">
*       TCODE   = SY-TCODE<br/>
*       OPTIMIZE                =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;values&nbsp;&nbsp;=&nbsp;lt_auth_values<br/>
</div>
<div class="codeComment">
*   EXCEPTIONS<br/>
*       USER_DOESNT_EXIST       = 1<br/>
*       OTHERS  = 2<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lt_r_werks&nbsp;TYPE&nbsp;RANGE&nbsp;OF&nbsp;werks_d&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_auth_values&nbsp;INTO&nbsp;DATA(ls_auth_value)&nbsp;WHERE&nbsp;field&nbsp;=&nbsp;'WERKS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_auth_value-highval&nbsp;=&nbsp;'*'&nbsp;OR&nbsp;ls_auth_value-lowval&nbsp;=&nbsp;'*'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_r_werks[].<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_r_werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_r_werks-low&nbsp;=&nbsp;ls_auth_value-lowval.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_r_werks-high&nbsp;=&nbsp;ls_auth_value-highval.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_r_werks-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lt_r_werks-low&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;lt_r_werks-high&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_r_werks-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_r_werks-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_r_werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;record_tab[]&nbsp;INTO&nbsp;DATA(ls_record)&nbsp;WHERE&nbsp;string+3(4)&nbsp;NOT&nbsp;IN&nbsp;lt_r_werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;record_tab[].<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>