<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_GET_DATA</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_GET_DATA.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IM_EBELN) TYPE  EBELN<br/>
*"     REFERENCE(IM_EBELP) TYPE  EBELP<br/>
*"  EXPORTING<br/>
*"     VALUE(EX_DATA) TYPE  MEPO_BADI_EXAMPL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
*  CLEAR ex_data.<br/>
*<br/>
*  CHECK NOT im_ebelp IS INITIAL.<br/>
*<br/>
*  READ TABLE gt_data INTO ex_data WITH TABLE KEY mandt = sy-mandt<br/>
*                                                 ebeln = im_ebeln<br/>
*                                                 ebelp = im_ebelp.<br/>
*  IF NOT sy-subrc IS INITIAL.<br/>
*    ex_data-mandt = sy-mandt.<br/>
*    ex_data-ebeln = im_ebeln.<br/>
*    ex_data-ebelp = im_ebelp.<br/>
*    INSERT ex_data INTO TABLE gt_data.<br/>
*  ENDIF.<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_get_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>