<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPR_EXPORT_CUSTOM_FIELDS</h2>
<h3> Description: 输出自定义字段</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zpr_export_custom_fields.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_ZEBAN) TYPE  ZEBAN<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zpr_export_custom_fields.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;e_zeban&nbsp;=&nbsp;zeban.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>