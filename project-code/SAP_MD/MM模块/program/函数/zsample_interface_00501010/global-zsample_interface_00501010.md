<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFG_ZTSEAN01TOP</h2>
<h3> Description: Schnittstellenbeschreibung zum Event 00501010</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&nbsp;regenerated&nbsp;at&nbsp;18.05.2021&nbsp;10:06:27<br/>
</div>
<div class="code">
FUNCTION-POOL ZFG_ZTSEAN01               MESSAGE-ID SV.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZFG_ZTSEAN01D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
<div class="code">
&nbsp;&nbsp;INCLUDE&nbsp;LSVIMDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;"general&nbsp;data&nbsp;decl.<br/>
&nbsp;&nbsp;INCLUDE&nbsp;LZFG_ZTSEAN01T00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;"view&nbsp;rel.&nbsp;data&nbsp;dcl.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>