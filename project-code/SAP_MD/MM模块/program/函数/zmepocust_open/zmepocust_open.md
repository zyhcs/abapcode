<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZMEPOCUST_OPEN</h2>
<h3> Description: </h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZMEPOCUST_OPEN.<br/>
</div>
<div class="codeComment">
*"--------------------------------------------------------------------<br/>
*"*"局部接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IM_EBELN) TYPE  EBELN<br/>
*"--------------------------------------------------------------------<br/>
<br/>
* read customer data from database<br/>
<br/>
*  CHECK NOT im_ebeln IS INITIAL.<br/>
*<br/>
*  SELECT * FROM mepo_badi_exampl INTO TABLE gt_persistent_data<br/>
*                                 WHERE ebeln = im_ebeln.<br/>
*<br/>
*  gt_data = gt_persistent_data.<br/>
<br/>
<div class="codeComment">*       <a href="global-zmepocust_open.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>