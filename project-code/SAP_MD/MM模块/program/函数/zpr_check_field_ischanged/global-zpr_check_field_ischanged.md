<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZMMG009TOP</h2>
<h3> Description: 检查事务类型</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zmmg009.                      "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZMMG009D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
<div class="code">
TABLES: zeban.<br/>
DATA ok-code TYPE sy-ucomm.<br/>
DATA g_original_changed_flag TYPE flag.<br/>
DATA g_trtyp TYPE trtyp. "H = Create, V = Change, A = Display<br/>
DATA go_header TYPE REF TO if_purchase_requisition.<br/>
DATA: gs_document        TYPE mepo_document,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_tcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sy-tcode,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_uncomplete	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mmpur_bool,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_parking_allowed&nbsp;TYPE&nbsp;mmpur_bool,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_changed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mmpur_bool,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_holding_allowed&nbsp;TYPE&nbsp;mmpur_bool,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_uncompletetype	&nbsp;TYPE&nbsp;memorytype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_parkhold_active&nbsp;TYPE&nbsp;mmpur_bool.<br/>
<br/>
<br/>
TYPE-POOLS mmmfd.<br/>
DATA: processed_model TYPE REF TO if_model_mm.<br/>
INCLUDE lmeviewsf01.<br/>
include <a href="zlocal_class.html">zlocal_class</a>.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>