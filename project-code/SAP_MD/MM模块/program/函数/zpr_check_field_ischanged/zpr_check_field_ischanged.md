<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPR_CHECK_FIELD_ISCHANGED</h2>
<h3> Description: 检查事务类型</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zpr_check_field_ischanged.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  EXPORTING<br/>
*"     REFERENCE(CH_CHANGED) TYPE  FLAG<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zpr_check_field_ischanged.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;@DATA(ls_custom_header)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zeban<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;banfn&nbsp;=&nbsp;@zeban-banfn.<br/>
&nbsp;&nbsp;IF&nbsp;ls_custom_header-zbm&nbsp;&lt;&gt;&nbsp;zeban-zbm&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zjj1&nbsp;&lt;&gt;&nbsp;zeban-zjj1&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zwzlb&nbsp;&lt;&gt;&nbsp;zeban-zwzlb&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-zxqlx&nbsp;&lt;&gt;&nbsp;zeban-zxqlx&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_custom_header-purps&nbsp;&lt;&gt;&nbsp;zeban-purps.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ch_changed&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;go_header&nbsp;IS&nbsp;BOUND.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA(lt_items)&nbsp;=&nbsp;go_header-&gt;get_items(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;lt_items&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_items&nbsp;INTO&nbsp;DATA(ls_item).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-item-&gt;if_dcm_adapter~set_changed(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>