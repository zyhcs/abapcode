<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM056_MATERIALQUERY</h2>
<h3> Description: OA获取ERP物料主数据信息</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZIF_MM056_MATERIALQUERY.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(INPUT) TYPE  ZMT_ERP_MM056_MATERIALQUERY_R1<br/>
*"  EXPORTING<br/>
*"     VALUE(OUTPUT) TYPE  ZMT_ERP_MM056_MATERIALQUERY_RE<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_mm056_materialquery.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;DATA:ls_header_req&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm056_materialquery_r6,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041b_inv_apl_back_r2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header&nbsp;TYPE&nbsp;zdt_erp_mm056_materialquery_r5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_items&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm056_materialquer_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm056_materialquery_r4.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lt_matnr&nbsp;TYPE&nbsp;RANGE&nbsp;OF&nbsp;mara-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_matnr&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lt_maktx&nbsp;TYPE&nbsp;RANGE&nbsp;OF&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_maktx&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_maktx.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lv_matnr&nbsp;TYPE&nbsp;mara-matnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'SI_ERP_MM056_MATERIALQUERY_SYN'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_req&nbsp;=&nbsp;input-mt_erp_mm056_materialquery_req-zdata-header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_matnr&nbsp;=&nbsp;|{&nbsp;ls_header_req-matnr&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header_req-vkorg&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'没有任何查询条件！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header_req-matnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_matnr-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_matnr-option&nbsp;=&nbsp;'CP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_matnr-low&nbsp;=&nbsp;ls_header_req-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_matnr&nbsp;TO&nbsp;lt_matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header_req-maktx&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_maktx-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_maktx-option&nbsp;=&nbsp;'CP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_maktx-low&nbsp;=&nbsp;ls_header_req-maktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_maktx&nbsp;TO&nbsp;lt_maktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;a~matnr,a~mtart,a~mstae,a~meins,b~maktx&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_data)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;mara&nbsp;AS&nbsp;a&nbsp;INNER&nbsp;JOIN&nbsp;makt&nbsp;AS&nbsp;b&nbsp;ON&nbsp;a~matnr&nbsp;=&nbsp;b~matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;mvke&nbsp;AS&nbsp;c&nbsp;ON&nbsp;a~matnr&nbsp;=&nbsp;c~matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;c~vkorg&nbsp;=&nbsp;@ls_header_req-vkorg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;a~matnr&nbsp;IN&nbsp;@lt_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;b~maktx&nbsp;IN&nbsp;@lt_maktx.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;ADJACENT&nbsp;DUPLICATES&nbsp;FROM&nbsp;lt_data&nbsp;COMPARING&nbsp;ALL&nbsp;FIELDS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lv_lines)&nbsp;=&nbsp;lines(&nbsp;lt_data&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_lines&nbsp;&gt;&nbsp;'1000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'本次查询结果大于1000条，请维护精确的查询条件！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;lv_lines&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'查询无数据！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;lv_lines&nbsp;&gt;&nbsp;'0'&nbsp;AND&nbsp;lv_lines&nbsp;&lt;=&nbsp;'1000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'查询成功！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zzhs&nbsp;=&nbsp;lv_lines.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_data&nbsp;INTO&nbsp;DATA(ls_data).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-matnr&nbsp;=&nbsp;&nbsp;ls_data-matnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-mtart&nbsp;=&nbsp;&nbsp;ls_data-mtart&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-mstae&nbsp;=&nbsp;&nbsp;ls_data-mstae&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-meins&nbsp;=&nbsp;&nbsp;ls_data-meins&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-maktx&nbsp;=&nbsp;&nbsp;ls_data-maktx&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_zdata_item&nbsp;TO&nbsp;ls_zdata_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-bustyp&nbsp;=&nbsp;'MM056'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-message_status&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-message_text&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_res-send_user&nbsp;=&nbsp;sy-uname.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output-mt_erp_mm056_materialquery_res-message_header_rs&nbsp;=&nbsp;ls_header_res.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output-mt_erp_mm056_materialquery_res-zdata-header&nbsp;=&nbsp;ls_zdata_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output-mt_erp_mm056_materialquery_res-zdata-items&nbsp;=&nbsp;ls_zdata_items.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当ERP为接收方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_proxy_receiver_msgid(&nbsp;)&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;&nbsp;&nbsp;key2&nbsp;=&nbsp;lv_matnr&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>