<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_SPLIT_BATCH</h2>
<h3> Description: 批次拆分</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zfm_split_batch.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(MATNR) TYPE  MATNR<br/>
*"     REFERENCE(WERKS) TYPE  WERKS_D<br/>
*"     REFERENCE(LGORT) TYPE  LGORT_D OPTIONAL<br/>
*"     REFERENCE(KUNNR) TYPE  KUNNR OPTIONAL<br/>
*"     REFERENCE(MEINS) TYPE  ERFME<br/>
*"  TABLES<br/>
*"      ET_DATA STRUCTURE  ZSMM006<br/>
*"  CHANGING<br/>
*"     REFERENCE(C_MENGE) TYPE  MENGE_D<br/>
*"  EXCEPTIONS<br/>
*"      NO_FIND<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zfm_split_batch.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;gv_string&nbsp;=&nbsp;c_menge.<br/>
&nbsp;&nbsp;CHECK&nbsp;c_menge&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;CONDENSE&nbsp;gv_string.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_r_lgort&nbsp;TYPE&nbsp;RANGE&nbsp;OF&nbsp;lgort_d.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_zsmm006.<br/>
&nbsp;&nbsp;IF&nbsp;lgort&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_r_lgort&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;(&nbsp;&nbsp;sign&nbsp;=&nbsp;'I'&nbsp;option&nbsp;=&nbsp;'EQ'&nbsp;low&nbsp;=&nbsp;lgort&nbsp;)&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;kunnr&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~hsdat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~ersda<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mchb~charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clabs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;gt_zsmm006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;mchb<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;mch1&nbsp;ON&nbsp;mchb~matnr&nbsp;=&nbsp;mch1~matnr&nbsp;AND&nbsp;mchb~charg&nbsp;=&nbsp;mch1~charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;mchb~matnr&nbsp;=&nbsp;matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;werks&nbsp;=&nbsp;werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;lgort&nbsp;IN&nbsp;lt_r_lgort<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;clabs&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~hsdat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mch1~ersda<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgort<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mcsd~charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sdlab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;gt_zsmm006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;mcsd<br/>
&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;mch1&nbsp;ON&nbsp;mcsd~matnr&nbsp;=&nbsp;mch1~matnr&nbsp;AND&nbsp;mcsd~charg&nbsp;=&nbsp;mch1~charg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;mcsd~matnr&nbsp;=&nbsp;matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;werks&nbsp;=&nbsp;werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;lgort&nbsp;IN&nbsp;lt_r_lgort<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;kunnr&nbsp;=&nbsp;kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;sdlab&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gt_zsmm006&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e013&nbsp;&nbsp;RAISING&nbsp;no_find.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;SORT&nbsp;gt_zsmm006&nbsp;BY&nbsp;hsdat&nbsp;ersda&nbsp;charg&nbsp;.<br/>
&nbsp;&nbsp;DATA(lv_menge)&nbsp;=&nbsp;c_menge.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_zsmm006&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_zsmm006&gt;).<br/>
</div>
<div class="codeComment">
*    CALL FUNCTION 'ENQUEUE_EMMCH1E'<br/>
*      EXPORTING<br/>
*        mode_mch1      = 'E'<br/>
**       MANDT          = SY-MANDT<br/>
*        matnr          = matnr<br/>
*        charg          = &lt;ls_zsmm006&gt;-charg<br/>
**       X_MATNR        = ' '<br/>
**       X_CHARG        = ' '<br/>
**       _SCOPE         = '2'<br/>
**       _WAIT          = ' '<br/>
**       _COLLECT       = ' '<br/>
*      EXCEPTIONS<br/>
*        foreign_lock   = 1<br/>
*        system_failure = 2<br/>
*        OTHERS         = 3.<br/>
*    IF sy-subrc &lt;&gt; 0.<br/>
*      &lt;ls_zsmm006&gt;-uname = sy-msgv1.<br/>
*      CONTINUE.<br/>
*    ENDIF.<br/>
*--------------------------------------------------------------------*<br/>
*转换单位<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_zsmm006&gt;-clabs&nbsp;=&nbsp;zcl_assist01=&gt;exchang_main_meins(&nbsp;EXPORTING&nbsp;matnr&nbsp;&nbsp;=&nbsp;&lt;ls_zsmm006&gt;-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins_in&nbsp;&nbsp;=&nbsp;meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_in&nbsp;&nbsp;=&nbsp;&lt;ls_zsmm006&gt;-clabs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kzmeinh	&nbsp;=&nbsp;''&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;lv_menge&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_menge&nbsp;=&nbsp;lv_menge&nbsp;-&nbsp;&lt;ls_zsmm006&gt;-clabs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_menge&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_zsmm006&gt;-lysl&nbsp;=&nbsp;&lt;ls_zsmm006&gt;-clabs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_zsmm006&gt;-lysl&nbsp;=&nbsp;&lt;ls_zsmm006&gt;-clabs&nbsp;+&nbsp;lv_menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_sum&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_zsmm006.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_zsmm006&nbsp;INTO&nbsp;DATA(ls_data).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;LAST.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_sum&nbsp;=&nbsp;ls_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;gt_zsmm006&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;BASE&nbsp;gt_zsmm006&nbsp;(&nbsp;lgort&nbsp;=&nbsp;'SUM'&nbsp;clabs&nbsp;=&nbsp;ls_sum-clabs&nbsp;lysl&nbsp;=&nbsp;ls_sum-lysl&nbsp;)&nbsp;).<br/>
&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;9500&nbsp;STARTING&nbsp;AT&nbsp;30&nbsp;5&nbsp;ENDING&nbsp;AT&nbsp;120&nbsp;15.<br/>
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gt_zsmm006&nbsp;TO&nbsp;et_data[].<br/>
&nbsp;&nbsp;DELETE&nbsp;et_data&nbsp;WHERE&nbsp;lysl&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;lgort&nbsp;=&nbsp;'SUM'.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;et_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;FIRST.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_menge&nbsp;=&nbsp;c_menge&nbsp;-&nbsp;et_data-lysl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 领退料平台<br/>
* 002 是否保存领退料单?<br/>
* A01 创建<br/>
* A02 修改<br/>
* A03 显示<br/>
* A04 过账<br/>
* B01 生产订单选择条件<br/>
* B02 维修工单选择条件<br/>
* F01 工厂<br/>
* F02 业务类型<br/>
* F03 领料类型<br/>
* F04 输入生产订单获取行项目<br/>
* F05 主资产号<br/>
* F06 资产次级编号<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*004   单据被用户&amp;1锁定!<br/>
*005   请维护必输字段:&amp;1!<br/>
*006   请维护行项目数据!<br/>
*007   OK!<br/>
*008   删除领退料单:&amp;1成功!<br/>
*009   删除领退料单&amp;1失败.<br/>
*010   请输入正确的领退料单据号!<br/>
*011   请选择要删除的行!<br/>
*012   请选择要复制的行!<br/>
*013   未查询到相关批次数据!<br/>
*014   单据&amp;1不存在!<br/>
*015   缺少权限:对象ZLTL;工厂:&amp;1;业务类型:&amp;2;活动:&amp;3.<br/>
*016   领退料单&amp;1&amp;2.<br/>
*017   未查询到符合条件的数据!<br/>
*018   查询到数据&amp;1行,请选择更精确的选择条件!<br/>
*019   单据&amp;1保存成功!<br/>
*020   请输入正确的内部订单编号!<br/>
*026   申请总数量不能超过工单需求数量!<br/>
*027   退货数量不能超过已过账数量!<br/>
*028   过账数量不能大于计划领用数量!<br/>
*031   成本中心所属公司与工厂所属公司&amp;1不一致!<br/>
*032   请输入订单类型!<br/>
*033   领退料单据尚未过账,不需要冲销!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>