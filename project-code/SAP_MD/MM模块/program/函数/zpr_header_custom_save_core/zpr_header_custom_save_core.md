<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPR_HEADER_CUSTOM_SAVE_CORE</h2>
<h3> Description: 保存数据</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZPR_HEADER_CUSTOM_SAVE_CORE.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"更新函数模块：<br/>
*"<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(IM_S_HEADER) TYPE  ZEBAN<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zpr_header_custom_save_core.html">Global data declarations</a></div><br/>
</div>
<div class="code">
MODIFY zeban FROM IM_S_HEADER.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*024   请检查抬头自定义字段提交人对应的外部企业伙伴编号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>