<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_POST_LTLDOC</h2>
<h3> Description: 过账领退料单</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zfm_post_ltldoc.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(HEADER) TYPE  ZTMM014<br/>
*"     VALUE(COMMIT) TYPE  FLAG OPTIONAL<br/>
*"  EXPORTING<br/>
*"     REFERENCE(MTYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(MESSAGE) TYPE  BAPI_MSG<br/>
*"     REFERENCE(MBLNR) TYPE  MBLNR<br/>
*"     REFERENCE(MJAHR) TYPE  MJAHR<br/>
*"  TABLES<br/>
*"      ITEMS STRUCTURE  ZTMM015<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zfm_post_ltldoc.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;ls_head&nbsp;&nbsp;TYPE&nbsp;bapi2017_gm_head_01,&nbsp;"BAPI抬头表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_code&nbsp;&nbsp;TYPE&nbsp;bapi2017_gm_code,&nbsp;"MIGO类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mblnr&nbsp;TYPE&nbsp;bapi2017_gm_head_ret-mat_doc,&nbsp;"生成物料凭证<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_year&nbsp;&nbsp;TYPE&nbsp;bapi2017_gm_head_ret-doc_year.&nbsp;"年度<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapi2017_gm_item_create,&nbsp;"BAPI行项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_item,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_extensionin&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiparex&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_serialnumber&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapi2017_gm_serialnumber,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_serialnumber&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lt_serialnumber.<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_return&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiret2.&nbsp;"BAPI返回消息<br/>
<br/>
&nbsp;&nbsp;ls_head-header_txt&nbsp;=&nbsp;header-bktxt.&nbsp;"抬头文本<br/>
&nbsp;&nbsp;ls_head-pstng_date&nbsp;=&nbsp;header-budat.&nbsp;"过账日期<br/>
&nbsp;&nbsp;ls_head-doc_date&nbsp;=&nbsp;sy-datum.&nbsp;"凭证日期<br/>
&nbsp;&nbsp;ls_head-pr_uname&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;ls_head-ref_doc_no&nbsp;=&nbsp;ztmm014-docnr.<br/>
&nbsp;&nbsp;IF&nbsp;header-ywlx&nbsp;CP&nbsp;'P*'.<br/>
</div>
<div class="codeComment">
*    IF header-isth = 'X'.<br/>
*      ls_code-gm_code = '05'.<br/>
*    ELSE.<br/>
*      ls_code-gm_code = '03'.<br/>
*    ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_code-gm_code&nbsp;=&nbsp;'03'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;header-ywlx&nbsp;CP&nbsp;'C*'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_code-gm_code&nbsp;=&nbsp;'03'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_code-gm_code&nbsp;=&nbsp;'03'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;items&nbsp;WHERE&nbsp;act_menge&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;header-ywlx&nbsp;CP&nbsp;'P*'&nbsp;OR&nbsp;header-ywlx(1)&nbsp;=&nbsp;'W'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;header-isth&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-reserv_no&nbsp;=&nbsp;items-rsnum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-res_item&nbsp;=&nbsp;items-rspos.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-orderid&nbsp;=&nbsp;items-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-costcenter&nbsp;=&nbsp;header-kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-orderid&nbsp;=&nbsp;header-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-asset_no&nbsp;=&nbsp;header-anln1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-sub_number&nbsp;=&nbsp;header-anln2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-move_type&nbsp;=&nbsp;items-bwart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-material_long&nbsp;&nbsp;=&nbsp;items-matnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;物料号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;header-werks.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-stge_loc&nbsp;&nbsp;=&nbsp;items-lgort.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;库存地点<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-entry_qnt&nbsp;=&nbsp;items-act_menge.&nbsp;&nbsp;"&nbsp;数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-entry_uom&nbsp;=&nbsp;items-meins.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-batch&nbsp;=&nbsp;&nbsp;items-charg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_item-customer&nbsp;=&nbsp;items-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_item-customer&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-spec_stock&nbsp;=&nbsp;'B'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_item&nbsp;TO&nbsp;lt_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_item.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_GOODSMVT_CREATE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;goodsmvt_header&nbsp;&nbsp;=&nbsp;ls_head<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;goodsmvt_code&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_code<br/>
</div>
<div class="codeComment">
*     TESTRUN          = ' '<br/>
*     GOODSMVT_REF_EWM =<br/>
*     GOODSMVT_PRINT_CTRL           =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
</div>
<div class="codeComment">
*     GOODSMVT_HEADRET =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;materialdocument&nbsp;=&nbsp;mblnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matdocumentyear&nbsp;&nbsp;=&nbsp;mjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;goodsmvt_item&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_item<br/>
</div>
<div class="codeComment">
*     GOODSMVT_SERIALNUMBER         =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
</div>
<div class="codeComment">
*     GOODSMVT_SERV_PART_DATA       =<br/>
*     EXTENSIONIN      =<br/>
*     GOODSMVT_ITEM_CWM             =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_msg&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_return&nbsp;INTO&nbsp;DATA(ls_return)&nbsp;WHERE&nbsp;type&nbsp;CA&nbsp;'AEX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;&nbsp;ls_return-id&nbsp;TYPE&nbsp;ls_return-type&nbsp;NUMBER&nbsp;ls_return-number&nbsp;WITH<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_return-message_v1&nbsp;ls_return-message_v2&nbsp;ls_return-message_v3&nbsp;ls_return-message_v4&nbsp;INTO&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;message&nbsp;lv_msg&nbsp;INTO&nbsp;message.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'<br/>
</div>
<div class="codeComment">
*      IMPORTING<br/>
*        RETURN        =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;mtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|过账成功,物料凭证:{&nbsp;mblnr&nbsp;}.|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header-status&nbsp;=&nbsp;'20'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header-mblnr&nbsp;=&nbsp;mblnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header-mjahr&nbsp;=&nbsp;mjahr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;header-zz_crt_dat&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_crt_dat&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_crt_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_crt_usr&nbsp;=&nbsp;sy-uname..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_upd_dat&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_upd_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header-zz_upd_usr&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;ztmm014&nbsp;FROM&nbsp;header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;FROM&nbsp;ztmm015&nbsp;WHERE&nbsp;docnr&nbsp;=&nbsp;header-docnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INSERT&nbsp;ztmm015&nbsp;FROM&nbsp;TABLE&nbsp;items[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*      IMPORTING<br/>
*       RETURN        =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;UPDATE&nbsp;matdoc&nbsp;SET&nbsp;zbanci&nbsp;=&nbsp;header-zbanci<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzhibie&nbsp;=&nbsp;header-zzhibie<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zpdate&nbsp;=&nbsp;header-bldat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;mblnr&nbsp;=&nbsp;mblnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;mjahr&nbsp;=&nbsp;mjahr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 领退料平台<br/>
* 002 是否保存领退料单?<br/>
* A01 创建<br/>
* A02 修改<br/>
* A03 显示<br/>
* A04 过账<br/>
* B01 生产订单选择条件<br/>
* B02 维修工单选择条件<br/>
* F01 工厂<br/>
* F02 业务类型<br/>
* F03 领料类型<br/>
* F04 输入生产订单获取行项目<br/>
* F05 主资产号<br/>
* F06 资产次级编号<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   下拉框出错，请联系管理员！<br/>
*<br/>
* Message class: ZMM001<br/>
*004   单据被用户&amp;1锁定!<br/>
*005   请维护必输字段:&amp;1!<br/>
*006   请维护行项目数据!<br/>
*007   OK!<br/>
*008   删除领退料单:&amp;1成功!<br/>
*009   删除领退料单&amp;1失败.<br/>
*010   请输入正确的领退料单据号!<br/>
*011   请选择要删除的行!<br/>
*012   请选择要复制的行!<br/>
*013   未查询到相关批次数据!<br/>
*014   单据&amp;1不存在!<br/>
*015   缺少权限:对象ZLTL;工厂:&amp;1;业务类型:&amp;2;活动:&amp;3.<br/>
*016   领退料单&amp;1&amp;2.<br/>
*017   未查询到符合条件的数据!<br/>
*018   查询到数据&amp;1行,请选择更精确的选择条件!<br/>
*019   单据&amp;1保存成功!<br/>
*020   请输入正确的内部订单编号!<br/>
*026   申请总数量不能超过工单需求数量!<br/>
*027   退货数量不能超过已过账数量!<br/>
*028   过账数量不能大于计划领用数量!<br/>
*031   成本中心所属公司与工厂所属公司&amp;1不一致!<br/>
*032   请输入订单类型!<br/>
*033   领退料单据尚未过账,不需要冲销!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>