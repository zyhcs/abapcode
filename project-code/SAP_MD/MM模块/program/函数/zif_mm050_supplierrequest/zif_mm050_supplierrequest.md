<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM050_SUPPLIERREQUEST</h2>
<h3> Description: 供应商主数据推送外围系统</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm050_supplierrequest.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_LIFNR) LIKE  LFA1-LIFNR<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_MSG_TYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(E_MSG) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_mm050_supplierrequest.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:supplierreq&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zco_si_erp_mm050_supplierdata.<br/>
&nbsp;&nbsp;DATA:lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_erp_mm050_supplierdata_req,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header&nbsp;TYPE&nbsp;zdt_erp_mm050_supplierdata_re1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm050_supplierdata_req.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_zdata_header&nbsp;TYPE&nbsp;zdt_erp_mm050_supplierdata_re3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm050_supplierdata_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_mm050_supplierdata_re2.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_bpext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;but000-bpext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bpkind&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;but000-bpkind,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_group_feature&nbsp;TYPE&nbsp;bp001-group_feature,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_smtp_addr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;adr6-smtp_addr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;adrct-remark,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_landx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t005t-landx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztfico001-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bezei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t005u-bezei.<br/>
&nbsp;&nbsp;DATA:i_lfa1&nbsp;TYPE&nbsp;lfa1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_lfbk&nbsp;TYPE&nbsp;lfbk.<br/>
</div>
<div class="codeComment">
* 获取UUID<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;lv_uuid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_MM050_SUPPLIERREQUEST'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'.<br/>
<br/>
</div>
<div class="codeComment">
* 发送信息抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-receiver&nbsp;=&nbsp;'EAS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-bustyp&nbsp;=&nbsp;'MM-050'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_user&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;i_lfa1&nbsp;FROM&nbsp;lfa1&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;i_lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_lifnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bpext&nbsp;INTO&nbsp;lv_bpext&nbsp;FROM&nbsp;but000&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;i_lfa1-lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;group_feature&nbsp;INTO&nbsp;lv_group_feature&nbsp;FROM&nbsp;bp001&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;i_lfa1-lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;smtp_addr&nbsp;INTO&nbsp;lv_smtp_addr&nbsp;FROM&nbsp;adr6&nbsp;WHERE&nbsp;addrnumber&nbsp;=&nbsp;i_lfa1-adrnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;remark&nbsp;INTO&nbsp;lv_remark&nbsp;FROM&nbsp;adrct&nbsp;WHERE&nbsp;addrnumber&nbsp;=&nbsp;i_lfa1-adrnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bpkind&nbsp;INTO&nbsp;lv_bpkind&nbsp;FROM&nbsp;but000&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;i_lfa1-lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zqylx&nbsp;INTO&nbsp;ls_zdata_header-zqylx&nbsp;&nbsp;FROM&nbsp;ztmm007&nbsp;WHERE&nbsp;bpkind&nbsp;=&nbsp;lv_bpkind.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;landx&nbsp;INTO&nbsp;lv_landx&nbsp;&nbsp;FROM&nbsp;t005t&nbsp;WHERE&nbsp;land1&nbsp;=&nbsp;i_lfa1-land1&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bezei&nbsp;INTO&nbsp;lv_bezei&nbsp;&nbsp;FROM&nbsp;t005u&nbsp;WHERE&nbsp;land1&nbsp;=&nbsp;i_lfa1-land1&nbsp;AND&nbsp;bland&nbsp;=&nbsp;i_lfa1-regio&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_lfa1-nodel&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_lfa1-nodel&nbsp;=&nbsp;'Y'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 获取主数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-guid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;i_lfa1-lifnr&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-land1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_landx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-name1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ort01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-ort01.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_bukrs&nbsp;=&nbsp;i_lfa1-vbund+2(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkbukrs&nbsp;INTO&nbsp;ls_zdata_header-vbund&nbsp;FROM&nbsp;ztfico001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;lv_bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-regio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_bezei.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-pstlz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-pstlz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-sortl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-sortl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-stras&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-stras.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_lfa1-ktokk&nbsp;=&nbsp;'Z001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ktokk&nbsp;=&nbsp;'Y'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ktokk&nbsp;=&nbsp;'N'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*  ls_zdata_header-ktokk             = i_lfa1-ktokk.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-nodel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-nodel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-stcd5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-stcd5.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ISOLA_OUTPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;i_lfa1-spras<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_zdata_header-spras.<br/>
</div>
<div class="codeComment">
*    ls_zdata_header-spras             = i_lfa1-spras.<br/>
* ls_zdata_header-ZQYLX             =                     "企业类型"<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bpext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_bpext.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bp_group_feature&nbsp;&nbsp;=&nbsp;lv_group_feature.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-tel_number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-telf1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-fax_number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_lfa1-telfx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-smtp_addr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_smtp_addr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_remark.<br/>
</div>
<div class="codeComment">
* 添加抬头数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-header&nbsp;=&nbsp;ls_zdata_header.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;ls_lfbk&nbsp;FROM&nbsp;lfbk&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;i_lfa1-lifnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-guid&nbsp;&nbsp;&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-lifnr&nbsp;&nbsp;=&nbsp;|{&nbsp;i_lfa1-lifnr&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-banks&nbsp;&nbsp;=&nbsp;ls_lfbk-banks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-bankl&nbsp;&nbsp;=&nbsp;ls_lfbk-bankl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_lfbk-bankn&nbsp;ls_lfbk-bkref&nbsp;INTO&nbsp;ls_zdata_item-bankn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_zdata_item&nbsp;TO&nbsp;lt_zdata_item.<br/>
</div>
<div class="codeComment">
* 添加item数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-items&nbsp;=&nbsp;lt_zdata_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_mm050_supplierdata_req-message_header&nbsp;=&nbsp;ls_msg_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_mm050_supplierdata_req-zdata&nbsp;=&nbsp;ls_zdata.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;supplierreq&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;supplierreq.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;supplierreq-&gt;si_erp_mm050_supplierdata_asyn<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'发送成功！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;supplierreq&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;e_msg_type&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;e_msg&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
<br/>
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;i_lfa1-lifnr&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>