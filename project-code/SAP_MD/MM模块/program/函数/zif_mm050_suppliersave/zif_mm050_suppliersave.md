<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM050_SUPPLIERSAVE</h2>
<h3> Description: 供应商主数据对象号获取</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZIF_MM050_SUPPLIERSAVE.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_LIFNR) TYPE  LFA1-LIFNR<br/>
*"     VALUE(I_UUID) TYPE  SYSUUID_C32 OPTIONAL<br/>
*"     VALUE(I_FLAG) TYPE  CHAR1 OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_mm050_suppliersave.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:ls_data&nbsp;TYPE&nbsp;ztmm006.<br/>
&nbsp;&nbsp;IF&nbsp;i_uuid&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;i_uuid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ls_data-lifnr&nbsp;=&nbsp;i_lifnr.<br/>
</div>
<div class="codeComment">
* 推送<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;i_flag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;UPDATE&nbsp;ztmm006&nbsp;SET&nbsp;zpush&nbsp;=&nbsp;'X'&nbsp;zz_upd_usr&nbsp;=&nbsp;sy-uname&nbsp;zz_upd_dat&nbsp;=&nbsp;sy-datum&nbsp;zz_upd_time&nbsp;=&nbsp;sy-uzeit&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;i_lifnr.<br/>
&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="codeComment">
* 创建<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_data-guid&nbsp;&nbsp;=&nbsp;i_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_data-zz_crt_usr&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_data-zz_crt_dat&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_data-zz_crt_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;ztmm006&nbsp;FROM&nbsp;ls_data&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>