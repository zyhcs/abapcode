<table class="outerTable">
<tr>
<td><h2>Table: ZMMS_002</h2>
<h3>Description: 采购收货单打印参考结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUTXT</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>NAME_L</td>
<td>2</td>
<td>&nbsp;</td>
<td>NAME1_GP</td>
<td>NAME</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>名称 1</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AEDAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>ERDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>记录创建日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZPURCONT</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZEPURCONT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>采购合同</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>EBELN</td>
<td>5</td>
<td>&nbsp;</td>
<td>EBELN</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购凭证编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>EBELP</td>
<td>6</td>
<td>&nbsp;</td>
<td>EBELP</td>
<td>EBELP</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>采购凭证的项目编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZXQLX</td>
<td>7</td>
<td>&nbsp;</td>
<td>CHAR50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>注释</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MATNR</td>
<td>8</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MAKTX</td>
<td>9</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MEINS</td>
<td>10</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>11</td>
<td>LGOBE</td>
<td>11</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MENGE</td>
<td>12</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>13</td>
<td>MENGE_WS</td>
<td>13</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MENGE_SH</td>
<td>14</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>MENGE_HG</td>
<td>15</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>16</td>
<td>CHARG</td>
<td>16</td>
<td>&nbsp;</td>
<td>CHARG_D</td>
<td>CHARG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>批次编号</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZSL</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>税率</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZHSDJ</td>
<td>18</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>24</td>
<td>&nbsp;</td>
<td>含税单价</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZHSJE</td>
<td>19</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>13</td>
<td>&nbsp;</td>
<td>含税金额</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZBHSJE</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>13</td>
<td>&nbsp;</td>
<td>不含税金额</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZJSHJ</td>
<td>21</td>
<td>&nbsp;</td>
<td>KWERT</td>
<td>WERTV7</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>定价值</td>
</tr>
<tr class="cell">
<td>22</td>
<td>AFNAM</td>
<td>22</td>
<td>&nbsp;</td>
<td>AFNAM</td>
<td>AFNAM</td>
<td>CHAR</td>
<td>12</td>
<td>X</td>
<td>申请人姓名</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZCGY</td>
<td>23</td>
<td>&nbsp;</td>
<td>BU_NAMEOR1</td>
<td>BU_NAME</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>组织名称 1</td>
</tr>
<tr class="cell">
<td>24</td>
<td>TEL_NUMBER</td>
<td>24</td>
<td>&nbsp;</td>
<td>AD_TLNMBR</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>电话号码: 拨区号 + 号码</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZBZ</td>
<td>25</td>
<td>&nbsp;</td>
<td>CHAR200</td>
<td>CHAR200</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>文本字段长度 200</td>
</tr>
<tr class="cell">
<td>26</td>
<td>EINDT</td>
<td>26</td>
<td>&nbsp;</td>
<td>EINDT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>项目交货日期</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZXH</td>
<td>27</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>INT4</td>
<td>10</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
<tr class="cell">
<td>28</td>
<td>MSEH6</td>
<td>28</td>
<td>&nbsp;</td>
<td>MSEH6</td>
<td>MSEH6</td>
<td>CHAR</td>
<td>6</td>
<td>X</td>
<td>技术格式中的外部计量单位(6-字符)</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZDJLSH</td>
<td>29</td>
<td>&nbsp;</td>
<td>CHAR10</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>字符字段长度为 10</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>