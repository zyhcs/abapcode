<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO012</h2>
<h3> Description: 采购订单预付查询及创建</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO012<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfico012.<br/>
<br/>
TABLES:ztmm009,ztmm010.<br/>
<br/>
include <a href="zincl_zfico012_top.html">zincl_zfico012_top</a>.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;SELECT-OPTIONS:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_zyfbh&nbsp;FOR&nbsp;ztmm009-zyfbh&nbsp;MODIF&nbsp;&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"销售订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_ebeln&nbsp;FOR&nbsp;ztmm009-ebeln&nbsp;MODIF&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"采购凭证<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_bukrs&nbsp;FOR&nbsp;ztmm009-bukrs&nbsp;OBLIGATORY&nbsp;MODIF&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"公司<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_ekorg&nbsp;FOR&nbsp;ztmm009-ekorg&nbsp;MATCHCODE&nbsp;OBJECT&nbsp;h_t024e&nbsp;MODIF&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"采购组织<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_zpcont&nbsp;&nbsp;FOR&nbsp;ztmm009-zpurcont&nbsp;MODIF&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"合同编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_lifnr&nbsp;FOR&nbsp;ztmm009-lifnr&nbsp;MODIF&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"供应商编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;s_zyfrq&nbsp;FOR&nbsp;ztmm009-zyfrq&nbsp;MODIF&nbsp;ID&nbsp;m1.&nbsp;&nbsp;"预付日期<br/>
<br/>
SELECTION-SCREEN END OF BLOCK blk1.<br/>
<br/>
include <a href="zincl_zfico012_f01.html">zincl_zfico012_f01</a>.<br/>
include <a href="zincl_zfico012_f02.html">zincl_zfico012_f02</a>.<br/>
include <a href="zincl_zfico012_f03.html">zincl_zfico012_f03</a>.<br/>
include <a href="zincl_zfico012_f04.html">zincl_zfico012_f04</a>.<br/>
include <a href="zincl_zfico012_pbo.html">zincl_zfico012_pbo</a>.<br/>
include <a href="zincl_zfico012_pai.html">zincl_zfico012_pai</a>.<br/>
include <a href="zincl_zfico012_pov.html">zincl_zfico012_pov</a>.<br/>
include <a href="zincl_zfico012_cons.html">zincl_zfico012_cons</a>.<br/>
<br/>
INITIALIZATION.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
<br/>
START-OF-SELECTION.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_authority_check.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_query_yf.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_display_yf.<br/>
<br/>
</div>
<div class="codeComment">
*GUI&nbsp;Texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;TT_ALV&nbsp;--&gt;&nbsp;采购单预付查询<br/>
*&nbsp;TT_CJ&nbsp;--&gt;&nbsp;创建预付单<br/>
*&nbsp;TT_CX&nbsp;--&gt;&nbsp;查询预付单<br/>
*&nbsp;TT_XG&nbsp;--&gt;&nbsp;修改预付单<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_EBELN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;采购凭证<br/>
*&nbsp;S_EKORG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;采购组织<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商<br/>
*&nbsp;S_ZPCONT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;合同<br/>
*&nbsp;S_ZYFBH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAP预付编号<br/>
*&nbsp;S_ZYFRQ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应付日期<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;预付信息已推送EAS，不允许更改！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>