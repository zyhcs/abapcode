<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_FICO_014_REQ</h2>
<h3> Description: 供应商采购订单预付数据推送(SAP-EAS)</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_fico_014_req .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IS_HEAD) TYPE  ZTMM009<br/>
*"  EXPORTING<br/>
*"     VALUE(OS_RETURN) TYPE  BAPIRET2<br/>
*"  TABLES<br/>
*"      IT_ITEM STRUCTURE  ZTMM010 OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
*  DATA ls_return TYPE bapiret2.<br/>
<div class="codeComment">*       <a href="global-zif_fico_014_req.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lv_uuid&nbsp;TYPE&nbsp;sysuuid_c32.<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_message&nbsp;TYPE&nbsp;bapi_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;TYPE&nbsp;bapi_mtype.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lp_proxy&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zpficoco_si_erp2eas_fico014_pc.<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;TYPE&nbsp;zpficomt_erp_fico014_pch_paym1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_in&nbsp;&nbsp;TYPE&nbsp;zpficomt_erp_fico014_pch_paymt.<br/>
&nbsp;&nbsp;DATA:os_header&nbsp;TYPE&nbsp;zpficodt_erp_fico014_pch_paym7,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zpficodt_erp_fico014_pch_paym5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ot_items&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;zpficodt_erp_fico014_pch_paym5.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_key(50)&nbsp;TYPE&nbsp;c.<br/>
<br/>
&nbsp;&nbsp;lv_uuid&nbsp;=&nbsp;cl_system_uuid=&gt;create_uuid_c32_static(&nbsp;).<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_FICO_014_REQ'&nbsp;).<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;is_head&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;'没有收到传入数据'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
</div>
<div class="codeComment">
*检查接口是否启用<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
<br/>
</div>
<div class="codeComment">
*调用同步接口<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;lp_proxy&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_ai_system_fault&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"构建发送的数据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;it_item&nbsp;ASCENDING&nbsp;BY&nbsp;zyfbh&nbsp;zposnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_10&gt;&nbsp;TYPE&nbsp;ztmm010.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_od&nbsp;&nbsp;TYPE&nbsp;string.&nbsp;&nbsp;"外部日期<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-guid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-zyfbh&nbsp;=&nbsp;is_head-zyfbh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-ebeln&nbsp;=&nbsp;is_head-ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bsart&nbsp;INTO&nbsp;os_header-bsart&nbsp;FROM&nbsp;ekko&nbsp;WHERE&nbsp;ebeln&nbsp;=&nbsp;is_head-ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-guid&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-zyfbh&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-ebeln&nbsp;NO-GAPS.<br/>
</div>
<div class="codeComment">
*  os_header-bukrs = is_head-bukrs.<br/>
*  os_header-ekorg = is_head-ekorg.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;"公司，进行内外转换<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-bukrs&nbsp;=&nbsp;zcl_eas_assist=&gt;exchang_bukrs(&nbsp;is_head-bukrs&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-bukrs&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"采购组织,内外转换<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-ekorg&nbsp;=&nbsp;zcl_eas_assist=&gt;exchang_ekorg(&nbsp;is_head-ekorg&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-ekorg&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-zcgy&nbsp;=&nbsp;is_head-zcgy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-zcgy&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-waers&nbsp;=&nbsp;is_head-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-waers&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-wkurs&nbsp;=&nbsp;is_head-wkurs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-wkurs&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-yftpr&nbsp;=&nbsp;is_head-zyftpr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-yftpr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_od.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"转外部日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_int_to_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datint&nbsp;&nbsp;&nbsp;=&nbsp;is_head-aedat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datext&nbsp;&nbsp;&nbsp;=&nbsp;lv_od<br/>
</div>
<div class="codeComment">
*           ex_datfmused =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_abap_datfm_format_unknown.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-aedat&nbsp;=&nbsp;lv_od.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-aedat&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-zpurcont&nbsp;=&nbsp;is_head-zpurcont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-zpurcont&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-zsqr&nbsp;=&nbsp;is_head-zsqr&nbsp;&amp;&amp;&nbsp;','&nbsp;&amp;&amp;&nbsp;is_head-zsqrms.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-zsqr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-remake&nbsp;=&nbsp;is_head-remark.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-remake&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-netpr&nbsp;=&nbsp;is_head-netwr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-netpr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_od.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"转外部日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_int_to_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datint&nbsp;&nbsp;&nbsp;=&nbsp;is_head-zyfrq<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datext&nbsp;&nbsp;&nbsp;=&nbsp;lv_od<br/>
</div>
<div class="codeComment">
*           ex_datfmused =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_abap_datfm_format_unknown.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-yfrq&nbsp;=&nbsp;lv_od.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-yfrq&nbsp;NO-GAPS.<br/>
</div>
<div class="codeComment">
*  os_header-lifnr = is_head-lifnr.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;is_head-lifnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;OR&nbsp;is_head-lifnr&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;os_header-lifnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;is_head-lifnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-lifnr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"科目进行内外转换<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zksaknr&nbsp;AS&nbsp;zracct<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;os_header-zracct<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ztfico002<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;saknr&nbsp;=&nbsp;is_head-zracct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_header-zracct&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_item&nbsp;ASSIGNING&nbsp;&lt;fs_10&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-zyfbh&nbsp;=&nbsp;&lt;fs_10&gt;-zyfbh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-ebeln&nbsp;=&nbsp;&lt;fs_10&gt;-ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-ebelp&nbsp;=&nbsp;&lt;fs_10&gt;-ebelp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-matnr&nbsp;=&nbsp;&lt;fs_10&gt;-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-matktx&nbsp;=&nbsp;&lt;fs_10&gt;-matktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-anln1&nbsp;=&nbsp;&lt;fs_10&gt;-anln1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-aufnr&nbsp;=&nbsp;&lt;fs_10&gt;-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-inetpr&nbsp;=&nbsp;&lt;fs_10&gt;-brtwr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-iyftpr&nbsp;=&nbsp;&lt;fs_10&gt;-zyftpr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-yftpr&nbsp;=&nbsp;&lt;fs_10&gt;-fjfje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-zyfbh&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-ebeln&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-ebelp&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-matnr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-matktx&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-anln1&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-aufnr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-inetpr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-iyftpr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;os_item-yftpr&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;os_item&nbsp;TO&nbsp;ot_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;os_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-zdata-header&nbsp;=&nbsp;os_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-zdata-items&nbsp;=&nbsp;ot_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-receiver&nbsp;=&nbsp;'EAS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-bustyp&nbsp;=&nbsp;'FICO014'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_fico014_pch_paymt_req-message_header-send_user&nbsp;=&nbsp;sy-uname.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"调接口<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lp_proxy-&gt;si_erp2eas_fico014_pch_paymt_s<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;ls_in.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_ai_system_fault&nbsp;INTO&nbsp;lo_error.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_in&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;'未收到OA系统返回的数据'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;ls_in-mt_erp_fico014_pch_paymt_res-zdata-header-header_status.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;ls_in-mt_erp_fico014_pch_paymt_res-zdata-header-header_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;lv_msgtype&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;=&nbsp;'S'&nbsp;AND&nbsp;lv_message&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;'数据推送成功'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;os_return-type&nbsp;=&nbsp;lv_msgtype.<br/>
&nbsp;&nbsp;os_return-message&nbsp;=&nbsp;lv_message.<br/>
<br/>
</div>
<div class="codeComment">
*当接口出现错误时,获取函数传入传出参数,以便重推<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;paras&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;get_func_paras(&nbsp;IMPORTING&nbsp;paras&nbsp;=&nbsp;DATA(lt_paras)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_paras&nbsp;&nbsp;ASSIGNING&nbsp;&nbsp;FIELD-SYMBOL(&lt;ls_paras&gt;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;(&lt;ls_paras&gt;-parameter)&nbsp;TO&nbsp;&lt;paras&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_paras&gt;-value&nbsp;=&nbsp;REF&nbsp;#(&nbsp;&lt;paras&gt;&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_func_msg(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;paras&nbsp;&nbsp;=&nbsp;lt_paras<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repush&nbsp;=&nbsp;'X'&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msgtype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;lv_message<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;''&nbsp;).<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>