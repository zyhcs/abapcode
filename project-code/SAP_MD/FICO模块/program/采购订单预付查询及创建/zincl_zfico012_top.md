<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZFICO012_TOP</h2>
<h3> Description: Include ZINCL_ZFICO012_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZFICO012_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
"预付抬头<br/>
TYPES:BEGIN OF ty_yf_head,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyfbh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zyfbh,&nbsp;"SAP预付编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-ebeln,&nbsp;"&nbsp;&nbsp;采购凭证<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-bukrs,&nbsp;"&nbsp;&nbsp;公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ekorg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-ekorg,&nbsp;"&nbsp;&nbsp;采购组织<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcgy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zcgy,&nbsp;"&nbsp;&nbsp;采购员<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcgyms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zcgyms,&nbsp;&nbsp;"采购员名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-waers,&nbsp;"&nbsp;&nbsp;货币<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wkurs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-wkurs,&nbsp;"汇率<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyftpr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zyftpr,&nbsp;"&nbsp;&nbsp;预付金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aedat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-aedat,&nbsp;"&nbsp;&nbsp;创建日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zpurcont&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zpurcont,&nbsp;"&nbsp;&nbsp;合同<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zracct&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zracct,&nbsp;"&nbsp;&nbsp;往来科目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsqr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zsqr,&nbsp;"&nbsp;&nbsp;申请人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsqrms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zsqrms,&nbsp;&nbsp;"申请人描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsqrcb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;70,&nbsp;&nbsp;&nbsp;"申请人组合信息：申请人编号+/+申请人描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-remark,&nbsp;"&nbsp;&nbsp;备注<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-netwr,&nbsp;"&nbsp;&nbsp;订单金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brtwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-brtwr,&nbsp;"订单含税金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyfrq&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zyfrq,&nbsp;"	应付日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-lifnr,&nbsp;"&nbsp;&nbsp;供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-name1,&nbsp;"&nbsp;&nbsp;供应商描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zflag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm009-zflag,&nbsp;"&nbsp;&nbsp;预付单状态：空：新建；L：已传输EAS；W：EAS退回<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zflag_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;20,&nbsp;"预付状态描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zdel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;1,&nbsp;&nbsp;"删除标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zxhd(4),&nbsp;"&nbsp;&nbsp;信号灯<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zmessage(200),&nbsp;"&nbsp;&nbsp;信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bsart,&nbsp;&nbsp;"采购凭证类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;frgrl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-frgrl,&nbsp;&nbsp;"审批状态，空标识不需要审批或已经审批完事<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAKNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;SAKNR,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_yf_head.<br/>
DATA: gs_yf_head     TYPE ty_yf_head,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_yf_head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_head,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_yf_head_old&nbsp;TYPE&nbsp;ty_yf_head,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_yf_head_old&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_head.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_yf_head_slt&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_head<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
</div>
<div class="code">
"预付项目<br/>
TYPES:BEGIN OF ty_yf_item,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyfbh&nbsp;&nbsp;TYPE&nbsp;ztmm010-zyfbh,&nbsp;"SAP预付编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zposnr&nbsp;TYPE&nbsp;ztmm010-zposnr,&nbsp;"预付行项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zdel&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztmm010-zdel,&nbsp;"删除标识。X：已删除<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;TYPE&nbsp;ztmm010-ebeln,&nbsp;"采购凭证编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;&nbsp;TYPE&nbsp;ztmm010-ebelp,&nbsp;"采购凭证的项目编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;TYPE&nbsp;ztmm010-matnr,&nbsp;"物料编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matktx&nbsp;TYPE&nbsp;ztmm010-matktx,&nbsp;"采购短文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;TYPE&nbsp;ztmm010-menge,&nbsp;"采购订单数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;TYPE&nbsp;ztmm010-meins,&nbsp;"基本计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anln1&nbsp;&nbsp;TYPE&nbsp;ztmm010-anln1,&nbsp;"主要资产编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;&nbsp;TYPE&nbsp;ztmm010-aufnr,&nbsp;"订单编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;TYPE&nbsp;ztmm010-netwr,&nbsp;"订单金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brtwr&nbsp;&nbsp;TYPE&nbsp;ztmm010-brtwr,&nbsp;"订单含税金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netpr&nbsp;&nbsp;TYPE&nbsp;ztmm010-netpr,&nbsp;"净价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyftpr&nbsp;TYPE&nbsp;ztmm010-zyftpr,&nbsp;"预付金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fjfje&nbsp;&nbsp;TYPE&nbsp;ztmm010-fjfje,&nbsp;"附加费金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;knttp&nbsp;&nbsp;TYPE&nbsp;ekpo-knttp,&nbsp;"科目分配类别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_yf_item.<br/>
DATA:<br/>
&nbsp;&nbsp;gs_yf_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ty_yf_item,<br/>
&nbsp;&nbsp;gt_yf_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_item,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_yf_item_old&nbsp;TYPE&nbsp;ty_yf_item,<br/>
</div>
<div class="code">
&nbsp;&nbsp;gt_yf_item_old&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_item,&nbsp;&nbsp;&nbsp;&nbsp;"记录最初的查询结果<br/>
&nbsp;&nbsp;gt_yf_item_del&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_yf_item.&nbsp;&nbsp;&nbsp;&nbsp;"记录被删除的行项目<br/>
<br/>
DATA:gds_layout_yf   TYPE lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gdt_fieldcat_yf&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gds_fieldcat_yf&nbsp;TYPE&nbsp;lvc_s_fcat.<br/>
DATA:g_grid_yf TYPE REF TO cl_gui_alv_grid.<br/>
<br/>
DATA gv_ebeln TYPE ekko-ebeln.  "采购订单\<br/>
</div>
<div class="codeComment">
*DATA&nbsp;gv_saknr&nbsp;TYPE&nbsp;saknr.&nbsp;&nbsp;&nbsp;&nbsp;"科目<br/>
</div>
<div class="code">
DATA gv_alv_uc TYPE string. "记录操作命令<br/>
</div>
<div class="codeComment">
*DATA&nbsp;gv_mod_f&nbsp;TYPE&nbsp;char1.&nbsp;"记录修改动作&nbsp;有修改动作:X<br/>
<br/>
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TB_YF_ITEM'&nbsp;ITSELF<br/>
</div>
<div class="code">
CONTROLS: tb_yf_item TYPE TABLEVIEW USING SCREEN 0100.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;LINES&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TB_YF_ITEM'<br/>
</div>
<div class="code">
DATA:     g_tb_yf_item_lines  LIKE sy-loopc.<br/>
DATA:     gv_actvt(2) TYPE c VALUE '03'.<br/>
DATA:     ok_code LIKE sy-ucomm.<br/>
<br/>
DATA gv_no TYPE ztmm009-zyfbh.  "预付单号，<br/>
<br/>
TYPES:BEGIN OF ty_ebeln_hp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-lifnr,&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zpurcont&nbsp;TYPE&nbsp;ekko-zpurcont,&nbsp;"合同号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;purps&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-purps,&nbsp;"采购员<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;"采购员<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zterm&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-zterm,&nbsp;"付款方式<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_ebeln_hp.<br/>
<br/>
</div>
<div class="codeComment">
*data&nbsp;gv_save_act&nbsp;type&nbsp;char1.&nbsp;&nbsp;"创建预付单操作时候，如果保存成功标识：X，防止连续点击保存按钮出现重复保存的情况，生成多条数据<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>