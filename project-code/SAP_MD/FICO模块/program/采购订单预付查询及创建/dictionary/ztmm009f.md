<table class="outerTable">
<tr>
<td><h2>Table: ZTMM009F</h2>
<h3>Description: 预付单流水号记录</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZDATS</td>
<td>1</td>
<td>X</td>
<td>DATS</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>类型 DATS 的字段</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZYFNO</td>
<td>2</td>
<td>&nbsp;</td>
<td>ZEYFNO</td>
<td>ZDYFNO</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>预付单流水号</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>