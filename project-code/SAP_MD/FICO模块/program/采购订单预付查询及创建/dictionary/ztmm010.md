<table class="outerTable">
<tr>
<td><h2>Table: ZTMM010</h2>
<h3>Description: 预付单行项目表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZYFBH</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>SAP预付编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZPOSNR</td>
<td>3</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>预付行项目</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZDEL</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>删除标识。X：已删除</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>EBELN</td>
<td>5</td>
<td>&nbsp;</td>
<td>EBELN</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购凭证编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>EBELP</td>
<td>6</td>
<td>&nbsp;</td>
<td>EBELP</td>
<td>EBELP</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>采购凭证的项目编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MATNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MATKTX</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>采购短文本</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MENGE</td>
<td>9</td>
<td>&nbsp;</td>
<td>BSTMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>采购订单数量</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MEINS</td>
<td>10</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ANLN1</td>
<td>11</td>
<td>&nbsp;</td>
<td>ANLN1</td>
<td>ANLN1</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>主要资产编号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>AUFNR</td>
<td>12</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>13</td>
<td>NETWR</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>订单金额</td>
</tr>
<tr class="cell">
<td>14</td>
<td>BRTWR</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>订单含税金额</td>
</tr>
<tr class="cell">
<td>15</td>
<td>NETPR</td>
<td>15</td>
<td>&nbsp;</td>
<td>NETPR</td>
<td>WERTV6</td>
<td>CURR</td>
<td>11</td>
<td>&nbsp;</td>
<td>净价</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZYFTPR</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>预付金额</td>
</tr>
<tr class="cell">
<td>17</td>
<td>FJFJE</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>附加费金额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>