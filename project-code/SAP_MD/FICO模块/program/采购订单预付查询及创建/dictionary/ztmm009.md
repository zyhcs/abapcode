<table class="outerTable">
<tr>
<td><h2>Table: ZTMM009</h2>
<h3>Description: 预付单抬头表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZYFBH</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>SAP预付编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZFLAG</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>L：不可修改;W：可以修改</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZDEL</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>删除标识。X：已删除</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>EBELN</td>
<td>5</td>
<td>&nbsp;</td>
<td>EBELN</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购凭证编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BUKRS</td>
<td>6</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>EKORG</td>
<td>7</td>
<td>&nbsp;</td>
<td>EKORG</td>
<td>EKORG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>采购组织</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZCGY</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZEPURPS</td>
<td>BU_PARTNER</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购员</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZCGYMS</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>35</td>
<td>&nbsp;</td>
<td>采购员名称</td>
</tr>
<tr class="cell">
<td>10</td>
<td>WAERS</td>
<td>10</td>
<td>&nbsp;</td>
<td>WAERS</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>货币码</td>
</tr>
<tr class="cell">
<td>11</td>
<td>WKURS</td>
<td>11</td>
<td>&nbsp;</td>
<td>WKURS</td>
<td>KURSP</td>
<td>DEC</td>
<td>9</td>
<td>&nbsp;</td>
<td>汇率</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZYFTPR</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>预付金额</td>
</tr>
<tr class="cell">
<td>13</td>
<td>AEDAT</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZPURCONT</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZEPURCONT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>采购合同</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZSQR</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>申请人</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZSQRMS</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>申请人描述</td>
</tr>
<tr class="cell">
<td>17</td>
<td>REMARK</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>备注</td>
</tr>
<tr class="cell">
<td>18</td>
<td>NETWR</td>
<td>18</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>订单金额</td>
</tr>
<tr class="cell">
<td>19</td>
<td>BRTWR</td>
<td>19</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>订单含税金额</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZYFRQ</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>应付日期</td>
</tr>
<tr class="cell">
<td>21</td>
<td>LIFNR</td>
<td>21</td>
<td>&nbsp;</td>
<td>LIFNR</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商或债权人的帐号</td>
</tr>
<tr class="cell">
<td>22</td>
<td>NAME1</td>
<td>22</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZRACCT</td>
<td>23</td>
<td>&nbsp;</td>
<td>SAKNR</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目编号</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>