<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZFICO012_PAI</h2>
<h3> Description: Include ZINCL_ZFICO012_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZFICO012_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_0100&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE user_command_0100 INPUT.<br/>
&nbsp;&nbsp;ok_code&nbsp;=&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_yn&nbsp;TYPE&nbsp;char1.&nbsp;&nbsp;"离开确认<br/>
&nbsp;&nbsp;CASE&nbsp;ok_code.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;BACK'&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gv_mod_f&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_confirm_yn&nbsp;USING&nbsp;'是否离开当前屏幕'&nbsp;CHANGING&nbsp;lv_yn.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_yn&nbsp;=&nbsp;'S'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;EXIT'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gv_mod_f&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_confirm_yn&nbsp;USING&nbsp;'是否离开当前程序'&nbsp;CHANGING&nbsp;lv_yn.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_yn&nbsp;=&nbsp;'S'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;CANCEL'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gv_mod_f&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_confirm_yn&nbsp;USING&nbsp;'是否离开当前屏幕'&nbsp;CHANGING&nbsp;lv_yn.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_yn&nbsp;=&nbsp;'S'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SAVE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exec_save.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SR_SRCH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exec_srch.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SR_CHTI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exec_chti.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SR_CITH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exec_cith.<br/>
<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
<br/>
FORM frm_exec_srch.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_query_yf_ek.<br/>
ENDFORM.<br/>
<br/>
FORM frm_exec_chti.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_chti.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_flag&nbsp;TYPE&nbsp;char1.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_zyftpr_comp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_chk_posnr_zyftpr_cp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
ENDFORM.<br/>
<br/>
FORM frm_exec_cith.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_cith.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_flag&nbsp;TYPE&nbsp;char1.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_zyftpr_comp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_chk_posnr_zyftpr_cp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
ENDFORM.<br/>
<br/>
FORM frm_exec_save.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_flag&nbsp;TYPE&nbsp;char1.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_items_line&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_flag&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_head_item_yftpr&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_flag&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_zyftpr_comp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_flag&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_chk_posnr_zyftpr_cp_brtwr&nbsp;CHANGING&nbsp;lv_flag.<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_flag&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_save_data.<br/>
ENDFORM.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TB_YF_ITEM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE tb_yf_item_modify INPUT.<br/>
&nbsp;&nbsp;MODIFY&nbsp;gt_yf_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;gs_yf_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;tb_yf_item-current_line.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODUL&nbsp;FOR&nbsp;TC&nbsp;'TB_YF_ITEM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MARK&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE tb_yf_item_mark INPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_tb_yf_item_wa2&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_yf_item.<br/>
&nbsp;&nbsp;IF&nbsp;tb_yf_item-line_sel_mode&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;AND&nbsp;gs_yf_item-zsel&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_yf_item&nbsp;INTO&nbsp;g_tb_yf_item_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;zsel&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_tb_yf_item_wa2-zsel&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_yf_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;g_tb_yf_item_wa2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;zsel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;MODIFY&nbsp;gt_yf_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;gs_yf_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INDEX&nbsp;tb_yf_item-current_line<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRANSPORTING&nbsp;zsel.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TB_YF_ITEM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;PROCESS&nbsp;USER&nbsp;COMMAND<br/>
</div>
<div class="code">
MODULE tb_yf_item_user_command INPUT.<br/>
&nbsp;&nbsp;ok_code&nbsp;=&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;PERFORM&nbsp;user_ok_tc&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;'TB_YF_ITEM'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'GT_YF_ITEM'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ZSEL'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;ok_code.<br/>
&nbsp;&nbsp;sy-ucomm&nbsp;=&nbsp;ok_code.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>