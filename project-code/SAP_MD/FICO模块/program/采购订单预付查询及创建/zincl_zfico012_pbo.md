<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZFICO012_PBO</h2>
<h3> Description: Include ZINCL_ZFICO012_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZFICO012_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_0100 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STS_CJ'.<br/>
&nbsp;&nbsp;IF&nbsp;gv_alv_uc&nbsp;=&nbsp;'ZCJ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TT_CJ'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;gv_alv_uc&nbsp;=&nbsp;'ZXG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TT_XG'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;gv_alv_uc&nbsp;=&nbsp;'ZCX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TT_CX'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TB_YF_ITEM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;UPDATE&nbsp;LINES&nbsp;FOR&nbsp;EQUIVALENT&nbsp;SCROLLBAR<br/>
</div>
<div class="code">
MODULE tb_yf_item_change_tc_attr OUTPUT.<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;gt_yf_item&nbsp;LINES&nbsp;tb_yf_item-lines.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TB_YF_ITEM'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;GET&nbsp;LINES&nbsp;OF&nbsp;TABLECONTROL<br/>
</div>
<div class="code">
MODULE tb_yf_item_get_lines OUTPUT.<br/>
&nbsp;&nbsp;g_tb_yf_item_lines&nbsp;=&nbsp;sy-loopc.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;MODIFY_100_SCREEN&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE modify_100_screen OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;=&nbsp;'TB_YF_ITEM_INSERT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gv_alv_uc&nbsp;&lt;&gt;&nbsp;'ZCJ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;=&nbsp;'SR_CGDT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GV_EBELN'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'SR_SRCH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gv_alv_uc&nbsp;=&nbsp;'ZCX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_HEAD-ZSQRCB'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_HEAD-ZYFTPR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_HEAD-ZRACCT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_HEAD-ZYFRQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_HEAD-REMARK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_ITEM-ZYFTPR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'GS_YF_ITEM-FJFJE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'SR_CHTI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'SR_CITH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;screen-name&nbsp;=&nbsp;'TB_YF_ITEM_DELETE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>