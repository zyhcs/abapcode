<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO018_TOP</h2>
<h3> Description: Include ZFICOR0033_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICOR0033_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES:BKPF,T001W,BSEG,MARA,ACDOCT.<br/>
<br/>
<br/>
TYPES:BEGIN OF TY_ALV,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLNBEZ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AFKO-PLNBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MARA-MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T023T-WGBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUART&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-AUART,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GLTRI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AFKO-GLTRI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T003P-TXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR_TXT&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DMBTR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BZDJ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,&nbsp;"产品标准单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SJPC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,&nbsp;"产品实际单价比标准单价偏差数<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XHCL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLMC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MARA-MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T023T-WGBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SJDH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JG_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBDWSL&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;LIPS-NTGEW,&nbsp;"aufm-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBXHSL&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBXHJE&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BZXHSL&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-MEG001,&nbsp;"标准消耗数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BZXHJE&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-WTG001,&nbsp;"标准消耗金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLBZDH&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-MEG001,&nbsp;"材料标准单耗<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLBZDJ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;COSS-WTG001,&nbsp;"材料标准单价<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sjdhpc&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;coss-meg001,&nbsp;&nbsp;"材料实际单耗比标准单耗偏差数<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sjjgpc&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;aufm-dmbtr,&nbsp;&nbsp;"材料实际价格比标准价格偏差数<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZRBMBM&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-PRCTR,&nbsp;&nbsp;"责任部门编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZRBMMC&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;CEPCT-KTEXT,&nbsp;"责任部门名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBJG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DHPC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,&nbsp;"材料实际单耗比标准单耗偏差数<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JGPC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,&nbsp;"材料实际价格比标准价格偏差数<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_ALV.<br/>
<br/>
TYPES:BEGIN OF TY_AUFK,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;&nbsp;LIKE&nbsp;AUFK-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUART&nbsp;&nbsp;LIKE&nbsp;AUFK-AUART,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR&nbsp;&nbsp;LIKE&nbsp;AUFK-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLNBEZ&nbsp;LIKE&nbsp;AFKO-PLNBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;&nbsp;LIKE&nbsp;AUFK-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GLTRI&nbsp;&nbsp;LIKE&nbsp;AFKO-GLTRI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARG&nbsp;&nbsp;LIKE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TXT&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T003P-TXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZRBMBM&nbsp;LIKE&nbsp;AUFK-PRCTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZRBMMC&nbsp;LIKE&nbsp;CEPCT-KTEXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KOSTV&nbsp;&nbsp;LIKE&nbsp;AUFK-KOSTV,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_AUFK,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;TY_COEP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GJAHR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-GJAHR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIO&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-PERIO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KSTAR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-KSTAR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BELNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-BELNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUZEI&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-BUZEI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBGBTR&nbsp;&nbsp;LIKE&nbsp;COEP-MBGBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTGBTR&nbsp;&nbsp;LIKE&nbsp;COEP-WTGBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINB&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-MEINB,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;COEP-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR_O&nbsp;LIKE&nbsp;COEP-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_COEP,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;TY_COSS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR&nbsp;&nbsp;LIKE&nbsp;COSS-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KSTAR&nbsp;&nbsp;LIKE&nbsp;COSS-KSTAR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG001&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG002&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG003&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG004&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG005&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG006&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG007&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG008&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG009&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG010&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG011&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WTG012&nbsp;LIKE&nbsp;COSS-WTG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG001&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG002&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG003&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG004&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG005&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG006&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG007&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG008&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG009&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG010&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG011&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEG012&nbsp;LIKE&nbsp;COSS-MEG001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_COSS.<br/>
<br/>
TYPES:BEGIN OF TY_ACDOCT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RCNTR&nbsp;LIKE&nbsp;ACDOCT-RCNTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RACCT&nbsp;LIKE&nbsp;ACDOCT-RACCT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSLVT&nbsp;LIKE&nbsp;ACDOCT-HSLVT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL01&nbsp;LIKE&nbsp;ACDOCT-HSL01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL02&nbsp;LIKE&nbsp;ACDOCT-HSL02,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL03&nbsp;LIKE&nbsp;ACDOCT-HSL03,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL04&nbsp;LIKE&nbsp;ACDOCT-HSL04,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL05&nbsp;LIKE&nbsp;ACDOCT-HSL05,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL06&nbsp;LIKE&nbsp;ACDOCT-HSL06,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL07&nbsp;LIKE&nbsp;ACDOCT-HSL07,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL08&nbsp;LIKE&nbsp;ACDOCT-HSL08,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL09&nbsp;LIKE&nbsp;ACDOCT-HSL09,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL10&nbsp;LIKE&nbsp;ACDOCT-HSL10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL11&nbsp;LIKE&nbsp;ACDOCT-HSL11,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL12&nbsp;LIKE&nbsp;ACDOCT-HSL12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL13&nbsp;LIKE&nbsp;ACDOCT-HSL13,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL14&nbsp;LIKE&nbsp;ACDOCT-HSL14,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL15&nbsp;LIKE&nbsp;ACDOCT-HSL15,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSL16&nbsp;LIKE&nbsp;ACDOCT-HSL16,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_ACDOCT.<br/>
<br/>
TYPES:BEGIN OF TY_MAKT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;LIKE&nbsp;MAKT-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAKTX&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_MAKT.<br/>
<br/>
TYPES:BEGIN OF TY_MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;TYPE&nbsp;MARA-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL&nbsp;TYPE&nbsp;T023T-MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ&nbsp;TYPE&nbsp;T023T-WGBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_MATKL.<br/>
<br/>
DATA:BEGIN OF GS_AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUDAT&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR_O&nbsp;LIKE&nbsp;AUFM-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR_O&nbsp;LIKE&nbsp;AUFK-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;GS_AUFNR.<br/>
DATA:GT_AUFNR LIKE TABLE OF GS_AUFNR.<br/>
<br/>
TYPES:BEGIN OF TY_AFKO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;LIKE&nbsp;AUFM-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GAMNG&nbsp;LIKE&nbsp;AFKO-GAMNG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IGMNG&nbsp;LIKE&nbsp;AFKO-IGMNG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFPL&nbsp;LIKE&nbsp;AFKO-AUFPL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_AFKO.<br/>
DATA:GT_AFKO TYPE TABLE OF TY_AFKO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_AFKO&nbsp;TYPE&nbsp;TY_AFKO.<br/>
<br/>
TYPES:BEGIN OF TY_AUFM_S,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;LIKE&nbsp;AUFM-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAKTO&nbsp;LIKE&nbsp;AUFM-SAKTO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;LIKE&nbsp;AUFM-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;LIKE&nbsp;AUFM-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARG&nbsp;LIKE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BWART&nbsp;LIKE&nbsp;AUFM-BWART,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DMBTR&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINS&nbsp;LIKE&nbsp;AUFM-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MJAHR&nbsp;LIKE&nbsp;AUFM-MJAHR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LFMON&nbsp;LIKE&nbsp;MBEWH-LFMON,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBSL&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBJE&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_AUFM_S.<br/>
<br/>
TYPES:BEGIN OF TY_TOTAL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WERKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFK-WERKS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLNBEZ&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AFKO-PLNBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MARA-MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T023T-WGBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR_TXT&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEINS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DMBTR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XHCL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLMC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MAKT-MAKTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATKL2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;MARA-MATKL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WGBEZ2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;T023T-WGBEZ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-MEINS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JG_&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;AUFM-DMBTR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_TOTAL.<br/>
DATA:GT_TOTAL TYPE TABLE OF TY_TOTAL,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_TOTAL&nbsp;TYPE&nbsp;TY_TOTAL.<br/>
<br/>
TYPES:BEGIN OF TY_MBEWH,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;TYPE&nbsp;MBEWH-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BWKEY&nbsp;TYPE&nbsp;MBEWH-BWKEY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LFGJA&nbsp;TYPE&nbsp;MBEWH-LFGJA,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LFMON&nbsp;TYPE&nbsp;MBEWH-LFMON,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STPRS&nbsp;TYPE&nbsp;MBEWH-STPRS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PEINH&nbsp;TYPE&nbsp;MBEWH-PEINH,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_MBEWH.<br/>
DATA:GT_MBEWH TYPE TABLE OF TY_MBEWH,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_MBEWH&nbsp;TYPE&nbsp;TY_MBEWH.<br/>
<br/>
TYPES:BEGIN OF TY_CONVERSION,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR_O&nbsp;TYPE&nbsp;AUFM-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR_O&nbsp;TYPE&nbsp;AUFK-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARG_O&nbsp;TYPE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR_O&nbsp;TYPE&nbsp;AUFK-OBJNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;&nbsp;&nbsp;TYPE&nbsp;AUFM-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AUFNR&nbsp;&nbsp;&nbsp;TYPE&nbsp;AUFK-AUFNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARG&nbsp;&nbsp;&nbsp;TYPE&nbsp;AUFM-CHARG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJNR&nbsp;&nbsp;&nbsp;TYPE&nbsp;AUFK-OBJNR,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;TYPE&nbsp;aufm-dmbtr,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE_1&nbsp;TYPE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MENGE_2&nbsp;TYPE&nbsp;AUFM-MENGE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;TY_CONVERSION.<br/>
DATA:GT_CONVERSION TYPE TABLE OF TY_CONVERSION,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GW_CONVERSION&nbsp;TYPE&nbsp;TY_CONVERSION.<br/>
<br/>
DATA:GT_ALV    TYPE TABLE OF TY_ALV,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WA_ALV&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TY_ALV,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_AUFM_S&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_AUFM_S,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GS_AUFM_S&nbsp;TYPE&nbsp;TY_AUFM_S,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_COEP&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_COEP,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_AUFK&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_AUFK,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GT_MATKL&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;TY_MATKL.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;SELECTION-SCREEN<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK01 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETER:P_BUKRS&nbsp;LIKE&nbsp;BKPF-BUKRS&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_WERKS&nbsp;&nbsp;FOR&nbsp;T001W-WERKS&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;BSEG-GJAHR&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;SY-DATUM(4).<br/>
&nbsp;&nbsp;SELECT-OPTIONS:S_MONAT&nbsp;FOR&nbsp;BSEG-H_MONAT&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;SY-DATUM+4(2),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_MATNR&nbsp;FOR&nbsp;MARA-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_AUFNR&nbsp;FOR&nbsp;BSEG-AUFNR.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;LINE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:RB_RAD1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;GR1&nbsp;DEFAULT&nbsp;'X'&nbsp;USER-COMMAND&nbsp;RADIO.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;3(8)&nbsp;TEXT-T02.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:RB_RAD2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;GR1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;14(8)&nbsp;TEXT-T03.<br/>
&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;LINE.<br/>
SELECTION-SCREEN END OF BLOCK BLK01.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>