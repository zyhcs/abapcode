<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007</h2>
<h3>Description: 模拟过账后数据(FICO-052)</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZVBELN1</td>
<td>1</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>交货单号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZPOSNR1</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>交货单项目</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZVBELN2</td>
<td>3</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZPOSNR2</td>
<td>4</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>发票项目</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZBUKRS</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZBELNR1</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账凭证</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZBELNR2</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>次月冲销凭证</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZGJAHR</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZFISPE</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>记账期间</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZDATE</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>写入日期</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZTIME</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>写入时间</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZNUMB</td>
<td>12</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>13</td>
<td>USNAM</td>
<td>13</td>
<td>&nbsp;</td>
<td>USNAM</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>用户名</td>
</tr>
<tr class="cell">
<td>14</td>
<td>BUDAT</td>
<td>14</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>