<table class="outerTable">
<tr>
<td><h2>Table: ZFICOS005</h2>
<h3>Description: POD途损调整过账扩展结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BSCHL</td>
<td>1</td>
<td>&nbsp;</td>
<td>BSCHL</td>
<td>BSCHL</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>过账代码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>POSNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>POSNR</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>销售和分销凭证的项目号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>XNEGP</td>
<td>3</td>
<td>&nbsp;</td>
<td>XNEGP</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>标识：负过账</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>