<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO010</h2>
<h3>Description: 过账后自建表(FICO053功能)</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZVBELN2</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZPOSNR2</td>
<td>3</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>开票凭证项目</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZBUKRS</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZBELNR1</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账凭证编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZGJAHR</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZFISPE</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>记账期间</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZBUDAT</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>过账日期</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZKUNNR</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZMATNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZWRBTR</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>22</td>
<td>&nbsp;</td>
<td>POD途损金额（过账）</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZMENGE</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>POD差异数量（过账）</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>