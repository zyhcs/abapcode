<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO018A</h2>
<h3> Description: 成本计算表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICOR0033<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfico018a.<br/>
<br/>
include <a href="zfico018a_top.html">ZFICO018A_TOP</a>.<br/>
<br/>
<br/>
include <a href="zfico018a_f01.html">ZFICO018A_F01</a>.<br/>
<br/>
<br/>
<br/>
START-OF-SELECTION.<br/>
&nbsp;&nbsp;"数据查询<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_matkl.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_mbewh.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;T01&nbsp;查询条件<br/>
*&nbsp;T02&nbsp;还原前<br/>
*&nbsp;T03&nbsp;还原后<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司<br/>
*&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计年度<br/>
*&nbsp;P_KM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分解到科目<br/>
*&nbsp;RB_RAD1&nbsp;<br/>
*&nbsp;RB_RAD2&nbsp;<br/>
*&nbsp;S_AUFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单号<br/>
*&nbsp;S_MATNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料<br/>
*&nbsp;S_MONAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计期间<br/>
*&nbsp;S_WERKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工厂<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>