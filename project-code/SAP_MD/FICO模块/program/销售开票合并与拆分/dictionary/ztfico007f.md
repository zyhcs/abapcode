<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007F</h2>
<h3>Description: 金税发票信息记录表_行项目</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZJSNO</td>
<td>2</td>
<td>X</td>
<td>ZJSNO</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>发票申请号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZJSPOSNR</td>
<td>3</td>
<td>X</td>
<td>ZJSPOSNR</td>
<td>CHAR6</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>发票申请号行项目</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>FKART</td>
<td>4</td>
<td>&nbsp;</td>
<td>FKART</td>
<td>FKART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>开票类型</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>MABER</td>
<td>5</td>
<td>&nbsp;</td>
<td>MABER</td>
<td>MABER</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>催款范围</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>VBELN</td>
<td>6</td>
<td>&nbsp;</td>
<td>VBELN</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>销售和分销凭证号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>POSNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>POSNR_VF</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>开票项目</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZFPDM</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZFPDM</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>金税发票代码</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZFPHM</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZFPHM</td>
<td>CHAR8</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>金税发票号码</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZFPKJZT</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZFPKJZT</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>金税发票开具状态</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZFPZFZT</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZFPZFZT</td>
<td>CHAR4</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>金税发票作废状态</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZFPDYZT</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZFPDYZT</td>
<td>CHAR4</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>金税发票打印状态</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZHZSQZT</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZHZSQZT</td>
<td>CHAR4</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>金税发票专用发票申请红字信息表状态</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>