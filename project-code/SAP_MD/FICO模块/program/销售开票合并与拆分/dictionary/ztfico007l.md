<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007L</h2>
<h3>Description: 金税开具含项目表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZJSNO</td>
<td>2</td>
<td>X</td>
<td>ZJSNO</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>发票申请号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZJSXMLX</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZJSXMLX</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>明细类型</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MAKTX</td>
<td>4</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>VRKME</td>
<td>5</td>
<td>&nbsp;</td>
<td>VRKME</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>销售单位</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>GROES</td>
<td>6</td>
<td>&nbsp;</td>
<td>GROES</td>
<td>CHAR32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>规格</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>FKIMG</td>
<td>7</td>
<td>&nbsp;</td>
<td>FKIMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际已开票数量</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZSTDJ</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZSTDJ</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>单价含税</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZSFDJ</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZSFDJ</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>单价不含税</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZJSDJ</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZJSDJ</td>
<td>CHAR21</td>
<td>CHAR</td>
<td>21</td>
<td>&nbsp;</td>
<td>传输单价</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZSL</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZSL</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>税率（非税码）</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MWSI</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZMWSI</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>税率</td>
</tr>
<tr class="cell">
<td>13</td>
<td>NETWR</td>
<td>13</td>
<td>&nbsp;</td>
<td>NETWR</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>凭证货币计量的净价值</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MWSBP</td>
<td>14</td>
<td>&nbsp;</td>
<td>MWSBP</td>
<td>WERTV7</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>以凭证货币计的税额</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZFLBM</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZEZFLBM</td>
<td>NUMC20</td>
<td>NUMC</td>
<td>20</td>
<td>&nbsp;</td>
<td>税分类编码</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZWDW</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZZWDW</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>单位</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>