<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007X</h2>
<h3>Description: 金税特许单位开票配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>PARTNER</td>
<td>3</td>
<td>X</td>
<td>BU_PARTNER</td>
<td>BU_PARTNER</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>业务伙伴编号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>NAME_ORG1</td>
<td>4</td>
<td>&nbsp;</td>
<td>BU_NAMEOR1</td>
<td>BU_NAME</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>组织名称 1</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZMEINS</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZMEINS</td>
<td>ZMD_ZMEINS</td>
<td>CHAR</td>
<td>3</td>
<td>X</td>
<td>开票特许单位</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZDWMX</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZDWMX</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>单位描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZMD_ZMEINS</td>
<td>PAC</td>
<td>&nbsp;</td>
<td>包</td>
</tr>
<tr class="cell">
<td>ZMD_ZMEINS</td>
<td>BAG</td>
<td>&nbsp;</td>
<td>袋</td>
</tr>
<tr class="cell">
<td>ZMD_ZMEINS</td>
<td>PC</td>
<td>&nbsp;</td>
<td>件</td>
</tr>
<tr class="cell">
<td>ZMD_ZMEINS</td>
<td>KG</td>
<td>&nbsp;</td>
<td>千克</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>