<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO003_SEL</h2>
<h3> Description: Include ZFICO003_SEL</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO003_SEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK bk WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_file&nbsp;LIKE&nbsp;rlgrap-filename&nbsp;.&nbsp;"导入数据文件路径<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;r_b1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rb1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;r_b2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rb1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;r_b3&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rb1.<br/>
SELECTION-SCREEN END OF BLOCK bk.<br/>
<br/>
SELECTION-SCREEN FUNCTION KEY 1.<br/>
<br/>
INITIALIZATION.<br/>
MOVE '下载模板' TO sscrfields-functxt_01.<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.<br/>
PERFORM frm_upload.<br/>
<br/>
AT SELECTION-SCREEN.<br/>
PERFORM frm_download_template.<br/>
<br/>
START-OF-SELECTION.<br/>
PERFORM frm_import.<br/>
<br/>
IF it_tab[] IS NOT INITIAL.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_show_data.<br/>
ENDIF.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>