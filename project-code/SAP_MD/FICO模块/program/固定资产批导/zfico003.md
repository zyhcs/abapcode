<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO003</h2>
<h3> Description: 固定资产批导</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO003<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO003.<br/>
<br/>
include <a href="zfico003_top.html">zfico003_top</a>.<br/>
<br/>
include <a href="zfico003_sel.html">zfico003_sel</a>.<br/>
<br/>
include <a href="zfico003_f01.html">zfico003_f01</a>.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_FILE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;路径<br/>
*&nbsp;R_B1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历史资产测试导入<br/>
*&nbsp;R_B2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历史资产导入<br/>
*&nbsp;R_B3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资产批量修改<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;00<br/>
*398&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;ZFICO001<br/>
*001&nbsp;&nbsp;&nbsp;请输入导入文件路径!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>