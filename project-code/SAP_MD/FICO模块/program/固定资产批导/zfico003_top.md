<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO003_TOP</h2>
<h3> Description: Include ZFICO003_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO003_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES:sscrfields."zacc_document.<br/>
<br/>
DATA: BEGIN OF it_tab OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlkl&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-anlkl,&nbsp;&nbsp;&nbsp;&nbsp;"资产分类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-bukrs,&nbsp;&nbsp;&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;gjahr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"财年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt50&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-txt50,&nbsp;&nbsp;&nbsp;&nbsp;"名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txa50&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-txa50,&nbsp;&nbsp;&nbsp;&nbsp;"描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlhtxt&nbsp;&nbsp;LIKE&nbsp;anlh-anlhtxt,&nbsp;&nbsp;"主号文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anln1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;anla-anln1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sernr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-sernr,&nbsp;&nbsp;&nbsp;&nbsp;"规格型号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invnr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-invnr,&nbsp;&nbsp;&nbsp;&nbsp;"存放地点<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-menge,&nbsp;&nbsp;&nbsp;&nbsp;"数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-meins,&nbsp;&nbsp;&nbsp;&nbsp;"单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invzu&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-invzu,&nbsp;&nbsp;&nbsp;&nbsp;"原卡片号<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posnr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-posnr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"政府拨款项目<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi1022_posnr_ext,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"政府拨款项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eaufn&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-eaufn,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herst&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-herst,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"制造厂家<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-lifnr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"采购供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostlv&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlz-kostlv,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"管理部门<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pernr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlz-pernr,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"人员<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aktiv&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-aktiv,&nbsp;&nbsp;&nbsp;&nbsp;"资本化日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kostl,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ord41&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-ord41,&nbsp;&nbsp;&nbsp;&nbsp;"使用状况<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ord42&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-ord42,&nbsp;&nbsp;&nbsp;&nbsp;"资产来源<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ord43&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-ord43,&nbsp;&nbsp;&nbsp;&nbsp;"资产用途<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gdlgrp&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-gdlgrp,&nbsp;&nbsp;&nbsp;"责任人<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afasl1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afasl,&nbsp;&nbsp;&nbsp;&nbsp;"折旧码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndjar1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndjar,&nbsp;&nbsp;&nbsp;&nbsp;"使用年限<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndper1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndper,&nbsp;&nbsp;&nbsp;&nbsp;"使用月份<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabg1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabg,&nbsp;&nbsp;&nbsp;&nbsp;"折旧开始日期<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afasl2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afasl,&nbsp;&nbsp;&nbsp;&nbsp;"折旧码<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndjar2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndjar,&nbsp;&nbsp;&nbsp;&nbsp;"使用年限<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndper2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndper,&nbsp;&nbsp;&nbsp;&nbsp;"使用月份<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabg2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabg,&nbsp;&nbsp;&nbsp;&nbsp;"折旧开始日期<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afasl3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afasl,&nbsp;&nbsp;&nbsp;&nbsp;"折旧码<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndjar3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndjar,&nbsp;&nbsp;&nbsp;&nbsp;"使用年限<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ndper3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-ndper,&nbsp;&nbsp;&nbsp;&nbsp;"使用月份<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabg3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlb-afabg,&nbsp;&nbsp;&nbsp;&nbsp;"折旧开始日期<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe1c&nbsp;&nbsp;LIKE&nbsp;anlc-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kansw1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-kansw,&nbsp;&nbsp;&nbsp;&nbsp;"购置价值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;knafa1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-knafa,&nbsp;&nbsp;&nbsp;&nbsp;"以前年度折旧<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nafag1&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-nafag,&nbsp;&nbsp;&nbsp;&nbsp;"本年已计提折旧<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kaufw&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bf_kaufw,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"以前年度减值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufwb&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bf_aufwb,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当前年度减值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ltext&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;50,&nbsp;&nbsp;&nbsp;"长文本<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe2c&nbsp;&nbsp;LIKE&nbsp;anlc-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kansw2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-kansw,&nbsp;&nbsp;&nbsp;&nbsp;"购置价值<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;knafa2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-knafa,&nbsp;&nbsp;&nbsp;&nbsp;"以前年度折旧<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nafag2&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-nafag,&nbsp;&nbsp;&nbsp;&nbsp;"本年已计提折旧<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;afabe3c&nbsp;&nbsp;LIKE&nbsp;anlc-afabe,&nbsp;&nbsp;&nbsp;&nbsp;"折旧范围<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kansw3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-kansw,&nbsp;&nbsp;&nbsp;&nbsp;"购置价值<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;knafa3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-knafa,&nbsp;&nbsp;&nbsp;&nbsp;"以前年度折旧<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nafag3&nbsp;&nbsp;&nbsp;LIKE&nbsp;anlc-nafag,&nbsp;&nbsp;&nbsp;&nbsp;"本年已计提折旧<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;light(4),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg(200),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;it_tab.<br/>
<br/>
CONSTANTS: cns_file TYPE char50 VALUE '固定资产导入模板'.<br/>
CONSTANTS: cns_file_filter TYPE char100 VALUE 'Excel Files (*.xlsx)|*.xlsx'.<br/>
CONSTANTS: cns_template_id TYPE wwwdatatab-objid VALUE 'ZFICO003'.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>