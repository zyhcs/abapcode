<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015A_FORM</h2>
<h3> Description: Include ZFICO015A_FORM</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO015A_FORM<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_main<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
form download .<br/>
&nbsp;&nbsp;data:&nbsp;l_filename&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fullpath&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname&nbsp;type&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;check&nbsp;sy-ucomm&nbsp;=&nbsp;'FC01'.<br/>
<br/>
&nbsp;&nbsp;case&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;when&nbsp;'FC01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename&nbsp;=&nbsp;text-003.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname&nbsp;=&nbsp;'ZFICO015A'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;when&nbsp;others.<br/>
&nbsp;&nbsp;endcase.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Get&nbsp;the&nbsp;file&nbsp;path<br/>
</div>
<div class="code">
&nbsp;&nbsp;perform&nbsp;get_fullpath&nbsp;changing&nbsp;l_fullpath&nbsp;l_path&nbsp;l_filename.<br/>
<br/>
&nbsp;&nbsp;if&nbsp;l_fullpath&nbsp;is&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;s066(zcewm01)&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;return.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
&nbsp;&nbsp;perform&nbsp;download_template&nbsp;using&nbsp;l_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path.<br/>
endform.<br/>
<br/>
form get_fullpath  changing p_fullpath type string<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_path&nbsp;type&nbsp;string<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_name&nbsp;type&nbsp;string.<br/>
&nbsp;&nbsp;data:&nbsp;l_init_path&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename&nbsp;&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fullpath&nbsp;&nbsp;type&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;l_filename&nbsp;=&nbsp;p_name.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---用户选择名称、路径<br/>
</div>
<div class="code">
&nbsp;&nbsp;call&nbsp;method&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'XLS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prompt_on_overwrite&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;changing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;exceptions<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;others&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
&nbsp;&nbsp;if&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_fullpath&nbsp;=&nbsp;l_fullpath.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_path.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
endform.<br/>
<br/>
form download_template  using    p_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_tempname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_path.<br/>
&nbsp;&nbsp;data:&nbsp;lwa_objdata&nbsp;&nbsp;&nbsp;like&nbsp;wwwdatatab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_destination&nbsp;like&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_objnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_errtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;data:&nbsp;lv_filename&nbsp;type&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_result,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_subrc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;sy-subrc.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---查找文件是否存在。<br/>
</div>
<div class="code">
&nbsp;&nbsp;select&nbsp;single&nbsp;relid&nbsp;objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;from&nbsp;wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;into&nbsp;corresponding&nbsp;fields&nbsp;of&nbsp;lwa_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;where&nbsp;srtf2&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;&nbsp;&nbsp;relid&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'MI'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;&nbsp;&nbsp;objid&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_tempname.<br/>
<br/>
&nbsp;&nbsp;l_destination&nbsp;&nbsp;&nbsp;=&nbsp;p_fullpath.<br/>
<br/>
&nbsp;&nbsp;clear:&nbsp;l_subrc.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---下载模版。<br/>
</div>
<div class="code">
&nbsp;&nbsp;call&nbsp;function&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lwa_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;l_destination<br/>
&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_subrc.<br/>
&nbsp;&nbsp;if&nbsp;l_subrc&nbsp;ne&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;s067(zcewm01)&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
endform.<br/>
<br/>
form f4_help_file .<br/>
&nbsp;&nbsp;call&nbsp;function&nbsp;'F4_FILENAME'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_name&nbsp;=&nbsp;p_file.<br/>
endform.<br/>
<br/>
<br/>
form frm_main .<br/>
</div>
<div class="codeComment">
*&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="code">
&nbsp;&nbsp;if&nbsp;p_file&nbsp;is&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;'请导入文件信息表'&nbsp;TYPE&nbsp;'S'&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;stop.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
&nbsp;&nbsp;perform&nbsp;upload_file.<br/>
</div>
<div class="codeComment">
*<br/>
**&nbsp;Check&nbsp;&amp;&nbsp;Prepare&nbsp;Data<br/>
</div>
<div class="code">
&nbsp;&nbsp;perform&nbsp;check_prepare_data.<br/>
<br/>
&nbsp;&nbsp;perform&nbsp;save_data.<br/>
</div>
<div class="codeComment">
**<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;display_mingxi.<br/>
*<br/>
*&nbsp;&nbsp;ELSEIF&nbsp;p_chaxun&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_display_chaxun.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
endform.<br/>
<br/>
<br/>
form upload_file .<br/>
&nbsp;&nbsp;data:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lit_intern&nbsp;type&nbsp;standard&nbsp;table&nbsp;of&nbsp;alsmex_tabline,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lwa_intern&nbsp;like&nbsp;line&nbsp;of&nbsp;lit_intern,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lwa_import&nbsp;like&nbsp;line&nbsp;of&nbsp;it_import,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;i.<br/>
&nbsp;&nbsp;field-symbols:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_temp&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;any.<br/>
<br/>
&nbsp;&nbsp;call&nbsp;function&nbsp;'ALSM_EXCEL_TO_INTERNAL_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;65536<br/>
&nbsp;&nbsp;&nbsp;&nbsp;tables<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lit_intern<br/>
&nbsp;&nbsp;&nbsp;&nbsp;exceptions<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inconsistent_parameters&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;upload_ole&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;others&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;if&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;File&nbsp;cannot&nbsp;be&nbsp;Uploaded<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;s001(zcl)&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;leave&nbsp;list-processing.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;get&nbsp;file&nbsp;data<br/>
</div>
<div class="code">
&nbsp;&nbsp;sort&nbsp;lit_intern&nbsp;by&nbsp;row&nbsp;col.<br/>
<br/>
&nbsp;&nbsp;loop&nbsp;at&nbsp;lit_intern&nbsp;into&nbsp;lwa_intern.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_col&nbsp;=&nbsp;lwa_intern-col.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;assign&nbsp;component&nbsp;l_col&nbsp;of&nbsp;structure&nbsp;lwa_import&nbsp;to&nbsp;&lt;fs_temp&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;&lt;fs_temp&gt;&nbsp;is&nbsp;assigned.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_temp&gt;&nbsp;=&nbsp;lwa_intern-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unassign&nbsp;&lt;fs_temp&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;lwa_import-zrow&nbsp;is&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lwa_import-zrow&nbsp;=&nbsp;lwa_intern-row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;at&nbsp;end&nbsp;of&nbsp;row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;append&nbsp;lwa_import&nbsp;to&nbsp;it_import.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clear:&nbsp;lwa_import.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endat.<br/>
&nbsp;&nbsp;endloop.<br/>
endform.<br/>
<br/>
<br/>
form check_prepare_data .<br/>
&nbsp;&nbsp;data:ls_import&nbsp;type&nbsp;t_import.<br/>
&nbsp;&nbsp;data:l_error&nbsp;type&nbsp;char10.<br/>
&nbsp;&nbsp;data:n&nbsp;type&nbsp;n&nbsp;length&nbsp;13.<br/>
&nbsp;&nbsp;data:l_type&nbsp;type&nbsp;dd01v-datatype.<br/>
&nbsp;&nbsp;data:l_xiangshu&nbsp;type&nbsp;c&nbsp;length&nbsp;13.<br/>
<br/>
<br/>
&nbsp;&nbsp;loop&nbsp;at&nbsp;it_import&nbsp;assigning&nbsp;field-symbol(&lt;fs_import&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs_import&gt;-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs_import&gt;-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;select&nbsp;single&nbsp;matnr&nbsp;into&nbsp;@data(lv_matnr)&nbsp;from&nbsp;mara&nbsp;where&nbsp;matnr&nbsp;=&nbsp;@&lt;fs_import&gt;-matnr.&nbsp;"检查物料编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_error&nbsp;=&nbsp;'物料不存在：'&nbsp;&amp;&amp;&nbsp;&lt;fs_import&gt;-matnr&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"检查公司<br/>
&nbsp;&nbsp;&nbsp;&nbsp;select&nbsp;single&nbsp;butxt&nbsp;into&nbsp;@data(lv_butxt)&nbsp;from&nbsp;t001&nbsp;where&nbsp;bukrs&nbsp;=&nbsp;@&lt;fs_import&gt;-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_error&nbsp;=&nbsp;'公司不存在:'&nbsp;&amp;&amp;&nbsp;&lt;fs_import&gt;-bukrs&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"检查客户<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs_import&gt;-kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs_import&gt;-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;select&nbsp;single&nbsp;name1&nbsp;into&nbsp;@data(lv_name1)&nbsp;from&nbsp;kna1&nbsp;where&nbsp;kunnr&nbsp;=&nbsp;@&lt;fs_import&gt;-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_error&nbsp;=&nbsp;'客户不存在:'&nbsp;&amp;&amp;&nbsp;&lt;fs_import&gt;-bukrs&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;move-corresponding&nbsp;&lt;fs_import&gt;&nbsp;to&nbsp;ls_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;append&nbsp;ls_ZTFICO007D&nbsp;to&nbsp;lt_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;clear:ls_ZTFICO007D.<br/>
&nbsp;&nbsp;endloop.<br/>
<br/>
&nbsp;&nbsp;if&nbsp;l_error&nbsp;is&nbsp;not&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;l_error&nbsp;type&nbsp;'E'&nbsp;display&nbsp;like&nbsp;'I'.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
endform.<br/>
<br/>
form save_data .<br/>
&nbsp;&nbsp;data:&nbsp;lcl_root&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;ref&nbsp;to&nbsp;cx_root.<br/>
&nbsp;&nbsp;if&nbsp;lt_ZTFICO007D&nbsp;is&nbsp;not&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;loop&nbsp;at&nbsp;lt_ZTFICO007D&nbsp;into&nbsp;ls_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;select&nbsp;single&nbsp;butxt&nbsp;into&nbsp;ls_ZTFICO007D-butxt&nbsp;from&nbsp;t001&nbsp;where&nbsp;bukrs&nbsp;=&nbsp;ls_ZTFICO007D-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;select&nbsp;single&nbsp;name1&nbsp;into&nbsp;ls_ZTFICO007D-name1&nbsp;from&nbsp;kna1&nbsp;where&nbsp;kunnr&nbsp;=&nbsp;ls_ZTFICO007D-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;lt_ZTFICO007D&nbsp;from&nbsp;ls_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clear:ls_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;endloop.<br/>
&nbsp;&nbsp;endif.<br/>
&nbsp;&nbsp;try.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;lt_ZTFICO007D&nbsp;is&nbsp;not&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;ztfico007d&nbsp;from&nbsp;table&nbsp;lt_ZTFICO007D.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;commit&nbsp;work&nbsp;and&nbsp;wait.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;catch&nbsp;cx_root&nbsp;into&nbsp;lcl_root.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rollback&nbsp;work.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;'导入失败'&nbsp;TYPE&nbsp;'S'&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;exit.<br/>
&nbsp;&nbsp;endtry.<br/>
&nbsp;&nbsp;message&nbsp;'导入成功'&nbsp;type&nbsp;'S'&nbsp;display&nbsp;like&nbsp;'I'.<br/>
endform.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>