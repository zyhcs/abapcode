<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015A_DATA</h2>
<h3> Description: Include ZFICO015A_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO015A_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
<br/>
</div>
<div class="code">
TYPE-POOLS: icon.<br/>
TABLES:sscrfields,ZTFICO007D,mara.<br/>
<br/>
<br/>
types: BEGIN OF t_import,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUKRS&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZTFICO007D-BUKRS,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KUNNR&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZTFICO007D-KUNNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MATNR&nbsp;TYPE&nbsp;ZTFICO007D-MATNR,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZMTXT&nbsp;&nbsp;&nbsp;TYPE&nbsp;ZTFICO007D-ZMTXT,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mark&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kcd_ex_row_n,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;light&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;t_import.<br/>
<br/>
<br/>
DATA:lt_ZTFICO007D TYPE STANDARD TABLE OF ZTFICO007D.<br/>
DATA:ls_ZTFICO007D TYPE ZTFICO007D.<br/>
DATA: it_import TYPE TABLE OF t_import.<br/>
<br/>
</div>
<div class="codeComment">
*DATA:&nbsp;wt_fieldcat&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
<br/>
<br/>
</div>
<div class="code">
DATA:wt_lqua LIKE TABLE OF lqua.<br/>
<br/>
SELECTION-SCREEN FUNCTION KEY 1.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.<br/>
<br/>
PARAMETERS: p_file TYPE localfile.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK b1.<br/>
<br/>
INITIALIZATION.<br/>
&nbsp;&nbsp;sscrfields-functxt_01&nbsp;=&nbsp;text-002.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;selection_sreen_pbo.<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;download.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"上传模板关键代码<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.  "文件路径搜索帮助<br/>
&nbsp;&nbsp;PERFORM&nbsp;f4_help_file.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>