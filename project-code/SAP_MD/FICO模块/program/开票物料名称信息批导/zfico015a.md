<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015A</h2>
<h3> Description: 开票物料名称信息批导</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO015A<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO015A.<br/>
<br/>
<br/>
include <a href="zfico015a_data.html">ZFICO015A_data</a>.<br/>
<br/>
include <a href="zfico015a_form.html">ZFICO015A_form</a>.<br/>
<br/>
START-OF-SELECTION.<br/>
PERFORM frm_main.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;002&nbsp;模板下载<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_FILE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;文件路径<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;请导入文件信息表<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>