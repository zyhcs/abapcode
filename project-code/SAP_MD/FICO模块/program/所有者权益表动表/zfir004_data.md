<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFIR004_DATA</h2>
<h3> Description: Include ZFIR004_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFIR004_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES:acdoca,t001.<br/>
data:gt_two like table of zfisyzqy.<br/>
data:gs_two like line of gt_two.<br/>
data:gt_one like table of zfisyzqy.<br/>
data:gs_one like line of gt_one.<br/>
data:gt_three like table of zfisyzqy.<br/>
data:gs_three like line of gt_three.<br/>
DATA: gc_path        TYPE  string.<br/>
data:gt_ACDOCT type table of acdoct.<br/>
data:gs_acdoct like line of gt_ACDOCT.<br/>
DATA:g_fldbz TYPE char3.<br/>
DATA:l_bukrs TYPE char100.<br/>
"DOI相关参数<br/>
</div>
<div class="codeComment">
*确定模板在BDS上的位置<br/>
</div>
<div class="code">
data:<br/>
&nbsp;&nbsp;g_doc_classname&nbsp;&nbsp;type&nbsp;sbdst_classname&nbsp;value&nbsp;'SOFFICEINTEGRATION',&nbsp;&nbsp;&nbsp;"OAOR参数定义<br/>
&nbsp;&nbsp;g_doc_classtype&nbsp;&nbsp;type&nbsp;sbdst_classtype&nbsp;value&nbsp;'OT',<br/>
&nbsp;&nbsp;g_doc_object_key&nbsp;type&nbsp;sbdst_object_key&nbsp;value&nbsp;'ZFIR004',<br/>
&nbsp;&nbsp;g_prop_value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;bapisignat-prop_value&nbsp;value&nbsp;'所有者权益',<br/>
&nbsp;&nbsp;g_prop_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;bapisignat-prop_name&nbsp;value&nbsp;'DESCRIPTION'.<br/>
</div>
<div class="codeComment">
*本地文件路径变量说明<br/>
</div>
<div class="code">
data:<br/>
&nbsp;&nbsp;g_def_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"默认文件名<br/>
&nbsp;&nbsp;g_item_url(256),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"模板在BDS上的URL地址<br/>
&nbsp;&nbsp;g_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"保存的文件路径<br/>
&nbsp;&nbsp;g_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"要保存文件的文件名<br/>
&nbsp;&nbsp;g_fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"全路径（路径+文件名）<br/>
&nbsp;&nbsp;g_user_action&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;i,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用户操作（0标示点击了‘保存’，9表示‘取消’）<br/>
&nbsp;&nbsp;g_file_name(200).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"全路径名<br/>
<br/>
</div>
<div class="codeComment">
*DOI变量申明<br/>
</div>
<div class="code">
data:<br/>
&nbsp;&nbsp;gcl_container&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;ref&nbsp;to&nbsp;cl_gui_custom_container,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"容器<br/>
&nbsp;&nbsp;gcl_control&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;ref&nbsp;to&nbsp;i_oi_container_control,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"DOI实例<br/>
&nbsp;&nbsp;gcl_document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;ref&nbsp;to&nbsp;i_oi_document_proxy,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用来打开BDS上文件的类<br/>
&nbsp;&nbsp;gcl_spreadsheet&nbsp;&nbsp;type&nbsp;ref&nbsp;to&nbsp;i_oi_spreadsheet,<br/>
&nbsp;&nbsp;gcl_errors&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;table&nbsp;of&nbsp;ref&nbsp;to&nbsp;i_oi_error&nbsp;with&nbsp;header&nbsp;line,&nbsp;"用来存储各个方法中产生的错误<br/>
&nbsp;&nbsp;g_retcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type&nbsp;soi_ret_string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"存放调用DOI方法之后的返回值<br/>
&nbsp;&nbsp;gcl_bds_instance&nbsp;type&nbsp;ref&nbsp;to&nbsp;cl_bds_document_set.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"此类用来获取上传的文档<br/>
<br/>
"取数内表定义<br/>
<br/>
<br/>
selection-screen begin of block blk1 with frame title text-001.<br/>
&nbsp;&nbsp;parameters:p_bukrs&nbsp;like&nbsp;t001-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_GJAHR&nbsp;like&nbsp;acdoca-gjahr.<br/>
&nbsp;&nbsp;select-options:s_POPER&nbsp;for&nbsp;acdoca-poper.<br/>
&nbsp;&nbsp;PARAMETERS:p_r1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_r2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1.<br/>
selection-screen end of block blk1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>