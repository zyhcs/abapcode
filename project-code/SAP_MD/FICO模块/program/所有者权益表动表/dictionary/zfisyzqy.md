<table class="outerTable">
<tr>
<td><h2>Table: ZFISYZQY</h2>
<h3>Description: 所有者权益结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZXMC</td>
<td>1</td>
<td>&nbsp;</td>
<td>CHAR50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>注释</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZXH</td>
<td>2</td>
<td>&nbsp;</td>
<td>CHAR2</td>
<td>CHAR2</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>版本号组件</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZSSZB</td>
<td>3</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZZBGJ</td>
<td>4</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZKCG</td>
<td>5</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZZXCB</td>
<td>6</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZYYGJ</td>
<td>7</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZYBFX</td>
<td>8</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZWFPLY</td>
<td>9</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZQT</td>
<td>10</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZXJ</td>
<td>11</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZSYZQY</td>
<td>12</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZSSZB_C</td>
<td>13</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZZBGJ_C</td>
<td>14</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZKCG_C</td>
<td>15</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZXCB_C</td>
<td>16</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZYYGJ_C</td>
<td>17</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZYBFX_C</td>
<td>18</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZWFPLY_C</td>
<td>19</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZQT_C</td>
<td>20</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZXJ_C</td>
<td>21</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZSYZQY_C</td>
<td>22</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>