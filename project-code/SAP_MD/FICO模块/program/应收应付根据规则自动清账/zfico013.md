<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO013</h2>
<h3> Description: 应收应付根据规则自动清账</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO013<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO013.<br/>
<br/>
include <a href="zfico013_top.html">ZFICO013_top</a>.<br/>
<br/>
include <a href="zfico013_screen.html">ZFICO013_screen</a>.<br/>
<br/>
include <a href="zfico013_f01.html">ZFICO013_f01</a>.<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;SELECTION-SCREEN&nbsp;&nbsp;处理载入屏幕字段事件<br/>
*-----------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN OUTPUT.<br/>
&nbsp;&nbsp;&nbsp;"选择屏幕处理<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_set_screen.<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;START-OF-SELECTION&nbsp;&nbsp;&nbsp;数据检索（程序主体部分）<br/>
*-----------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
&nbsp;&nbsp;PERFORM&nbsp;CHECK_AUTH.<br/>
</div>
<div class="codeComment">
*&nbsp;执行查询数据逻辑<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;sub_get_data.<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END-OF-SELECTION&nbsp;&nbsp;&nbsp;数据显示<br/>
*-----------------------------------------------------------------*<br/>
</div>
<div class="code">
END-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*&nbsp;数据展示<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;sub_show_alv.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;W01&nbsp;您没有操作<br/>
*&nbsp;W02&nbsp;公司代码的权限！<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;P_YFL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应付类<br/>
*&nbsp;P_YSL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应收类<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>