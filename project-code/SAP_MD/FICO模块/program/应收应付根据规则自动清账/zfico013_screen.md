<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO013_SCREEN</h2>
<h3> Description: Include ZFICO013_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO013_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN<br/>
*---------------定义选择屏幕*--------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.<br/>
SELECT-OPTIONS: s_bukrs FOR bsik-bukrs OBLIGATORY.  "公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:P_budat&nbsp;like&nbsp;bsik-budat&nbsp;OBLIGATORY&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"s_prctr&nbsp;FOR&nbsp;cepct-prctr&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
SELECT-OPTIONS: s_lifnr   FOR bsik-lifnr MODIF ID m1,  "供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kunnr&nbsp;&nbsp;&nbsp;FOR&nbsp;bsid-kunnr&nbsp;MODIF&nbsp;ID&nbsp;m2.&nbsp;&nbsp;&nbsp;"客户<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_yfl&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1&nbsp;USER-COMMAND&nbsp;uc1&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_ysl&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;b1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>