<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO013_TOP</h2>
<h3> Description: Include ZFICO013_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO013_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;表/结构/视图声明<br/>
*&amp;-----------------------------------------------------------------*<br/>
<br/>
<br/>
</div>
<div class="code">
TABLES:bsid,bsik,t001,lfa1,cepct.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*ALV用到的内表<br/>
<br/>
<br/>
</div>
<div class="code">
TYPES : BEGIN OF typ_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;icon&nbsp;&nbsp;&nbsp;TYPE&nbsp;CHAR4,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;&nbsp;&nbsp;TYPE&nbsp;uuid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;TYPE&nbsp;bsik-bukrs,&nbsp;&nbsp;&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;butxt&nbsp;&nbsp;TYPE&nbsp;t001-butxt,&nbsp;&nbsp;&nbsp;&nbsp;"公司说明<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;TYPE&nbsp;bsik-lifnr,&nbsp;&nbsp;&nbsp;&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;TYPE&nbsp;bsid-kunnr,&nbsp;&nbsp;&nbsp;&nbsp;"客户<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;&nbsp;&nbsp;&nbsp;"供应商描述&nbsp;or&nbsp;客户描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umskz&nbsp;&nbsp;TYPE&nbsp;bsik-umskz,&nbsp;&nbsp;&nbsp;&nbsp;"特别总账标记<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;TYPE&nbsp;bsik-budat,&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;TYPE&nbsp;bsik-belnr,&nbsp;&nbsp;&nbsp;&nbsp;"凭证编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;TYPE&nbsp;gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;blart&nbsp;&nbsp;TYPE&nbsp;bsik-blart,&nbsp;&nbsp;&nbsp;&nbsp;"凭证类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;TYPE&nbsp;bsik-dmbtr,&nbsp;&nbsp;&nbsp;&nbsp;"金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prctr&nbsp;&nbsp;TYPE&nbsp;bsik-prctr,&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;&nbsp;TYPE&nbsp;cepct-ktext,&nbsp;&nbsp;&nbsp;"利润中心描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"awref&nbsp;&nbsp;TYPE&nbsp;awref,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"发票凭证<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shkzg&nbsp;&nbsp;TYPE&nbsp;shkzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bschl&nbsp;&nbsp;TYPE&nbsp;bschl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;slbox&nbsp;&nbsp;TYPE&nbsp;icon-internal,&nbsp;"判断数据是否正确<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cleaed&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"已清账<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mess&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"是否创建成功<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gty_alv&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;typ_alv.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*内表定义<br/>
<br/>
<br/>
</div>
<div class="code">
DATA: gt_alv TYPE STANDARD TABLE OF typ_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_layo&nbsp;TYPE&nbsp;lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_fcat&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_grid&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;变量定义<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>