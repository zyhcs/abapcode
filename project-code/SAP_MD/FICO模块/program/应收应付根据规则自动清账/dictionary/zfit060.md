<table class="outerTable">
<tr>
<td><h2>Table: ZFIT060</h2>
<h3>Description: 自动清账凭证记录表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BELNR</td>
<td>2</td>
<td>X</td>
<td>BELNR_D</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>会计凭证号码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUKRS</td>
<td>3</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>GJAHR</td>
<td>4</td>
<td>X</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ERDAT</td>
<td>5</td>
<td>&nbsp;</td>
<td>ERDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>记录创建日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ERZET</td>
<td>6</td>
<td>&nbsp;</td>
<td>ERZET</td>
<td>UZEIT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>输入时间</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ERNAM</td>
<td>7</td>
<td>&nbsp;</td>
<td>ERNAM</td>
<td>USNAM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>创建对象的人员名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>