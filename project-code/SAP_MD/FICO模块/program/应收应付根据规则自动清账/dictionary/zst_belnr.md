<table class="outerTable">
<tr>
<td><h2>Table: ZST_BELNR</h2>
<h3>Description: 凭证编号</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BELNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>BELNR_D</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>会计凭证号码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>WRBTR</td>
<td>2</td>
<td>&nbsp;</td>
<td>WRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>凭证货币金额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>