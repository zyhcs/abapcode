<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO019</h2>
<h3>Description: 应上缴弥补款项表数据</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZYEAR</td>
<td>3</td>
<td>X</td>
<td>RYEAR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZPMAX</td>
<td>4</td>
<td>X</td>
<td>RPMAX</td>
<td>RPMAX</td>
<td>NUMC</td>
<td>3</td>
<td>&nbsp;</td>
<td>期间</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZINDEX</td>
<td>5</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>序号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BUTXT</td>
<td>6</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZNAME</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>项目名称</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>HSLSM</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>按本位币的期间中的业务总计</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>HSLSM_M</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>当月本位币期间值</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>