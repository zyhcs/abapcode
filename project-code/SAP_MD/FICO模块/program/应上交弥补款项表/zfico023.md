<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO023</h2>
<h3> Description: 应上交弥补款项表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&nbsp;程序名：&nbsp;&nbsp;ZFICO023<br/>
*&nbsp;开发日期：2021-09-09<br/>
*&nbsp;创建者：&nbsp;&nbsp;&nbsp;彭志<br/>
*---------------------------------------------------------*<br/>
*&nbsp;概要说明<br/>
*---------------------------------------------------------*<br/>
*&nbsp;&nbsp;应上交弥补款项表<br/>
*---------------------------------------------------------*<br/>
*&nbsp;变更记录<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;日期&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改者&nbsp;&nbsp;&nbsp;&nbsp;传输请求号&nbsp;&nbsp;&nbsp;&nbsp;修改内容及原因<br/>
*---------------------------------------------------------*<br/>
*&nbsp;2021-09-09&nbsp;&nbsp;&nbsp;&nbsp;彭志&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S4DK902056&nbsp;&nbsp;&nbsp;&nbsp;初始化<br/>
*---------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfico023.<br/>
TABLES:t001,acdoct.<br/>
<br/>
TYPES : BEGIN OF ty_tab ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ryear&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-ryear,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-drcrk,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"借方/贷方标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rpmax&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-rpmax,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"期间<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;LIKE&nbsp;acdoct-rtcur,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"货币码<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-racct,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"帐号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbukrs&nbsp;&nbsp;LIKE&nbsp;acdoct-rbukrs,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公司<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;LIKE&nbsp;acdoct-rfarea,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"功能范围<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hslvt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"年初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl01&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl01,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"1月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl02&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl02,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"2月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl03&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl03,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"3月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl04&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl04,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"4月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl05&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl05,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"5月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl06&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl06,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"6月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl07&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl07,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"7月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl08&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl08,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"8月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl09&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl09,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"9月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl10&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl10,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"10月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl11&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl11,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"11月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl12&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl12,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"12月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl13&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl13,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"13月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl14&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl14,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"14月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl15&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl15,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"15月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl16&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"16月余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"往期累加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm_m&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当月数据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_tab.<br/>
</div>
<div class="codeComment">
*DATA:&nbsp;gt_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_tab_clt&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab.<br/>
<br/>
</div>
<div class="code">
"最终显示alv<br/>
TYPES:BEGIN OF ty_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;60,&nbsp;&nbsp;"项目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm_m&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当月<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"本年累加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;60,&nbsp;&nbsp;"项目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index2&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm_m2&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当月<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm2&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"本年累加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_alv.<br/>
DATA gt_alv TYPE STANDARD TABLE OF ty_alv.<br/>
DATA gt_alv_index TYPE STANDARD TABLE OF ty_alv.<br/>
<br/>
<br/>
<br/>
"配置表，基本信息,科目号<br/>
TYPES:BEGIN OF ty_prjs_racct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;60,&nbsp;&nbsp;"项目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;TYPE&nbsp;acdoct-racct,&nbsp;&nbsp;"科目号列表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;TYPE&nbsp;acdoct-drcrk,&nbsp;&nbsp;"借贷标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_prjs_racct.<br/>
DATA gt_prjs_racct TYPE STANDARD TABLE OF ty_prjs_racct.<br/>
<br/>
"配置表，基本信息,累计序号<br/>
TYPES:BEGIN OF ty_prjs_smidx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;60,&nbsp;&nbsp;"项目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smidx&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;220,&nbsp;&nbsp;"汇总的序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_prjs_smidx.<br/>
DATA gt_prjs_smidx TYPE STANDARD TABLE OF ty_prjs_smidx.<br/>
<br/>
"配置表：序号科目对应表<br/>
TYPES:BEGIN OF ty_index_racct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;255,&nbsp;&nbsp;"科目号列表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;TYPE&nbsp;acdoct-drcrk,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"借贷标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_index_racct.<br/>
DATA gt_index_racct TYPE STANDARD TABLE OF ty_index_racct.<br/>
<br/>
"序号对应合计值<br/>
TYPES : BEGIN OF ty_index_hslsm ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;index&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm_m&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当月<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslsm&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"当年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_index_hslsm.<br/>
<br/>
<br/>
DATA : gs_index_hslsm TYPE ty_index_hslsm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_index_hslsm&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_index_hslsm.<br/>
<br/>
RANGES rg_racct FOR acdoct-racct.<br/>
<br/>
DATA:gds_alv_layout_lvc   TYPE lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gdt_alv_fieldcat_lvc&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gds_alv_fieldcat_lvc&nbsp;TYPE&nbsp;lvc_s_fcat.<br/>
DATA:g_grid TYPE REF TO cl_gui_alv_grid.<br/>
<br/>
DATA gv_butxt TYPE butxt."公司名称<br/>
<br/>
"ole相关<br/>
DATA: gs_excel    TYPE ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_books&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_workbook&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_sheets&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_sheet&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_range&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_font&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_cell&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_value&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;8.<br/>
<br/>
</div>
<div class="codeComment">
*TYPES:BEGIN&nbsp;OF&nbsp;ty_racct,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;LIKE&nbsp;acdoct-racct,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_racct.<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_bukrs&nbsp;TYPE&nbsp;t001-bukrs&nbsp;&nbsp;OBLIGATORY&nbsp;MODIF&nbsp;&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"公司<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_ryear&nbsp;TYPE&nbsp;acdoct-ryear&nbsp;&nbsp;DEFAULT&nbsp;sy-datum+0(4)&nbsp;OBLIGATORY&nbsp;MODIF&nbsp;&nbsp;ID&nbsp;m1,&nbsp;&nbsp;"会计年度<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_rpmax&nbsp;TYPE&nbsp;acdoct-rpmax&nbsp;&nbsp;DEFAULT&nbsp;sy-datum+4(2)&nbsp;OBLIGATORY&nbsp;MODIF&nbsp;ID&nbsp;m1.&nbsp;"会计期间<br/>
<br/>
SELECTION-SCREEN END OF BLOCK blk1.<br/>
<br/>
INITIALIZATION.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
<br/>
START-OF-SELECTION.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_init_prjs.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_sum.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_create_alv.<br/>
<br/>
END-OF-SELECTION.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_displaydata.<br/>
<br/>
&nbsp;&nbsp;"初始化项目基本信息<br/>
FORM frm_init_prjs.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gt_prjs_racct[]&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;ls_prj_tact&nbsp;TYPE&nbsp;ty_prjs_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DEFINE&nbsp;add_prj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_tact-name&nbsp;=&nbsp;&amp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_tact-index&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_tact-racct&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_tact-drcrk&nbsp;=&nbsp;&amp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_prj_tact&nbsp;TO&nbsp;gt_prjs_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_prj_tact.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;END-OF-DEFINITION.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'一、增值税'&nbsp;'1'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;1、应交增值税'&nbsp;'2'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;（1）年初未抵扣数(用“－”表示)'&nbsp;'3'&nbsp;'2221010300'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;（2）销项税额'&nbsp;'4'&nbsp;'2221010100'&nbsp;'2221010108'&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出口退税'&nbsp;'5'&nbsp;'2221010700'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;进项税额转出'&nbsp;'6'&nbsp;'2221010400'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;转出多交增值税'&nbsp;'7'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;（3）进项税额'&nbsp;'8'&nbsp;'2221010281'&nbsp;'2221010299'&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;已交税金'&nbsp;'9'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;减免税款'&nbsp;'10'&nbsp;'2221010900'&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出口抵减内销产品应纳税额'&nbsp;'11'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;转出未交增值税'&nbsp;'12'&nbsp;'2221020000'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;（4）年末未抵扣数(用“－”表示)'&nbsp;'13'&nbsp;''&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;2、未交增值税'&nbsp;'14'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;（1）年初未交数(多交数以“－”填列)'&nbsp;'15'&nbsp;'2221010100'&nbsp;'2221010299'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;（2）本年转入数(多交数以“－”填列)'&nbsp;'16'&nbsp;'221020000'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;（3）本年已交数'&nbsp;'17'&nbsp;'2221020000'&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;（4）年末未交数(多交数以“－”填列)'&nbsp;'18'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'二、营业税：'&nbsp;'19'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'20'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'21'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'22'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'23'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'三、城建税'&nbsp;'24'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'25'&nbsp;'2221050000'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'26'&nbsp;'2221050000'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'27'&nbsp;'2221050000'&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'28'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'四、所得税'&nbsp;'29'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'30'&nbsp;'2221130100'&nbsp;'2221130200'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'31'&nbsp;'2221130100'&nbsp;'2221130200'&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'32'&nbsp;'2221130100'&nbsp;'2221130200'&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'33'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'五、教育费附加'&nbsp;'34'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'35'&nbsp;'2221060100'&nbsp;'2221060200'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'36'&nbsp;'2221060100'&nbsp;'2221060200'&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_prjs&nbsp;USING&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'37'&nbsp;'2221060100'&nbsp;'2221060200'&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'38'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'六、其它各税(包括费用性税金)'&nbsp;'39'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'40'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'41'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'42'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'43'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'七、基本养老保险'&nbsp;'44'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'45'&nbsp;'2211030100'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'46'&nbsp;'2211030100'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'47'&nbsp;'2211030100'&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'48'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'八、失业保险'&nbsp;'49'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'50'&nbsp;'2211030300'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'51'&nbsp;'2211030300'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'52'&nbsp;'2211030300'&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'53'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'九、基本医疗保险'&nbsp;'54'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年初未交数'&nbsp;'55'&nbsp;'2211030200'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年应交数'&nbsp;'56'&nbsp;'2211030200'&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本年已交数'&nbsp;'57'&nbsp;'2211030200'&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'58'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'补充资料：'&nbsp;'59'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'一、本年应交税金'&nbsp;'60'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;'二、本年实际上交税金'&nbsp;'61'&nbsp;''&nbsp;''&nbsp;.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gt_prjs_smidx[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;ls_prj_smidx&nbsp;TYPE&nbsp;ty_prjs_smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DEFINE&nbsp;add_prj_smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_smidx-name&nbsp;=&nbsp;&amp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_smidx-index&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_prj_smidx-smidx&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_prj_smidx&nbsp;TO&nbsp;gt_prjs_smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_prj_smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;end-OF-DEFINITION.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;（4）年末未抵扣数(用“－”表示)'&nbsp;'13'&nbsp;'3|4|5|6|7|8|-9|-10|-11|-12'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;（4）年末未交数(多交数以“－”填列)'&nbsp;'18'&nbsp;'15|16|-17'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'28'&nbsp;'25|26|-27'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'33'&nbsp;'30|31|-32'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'38'&nbsp;'35|36|-37'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'53'&nbsp;'50|51|-52'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'48'&nbsp;'45|46|-47'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年末未交数'&nbsp;'58'&nbsp;'55|56|-57'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'一、本年应交税金'&nbsp;'60'&nbsp;&nbsp;'16|21|31|26|36|41'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj_smidx&nbsp;'二、本年实际上交税金'&nbsp;'61'&nbsp;&nbsp;'17|22|32|27|37|42'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gt_index_racct[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"构建科目的查询条件,构建序号跟科目号的对应关系<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_prj_racct&gt;&nbsp;TYPE&nbsp;ty_prjs_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:ls_index_racct&nbsp;TYPE&nbsp;ty_index_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;rg_racct-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;rg_racct-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_prjs_racct&nbsp;ASSIGNING&nbsp;&lt;fs_prj_racct&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_prj_racct&gt;-racct&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rg_racct-low&nbsp;=&nbsp;&lt;fs_prj_racct&gt;-racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;rg_racct&nbsp;TO&nbsp;rg_racct[].<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_index_racct-index&nbsp;=&nbsp;&lt;fs_prj_racct&gt;-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_index_racct-racct&nbsp;=&nbsp;&lt;fs_prj_racct&gt;-racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_index_racct-drcrk&nbsp;=&nbsp;&lt;fs_prj_racct&gt;-drcrk.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_index_racct&nbsp;TO&nbsp;gt_index_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_index_racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;rg_racct[]&nbsp;BY&nbsp;low.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;ADJACENT&nbsp;DUPLICATES&nbsp;FROM&nbsp;rg_racct[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;gt_index_racct&nbsp;ASCENDING&nbsp;BY&nbsp;index&nbsp;racct.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;"完成最终ALV的一些信息，：序号，便于赋值<br/>
&nbsp;&nbsp;IF&nbsp;gt_alv_index[]&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'1'&nbsp;'34'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'2'&nbsp;'35'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'3'&nbsp;'36'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'4'&nbsp;'37'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'5'&nbsp;'38'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'6'&nbsp;'39'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'7'&nbsp;'40'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'8'&nbsp;'41'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'9'&nbsp;'42'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'10'&nbsp;'43'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'11'&nbsp;'44'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'12'&nbsp;'45'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'13'&nbsp;'46'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'14'&nbsp;'47'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'15'&nbsp;'48'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'16'&nbsp;'49'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'17'&nbsp;'50'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'18'&nbsp;'51'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'19'&nbsp;'52'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'20'&nbsp;'53'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'21'&nbsp;'54'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'22'&nbsp;'55'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'23'&nbsp;'56'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'24'&nbsp;'57'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'25'&nbsp;'58'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'26'&nbsp;'59'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'27'&nbsp;'60'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'28'&nbsp;'61'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'29'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'30'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'31'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'32'&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_add_alv_index&nbsp;USING&nbsp;'33'&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_add_alv_index USING iv_index TYPE char3 iv_index2 TYPE char3.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_alv&nbsp;TYPE&nbsp;ty_alv.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_prj_tact&nbsp;TYPE&nbsp;ty_prjs_racct.<br/>
&nbsp;&nbsp;ls_alv-index&nbsp;=&nbsp;iv_index.<br/>
&nbsp;&nbsp;ls_alv-index2&nbsp;=&nbsp;iv_index2.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_prjs_racct&nbsp;INTO&nbsp;ls_prj_tact&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;ls_alv-index.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_alv-name&nbsp;=&nbsp;ls_prj_tact-name.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_prj_tact.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_prjs_racct&nbsp;INTO&nbsp;ls_prj_tact&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;ls_alv-index2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_alv-name2&nbsp;=&nbsp;ls_prj_tact-name.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_alv&nbsp;TO&nbsp;gt_alv_index.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;ls_alv,ls_prj_tact.<br/>
ENDFORM.<br/>
<br/>
FORM frm_get_data.<br/>
&nbsp;&nbsp;CLEAR:gt_index_hslsm,gt_index_hslsm[].<br/>
&nbsp;&nbsp;"公司<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;butxt&nbsp;INTO&nbsp;gv_butxt&nbsp;FROM&nbsp;t001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_bukrs.<br/>
&nbsp;&nbsp;CHECK&nbsp;gv_butxt&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;"动态创建查询字段<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_col&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_cols&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;'RYEAR'&nbsp;TO&nbsp;lt_cols.<br/>
&nbsp;&nbsp;APPEND&nbsp;'DRCRK'&nbsp;TO&nbsp;lt_cols.<br/>
&nbsp;&nbsp;APPEND&nbsp;'RACCT'&nbsp;TO&nbsp;lt_cols.<br/>
&nbsp;&nbsp;APPEND&nbsp;'RBUKRS'&nbsp;TO&nbsp;lt_cols.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;APPEND&nbsp;'RFAREA'&nbsp;TO&nbsp;lt_cols.<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;'HSLVT'&nbsp;TO&nbsp;lt_cols.<br/>
<br/>
&nbsp;&nbsp;DATA:gv_month&nbsp;TYPE&nbsp;acdoct-rpmax,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;znum(2)&nbsp;&nbsp;TYPE&nbsp;n.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_num&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_tab&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab.<br/>
&nbsp;&nbsp;"声明：按年、科目统计各项合计<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_tab_sum&nbsp;TYPE&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_tab_sum&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab.<br/>
&nbsp;&nbsp;CLEAR&nbsp;znum.<br/>
&nbsp;&nbsp;DO&nbsp;p_rpmax&nbsp;TIMES.&nbsp;"计算期末数累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;znum&nbsp;&gt;&nbsp;16.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;znum&nbsp;=&nbsp;znum&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'HSL'&nbsp;znum&nbsp;INTO&nbsp;ls_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_col&nbsp;&nbsp;TO&nbsp;lt_cols.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;"统计今年和上年的数据<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;(lt_cols)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_tab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;acdoct<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ryear&nbsp;=&nbsp;p_ryear<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;rbukrs&nbsp;=&nbsp;p_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;racct&nbsp;IN&nbsp;rg_racct.<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;lt_tab&nbsp;ASCENDING&nbsp;BY&nbsp;rbukrs&nbsp;ryear&nbsp;racct&nbsp;drcrk.<br/>
<br/>
&nbsp;&nbsp;"统计每个月对应的合计值<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_tab&gt;&nbsp;TYPE&nbsp;ty_tab.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_tab&nbsp;ASSIGNING&nbsp;&lt;fs_tab&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_rpmax&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl02.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl03.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl04.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl05.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;6.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl06.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;7.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl07.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;8.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl08.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;9.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl09.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;10.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl10.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;11.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl11.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;12.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl12.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl12.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;13.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl12<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl13.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl13.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;14.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl12<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl13&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl14.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl14.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;15.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl12<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl13&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl14&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl15.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl15.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p_rpmax&nbsp;=&nbsp;16.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl01&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl02&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl03&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl04<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl05&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl06&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl07&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl08<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl09&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl10&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl11&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl12<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl13&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl14&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl15&nbsp;+&nbsp;&lt;fs_tab&gt;-hsl16.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hsl16.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;&lt;fs_tab&gt;-rpmax,&lt;fs_tab&gt;-rbukrs."&lt;fs_tab&gt;-drcrk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-ryear&nbsp;=&nbsp;&lt;fs_tab&gt;-ryear.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-racct&nbsp;=&nbsp;&lt;fs_tab&gt;-racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm_m.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-drcrk&nbsp;=&nbsp;&lt;fs_tab&gt;-drcrk.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab_sum-hslvt&nbsp;=&nbsp;&lt;fs_tab&gt;-hslvt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_tab_sum&nbsp;INTO&nbsp;lt_tab_sum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_tab_sum.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"通过科目号找出对应的序号,序号和科目是一对多关系<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_ir&gt;&nbsp;TYPE&nbsp;ty_index_racct.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_tab_sum&nbsp;ASSIGNING&nbsp;&lt;fs_tab&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_index_racct&nbsp;ASSIGNING&nbsp;&lt;fs_ir&gt;&nbsp;WHERE&nbsp;racct&nbsp;=&nbsp;&lt;fs_tab&gt;-racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;&lt;fs_ir&gt;-index&nbsp;TO&nbsp;gs_index_hslsm-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'3'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'15'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'25'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'30'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'35'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'45'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'50'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'55'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hslvt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hslvt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_ir&gt;-drcrk&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm_m.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;fs_ir&gt;-drcrk&nbsp;=&nbsp;&lt;fs_tab&gt;-drcrk.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm_m&nbsp;=&nbsp;&lt;fs_tab&gt;-hslsm_m.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm_m&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'3'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"如果科目描述中带有'-'，数值不做绝对值处理，否则需要做绝对值处理<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'13'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'15'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'16'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&lt;fs_ir&gt;-index&nbsp;=&nbsp;'18'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;gs_index_hslsm-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;gs_index_hslsm-hslsm&nbsp;&lt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;gs_index_hslsm-hslsm&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm_m&nbsp;=&nbsp;gs_index_hslsm-hslsm_m&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;gs_index_hslsm&nbsp;INTO&nbsp;gt_index_hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_index_hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;构建显示ALV<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_create_alv.<br/>
&nbsp;&nbsp;"构建显示ALV<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_alv&gt;&nbsp;TYPE&nbsp;ty_alv.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_ih&gt;&nbsp;TYPE&nbsp;ty_index_hslsm.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gt_alv,gt_alv[].<br/>
&nbsp;&nbsp;"模板数据更新到alv<br/>
&nbsp;&nbsp;APPEND&nbsp;LINES&nbsp;OF&nbsp;gt_alv_index&nbsp;TO&nbsp;gt_alv.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs_alv&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_index_hslsm&nbsp;ASSIGNING&nbsp;&lt;fs_ih&gt;&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;&lt;fs_alv&gt;-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_alv&gt;-hslsm_m&nbsp;&nbsp;=&nbsp;&lt;fs_ih&gt;-hslsm_m.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_alv&gt;-hslsm&nbsp;&nbsp;=&nbsp;&lt;fs_ih&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_index_hslsm&nbsp;ASSIGNING&nbsp;&lt;fs_ih&gt;&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;&lt;fs_alv&gt;-index2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_alv&gt;-hslsm_m2&nbsp;&nbsp;=&nbsp;&lt;fs_ih&gt;-hslsm_m.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_alv&gt;-hslsm2&nbsp;&nbsp;=&nbsp;&lt;fs_ih&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
FORM frm_displaydata.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_addlayout&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_addfieldcat&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_display&nbsp;&nbsp;.<br/>
ENDFORM.<br/>
<br/>
FORM frm_addlayout.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gds_alv_layout_lvc.<br/>
&nbsp;&nbsp;gds_alv_layout_lvc-cwidth_opt&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gds_alv_layout_lvc-zebra&nbsp;&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gds_alv_layout_lvc-no_rowmark&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;gds_alv_layout_lvc-box_fname&nbsp;=&nbsp;'ZSEL'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;gds_alv_layout_lvc-sel_mode&nbsp;=&nbsp;'A'.<br/>
ENDFORM.<br/>
<br/>
FORM frm_addfieldcat.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fieldcat&nbsp;TYPE&nbsp;lvc_s_fcat&nbsp;.<br/>
&nbsp;&nbsp;DEFINE&nbsp;add_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-ref_field&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-ref_table&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-scrtext_l&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-intlen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat-no_zero&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'&nbsp;&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;gdt_alv_fieldcat_lvc&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;&nbsp;ls_fieldcat&nbsp;.<br/>
&nbsp;&nbsp;END-OF-DEFINITION&nbsp;.<br/>
<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'NAME'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'项目名称'&nbsp;40&nbsp;40&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'INDEX'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'行次'&nbsp;8&nbsp;8&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM_M'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'本月合计'&nbsp;16&nbsp;16&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'本年累计'&nbsp;16&nbsp;16&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM_LY'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'上年累计'&nbsp;16&nbsp;16&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;add_fieldcat&nbsp;'NAME2'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'项目名称'&nbsp;40&nbsp;40&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'INDEX2'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'行次'&nbsp;8&nbsp;8&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM_M2'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'本月合计'&nbsp;16&nbsp;16&nbsp;.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM2'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'本年累计'&nbsp;16&nbsp;16&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;add_fieldcat&nbsp;'HSLSM_LY2'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;'上年累计'&nbsp;16&nbsp;16&nbsp;.<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
FORM frm_display.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_INTERFACE_CHECK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BYPASSING_BUFFER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BUFFER_ACTIVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_PF_STATUS_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'USER_COMMAND_LVC'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_TOP_OF_PAGE&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_HTML_TOP_OF_PAGE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_HTML_END_OF_LIST&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_STRUCTURE_NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BACKGROUND_ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_GRID_TITLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_GRID_SETTINGS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gds_alv_layout_lvc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gdt_alv_fieldcat_lvc[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_EXCLUDING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_SPECIAL_GROUPS_LVC&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_SORT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_FILTER_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_HYPERLINK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_SEL_HIDE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_VARIANT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_event<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_EVENT_EXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_PRINT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_REPREP_ID_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_START_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_START_LINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_END_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_END_LINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_HTML_HEIGHT_TOP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_HTML_HEIGHT_END&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_ALV_GRAPHICS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_EXCEPT_QINFO_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IR_SALV_FULLSCREEN_ADAPTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_EXIT_CAUSED_BY_CALLER&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ES_EXIT_CAUSED_BY_USER&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_alv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
FORM set_pf_status_lvc USING p_extab TYPE slis_t_extab .<br/>
&nbsp;&nbsp;CLEAR&nbsp;p_extab[].<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STS'&nbsp;EXCLUDING&nbsp;p_extab[].<br/>
ENDFORM.<br/>
<br/>
FORM user_command_lvc USING im_ucomm   TYPE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_filecat&nbsp;TYPE&nbsp;slis_selfield.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_GLOBALS_FROM_SLVC_FULLSCR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_grid&nbsp;=&nbsp;g_grid.<br/>
&nbsp;&nbsp;CASE&nbsp;im_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZEXP'.&nbsp;"导出<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;SAVE'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"保存数据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_save_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;POST'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"推送至EAS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_send_to_eas.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_stbl&nbsp;TYPE&nbsp;lvc_s_stbl.<br/>
&nbsp;&nbsp;ls_stbl-row&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_stbl-col&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_grid-&gt;refresh_table_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_stable&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_stbl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_soft_refresh&nbsp;=&nbsp;'X'.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_exp.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_excel&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_books&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_workbook&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_sheets&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_sheet&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_range&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_cell&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_interior&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_value&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;8.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_filepath&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_filename&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_fullpath&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;"获取保存路径<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_save_path&nbsp;CHANGING&nbsp;lv_filepath&nbsp;lv_filename&nbsp;lv_fullpath.<br/>
&nbsp;&nbsp;CHECK&nbsp;lv_fullpath&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;"列名称<br/>
&nbsp;&nbsp;DATA&nbsp;lv_row&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_col&nbsp;TYPE&nbsp;i.<br/>
<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;&nbsp;&nbsp;ls_excel&nbsp;'Excel.Application'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_excel&nbsp;'Visible'&nbsp;=&nbsp;1.&nbsp;"可见<br/>
</div>
<div class="code">
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_excel&nbsp;'SHEETSINNEWWORKBOOK'&nbsp;=&nbsp;1.&nbsp;"设置&nbsp;Microsoft&nbsp;Excel&nbsp;软件打开时，自动插入到新工作簿中的工作表数目（即初始sheet数目，默认名字依次为&nbsp;Sheet1、Sheet2.....）<br/>
<br/>
&nbsp;&nbsp;"创建workbook<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_excel&nbsp;'Workbooks'&nbsp;=&nbsp;ls_books.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_books&nbsp;'ADD'&nbsp;=&nbsp;ls_workbook.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;"打开workbook<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_books&nbsp;'OPEN'EXPORTING&nbsp;#1&nbsp;=&nbsp;'c:\1.xlsx'."开文件<br/>
</div>
<div class="code">
&nbsp;&nbsp;"添加sheet页<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'sheets'&nbsp;=&nbsp;ls_sheets.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'Worksheets'&nbsp;=&nbsp;ls_sheet&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'Sheet1'.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheets&nbsp;'Add'&nbsp;&nbsp;=&nbsp;ls_sheet.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_sheet&nbsp;'Name'&nbsp;=&nbsp;'应上交应弥补款项表'."sheet重命名<br/>
<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'CELLS'&nbsp;=&nbsp;ls_range.&nbsp;"选中所有单元格<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;9&nbsp;'False'.<br/>
<br/>
&nbsp;&nbsp;"设置行高<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'RowHeight'&nbsp;=&nbsp;'14.5'.<br/>
<br/>
&nbsp;&nbsp;"报表表名<br/>
&nbsp;&nbsp;"合并单位格<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A1:H1'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_merge&nbsp;USING&nbsp;ls_range.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;20&nbsp;'True'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',<br/>
&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;"设置行高<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'RowHeight'&nbsp;=&nbsp;'26'.<br/>
&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;"赋值<br/>
&nbsp;&nbsp;"表单名称<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;1&nbsp;&nbsp;1&nbsp;'雪天盐业集团股份有限公司会计报表'.<br/>
<br/>
&nbsp;&nbsp;"副标题<br/>
&nbsp;&nbsp;"合并单位格<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A2:H2'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_merge&nbsp;USING&nbsp;ls_range.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;14&nbsp;'True'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',<br/>
&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;"设置行高<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'RowHeight'&nbsp;=&nbsp;'18'.<br/>
&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;"副标题名称<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;2&nbsp;&nbsp;1&nbsp;'应上交应弥补款项表'.<br/>
<br/>
&nbsp;&nbsp;"统计期间<br/>
&nbsp;&nbsp;"合并单位格<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A3:G3'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_merge&nbsp;USING&nbsp;ls_range.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;9&nbsp;'false'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',<br/>
&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;"统计期间<br/>
&nbsp;&nbsp;lv_value&nbsp;=&nbsp;|{&nbsp;p_ryear&nbsp;}{&nbsp;'年'&nbsp;}{&nbsp;p_rpmax&nbsp;}{&nbsp;'期'&nbsp;}|.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;3&nbsp;&nbsp;1&nbsp;lv_value.<br/>
&nbsp;&nbsp;CLEAR&nbsp;lv_value.<br/>
<br/>
&nbsp;&nbsp;"会企03表<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'H3'.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;9&nbsp;'false'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'4',<br/>
&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;3&nbsp;&nbsp;8&nbsp;'会企06表'.<br/>
<br/>
&nbsp;&nbsp;"编制单位<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A4:D4'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_merge&nbsp;USING&nbsp;ls_range.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;9&nbsp;'false'.<br/>
&nbsp;&nbsp;lv_value&nbsp;=&nbsp;|{&nbsp;'编制单位:'&nbsp;}{&nbsp;gv_butxt&nbsp;}|.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;4&nbsp;&nbsp;1&nbsp;lv_value.<br/>
&nbsp;&nbsp;CLEAR&nbsp;lv_value.<br/>
<br/>
&nbsp;&nbsp;"单位<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'H4'.<br/>
&nbsp;&nbsp;"设置字体<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_font&nbsp;USING&nbsp;ls_range&nbsp;9&nbsp;'false'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'4',<br/>
" &amp;&amp; 水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;4&nbsp;&nbsp;8&nbsp;'单位:元'.<br/>
<br/>
&nbsp;&nbsp;"设置边框<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A5:H38'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_frame&nbsp;USING&nbsp;ls_range&nbsp;.<br/>
<br/>
&nbsp;&nbsp;"设置数字格式<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'C6:D38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_range&nbsp;'Select'&nbsp;.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;'NumberFormatLocal'&nbsp;=&nbsp;'#,##0.00'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'G6:H38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_range&nbsp;'Select'&nbsp;.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;'NumberFormatLocal'&nbsp;=&nbsp;'#,##0.00'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'Range'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A1'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_range&nbsp;'Select'.<br/>
<br/>
&nbsp;&nbsp;"表头<br/>
&nbsp;&nbsp;lv_row&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fieldcat&nbsp;TYPE&nbsp;lvc_s_fcat&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gdt_alv_fieldcat_lvc&nbsp;INTO&nbsp;ls_fieldcat&nbsp;WHERE&nbsp;no_out&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col&nbsp;ls_fieldcat-scrtext_l.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A5:H5'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
<br/>
&nbsp;&nbsp;"行次列<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'B6:B38'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'F6:F38'.<br/>
&nbsp;&nbsp;"居中<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range:&nbsp;'HorizontalAlignment'&nbsp;=&nbsp;'3',&nbsp;&nbsp;&nbsp;"&nbsp;&amp;&amp;&nbsp;水平方向　2左对齐，3居中，4右对齐<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VERTICALALIGNMENT'&nbsp;=&nbsp;'2'.&nbsp;&nbsp;"&amp;&amp;&nbsp;垂直方向　1靠上　，2居中，3靠下<br/>
<br/>
&nbsp;&nbsp;"内容<br/>
&nbsp;&nbsp;DATA&nbsp;lv_i&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_alv&gt;&nbsp;TYPE&nbsp;ty_alv.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs_alv&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_row&nbsp;=&nbsp;lv_row&nbsp;+&nbsp;1.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;8&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_col&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;1.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_i&nbsp;=&nbsp;SWITCH&nbsp;#(&nbsp;lv_col&nbsp;WHEN&nbsp;1&nbsp;THEN&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;2&nbsp;THEN&nbsp;3<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;3&nbsp;THEN&nbsp;5&nbsp;).<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;lv_col&nbsp;OF&nbsp;STRUCTURE&nbsp;&lt;fs_alv&gt;&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;lv_any&gt;).<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col&nbsp;&lt;lv_any&gt;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;&lt;lv_any&gt;&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"使用全局变量,以便启用no&nbsp;flush&nbsp;,提示效率<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'CELLS'&nbsp;=&nbsp;gs_cell&nbsp;NO&nbsp;FLUSH&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;lv_row&nbsp;#2&nbsp;=&nbsp;lv_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;gs_cell&nbsp;'Value'&nbsp;=&nbsp;&lt;lv_any&gt;&nbsp;NO&nbsp;FLUSH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;"自动调整列宽<br/>
*&nbsp;&nbsp;DATA&nbsp;ls_entcol&nbsp;TYPE&nbsp;ole2_object.<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A:H'.<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_range&nbsp;'Select'&nbsp;.<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_range&nbsp;'EntireColumn'&nbsp;=&nbsp;ls_entcol&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_entcol&nbsp;'AutoFit'&nbsp;&nbsp;.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;"调整列宽<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'A1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'33.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'B1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'6.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'C1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'13.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'D1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'13.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'E1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'29.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'F1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'6.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'G1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'13.38'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_sheet&nbsp;'RANGE'&nbsp;=&nbsp;ls_range&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;'H1'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'13.38'.<br/>
<br/>
&nbsp;&nbsp;"设置表格颜色<br/>
&nbsp;&nbsp;lv_row&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;CLEAR&nbsp;lv_col.<br/>
&nbsp;&nbsp;DATA&nbsp;lo_interior&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;io_font&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_col_next&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DO&nbsp;8&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'CELLS'&nbsp;=&nbsp;ls_cell&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;lv_row&nbsp;#2&nbsp;=&nbsp;lv_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'Interior'&nbsp;=&nbsp;lo_interior.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;lo_interior&nbsp;'Color'&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'16776960'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'font'&nbsp;=&nbsp;io_font.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;io_font&nbsp;'BOLD'&nbsp;=&nbsp;'true'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lo_interior,io_font.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;lv_row&nbsp;=&nbsp;6.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;lv_col.<br/>
&nbsp;&nbsp;lv_col&nbsp;=&nbsp;5.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lo_interior&nbsp;TYPE&nbsp;ole2_object.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DO&nbsp;6&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'CELLS'&nbsp;=&nbsp;ls_cell&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;lv_row&nbsp;#2&nbsp;=&nbsp;lv_col.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'Interior'&nbsp;=&nbsp;lo_interior.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;lo_interior&nbsp;'Color'&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'16776960'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'font'&nbsp;=&nbsp;io_font.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;io_font&nbsp;'BOLD'&nbsp;=&nbsp;'true'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_col_next.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col_next&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col_next&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_col_next.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col_next&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col_next&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_row&nbsp;=&nbsp;lv_row&nbsp;+&nbsp;5.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lo_interior,io_font.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'CELLS'&nbsp;=&nbsp;ls_cell&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;6&nbsp;#2&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'font'&nbsp;=&nbsp;io_font.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;io_font&nbsp;'BOLD'&nbsp;=&nbsp;'true'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR:&nbsp;lo_interior,io_font.<br/>
<br/>
&nbsp;&nbsp;lv_row&nbsp;=&nbsp;24.<br/>
&nbsp;&nbsp;CLEAR&nbsp;lv_col.<br/>
&nbsp;&nbsp;lv_col&nbsp;=&nbsp;1.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lo_interior&nbsp;TYPE&nbsp;ole2_object.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DO&nbsp;3&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_excel&nbsp;'CELLS'&nbsp;=&nbsp;ls_cell&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;lv_row&nbsp;#2&nbsp;=&nbsp;lv_col.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'Interior'&nbsp;=&nbsp;lo_interior.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;lo_interior&nbsp;'Color'&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'16776960'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_col&nbsp;=&nbsp;5.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'font'&nbsp;=&nbsp;io_font.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;io_font&nbsp;'BOLD'&nbsp;=&nbsp;'true'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_col_next.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col_next&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col_next&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_col_next.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col_next&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;&nbsp;lv_col_next&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_row&nbsp;=&nbsp;lv_row&nbsp;+&nbsp;5.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lo_interior,io_font.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;"对特殊单元格数值做空值处理<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;6&nbsp;&nbsp;3&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;6&nbsp;&nbsp;4&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;7&nbsp;&nbsp;3&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;7&nbsp;&nbsp;4&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;19&nbsp;&nbsp;3&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;19&nbsp;&nbsp;4&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;lv_row,lv_col.<br/>
&nbsp;&nbsp;lv_row&nbsp;=&nbsp;34.<br/>
&nbsp;&nbsp;DO&nbsp;5&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_col&nbsp;=&nbsp;7.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;2&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_value&nbsp;USING&nbsp;ls_sheet&nbsp;lv_row&nbsp;lv_col&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_col&nbsp;=&nbsp;lv_col&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_row&nbsp;=&nbsp;lv_row&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_range&nbsp;:&nbsp;'columnwidth'&nbsp;=&nbsp;'46.5'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_excel&nbsp;'Visible'&nbsp;=&nbsp;1.&nbsp;"可见<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_workbook&nbsp;'SaveAs'&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;lv_fullpath.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'导出成功'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'导出失败'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;ls_sheet.<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;ls_workbook.<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;ls_books.<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;ls_excel.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_get_save_path CHANGING cv_filepath cv_filename cv_fullpath.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_string&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_value&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;lv_value&nbsp;=&nbsp;|{&nbsp;'应上交应弥补款项表_'&nbsp;}{&nbsp;p_ryear&nbsp;}{&nbsp;'年'&nbsp;}{&nbsp;p_rpmax&nbsp;}{&nbsp;'期'&nbsp;}|.<br/>
<br/>
&nbsp;&nbsp;lv_string&nbsp;=&nbsp;'请选择文件保存路径'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_string&nbsp;&nbsp;"请选择文件保存路径!<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'xlsx'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_value<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH_ENCODING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_frontend_services=&gt;filetype_excel<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INITIAL_DIRECTORY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C:\*\*\Desktop\'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROMPT_ON_OVERWRITE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_filepath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_fullpath<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USER_ACTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILE_ENCODING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_default_file_name&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'获取文件保存路径失败!'&nbsp;TYPE&nbsp;'E'.&nbsp;&nbsp;"获取文件保存路径失败!<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_result&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_exist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;result&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_result<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_parameter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;lv_result&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_subrc&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_delete<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_subrc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_delete_failed&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;access_denied&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unknown_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_parameter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;9.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'删除原有文件失败!请检查文件是否已打开!'&nbsp;TYPE&nbsp;'E'&nbsp;.&nbsp;"删除原有文件失败!请检查文件是否已打开!<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_set_font<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;设置字体<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;is_range<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;iv_size<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_font  USING     is_range TYPE ole2_object<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_size&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_bold&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_font&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_range&nbsp;'Select'.<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;is_range&nbsp;'Font'&nbsp;=&nbsp;ls_font.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_font&nbsp;:&nbsp;'Name'&nbsp;=&nbsp;'Microsoft&nbsp;YaHei&nbsp;Light',<br/>
</div>
<div class="code">
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_font&nbsp;:&nbsp;'Name'&nbsp;=&nbsp;'宋体',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Size'&nbsp;=&nbsp;iv_size,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Bold'&nbsp;=&nbsp;iv_bold.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_merge<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;合并单元格<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LS_RANGE<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_merge  USING is_range TYPE ole2_object.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_range&nbsp;'Select'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_range&nbsp;'Merge'.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_fill_value  USING    is_sheet<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_row&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_col&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_value&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_cell&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;lo_interior&nbsp;TYPE&nbsp;ole2_object.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_sheet&nbsp;'CELLS'&nbsp;=&nbsp;ls_cell&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;iv_row&nbsp;#2&nbsp;=&nbsp;iv_col.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_cell&nbsp;'VALUE'&nbsp;=&nbsp;iv_value&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;iv_row&nbsp;=&nbsp;5.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;ls_cell&nbsp;'INTERIOR'&nbsp;=&nbsp;lo_interior.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;lo_interior&nbsp;'COLOR'&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'10329501'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;lo_interior&nbsp;'COLOR'&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'00FFFF'.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_set_frame<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;设置边框<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;RANGE<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_frame  USING   is_range TYPE ole2_object.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_border&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_range&nbsp;'Select'.<br/>
&nbsp;&nbsp;DO&nbsp;4&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;is_range&nbsp;'Borders'&nbsp;=&nbsp;ls_border&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;sy-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;ls_border:&nbsp;'LineStyle'&nbsp;=&nbsp;1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'ColorIndex'&nbsp;=&nbsp;0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'TintAndShade'&nbsp;=&nbsp;0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'Weight'&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"(BORDERS参数：1－左、2－右、3－顶、4－底、5－斜、6－斜/；<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&amp;LINESTYLE值：1与7－细实、2－细虚、4－点虚、9－双细实线)<br/>
&nbsp;&nbsp;ENDDO.<br/>
ENDFORM.<br/>
<br/>
DATA:lp_name   TYPE c LENGTH 60,    "项目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_index&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_type&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-drcrk,&nbsp;&nbsp;&nbsp;"借贷标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_racct1&nbsp;TYPE&nbsp;acdoct-racct,&nbsp;&nbsp;&nbsp;"科目号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_racct2&nbsp;TYPE&nbsp;acdoct-racct.&nbsp;&nbsp;&nbsp;"科目号<br/>
FORM frm_add_prjs USING lp_name lp_index lp_racct1 lp_racct2 lp_type.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA:&nbsp;lp_times&nbsp;TYPE&nbsp;i.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_prj_tact&nbsp;TYPE&nbsp;ty_prjs_racct.<br/>
&nbsp;&nbsp;SELECT&nbsp;DISTINCT&nbsp;saknr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ska1<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;saknr&nbsp;BETWEEN&nbsp;@lp_racct1&nbsp;AND&nbsp;@lp_racct2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@DATA(lt_ska1).<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_ska1&nbsp;INTO&nbsp;DATA(lw_ska1).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;add_prj&nbsp;lp_name&nbsp;lp_index&nbsp;lw_ska1-saknr&nbsp;lp_type.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;计算汇总数据<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_sum.<br/>
&nbsp;&nbsp;"程序中，不采用根据序号，按序号合计，通过序号，找到对应的科目，再通过科目做合计<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_prj_smidx&gt;&nbsp;TYPE&nbsp;ty_prjs_smidx.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_prjs_smidx&nbsp;ASSIGNING&nbsp;&lt;fs_prj_smidx&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_prj_smidx&gt;-smidx&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_split&nbsp;USING&nbsp;&lt;fs_prj_smidx&gt;-index&nbsp;&lt;fs_prj_smidx&gt;-smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_get_split USING lv_index TYPE char3 lv_smidx TYPE char220.<br/>
&nbsp;&nbsp;"拆分序列汇总<br/>
&nbsp;&nbsp;SPLIT&nbsp;lv_smidx&nbsp;AT&nbsp;'|'&nbsp;INTO&nbsp;TABLE&nbsp;DATA(lt_idx)&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_prj_smidx&nbsp;TYPE&nbsp;ty_prjs_smidx.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_ih&gt;&nbsp;TYPE&nbsp;ty_index_hslsm.<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_len&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm_sum&nbsp;LIKE&nbsp;acdoct-hsl16,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_idx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;4.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_idx&nbsp;INTO&nbsp;DATA(ls_idx).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"到合计序号表，找下级<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_prjs_smidx&nbsp;INTO&nbsp;ls_prj_smidx&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;ls_idx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"如果下级还是个序号合计，递归<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_prj_smidx-smidx&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_split&nbsp;USING&nbsp;lv_index&nbsp;ls_prj_smidx-smidx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_idx+0(1)&nbsp;=&nbsp;'-'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_len&nbsp;=&nbsp;cl_abap_list_utilities=&gt;dynamic_output_length(&nbsp;ls_idx&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_len&nbsp;=&nbsp;lv_len&nbsp;-&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_idx&nbsp;=&nbsp;&nbsp;ls_idx+1(lv_len).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_type&nbsp;=&nbsp;'-'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_idx&nbsp;=&nbsp;ls_idx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_type&nbsp;=&nbsp;'+'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"如果下级没有序号合计，找到已经算好的序号数据进行累加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_index_hslsm&nbsp;ASSIGNING&nbsp;&lt;fs_ih&gt;&nbsp;WITH&nbsp;KEY&nbsp;index&nbsp;=&nbsp;lv_idx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm&nbsp;=&nbsp;&lt;fs_ih&gt;-hslsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_type&nbsp;=&nbsp;'-'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm_sum&nbsp;=&nbsp;lv_hslsm_sum&nbsp;-&nbsp;lv_hslsm&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm_sum&nbsp;=&nbsp;lv_hslsm_sum&nbsp;+&nbsp;lv_hslsm&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_index_hslsm-name&nbsp;=&nbsp;lv_name.<br/>
</div>
<div class="code">
&nbsp;&nbsp;gs_index_hslsm-index&nbsp;=&nbsp;lv_index.<br/>
&nbsp;&nbsp;IF&nbsp;lv_index&nbsp;=&nbsp;'3'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"如果科目描述中带有'-'，数值不做绝对值处理，否则需要做绝对值处理<br/>
&nbsp;&nbsp;OR&nbsp;lv_index&nbsp;=&nbsp;'13'<br/>
&nbsp;&nbsp;OR&nbsp;lv_index&nbsp;=&nbsp;'15'<br/>
&nbsp;&nbsp;OR&nbsp;lv_index&nbsp;=&nbsp;'16'<br/>
&nbsp;&nbsp;OR&nbsp;lv_index&nbsp;=&nbsp;'18'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;gs_index_hslsm-hslsm.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ELSEIF&nbsp;lv_hslsm_sum&nbsp;&lt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_hslsm_sum&nbsp;=&nbsp;lv_hslsm_sum&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;gs_index_hslsm-hslsm&nbsp;=&nbsp;lv_hslsm_sum.<br/>
&nbsp;&nbsp;APPEND&nbsp;gs_index_hslsm&nbsp;TO&nbsp;gt_index_hslsm.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_save_data.<br/>
&nbsp;&nbsp;DATA&nbsp;lw_fico019&nbsp;LIKE&nbsp;ztfico019.<br/>
&nbsp;&nbsp;SORT&nbsp;gt_prjs_racct&nbsp;BY&nbsp;index.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_prjs_racct&nbsp;INTO&nbsp;DATA(lw_prjs_racct).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lw_fico019.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;hslsm,hslsm_m&nbsp;FROM&nbsp;@gt_index_hslsm&nbsp;AS&nbsp;idx&nbsp;WHERE&nbsp;index&nbsp;=&nbsp;@lw_prjs_racct-index&nbsp;INTO&nbsp;(&nbsp;@lw_fico019-hslsm,@lw_fico019-hslsm_m&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-bukrs&nbsp;&nbsp;=&nbsp;&nbsp;p_bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-zyear&nbsp;&nbsp;=&nbsp;&nbsp;p_ryear.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-zpmax&nbsp;&nbsp;=&nbsp;&nbsp;p_rpmax.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-butxt&nbsp;&nbsp;=&nbsp;&nbsp;gv_butxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-zname&nbsp;&nbsp;=&nbsp;&nbsp;lw_prjs_racct-name.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-zindex&nbsp;=&nbsp;&nbsp;lw_prjs_racct-index.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lw_fico019-hslsm&nbsp;&nbsp;=&nbsp;&nbsp;gs_index_hslsm-hslsm.<br/>
<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lw_fico019&nbsp;to&nbsp;zfico019.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;ztfico019&nbsp;FROM&nbsp;lw_fico019.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lw_prjs_racct.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'保存数据成功'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_SEND_TO_EAS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_send_to_eas .<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_input&nbsp;TYPE&nbsp;zmt_fico068_submitting_reimbur.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"类名<br/>
&nbsp;&nbsp;ASSIGN&nbsp;lv_input-mt_fico068_submitting_reimburs-message_header&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;ls_msghead&gt;).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"类属性<br/>
&nbsp;&nbsp;ASSIGN&nbsp;lv_input-mt_fico068_submitting_reimburs-zdata-header&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;ls_head&gt;).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"类属性<br/>
&nbsp;&nbsp;ASSIGN&nbsp;lv_input-mt_fico068_submitting_reimburs-zdata-items&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;lt_items&gt;).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"类属性<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*初始化日志类.<br/>
*--------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;interface&nbsp;=&nbsp;'SI_ERP_FICO068_SUBMITTING_REIMBURSABLE_AMOUNT_ASY_OUT'&nbsp;).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"接口名称<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-messageid&nbsp;=&nbsp;lo_log-&gt;iflog-id.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-receiver&nbsp;=&nbsp;'EAS'.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-bustyp&nbsp;=&nbsp;'FICO068'.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&lt;ls_msghead&gt;-send_user&nbsp;=&nbsp;sy-uname.<br/>
<br/>
&nbsp;&nbsp;"公司转换<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;zcl_eas_assist=&gt;exchang_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_bukrs&nbsp;=&nbsp;&lt;ls_head&gt;-bukrs.<br/>
<br/>
&nbsp;&nbsp;&lt;ls_head&gt;-gjahr&nbsp;=&nbsp;p_ryear.<br/>
&nbsp;&nbsp;&lt;ls_head&gt;-monat&nbsp;=&nbsp;p_rpmax.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_OUTPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;ls_head&gt;-monat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;ls_head&gt;-monat.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;DATA(lw_alv)&nbsp;WHERE&nbsp;index&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;INITIAL&nbsp;LINE&nbsp;TO&nbsp;&lt;lt_items&gt;&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;ls_items&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-zindex&nbsp;=&nbsp;lw_alv-index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-ztext&nbsp;=&nbsp;lw_alv-name.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;数字转字符串,并实现负号前提及压缩空格.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ls_item-swrbtr&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;&nbsp;ls_data-wsl&nbsp;).<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslby&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;lw_alv-hslsm_m&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslbn&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;lw_alv-hslsm&nbsp;).<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslby&nbsp;=&nbsp;lw_alv-hslsm_m.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslbn&nbsp;=&nbsp;lw_alv-hslsm.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;lw_alv&nbsp;WHERE&nbsp;index2&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;INITIAL&nbsp;LINE&nbsp;TO&nbsp;&lt;lt_items&gt;&nbsp;ASSIGNING&nbsp;&lt;ls_items&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-zindex&nbsp;=&nbsp;lw_alv-index2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-ztext&nbsp;=&nbsp;lw_alv-name2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslby&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;lw_alv-hslsm_m2&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslbn&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;lw_alv-hslsm2&nbsp;).<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslby&nbsp;=&nbsp;lw_alv-hslsm_m2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_items&gt;-hslbn&nbsp;=&nbsp;lw_alv-hslsm2.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当ERP为发送方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lo_proxy)&nbsp;=&nbsp;NEW&nbsp;&nbsp;zco_si_erp_fico068_submitting(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_proxy-&gt;si_erp_fico068_submitting_reim(&nbsp;EXPORTING&nbsp;output&nbsp;=&nbsp;lv_input&nbsp;&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'发送金蝶成功!'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lv_msgtype)&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lv_message)&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;|发送金蝶失败:{&nbsp;lv_message&nbsp;}|&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;lo_proxy&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msgtype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;|{&nbsp;lv_message&nbsp;}|<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;|{&nbsp;p_bukrs&nbsp;}|<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;|{&nbsp;p_ryear&nbsp;}|<br/>
&nbsp;&nbsp;key3&nbsp;=&nbsp;|{&nbsp;p_rpmax&nbsp;}|&nbsp;).<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司编码<br/>
*&nbsp;P_RPMAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计期间<br/>
*&nbsp;P_RYEAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计年度<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;导出成功<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>