<table class="outerTable">
<tr>
<td><h2>Table: ZACDOCA</h2>
<h3>Description: 为视图生成的表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>RLDNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>FINS_LEDGER</td>
<td>FINS_LEDGER</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>总账会计中的分类账</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>RRCTY</td>
<td>3</td>
<td>&nbsp;</td>
<td>RRCTY</td>
<td>RRCTY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>记录类型</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>RYEAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>GJAHR_POS</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>总账会计年度</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>RTCUR</td>
<td>5</td>
<td>&nbsp;</td>
<td>FINS_CURRT</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>余额交易货币</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>DRCRK</td>
<td>6</td>
<td>&nbsp;</td>
<td>SHKZG</td>
<td>SHKZG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>借/贷标识</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>POPER</td>
<td>7</td>
<td>&nbsp;</td>
<td>POPER</td>
<td>POPER</td>
<td>NUMC</td>
<td>3</td>
<td>&nbsp;</td>
<td>过账期间</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>BLART</td>
<td>8</td>
<td>&nbsp;</td>
<td>BLART</td>
<td>BLART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>凭证类型</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>GJAHR</td>
<td>9</td>
<td>&nbsp;</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td>10</td>
<td>BELNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>BELNR_D</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>会计凭证号码</td>
</tr>
<tr class="cell">
<td>11</td>
<td>DOCLN</td>
<td>11</td>
<td>&nbsp;</td>
<td>DOCLN6</td>
<td>DOCLN6</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>分类账 6 字符过账项目</td>
</tr>
<tr class="cell">
<td>12</td>
<td>BSCHL</td>
<td>12</td>
<td>&nbsp;</td>
<td>BSCHL</td>
<td>BSCHL</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>过账代码</td>
</tr>
<tr class="cell">
<td>13</td>
<td>LINETYPE</td>
<td>13</td>
<td>&nbsp;</td>
<td>LINETYPE</td>
<td>LINETYPE</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>项目类别</td>
</tr>
<tr class="cell">
<td>14</td>
<td>KTOSL</td>
<td>14</td>
<td>&nbsp;</td>
<td>KTOSL</td>
<td>CHAR3</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>交易码</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZUONR</td>
<td>15</td>
<td>&nbsp;</td>
<td>DZUONR</td>
<td>ZUONR</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>分配编号</td>
</tr>
<tr class="cell">
<td>16</td>
<td>RBUKRS</td>
<td>16</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td>17</td>
<td>BLDAT</td>
<td>17</td>
<td>&nbsp;</td>
<td>BLDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的凭证日期</td>
</tr>
<tr class="cell">
<td>18</td>
<td>BUDAT</td>
<td>18</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td>19</td>
<td>VORGN</td>
<td>19</td>
<td>&nbsp;</td>
<td>VORGN</td>
<td>VORGN</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>总账交易类型</td>
</tr>
<tr class="cell">
<td>20</td>
<td>VRGNG</td>
<td>20</td>
<td>&nbsp;</td>
<td>CO_VORGANG</td>
<td>J_VORGANG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>CO 业务事务</td>
</tr>
<tr class="cell">
<td>21</td>
<td>RWCUR</td>
<td>21</td>
<td>&nbsp;</td>
<td>FINS_CURRW</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>交易货币</td>
</tr>
<tr class="cell">
<td>22</td>
<td>USNAM</td>
<td>22</td>
<td>&nbsp;</td>
<td>USNAM</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>用户名</td>
</tr>
<tr class="cell">
<td>23</td>
<td>BKTXT</td>
<td>23</td>
<td>&nbsp;</td>
<td>BKTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>凭证抬头文本</td>
</tr>
<tr class="cell">
<td>24</td>
<td>XBLNR</td>
<td>24</td>
<td>&nbsp;</td>
<td>XBLNR1</td>
<td>XBLNR1</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>参考凭证编号</td>
</tr>
<tr class="cell">
<td>25</td>
<td>XREF1_HD</td>
<td>25</td>
<td>&nbsp;</td>
<td>XREF1_HD</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>凭证抬头的内部参考键值 1</td>
</tr>
<tr class="cell">
<td>26</td>
<td>XREF2_HD</td>
<td>26</td>
<td>&nbsp;</td>
<td>XREF2_HD</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>凭证抬头的内部参考键值 2</td>
</tr>
<tr class="cell">
<td>27</td>
<td>RACCT</td>
<td>27</td>
<td>&nbsp;</td>
<td>RACCT</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>科目号</td>
</tr>
<tr class="cell">
<td>28</td>
<td>RBUSA</td>
<td>28</td>
<td>&nbsp;</td>
<td>GSBER</td>
<td>GSBER</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>业务范围</td>
</tr>
<tr class="cell">
<td>29</td>
<td>RCNTR</td>
<td>29</td>
<td>&nbsp;</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td>30</td>
<td>RFAREA</td>
<td>30</td>
<td>&nbsp;</td>
<td>FKBER</td>
<td>FKBER</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>功能范围</td>
</tr>
<tr class="cell">
<td>31</td>
<td>SEGMENT</td>
<td>31</td>
<td>&nbsp;</td>
<td>FB_SEGMENT</td>
<td>FB_SEGMENT</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>部分报表分段</td>
</tr>
<tr class="cell">
<td>32</td>
<td>PRCTR</td>
<td>32</td>
<td>&nbsp;</td>
<td>PRCTR</td>
<td>PRCTR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>利润中心</td>
</tr>
<tr class="cell">
<td>33</td>
<td>KUNNR</td>
<td>33</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>34</td>
<td>LIFNR</td>
<td>34</td>
<td>&nbsp;</td>
<td>LIFNR</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商或债权人的帐号</td>
</tr>
<tr class="cell">
<td>35</td>
<td>UMSKZ</td>
<td>35</td>
<td>&nbsp;</td>
<td>UMSKZ</td>
<td>UMSKZ</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>特殊总账标识</td>
</tr>
<tr class="cell">
<td>36</td>
<td>AUFNR</td>
<td>36</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>37</td>
<td>MATNR</td>
<td>37</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>38</td>
<td>ANBWA</td>
<td>38</td>
<td>&nbsp;</td>
<td>ANBWA</td>
<td>BWASL</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>资产交易类型</td>
</tr>
<tr class="cell">
<td>39</td>
<td>AWSYS</td>
<td>39</td>
<td>&nbsp;</td>
<td>AWSYS</td>
<td>LOGSYS</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>源凭证的逻辑系统</td>
</tr>
<tr class="cell">
<td>40</td>
<td>ANLN1</td>
<td>40</td>
<td>&nbsp;</td>
<td>ANLN1</td>
<td>ANLN1</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>主要资产编号</td>
</tr>
<tr class="cell">
<td>41</td>
<td>ANLN2</td>
<td>41</td>
<td>&nbsp;</td>
<td>ANLN2</td>
<td>ANLN2</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>资产子编号</td>
</tr>
<tr class="cell">
<td>42</td>
<td>MWSKZ</td>
<td>42</td>
<td>&nbsp;</td>
<td>MWSKZ</td>
<td>MWSKZ</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>销售/购买税代码</td>
</tr>
<tr class="cell">
<td>43</td>
<td>EBELN</td>
<td>43</td>
<td>&nbsp;</td>
<td>EBELN</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购凭证编号</td>
</tr>
<tr class="cell">
<td>44</td>
<td>EBELP</td>
<td>44</td>
<td>&nbsp;</td>
<td>EBELP</td>
<td>EBELP</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>采购凭证的项目编号</td>
</tr>
<tr class="cell">
<td>45</td>
<td>WERKS</td>
<td>45</td>
<td>&nbsp;</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td>46</td>
<td>PERNR</td>
<td>46</td>
<td>&nbsp;</td>
<td>PERNR_D</td>
<td>PERNR</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>员工号</td>
</tr>
<tr class="cell">
<td>47</td>
<td>HBKID</td>
<td>47</td>
<td>&nbsp;</td>
<td>HBKID</td>
<td>HBKID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>开户行简短代码</td>
</tr>
<tr class="cell">
<td>48</td>
<td>HKTID</td>
<td>48</td>
<td>&nbsp;</td>
<td>HKTID</td>
<td>HKTID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>帐户细目的代码</td>
</tr>
<tr class="cell">
<td>49</td>
<td>PBUKRS</td>
<td>49</td>
<td>&nbsp;</td>
<td>PBUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>合伙人公司代码</td>
</tr>
<tr class="cell">
<td>50</td>
<td>SBUSA</td>
<td>50</td>
<td>&nbsp;</td>
<td>PARGB</td>
<td>GSBER</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>贸易伙伴的业务范围</td>
</tr>
<tr class="cell">
<td>51</td>
<td>SCNTR</td>
<td>51</td>
<td>&nbsp;</td>
<td>SKOST</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>发送方成本中心</td>
</tr>
<tr class="cell">
<td>52</td>
<td>SFAREA</td>
<td>52</td>
<td>&nbsp;</td>
<td>SFKBER</td>
<td>FKBER</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>伙伴功能范围</td>
</tr>
<tr class="cell">
<td>53</td>
<td>TSL</td>
<td>53</td>
<td>&nbsp;</td>
<td>FINS_VTCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以余额交易货币计的金额</td>
</tr>
<tr class="cell">
<td>54</td>
<td>HSL</td>
<td>54</td>
<td>&nbsp;</td>
<td>FINS_VHCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以公司代码货币计的金额</td>
</tr>
<tr class="cell">
<td>55</td>
<td>KSL</td>
<td>55</td>
<td>&nbsp;</td>
<td>FINS_VKCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以全球货币计的金额</td>
</tr>
<tr class="cell">
<td>56</td>
<td>WSL</td>
<td>56</td>
<td>&nbsp;</td>
<td>FINS_VWCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以交易货币计的金额</td>
</tr>
<tr class="cell">
<td>57</td>
<td>OSL</td>
<td>57</td>
<td>&nbsp;</td>
<td>FINS_VOCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以自由定义的货币 1 计的金额</td>
</tr>
<tr class="cell">
<td>58</td>
<td>MSL</td>
<td>58</td>
<td>&nbsp;</td>
<td>QUAN1_12</td>
<td>MENGV12</td>
<td>QUAN</td>
<td>23</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>59</td>
<td>RUNIT</td>
<td>59</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>60</td>
<td>SGTXT</td>
<td>60</td>
<td>&nbsp;</td>
<td>SGTXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>项目文本</td>
</tr>
<tr class="cell">
<td>61</td>
<td>REBZT</td>
<td>61</td>
<td>&nbsp;</td>
<td>REBZT</td>
<td>REBZT</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>后续凭证类型</td>
</tr>
<tr class="cell">
<td>62</td>
<td>AWREF</td>
<td>62</td>
<td>&nbsp;</td>
<td>AWREF</td>
<td>AWREF</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>参考凭证编号</td>
</tr>
<tr class="cell">
<td>63</td>
<td>AWITEM</td>
<td>63</td>
<td>&nbsp;</td>
<td>FINS_AWITEM</td>
<td>NUMC6</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>参考凭证行项目</td>
</tr>
<tr class="cell">
<td>64</td>
<td>AWTYP</td>
<td>64</td>
<td>&nbsp;</td>
<td>AWTYP</td>
<td>AWTYP</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>参考过程</td>
</tr>
<tr class="cell">
<td>65</td>
<td>AWORG</td>
<td>65</td>
<td>&nbsp;</td>
<td>AWORG</td>
<td>AWORG</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>参考组织单位</td>
</tr>
<tr class="cell">
<td>66</td>
<td>RASSC</td>
<td>66</td>
<td>&nbsp;</td>
<td>RASSC</td>
<td>RCOMP</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>贸易合作伙伴的公司标识</td>
</tr>
<tr class="cell">
<td>67</td>
<td>RSTGR</td>
<td>67</td>
<td>&nbsp;</td>
<td>RSTGR</td>
<td>RSTGR</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>付款原因代码</td>
</tr>
<tr class="cell">
<td>68</td>
<td>VBUND</td>
<td>68</td>
<td>&nbsp;</td>
<td>RASSC</td>
<td>RCOMP</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>贸易合作伙伴的公司标识</td>
</tr>
<tr class="cell">
<td>69</td>
<td>BEWAR</td>
<td>69</td>
<td>&nbsp;</td>
<td>RMVCT</td>
<td>RMVCT</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>事务类型</td>
</tr>
<tr class="cell">
<td>70</td>
<td>XNEGP</td>
<td>70</td>
<td>&nbsp;</td>
<td>XNEGP</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>标识：负过账</td>
</tr>
<tr class="cell">
<td>71</td>
<td>BWART</td>
<td>71</td>
<td>&nbsp;</td>
<td>BWART</td>
<td>BWART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>移动类型(库存管理)</td>
</tr>
<tr class="cell">
<td>72</td>
<td>VBELN</td>
<td>72</td>
<td>&nbsp;</td>
<td>VBELN_VF</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
<tr class="cell">
<td>73</td>
<td>POSN2</td>
<td>73</td>
<td>&nbsp;</td>
<td>POSNR_VA</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>销售凭证项目</td>
</tr>
<tr class="cell">
<td>74</td>
<td>POSNR</td>
<td>74</td>
<td>&nbsp;</td>
<td>POSNR_VA</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>销售凭证项目</td>
</tr>
<tr class="cell">
<td>75</td>
<td>XREF1</td>
<td>75</td>
<td>&nbsp;</td>
<td>XREF1</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>业务伙伴参考码</td>
</tr>
<tr class="cell">
<td>76</td>
<td>XREF3</td>
<td>76</td>
<td>&nbsp;</td>
<td>XREF3</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>行项目的参考码</td>
</tr>
<tr class="cell">
<td>77</td>
<td>PROJK</td>
<td>77</td>
<td>&nbsp;</td>
<td>PS_PSP_PNR</td>
<td>PS_POSNR</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>工作分解结构元素 (WBS 元素)</td>
</tr>
<tr class="cell">
<td>78</td>
<td>ZKSAKNR</td>
<td>78</td>
<td>&nbsp;</td>
<td>ZEKSAKNR</td>
<td>ZDKSAKNR</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶末级科目</td>
</tr>
<tr class="cell">
<td>79</td>
<td>ZSAP2EAS</td>
<td>79</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>80</td>
<td>ZKRFAREA</td>
<td>80</td>
<td>&nbsp;</td>
<td>ZE_RFAREA</td>
<td>ZD_RFAREA</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>功能范围</td>
</tr>
<tr class="cell">
<td>81</td>
<td>XMATNR</td>
<td>81</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>82</td>
<td>XAUFNR</td>
<td>82</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>83</td>
<td>XRCNTR</td>
<td>83</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>84</td>
<td>XKUNNR</td>
<td>84</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>85</td>
<td>XLIFNR</td>
<td>85</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>86</td>
<td>XMWSKZ</td>
<td>86</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>87</td>
<td>XRASSC</td>
<td>87</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>88</td>
<td>XHBKID</td>
<td>88</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>89</td>
<td>XRSTGR</td>
<td>89</td>
<td>&nbsp;</td>
<td>ZE_DELIVERY</td>
<td>ZD_DELIVERY</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否传输</td>
</tr>
<tr class="cell">
<td>90</td>
<td>ZCPLB</td>
<td>90</td>
<td>&nbsp;</td>
<td>ZE_CPLB</td>
<td>ZD_CPLB</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>EAS产品类别明细</td>
</tr>
<tr class="cell">
<td>91</td>
<td>ZZFLAG</td>
<td>91</td>
<td>&nbsp;</td>
<td>CHAR10</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>字符字段长度为 10</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>RRCTY</td>
<td>0</td>
<td>&nbsp;</td>
<td>实际</td>
</tr>
<tr class="cell">
<td>RRCTY</td>
<td>1</td>
<td>&nbsp;</td>
<td>计划</td>
</tr>
<tr class="cell">
<td>RRCTY</td>
<td>2</td>
<td>&nbsp;</td>
<td>实际分摊/分配</td>
</tr>
<tr class="cell">
<td>RRCTY</td>
<td>3</td>
<td>&nbsp;</td>
<td>计划分摊/分配</td>
</tr>
<tr class="cell">
<td>RRCTY</td>
<td>5</td>
<td>&nbsp;</td>
<td>年终关帐过帐</td>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>H</td>
<td>&nbsp;</td>
<td>贷方</td>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>S</td>
<td>&nbsp;</td>
<td>借方</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>