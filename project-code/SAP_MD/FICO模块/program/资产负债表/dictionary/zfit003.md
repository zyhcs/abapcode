<table class="outerTable">
<tr>
<td><h2>Table: ZFIT003</h2>
<h3>Description: 现金流量表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>RBUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>RYEAR</td>
<td>3</td>
<td>X</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>RPMAX</td>
<td>4</td>
<td>X</td>
<td>RPMAX</td>
<td>RPMAX</td>
<td>NUMC</td>
<td>3</td>
<td>&nbsp;</td>
<td>期间</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZTYPE</td>
<td>5</td>
<td>X</td>
<td>ZTYPE</td>
<td>CHAR3</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>报表类型</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZROW</td>
<td>6</td>
<td>X</td>
<td>ZROW</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>行号(报表中的行号)</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZTEXT</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZTEXT1</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>HSLVT</td>
<td>8</td>
<td>&nbsp;</td>
<td>HSLVT12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>用本币计算的结转余额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>