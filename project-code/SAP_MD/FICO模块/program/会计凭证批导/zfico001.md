<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO001</h2>
<h3> Description: 会计凭证批导</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*---------------------------------------------------------*<br/>
*&nbsp;程序名称：ZFICO001<br/>
*&nbsp;程序名：&nbsp;&nbsp;会计凭证批导<br/>
*&nbsp;开发日期：2021-05-27<br/>
*&nbsp;创建者：&nbsp;&nbsp;weixp<br/>
*---------------------------------------------------------*<br/>
*&nbsp;概要说明<br/>
*---------------------------------------------------------*<br/>
*&nbsp;&nbsp;会计凭证批量导入<br/>
*---------------------------------------------------------*<br/>
*&nbsp;变更记录<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;日期&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改者&nbsp;&nbsp;&nbsp;&nbsp;传输请求号&nbsp;&nbsp;&nbsp;&nbsp;修改内容及原因<br/>
*---------------------------------------------------------*<br/>
*&nbsp;yyyy-mm-dd&nbsp;&nbsp;&nbsp;&nbsp;张三&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEVK90000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因为.......所以修改了.....<br/>
*&nbsp;yyyy-mm-dd&nbsp;&nbsp;&nbsp;&nbsp;李四&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DEVK90010&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因为.......所以修改了.....<br/>
*---------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
REPORT zfico001.<br/>
<br/>
<br/>
TABLES:sscrfields,bkpf,bsid.    "选择屏幕上的字段<br/>
<br/>
TYPES: BEGIN OF typ_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;headid(10)&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证序号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-belnr,&nbsp;&nbsp;"凭证编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证模拟返回flag<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg(100)&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证模拟消息<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;head<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-bukrs,&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;blart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-blart,&nbsp;&nbsp;"凭证类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bldat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-bldat,&nbsp;&nbsp;"凭证日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-budat,&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;monat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-monat,&nbsp;&nbsp;"期间<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-waers,&nbsp;&nbsp;"货币<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-xblnr,&nbsp;&nbsp;"参考凭证号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref1_hd&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-xref1_hd,&nbsp;"参考代码1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref2_hd&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-xref2_hd,&nbsp;"参考代码2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;numpg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-numpg,&nbsp;&nbsp;"发票张数<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bktxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-bktxt,&nbsp;&nbsp;"抬头文本<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;item<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bschl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-bschl,&nbsp;&nbsp;"记账码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;newko(10)&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"屏幕科目<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kna1-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-umskz,&nbsp;&nbsp;"特别总帐标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-hkont,&nbsp;&nbsp;"总账科目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-hkont,&nbsp;&nbsp;"总账科目(屏幕科目)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr(20)&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"按本位币计的金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrbtr(20)&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证货币金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-kostl,&nbsp;&nbsp;"成本中心编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prps-posid,&nbsp;&nbsp;"WBS元素<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-aufnr,&nbsp;&nbsp;"订单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-matnr,&nbsp;&nbsp;"物料<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-werks,&nbsp;&nbsp;"工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge(20)&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-meins,&nbsp;&nbsp;"单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zfbdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-zfbdt,&nbsp;&nbsp;"基准日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hbkid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-hbkid,&nbsp;&nbsp;"开户行<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pernr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-pernr,&nbsp;&nbsp;"人员<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hktid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-hktid,&nbsp;&nbsp;"账户标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sgtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-sgtxt,&nbsp;&nbsp;"项目文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rstgr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-rstgr,&nbsp;&nbsp;"原因代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xnegp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-xnegp,&nbsp;&nbsp;"反记账标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-xref2,&nbsp;&nbsp;"参考2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-xref3,&nbsp;&nbsp;"参考3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zuonr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-zuonr,&nbsp;&nbsp;"分配<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prctr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-prctr,&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;newbw&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;newbw,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"事务类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-mwskz,&nbsp;&nbsp;&nbsp;&nbsp;"税码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fkber&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-rfarea,&nbsp;&nbsp;&nbsp;"功能范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbund&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-vbund,&nbsp;"贸易伙伴<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;network&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;nplnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;activity&nbsp;&nbsp;&nbsp;TYPE&nbsp;vornr,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzlifnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zelifnr,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzkunnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zekunnr,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gsber&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-gsber,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Text&nbsp;Desc<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shkzg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-shkzg,&nbsp;&nbsp;"借贷<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;skat-txt50,&nbsp;&nbsp;"科目描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msehl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-msehl,&nbsp;"数量单位描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;aufk-ktext,&nbsp;&nbsp;"内部订单描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext_2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;cskt-ktext,&nbsp;&nbsp;"成本中心描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt40&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t053s-txt40,&nbsp;"原因代码文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;banka&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bnka-banka,&nbsp;&nbsp;"银行描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t012t-text1,&nbsp;"账户描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt50_2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;anla-txt50,&nbsp;&nbsp;"资产描述字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kna1-name1,&nbsp;&nbsp;"客户描述字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1_2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;&nbsp;"供应商描述字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext_3&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;cepct-ktext,&nbsp;"利润中心描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1_3&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,&nbsp;&nbsp;"职员描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext_4&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;coas-ktext,&nbsp;&nbsp;"项目描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext_5&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbak-ktext,&nbsp;&nbsp;"合同描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vertn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-vertn,&nbsp;&nbsp;"合同号<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_alv.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;*****工具栏按钮设置<br/>
</div>
<div class="code">
DATA: BEGIN OF excltab OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcode&nbsp;LIKE&nbsp;sy-ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;excltab.<br/>
DATA: g_ok_code LIKE sy-ucomm.<br/>
DATA: functxt TYPE smp_dyntxt.<br/>
<br/>
DATA: gt_excel TYPE kcde_intern_struc OCCURS 0 WITH HEADER LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_alv&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;typ_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv&nbsp;&nbsp;&nbsp;TYPE&nbsp;typ_alv.<br/>
<br/>
DATA: gt_fieldcat    TYPE slis_t_fieldcat_alv.<br/>
DATA: gs_layout      TYPE slis_layout_alv.<br/>
DATA: g_check_button(1) TYPE c.<br/>
<br/>
DATA:ls_header LIKE bapiache09.                              "凭证抬头<br/>
DATA:ls_customercpd LIKE bapiacpa09 .     "<br/>
DATA:lt_bapi_item LIKE bapiacgl09 OCCURS 0 WITH HEADER LINE.      "总帐项目数据<br/>
DATA:lt_bapi_vendor LIKE bapiacap09 OCCURS 0 WITH HEADER LINE.  "供应商项目数据<br/>
DATA:lt_bapi_customer LIKE bapiacar09 OCCURS 0 WITH HEADER LINE.  "客户项目数据<br/>
DATA:lt_bapi_currency LIKE bapiaccr09 OCCURS 0 WITH HEADER LINE.  "项目货币数据<br/>
DATA:lt_bapi_extension2 LIKE bapiparex OCCURS 0 WITH HEADER LINE.  "项目货币数据<br/>
DATA:lt_bapi_criteria LIKE bapiackec9 OCCURS 0 WITH HEADER LINE. "获利能力段 分析<br/>
DATA:lt_bapi_return LIKE bapiret2 OCCURS 0 WITH HEADER LINE. "BAPI返回信息<br/>
DATA:wa_zpostbapi LIKE zficos0001." zfispostbapi.<br/>
<br/>
DATA:gv_posnr TYPE posnr.<br/>
<br/>
DATA:BEGIN OF gwa_t001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;TYPE&nbsp;t001-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;TYPE&nbsp;t001-waers,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gwa_t001.<br/>
DATA:gt_t001 LIKE STANDARD TABLE OF gwa_t001.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;PARAMETERS&nbsp;p_bukrs&nbsp;TYPE&nbsp;bkpf-bukrs&nbsp;&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;PARAMETERS&nbsp;p_file&nbsp;TYPE&nbsp;char128&nbsp;MEMORY&nbsp;ID&nbsp;file.<br/>
</div>
<div class="codeComment">
*PARAMETERS:&nbsp;ybpz&nbsp;RADIOBUTTON&nbsp;rad1&nbsp;DEFAULT&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;skd&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
</div>
<div class="code">
SELECTION-SCREEN END OF BLOCK blk1.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;****创建&nbsp;下载模板&nbsp;按钮<br/>
</div>
<div class="code">
SELECTION-SCREEN FUNCTION KEY 1.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INITIALIZATION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
INITIALIZATION.<br/>
&nbsp;&nbsp;functxt-icon_id&nbsp;&nbsp;&nbsp;=&nbsp;icon_xls.<br/>
&nbsp;&nbsp;functxt-icon_text&nbsp;=&nbsp;'下载模板'.<br/>
&nbsp;&nbsp;sscrfields-functxt_01&nbsp;=&nbsp;functxt.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;SELECTION-SCREEN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_file_input.<br/>
<br/>
AT SELECTION-SCREEN.<br/>
&nbsp;&nbsp;IF&nbsp;sscrfields-ucomm&nbsp;=&nbsp;'FC01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_downdload_template&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;START-OF-SELECTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
<br/>
</div>
<div class="codeComment">
*公司权限<br/>
</div>
<div class="code">
&nbsp;&nbsp;AUTHORITY-CHECK&nbsp;OBJECT&nbsp;'F_BKPF_BUK'<br/>
&nbsp;&nbsp;ID&nbsp;'BUKRS'&nbsp;FIELD&nbsp;p_bukrs<br/>
&nbsp;&nbsp;ID&nbsp;'ACTVT'&nbsp;FIELD&nbsp;'03'.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'您没有该公司查看权限！'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;p_file&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data_from_excel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_text.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'没有选择导入文件！'&nbsp;&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END-OF-SELECTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
END-OF-SELECTION.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_display_data.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILE_INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_file_input .<br/>
<br/>
&nbsp;&nbsp;DATA:lt_tab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;filetable,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"存放文件名的内表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_file_filter&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"file&nbsp;filter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"函数返回值<br/>
<br/>
&nbsp;&nbsp;REFRESH:lt_tab.<br/>
&nbsp;&nbsp;CLEAR:lv_file_filter,lv_rc.<br/>
<br/>
&nbsp;&nbsp;lv_file_filter&nbsp;=&nbsp;'EXCEL文件|*.XLSX;*.XLS'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_open_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选择数据文件'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_file_filter<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C:\'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_tab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_rc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_open_dialog_failed&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;AND&nbsp;lv_rc&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_tab&nbsp;INTO&nbsp;p_file&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'没有选择文件或是选择文件错误！'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_DOWNDLOAD_TEMPLATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_downdload_template .<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;w_objdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;wwwdatatab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_mime&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;w3mime,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_filename&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_fullpath&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'C:/TEMP/',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;VALUE&nbsp;'C:/TEMP/',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_destination&nbsp;LIKE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_objnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-subrc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_errtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DATA:w_objid&nbsp;TYPE&nbsp;wwwdatatab-objid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;w_dest&nbsp;&nbsp;LIKE&nbsp;sapb-sappfad.<br/>
<br/>
&nbsp;&nbsp;w_objid&nbsp;=&nbsp;'ZFICO001'.<br/>
&nbsp;&nbsp;w_filename&nbsp;&nbsp;='会计凭证批量导入模板'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'下载导入模板'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'XLS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_action<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;AND&nbsp;w_action&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;w_dest&nbsp;=&nbsp;w_fullpath.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;w_objnam&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;relid&nbsp;objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;w_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;srtf2&nbsp;=&nbsp;0&nbsp;AND&nbsp;relid&nbsp;=&nbsp;'MI'&nbsp;AND&nbsp;objid&nbsp;=&nbsp;w_objid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0&nbsp;OR&nbsp;w_objdata-objid&nbsp;EQ&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'模板文件'&nbsp;w_objnam&nbsp;'不存在'&nbsp;INTO&nbsp;w_errtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;w_errtxt&nbsp;TYPE&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;w_destination&nbsp;=&nbsp;w_dest.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;w_destination<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;w_rc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;w_rc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'模板文件：'&nbsp;w_objnam&nbsp;'下载失败'&nbsp;INTO&nbsp;w_errtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;w_errtxt&nbsp;TYPE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_DATA_FROM_EXCEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_data_from_excel .<br/>
<br/>
&nbsp;&nbsp;REFRESH&nbsp;gt_excel.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'KCD_EXCEL_OLE_TO_INT_CONVERT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;56<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;65535<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_excel<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inconsistent_parameters&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;upload_ole&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'打开文件错误，请检查文件，确保文件关闭！'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;REFRESH&nbsp;gt_alv.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gs_alv.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_excel.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;gt_excel-col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0002'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gt_excel-value&nbsp;&lt;&gt;&nbsp;p_bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'公司代码错误！'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_excel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;gt_excel-col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-headid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0002'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0003'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-blart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0004'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bldat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0005'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-budat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0006'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-monat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0007'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0008'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xblnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0009'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref1_hd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0010'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref2_hd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0011'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-numpg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0012'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bktxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0013'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bschl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0014'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-newko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0015'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-umskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0016'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;NOT&nbsp;gs_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-hkont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0017'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0018'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0019'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-kostl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0020'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-posid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0021'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-aufnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0022'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_MATN1_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_alv-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_alv-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;length_error&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0023'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0024'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0025'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_CUNIT_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_alv-meins<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-langu<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_alv-meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unit_not_found&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0026'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zfbdt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0027'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-hbkid.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-pernr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0028'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-hbkid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0029'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-hktid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0030'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0031'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-rstgr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0032'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xnegp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0033'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0034'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0035'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0036'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-prctr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-prctr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-prctr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0037'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-newbw.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0037'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-mwskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0038'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-mwskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0040'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-vbund.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-vbund<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-vbund.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0041'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zzlifnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-zzlifnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-zzlifnr.<br/>
<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0042'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zzkunnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-zzkunnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-zzkunnr.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0041'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-vertn.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0042'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-gsber.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_alv&nbsp;TO&nbsp;gt_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;&nbsp;gs_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_excel.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;gt_excel-col.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0001'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-headid.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0002'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bukrs.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0003'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-blart.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0004'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bldat.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0005'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-budat.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0006'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-monat.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0007'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-waers.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0008'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xblnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0009'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref1_hd.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0010'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-xref2_hd.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0011'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-numpg.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0012'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-bktxt.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0013'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-hkont2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0014'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-wrbtr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0015'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-rstgr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0016'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-kunnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;gs_alv-kunnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;gs_alv-kunnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0017'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr1.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0018'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0019'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr3.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0020'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr4.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0021'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE&nbsp;gt_excel-value&nbsp;TO&nbsp;gs_alv-zuonr5.<br/>
*<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_alv&nbsp;TO&nbsp;gt_alv.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;&nbsp;gs_alv.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_data .<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs&gt;&nbsp;LIKE&nbsp;gs_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_eflag(1)&nbsp;TYPE&nbsp;c.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_csks&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;csks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_csks&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;&nbsp;lt_csks.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_cepc&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;cepc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_cepc&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;&nbsp;lt_cepc.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs&gt;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;检查凭证日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bldat&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_date&nbsp;USING&nbsp;&lt;fs&gt;-bldat&nbsp;CHANGING&nbsp;l_eflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_eflag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'凭证日期的格式错误！'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'凭证日期不能为空！'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;检查过账日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-budat&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_date&nbsp;USING&nbsp;&lt;fs&gt;-budat&nbsp;CHANGING&nbsp;l_eflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_eflag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'过账日期的格式错误！'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'过账日期不能为空！'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;货币<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;tcurc&nbsp;WHERE&nbsp;waers&nbsp;=&nbsp;&lt;fs&gt;-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;&nbsp;'货币代码不存在！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;公司代码<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bukrs&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;t001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码不存在！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码不能为空！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*屏幕科目<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-newko&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;xintb&nbsp;INTO&nbsp;@DATA(lv_xintb)&nbsp;FROM&nbsp;skb1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;@&lt;fs&gt;-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;saknr&nbsp;=&nbsp;@&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;AND&nbsp;lv_xintb&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'此科目在该公司代码下只能自动过账到科目！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_xintb.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;利润中心<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-prctr&nbsp;&lt;&gt;&nbsp;''.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_alpha_input&nbsp;USING&nbsp;&lt;fs&gt;-prctr&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'KE_PROFIT_CENTER_CHECK'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;datum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-datum<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prctr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-prctr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;test_kokrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'8000'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_defined_for_date&nbsp;&nbsp;=&nbsp;2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_kokrs_for_bukrs&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;parameter_mismatch&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prctr_locked&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_defined_for_bukrs&nbsp;=&nbsp;6<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;7.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;sy-subrc.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;1.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'利润中心不存在！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'利润中心无效！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;3.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码与控制范围不一致！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;4.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码或控制范围有误！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;5.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'利润中心被锁定！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;6.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'利润中心与公司代码不一致！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-newko&nbsp;CP&nbsp;'9999*'.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'利润中心不能为空！'.<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
*&nbsp;&nbsp;&nbsp;成本中心<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-kostl&nbsp;&lt;&gt;&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_alpha_input&nbsp;USING&nbsp;&lt;fs&gt;-kostl&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;TABLE&nbsp;lt_csks&nbsp;FROM&nbsp;csks&nbsp;WHERE&nbsp;kokrs&nbsp;LIKE&nbsp;'8000'&nbsp;AND&nbsp;kostl&nbsp;=&nbsp;&lt;fs&gt;-kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'成本中心不存在！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_csks&nbsp;BY&nbsp;datbi&nbsp;DESCENDING.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_csks&nbsp;INTO&nbsp;ls_csks&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_csks-datbi&nbsp;&lt;&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'成本中心已过期！'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_csks-bukrs&nbsp;&lt;&gt;&nbsp;&lt;fs&gt;-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;&nbsp;'成本中心：'&nbsp;&lt;fs&gt;-kostl&nbsp;'不属于'&nbsp;&lt;fs&gt;-bukrs&nbsp;'公司代码！'&nbsp;INTO&nbsp;&lt;fs&gt;-msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;反记账标识<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-xnegp&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-xnegp&nbsp;NE&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'反记账标识应大写！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-monat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs&gt;-monat.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-lifnr&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-kunnr&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-hkont2&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-zzp01001<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs&gt;-zzp01001.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-zzp01002<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs&gt;-zzp01002.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-zzp01003<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs&gt;-zzp01003.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs&gt;.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;检查凭证日期<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bldat&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_date&nbsp;USING&nbsp;&lt;fs&gt;-bldat&nbsp;CHANGING&nbsp;l_eflag.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_eflag&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'凭证日期的格式错误！'&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'凭证日期不能为空！'&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;检查过账日期<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-budat&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_check_date&nbsp;USING&nbsp;&lt;fs&gt;-budat&nbsp;CHANGING&nbsp;l_eflag.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_eflag&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'过账日期的格式错误！'&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'过账日期不能为空！'&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;货币<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;tcurc&nbsp;WHERE&nbsp;waers&nbsp;=&nbsp;&lt;fs&gt;-waers.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;&nbsp;'货币代码不存在！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bukrs&nbsp;&lt;&gt;&nbsp;''.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;COUNT(*)&nbsp;FROM&nbsp;t001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码不存在！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msg&nbsp;=&nbsp;'公司代码不能为空！'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs&gt;-monat<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs&gt;-monat.<br/>
*<br/>
*<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_eflag.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_DATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GT_ALV_BLDAT&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_EFLAG&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_date  USING    pv_bldat<br/>
CHANGING pv_eflag.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DATE_CHECK_PLAUSIBILITY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;pv_bldat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plausibility_check_failed&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;pv_eflag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_ALPHA_INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GT_ALV_KOSTL&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_GT_ALV_KOSTL&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_alpha_input  USING    pv_input.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;pv_input<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;pv_input.<br/>
<br/>
ENDFORM.                    " FRM_ALPHA_INPUT<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GET_TEXT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_text .<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs&gt;&nbsp;LIKE&nbsp;gs_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_skat&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;skat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_skat&nbsp;&nbsp;TYPE&nbsp;skat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_t006a&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t006a,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_t006a&nbsp;TYPE&nbsp;t006a,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_aufk&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;aufk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_aufk&nbsp;&nbsp;TYPE&nbsp;aufk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_cskt&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;cskt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_cskt&nbsp;&nbsp;TYPE&nbsp;cskt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_t053s&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t053s,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_t053s&nbsp;TYPE&nbsp;t053s,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_t012&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t012,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_t012&nbsp;&nbsp;TYPE&nbsp;t012,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bnka&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bnka,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bnka&nbsp;&nbsp;TYPE&nbsp;bnka,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_t012t&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t012t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_t012t&nbsp;TYPE&nbsp;t012t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_anla&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;anla,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_anla&nbsp;&nbsp;TYPE&nbsp;anla,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_kna1&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;kna1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_kna1&nbsp;&nbsp;TYPE&nbsp;kna1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_lfa1&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;lfa1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_lfa1&nbsp;&nbsp;TYPE&nbsp;lfa1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_cepct&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;cepct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_cepct&nbsp;TYPE&nbsp;cepct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_coas&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;coas,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_coas&nbsp;&nbsp;TYPE&nbsp;coas,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_vbak&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;vbak,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_vbak&nbsp;&nbsp;TYPE&nbsp;vbak.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_asset_no&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiacgl09-asset_no,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_sub_number&nbsp;TYPE&nbsp;bapiacgl09-sub_number.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
*&nbsp;科目描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_skat<br/>
&nbsp;&nbsp;FROM&nbsp;skat<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;saknr&nbsp;=&nbsp;gt_alv-hkont<br/>
&nbsp;&nbsp;AND&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;AND&nbsp;spras&nbsp;=&nbsp;sy-langu.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;APPENDING&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_skat<br/>
&nbsp;&nbsp;FROM&nbsp;skat<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;saknr&nbsp;=&nbsp;gt_alv-hkont2<br/>
&nbsp;&nbsp;AND&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;AND&nbsp;spras&nbsp;=&nbsp;sy-langu.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;数量单位描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_t006a<br/>
&nbsp;&nbsp;FROM&nbsp;t006a<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;msehi&nbsp;=&nbsp;gt_alv-meins<br/>
&nbsp;&nbsp;AND&nbsp;spras&nbsp;=&nbsp;sy-langu.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;内部订单描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_aufk<br/>
&nbsp;&nbsp;FROM&nbsp;aufk<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;aufnr&nbsp;=&nbsp;gt_alv-aufnr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;成本中心描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_cskt<br/>
&nbsp;&nbsp;FROM&nbsp;cskt<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;kokrs&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;AND&nbsp;kostl&nbsp;=&nbsp;gt_alv-kostl.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;原因代码文本<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_t053s<br/>
&nbsp;&nbsp;FROM&nbsp;t053s<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;gt_alv-bukrs<br/>
&nbsp;&nbsp;AND&nbsp;rstgr&nbsp;=&nbsp;gt_alv-rstgr.<br/>
<br/>
</div>
<div class="codeComment">
**&nbsp;银行描述<br/>
*&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_t012<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t012<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;gt_alv-bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;hbkid&nbsp;=&nbsp;gt_alv-hbkid.<br/>
*<br/>
*&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_bnka<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;bnka<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;lt_t012<br/>
*&nbsp;&nbsp;&nbsp;WHERE&nbsp;banks&nbsp;=&nbsp;lt_t012-banks<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;bankl&nbsp;=&nbsp;lt_t012-bankl.<br/>
<br/>
*&nbsp;账户描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_t012t<br/>
&nbsp;&nbsp;FROM&nbsp;t012t<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;gt_alv-bukrs<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;hbkid&nbsp;=&nbsp;gt_alv-hbkid<br/>
</div>
<div class="code">
&nbsp;&nbsp;AND&nbsp;hktid&nbsp;=&nbsp;gt_alv-hktid.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;资产描述字段<br/>
<br/>
<br/>
*&nbsp;客户描述字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_kna1<br/>
&nbsp;&nbsp;FROM&nbsp;kna1<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;gt_alv-kunnr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;供应商描述字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_lfa1<br/>
&nbsp;&nbsp;FROM&nbsp;lfa1<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;gt_alv-lifnr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;利润中心描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_cepct<br/>
&nbsp;&nbsp;FROM&nbsp;cepct<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;prctr&nbsp;=&nbsp;gt_alv-prctr.<br/>
<br/>
</div>
<div class="codeComment">
**&nbsp;职员描述<br/>
*&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;APPENDING&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_lfa1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;lfa1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;gt_alv-zzp01001.<br/>
*<br/>
**&nbsp;项目描述<br/>
*&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;APPENDING&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_coas<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;coas<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;WHERE&nbsp;aufnr&nbsp;=&nbsp;gt_alv-zzp01002.<br/>
*<br/>
**&nbsp;合同描述<br/>
*&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_vbak<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;vbak<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;gt_alv-zzp01003.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs&gt;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;借/贷<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'40'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'01'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'09'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'21'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'29'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'70'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-shkzg&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'50'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'11'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'19'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'31'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'39'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'75'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;科目描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-hkont&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_skat&nbsp;INTO&nbsp;ls_skat&nbsp;WITH&nbsp;KEY&nbsp;saknr&nbsp;=&nbsp;&lt;fs&gt;-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_skat&nbsp;INTO&nbsp;ls_skat&nbsp;WITH&nbsp;KEY&nbsp;saknr&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-txt50&nbsp;=&nbsp;ls_skat-txt50.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;数量单位描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t006a&nbsp;INTO&nbsp;ls_t006a&nbsp;WITH&nbsp;KEY&nbsp;msehi&nbsp;=&nbsp;&lt;fs&gt;-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-msehl&nbsp;=&nbsp;ls_t006a-msehl.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;内部订单描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_aufk&nbsp;INTO&nbsp;ls_aufk&nbsp;WITH&nbsp;KEY&nbsp;aufnr&nbsp;=&nbsp;&lt;fs&gt;-aufnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-ktext&nbsp;=&nbsp;ls_aufk-ktext.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;成本中心描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_cskt&nbsp;INTO&nbsp;ls_cskt&nbsp;WITH&nbsp;KEY&nbsp;kostl&nbsp;=&nbsp;&lt;fs&gt;-kostl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-ktext_2&nbsp;=&nbsp;ls_cskt-ktext.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;原因代码文本<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t053s&nbsp;INTO&nbsp;ls_t053s&nbsp;WITH&nbsp;KEY&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;rstgr&nbsp;=&nbsp;&lt;fs&gt;-rstgr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-txt40&nbsp;=&nbsp;ls_t053s-txt40.<br/>
<br/>
</div>
<div class="codeComment">
**&nbsp;&nbsp;&nbsp;银行描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t012&nbsp;INTO&nbsp;ls_t012&nbsp;WITH&nbsp;KEY&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hbkid&nbsp;=&nbsp;&lt;fs&gt;-hbkid.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_bnka&nbsp;INTO&nbsp;ls_bnka&nbsp;WITH&nbsp;KEY&nbsp;banks&nbsp;=&nbsp;ls_t012-banks<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bankl&nbsp;=&nbsp;ls_t012-bankl.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-banka&nbsp;=&nbsp;ls_bnka-banka.<br/>
<br/>
*&nbsp;&nbsp;&nbsp;账户描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t012t&nbsp;INTO&nbsp;ls_t012t&nbsp;WITH&nbsp;KEY&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hbkid&nbsp;=&nbsp;&lt;fs&gt;-hbkid<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;hktid&nbsp;=&nbsp;&lt;fs&gt;-hktid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-text1&nbsp;=&nbsp;ls_t012t-text1.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;资产描述字段<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-newko&nbsp;AT&nbsp;&nbsp;INTO&nbsp;lv_asset_no&nbsp;lv_sub_number.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lv_asset_no&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_sub_number&nbsp;=&nbsp;'0000'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;txt50<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;&lt;fs&gt;-txt50_2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;anla<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;anln1&nbsp;=&nbsp;lv_asset_no<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;anln2&nbsp;=&nbsp;lv_sub_number.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;客户描述字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'01'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'11'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'09'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'19'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_kna1&nbsp;INTO&nbsp;ls_kna1&nbsp;WITH&nbsp;KEY&nbsp;kunnr&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-name1&nbsp;=&nbsp;ls_kna1-name1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;供应商描述字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'21'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'31'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'29'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-bschl&nbsp;=&nbsp;'39'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_lfa1&nbsp;INTO&nbsp;ls_lfa1&nbsp;WITH&nbsp;KEY&nbsp;lifnr&nbsp;=&nbsp;&lt;fs&gt;-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-name1_2&nbsp;=&nbsp;ls_lfa1-name1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;利润中心描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_cepct&nbsp;INTO&nbsp;ls_cepct&nbsp;WITH&nbsp;KEY&nbsp;prctr&nbsp;=&nbsp;&lt;fs&gt;-prctr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-ktext_3&nbsp;=&nbsp;ls_cepct-ktext.<br/>
<br/>
</div>
<div class="codeComment">
**&nbsp;&nbsp;&nbsp;职员描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_lfa1&nbsp;INTO&nbsp;ls_lfa1&nbsp;WITH&nbsp;KEY&nbsp;lifnr&nbsp;=&nbsp;&lt;fs&gt;-zzp01001.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-name1_3&nbsp;=&nbsp;ls_lfa1-name1.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;项目描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_coas&nbsp;INTO&nbsp;ls_coas&nbsp;WITH&nbsp;KEY&nbsp;aufnr&nbsp;=&nbsp;&lt;fs&gt;-zzp01002.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-ktext_4&nbsp;=&nbsp;ls_coas-ktext.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;合同描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_vbak&nbsp;INTO&nbsp;ls_vbak&nbsp;WITH&nbsp;KEY&nbsp;vbeln&nbsp;=&nbsp;&lt;fs&gt;-zzp01003.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-ktext_5&nbsp;=&nbsp;ls_vbak-ktext.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_skat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_t006a,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_aufk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_cskt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_t053s,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_t012,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bnka,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_t012t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bnka,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_t012t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_anla,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_kna1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_lfa1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_cepct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_coas,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_vbak,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_asset_no,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_sub_number.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
**&nbsp;科目描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;APPENDING&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_skat<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;skat<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;saknr&nbsp;=&nbsp;gt_alv-hkont2<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;spras&nbsp;=&nbsp;sy-langu.<br/>
*<br/>
*<br/>
**&nbsp;原因代码文本<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_t053s<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t053s<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;gt_alv-bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;rstgr&nbsp;=&nbsp;gt_alv-rstgr.<br/>
*<br/>
*<br/>
*<br/>
**&nbsp;客户描述字段<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_kna1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;kna1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;gt_alv-kunnr.<br/>
*<br/>
*<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;ASSIGNING&nbsp;&lt;fs&gt;.<br/>
*<br/>
**&nbsp;&nbsp;&nbsp;科目描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_skat&nbsp;INTO&nbsp;ls_skat&nbsp;WITH&nbsp;KEY&nbsp;saknr&nbsp;=&nbsp;&lt;fs&gt;-hkont2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-txt50&nbsp;=&nbsp;ls_skat-txt50.<br/>
**&nbsp;&nbsp;&nbsp;原因代码文本<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t053s&nbsp;INTO&nbsp;ls_t053s&nbsp;WITH&nbsp;KEY&nbsp;bukrs&nbsp;=&nbsp;&lt;fs&gt;-bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rstgr&nbsp;=&nbsp;&lt;fs&gt;-rstgr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-txt40&nbsp;=&nbsp;ls_t053s-txt40.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_kna1&nbsp;INTO&nbsp;ls_kna1&nbsp;WITH&nbsp;KEY&nbsp;kunnr&nbsp;=&nbsp;&lt;fs&gt;-kunnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs&gt;-name1&nbsp;=&nbsp;ls_kna1-name1.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_skat,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_t053s,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_kna1.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
*<br/>
*&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;获取本位币信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR:gt_t001.<br/>
&nbsp;&nbsp;SELECT&nbsp;bukrs&nbsp;waers<br/>
&nbsp;&nbsp;FROM&nbsp;t001<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gt_t001<br/>
&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;gt_alv<br/>
&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;gt_alv-bukrs.<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_DISPLAY_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_display_data .<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;build_fieldcat.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;build_fieldcat1.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;gs_layout-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_layout-colwidth_optimize&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_PF_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_alv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;BUILD_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM build_fieldcat .<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HEADID'&nbsp;'凭证号'&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BELNR'&nbsp;&nbsp;'凭证编号'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'FLAG'&nbsp;&nbsp;&nbsp;'标识'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MSG'&nbsp;&nbsp;&nbsp;&nbsp;'消息'&nbsp;&nbsp;'50'&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BUKRS'&nbsp;'公司代码'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BLART'&nbsp;'凭证类型'&nbsp;&nbsp;'8'&nbsp;&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BLDAT'&nbsp;'凭证日期'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BUDAT'&nbsp;'记账日期'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MONAT'&nbsp;'期间'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'WAERS'&nbsp;'货币'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XBLNR'&nbsp;'参考凭证号'&nbsp;&nbsp;'16'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF1_HD'&nbsp;'参考代码1'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF2_HD'&nbsp;'参考代码2'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NUMPG'&nbsp;'发票页数'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BKTXT'&nbsp;'抬头文本'&nbsp;&nbsp;'40'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'SHKZG'&nbsp;'借贷'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BSCHL'&nbsp;'记账码'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NEWKO'&nbsp;'屏幕科目'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NAME1'&nbsp;'客户描述字段'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NAME1_2'&nbsp;'供应商描述字段'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TXT50_2'&nbsp;'资产描述字段'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'UMSKZ'&nbsp;'特别总帐标识'&nbsp;&nbsp;'12'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HKONT'&nbsp;'总账科目'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TXT50'&nbsp;'科目描述'&nbsp;&nbsp;'50'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'DMBTR'&nbsp;'本位币金额'&nbsp;&nbsp;'15'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'WRBTR'&nbsp;'凭证货币金额'&nbsp;&nbsp;'15'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'KOSTL'&nbsp;'成本中心'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'KTEXT_2'&nbsp;'成本中心描述'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'POSID'&nbsp;'WBS元素'&nbsp;&nbsp;'24'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'AUFNR'&nbsp;'订单号'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'KTEXT'&nbsp;'内部订单描述'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MATNR'&nbsp;'物料号'&nbsp;&nbsp;'40'&nbsp;'MARA'&nbsp;'MATNR'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'WERKS'&nbsp;'工厂'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MENGE'&nbsp;'数量'&nbsp;&nbsp;'9'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MEINS'&nbsp;'单位'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MSEHL'&nbsp;'数量单位描述'&nbsp;&nbsp;'30'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZFBDT'&nbsp;'基准日期'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'PERNR'&nbsp;'人员'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HBKID'&nbsp;'开户行'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BANKA'&nbsp;'银行描述'&nbsp;&nbsp;'20'&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HKTID'&nbsp;'账户标识'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TEXT1'&nbsp;'账户描述'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'SGTXT'&nbsp;'项目文本'&nbsp;&nbsp;'40'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'RSTGR'&nbsp;'原因代码'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TXT40'&nbsp;'原因代码文本'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XNEGP'&nbsp;'反记账标识'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF2'&nbsp;'参考2'&nbsp;&nbsp;'12'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF3'&nbsp;'参考3'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR'&nbsp;'分配'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'PRCTR'&nbsp;'利润中心'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'KTEXT_3'&nbsp;'利润中心描述'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NEWBW'&nbsp;'事务类型'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MWSKZ'&nbsp;'税码'&nbsp;&nbsp;'2'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'FKBER'&nbsp;'功能范围'&nbsp;&nbsp;'16'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'VBUND'&nbsp;'贸易伙伴'&nbsp;&nbsp;'16'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NETWORK'&nbsp;'网络'&nbsp;&nbsp;'12'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ACTIVITY'&nbsp;'活动'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZZLIFNR'&nbsp;'供应商'&nbsp;&nbsp;'10'&nbsp;.<br/>
*&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZZKUNNR'&nbsp;'客户'&nbsp;&nbsp;'10'&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'VERTN'&nbsp;'合同号'&nbsp;&nbsp;'13'&nbsp;''&nbsp;''.<br/>
ENDFORM.<br/>
FORM build_fieldcat1 .<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HEADID'&nbsp;'凭证号'&nbsp;'6'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BELNR'&nbsp;&nbsp;'凭证编号'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'FLAG'&nbsp;&nbsp;&nbsp;'标识'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MSG'&nbsp;&nbsp;&nbsp;&nbsp;'消息'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BUKRS'&nbsp;'公司代码'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BLART'&nbsp;'凭证类型'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BLDAT'&nbsp;'凭证日期'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BUDAT'&nbsp;'记账日期'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'MONAT'&nbsp;'期间'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'WAERS'&nbsp;'货币'&nbsp;&nbsp;'4'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XBLNR'&nbsp;'参考凭证号'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF1_HD'&nbsp;'参考代码1'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'XREF2_HD'&nbsp;'参考代码2'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NUMPG'&nbsp;'发票页数'&nbsp;&nbsp;'8'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'BKTXT'&nbsp;'抬头文本'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'HKONT2'&nbsp;'银行科目'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TXT50'&nbsp;'科目描述'&nbsp;&nbsp;'50'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'WRBTR'&nbsp;'银行金额'&nbsp;&nbsp;'15'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'RSTGR'&nbsp;'原因代码'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'TXT40'&nbsp;'原因代码文本'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'KUNNR'&nbsp;'客户编码'&nbsp;&nbsp;'10'&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'NAME1'&nbsp;'客户描述字段'&nbsp;&nbsp;'20'&nbsp;''&nbsp;''..<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR1'&nbsp;'分配1'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR2'&nbsp;'分配2'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR3'&nbsp;'分配3'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR4'&nbsp;'分配4'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_fill_fieldcat_alv&nbsp;USING&nbsp;'GT_ALV'&nbsp;&nbsp;'ZUONR5'&nbsp;'分配5'&nbsp;&nbsp;'18'&nbsp;''&nbsp;''.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SUB_FILL_FIELDCAT_ALV<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_2352&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_2353&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_2354&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_2355&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_2356&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM sub_fill_fieldcat_alv  USING tabname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fieldlabel<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pv_outputlen<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pv_table<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pv_field.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;w_col_pos&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DATA:&nbsp;ws_fieldcat&nbsp;TYPE&nbsp;slis_fieldcat_alv.<br/>
<br/>
&nbsp;&nbsp;w_col_pos&nbsp;=&nbsp;w_col_pos&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;CLEAR&nbsp;ws_fieldcat.<br/>
<br/>
&nbsp;&nbsp;ws_fieldcat-fieldname&nbsp;=&nbsp;fieldname.<br/>
&nbsp;&nbsp;IF&nbsp;fieldlabel&nbsp;&lt;&gt;&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ws_fieldcat-seltext_l&nbsp;=&nbsp;fieldlabel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ws_fieldcat-seltext_m&nbsp;=&nbsp;fieldlabel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ws_fieldcat-seltext_s&nbsp;=&nbsp;fieldlabel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ws_fieldcat-ddictxt&nbsp;=&nbsp;'M'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ws_fieldcat-reptext_ddic&nbsp;=&nbsp;fieldlabel.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ws_fieldcat-key&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;ws_fieldcat-do_sum&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;ws_fieldcat-col_pos&nbsp;=&nbsp;w_col_pos.<br/>
&nbsp;&nbsp;ws_fieldcat-no_out&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;ws_fieldcat-hotspot&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;ws_fieldcat-tabname&nbsp;=&nbsp;tabname.<br/>
&nbsp;&nbsp;ws_fieldcat-outputlen&nbsp;&nbsp;=&nbsp;pv_outputlen.<br/>
&nbsp;&nbsp;ws_fieldcat-ref_tabname&nbsp;&nbsp;=&nbsp;pv_table.<br/>
&nbsp;&nbsp;ws_fieldcat-ref_fieldname&nbsp;&nbsp;=&nbsp;pv_field.<br/>
<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;ws_fieldcat&nbsp;TO&nbsp;gt_fieldcat&nbsp;.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SET_PF_STATUS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;EXTAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM set_pf_status USING extab TYPE slis_t_extab.<br/>
<br/>
&nbsp;&nbsp;REFRESH&nbsp;excltab.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_error_flag(1)&nbsp;TYPE&nbsp;c.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;l_error_flag&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;msg&nbsp;&lt;&gt;&nbsp;''.&nbsp;&nbsp;&nbsp;"有错误<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_error_flag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;****当有错误信息的时候，“凭证检验”&nbsp;和“凭证过账”&nbsp;按钮不可用<br/>
*&amp;****在执行&nbsp;”凭证过账“&nbsp;之前需要先执行&nbsp;”凭证检验“<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;l_error_flag&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_check_button&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;excltab-fcode&nbsp;=&nbsp;'ZCHECK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;excltab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;excltab-fcode&nbsp;=&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;excltab.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_check_button&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;excltab-fcode&nbsp;=&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;excltab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;***当凭证导入成功后，把&nbsp;“凭证过帐”&nbsp;按钮隐藏掉<br/>
</div>
<div class="code">
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;belnr&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;excltab-fcode&nbsp;=&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;excltab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'MAIN'&nbsp;EXCLUDING&nbsp;excltab&nbsp;IMMEDIATELY.<br/>
<br/>
ENDFORM.                    " SET_PF_STATUS<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;user_command<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;R_UCOMM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;RS_SELFIELD&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM user_command  USING r_ucomm LIKE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rs_selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_answer(1)&nbsp;TYPE&nbsp;c.<br/>
<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CASE&nbsp;r_ucomm&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZCHECK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_ok_code&nbsp;=&nbsp;r_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;r_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_check_button&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;msg&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e001(00)&nbsp;WITH&nbsp;'数据检查有误，请看错误信息！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_process_data_for_bapi.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_process_data_for_bapi1.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_ok_code&nbsp;=&nbsp;r_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;r_ucomm.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ybpz&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_process_data_for_bapi.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;skd&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_process_data_for_bapi1.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;INDEX&nbsp;rs_selfield-tabindex.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;rs_selfield-value&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;AND&nbsp;rs_selfield-fieldname&nbsp;=&nbsp;'BELNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BLN'&nbsp;FIELD&nbsp;gs_alv-belnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BUK'&nbsp;FIELD&nbsp;gs_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'GJR'&nbsp;FIELD&nbsp;gs_alv-budat(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'FB03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS&nbsp;.<br/>
<br/>
&nbsp;&nbsp;ENDCASE&nbsp;.<br/>
<br/>
&nbsp;&nbsp;rs_selfield-refresh&nbsp;=&nbsp;'X'.<br/>
<br/>
ENDFORM .                    "USER_COMMAND<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_PROCESS_DATA_FOR_BAPI<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_process_data_for_bapi .<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_tabix&nbsp;LIKE&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_alv&nbsp;TYPE&nbsp;typ_alv.<br/>
<br/>
&nbsp;&nbsp;CLEAR:ls_header.<br/>
&nbsp;&nbsp;CLEAR:ls_customercpd.<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_item,lt_bapi_customer,lt_bapi_vendor,lt_bapi_currency.<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_item[],lt_bapi_customer[],lt_bapi_vendor[],lt_bapi_currency[].<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_criteria,lt_bapi_criteria[].<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;gt_alv&nbsp;BY&nbsp;headid.<br/>
<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_alv&nbsp;=&nbsp;gs_alv.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;***凭证抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;NEW&nbsp;headid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_customercpd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lt_bapi_item,lt_bapi_customer,lt_bapi_vendor,lt_bapi_currency,lt_bapi_extension2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lt_bapi_item[],lt_bapi_customer[],lt_bapi_vendor[],lt_bapi_currency[],lt_bapi_extension2[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tabix&nbsp;=&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_header&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gv_posnr&nbsp;=&nbsp;gv_posnr&nbsp;+&nbsp;1.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;***凭证行项目<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_item&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;****货币信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_currency&nbsp;USING&nbsp;ls_alv.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;***增强字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_extension2&nbsp;USING&nbsp;ls_alv.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;headid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lt_bapi_return,lt_bapi_return[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_ok_code&nbsp;=&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_call_bapi_post_data&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;g_ok_code&nbsp;=&nbsp;'ZCHECK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_call_bapi_check_data&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_alv,ls_alv.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILL_HEADER<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_header  USING    p_ls_alv TYPE typ_alv.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_header-bus_act&nbsp;&nbsp;&nbsp;=&nbsp;'RFBU'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_header-username&nbsp;&nbsp;&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;ls_header-header_txt&nbsp;=&nbsp;p_ls_alv-bktxt.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证抬头文本<br/>
&nbsp;&nbsp;ls_header-comp_code&nbsp;&nbsp;=&nbsp;p_ls_alv-bukrs.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;ls_header-doc_date&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-bldat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证日期<br/>
&nbsp;&nbsp;ls_header-pstng_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;ls_header-fisc_year&nbsp;&nbsp;=&nbsp;p_ls_alv-budat(4).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"财年<br/>
&nbsp;&nbsp;IF&nbsp;p_ls_alv-monat&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header-fis_period&nbsp;=&nbsp;p_ls_alv-budat+4(2).&nbsp;"会计期间<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header-fis_period&nbsp;=&nbsp;p_ls_alv-monat.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ls_header-doc_type&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-blart.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证类型<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_header-neg_postng&nbsp;=&nbsp;p_ls_alv-xnegp.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"标识:&nbsp;反记帐<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_header-ref_doc_no&nbsp;=&nbsp;p_ls_alv-xblnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"标识:&nbsp;反记帐<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILL_ITEM<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_item  USING    p_ls_alv TYPE typ_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_asset_no&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiacgl09-asset_no,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_sub_number&nbsp;TYPE&nbsp;bapiacgl09-sub_number,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_ktogr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;anla-ktogr.<br/>
<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_item,lt_bapi_vendor,lt_bapi_customer.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'40'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'50'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-gl_account&nbsp;=&nbsp;p_ls_alv-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-doc_type&nbsp;=&nbsp;p_ls_alv-blart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-monat&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fis_period&nbsp;=&nbsp;p_ls_alv-budat+4(2).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fis_period&nbsp;=&nbsp;p_ls_alv-monat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fisc_year&nbsp;=&nbsp;p_ls_alv-budat(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-pstng_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-costcenter&nbsp;=&nbsp;p_ls_alv-kostl.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-wbs_element&nbsp;=&nbsp;p_ls_alv-posid.&nbsp;&nbsp;&nbsp;&nbsp;"WBS元素<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-orderid&nbsp;=&nbsp;p_ls_alv-aufnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-material_long&nbsp;=&nbsp;p_ls_alv-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-plant&nbsp;=&nbsp;p_ls_alv-werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-quantity&nbsp;=&nbsp;p_ls_alv-menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-base_uom&nbsp;=&nbsp;p_ls_alv-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-tax_code&nbsp;=&nbsp;p_ls_alv-mwskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-func_area_long&nbsp;=&nbsp;p_ls_alv-fkber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-network&nbsp;=&nbsp;p_ls_alv-network.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-activity&nbsp;=&nbsp;p_ls_alv-activity.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-wbs_element&nbsp;=&nbsp;gs_alv-posid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-bus_area&nbsp;=&nbsp;p_ls_alv-gsber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_item.<br/>
<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'09'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'19'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-customer&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-newko.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;p_ls_alv-newko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lt_bapi_customer-customer.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;skont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_customer-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t074<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;koart&nbsp;=&nbsp;'D'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;umskz&nbsp;=&nbsp;p_ls_alv-umskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-gl_account&nbsp;=&nbsp;p_ls_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-bline_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-sp_gl_ind&nbsp;=&nbsp;p_ls_alv-umskz.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"特殊总帐标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-bus_area&nbsp;=&nbsp;p_ls_alv-gsber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_customer.<br/>
<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'29'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'39'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-vendor_no&nbsp;=&nbsp;p_ls_alv-newko.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;p_ls_alv-newko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lt_bapi_vendor-vendor_no.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;skont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_vendor-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t074<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ktopl&nbsp;=&nbsp;'8000'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;koart&nbsp;=&nbsp;'K'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;umskz&nbsp;=&nbsp;p_ls_alv-umskz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-gl_account&nbsp;=&nbsp;p_ls_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-bline_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-bus_area&nbsp;=&nbsp;p_ls_alv-gsber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_vendor.<br/>
<br/>
<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'70'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'75'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;SPLIT&nbsp;p_ls_alv-newko&nbsp;AT&nbsp;'-'&nbsp;INTO&nbsp;lv_asset_no&nbsp;lv_sub_number.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lv_asset_no&nbsp;=&nbsp;p_ls_alv-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_sub_number&nbsp;=&nbsp;'0000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;lv_asset_no<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_asset_no.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;lv_sub_number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_sub_number.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ktogr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lv_ktogr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;anla<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;anln1&nbsp;=&nbsp;lv_asset_no<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;anln2&nbsp;=&nbsp;lv_sub_number.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ktansw<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_item-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t095<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ktogr&nbsp;=&nbsp;lv_ktogr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-gl_account&nbsp;=&nbsp;p_ls_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-doc_type&nbsp;=&nbsp;p_ls_alv-blart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-monat&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fis_period&nbsp;=&nbsp;p_ls_alv-budat+4(2).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fis_period&nbsp;=&nbsp;p_ls_alv-monat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-acct_type&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fisc_year&nbsp;=&nbsp;p_ls_alv-budat(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-pstng_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-costcenter&nbsp;=&nbsp;p_ls_alv-kostl.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-wbs_element&nbsp;=&nbsp;p_ls_alv-posid.&nbsp;&nbsp;&nbsp;&nbsp;"WBS元素<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-orderid&nbsp;=&nbsp;p_ls_alv-aufnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-material&nbsp;=&nbsp;p_ls_alv-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-plant&nbsp;=&nbsp;p_ls_alv-werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-quantity&nbsp;=&nbsp;p_ls_alv-menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-base_uom&nbsp;=&nbsp;p_ls_alv-meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-asset_no&nbsp;=&nbsp;lv_asset_no.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-sub_number&nbsp;=&nbsp;lv_sub_number.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_item.<br/>
<br/>
<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'01'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'11'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-customer&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-newko.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;p_ls_alv-newko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lt_bapi_customer-customer.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;akont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_customer-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;knb1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_ls_alv-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;kunnr&nbsp;=&nbsp;lt_bapi_customer-customer.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-gl_account&nbsp;=&nbsp;p_ls_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-bline_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-sp_gl_ind&nbsp;=&nbsp;p_ls_alv-umskz.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"特殊总帐标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-bus_area&nbsp;=&nbsp;p_ls_alv-gsber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_customer.<br/>
<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'21'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'31'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-vendor_no&nbsp;=&nbsp;p_ls_alv-newko.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;p_ls_alv-newko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lt_bapi_vendor-vendor_no.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-hkont&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;akont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_vendor-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;lfb1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_ls_alv-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;lifnr&nbsp;=&nbsp;lt_bapi_vendor-vendor_no.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-gl_account&nbsp;=&nbsp;p_ls_alv-hkont.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-item_text&nbsp;=&nbsp;p_ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-comp_code&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-bline_date&nbsp;=&nbsp;p_ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-alloc_nmbr&nbsp;=&nbsp;p_ls_alv-zuonr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分配编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-profit_ctr&nbsp;=&nbsp;p_ls_alv-prctr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"利润中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-ref_key_2&nbsp;=&nbsp;p_ls_alv-xref2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-ref_key_3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_vendor-bus_area&nbsp;=&nbsp;p_ls_alv-gsber.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_vendor.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILL_CURRENCY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_currency  USING    p_ls_alv TYPE typ_alv.<br/>
&nbsp;&nbsp;DATA:lv_amt_doccur&nbsp;TYPE&nbsp;bapidoccur.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;dcpfm&nbsp;INTO&nbsp;@DATA(lv_dcpfm)&nbsp;FROM&nbsp;usr01<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bname&nbsp;=&nbsp;@sy-uname.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;lt_bapi_currency.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_t001&nbsp;INTO&nbsp;gwa_t001&nbsp;WITH&nbsp;KEY&nbsp;bukrs&nbsp;=&nbsp;p_ls_alv-bukrs.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ls_alv-waers&nbsp;&lt;&gt;&nbsp;gwa_t001-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-curr_type&nbsp;&nbsp;=&nbsp;'00'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'UNITS_STRING_CONVERT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units_string&nbsp;=&nbsp;p_ls_alv-wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dcpfm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_dcpfm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_amt_doccur<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_type&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lv_amt_doccur.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&nbsp;p_ls_alv-shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lt_bapi_currency-amt_doccur&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_currency.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-curr_type&nbsp;&nbsp;=&nbsp;'10'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;=&nbsp;gwa_t001-waers.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;&nbsp;=&nbsp;'CNY'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'UNITS_STRING_CONVERT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units_string&nbsp;=&nbsp;p_ls_alv-wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dcpfm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_dcpfm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_amt_doccur<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_type&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lv_amt_doccur.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&nbsp;p_ls_alv-shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lt_bapi_currency-amt_doccur&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_currency.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;&nbsp;=&nbsp;p_ls_alv-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'UNITS_STRING_CONVERT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units_string&nbsp;=&nbsp;p_ls_alv-wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dcpfm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_dcpfm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_amt_doccur<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_type&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lv_amt_doccur.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&nbsp;p_ls_alv-shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;lt_bapi_currency-amt_doccur&nbsp;*&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_currency.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILL_EXTENSION2_n<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_extension2  USING    p_ls_alv TYPE typ_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_menge&nbsp;TYPE&nbsp;menge_d.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;dcpfm&nbsp;INTO&nbsp;@DATA(lv_dcpfm)&nbsp;FROM&nbsp;usr01<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bname&nbsp;=&nbsp;@sy-uname.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;wa_zpostbapi.<br/>
&nbsp;&nbsp;wa_zpostbapi-posnr&nbsp;=&nbsp;gv_posnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证行项目<br/>
&nbsp;&nbsp;wa_zpostbapi-numpg&nbsp;=&nbsp;p_ls_alv-numpg.&nbsp;"凭证行项目原因代码<br/>
&nbsp;&nbsp;wa_zpostbapi-rstgr&nbsp;=&nbsp;p_ls_alv-rstgr.&nbsp;"凭证行项目原因代码<br/>
&nbsp;&nbsp;wa_zpostbapi-bschl&nbsp;=&nbsp;p_ls_alv-bschl.&nbsp;"凭证行项目过账码<br/>
&nbsp;&nbsp;wa_zpostbapi-umskz&nbsp;=&nbsp;p_ls_alv-umskz.&nbsp;"凭证行项目特别总账标识<br/>
&nbsp;&nbsp;wa_zpostbapi-zfbdt&nbsp;=&nbsp;p_ls_alv-zfbdt.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-hbkid&nbsp;=&nbsp;p_ls_alv-hbkid.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-hktid&nbsp;=&nbsp;p_ls_alv-hktid.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-xnegp&nbsp;=&nbsp;p_ls_alv-xnegp.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-xref1_hd&nbsp;=&nbsp;p_ls_alv-xref1_hd.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-xref2_hd&nbsp;=&nbsp;p_ls_alv-xref2_hd.&nbsp;"<br/>
&nbsp;&nbsp;wa_zpostbapi-anbwa&nbsp;=&nbsp;p_ls_alv-newbw.<br/>
&nbsp;&nbsp;wa_zpostbapi-vbund&nbsp;=&nbsp;p_ls_alv-vbund."贸易伙伴<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;wa_zpostbapi-zzlifnr&nbsp;=&nbsp;p_ls_alv-zzlifnr.<br/>
*&nbsp;&nbsp;wa_zpostbapi-zzkunnr&nbsp;=&nbsp;p_ls_alv-zzkunnr.<br/>
</div>
<div class="code">
&nbsp;&nbsp;wa_zpostbapi-vertn&nbsp;=&nbsp;p_ls_alv-vertn.&nbsp;&nbsp;&nbsp;"合同号<br/>
&nbsp;&nbsp;wa_zpostbapi-kostl&nbsp;=&nbsp;p_ls_alv-kostl.<br/>
&nbsp;&nbsp;wa_zpostbapi-xref3&nbsp;=&nbsp;p_ls_alv-xref3.<br/>
&nbsp;&nbsp;wa_zpostbapi-aufnr&nbsp;=&nbsp;p_ls_alv-aufnr.<br/>
&nbsp;&nbsp;wa_zpostbapi-matnr&nbsp;=&nbsp;p_ls_alv-matnr.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'UNITS_STRING_CONVERT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units_string&nbsp;=&nbsp;p_ls_alv-menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dcpfm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_dcpfm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_type&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-menge&nbsp;=&nbsp;lv_menge.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;wa_zpostbapi-meins&nbsp;=&nbsp;p_ls_alv-meins.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'70'&nbsp;OR&nbsp;p_ls_alv-bschl&nbsp;=&nbsp;'75'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-anln1&nbsp;=&nbsp;p_ls_alv-newko.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-anln2&nbsp;=&nbsp;'0000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;wa_zpostbapi-anln1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;wa_zpostbapi-anln1.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;wa_zpostbapi-anln2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;wa_zpostbapi-anln2.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;zficos0001<br/>
</div>
<div class="code">
&nbsp;&nbsp;lt_bapi_extension2-structure&nbsp;&nbsp;=&nbsp;'ZFICOS0001'."'ZFISPOSTBAPI'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_container_utilities=&gt;fill_container_c<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wa_zpostbapi<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_container&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_extension2-valuepart1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;illegal_parameter_type&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;lt_bapi_extension2.<br/>
<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CALL_BAPI_POST_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_call_bapi_post_data  USING    p_ls_alv TYPE typ_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_message(200)&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_alv&nbsp;TYPE&nbsp;typ_alv.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_ACC_DOCUMENT_POST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;documentheader&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;customercpd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_customercpd<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTRACTHEADER&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_SYS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountgl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountreceivable&nbsp;=&nbsp;lt_bapi_customer<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountpayable&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_vendor<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACCOUNTTAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;currencyamount&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_currency<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;criteria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_criteria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"获利能力<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUEFIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXTENSION1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_return<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PAYMENTCARD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTRACTITEM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;extension2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_extension2[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REALESTATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACCOUNTWT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;l_message.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_bapi_return&nbsp;&nbsp;WHERE&nbsp;type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;l_message&nbsp;lt_bapi_return-message&nbsp;'；'&nbsp;INTO&nbsp;l_message.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;l_message&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_bapi_return&nbsp;INDEX&nbsp;1.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;headid&nbsp;=&nbsp;p_ls_alv-headid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-belnr&nbsp;=&nbsp;lt_bapi_return-message_v2(10).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-flag&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_alv&nbsp;FROM&nbsp;gs_alv&nbsp;INDEX&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;headid&nbsp;=&nbsp;p_ls_alv-headid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-msg&nbsp;=&nbsp;l_message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-flag&nbsp;=&nbsp;'F'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_alv&nbsp;FROM&nbsp;gs_alv&nbsp;INDEX&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CALL_BAPI_CHECK_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_LS_ALV&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_call_bapi_check_data  USING    p_ls_alv TYPE typ_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_message(200)&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_alv&nbsp;TYPE&nbsp;typ_alv.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_ACC_DOCUMENT_CHECK'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;documentheader&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;customercpd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_customercpd<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTRACTHEADER&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OBJ_SYS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountgl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountreceivable&nbsp;=&nbsp;lt_bapi_customer<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;accountpayable&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_vendor<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACCOUNTTAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;currencyamount&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_currency<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;criteria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_criteria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"获利能力<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUEFIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXTENSION1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_return<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PAYMENTCARD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTRACTITEM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;extension2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapi_extension2<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REALESTATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACCOUNTWT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;l_message.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_bapi_return&nbsp;&nbsp;WHERE&nbsp;type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;l_message&nbsp;lt_bapi_return-message&nbsp;'；'&nbsp;INTO&nbsp;l_message.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;headid&nbsp;=&nbsp;p_ls_alv-headid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-msg&nbsp;=&nbsp;l_message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_alv&nbsp;FROM&nbsp;gs_alv&nbsp;INDEX&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_process_data_for_bapi1 .<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_bsid&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bsid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid&nbsp;&nbsp;TYPE&nbsp;bsid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bsid1&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bsid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid1&nbsp;TYPE&nbsp;bsid.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_tabix&nbsp;LIKE&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_alv&nbsp;TYPE&nbsp;typ_alv.<br/>
<br/>
&nbsp;&nbsp;CLEAR:ls_header.<br/>
&nbsp;&nbsp;CLEAR:ls_customercpd.<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_item,lt_bapi_customer,lt_bapi_vendor,lt_bapi_currency.<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_item[],lt_bapi_customer[],lt_bapi_vendor[],lt_bapi_currency[].<br/>
&nbsp;&nbsp;CLEAR:lt_bapi_criteria,lt_bapi_criteria[].<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;gt_alv&nbsp;BY&nbsp;headid.<br/>
<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;kunnr&nbsp;zuonr&nbsp;shkzg&nbsp;dmbtr&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;&nbsp;lt_bsid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;bsid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;ls_alv-kunnr.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_alv&nbsp;=&nbsp;gs_alv.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_customercpd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lt_bapi_item,lt_bapi_customer,lt_bapi_vendor,lt_bapi_currency,lt_bapi_extension2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lt_bapi_item[],lt_bapi_customer[],lt_bapi_vendor[],lt_bapi_currency[],lt_bapi_extension2[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_tabix&nbsp;=&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_header&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gv_posnr&nbsp;=&nbsp;gv_posnr&nbsp;+&nbsp;1.<br/>
</div>
<div class="codeComment">
*银行<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-gl_account&nbsp;=&nbsp;ls_alv-hkont2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-item_text&nbsp;=&nbsp;ls_alv-sgtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-doc_type&nbsp;=&nbsp;ls_alv-blart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-comp_code&nbsp;=&nbsp;ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fis_period&nbsp;=&nbsp;ls_alv-monat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-fisc_year&nbsp;=&nbsp;ls_alv-budat(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-pstng_date&nbsp;=&nbsp;ls_alv-budat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"过账日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_item-costcenter&nbsp;=&nbsp;ls_alv-kostl.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;&nbsp;=&nbsp;ls_alv-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;ls_alv-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_currency.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;wa_zpostbapi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-posnr&nbsp;=&nbsp;gv_posnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证行项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-rstgr&nbsp;=&nbsp;ls_alv-rstgr.&nbsp;"凭证行项目原因代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-bschl&nbsp;=&nbsp;'40'.&nbsp;"凭证行项目过账码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_extension2-structure&nbsp;&nbsp;=&nbsp;'ZFICOS0001'."'ZFISPOSTBAPI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_extension2-valuepart1&nbsp;=&nbsp;wa_zpostbapi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_extension2.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gv_posnr&nbsp;=&nbsp;gv_posnr&nbsp;+&nbsp;1.<br/>
<br/>
</div>
<div class="codeComment">
*客户，有5个分配，按金额组合，如果一致输出5行，如果不一致输出一一行。<br/>
*按分配汇总，如果金额有一致的就清账<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;kunnr&nbsp;zuonr&nbsp;shkzg&nbsp;dmbtr&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;&nbsp;lt_bsid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;bsid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;ls_alv-kunnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_bsid&nbsp;INTO&nbsp;ls_bsid&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid1-kunnr&nbsp;=&nbsp;ls_bsid-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid1-zuonr&nbsp;=&nbsp;ls_bsid-zuonr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_bsid-shkzg&nbsp;=&nbsp;'H'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid1-dmbtr&nbsp;=&nbsp;-&nbsp;ls_bsid-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bsid1-dmbtr&nbsp;=&nbsp;ls_bsid-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_bsid1&nbsp;INTO&nbsp;lt_bsid1.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;akont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lt_bapi_customer-gl_account<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;knb1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;ls_alv-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;kunnr&nbsp;=&nbsp;ls_alv-kunnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;&nbsp;lt_bsid1&nbsp;INTO&nbsp;ls_bsid1&nbsp;WITH&nbsp;&nbsp;KEY&nbsp;dmbtr&nbsp;=&nbsp;gs_alv-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-alloc_nmbr&nbsp;=&nbsp;ls_bsid1-zuonr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-customer&nbsp;=&nbsp;ls_alv-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-comp_code&nbsp;=&nbsp;ls_alv-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-bline_date&nbsp;=&nbsp;ls_alv-budat.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_customer-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_customer.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-itemno_acc&nbsp;=&nbsp;gv_posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-currency&nbsp;&nbsp;&nbsp;=&nbsp;ls_alv-waers.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_currency-amt_doccur&nbsp;=&nbsp;-&nbsp;ls_alv-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_currency.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-posnr&nbsp;=&nbsp;gv_posnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"凭证行项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_zpostbapi-bschl&nbsp;=&nbsp;'11'.&nbsp;"凭证行项目过账码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_extension2-structure&nbsp;&nbsp;=&nbsp;'ZFICOS0001'."'ZFISPOSTBAPI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapi_extension2-valuepart1&nbsp;=&nbsp;wa_zpostbapi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_bapi_extension2.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lt_bapi_return,lt_bapi_return[].<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_ok_code&nbsp;=&nbsp;'ZPOST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_call_bapi_post_data&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;g_ok_code&nbsp;=&nbsp;'ZCHECK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_call_bapi_check_data&nbsp;USING&nbsp;ls_alv&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gv_posnr.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_alv,ls_alv,lt_bsid,ls_bsid,ls_bsid1,lt_bsid1.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUKRS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;P_FILE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;文件<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;00<br/>
*001&nbsp;&nbsp;&nbsp;&amp;1&amp;2&amp;3&amp;4&amp;5&amp;6&amp;7&amp;8<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;您没有该公司查看权限！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>