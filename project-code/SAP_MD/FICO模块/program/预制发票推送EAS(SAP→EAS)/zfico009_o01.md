<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO009_O01</h2>
<h3> Description: Include ZFICOIF007_O01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICOIF007_O01<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_BULID_LAYOUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;设置ALV的布局<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_BULID_LAYOUT .<br/>
&nbsp;&nbsp;GW_LAYOUT-ZEBRA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;''.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;斑马条&nbsp;使ALV界面呈现颜色交替<br/>
&nbsp;&nbsp;GW_LAYOUT-CWIDTH_OPT&nbsp;=&nbsp;''.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;自动优化列宽<br/>
&nbsp;&nbsp;GW_LAYOUT-SEL_MODE&nbsp;&nbsp;&nbsp;=&nbsp;'A'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;选择模式，“A”在最左端有选择按钮<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_BULID_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;使用宏来填充FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_BULID_FIELDCAT .<br/>
&nbsp;&nbsp;REFRESH&nbsp;GT_FIELDCAT&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'CK'&nbsp;TEXT-O01&nbsp;''&nbsp;''&nbsp;'X'&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'msg'&nbsp;TEXT-O33&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BELNR'&nbsp;TEXT-O02&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'GJAHR'&nbsp;TEXT-O03&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'RBSTAT_TEXT'&nbsp;TEXT-O04&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'ZREL_TEXT'&nbsp;TEXT-O05&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'ZFLAG_TEXT'&nbsp;TEXT-O06&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'ZSTATUS_TEXT'&nbsp;TEXT-O07&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'ZEASDH'&nbsp;TEXT-O08&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'LIFNR'&nbsp;TEXT-O09&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'NAME1'&nbsp;TEXT-O10&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BUDAT'&nbsp;TEXT-O11&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'NAME_TEXTC'&nbsp;TEXT-O12&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'CPUDT'&nbsp;TEXT-O13''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BUKRS'&nbsp;TEXT-O14&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BUTXT'&nbsp;TEXT-O15&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'WAERS'&nbsp;TEXT-O16&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'KURSF'&nbsp;TEXT-O17&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'XBLNR'&nbsp;TEXT-O18&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'RMWWR'&nbsp;TEXT-O19&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BEZNK'&nbsp;TEXT-O20&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'WMWST1'&nbsp;TEXT-O21&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BKTXT'&nbsp;TEXT-O22&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'EBELN'&nbsp;TEXT-O23&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'EBELP'&nbsp;TEXT-O24&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BUZEI'&nbsp;TEXT-O35&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MATNR'&nbsp;TEXT-O25&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MAKTX'&nbsp;TEXT-O34&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'SHKZG'&nbsp;TEXT-O26&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'LFBNR'&nbsp;TEXT-O27&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'LFPOS'&nbsp;TEXT-O28&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'WRBTR'&nbsp;TEXT-O29&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MENGE'&nbsp;TEXT-O30&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MEINS'&nbsp;TEXT-O31&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'SGTXT'&nbsp;TEXT-O32&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_ALV_DISPLAY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_ALV_DISPLAY .<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_BULID_LAYOUT&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_BULID_FIELDCAT&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;GT_DATA&nbsp;IS&nbsp;not&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BUFFER_ACTIVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;SY-REPID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_PF_STATUS_SET&nbsp;=&nbsp;'FRM_SET_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_USER_COMMAND&nbsp;&nbsp;=&nbsp;'FRM_USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_LAYOUT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GW_LAYOUT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_FIELDCAT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_FIELDCAT[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_SORT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_SORT[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SAVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_OUTTAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_DATA[].<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;TEXT-W01&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>