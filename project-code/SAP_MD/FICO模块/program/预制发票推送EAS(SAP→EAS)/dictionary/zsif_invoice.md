<table class="outerTable">
<tr>
<td><h2>Table: ZSIF_INVOICE</h2>
<h3>Description: 预制发票</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUKRS</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUDAT</td>
<td>2</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>XBLNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>XBLNR</td>
<td>XBLNR</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>参考凭证编号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BKTXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>BKTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>凭证抬头文本</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BLINE_DATE</td>
<td>5</td>
<td>&nbsp;</td>
<td>DZFBDT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>到期日期计算的起算日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>DIFF_INV</td>
<td>6</td>
<td>&nbsp;</td>
<td>LIFRE</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>不同出票方</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ALLOC_NMBR</td>
<td>7</td>
<td>&nbsp;</td>
<td>DZUONR</td>
<td>ZUONR</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>分配编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>PO_NUMBER</td>
<td>8</td>
<td>&nbsp;</td>
<td>BSTNR</td>
<td>EBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>采购订单号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>PROFIT_CTR</td>
<td>9</td>
<td>&nbsp;</td>
<td>PRCTR</td>
<td>PRCTR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>利润中心</td>
</tr>
<tr class="cell">
<td>10</td>
<td>BUS_AREA</td>
<td>10</td>
<td>&nbsp;</td>
<td>GSBER</td>
<td>GSBER</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>业务范围</td>
</tr>
<tr class="cell">
<td>11</td>
<td>GL_ACCOUNT</td>
<td>11</td>
<td>&nbsp;</td>
<td>HKONT</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ITEM_AMOUNT</td>
<td>12</td>
<td>&nbsp;</td>
<td>BAPIWRBTR</td>
<td>BAPICURR</td>
<td>DEC</td>
<td>23</td>
<td>&nbsp;</td>
<td>凭证货币金额</td>
</tr>
<tr class="cell">
<td>13</td>
<td>DB_CR_IND</td>
<td>13</td>
<td>&nbsp;</td>
<td>SHKZG</td>
<td>SHKZG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>借/贷标识</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>H</td>
<td>&nbsp;</td>
<td>贷方</td>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>S</td>
<td>&nbsp;</td>
<td>借方</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>