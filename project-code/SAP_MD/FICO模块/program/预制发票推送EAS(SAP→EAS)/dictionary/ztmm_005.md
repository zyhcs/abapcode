<table class="outerTable">
<tr>
<td><h2>Table: ZTMM_005</h2>
<h3>Description: 接口推送状态表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BELNR</td>
<td>2</td>
<td>X</td>
<td>RE_BELNR</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>发票凭证的凭证编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>GJAHR</td>
<td>3</td>
<td>X</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZEASDH</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZEASDH</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>25</td>
<td>&nbsp;</td>
<td>EAS应付单号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZRELEASE</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZE_ZRELEASE</td>
<td>ZDM_ZRELEASE</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>审批标识</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZFLAG</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZE_ZFLAG</td>
<td>ZDM_ZFLAG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>操作标识</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZSTATUS</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZE_STATUS</td>
<td>ZDM_STATUS</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>状态标识</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZDM_ZRELEASE</td>
<td>T</td>
<td>&nbsp;</td>
<td>审核通过</td>
</tr>
<tr class="cell">
<td>ZDM_ZRELEASE</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>审核中</td>
</tr>
<tr class="cell">
<td>ZDM_ZFLAG</td>
<td>L</td>
<td>&nbsp;</td>
<td>不可修改</td>
</tr>
<tr class="cell">
<td>ZDM_ZFLAG</td>
<td>W</td>
<td>&nbsp;</td>
<td>可以修改</td>
</tr>
<tr class="cell">
<td>ZDM_STATUS</td>
<td>D</td>
<td>&nbsp;</td>
<td>可以冲销</td>
</tr>
<tr class="cell">
<td>ZDM_STATUS</td>
<td>B</td>
<td>&nbsp;</td>
<td>不可冲销</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>