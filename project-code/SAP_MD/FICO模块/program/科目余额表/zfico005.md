<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO005</h2>
<h3> Description: 科目余额表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICOR0001<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
report zfico005 message-id 00.<br/>
<br/>
include <a href="zficor005_top.html">zficor005_top</a>.<br/>
<br/>
include <a href="zficor005_sel.html">zficor005_sel</a>.<br/>
<br/>
<br/>
include <a href="zficor005_f01.html">zficor005_f01</a>.<br/>
<br/>
<br/>
include <a href="zficor005_pbo.html">zficor005_pbo</a>.<br/>
<br/>
<br/>
include <a href="zficor005_pai.html">zficor005_pai</a>.<br/>
<br/>
<br/>
at selection-screen on value-request for p_banben.<br/>
&nbsp;&nbsp;perform&nbsp;frm_val_banben&nbsp;changing&nbsp;p_banben.<br/>
<br/>
at selection-screen on value-request for s_fmisn1-low.<br/>
&nbsp;&nbsp;perform&nbsp;frm_val_fmisn1&nbsp;changing&nbsp;s_fmisn1-low.<br/>
<br/>
at selection-screen on value-request for s_fmisn1-high.<br/>
&nbsp;&nbsp;perform&nbsp;frm_val_fmisn1&nbsp;changing&nbsp;s_fmisn1-high.<br/>
<br/>
</div>
<div class="codeComment">
*AT&nbsp;SELECTION-SCREEN&nbsp;ON&nbsp;VALUE-REQUEST&nbsp;FOR&nbsp;s_prctr-low.<br/>
*&nbsp;&nbsp;PERFORM&nbsp;frm_val_prctr&nbsp;CHANGING&nbsp;s_prctr-low.<br/>
*<br/>
*AT&nbsp;SELECTION-SCREEN&nbsp;ON&nbsp;VALUE-REQUEST&nbsp;FOR&nbsp;s_prctr-high.<br/>
*&nbsp;&nbsp;PERFORM&nbsp;frm_val_prctr&nbsp;CHANGING&nbsp;s_prctr-high.<br/>
<br/>
<br/>
</div>
<div class="code">
start-of-selection.<br/>
&nbsp;&nbsp;if&nbsp;g_ckcfg&nbsp;eq&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;submit&nbsp;zfir0010&nbsp;with&nbsp;s_bukrs&nbsp;in&nbsp;s_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;with&nbsp;p_year&nbsp;&nbsp;=&nbsp;p_year<br/>
&nbsp;&nbsp;&nbsp;&nbsp;with&nbsp;s_poper&nbsp;in&nbsp;s_poper<br/>
&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;return.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
&nbsp;&nbsp;perform&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;perform&nbsp;frm_prc_data.<br/>
<br/>
END-OF-SELECTION.<br/>
&nbsp;&nbsp;check&nbsp;g_ckcfg&nbsp;eq&nbsp;space.<br/>
<br/>
&nbsp;&nbsp;if&nbsp;g_msg&nbsp;is&nbsp;initial.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;screen&nbsp;100.<br/>
&nbsp;&nbsp;else.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001&nbsp;WITH&nbsp;g_msg&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;g_msg&nbsp;type&nbsp;'S'&nbsp;display&nbsp;like&nbsp;'E'.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;G_CKCFG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;检查科目配置表<br/>
*&nbsp;G_DOCUR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凭证货币<br/>
*&nbsp;G_LOCUR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本位币<br/>
*&nbsp;P_BANBEN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本<br/>
*&nbsp;P_YEAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计年度<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_FMISN1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;科目<br/>
*&nbsp;S_GSBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业务范围<br/>
*&nbsp;S_POPER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账期间<br/>
*&nbsp;S_PRCTR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;利润中心<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;00<br/>
*001&nbsp;&nbsp;&nbsp;&amp;1&amp;2&amp;3&amp;4&amp;5&amp;6&amp;7&amp;8<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;不存在相应的报表版本号！<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>