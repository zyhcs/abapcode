<table class="outerTable">
<tr>
<td><h2>Table: ZCRS_EXCEL_BLOCK_01</h2>
<h3>Description: ZCRS_EXCEL_BLOCK_01</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MACRO</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>256</td>
<td>&nbsp;</td>
<td>Excel宏名</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>FIELD</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>256</td>
<td>&nbsp;</td>
<td>字段</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BROW</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>起始行</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BCOL</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>起始列</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>EROW</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>结束行</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ECOL</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>结束列</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>TROW</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>目标起始行</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>TCOL</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>目标起始列</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>SHEET</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>sheet名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>