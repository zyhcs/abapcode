<table class="outerTable">
<tr>
<td><h2>Table: ZCRS_ALSMEX_TABLINE</h2>
<h3>Description: ZCRS_ALSMEX_TABLINE</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ROW</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>弹性Excel上传：行号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>COL</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>列</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VALUE</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>256</td>
<td>&nbsp;</td>
<td>WWI 收回的数据元素</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BORDERS</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否需要画线框</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>POSITION</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>对齐</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BOLD</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否粗体</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>SIZE</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>字体</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>SHEET</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>sheet名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>