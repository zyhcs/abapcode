<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICOR005_SEL</h2>
<h3> Description: Include ZFICOR0001_SEL</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICOR0001_SEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
selection-screen begin of block blk1 with frame.<br/>
select-options: s_bukrs for t001-bukrs obligatory,<br/>
s_prctr for cepc-prctr.<br/>
parameters: p_banben like zfit0002-zedition obligatory,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_year&nbsp;&nbsp;&nbsp;like&nbsp;zacdoct-ryear&nbsp;obligatory.<br/>
select-options: s_poper for zacdoca-poper obligatory.<br/>
select-options: s_fmisn1 for zfit0002-fmisn1.<br/>
select-options: s_gsber for  tgsb-gsber.<br/>
<br/>
parameters: g_locur radiobutton group gp1 default 'X', "本位币<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_docur&nbsp;radiobutton&nbsp;group&nbsp;gp1,&nbsp;&nbsp;"凭证货币<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_ckcfg&nbsp;radiobutton&nbsp;group&nbsp;gp1.&nbsp;&nbsp;"检查科目配置表<br/>
selection-screen end of block blk1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>