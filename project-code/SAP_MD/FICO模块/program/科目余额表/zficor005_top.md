<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICOR005_TOP</h2>
<h3> Description: Include ZFICOR0001_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICOR0001_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TABLES: zfit0002,t001,cepc,setnode,skb1,glidxa,zacdoct,zacdoca,tgsb.<br/>
TABLES: pa0002.<br/>
<br/>
DATA: g_msg      TYPE string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_fldbz(3)&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_poper&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;poper.<br/>
<br/>
DATA: i_confg TYPE TABLE OF zfit0002 WITH HEADER LINE.<br/>
<br/>
DATA: BEGIN OF i_selcfg OCCURS 0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;include&nbsp;structure&nbsp;<a&nbsp;href&nbsp;="zfit0002 dictionary-zfit0002.html"="">zfit0002.<br/>
DATA:   rtcur    LIKE zacdoct-rtcur,     "货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;noconfig,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_selcfg.<br/>
<br/>
DATA: BEGIN OF wa_fagsum,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;wa_fagsum.<br/>
<br/>
DATA: BEGIN OF i_base OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;&nbsp;LIKE&nbsp;zacdoct-rfarea,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-racct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rtcur,&nbsp;&nbsp;"货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag(1),<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_base.<br/>
<br/>
DATA: BEGIN OF i_base_noconfig OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;LIKE&nbsp;zacdoct-rfarea,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;LIKE&nbsp;zacdoct-rtcur,&nbsp;&nbsp;"货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;LIKE&nbsp;zacdoct-racct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_base_noconfig.<br/>
<br/>
DATA: i_skat TYPE TABLE OF skat WITH HEADER LINE.<br/>
<br/>
DATA: i_t012k TYPE TABLE OF t012k WITH HEADER LINE.<br/>
RANGES: lr_gsber FOR tgsb-gsber.<br/>
DATA: BEGIN OF i_data OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_nkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;level&nbsp;&nbsp;TYPE&nbsp;n,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmisn1&nbsp;TYPE&nbsp;char40,&nbsp;&nbsp;&nbsp;"上级ID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmist1&nbsp;TYPE&nbsp;char20,&nbsp;&nbsp;"上级文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmisn2&nbsp;TYPE&nbsp;char40,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmisn&nbsp;&nbsp;TYPE&nbsp;char4,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fmist&nbsp;&nbsp;TYPE&nbsp;char20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;TYPE&nbsp;fkber,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;TYPE&nbsp;hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt40&nbsp;&nbsp;TYPE&nbsp;char100,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;LIKE&nbsp;zacdoct-rtcur,&nbsp;&nbsp;"货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_data.<br/>
<br/>
TYPES: BEGIN OF ty_out,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_nkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;LIKE&nbsp;zacdoct-rtcur,&nbsp;&nbsp;"货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_out.<br/>
DATA: i_out  TYPE TABLE OF ty_out,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_out&nbsp;TYPE&nbsp;ty_out.<br/>
DATA:g_yhzhbz TYPE char1.<br/>
DATA: BEGIN OF i_out_jccx OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-racct,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"总分类帐帐目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skat-txt20,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"总帐科目名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rbukrs,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;butxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001-butxt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公司代码或公司的名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rcntr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rcntr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntr_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;cskt-ktext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prctr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-prctr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzprctr_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;cepct-ktext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktokd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;kna1-ktokd,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktokd_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t077x-txt30,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzkunnr_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;kna1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktokk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;lfa1-ktokk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktokk_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t077y-txt30,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzlifnr_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;lfa1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzaufnr_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;aufk-ktext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anln1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-anln1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzanln1_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-txt50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anln2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-anln2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzanln2_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-txt50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;anla-anlkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlkl_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;ankt-txk20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mwskz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t007s-text1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbund&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-vbund&nbsp;&nbsp;,&nbsp;"贸易伙伴<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbund_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t880-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rstgr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rstgr&nbsp;,&nbsp;"原因代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzrstgr_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;t053s-txt20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bewar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-bewar,&nbsp;"合并事务类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzbewar_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;t856t-txt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anbwa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-anbwa,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anbwa_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tabwt-bwatxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;projk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-projk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;prps-posid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;post1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;prps-post1,<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbusa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rbusa,&nbsp;&nbsp;&nbsp;"业务范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbusa_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tgsbt-gtext,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"业务范围名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;segment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-segment&nbsp;&nbsp;&nbsp;,&nbsp;"段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;segment_txt&nbsp;&nbsp;&nbsp;LIKE&nbsp;fagl_segmt-name,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"段名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"物料号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;makt-maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"物料描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-bwart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"移动类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwart_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t156ht-btext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"移动类型描述<br/>
<br/>
</a&nbsp;href&nbsp;="zfit0002></div>
<div class="codeComment">
*&nbsp;&nbsp;zzxf001&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-zzxf001&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"资金委托付款书编号<br/>
*&nbsp;&nbsp;zzxf002&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-zzxf002&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"项目合同编号<br/>
*&nbsp;&nbsp;zzxf003&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-zzxf003&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"项目供应商编号<br/>
*&nbsp;&nbsp;zzxf003_txt&nbsp;LIKE&nbsp;zacdoct-zzxf003&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"项目供应商名称<br/>
*&nbsp;&nbsp;zzxf004&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-zzxf004&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"销售业务类型<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"销售和分销凭证号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-posnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"销售和分销凭证号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"采购凭证号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-ebelp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"采购凭证号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001w-name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"工厂描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pernr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-pernr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,&nbsp;"人员编号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pernr_txt(40)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,"人员姓名<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bismt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;mara-bismt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"NC物料号<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;nckun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;kna1-nckun,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"NC客户编号&nbsp;&nbsp;"兴发专用<br/>
*&nbsp;&nbsp;sort2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;adrc-sort2,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"NC供应商编码&nbsp;&nbsp;"兴发专用<br/>
*&nbsp;&nbsp;cwlx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbap-cwlx,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"财务销售类型&nbsp;&nbsp;"兴发专用<br/>
*&nbsp;&nbsp;cwlx_txt(50),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"财务销售类型&nbsp;&nbsp;"兴发专用<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vtweg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbak-vtweg,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分销渠道<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vtext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tvtwt-vtext,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"分销渠道名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jdbj(2),&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"借贷标记<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;khyhmc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t012t-text1,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"开户银行名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hbkid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hbkid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hktid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hktid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslbnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsldqa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-hslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"本年期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslbnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期期初<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msldqa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mslvt3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-mslvt,&nbsp;&nbsp;"当期余额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;runit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-runit,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoct-rtcur,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"币别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netpr1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;p&nbsp;LENGTH&nbsp;16&nbsp;DECIMALS&nbsp;2,&nbsp;"初始单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netpr2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;p&nbsp;LENGTH&nbsp;16&nbsp;DECIMALS&nbsp;2,&nbsp;"余额单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-xref3,&nbsp;"参考码3&nbsp;&nbsp;add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref3_TXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;auftext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ord41&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;anla-ord41,&nbsp;"使用状况&nbsp;Add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ord41_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;"使用状况描述&nbsp;add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yhzh_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char45,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_out_jccx.<br/>
<br/>
DATA: BEGIN OF i_jccx_keys OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fname(30),&nbsp;&nbsp;"字段名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ckind(1),&nbsp;&nbsp;&nbsp;"字段输出已选择<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_jccx_keys.<br/>
<br/>
<br/>
DATA: BEGIN OF i_out_pzcx OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;poper&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-poper,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-belnr,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buzei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-buzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-dmbtr,&nbsp;"借方<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-shkzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rebzt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-rebzt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-docln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;blart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-blart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-rtcur,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;runit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-runit,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sgtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-sgtxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zuonr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-zuonr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbusa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-rbusa,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-xblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bktxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-bktxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ppnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-ppnam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;usnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-usnam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cpudt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-cpudt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brnch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bkpf-brnch,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;j_1bbranch-name,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-shkzg,&nbsp;&nbsp;"借贷标识的中文描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ppnam_d&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"预制人名字信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;usnam_d&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"审核人名字信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;butxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001-butxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr_1&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-dmbtr,&nbsp;&nbsp;&nbsp;"借方<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr_2&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-dmbtr,&nbsp;&nbsp;"贷方<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msl_1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-menge,&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msl_2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bseg-menge,&nbsp;&nbsp;"贷<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr_y&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zacdoca-hsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fldname&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_scol,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;awref&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoca-awref,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;awitem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoca-awitem,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont_txt&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xreversal&nbsp;&nbsp;LIKE&nbsp;bkpf-xreversal,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xreversing&nbsp;LIKE&nbsp;bkpf-xreversing,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xreversed&nbsp;&nbsp;LIKE&nbsp;bkpf-xreversed,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;i_out_pzcx.<br/>
<br/>
"转换特殊功能分类帐<br/>
TYPES: BEGIN OF typ_glidxa,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;LIKE&nbsp;glidxa-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docnr&nbsp;LIKE&nbsp;glidxa-docnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;LIKE&nbsp;glidxa-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_glidxa.<br/>
DATA: tab_glidxa TYPE TABLE OF typ_glidxa,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_glidxa&nbsp;&nbsp;TYPE&nbsp;typ_glidxa.<br/>
<br/>
DATA: i_keyall  TYPE lvc_t_nkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sel_chd&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;lvc_nkey&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
RANGES: r_selnodes FOR i_data-key.<br/>
<br/>
DATA: g_alv_tree           TYPE REF TO cl_gui_alv_tree,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_customer_container&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_custom_container.<br/>
<br/>
<br/>
DATA: c_bukrs    TYPE c,c_fmis TYPE c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_cntr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,c_zzprctr&nbsp;TYPE&nbsp;c,c_zzkunnr&nbsp;TYPE&nbsp;c,c_zzlifnr&nbsp;TYPE&nbsp;c,&nbsp;c_zzaufnr&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_zzcfck&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,c_zzccpck&nbsp;TYPE&nbsp;c,c_zzfkck&nbsp;TYPE&nbsp;c,c_zzkwfzck&nbsp;TYPE&nbsp;c,c_zzylck&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_zzanln1&nbsp;&nbsp;TYPE&nbsp;c,c_zzanln2&nbsp;TYPE&nbsp;c,c_mwskz&nbsp;TYPE&nbsp;c,c_vbund&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_zzrstgr&nbsp;&nbsp;TYPE&nbsp;c,c_zzxref1&nbsp;TYPE&nbsp;c,c_zzxref2&nbsp;TYPE&nbsp;c,c_zzxref3&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_zzbewar&nbsp;&nbsp;TYPE&nbsp;c,c_anbwa&nbsp;TYPE&nbsp;c.<br/>
<br/>
DATA: it_field TYPE slis_t_fieldcat_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field&nbsp;TYPE&nbsp;slis_fieldcat_alv.<br/>
DATA: g_repid TYPE sy-repid.<br/>
DATA: gs_layout TYPE slis_layout_alv.<br/>
<br/>
</div>
<div class="codeComment">
*某些全局变量<br/>
</div>
<div class="code">
DATA: col    TYPE lvc_s_scol,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;coltab&nbsp;TYPE&nbsp;lvc_t_scol,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;color&nbsp;&nbsp;TYPE&nbsp;lvc_s_colo.<br/>
color-col = '6'.<br/>
color-int = '1'.<br/>
color-inv = '0'.<br/>
<br/>
TYPES: BEGIN OF valut_typ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;TYPE&nbsp;char50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;valut_typ.<br/>
DATA: lt_value TYPE TABLE OF valut_typ.<br/>
DATA: wa_value TYPE valut_typ.<br/>
DATA: lt_field_tab TYPE TABLE OF dfies.<br/>
DATA: wa_field_tab TYPE dfies.<br/>
DATA: lt_return_tab TYPE TABLE OF ddshretval.<br/>
DATA: wa_return_tab TYPE ddshretval.<br/>
DATA: lt_return LIKE ddshretval OCCURS 0 WITH HEADER LINE.<br/>
<br/>
DATA: g_rootkey TYPE lvc_nkey.<br/>
<br/>
DATA: g_jccx_bukrs(1).<br/>
DATA: g_jccx_hkont(1).<br/>
DATA: g_jccx_khyhmc(1).<br/>
DATA:g_jccx_empty(1).<br/>
DATA: g_single_bukrs TYPE t001-bukrs. "单选时公司代码<br/>
<br/>
RANGES: r_fmis FOR zfit0002-fmisn1, "zfit0001--&gt;zfit0002<br/>
r_bukrs FOR zfit0002-bukrs,<br/>
r_gsber FOR zacdoct-rbusa,<br/>
r_cntr FOR zacdoct-rcntr,<br/>
r_zzprctr FOR zacdoct-prctr,<br/>
r_zzkunnr FOR zacdoct-kunnr,<br/>
r_zzlifnr FOR zacdoct-lifnr,<br/>
r_zzaufnr FOR zacdoct-aufnr,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_zzcfck&nbsp;&nbsp;FOR&nbsp;zacdoct-rzzcfck,&nbsp;&nbsp;"财辅参考<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_zzccpck&nbsp;FOR&nbsp;zacdoct-rzzccpck,&nbsp;&nbsp;"销售产品<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_zzfkck&nbsp;&nbsp;FOR&nbsp;zacdoct-rzzfkck,&nbsp;"付款参考<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_zzkwfzck&nbsp;FOR&nbsp;zacdoct-rzzkwfzck,&nbsp;"库位/责任区域<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_zzylck&nbsp;&nbsp;FOR&nbsp;zacdoct-rzzylck,&nbsp;&nbsp;"原料参考<br/>
</div>
<div class="code">
r_zzanln1 FOR zacdoct-anln1,<br/>
r_zzanln2 FOR zacdoct-anln2,<br/>
r_mwskz FOR zacdoct-mwskz,<br/>
r_vbund FOR zacdoct-vbund,<br/>
r_zzrstgr FOR zacdoct-rstgr,<br/>
r_zzxref1 FOR zacdoct-lifnr,<br/>
r_zzxref2 FOR zacdoct-kunnr,<br/>
r_zzxref3 FOR zacdoct-aufnr,<br/>
r_zzbewar FOR zacdoct-bewar,<br/>
r_anbwa FOR zacdoct-anbwa.<br/>
<br/>
<br/>
DEFINE add_field.<br/>
&nbsp;&nbsp;CLEAR&nbsp;wa_field.<br/>
&nbsp;&nbsp;wa_field-fieldname&nbsp;=&nbsp;&amp;1.<br/>
&nbsp;&nbsp;wa_field-reptext_ddic&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;wa_field-just&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;wa_field-do_sum&nbsp;&nbsp;=&nbsp;&amp;4.<br/>
&nbsp;&nbsp;wa_field-no_out&nbsp;&nbsp;=&nbsp;&amp;5.<br/>
&nbsp;&nbsp;wa_field-checkbox&nbsp;=&nbsp;&amp;6.<br/>
&nbsp;&nbsp;wa_field-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"可编辑<br/>
&nbsp;&nbsp;IF&nbsp;&amp;1&nbsp;=&nbsp;'lifnr'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-edit_mask&nbsp;=&nbsp;'==ALPHA'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'LFA1'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'LIFNR'.<br/>
</div>
<div class="code">
ELSEIF &amp;1 = 'kunnr'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-edit_mask&nbsp;=&nbsp;'==ALPHA'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'KNA1'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'KUNNR'.<br/>
</div>
<div class="code">
ELSEIF &amp;1 = 'BELNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'BKPF'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'BELNR'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;&amp;1&nbsp;EQ&nbsp;'matnr'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-edit_mask&nbsp;=&nbsp;'==MATN1'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;IF&nbsp;g_docur&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;IF&nbsp;&amp;1&nbsp;=&nbsp;'DMBTR_1'&nbsp;&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'DMBTR_2'&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'DMBTR_Y'&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLVT1'&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLBNS'&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLBNH'OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLVT2'OR&nbsp;&amp;1&nbsp;=&nbsp;'DMBTR_Y'OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLDQS'OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLDQH'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLDQA'&nbsp;OR&nbsp;&amp;1&nbsp;=&nbsp;'HSLVT3'.<br/>
wa_field-cfieldname = 'RTCUR'.<br/>
wa_field-ctabname = '1'.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;&amp;1&nbsp;=&nbsp;'BUTXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'T001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'BUTXT'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'TXT20'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'SKAT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'TXT20'.<br/>
&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ZZPRCTR_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'CEPCT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'KTEXT'.<br/>
&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ZZKUNNR_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'KNA1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'NAME1'.<br/>
&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ZZLIFNR_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'LFA1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'NAME1'.<br/>
&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'MATNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'MARA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'MATNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'MAKTX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'MAKT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'MAKTX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ZZRSTGR_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'T053S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'TXT20'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ZZAUFNR_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'AUFK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'KTEXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&amp;1&nbsp;=&nbsp;'ANLKL_TXT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_tabname&nbsp;=&nbsp;'ANKT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;wa_field-ref_fieldname&nbsp;=&nbsp;'TXK20'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;wa_field&nbsp;TO&nbsp;it_field.<br/>
END-OF-DEFINITION.<br/>
<br/>
DEFINE m_add_fldcat.<br/>
&nbsp;&nbsp;CLEAR&nbsp;i_fieldcat.<br/>
&nbsp;&nbsp;l_pos&nbsp;=&nbsp;l_pos&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;i_fieldcat-col_pos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;l_pos.<br/>
&nbsp;&nbsp;i_fieldcat-fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;1.<br/>
&nbsp;&nbsp;i_fieldcat-outputlen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;2.<br/>
&nbsp;&nbsp;i_fieldcat-coltext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;3.<br/>
&nbsp;&nbsp;i_fieldcat-no_zero&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;4.<br/>
&nbsp;&nbsp;i_fieldcat-no_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;5.<br/>
&nbsp;&nbsp;i_fieldcat-inttype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'F'.<br/>
&nbsp;&nbsp;APPEND&nbsp;i_fieldcat.<br/>
END-OF-DEFINITION.<br/>
DEFINE m_add_fldcat1.<br/>
&nbsp;&nbsp;CLEAR&nbsp;i_fieldcat.<br/>
&nbsp;&nbsp;l_pos&nbsp;=&nbsp;l_pos&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;i_fieldcat-col_pos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;l_pos.<br/>
&nbsp;&nbsp;i_fieldcat-fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;1.<br/>
&nbsp;&nbsp;i_fieldcat-outputlen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;2.<br/>
&nbsp;&nbsp;i_fieldcat-coltext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;3.<br/>
&nbsp;&nbsp;i_fieldcat-no_zero&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;4.<br/>
&nbsp;&nbsp;i_fieldcat-no_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;&amp;5.<br/>
&nbsp;&nbsp;i_fieldcat-inttype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'F'.<br/>
<br/>
&nbsp;IF&nbsp;g_docur&nbsp;EQ&nbsp;'X'.<br/>
i_fieldcat-cfieldname = 'RTCUR'.<br/>
i_fieldcat-quantity = 1.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;i_fieldcat.<br/>
END-OF-DEFINITION.<br/>
<br/>
DEFINE m_set_range.<br/>
&nbsp;&nbsp;&amp;1-sign&nbsp;&nbsp;&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&amp;1-low&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;IF&nbsp;&amp;3&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&amp;1-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&amp;1-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&amp;1-high&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;&amp;1.<br/>
&nbsp;&nbsp;CLEAR&nbsp;&amp;1.<br/>
END-OF-DEFINITION.<br/>
<br/>
<br/>
DEFINE m_set_range_1.<br/>
&nbsp;&nbsp;&amp;1-sign&nbsp;&nbsp;&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&amp;1-low(4)&nbsp;=&nbsp;&amp;2(4).<br/>
&nbsp;&nbsp;&amp;1-low+4&nbsp;&nbsp;=&nbsp;'*'.<br/>
&nbsp;&nbsp;&amp;1-option&nbsp;=&nbsp;'CP'.<br/>
&nbsp;&nbsp;COLLECT&nbsp;&amp;1.<br/>
&nbsp;&nbsp;CLEAR&nbsp;&amp;1.<br/>
END-OF-DEFINITION.<br/>
<br/>
DATA: fmisn(4).<br/>
<br/>
DATA:gv_hkont TYPE bseg-hkont.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>