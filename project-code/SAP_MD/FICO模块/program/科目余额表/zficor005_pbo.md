<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICOR005_PBO</h2>
<h3> Description: Include ZFICOR0001_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICOR0001_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_0100 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STANDARD2'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;INIT_TREE&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE init_tree OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;g_tree_header&nbsp;TYPE&nbsp;treev_hhdr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_node_txt&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_value,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_fcat&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_pos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DATA:&nbsp;wa_data&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;i_data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_upkey&nbsp;TYPE&nbsp;lvc_nkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_key&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_nkey.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_layout&nbsp;TYPE&nbsp;lvc_s_layn.<br/>
<br/>
&nbsp;&nbsp;CHECK&nbsp;g_alv_tree&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;g_alv_tree<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;parent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_container=&gt;default_screen<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node_selection_mode&nbsp;=&nbsp;cl_gui_column_tree=&gt;node_sel_mode_multiple<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;item_selection&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_toolbar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_html_header&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;g_tree_header-heading&nbsp;=&nbsp;'项目编码/描述'.<br/>
&nbsp;&nbsp;g_tree_header-tooltip&nbsp;=&nbsp;'项目编码/描述'.<br/>
&nbsp;&nbsp;g_tree_header-width&nbsp;&nbsp;&nbsp;=&nbsp;54.<br/>
<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLVT1'&nbsp;24&nbsp;'本年期初数量'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLVT1'&nbsp;24&nbsp;'本年期初'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLBNS'&nbsp;24&nbsp;'本年借方发生数量'&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLBNS'&nbsp;24&nbsp;'本年借方发生金额'&nbsp;&nbsp;'X'&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLBNH'&nbsp;24&nbsp;'本年贷方发生数量'&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLBNH'&nbsp;24&nbsp;'本年贷方发生金额'&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLVT2'&nbsp;24&nbsp;'当期期初数量'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLVT2'&nbsp;24&nbsp;'当期期初金额'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLDQS'&nbsp;24&nbsp;'当期借方发生数量'&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLDQS'&nbsp;24&nbsp;'当期借方发生金额'&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLDQH'&nbsp;24&nbsp;'当期贷方发生数量'&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLDQH'&nbsp;24&nbsp;'当期贷方发生金额'&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLDQA'&nbsp;24&nbsp;'当期累计数量'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLDQA'&nbsp;24&nbsp;'当期累计金额'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;m_add_fldcat&nbsp;'MSLVT3'&nbsp;24&nbsp;'期末余数量'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;'X'.<br/>
&nbsp;&nbsp;m_add_fldcat1&nbsp;'HSLVT3'&nbsp;24&nbsp;'期末余金额'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'X'&nbsp;''.<br/>
&nbsp;&nbsp;IF&nbsp;g_docur&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;m_add_fldcat&nbsp;'RTCUR'&nbsp;&nbsp;10&nbsp;&nbsp;'货币'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;set_table_for_first_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_hierarchy_header&nbsp;=&nbsp;g_tree_header<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SAVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcatalog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_fieldcat[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_out.<br/>
<br/>
</div>
<div class="codeComment">
*&gt;root&nbsp;node<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;l_ktopl&nbsp;TYPE&nbsp;t001-ktopl.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;ktopl&nbsp;INTO&nbsp;l_ktopl&nbsp;FROM&nbsp;t001&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS&nbsp;WHERE&nbsp;bukrs&nbsp;IN&nbsp;r_bukrs.&nbsp;ENDSELECT.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ktplt&nbsp;INTO&nbsp;l_node_txt&nbsp;FROM&nbsp;t004t&nbsp;WHERE&nbsp;spras&nbsp;EQ&nbsp;sy-langu&nbsp;AND&nbsp;ktopl&nbsp;=&nbsp;l_ktopl.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;wa_out.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;add_node<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relat_node_key&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relationship&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_column_tree=&gt;relat_last_child<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_outtab_line&nbsp;&nbsp;&nbsp;=&nbsp;wa_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_node_text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_node_txt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_new_node_key&nbsp;&nbsp;&nbsp;=&nbsp;g_rootkey.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;i_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_upkey.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;i_data&nbsp;TO&nbsp;wa_out.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_node_txt&nbsp;=&nbsp;i_data-txt40.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;i_data-rfarea&nbsp;l_node_txt&nbsp;into&nbsp;l_node_txt&nbsp;SEPARATED&nbsp;BY&nbsp;'/'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;i_data-fmisn2&nbsp;l_node_txt&nbsp;into&nbsp;l_node_txt&nbsp;SEPARATED&nbsp;BY&nbsp;'/'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;l_node_txt&nbsp;=&nbsp;i_data-fmisn2.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_data-fmisn1&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;i_data&nbsp;INTO&nbsp;wa_data&nbsp;WITH&nbsp;KEY&nbsp;rfarea&nbsp;=&nbsp;i_data-rfarea&nbsp;fmisn2&nbsp;=&nbsp;i_data-fmisn1&nbsp;fmist&nbsp;=&nbsp;i_data-fmist1&nbsp;rtcur&nbsp;=&nbsp;i_data-rtcur.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;i_data&nbsp;INTO&nbsp;wa_data&nbsp;WITH&nbsp;KEY&nbsp;rfarea&nbsp;=&nbsp;'*'&nbsp;fmisn2&nbsp;=&nbsp;i_data-fmisn1&nbsp;fmist&nbsp;=&nbsp;i_data-fmist1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_upkey&nbsp;=&nbsp;wa_data-key.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_upkey&nbsp;IS&nbsp;INITIAL&nbsp;AND&nbsp;i_data-level&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_upkey&nbsp;=&nbsp;g_rootkey.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_data-fmist&nbsp;=&nbsp;'未配置的科目'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_layout-isfolder&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_layout-n_image&nbsp;=&nbsp;'@5C@'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_layout-exp_image&nbsp;=&nbsp;'@5C@'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;add_node<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relat_node_key&nbsp;=&nbsp;l_upkey<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relationship&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_column_tree=&gt;relat_last_child<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_outtab_line&nbsp;&nbsp;&nbsp;=&nbsp;wa_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_node_text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_node_txt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_node_layout&nbsp;&nbsp;&nbsp;=&nbsp;l_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_new_node_key&nbsp;&nbsp;&nbsp;=&nbsp;l_key.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;add_node<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relat_node_key&nbsp;=&nbsp;l_upkey<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_relationship&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_column_tree=&gt;relat_last_child<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_outtab_line&nbsp;&nbsp;&nbsp;=&nbsp;wa_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_node_text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_node_txt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_new_node_key&nbsp;&nbsp;&nbsp;=&nbsp;l_key.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;expand_node<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_node_key&nbsp;=&nbsp;g_rootkey.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;g_alv_tree-&gt;frontend_update.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;MODIFY_SCREEN&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE modify_screen OUTPUT.<br/>
&nbsp;&nbsp;IF&nbsp;sy-dynnr&nbsp;EQ&nbsp;'9200'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_modify_screen_new.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_modify_screen.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_9201&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_9201 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'JCCX1'.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;&nbsp;'JCCX'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_MODIFY_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_modify_screen .<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_MODIFY_SCREEN_NEW<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_modify_screen_new .<br/>
&nbsp;&nbsp;DATA:&nbsp;l_flag_1&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_fldname(30).<br/>
&nbsp;&nbsp;DATA:&nbsp;l_tabname(30).<br/>
&nbsp;&nbsp;DATA:&nbsp;l_len_tabname&nbsp;TYPE&nbsp;i.<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS:&nbsp;&lt;fs&gt;.<br/>
<br/>
&nbsp;&nbsp;REFRESH&nbsp;i_jccx_keys.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;EQ&nbsp;'KEY'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SPLIT&nbsp;screen-name&nbsp;AT&nbsp;'-'&nbsp;INTO&nbsp;l_tabname&nbsp;l_fldname&nbsp;IN&nbsp;CHARACTER&nbsp;MODE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;i_selcfg&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;l_fldname&nbsp;OF&nbsp;STRUCTURE&nbsp;i_selcfg&nbsp;TO&nbsp;&lt;fs&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs&gt;&nbsp;EQ&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-input&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_jccx_keys-fname&nbsp;=&nbsp;l_fldname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;i_jccx_keys.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>