<table class="outerTable">
<tr>
<td><h2>Table: ZFICOS004</h2>
<h3>Description: 会计凭证创建返回结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BELNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>BELNR_D</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>会计凭证号码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>GJAHR</td>
<td>4</td>
<td>&nbsp;</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MSGTXT</td>
<td>2</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MSGTYP</td>
<td>1</td>
<td>&nbsp;</td>
<td>MSGTY</td>
<td>MSGAR</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZRSV01</td>
<td>5</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZRSV02</td>
<td>6</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZRSV03</td>
<td>7</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZRSV04</td>
<td>8</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZRSV05</td>
<td>9</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>MSGAR</td>
<td>S</td>
<td>&nbsp;</td>
<td>在下一屏幕的消息</td>
</tr>
<tr class="cell">
<td>MSGAR</td>
<td>I</td>
<td>&nbsp;</td>
<td>信息</td>
</tr>
<tr class="cell">
<td>MSGAR</td>
<td>A</td>
<td>&nbsp;</td>
<td>取消</td>
</tr>
<tr class="cell">
<td>MSGAR</td>
<td>E</td>
<td>&nbsp;</td>
<td>错误</td>
</tr>
<tr class="cell">
<td>MSGAR</td>
<td>W</td>
<td>&nbsp;</td>
<td>警告</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>