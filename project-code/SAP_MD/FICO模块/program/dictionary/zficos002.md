<table class="outerTable">
<tr>
<td><h2>Table: ZFICOS002</h2>
<h3>Description: 会计凭证创建接口导入参数</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>COMP_CODE</td>
<td>2</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>CURRENCY</td>
<td>7</td>
<td>&nbsp;</td>
<td>WAERS</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>货币码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>DOC_DATE</td>
<td>4</td>
<td>&nbsp;</td>
<td>BLDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的凭证日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>DOC_STATUS</td>
<td>14</td>
<td>&nbsp;</td>
<td>BAPI_ACC_DOC_STATUS</td>
<td>ACC_STATUS</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>凭证状态</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>DOC_TYPE</td>
<td>3</td>
<td>&nbsp;</td>
<td>BLART</td>
<td>BLART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>凭证类型</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>FIS_PERIOD</td>
<td>6</td>
<td>&nbsp;</td>
<td>MONAT</td>
<td>MONAT</td>
<td>NUMC</td>
<td>2</td>
<td>&nbsp;</td>
<td>会计期间</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>HEADER_TXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>BKTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>凭证抬头文本</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ITEMS</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZTTFICO001</td>
<td>&nbsp;</td>
<td>TTYP</td>
<td>0</td>
<td>&nbsp;</td>
<td>会计凭证过账行项目表</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>NUMPG</td>
<td>13</td>
<td>&nbsp;</td>
<td>J_1ANOPG</td>
<td>J_1ANOPG</td>
<td>NUMC</td>
<td>3</td>
<td>&nbsp;</td>
<td>发票的页数</td>
</tr>
<tr class="cell">
<td>10</td>
<td>PSTNG_DATE</td>
<td>5</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td>11</td>
<td>REF_DOC_NO</td>
<td>10</td>
<td>&nbsp;</td>
<td>XBLNR</td>
<td>XBLNR</td>
<td>CHAR</td>
<td>16</td>
<td>&nbsp;</td>
<td>参考凭证编号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>USERNAME</td>
<td>9</td>
<td>&nbsp;</td>
<td>USNAM</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>用户名</td>
</tr>
<tr class="cell">
<td>13</td>
<td>XREF1_HD</td>
<td>11</td>
<td>&nbsp;</td>
<td>XREF1_HD</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>凭证抬头的内部参考键值 1</td>
</tr>
<tr class="cell">
<td>14</td>
<td>XREF2_HD</td>
<td>12</td>
<td>&nbsp;</td>
<td>XREF2_HD</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>凭证抬头的内部参考键值 2</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZID</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>GUID标识</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ACC_STATUS</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>未使用的概念</td>
</tr>
<tr class="cell">
<td>ACC_STATUS</td>
<td>1</td>
<td>&nbsp;</td>
<td>已删除</td>
</tr>
<tr class="cell">
<td>ACC_STATUS</td>
<td>2</td>
<td>&nbsp;</td>
<td>未测试的</td>
</tr>
<tr class="cell">
<td>ACC_STATUS</td>
<td>3</td>
<td>&nbsp;</td>
<td>已检查的和完成的</td>
</tr>
<tr class="cell">
<td>ACC_STATUS</td>
<td>4</td>
<td>&nbsp;</td>
<td>已过账</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>