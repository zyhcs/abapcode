<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO016</h2>
<h3>Description: 接口FICO011日志表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>.INCLUDE</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZSCOMMON</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>0</td>
<td>&nbsp;</td>
<td>自定义表统一附加结构</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>5</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>GJAHR</td>
<td>3</td>
<td>&nbsp;</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>HZID</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZEHZID</td>
<td>ZDHZID</td>
<td>NUMC</td>
<td>20</td>
<td>&nbsp;</td>
<td>会计凭证汇总ID</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MESSAGE</td>
<td>9</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MONAT</td>
<td>4</td>
<td>&nbsp;</td>
<td>MONAT</td>
<td>MONAT</td>
<td>NUMC</td>
<td>2</td>
<td>&nbsp;</td>
<td>会计期间</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MTYPE</td>
<td>8</td>
<td>&nbsp;</td>
<td>BAPI_MTYPE</td>
<td>SYCHAR01</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型: S 成功,E 错误,W 警告,I 信息,A 中断</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>UUID</td>
<td>2</td>
<td>X</td>
<td>SYSUUID_C32</td>
<td>SYSUUID_C32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 32 Characters (Hexadecimal Encoded)</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZKBELNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZEKBELNR</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>金蝶凭证</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZZ_CRT_DAT</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZE_CRT_DAT</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZZ_CRT_TIME</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZE_CRT_TIME</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>创建时间</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZZ_CRT_USR</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZE_CRT_USR</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>创建者</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZZ_DEL</td>
<td>17</td>
<td>&nbsp;</td>
<td>ZE_DEL</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>删除标记</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZZ_SEL</td>
<td>18</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>选择标识</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZ_UPD_DAT</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZE_UPD_DAT</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>变更日期</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZZ_UPD_TIME</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZE_UPD_TIME</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>变更时间</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZZ_UPD_USR</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZE_UPD_USR</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>变更者</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>