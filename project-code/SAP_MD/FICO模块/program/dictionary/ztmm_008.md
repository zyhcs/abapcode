<table class="outerTable">
<tr>
<td><h2>Table: ZTMM_008</h2>
<h3>Description: 科目对应表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BSART</td>
<td>3</td>
<td>X</td>
<td>BSART</td>
<td>BSART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>订单类型（采购）</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>LGACT</td>
<td>5</td>
<td>X</td>
<td>ZE_LGACT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否科目分配</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>LGMAT</td>
<td>4</td>
<td>X</td>
<td>ZE_LGMAT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否有物料</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>SAKNR</td>
<td>2</td>
<td>X</td>
<td>SAKNR</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目编号</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>