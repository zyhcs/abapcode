<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFICO_GET_MTYPE</h2>
<h3> Description: 获取物料分类</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZFICO_GET_MTYPE.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(G_OBJEK) TYPE  CUOBN<br/>
*"  EXPORTING<br/>
*"     VALUE(G_ZWLFL) TYPE  ZWLFL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
*SELECT * INTO CORRESPONDING FIELDS OF TABLE gt_cabn<br/>
*  FROM cabn WHERE ( ATNAM = 'ZYYYHG' or ATNAM = 'ZBZGG' or ATNAM = 'ZSYYGYS' or ATNAM = 'ZCLTJJ' ).<br/>
*<br/>
*if gt_cabn is NOT INITIAL.<br/>
*  SELECT * INTO CORRESPONDING FIELDS OF TABLE gt_ausp<br/>
*    FROM ausp<br/>
*    FOR ALL ENTRIES IN gt_cabn<br/>
*    WHERE OBJEK = G_OBJEK<br/>
*    AND ATINN = gt_cabn-ATINN.<br/>
* ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zfico_get_mtype.html">Global data declarations</a></div><br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>