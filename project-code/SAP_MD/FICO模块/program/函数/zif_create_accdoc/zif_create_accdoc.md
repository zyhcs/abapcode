<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_CREATE_ACCDOC</h2>
<h3> Description: 创建会计凭证</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_create_accdoc.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(I_DATA) TYPE  ZFICOS002<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_RETURN) TYPE  ZFICOS004<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_create_accdoc.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR:&nbsp;gv_date,gv_time&nbsp;.<br/>
&nbsp;&nbsp;gv_date&nbsp;=&nbsp;sy-datum.&nbsp;"消息进入日期<br/>
&nbsp;&nbsp;gv_time&nbsp;=&nbsp;sy-uzeit.&nbsp;"消息进入时间<br/>
<br/>
</div>
<div class="codeComment">
*     控制信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;i_data&nbsp;TO&nbsp;gs_contrl.<br/>
<br/>
<br/>
&nbsp;&nbsp;"数据检查<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_data&nbsp;&nbsp;&nbsp;USING&nbsp;i_data&nbsp;CHANGING&nbsp;e_return.<br/>
<br/>
&nbsp;&nbsp;CHECK&nbsp;e_return&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
</div>
<div class="codeComment">
* 过账处理<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_acc_document&nbsp;&nbsp;USING&nbsp;i_data&nbsp;CHANGING&nbsp;e_return&nbsp;.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>