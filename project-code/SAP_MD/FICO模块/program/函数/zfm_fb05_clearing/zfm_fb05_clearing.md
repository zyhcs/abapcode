<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_FB05_CLEARING</h2>
<h3> Description: 会计凭证清账函数</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZFM_FB05_CLEARING.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IV_BUKRS) TYPE  BUKRS<br/>
*"     REFERENCE(IV_HKONT) TYPE  HKONT<br/>
*"     REFERENCE(IV_KOART) TYPE  KOART OPTIONAL<br/>
*"     REFERENCE(IV_AGUMS) TYPE  AGUMS OPTIONAL<br/>
*"     REFERENCE(IV_MODE) TYPE  RFPDO-ALLGAZMD DEFAULT 'A'<br/>
*"     REFERENCE(IV_DATUM) TYPE  SY-DATUM OPTIONAL<br/>
*"  EXPORTING<br/>
*"     REFERENCE(ES_RETURN) TYPE  MASSMSG<br/>
*"     REFERENCE(EV_SUBRC) TYPE  SY-SUBRC<br/>
*"  TABLES<br/>
*"      IT_BELNR STRUCTURE  ZST_BELNR<br/>
*"----------------------------------------------------------------------<br/>
<br/>
* Global data declarations<br/>
<div class="codeComment">*       <a href="global-zfm_fb05_clearing.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;L_AUGLV&nbsp;&nbsp;&nbsp;TYPE&nbsp;T041A-AUGLV&nbsp;VALUE&nbsp;'UMBUCHNG',&nbsp;"Posting&nbsp;with&nbsp;Clearing<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"You&nbsp;get&nbsp;an&nbsp;error&nbsp;with&nbsp;any&nbsp;other&nbsp;value<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L_TCODE&nbsp;&nbsp;&nbsp;TYPE&nbsp;SY-TCODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE&nbsp;'FB05',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L_SGFUNCT&nbsp;TYPE&nbsp;RFIPI-SGFUNCT&nbsp;VALUE&nbsp;'C'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Post&nbsp;immediately<br/>
&nbsp;&nbsp;"&nbsp;&nbsp;DATA:&nbsp;LV_MODE&nbsp;TYPE&nbsp;CTU_MODE&nbsp;VALUE&nbsp;'N'.<br/>
&nbsp;&nbsp;DATA:&nbsp;LT_BLNTAB&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;BLNTAB&nbsp;&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;FTCLEAR&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_FTPOST&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;FTPOST&nbsp;&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LT_FTTAX&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;FTTAX&nbsp;&nbsp;&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'POSTING_INTERFACE_START'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CLIENT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;SY-MANDT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_FUNCTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C'<br/>
</div>
<div class="codeComment">
*     I_GROUP            = ''<br/>
*     I_HOLDDATE         = ''<br/>
*     I_KEEP             = ''<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_MODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;IV_MODE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_UPDATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'S'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_USER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;SY-UNAME<br/>
</div>
<div class="codeComment">
*     I_XBDCC            = ''<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLIENT_INCORRECT&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FUNCTION_INVALID&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GROUP_NAME_MISSING&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODE_INVALID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPDATE_INVALID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EV_SUBRC&nbsp;=&nbsp;SY-SUBRC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*Batch Input Values<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-STYPE&nbsp;=&nbsp;'K'."Header<br/>
&nbsp;&nbsp;LT_FTPOST-COUNT&nbsp;=&nbsp;1.&nbsp;&nbsp;"number&nbsp;of&nbsp;Dynpro<br/>
<br/>
</div>
<div class="codeComment">
* 凭证日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-BLDAT'.<br/>
&nbsp;&nbsp;IF&nbsp;IV_DATUM&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;IV_DATUM.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
<br/>
</div>
<div class="codeComment">
*<br/>
*LT_FTPOST-FNAM = 'RF05A-AGKON'."客户<br/>
*LT_FTPOST-FVAL = L_KUNNR.<br/>
*APPEND LT_FTPOST.<br/>
<br/>
* 凭证类型<br/>
*  if IV_KOART = 'K'.<br/>
*  LT_FTPOST-FNAM = 'BKPF-BLART'.<br/>
*  LT_FTPOST-FVAL = 'KS'.     "Same type as documents cleared via F-32 QZ<br/>
*  APPEND LT_FTPOST.<br/>
*  ELSE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-BLART'.<br/>
&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;'AB'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Same&nbsp;type&nbsp;as&nbsp;documents&nbsp;cleared&nbsp;via&nbsp;F-32&nbsp;QZ<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
</div>
<div class="codeComment">
*  ENDIF.<br/>
* 公司代码<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-BUKRS'.<br/>
&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;IV_BUKRS.<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
<br/>
</div>
<div class="codeComment">
* 过帐日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-BUDAT'.<br/>
&nbsp;&nbsp;IF&nbsp;IV_DATUM&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;SY-DATUM.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;IV_DATUM.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
<br/>
</div>
<div class="codeComment">
* 记帐期间<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-MONAT'.<br/>
&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;SY-DATUM+4(2).<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
<br/>
</div>
<div class="codeComment">
* 货币<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTPOST-FNAM&nbsp;=&nbsp;'BKPF-WAERS'.<br/>
&nbsp;&nbsp;LT_FTPOST-FVAL&nbsp;=&nbsp;'CNY'.<br/>
&nbsp;&nbsp;APPEND&nbsp;LT_FTPOST.<br/>
<br/>
</div>
<div class="codeComment">
*Documents to be cleared<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTCLEAR-AGKOA&nbsp;=&nbsp;IV_KOART.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Account&nbsp;Type&nbsp;客户<br/>
&nbsp;&nbsp;IF&nbsp;IV_KOART&nbsp;=&nbsp;'K'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-XNOPS&nbsp;=&nbsp;''.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Indicator:&nbsp;Select&nbsp;only&nbsp;open&nbsp;items<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-AGUMS&nbsp;=&nbsp;IV_AGUMS.&nbsp;&nbsp;&nbsp;&nbsp;"特殊总帐标识符<br/>
&nbsp;&nbsp;ELSEIF&nbsp;IV_KOART&nbsp;=&nbsp;'D'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"客户<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-XNOPS&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Indicator:&nbsp;Select&nbsp;only&nbsp;open<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-AGUMS&nbsp;=&nbsp;IV_AGUMS.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"特殊总帐标识符<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*which are not special G/L?<br/>
</div>
<div class="code">
&nbsp;&nbsp;LT_FTCLEAR-AGBUK&nbsp;=&nbsp;IV_BUKRS.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Example&nbsp;company&nbsp;code<br/>
&nbsp;&nbsp;LT_FTCLEAR-AGKON&nbsp;=&nbsp;IV_HKONT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Example&nbsp;Customer<br/>
<br/>
&nbsp;&nbsp;LT_FTCLEAR-SELFD&nbsp;=&nbsp;'BELNR'."Selection&nbsp;Field<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;IT_BELNR.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-SELVON&nbsp;=&nbsp;IT_BELNR-BELNR.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LT_FTCLEAR-SELBIS&nbsp;=&nbsp;IT_BELNR-BELNR.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;LT_FTCLEAR.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;L_MSGNO&nbsp;TYPE&nbsp;SYMSGNO.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'POSTING_INTERFACE_CLEARING'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_AUGLV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;L_AUGLV<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_TCODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;L_TCODE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SGFUNCT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;L_SGFUNCT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_NO_AUTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGNO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;L_MSGNO<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGTY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGTY<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGV1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGV1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGV2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGV2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGV3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGV3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_MSGV4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ES_RETURN-MSGV4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_SUBRC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;EV_SUBRC<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_BLNTAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_BLNTAB<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_FTCLEAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_FTCLEAR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_FTPOST&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_FTPOST<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_FTTAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;LT_FTTAX<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEARING_PROCEDURE_INVALID&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEARING_PROCEDURE_MISSING&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLE_T041A_EMPTY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRANSACTION_CODE_INVALID&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AMOUNT_FORMAT_ERROR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOO_MANY_LINE_ITEMS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPANY_CODE_INVALID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN_NOT_FOUND&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO_AUTHORIZATION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;9<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;10.<br/>
<br/>
&nbsp;&nbsp;ES_RETURN-MSGNO&nbsp;=&nbsp;L_MSGNO&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EV_SUBRC&nbsp;=&nbsp;SY-SUBRC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'POSTING_INTERFACE_END'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BDCIMMED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*       I_BDCSTRTDT             = NO_DATE<br/>
*       I_BDCSTRTTM             = NO_TIME<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SESSION_NOT_PROCESSABLE&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;EV_SUBRC&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SY-SUBRC&nbsp;=&nbsp;EV_SUBRC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'POSTING_INTERFACE_END'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BDCIMMED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*       I_BDCSTRTDT             = NO_DATE<br/>
*       I_BDCSTRTTM             = NO_TIME<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SESSION_NOT_PROCESSABLE&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'POSTING_INTERFACE_END'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BDCIMMED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*     I_BDCSTRTDT             = NO_DATE<br/>
*     I_BDCSTRTTM             = NO_TIME<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SESSION_NOT_PROCESSABLE&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EV_SUBRC&nbsp;=&nbsp;SY-SUBRC.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>