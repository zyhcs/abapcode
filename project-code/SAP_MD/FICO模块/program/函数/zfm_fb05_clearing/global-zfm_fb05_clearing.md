<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFICOG003TOP</h2>
<h3> Description: 会计凭证清账函数</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zficog003.                     "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZFICOG02D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
</div>
<div class="code">
DATA:BEGIN OF gs_contrl,"接口控制信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sender&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;receiver&nbsp;&nbsp;TYPE&nbsp;char10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intfid&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char15,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;timestamp&nbsp;TYPE&nbsp;dec15,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gs_contrl.<br/>
<br/>
TYPES:<br/>
&nbsp;&nbsp;tyt_accountgl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiacgl09,<br/>
&nbsp;&nbsp;tyt_accountreceivable&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiacar09,<br/>
&nbsp;&nbsp;tyt_accountpayable&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiacap09,<br/>
&nbsp;&nbsp;tyt_currencyamount&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiaccr09,<br/>
&nbsp;&nbsp;tyt_extension&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapiparex.<br/>
<br/>
DATA: gv_date TYPE sy-datum,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_time&nbsp;TYPE&nbsp;sy-uzeit.<br/>
<br/>
<br/>
DATA :gt_t012k   TYPE TABLE OF t012k.<br/>
DATA : gv_vbund TYPE vbund.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>