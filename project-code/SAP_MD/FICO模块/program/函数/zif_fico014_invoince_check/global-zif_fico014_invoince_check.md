<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFICOG008TOP</h2>
<h3> Description: 校验发票金额大于订单金额</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zficog008.                    "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZFICOG008D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
<br/>
</div>
<div class="code">
TYPES:BEGIN OF ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;TYPE&nbsp;ebeln,&nbsp;&nbsp;"采购单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zmbtr&nbsp;TYPE&nbsp;rseg-wrbtr,&nbsp;&nbsp;"维护的金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zyftpr&nbsp;TYPE&nbsp;ztmm009-zyftpr,&nbsp;"预付单金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrbtr&nbsp;&nbsp;TYPE&nbsp;rseg-wrbtr,&nbsp;&nbsp;"已预制或者过账的发票金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,&nbsp;&nbsp;"采购单金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zovtpr&nbsp;type&nbsp;ztmm009-zyftpr,&nbsp;&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_tab.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>