<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_FICO014_INVOINCE_CHECK</h2>
<h3> Description: 校验发票金额大于订单金额</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_fico014_invoince_check.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  TABLES<br/>
*"      IT_MRMRSEG TYPE  MRM_TAB_MRMRSEG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_fico014_invoince_check.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;it_mrmrseg[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_ebeln&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ebeln&nbsp;TYPE&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_ebeln_sum&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_rseg&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;it_mrmrseg.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_tab&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_zt_sum&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_sg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_sg_sum&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_po&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_po_sum&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_tab.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_tab&gt;&nbsp;TYPE&nbsp;ty_tab.<br/>
</div>
<div class="codeComment">
*  DATA lv_zyftpr TYPE ztmm009-zyftpr.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;"一个发票对应若干个采购单，去掉重复的采购单<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_mrmrseg&nbsp;INTO&nbsp;ls_rseg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_ebeln-ebeln&nbsp;=&nbsp;ls_rseg-ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_ebeln-wrbtr&nbsp;=&nbsp;ls_rseg-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_ebeln&nbsp;TO&nbsp;lt_ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;ls_rseg,ls_ebeln.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;lt_ebeln&nbsp;ASCENDING&nbsp;BY&nbsp;ebeln.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_ebeln&nbsp;into&nbsp;ls_ebeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_ebeln&nbsp;into&nbsp;lt_ebeln_sum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_ebeln.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"查询预付订单的金额<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;zyftpr&nbsp;AS&nbsp;zyftpr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_zt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ztmm009<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;lt_ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ebeln&nbsp;=&nbsp;lt_ebeln-ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;zdel&nbsp;&lt;&gt;&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;SORT&nbsp;lt_zt&nbsp;ASCENDING&nbsp;BY&nbsp;ebeln.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_zt&nbsp;INTO&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_tab&nbsp;INTO&nbsp;lt_zt_sum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"查询采购单对应发票的金额<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;rseg~ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;rseg~wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_sg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;rseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;rbkp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;rseg~belnr&nbsp;=&nbsp;rbkp~belnr&nbsp;AND&nbsp;rseg~gjahr&nbsp;=&nbsp;rbkp~gjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;lt_ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;rseg~ebeln&nbsp;=&nbsp;lt_ebeln-ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;rbkp~rbstat&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;SORT&nbsp;lt_sg&nbsp;ASCENDING&nbsp;BY&nbsp;ebeln.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_sg&nbsp;INTO&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_tab&nbsp;INTO&nbsp;lt_sg_sum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"查询订单的金额<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;netwr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_po<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;ekpo<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;lt_ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;ebeln&nbsp;=&nbsp;lt_ebeln-ebeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;loekz&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;SORT&nbsp;lt_po&nbsp;ASCENDING&nbsp;BY&nbsp;ebeln.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_po&nbsp;INTO&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;ls_tab&nbsp;INTO&nbsp;lt_po_sum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_ebeln_sum&nbsp;ASSIGNING&nbsp;&lt;fs_tab&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_zt_sum&nbsp;INTO&nbsp;ls_tab&nbsp;WITH&nbsp;KEY&nbsp;ebeln&nbsp;=&nbsp;&lt;fs_tab&gt;-ebeln&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-zyftpr&nbsp;=&nbsp;ls_tab-zyftpr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_sg_sum&nbsp;INTO&nbsp;ls_tab&nbsp;WITH&nbsp;KEY&nbsp;ebeln&nbsp;=&nbsp;&lt;fs_tab&gt;-ebeln&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-wrbtr&nbsp;=&nbsp;ls_tab-wrbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_po_sum&nbsp;INTO&nbsp;ls_tab&nbsp;WITH&nbsp;KEY&nbsp;ebeln&nbsp;=&nbsp;&lt;fs_tab&gt;-ebeln&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-netwr&nbsp;=&nbsp;ls_tab-netwr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_tab&gt;-zovtpr&nbsp;=&nbsp;&lt;fs_tab&gt;-zyftpr&nbsp;+&nbsp;&lt;fs_tab&gt;-wrbtr&nbsp;+&nbsp;&lt;fs_tab&gt;-zmbtr&nbsp;-&nbsp;&lt;fs_tab&gt;-netwr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_tab&gt;-zovtpr&nbsp;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'订单发票金额大于订单金额，超过：'&nbsp;&amp;&amp;&nbsp;&lt;fs_tab&gt;-zovtpr&nbsp;type&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: Hard coded<br/>
*   订单发票金额大于订单金额，超过：<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>