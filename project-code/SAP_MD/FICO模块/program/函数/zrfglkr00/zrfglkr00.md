<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZRFGLKR00</h2>
<h3> Description: Trial Balance (South Korea)</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
**********************************************************************<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trial&nbsp;Balance&nbsp;Sheet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**********************************<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**********************************<br/>
*&nbsp;&nbsp;created&nbsp;by&nbsp;Markus&nbsp;Hermann&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**********************************<br/>
*&nbsp;&nbsp;Walldorf&nbsp;-&nbsp;March&nbsp;1998&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**********************************<br/>
*&nbsp;&nbsp;05.01.2004&nbsp;Currency&nbsp;format&nbsp;changes&nbsp;for&nbsp;foreign&nbsp;currency&nbsp;GL&nbsp;accounts<br/>
**********************************************************************<br/>
<br/>
</div>
<div class="code">
REPORT RFGLKR00 MESSAGE-ID ICC-KR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LINE-SIZE&nbsp;170<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LINE-COUNT&nbsp;60(0)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO&nbsp;STANDARD&nbsp;PAGE&nbsp;HEADING.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
*&nbsp;this&nbsp;report&nbsp;is&nbsp;based&nbsp;on&nbsp;the&nbsp;logical&nbsp;database&nbsp;SDF&nbsp;*********************<br/>
************************************************************************<br/>
<br/>
************************************************************************<br/>
*&nbsp;this&nbsp;report&nbsp;is&nbsp;based&nbsp;on&nbsp;the&nbsp;logical&nbsp;database&nbsp;SDF&nbsp;*********************<br/>
************************************************************************<br/>
<br/>
</div>
<div class="code">
TABLES: skc1a,             "G/L account sales segment derived from GLDB<br/>
&nbsp;&nbsp;skb1,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"G/L&nbsp;account&nbsp;master&nbsp;(company&nbsp;code)<br/>
&nbsp;&nbsp;skat,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"G/L&nbsp;account&nbsp;(chart&nbsp;of&nbsp;accounts:&nbsp;description)<br/>
&nbsp;&nbsp;t001,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Company&nbsp;Codes<br/>
&nbsp;&nbsp;t041c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Reasons&nbsp;for&nbsp;reverse&nbsp;posting<br/>
&nbsp;&nbsp;bseg,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"needed&nbsp;for&nbsp;net.increase&nbsp;elimination<br/>
&nbsp;&nbsp;bkpf,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"just&nbsp;needed&nbsp;for&nbsp;month&nbsp;selection<br/>
&nbsp;&nbsp;sscrfields.<br/>
<br/>
</div>
<div class="codeComment">
***********************************************&nbsp;data&nbsp;list&nbsp;viewer&nbsp;*****<br/>
<br/>
</div>
<div class="code">
TYPE-POOLS: slis.<br/>
<br/>
INCLUDE &lt;icon&gt;.<br/>
INCLUDE &lt;symbol&gt;.<br/>
<br/>
CONSTANTS : gc_formname_top_of_page TYPE slis_formname<br/>
VALUE 'TOP_OF_PAGE',<br/>
gc_formname_end_of_list TYPE slis_formname<br/>
VALUE 'END_OF_LIST'.<br/>
<br/>
DATA : gt_fieldcat         TYPE slis_t_fieldcat_alv, "fieldattributes<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_layout_alv,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"list&nbsp;layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_t_event,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"list&nbsp;events<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_sp_group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_t_sp_group_alv,&nbsp;"list&nbsp;groups<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_repid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-repid,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"reportname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_list_top_of_page&nbsp;TYPE&nbsp;slis_t_listheader,&nbsp;&nbsp;"top-of-page<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_save(1)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gx_variant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;disvariant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_variant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;disvariant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_exit(1)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
***********************************************&nbsp;&nbsp;data&nbsp;records&nbsp;&nbsp;&nbsp;**<br/>
<br/>
*********&nbsp;list&nbsp;*******************<br/>
</div>
<div class="code">
DATA: BEGIN OF list,<br/>
&nbsp;&nbsp;saknr&nbsp;LIKE&nbsp;skc1a-saknr,<br/>
&nbsp;&nbsp;txt50&nbsp;LIKE&nbsp;skat-txt50,<br/>
&nbsp;&nbsp;curr&nbsp;&nbsp;LIKE&nbsp;skc1a-hwaer,<br/>
END OF list.<br/>
<br/>
</div>
<div class="codeComment">
***********************************************&nbsp;&nbsp;internal&nbsp;tables&nbsp;&nbsp;&nbsp;**<br/>
<br/>
*******&nbsp;output_list&nbsp;******************<br/>
</div>
<div class="code">
DATA: BEGIN OF output_list OCCURS 100,<br/>
&nbsp;&nbsp;saknr&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skc1a-saknr,<br/>
&nbsp;&nbsp;txt50&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skat-txt50,<br/>
&nbsp;&nbsp;wa_umxxs&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;wa_umxxh&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;wa_umxxk&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;wa_lasts&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;wa_lasth&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;wa_bebal&nbsp;LIKE&nbsp;skc1a-um01k,<br/>
&nbsp;&nbsp;curr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skc1a-hwaer,<br/>
END OF output_list.<br/>
<br/>
</div>
<div class="codeComment">
********&nbsp;except_list&nbsp;************************<br/>
</div>
<div class="code">
DATA: except_list LIKE output_list OCCURS 0.<br/>
<br/>
</div>
<div class="codeComment">
**************************************************************&nbsp;data&nbsp;***<br/>
</div>
<div class="code">
DATA: umxxs           LIKE skc1a-um01s,   "variable for debit fields<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umxxh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skc1a-um01h,&nbsp;&nbsp;&nbsp;"variable&nbsp;for&nbsp;credit&nbsp;fields<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umxxk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skc1a-um01k,&nbsp;&nbsp;&nbsp;"varible&nbsp;for&nbsp;ending&nbsp;balance<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bebal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;skc1a-um01k,&nbsp;&nbsp;&nbsp;&nbsp;"variable&nbsp;for&nbsp;begining&nbsp;balance<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-INDEX,&nbsp;&nbsp;&nbsp;&nbsp;"loop&nbsp;counter&nbsp;for&nbsp;begining&nbsp;balance<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;linecount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-dbcnt,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Line&nbsp;counter&nbsp;output&nbsp;table<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date_string(10)&nbsp;TYPE&nbsp;C,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"date&nbsp;string-format<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;per_head(12)&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"list&nbsp;period&nbsp;in&nbsp;header<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;headli1(30)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;C,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"column&nbsp;headline&nbsp;last&nbsp;Period<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;I,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"line&nbsp;counter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sthfound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;I,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;curren&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001-waers.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;currency&nbsp;format<br/>
DATA: ok_code TYPE sy-ucomm.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;specific&nbsp;selections&nbsp;screen&nbsp;design&nbsp;************************************<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK period WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;&nbsp;period&nbsp;&nbsp;FOR&nbsp;&nbsp;bkpf-monat&nbsp;&nbsp;&nbsp;&nbsp;DEFAULT&nbsp;sy-datum+4(2)&nbsp;"'01'&nbsp;TO&nbsp;'16'<br/>
&nbsp;&nbsp;OBLIGATORY.<br/>
<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;&nbsp;doctype&nbsp;FOR&nbsp;&nbsp;bkpf-blart.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;without&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AS&nbsp;CHECKBOX&nbsp;MODIF&nbsp;ID&nbsp;m1.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK period.<br/>
<br/>
</div>
<div class="codeComment">
*************************************************************&nbsp;two<br/>
*&nbsp;selection-screen&nbsp;begin&nbsp;of&nbsp;block&nbsp;two&nbsp;with&nbsp;frame.<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK a WITH FRAME TITLE TEXT-060.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;p_zebra&nbsp;&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;p_colopt&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_keyhot&nbsp;as&nbsp;checkbox&nbsp;default&nbsp;'&nbsp;'.<br/>
</div>
<div class="code">
SELECTION-SCREEN END OF BLOCK a.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------<br/>
<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;P_TOTONL&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'&nbsp;',<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_TOTEXT(60),<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_STTEXT(60).<br/>
*&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;C.<br/>
<br/>
*----------------------------------------------------------------<br/>
<br/>
*&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;D&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-064.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_chkbox&nbsp;as&nbsp;checkbox&nbsp;default&nbsp;'X',<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_GROUPB&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'&nbsp;',<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_DETPOP&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'X',<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_GROUPS&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'&nbsp;'.<br/>
*&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;D.<br/>
<br/>
*&nbsp;selection-screen&nbsp;end&nbsp;of&nbsp;block&nbsp;two.<br/>
**************************************************************&nbsp;six<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK forth WITH FRAME TITLE TEXT-063.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;&nbsp;p_vari&nbsp;&nbsp;&nbsp;LIKE&nbsp;disvariant-variant&nbsp;MODIF&nbsp;ID&nbsp;m1&nbsp;.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_BKG&nbsp;TYPE&nbsp;CHAR01&nbsp;NO-DISPLAY.<br/>
SELECTION-SCREEN END OF BLOCK forth.<br/>
<br/>
</div>
<div class="codeComment">
**&nbsp;end&nbsp;selection-screen&nbsp;*******************************************<br/>
<br/>
************************************************************************<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN OUTPUT.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*<br/>
</div>
<div class="code">
"隐藏标准屏幕项<br/>
LOOP AT SCREEN .<br/>
&nbsp;&nbsp;IF&nbsp;SCREEN-group1&nbsp;=&nbsp;'M1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-active&nbsp;=&nbsp;'0'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;SCREEN-group4&nbsp;=&nbsp;'027'&nbsp;OR&nbsp;SCREEN-group4&nbsp;=&nbsp;'028'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-active&nbsp;=&nbsp;'0'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDLOOP.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*<br/>
**&nbsp;eliminate&nbsp;unused&nbsp;screen&nbsp;selections<br/>
</div>
<div class="code">
LOOP AT SCREEN.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-group3&nbsp;=&nbsp;'IXS'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-group4&nbsp;=&nbsp;'003'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-group4&nbsp;=&nbsp;'012'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-group4&nbsp;=&nbsp;'013'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-name&nbsp;&nbsp;&nbsp;=&nbsp;'SD_BUKRS-HIGH'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-name&nbsp;&nbsp;&nbsp;=&nbsp;'SD_GJAHR-HIGH'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-name&nbsp;&nbsp;&nbsp;=&nbsp;'%_SD_BUKRS_%_APP_%-VALU_PUSH'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-name&nbsp;&nbsp;&nbsp;=&nbsp;'%_SD_GJAHR_%_APP_%-VALU_PUSH'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;field&nbsp;won#t&nbsp;be&nbsp;displayed<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-active&nbsp;=&nbsp;0.<br/>
<br/>
ELSEIF     SCREEN-name = 'SD_BUKRS-LOW'<br/>
&nbsp;&nbsp;OR&nbsp;&nbsp;SCREEN-name&nbsp;=&nbsp;'SD_GJAHR-LOW'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;field&nbsp;is&nbsp;obligatory<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SCREEN-required&nbsp;=&nbsp;1.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*************************************************&nbsp;&nbsp;initialization&nbsp;****<br/>
</div>
<div class="code">
INITIALIZATION.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;clearing<br/>
</div>
<div class="code">
CLEAR: umxxs, umxxh, umxxk,<br/>
CNT, list, date_string,<br/>
per_head, headli1.<br/>
REFRESH: output_list, except_list.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;set&nbsp;the&nbsp;current&nbsp;year&nbsp;as&nbsp;a&nbsp;default&nbsp;value&nbsp;for&nbsp;the&nbsp;fiscal&nbsp;year<br/>
</div>
<div class="code">
sd_gjahr-low = sy-datum.<br/>
APPEND sd_gjahr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;initialization&nbsp;list&nbsp;viewer&nbsp;------------------------<br/>
</div>
<div class="code">
g_repid = sy-repid.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;perform&nbsp;fieldcat_init&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;using&nbsp;&nbsp;&nbsp;&nbsp;gt_fieldcat[].<br/>
</div>
<div class="code">
PERFORM eventtab_build     USING    gt_events[].<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;perform&nbsp;make_header_line.<br/>
*&nbsp;perform&nbsp;comment_build&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;using&nbsp;&nbsp;&nbsp;&nbsp;gt_list_top_of_page[].<br/>
<br/>
</div>
<div class="code">
PERFORM sp_group_build     USING    gt_sp_group[].<br/>
<br/>
g_save = 'A'.<br/>
<br/>
CLEAR g_variant.<br/>
g_variant-REPORT = g_repid.<br/>
<br/>
CALL FUNCTION 'REUSE_ALV_VARIANT_DEFAULT_GET'<br/>
EXPORTING<br/>
&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_save<br/>
CHANGING<br/>
&nbsp;&nbsp;cs_variant&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gx_variant<br/>
EXCEPTIONS<br/>
&nbsp;&nbsp;wrong_input&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;program_error&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
IF sy-subrc = 0.<br/>
&nbsp;&nbsp;p_vari&nbsp;=&nbsp;gx_variant-variant.<br/>
ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*****************************************&nbsp;&nbsp;Process&nbsp;on&nbsp;value&nbsp;request&nbsp;**<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_vari.<br/>
PERFORM for_variant.<br/>
<br/>
</div>
<div class="codeComment">
***********************************&nbsp;At&nbsp;Selection&nbsp;Screen&nbsp;//&nbsp;&nbsp;PAI&nbsp;******<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
<br/>
PERFORM pai_of_selection_screen.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Period&nbsp;value&nbsp;must&nbsp;be&nbsp;in&nbsp;1&nbsp;to&nbsp;16!<br/>
</div>
<div class="code">
IF   period-low &gt; '16'    OR    period-high &gt; '16'.<br/>
&nbsp;&nbsp;MESSAGE&nbsp;e712.<br/>
ENDIF.<br/>
<br/>
IF       NOT  period-low    IS INITIAL<br/>
AND       period-high   IS INITIAL.<br/>
&nbsp;&nbsp;MOVE&nbsp;period-low&nbsp;TO&nbsp;period-high.<br/>
ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Choose&nbsp;one&nbsp;company&nbsp;code!<br/>
</div>
<div class="code">
IF sd_bukrs-low CA '*'.<br/>
&nbsp;&nbsp;MESSAGE&nbsp;e713.<br/>
ENDIF.<br/>
<br/>
<br/>
CHECK sscrfields-ucomm  = 'ONLI' OR<br/>
sscrfields-ucomm  = 'PRIN' OR<br/>
sscrfields-ucomm  = 'SJOB'.         "when executed only<br/>
<br/>
<br/>
SELECT SINGLE * FROM  t001<br/>
WHERE bukrs = sd_bukrs-low.<br/>
<br/>
MOVE t001-waers TO curren.<br/>
</div>
<div class="codeComment">
*&nbsp;negative&nbsp;posting&nbsp;flag&nbsp;is&nbsp;not&nbsp;set&nbsp;on!<br/>
</div>
<div class="code">
IF t001-xnegp &lt;&gt; 'X'.<br/>
&nbsp;&nbsp;SET&nbsp;CURSOR&nbsp;FIELD&nbsp;sd_bukrs-low.<br/>
&nbsp;&nbsp;MESSAGE&nbsp;w714&nbsp;WITH&nbsp;sd_bukrs-low.<br/>
ELSE.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;The&nbsp;result&nbsp;may&nbsp;not&nbsp;be&nbsp;correct&nbsp;because&nbsp;of&nbsp;partial&nbsp;negative&nbsp;posting!<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;FROM&nbsp;t041c&nbsp;WHERE&nbsp;xnegp&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;ENDSELECT.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;w715.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
***********************************************&nbsp;&nbsp;start-of-selection&nbsp;**<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
CLEAR: output_list[].<br/>
CNT = period-low - 1.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;-----------------&nbsp;GET&nbsp;skb1&nbsp;----------<br/>
</div>
<div class="code">
GET skb1.<br/>
</div>
<div class="codeComment">
*&nbsp;-------------------------------------<br/>
<br/>
</div>
<div class="code">
CLEAR sthfound.<br/>
<br/>
SELECT SINGLE * FROM skat    WHERE spras = sy-langu<br/>
AND   ktopl = t001-ktopl<br/>
AND   saknr = skb1-saknr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;global&nbsp;account&nbsp;data<br/>
</div>
<div class="code">
list-saknr = skb1-saknr.<br/>
list-txt50 = skat-txt50.<br/>
list-curr  = curren.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;-----------------&nbsp;GET&nbsp;SKC1A&nbsp;---------<br/>
</div>
<div class="code">
GET skc1a.<br/>
</div>
<div class="codeComment">
*&nbsp;-------------------------------------<br/>
<br/>
</div>
<div class="code">
PERFORM create_list.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;the&nbsp;account&nbsp;has&nbsp;been&nbsp;posted<br/>
</div>
<div class="code">
sthfound = 1.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;-------------------&nbsp;GET&nbsp;SKB1&nbsp;LATE&nbsp;------<br/>
</div>
<div class="code">
GET skb1 LATE.<br/>
</div>
<div class="codeComment">
*&nbsp;----------------------------------------<br/>
<br/>
*&nbsp;a&nbsp;non-posting&nbsp;account&nbsp;was&nbsp;detected<br/>
*&nbsp;and&nbsp;display&nbsp;without&nbsp;non-posting&nbsp;accounts&nbsp;are&nbsp;required<br/>
</div>
<div class="code">
IF      without = space<br/>
AND  sthfound IS INITIAL.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;output_list.<br/>
&nbsp;&nbsp;REJECT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"skip&nbsp;to&nbsp;the&nbsp;next&nbsp;record<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;a&nbsp;non-posting&nbsp;account&nbsp;was&nbsp;detected<br/>
*&nbsp;and&nbsp;display&nbsp;with&nbsp;non-posting&nbsp;accounts&nbsp;are&nbsp;required<br/>
</div>
<div class="code">
ELSEIF      without = 'X'<br/>
AND  sthfound IS INITIAL.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;add&nbsp;non-posting&nbsp;account<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;output_list.<br/>
&nbsp;&nbsp;output_list-saknr&nbsp;=&nbsp;skb1-saknr.<br/>
&nbsp;&nbsp;output_list-txt50&nbsp;=&nbsp;skat-txt50.<br/>
&nbsp;&nbsp;output_list-curr&nbsp;&nbsp;=&nbsp;skb1-waers.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fill&nbsp;internal&nbsp;table&nbsp;output_list&nbsp;with&nbsp;non-posting&nbsp;accountinfo<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;output_list.<br/>
<br/>
ENDIF.<br/>
</div>
<div class="codeComment">
*********************************************&nbsp;end-of-selection&nbsp;*******<br/>
</div>
<div class="code">
END-OF-SELECTION.<br/>
<br/>
<br/>
PERFORM fieldcat_init      USING    gt_fieldcat[].<br/>
PERFORM comment_build      USING    gt_list_top_of_page[].<br/>
PERFORM layout_build       USING    gs_layout.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;posted&nbsp;tax&nbsp;invoices&nbsp;have&nbsp;to&nbsp;be&nbsp;&nbsp;eleminated<br/>
</div>
<div class="code">
PERFORM correction_output_list.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;how&nbsp;many&nbsp;accounts&nbsp;have&nbsp;been&nbsp;found&nbsp;?<br/>
</div>
<div class="code">
DESCRIBE TABLE output_list LINES found.<br/>
<br/>
"把数据存储到database 在现金流量表中计算余额<br/>
IF P_BKG = 'X'.<br/>
&nbsp;&nbsp;EXPORT&nbsp;output_list[]&nbsp;TO&nbsp;DATABASE&nbsp;indx(LT)&nbsp;ID&nbsp;'LIST'.<br/>
&nbsp;&nbsp;EXIT.<br/>
ENDIF.<br/>
<br/>
IF found &gt;= 1.<br/>
&nbsp;&nbsp;IF&nbsp;P_BKG&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;OUTPUT.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ELSE.<br/>
&nbsp;&nbsp;MESSAGE&nbsp;i500.<br/>
ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*****************************************************&nbsp;top&nbsp;of&nbsp;page&nbsp;****<br/>
</div>
<div class="code">
TOP-OF-PAGE.<br/>
<br/>
</div>
<div class="codeComment">
***********************************************************&nbsp;forms&nbsp;****<br/>
<br/>
<br/>
**&nbsp;----------------------------------------------<br/>
**&nbsp;output&nbsp;****************************<br/>
**&nbsp;----------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM OUTPUT.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;this&nbsp;form&nbsp;diplays&nbsp;the&nbsp;selected&nbsp;check&nbsp;list&nbsp;above&nbsp;with&nbsp;the<br/>
*&nbsp;ABAP&nbsp;List&nbsp;viewer<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY'<br/>
&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_INTERFACE_CHECK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_structure_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'OUTPUT_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;is_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_EXCLUDING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;it_special_groups&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_sp_group[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_SORT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_FILTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_SEL_HIDE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_DEFAULT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_save<br/>
&nbsp;&nbsp;&nbsp;&nbsp;is_variant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_variant<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_events[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_EVENT_EXIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_PRINT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_REPREP_ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_START_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_START_LINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_END_COLUMN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SCREEN_END_LINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_EXIT_CAUSED_BY_CALLER&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ES_EXIT_CAUSED_BY_USER&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;output_list<br/>
&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;MESSAGE&nbsp;ID&nbsp;SY-MSGID&nbsp;TYPE&nbsp;SY-MSGTY&nbsp;NUMBER&nbsp;SY-MSGNO<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;SY-MSGV1&nbsp;SY-MSGV2&nbsp;SY-MSGV3&nbsp;SY-MSGV4.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*********************************************************************<br/>
<br/>
**&nbsp;----------------------------------------------------<br/>
**&nbsp;form&nbsp;fieldcat_init&nbsp;*********************************<br/>
**&nbsp;----------------------------------------------------<br/>
<br/>
*&nbsp;&nbsp;this&nbsp;form&nbsp;creates&nbsp;the&nbsp;fieldcatalog&nbsp;which&nbsp;is&nbsp;necessary&nbsp;for<br/>
*&nbsp;&nbsp;the&nbsp;ABAP&nbsp;List&nbsp;Viewer<br/>
<br/>
</div>
<div class="code">
FORM fieldcat_init USING lt_fieldcat TYPE slis_t_fieldcat_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_fieldcat&nbsp;TYPE&nbsp;slis_fieldcat_alv.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'SAKNR'.<br/>
&nbsp;&nbsp;ls_fieldcat-KEY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_fieldcat-no_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-sp_group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'A'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;text-100.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;10.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'TXT50'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'L'.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;text-104.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKAT'.<br/>
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;50.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;ls_fieldcat,&nbsp;headli1.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_BEBAL'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;TEXT-303.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
*&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
&nbsp;&nbsp;ls_fieldcat-edit_mask&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'3'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CLEAR:&nbsp;ls_fieldcat,&nbsp;headli1.<br/>
*&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_LASTS'.<br/>
*&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
*<br/>
*&nbsp;&nbsp;CONCATENATE&nbsp;text-300<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;period-high<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text-302&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;headli1&nbsp;&nbsp;&nbsp;SEPARATED&nbsp;BY&nbsp;space&nbsp;.<br/>
*<br/>
*&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;headli1.<br/>
*&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
*&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
*&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
*&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
*&nbsp;&nbsp;ls_fieldcat-emphasize&nbsp;&nbsp;=&nbsp;'C300'.<br/>
*&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
**&nbsp;ls_fieldcat-ref_fieldname&nbsp;=&nbsp;'UM01S'.<br/>
*&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'4'.<br/>
*&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
*&nbsp;&nbsp;CLEAR:&nbsp;ls_fieldcat,&nbsp;headli1.<br/>
*&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_LASTH'.<br/>
*&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
*<br/>
*&nbsp;&nbsp;CONCATENATE&nbsp;text-300<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;period-high<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text-301&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;headli1&nbsp;&nbsp;&nbsp;SEPARATED&nbsp;BY&nbsp;space&nbsp;.<br/>
*<br/>
*&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;headli1.<br/>
*&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
*&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
*&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
*&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
*&nbsp;&nbsp;ls_fieldcat-emphasize&nbsp;&nbsp;=&nbsp;'C300'.<br/>
*&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'5'.<br/>
*&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_UMXXS'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;TEXT-106.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'6'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_UMXXH'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;TEXT-107.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'7'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'WA_UMXXK'.<br/>
&nbsp;&nbsp;ls_fieldcat-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;ls_fieldcat-reptext_ddic&nbsp;&nbsp;=&nbsp;TEXT-108.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_fieldcat-ref_fieldname&nbsp;=&nbsp;'UMXXK'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CURR'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_fieldcat-cfieldname&nbsp;=&nbsp;'CURR'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;15.<br/>
&nbsp;&nbsp;ls_fieldcat-do_sum&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'8'.<br/>
&nbsp;&nbsp;ls_fieldcat-edit_mask&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat.<br/>
&nbsp;&nbsp;ls_fieldcat-fieldname&nbsp;=&nbsp;'CURR'.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_tabname&nbsp;=&nbsp;'SKC1A'.<br/>
&nbsp;&nbsp;ls_fieldcat-ref_fieldname&nbsp;=&nbsp;'HWAER'.<br/>
&nbsp;&nbsp;ls_fieldcat-outputlen&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;ls_fieldcat-datatype&nbsp;=&nbsp;'CUKY'.<br/>
&nbsp;&nbsp;ls_fieldcat-col_pos&nbsp;&nbsp;=&nbsp;'9'.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat&nbsp;TO&nbsp;lt_fieldcat.<br/>
<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*******************************************************************<br/>
<br/>
**&nbsp;------------------------------------------------<br/>
**&nbsp;form&nbsp;layout_build&nbsp;******************************<br/>
**&nbsp;------------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM layout_build USING ls_layout TYPE slis_layout_alv.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;ls_layout-f2code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&amp;ETA'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_zebra.<br/>
&nbsp;&nbsp;ls_layout-colwidth_optimize&nbsp;=&nbsp;p_colopt.<br/>
</div>
<div class="codeComment">
*&nbsp;if&nbsp;p_chkbox&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;ls_layout-box_fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'BOX'.<br/>
*&nbsp;else.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout-box_fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;space.<br/>
</div>
<div class="codeComment">
*&nbsp;endif.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout-no_input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ls_layout-no_vline&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'.<br/>
&nbsp;&nbsp;ls_layout-no_colhead&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;if&nbsp;p_lights&nbsp;=&nbsp;'X'&nbsp;or&nbsp;p_lightc&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;ls_layout-lights_fieldname&nbsp;=&nbsp;'LIGHTS'.<br/>
*&nbsp;&nbsp;else.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;clear&nbsp;ls_layout-lights_fieldname.<br/>
*&nbsp;&nbsp;endif.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout-lights_condense&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_layout-totals_text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_totext.<br/>
*&nbsp;ls_layout-subtotals_text&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_sttext.<br/>
*&nbsp;ls_layout-totals_only&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_totonl.<br/>
*&nbsp;ls_layout-key_hotspot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_keyhot.<br/>
*&nbsp;ls_layout-detail_popup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_detpop.<br/>
*&nbsp;ls_layout-group_change_edit&nbsp;=&nbsp;p_groups.<br/>
*&nbsp;LS_LAYOUT-GROUP_BUTTONS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;P_GROUPB.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_layout-group_buttons&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;space.<br/>
</div>
<div class="codeComment">
*&nbsp;ls_layout-numc_sum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
*&nbsp;ls_layout-totals_before_items&nbsp;='X'.<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
******************************************************************<br/>
<br/>
**&nbsp;--------------------------------------------------<br/>
**&nbsp;form&nbsp;eventtab_build&nbsp;******************************<br/>
**&nbsp;--------------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM eventtab_build USING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_events&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;slis_t_event.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_event&nbsp;TYPE&nbsp;slis_alv_event.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_EVENTS_GET'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_list_type&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;et_events&nbsp;&nbsp;&nbsp;=&nbsp;lt_events.<br/>
<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_events&nbsp;WITH&nbsp;KEY&nbsp;name&nbsp;=&nbsp;slis_ev_top_of_page<br/>
&nbsp;&nbsp;INTO&nbsp;ls_event.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;gc_formname_top_of_page&nbsp;TO&nbsp;ls_event-FORM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_event&nbsp;TO&nbsp;lt_events.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_events&nbsp;WITH&nbsp;KEY&nbsp;name&nbsp;=&nbsp;slis_ev_end_of_list<br/>
&nbsp;&nbsp;INTO&nbsp;ls_event.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;gc_formname_end_of_list&nbsp;TO&nbsp;ls_event-FORM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_event&nbsp;TO&nbsp;lt_events.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
******************************************************************<br/>
<br/>
**&nbsp;---------------------------------------------------<br/>
**&nbsp;form&nbsp;comment_build&nbsp;********************************<br/>
**&nbsp;---------------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM comment_build USING lt_top_of_page TYPE slis_t_listheader.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_line&nbsp;TYPE&nbsp;slis_listheader.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Listenüberschrift:&nbsp;Typ&nbsp;H<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;TEXT-052.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Kopfinfo:&nbsp;Typ&nbsp;S<br/>
<br/>
*&nbsp;----------------------------------&nbsp;1.&nbsp;Zeile<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ls_line-KEY&nbsp;&nbsp;=&nbsp;TEXT-101.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;----------------------------------&nbsp;2.&nbsp;Zeile<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ls_line-KEY&nbsp;&nbsp;=&nbsp;TEXT-099.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;sy-repid.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;----------------------------------&nbsp;3.&nbsp;Zeile<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ls_line-KEY&nbsp;&nbsp;=&nbsp;TEXT-102.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;sy-TITLE.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;-----------------------------------&nbsp;4.Zeile<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'S'.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERT_DATE_TO_EXTERNAL'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;date_internal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-datum<br/>
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;date_external&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;date_string<br/>
&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;date_internal_is_invalid&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;MESSAGE&nbsp;ID&nbsp;SY-MSGID&nbsp;TYPE&nbsp;SY-MSGTY&nbsp;NUMBER&nbsp;SY-MSGNO<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;SY-MSGV1&nbsp;SY-MSGV2&nbsp;SY-MSGV3&nbsp;SY-MSGV4.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ls_line-KEY&nbsp;&nbsp;=&nbsp;TEXT-100.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;date_string.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;----------------------------------&nbsp;5.&nbsp;Zeile<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CONCATENATE&nbsp;&nbsp;period-low&nbsp;'-'&nbsp;period-high&nbsp;INTO&nbsp;per_head.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ls_line-KEY&nbsp;&nbsp;=&nbsp;TEXT-098.<br/>
&nbsp;&nbsp;ls_line-info&nbsp;=&nbsp;per_head.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;lt_top_of_page.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Aktionsinfo:&nbsp;Typ&nbsp;A<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_line.<br/>
&nbsp;&nbsp;ls_line-typ&nbsp;&nbsp;=&nbsp;'A'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_line-key&nbsp;=&nbsp;text-053.<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;ls_line&nbsp;TO&nbsp;&nbsp;lt_top_of_page.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*******************************************************************<br/>
<br/>
**&nbsp;---------------------------------------------<br/>
**&nbsp;form&nbsp;top_of_page&nbsp;****************************<br/>
**&nbsp;---------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM top_of_page.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_COMMENTARY_WRITE'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_list_commentary&nbsp;=&nbsp;gt_list_top_of_page.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*******************************************************************<br/>
<br/>
**&nbsp;---------------------------------------------<br/>
**&nbsp;form&nbsp;for_variant&nbsp;****************************<br/>
**&nbsp;---------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM for_variant.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_VARIANT_F4'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;is_variant&nbsp;=&nbsp;g_variant<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_save<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_default_fieldcat&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_exit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_exit<br/>
&nbsp;&nbsp;&nbsp;&nbsp;es_variant&nbsp;=&nbsp;gx_variant<br/>
&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;not_found&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;'S'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;ELSE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_exit&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_vari&nbsp;=&nbsp;gx_variant-variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
********************************************************************<br/>
<br/>
**&nbsp;--------------------------------------------<br/>
**&nbsp;form&nbsp;pai_of_selection_screen&nbsp;***************<br/>
**&nbsp;--------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM pai_of_selection_screen.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;NOT&nbsp;p_vari&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;g_variant&nbsp;TO&nbsp;gx_variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;p_vari&nbsp;TO&nbsp;gx_variant-variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_VARIANT_EXISTENCE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;g_save<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cs_variant&nbsp;=&nbsp;gx_variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;g_variant&nbsp;=&nbsp;gx_variant.<br/>
<br/>
&nbsp;&nbsp;ELSE.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;g_variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;g_variant-REPORT&nbsp;=&nbsp;g_repid.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.                               " PAI_OF_SELECTION_SCREEN<br/>
<br/>
</div>
<div class="codeComment">
********************************************************************<br/>
<br/>
**&nbsp;-----------------------------------------------<br/>
**&nbsp;form&nbsp;sp_group_build&nbsp;***************************<br/>
**&nbsp;-----------------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM sp_group_build USING lt_sp_group TYPE slis_t_sp_group_alv.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_sp_group&nbsp;TYPE&nbsp;slis_sp_group_alv.<br/>
</div>
<div class="codeComment">
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;&nbsp;ls_sp_group.<br/>
&nbsp;&nbsp;ls_sp_group-sp_group&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;ls_sp_group-TEXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;TEXT-005.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_sp_group&nbsp;TO&nbsp;lt_sp_group.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
**********************************************************************<br/>
<br/>
<br/>
**&nbsp;--------------------------------------<br/>
**&nbsp;form&nbsp;create_list<br/>
**&nbsp;--------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM create_list.<br/>
<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;output_list&nbsp;WITH&nbsp;KEY&nbsp;saknr&nbsp;=&nbsp;skc1a-saknr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;nothing&nbsp;found<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&gt;&nbsp;2.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;add&nbsp;new&nbsp;header&nbsp;information<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;output_list-saknr&nbsp;=&nbsp;skc1a-saknr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-txt50&nbsp;=&nbsp;list-txt50.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-curr&nbsp;&nbsp;=&nbsp;list-curr.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initialize&nbsp;reporting&nbsp;amounts<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;output_list-wa_umxxs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_umxxh,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_bebal,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_umxxk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_lasts,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_lasth.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;output_list.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;read&nbsp;SKC1A&nbsp;data&nbsp;from&nbsp;period-low&nbsp;to&nbsp;period-high<br/>
</div>
<div class="code">
&nbsp;&nbsp;DO&nbsp;period-high&nbsp;TIMES<br/>
&nbsp;&nbsp;VARYING&nbsp;umxxs&nbsp;FROM&nbsp;skc1a-um01s&nbsp;NEXT&nbsp;skc1a-um02s<br/>
&nbsp;&nbsp;VARYING&nbsp;umxxh&nbsp;FROM&nbsp;skc1a-um01h&nbsp;NEXT&nbsp;skc1a-um02h<br/>
&nbsp;&nbsp;VARYING&nbsp;umxxk&nbsp;FROM&nbsp;skc1a-um01k&nbsp;NEXT&nbsp;skc1a-um02k.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-INDEX&nbsp;&gt;=&nbsp;period-low.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;summarize<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_umxxs&nbsp;=&nbsp;output_list-wa_umxxs&nbsp;+&nbsp;umxxs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;output_list-wa_umxxh&nbsp;=&nbsp;output_list-wa_umxxh&nbsp;+&nbsp;umxxh.<br/>
<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;update<br/>
</div>
<div class="code">
&nbsp;&nbsp;output_list-wa_lasts&nbsp;=&nbsp;output_list-wa_lasts&nbsp;+&nbsp;umxxs.<br/>
&nbsp;&nbsp;output_list-wa_lasth&nbsp;=&nbsp;output_list-wa_lasth&nbsp;+&nbsp;umxxh.<br/>
&nbsp;&nbsp;output_list-wa_umxxk&nbsp;=&nbsp;output_list-wa_umxxk&nbsp;+&nbsp;umxxk.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;determine&nbsp;the&nbsp;starting&nbsp;balance<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;period-low&nbsp;=&nbsp;'01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;bebal&nbsp;=&nbsp;skc1a-umsav.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;CNT&nbsp;TIMES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;VARYING&nbsp;bebal&nbsp;FROM&nbsp;skc1a-um01k&nbsp;NEXT&nbsp;skc1a-um02k.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;output_list-wa_bebal&nbsp;=&nbsp;output_list-wa_bebal&nbsp;+&nbsp;bebal.<br/>
</div>
<div class="codeComment">
*&nbsp;end&nbsp;bebal<br/>
<br/>
*&nbsp;change&nbsp;output_list<br/>
</div>
<div class="code">
&nbsp;&nbsp;MODIFY&nbsp;TABLE&nbsp;output_list.<br/>
<br/>
ENDFORM.                               " CREATE_NET_LIST<br/>
<br/>
<br/>
</div>
<div class="codeComment">
***********************************************************************<br/>
<br/>
**&nbsp;-------------------------------------<br/>
**&nbsp;form&nbsp;end_of_list<br/>
**&nbsp;------------------------------------<br/>
<br/>
</div>
<div class="code">
FORM end_of_list.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;how&nbsp;many&nbsp;accounts&nbsp;have&nbsp;been&nbsp;selected<br/>
</div>
<div class="code">
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;output_list&nbsp;LINES&nbsp;linecount.<br/>
<br/>
&nbsp;&nbsp;SKIP&nbsp;1.<br/>
&nbsp;&nbsp;WRITE:/&nbsp;TEXT-054,&nbsp;linecount&nbsp;.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
</div>
<div class="code">
FORM correction_output_list.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;&nbsp;wa_out_list&nbsp;LIKE&nbsp;output_list.<br/>
&nbsp;&nbsp;DATA:&nbsp;&nbsp;wa_lt_bseg&nbsp;TYPE&nbsp;bseg,&nbsp;"BVN&nbsp;NEW&nbsp;G/L<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_bseg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;fagl_t_bseg.&nbsp;"BVN&nbsp;NEW&nbsp;G/L<br/>
&nbsp;&nbsp;CLEAR:&nbsp;wa_out_list.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;NOT&nbsp;doctype&nbsp;IS&nbsp;INITIAL.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;tax&nbsp;invoice&nbsp;documents&nbsp;will&nbsp;be&nbsp;selected<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;&nbsp;*&nbsp;FROM&nbsp;&nbsp;bkpf<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sd_bukrs-low<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sd_gjahr-low<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;blart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IN&nbsp;doctype<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;monat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IN&nbsp;period.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;bkpf-stblg&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"BVN&nbsp;NEW&nbsp;G/L&nbsp;Start<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BSEG&nbsp;items&nbsp;belonging&nbsp;to&nbsp;the&nbsp;found&nbsp;document&nbsp;header&nbsp;will&nbsp;be&nbsp;sel.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;FROM&nbsp;&nbsp;BSEG<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;BKPF-BUKRS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;BELNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;BKPF-BELNR<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;&nbsp;GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;BKPF-GJAHR&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'FAGL_GET_BSEG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_bukrs&nbsp;&nbsp;&nbsp;=&nbsp;bkpf-bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_belnr&nbsp;&nbsp;&nbsp;=&nbsp;bkpf-belnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_gjahr&nbsp;&nbsp;&nbsp;=&nbsp;bkpf-gjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;et_bseg&nbsp;&nbsp;&nbsp;=&nbsp;lt_bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_found&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_bseg&nbsp;INTO&nbsp;wa_lt_bseg.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;wa_lt_bseg-hkont&nbsp;IN&nbsp;sd_saknr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;output_list&nbsp;WITH&nbsp;KEY&nbsp;saknr&nbsp;=&nbsp;wa_lt_bseg-hkont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;wa_out_list.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&nbsp;4.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update&nbsp;net&nbsp;increase&nbsp;amounts<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;update_amounts&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bkpf<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;&nbsp;wa_out_list.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;output_list&nbsp;FROM&nbsp;wa_out_list&nbsp;INDEX&nbsp;sy-tabix.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create&nbsp;internal&nbsp;table&nbsp;including&nbsp;all&nbsp;taxinvoice<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;net&nbsp;increase<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;create_except_list&nbsp;USING&nbsp;wa_out_list<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_lt_bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bkpf.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDSELECT.&nbsp;&nbsp;"&nbsp;BSEG<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"BVN&nbsp;NEW&nbsp;G/L&nbsp;END<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDSELECT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;BKPF<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;FORM&nbsp;update_amounts&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bseg&nbsp;LIKE&nbsp;bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bkpf&nbsp;LIKE&nbsp;bkpf<br/>
&nbsp;&nbsp;CHANGING&nbsp;&nbsp;wa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;output_list.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;net&nbsp;increase&nbsp;accumulated&nbsp;will&nbsp;be&nbsp;updated<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;wa_bseg-shkzg&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa-wa_umxxs&nbsp;=&nbsp;wa-wa_umxxs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;wa_bseg-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa-wa_umxxh&nbsp;=&nbsp;wa-wa_umxxh<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;wa_bseg-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;net&nbsp;increase&nbsp;last&nbsp;reported&nbsp;period&nbsp;will&nbsp;be&nbsp;updated<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;wa_bkpf-monat&nbsp;=&nbsp;period-high.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;wa_bseg-shkzg&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa-wa_lasts&nbsp;=&nbsp;wa-wa_lasts<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;wa_bseg-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa-wa_lasth&nbsp;=&nbsp;wa-wa_lasth<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;wa_bseg-dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;FORM&nbsp;create_except_list&nbsp;USING&nbsp;wa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;output_list<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bseg&nbsp;&nbsp;LIKE&nbsp;bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bkpf&nbsp;&nbsp;LIKE&nbsp;bkpf&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;&nbsp;wa_except&nbsp;&nbsp;&nbsp;LIKE&nbsp;output_list.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;wa_except.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;except_list&nbsp;WITH&nbsp;KEY&nbsp;&nbsp;&nbsp;saknr&nbsp;=&nbsp;wa-saknr&nbsp;INTO&nbsp;wa_except.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;entry&nbsp;not&nbsp;found<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&gt;=&nbsp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;wa_except.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_except-saknr&nbsp;=&nbsp;wa-saknr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_except-txt50&nbsp;=&nbsp;wa-txt50.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_except-curr&nbsp;&nbsp;=&nbsp;wa-curr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;update_amounts&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bkpf<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;&nbsp;wa_except.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;&nbsp;wa_except&nbsp;TO&nbsp;except_list.<br/>
</div>
<div class="codeComment">
*&nbsp;entry&nbsp;found<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;update_amounts&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bseg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wa_bkpf<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;&nbsp;wa_except.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;except_list&nbsp;FROM&nbsp;wa_except&nbsp;INDEX&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
*&nbsp;------------------------<br/>
*&nbsp;form&nbsp;set_status<br/>
*&nbsp;------------------------<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;FORM&nbsp;&nbsp;&nbsp;set_status&nbsp;&nbsp;&nbsp;&nbsp;USING&nbsp;&nbsp;rt_extab&nbsp;&nbsp;TYPE&nbsp;&nbsp;slis_t_extab.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;&nbsp;'STATUS_STANDARD'&nbsp;EXCLUDING&nbsp;rt_extab.<br/>
<br/>
&nbsp;&nbsp;ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
*&nbsp;--------------------------<br/>
*&nbsp;form&nbsp;&nbsp;&nbsp;user_command<br/>
*&nbsp;--------------------------<br/>
<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;FORM&nbsp;&nbsp;&nbsp;user_command&nbsp;&nbsp;&nbsp;USING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_ucomm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rs_selfield&nbsp;TYPE&nbsp;&nbsp;slis_selfield.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;&nbsp;n&nbsp;TYPE&nbsp;I.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;n.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;r_ucomm.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'EXCEPT'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;except_list&nbsp;LINES&nbsp;n.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;n&nbsp;=&nbsp;0&nbsp;.&nbsp;&nbsp;&nbsp;n&nbsp;=&nbsp;4.&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;n&nbsp;=&nbsp;n&nbsp;+&nbsp;8.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;8000&nbsp;&nbsp;&nbsp;STARTING&nbsp;AT&nbsp;5&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDING&nbsp;&nbsp;&nbsp;AT&nbsp;150&nbsp;n.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;rs_selfield-fieldname&nbsp;=&nbsp;'SAKNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;output_list&nbsp;INDEX&nbsp;rs_selfield-tabindex.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"双击更改单元格时清空<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'ACC'&nbsp;FIELD&nbsp;output_list-saknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BUK'&nbsp;FIELD&nbsp;sd_bukrs-low&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'GJR'&nbsp;FIELD&nbsp;sd_gjahr-low&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'FAGLB03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
<br/>
&nbsp;&nbsp;ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*********************************************************************<br/>
**&nbsp;form&nbsp;routines&nbsp;:&nbsp;end&nbsp;**********************************************<br/>
***********************************************************************<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;STATUS_8000&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;MODULE&nbsp;status_8000&nbsp;OUTPUT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STATI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'ST2'.<br/>
&nbsp;&nbsp;ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;OUTPUT_EXCEPTION_8000&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;MODULE&nbsp;output_exception_8000&nbsp;OUTPUT.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;&nbsp;wa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;output_list,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;how_many&nbsp;TYPE&nbsp;I.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;wa,&nbsp;how_many.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;except_list&nbsp;LINES&nbsp;how_many.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SUPPRESS&nbsp;DIALOG.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;LIST-PROCESSING&nbsp;&nbsp;AND&nbsp;RETURN&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;how_many&nbsp;&gt;&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SKIP&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;/2&nbsp;&nbsp;&nbsp;&nbsp;TEXT-901,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15&nbsp;&nbsp;&nbsp;TEXT-902.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;67&nbsp;&nbsp;&nbsp;TEXT-905.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;90&nbsp;&nbsp;&nbsp;TEXT-906.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;113&nbsp;&nbsp;TEXT-903.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;136&nbsp;&nbsp;TEXT-904.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;ON.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;159&nbsp;&nbsp;TEXT-907.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;except_list&nbsp;BY&nbsp;saknr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_NEGATIVE&nbsp;INTENSIFIED&nbsp;OFF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;except_list&nbsp;INTO&nbsp;wa.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;/2&nbsp;&nbsp;&nbsp;&nbsp;wa-saknr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15&nbsp;&nbsp;&nbsp;wa-txt50.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;67&nbsp;&nbsp;&nbsp;wa-wa_umxxs&nbsp;CURRENCY&nbsp;wa-curr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;90&nbsp;&nbsp;&nbsp;wa-wa_umxxh&nbsp;CURRENCY&nbsp;wa-curr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;113&nbsp;&nbsp;wa-wa_lasts&nbsp;CURRENCY&nbsp;wa-curr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_TOTAL&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;136&nbsp;&nbsp;wa-wa_lasth&nbsp;CURRENCY&nbsp;wa-curr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;COL_POSITIVE&nbsp;INTENSIFIED&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;&nbsp;&nbsp;159&nbsp;&nbsp;wa-curr.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SKIP&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORMAT&nbsp;COLOR&nbsp;OFF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;/20&nbsp;&nbsp;TEXT-995.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;001&nbsp;Special&nbsp;Selections<br/>
*&nbsp;005&nbsp;to<br/>
*&nbsp;052&nbsp;Trial&nbsp;Balance<br/>
*&nbsp;054&nbsp;Number&nbsp;of&nbsp;Accounts:<br/>
*&nbsp;060&nbsp;Options<br/>
*&nbsp;063&nbsp;Variants<br/>
*&nbsp;098&nbsp;Time&nbsp;Frame<br/>
*&nbsp;099&nbsp;Program<br/>
*&nbsp;100&nbsp;Date<br/>
*&nbsp;101&nbsp;User<br/>
*&nbsp;102&nbsp;Title<br/>
*&nbsp;106&nbsp;Accum.&nbsp;Value&nbsp;-&nbsp;D<br/>
*&nbsp;107&nbsp;Accum.&nbsp;Value&nbsp;-&nbsp;C<br/>
*&nbsp;108&nbsp;Cl.&nbsp;Bal.<br/>
*&nbsp;300&nbsp;Period<br/>
*&nbsp;301&nbsp;C<br/>
*&nbsp;302&nbsp;D<br/>
*&nbsp;303&nbsp;Beg.&nbsp;Balance<br/>
*&nbsp;901&nbsp;G/L&nbsp;Acct<br/>
*&nbsp;902&nbsp;Name<br/>
*&nbsp;903&nbsp;Debit&nbsp;(Last&nbsp;Period)<br/>
*&nbsp;904&nbsp;Credit&nbsp;(Last&nbsp;Period)<br/>
*&nbsp;905&nbsp;Total&nbsp;Debits<br/>
*&nbsp;906&nbsp;Total&nbsp;Credits<br/>
*&nbsp;907&nbsp;Crcy<br/>
*&nbsp;995&nbsp;***&nbsp;No&nbsp;disregarded&nbsp;documents&nbsp;found&nbsp;*********************************<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;DOCTYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disregard&nbsp;Document&nbsp;Type<br/>
*&nbsp;PERIOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fiscal&nbsp;Period<br/>
*&nbsp;P_COLOPT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optimize&nbsp;Columns<br/>
*&nbsp;P_VARI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Variant<br/>
*&nbsp;P_ZEBRA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Striped&nbsp;Pattern<br/>
*&nbsp;WITHOUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Accounts&nbsp;w/o&nbsp;Any&nbsp;Transactions<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;ICC-KR<br/>
*500&nbsp;&nbsp;&nbsp;No&nbsp;entries&nbsp;were&nbsp;found&nbsp;for&nbsp;the&nbsp;selections&nbsp;entered<br/>
*712&nbsp;&nbsp;&nbsp;Only&nbsp;period&nbsp;values&nbsp;between&nbsp;1&nbsp;and&nbsp;16&nbsp;are&nbsp;allowed<br/>
*713&nbsp;&nbsp;&nbsp;Enter&nbsp;one&nbsp;company&nbsp;code&nbsp;only<br/>
*714&nbsp;&nbsp;&nbsp;Negative&nbsp;postings&nbsp;are&nbsp;currently&nbsp;not&nbsp;permitted&nbsp;for&nbsp;company&nbsp;code&nbsp;&amp;<br/>
<br/>
*GUI&nbsp;Texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;ST2&nbsp;--&gt;&nbsp;Irrelevante&nbsp;Kontobewegungen&nbsp;laut&nbsp;spezifizierter&nbsp;Belegarten<br/>
<br/>
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;001&nbsp;Special&nbsp;Selections<br/>
*&nbsp;005&nbsp;to<br/>
*&nbsp;052&nbsp;Trial&nbsp;Balance<br/>
*&nbsp;054&nbsp;Number&nbsp;of&nbsp;Accounts:<br/>
*&nbsp;060&nbsp;Options<br/>
*&nbsp;063&nbsp;Variants<br/>
*&nbsp;098&nbsp;Time&nbsp;Frame<br/>
*&nbsp;099&nbsp;Program<br/>
*&nbsp;100&nbsp;Date<br/>
*&nbsp;101&nbsp;User<br/>
*&nbsp;102&nbsp;Title<br/>
*&nbsp;106&nbsp;Accum.&nbsp;Value&nbsp;-&nbsp;D<br/>
*&nbsp;107&nbsp;Accum.&nbsp;Value&nbsp;-&nbsp;C<br/>
*&nbsp;108&nbsp;Cl.&nbsp;Bal.<br/>
*&nbsp;300&nbsp;Period<br/>
*&nbsp;301&nbsp;C<br/>
*&nbsp;302&nbsp;D<br/>
*&nbsp;303&nbsp;Beg.&nbsp;Balance<br/>
*&nbsp;901&nbsp;G/L&nbsp;Acct<br/>
*&nbsp;902&nbsp;Name<br/>
*&nbsp;903&nbsp;Debit&nbsp;(Last&nbsp;Period)<br/>
*&nbsp;904&nbsp;Credit&nbsp;(Last&nbsp;Period)<br/>
*&nbsp;905&nbsp;Total&nbsp;Debits<br/>
*&nbsp;906&nbsp;Total&nbsp;Credits<br/>
*&nbsp;907&nbsp;Crcy<br/>
*&nbsp;995&nbsp;***&nbsp;No&nbsp;disregarded&nbsp;documents&nbsp;found&nbsp;*********************************<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;DOCTYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disregard&nbsp;Document&nbsp;Type<br/>
*&nbsp;PERIOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fiscal&nbsp;Period<br/>
*&nbsp;P_COLOPT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optimize&nbsp;Columns<br/>
*&nbsp;P_VARI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Variant<br/>
*&nbsp;P_ZEBRA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Striped&nbsp;Pattern<br/>
*&nbsp;WITHOUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Accounts&nbsp;w/o&nbsp;Any&nbsp;Transactions<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;ICC-KR<br/>
*500&nbsp;&nbsp;&nbsp;No&nbsp;entries&nbsp;were&nbsp;found&nbsp;for&nbsp;the&nbsp;selections&nbsp;entered<br/>
*712&nbsp;&nbsp;&nbsp;Only&nbsp;period&nbsp;values&nbsp;between&nbsp;1&nbsp;and&nbsp;16&nbsp;are&nbsp;allowed<br/>
*713&nbsp;&nbsp;&nbsp;Enter&nbsp;one&nbsp;company&nbsp;code&nbsp;only<br/>
*714&nbsp;&nbsp;&nbsp;Negative&nbsp;postings&nbsp;are&nbsp;currently&nbsp;not&nbsp;permitted&nbsp;for&nbsp;company&nbsp;code&nbsp;&amp;<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>