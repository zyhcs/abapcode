<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFICO_GRPTOP</h2>
<h3> Description: 余额表导出</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZFICO_GRP.                    "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZFICO_GRPD...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
*&nbsp;INCLUDE&nbsp;LZFICO_GRPD...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
<br/>
*&nbsp;INCLUDE&nbsp;LZFICO_GRPD...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
<div class="code">
TYPE-POOLS ole2.<br/>
<br/>
</div>
<div class="codeComment">
*FOR&nbsp;YEXCEL_DISPLAY_OLE<br/>
<br/>
</div>
<div class="code">
INCLUDE ole2incl.<br/>
DATA: g_excel_obj    TYPE ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_workbook_obj&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_sheet_obj&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_cell_obj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;column_obj&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;range_obj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_borders_obj&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;button_obj&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_int_obj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_font_obj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_row_obj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object.<br/>
<br/>
DATA: g_objid       LIKE wwwdata-objid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_relid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;wwwdata-relid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_destination&nbsp;LIKE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_fname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_visible,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_save,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_sheet_name&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_fname1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string.<br/>
<br/>
DATA: g_string TYPE string.<br/>
<br/>
DATA: i_insert1 LIKE zcrs_alsmex_tabline OCCURS 0 WITH HEADER LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_insert2&nbsp;LIKE&nbsp;zcrs_excel_block_01&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_insert3&nbsp;LIKE&nbsp;zcrs_alsmex_tabline&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_copy&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zcrs_excel_block_01&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_coal&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zcrs_excel_block_01&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_merge&nbsp;&nbsp;&nbsp;LIKE&nbsp;zcrs_excel_block_01&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_left&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;zcrs_excel_block_01&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sheets&nbsp;like&nbsp;zcrs_excel_sheets_name&nbsp;occurs&nbsp;0&nbsp;with&nbsp;header&nbsp;line.<br/>
</div>
<div class="code">
DATA: i_data LIKE zcrs_alsmex_tabline1 OCCURS 0 WITH HEADER LINE.<br/>
<br/>
DATA: g_macro TYPE char100.<br/>
<br/>
DATA: g_object_key          LIKE bapibds01-objkey,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_prop_value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;bapisignat-prop_value,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_brow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_bcol&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_mcoal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;dd03d-fieldname,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_show&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_fit_rows&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char256,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_macro_sheets&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char256,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_macro_sheets_delete&nbsp;TYPE&nbsp;char256,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_delete_sheetname&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char256.<br/>
<br/>
include <a href="zfico_grpf01.html">zfico_grpf01</a>.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>