<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFICOG005F01</h2>
<h3> Description: Include LZFICOG005F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
***INCLUDE&nbsp;LZFICOG005F01.<br/>
*----------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_get_month_lastdate<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;&lt;LS_HEADER&gt;_BLDAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_month_lastdate USING iv_year iv_mon CHANGING cv_last_date.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_mon&nbsp;TYPE&nbsp;n&nbsp;LENGTH&nbsp;2.<br/>
&nbsp;&nbsp;lv_mon&nbsp;=&nbsp;iv_mon.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_date&nbsp;TYPE&nbsp;dats.<br/>
&nbsp;&nbsp;lv_date&nbsp;=&nbsp;iv_year&nbsp;&amp;&amp;&nbsp;lv_mon&nbsp;&amp;&amp;&nbsp;'01'.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BKK_GET_MONTH_LASTDAY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_date&nbsp;=&nbsp;lv_date<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_date&nbsp;=&nbsp;lv_date.<br/>
&nbsp;&nbsp;cv_last_date&nbsp;&nbsp;=&nbsp;zcl_assist01=&gt;conv_date_int_to_ext(&nbsp;lv_date&nbsp;).<br/>
<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>