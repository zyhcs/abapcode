<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_MM012_PURCHASEREQUEST</h2>
<h3> Description: 取采购审批相关信息（ERP-OA）</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_mm012_purchaserequest .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(IV_BANFN) TYPE  BANFN<br/>
*"     VALUE(IV_UUID) TYPE  SYSUUID_C32 OPTIONAL<br/>
*"     VALUE(IT_EBAN) TYPE  ZMMT_PRITEM<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_mm012_purchaserequest.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:"LI_PROXY&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;ZPICO_SI_ERP_MM012_PURCHASEREQ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output&nbsp;TYPE&nbsp;zpimt_erp_mm012_purchasereque1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_input&nbsp;&nbsp;TYPE&nbsp;zpimt_erp_mm012_purchasereques.<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_item&nbsp;&nbsp;TYPE&nbsp;zpidt_erp_mm012_purchaser_tab1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item&nbsp;&nbsp;TYPE&nbsp;zpidt_erp_mm012_purchasereque5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_t006a&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;t006a&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;DATA:ls_eban&nbsp;TYPE&nbsp;zmms_pritem&nbsp;.<br/>
&nbsp;&nbsp;DATA:lv_key(50)&nbsp;TYPE&nbsp;c.<br/>
</div>
<div class="codeComment">
*  DATA:LT_IF_ITEMS TYPE  ZPIDT_ERP_MM012_PURCHASER_TAB2,<br/>
*       LS_IF_ITEMS TYPE  ZPIDT_ERP_MM012_PURCHASEREQUE6,<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;it_in_items&nbsp;TYPE&nbsp;zt_items,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_in_items&nbsp;TYPE&nbsp;zspr_items.<br/>
&nbsp;&nbsp;DATA:lv_string&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_send_user&nbsp;TYPE&nbsp;bu_bpext.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;iv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_MM012_PURCHASEREQUEST'&nbsp;).<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*检查接口是否启用<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
</div>
<div class="codeComment">
*    LS_RETURN-TYPE = 'E' .<br/>
*    LS_RETURN-MESSAGE   = '接口未启用.'.<br/>
*    APPEND LS_RETURN TO RETURN  .<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;it_eban&nbsp;INTO&nbsp;ls_eban&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bpext&nbsp;INTO&nbsp;lv_send_user&nbsp;&nbsp;FROM&nbsp;but000&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;ls_eban-purps.<br/>
&nbsp;&nbsp;IF&nbsp;lv_send_user&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="codeComment">
*接口数据准备<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-workflowid&nbsp;=&nbsp;'304'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-banfn&nbsp;=&nbsp;iv_banfn&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_eban-zjj1&nbsp;EQ&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zjj1&nbsp;=&nbsp;'急'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zwzlb&nbsp;=&nbsp;ls_eban-zwzlb.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zxqlx&nbsp;=&nbsp;ls_eban-zxqlx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zwzlb&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zxqlx&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zcjz&nbsp;=&nbsp;ls_eban-zcjz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zbm&nbsp;=&nbsp;ls_eban-zbm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-header-zoazh&nbsp;=&nbsp;ls_eban-zoazh.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_eban&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;lt_t006a<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t006a<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;it_eban<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;msehi&nbsp;=&nbsp;it_eban-meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;spras&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_t006a&nbsp;BY&nbsp;msehi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_eban&nbsp;INTO&nbsp;ls_eban&nbsp;.<br/>
</div>
<div class="codeComment">
*    LS_ITEM-BANFN = LS_EBAN-BANFN .<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-item_key&nbsp;=&nbsp;ls_eban-bnfpo&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-knttp&nbsp;=&nbsp;ls_eban-knttp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-knttx&nbsp;=&nbsp;ls_eban-knttx&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-epstp&nbsp;=&nbsp;ls_eban-epstp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-ptext&nbsp;=&nbsp;ls_eban-ptext&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-matnr&nbsp;=&nbsp;ls_eban-matnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-maktx&nbsp;=&nbsp;ls_eban-maktx&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-menge&nbsp;=&nbsp;ls_eban-menge&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_t006a&nbsp;WITH&nbsp;KEY&nbsp;msehi&nbsp;=&nbsp;ls_eban-meins&nbsp;BINARY&nbsp;SEARCH.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-meins&nbsp;=&nbsp;lt_t006a-msehl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"LS_ITEM-MEINS&nbsp;=&nbsp;LS_EBAN-MEINS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-ekorg&nbsp;=&nbsp;ls_eban-ekorg&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-ekotx&nbsp;=&nbsp;ls_eban-ekotx&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-ekgrp&nbsp;=&nbsp;ls_eban-ekgrp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-eknam&nbsp;=&nbsp;ls_eban-eknam&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-werks&nbsp;=&nbsp;ls_eban-werks&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-name1&nbsp;=&nbsp;ls_eban-name1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_int_to_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datint&nbsp;&nbsp;&nbsp;=&nbsp;ls_eban-eeind<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datext&nbsp;&nbsp;&nbsp;=&nbsp;lv_string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"LS_ITEM-EEIND&nbsp;=&nbsp;LS_EBAN-EEIND&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-eeind&nbsp;=&nbsp;lv_string.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-matkl&nbsp;=&nbsp;ls_eban-matkl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-wgbez&nbsp;=&nbsp;ls_eban-wgbez&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zkbetr&nbsp;=&nbsp;ls_eban-zkbetr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-anln1&nbsp;=&nbsp;ls_eban-anln1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-txt50&nbsp;=&nbsp;ls_eban-txt50&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zsqr&nbsp;&nbsp;=&nbsp;ls_eban-zsqr&nbsp;.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_int_to_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datint&nbsp;&nbsp;&nbsp;=&nbsp;ls_eban-badat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datext&nbsp;&nbsp;&nbsp;=&nbsp;lv_string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-badat&nbsp;=&nbsp;lv_string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zprbz&nbsp;=&nbsp;ls_eban-zprbz.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_in_items&nbsp;=&nbsp;ls_eban-if_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_in_items&nbsp;INTO&nbsp;ls_in_items&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zmatnr&nbsp;=&nbsp;ls_in_items-zmatnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zmaktx&nbsp;=&nbsp;ls_in_items-zmaktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zmenge&nbsp;=&nbsp;ls_in_items-zmenge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zmeins&nbsp;=&nbsp;ls_in_items-zmeins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_item&nbsp;TO&nbsp;lt_item&nbsp;.<br/>
</div>
<div class="codeComment">
*      MOVE-CORRESPONDING LS_IN_ITEMS TO LS_IF_ITEMS .<br/>
*      APPEND LS_IF_ITEMS TO LT_IF_ITEMS .<br/>
*      CLEAR LS_IF_ITEMS .<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*    LS_ITEM-IF_ITEMS = LT_IF_ITEMS .<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_in_items&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_item&nbsp;TO&nbsp;lt_item&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;ls_item,lv_string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REFRESH:it_in_items.",LT_IF_ITEMS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_in_items.",LS_IF_ITEMS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-zdata-items&nbsp;=&nbsp;lt_item&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-bustyp&nbsp;=&nbsp;'MM012'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-messageid&nbsp;=&nbsp;iv_banfn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_erp_mm012_purchaserequest_r-message_header-send_user&nbsp;=&nbsp;lv_send_user."&nbsp;LS_EBAN-ZOAZH."SY-UNAME.<br/>
<br/>
</div>
<div class="codeComment">
*当ERP为发送方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;lv_message&nbsp;TYPE&nbsp;bapi_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;TYPE&nbsp;bapi_mtype.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA(lo_proxy)&nbsp;=&nbsp;NEW&nbsp;&nbsp;zpico_si_erp_mm012_purchasereq(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_proxy-&gt;si_erp_mm012_purchaserequest_s(&nbsp;EXPORTING&nbsp;output&nbsp;=&nbsp;ls_output<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;input&nbsp;=&nbsp;ls_input&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;lo_proxy&nbsp;).<br/>
<br/>
</div>
<div class="codeComment">
*当接口出现错误时,获取函数传入传出参数,以便重推<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;paras&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;get_func_paras(&nbsp;IMPORTING&nbsp;paras&nbsp;=&nbsp;DATA(lt_paras)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_paras&nbsp;&nbsp;ASSIGNING&nbsp;&nbsp;FIELD-SYMBOL(&lt;ls_paras&gt;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;(&lt;ls_paras&gt;-parameter)&nbsp;TO&nbsp;&lt;paras&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_paras&gt;-value&nbsp;=&nbsp;REF&nbsp;#(&nbsp;&lt;paras&gt;&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_func_msg(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;paras&nbsp;&nbsp;=&nbsp;lt_paras<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repush&nbsp;=&nbsp;'X'&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;&nbsp;ls_input-mt_erp_mm012_purchaserequest_r-zdata-header-header_status.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;&nbsp;ls_input-mt_erp_mm012_purchaserequest_r-zdata-header-header_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;'senduser为空,不能对采购申请进行一级审批，不推送OA'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;iv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msgtype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;lv_message<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;iv_banfn<br/>
&nbsp;&nbsp;key3&nbsp;=&nbsp;''&nbsp;).<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>