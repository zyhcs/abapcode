<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO028A</h2>
<h3>Description: 成本中心调整凭证</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BELNR</td>
<td>2</td>
<td>X</td>
<td>BELNR_D</td>
<td>BELNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>会计凭证号码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>GJAHR</td>
<td>3</td>
<td>X</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MONAT</td>
<td>4</td>
<td>&nbsp;</td>
<td>MONAT</td>
<td>MONAT</td>
<td>NUMC</td>
<td>2</td>
<td>&nbsp;</td>
<td>会计期间</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BUDAT</td>
<td>5</td>
<td>&nbsp;</td>
<td>BUDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>凭证中的过账日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>USNAM</td>
<td>6</td>
<td>&nbsp;</td>
<td>USNAM</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>用户名</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZTZPZ</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZTZPZ</td>
<td>CHAR250</td>
<td>CHAR</td>
<td>250</td>
<td>&nbsp;</td>
<td>调整凭证</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>BUKRS</td>
<td>8</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZCXPZ</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZCXPZ</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>冲销凭证</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZCXR</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZCXR</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>冲销人</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZCXRQ</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZCXRQ</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>冲销日期</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>