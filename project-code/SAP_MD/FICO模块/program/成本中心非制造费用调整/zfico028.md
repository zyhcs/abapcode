<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO028</h2>
<h3> Description: 成本中心非制造费用调整</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO028<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO028.<br/>
<br/>
<br/>
<br/>
include <a href="zfico028_data.html">ZFICO028_DATA</a>.<br/>
<br/>
include <a href="zfico028_from.html">ZFICO028_FROM</a>.<br/>
<br/>
INITIALIZATION.<br/>
<br/>
START-OF-SELECTION.<br/>
<br/>
if P_R1 = 'X'.<br/>
PERFORM FRM_GET_DATA.<br/>
ELSE.<br/>
PERFORM FRM_GET_DATA_CX.<br/>
ENDIF.<br/>
<br/>
<br/>
END-OF-SELECTION.<br/>
<br/>
IF P_R1 = 'X'.<br/>
PERFORM frm_display.<br/>
ELSE.<br/>
PERFORM frm_display_cx.<br/>
ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;P_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计年度<br/>
*&nbsp;P_POPER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计期间<br/>
*&nbsp;P_R1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凭证调整<br/>
*&nbsp;P_R2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;查询与冲销<br/>
*&nbsp;S_BELNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计凭证<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;选择的数据存在调整成功的数据<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>