<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO013B</h2>
<h3> Description: 自动清账03</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO013B<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;一键清账程序<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO013B.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFIR035A<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
<br/>
</div>
<div class="code">
TABLES: bseg,bsik,bsid,mkal,marc .<br/>
<br/>
TYPE-POOLS: slis.<br/>
</div>
<div class="codeComment">
*&amp;--------------------------------------------------------------------<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;INCLUDE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*INCLUDE&nbsp;zcom001.<br/>
*<br/>
*<br/>
*DATA:&nbsp;gs_gf_dbconame(20)&nbsp;TYPE&nbsp;c&nbsp;VALUE&nbsp;'ZDEMO01'.<br/>
*DATA:&nbsp;gs_gr_ex&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cx_root.<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;STRUCTURE<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TYPES: BEGIN OF gty_gts_data,"类型<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-werks,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;koart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-koart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-h_budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rebzg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-rebzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rebzz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-rebzz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zmsgtx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char100,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kotxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;char20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;umskz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_success(1)&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;bseg-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;bseg-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rebzg1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bseg-rebzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bsid-dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sno&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;i,<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gty_gts_data.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;DEFIEN&nbsp;INNER&nbsp;TABLE<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA: gt_data TYPE TABLE OF gty_gts_data WITH HEADER LINE."内表<br/>
DATA: gt_data1 TYPE TABLE OF gty_gts_data WITH HEADER LINE."内表<br/>
DATA: gs_data TYPE  gty_gts_data ."工作区<br/>
DATA: gt_post TYPE TABLE OF gty_gts_data WITH HEADER LINE."内表<br/>
DATA: gs_post TYPE gty_gts_data."工作区<br/>
<br/>
<br/>
<br/>
TYPES:BEGIN OF gty_tys_bsid,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;TYPE&nbsp;bsid-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;TYPE&nbsp;bsid-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;TYPE&nbsp;bsik-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;TYPE&nbsp;bsik-hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umskz&nbsp;&nbsp;TYPE&nbsp;bsid-umskz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;TYPE&nbsp;bsid-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buzei&nbsp;&nbsp;TYPE&nbsp;bsid-buzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shkzg&nbsp;&nbsp;TYPE&nbsp;bsid-shkzg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrbtr&nbsp;&nbsp;TYPE&nbsp;bsid-wrbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bldat&nbsp;&nbsp;TYPE&nbsp;bsid-bldat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;TYPE&nbsp;bsid-waers,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sno&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr1&nbsp;TYPE&nbsp;gjahr,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag&nbsp;&nbsp;&nbsp;TYPE&nbsp;char1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gty_tys_bsid.<br/>
DATA: gt_bsid TYPE TABLE OF  gty_tys_bsid WITH HEADER LINE.<br/>
DATA: gt_bsid1 TYPE TABLE OF  gty_tys_bsid WITH HEADER LINE.<br/>
DATA: gt_bsid2 TYPE TABLE OF  gty_tys_bsid WITH HEADER LINE.<br/>
DATA: gt_bsid3 TYPE TABLE OF  gty_tys_bsid WITH HEADER LINE.<br/>
DATA: gt_bsid_sum TYPE TABLE OF  gty_tys_bsid WITH HEADER LINE.<br/>
DATA: gs_bsid  LIKE  LINE OF gt_bsid.<br/>
DATA: gs_bsid_sum  LIKE  LINE OF gt_bsid.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;DEFIEN&nbsp;ALV<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA: gs_wcl_container TYPE REF TO cl_gui_docking_container,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_wcl_alv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ALV网格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_row_alv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid_base,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_exclude&nbsp;&nbsp;&nbsp;TYPE&nbsp;ui_functions,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用于去掉不要的菜单栏<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_index_columns&nbsp;TYPE&nbsp;lvc_t_col,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"选择列<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_fieldcat&nbsp;&nbsp;TYPE&nbsp;lvc_t_fcat,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;LINE&nbsp;OF&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_f4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_f4&nbsp;WITH&nbsp;HEADER&nbsp;LINE,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"搜索帮助<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_layout&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_layo&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"布局结构<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_variant&nbsp;&nbsp;&nbsp;TYPE&nbsp;disvariant,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"字段格式保存<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_sort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_sort,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用于排序<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_index&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_col,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"选择列<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_filt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_filt.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用于过滤<br/>
<br/>
<br/>
DATA: gs_gds_stable    TYPE lvc_s_stbl.                              "刷新稳定<br/>
DATA: gs_ok_code   TYPE sy-ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_save_code&nbsp;TYPE&nbsp;sy-ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_if_code&nbsp;&nbsp;&nbsp;TYPE&nbsp;sy-ucomm.<br/>
<br/>
DATA: gs_gdt_nu3_rows TYPE  lvc_t_row,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gdt_nu3_no&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;lvc_t_roid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_www_nu3_no&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;lvc_s_roid.<br/>
DATA:  gv_txt2  TYPE char100.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;包括&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class定义<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
CLASS lcl_event_handler DEFINITION.<br/>
&nbsp;&nbsp;PUBLIC&nbsp;SECTION.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;METHODS:<br/>
</div>
<div class="codeComment">
*--基于单元格的效验<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_data_changed&nbsp;FOR&nbsp;EVENT&nbsp;data_changed&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;er_data_changed&nbsp;e_onf4&nbsp;e_onf4_before&nbsp;e_onf4_after&nbsp;e_ucomm,<br/>
<br/>
</div>
<div class="codeComment">
*--在数据修改完成之后<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_data_changed_finished&nbsp;FOR&nbsp;EVENT&nbsp;data_changed_finished&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_modified&nbsp;et_good_cells&nbsp;,<br/>
</div>
<div class="codeComment">
*--按钮触发前事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_before_user_command&nbsp;FOR&nbsp;EVENT&nbsp;before_user_command&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_ucomm,<br/>
</div>
<div class="codeComment">
*--搜索帮助<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_onf4&nbsp;FOR&nbsp;EVENT&nbsp;onf4&nbsp;OF&nbsp;cl_gui_alv_grid&nbsp;IMPORTING&nbsp;e_fieldname&nbsp;es_row_no&nbsp;er_event_data,<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;双击<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_double_click&nbsp;FOR&nbsp;EVENT&nbsp;double_click&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_row&nbsp;e_column&nbsp;es_row_no.<br/>
<br/>
ENDCLASS.                    "lcl_event_han<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;包括&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class实例化<br/>
*----------------------------------------------------------------------*<br/>
*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
CLASS lcl_event_handler IMPLEMENTATION.<br/>
&nbsp;&nbsp;METHOD&nbsp;handle_data_changed.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单元格事件触发相关<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_handle_data_changed&nbsp;USING&nbsp;er_data_changed.<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handle_data_changed<br/>
<br/>
</div>
<div class="codeComment">
*--Handle&nbsp;data&nbsp;changed&nbsp;finished<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_data_changed_finished.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_handle_data_changed_finish&nbsp;USING&nbsp;e_modified&nbsp;et_good_cells&nbsp;.<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handle_data_changed_finished<br/>
<br/>
</div>
<div class="codeComment">
*--按钮触发前事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_before_user_command&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_handle_before_user_command&nbsp;USING&nbsp;e_ucomm&nbsp;.<br/>
&nbsp;&nbsp;ENDMETHOD&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handl_before_user_command<br/>
<br/>
</div>
<div class="codeComment">
*--搜索帮助<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_onf4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_handle_onf4&nbsp;USING&nbsp;e_fieldname&nbsp;es_row_no&nbsp;er_event_data.<br/>
&nbsp;&nbsp;ENDMETHOD&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handl_before_user_command<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;双击<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_double_click.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_handle_double_click&nbsp;USING&nbsp;e_row&nbsp;e_column&nbsp;es_row_no.<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"handle_double_click<br/>
<br/>
<br/>
ENDCLASS.                    "lcl_event_handler IMPLEMENTATION<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;THE&nbsp;MACROS/宏<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
DEFINE df_set_fieldcat.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gs_fieldcat&nbsp;.<br/>
&nbsp;&nbsp;gs_fieldcat-fieldname&nbsp;=&nbsp;&amp;1."内表的字段<br/>
&nbsp;&nbsp;gs_fieldcat-outputlen&nbsp;=&nbsp;&amp;2."输出长度<br/>
&nbsp;&nbsp;gs_fieldcat-scrtext_l&nbsp;=&nbsp;&amp;3."列标题<br/>
&nbsp;&nbsp;gs_fieldcat-no_zero&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4."关键字列<br/>
&nbsp;&nbsp;gs_fieldcat-emphasize&nbsp;=&nbsp;&amp;5."列颜色<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_fieldcat-checkbox&nbsp;&nbsp;=&nbsp;&amp;5&nbsp;.&nbsp;&nbsp;"作为复选框<br/>
</div>
<div class="code">
&nbsp;&nbsp;gs_fieldcat-edit_mask&nbsp;=&nbsp;&amp;6."转换例程<br/>
&nbsp;&nbsp;gs_fieldcat-ref_table&nbsp;=&nbsp;&amp;7."参照表<br/>
&nbsp;&nbsp;gs_fieldcat-ref_field&nbsp;=&nbsp;&amp;8."参照字段<br/>
&nbsp;&nbsp;gs_fieldcat-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9."可编辑<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_fieldcat-do_sum&nbsp;&nbsp;&nbsp;=&nbsp;.&nbsp;&nbsp;"汇总<br/>
*&nbsp;&nbsp;gs_fieldcat-no_zero&nbsp;&nbsp;=&nbsp;.&nbsp;&nbsp;"不输出0<br/>
<br/>
*&nbsp;&nbsp;gs_fieldcat-style&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;.&nbsp;&nbsp;"样式<br/>
*&nbsp;&nbsp;gs_fieldcat-fix_column&nbsp;=&nbsp;."固定列<br/>
</div>
<div class="code">
&nbsp;&nbsp;APPEND&nbsp;gs_fieldcat&nbsp;TO&nbsp;gs_gdt_fieldcat[]&nbsp;.<br/>
END-OF-DEFINITION.<br/>
<br/>
<br/>
FORM frm_display_alv .<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;gs_wcl_alv&nbsp;IS&nbsp;INITIAL&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ALV对象如果为空，则生成对象，把ALV放入容器中<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;gs_wcl_container<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'9000'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;extension&nbsp;=&nbsp;2050<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;side&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cl_gui_docking_container=&gt;property_floating.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;gs_wcl_alv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_parent&nbsp;=&nbsp;gs_wcl_container.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_build_fieldcat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;gs_gdt_fieldcat.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"获取字段格式<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_exclude_tb_functions&nbsp;CHANGING&nbsp;gs_gdt_exclude.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"去掉不用的菜单按钮<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_prepare_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;gs_gds_layout&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"获取样式<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_eventload.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"读取事件（总）<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_adjust_edittables&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"调整可更改的单元格<br/>
<br/>
</div>
<div class="codeComment">
*-----显示ALV-------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_gds_variant.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_variant&nbsp;=&nbsp;sy-repid.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gs_wcl_alv-&gt;set_table_for_first_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_gds_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_toolbar_excluding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_gdt_exclude<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_variant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_gds_variant<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_data[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcatalog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_gdt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;invalid_parameter_combination&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;too_many_lines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
</div>
<div class="codeComment">
*----------------------------刷新ALV----------------------<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_refurbish_alv.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"刷新ALV<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;BUILD_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;GDT_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
FORM frm_build_fieldcat  CHANGING p_gdt_fieldcat.<br/>
&nbsp;&nbsp;REFRESH:gs_gdt_fieldcat[].<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'BUKRS'&nbsp;''&nbsp;&nbsp;'公司代码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'KOART'&nbsp;''&nbsp;&nbsp;'账户类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'KOTXT'&nbsp;''&nbsp;&nbsp;'账户类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'LIFNR'&nbsp;''&nbsp;&nbsp;'供应商编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'KUNNR'&nbsp;''&nbsp;&nbsp;'客户编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'BUDAT'&nbsp;''&nbsp;&nbsp;'过账日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZG'&nbsp;''&nbsp;&nbsp;'原凭证编号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZZ'&nbsp;''&nbsp;&nbsp;'原凭证编号行项目'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'BELNR'&nbsp;''&nbsp;&nbsp;'清账凭证'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'DMBTR'&nbsp;''&nbsp;&nbsp;'剩余金额'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZG1'&nbsp;''&nbsp;&nbsp;'参考发票号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'IS_SUCCESS'&nbsp;''&nbsp;'是否成功'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;df_set_fieldcat&nbsp;'ZMSGTX'&nbsp;''&nbsp;'消息'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;EXCLUDE_TB_FUNCTIONS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;GDT_EXCLUDE<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_exclude_tb_functions CHANGING p_gdt_exclude TYPE ui_functions.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_exclude&nbsp;TYPE&nbsp;ui_func.<br/>
<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_append_row&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_copy&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;LS_EXCLUDE&nbsp;=&nbsp;CL_GUI_ALV_GRID=&gt;MC_FC_LOC_COPY_ROW&nbsp;.<br/>
*&nbsp;&nbsp;APPEND&nbsp;LS_EXCLUDE&nbsp;TO&nbsp;GDT_EXCLUDE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_cut&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_insert_row&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_delete_row&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_move_row&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_paste&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_paste_new_row&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
&nbsp;&nbsp;ls_exclude&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_fc_loc_undo&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;ls_exclude&nbsp;TO&nbsp;gs_gdt_exclude.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;PREPARE_LAYOUT_9001<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;获取显示状态<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GDS_LAYOUT&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_prepare_layout  CHANGING p_gds_layout TYPE lvc_s_layo.<br/>
<br/>
&nbsp;&nbsp;p_gds_layout-cwidth_opt&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"最优化宽度<br/>
&nbsp;&nbsp;p_gds_layout-sel_mode&nbsp;=&nbsp;'D'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"选择方式<br/>
&nbsp;&nbsp;p_gds_layout-zebra&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"间隔颜色<br/>
&nbsp;&nbsp;p_gds_layout-stylefname&nbsp;=&nbsp;'CELLSTYLES'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"自动可编辑<br/>
<br/>
ENDFORM.                    " PREPARE_LAYOUT<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;EVENTLOAD<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;事件加载<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_eventload .<br/>
&nbsp;&nbsp;DATA&nbsp;ls_gr_event_handler&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;lcl_event_handler&nbsp;.&nbsp;&nbsp;"事件响应<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;ls_gr_event_handler&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;GR_EVENT_HANDLER-&gt;HANDLE_TOOLBAR&nbsp;FOR&nbsp;WCL_ALV&nbsp;.<br/>
<br/>
*-----回车触发单元格校验事件<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gs_wcl_alv-&gt;register_edit_event<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_event_id&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_evt_enter.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;ls_gr_event_handler-&gt;handle_data_changed&nbsp;FOR&nbsp;gs_wcl_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;ls_gr_event_handler-&gt;handle_data_changed_finished&nbsp;FOR&nbsp;gs_wcl_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;ls_gr_event_handler-&gt;handle_double_click&nbsp;FOR&nbsp;gs_wcl_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;ls_gr_event_handler-&gt;handle_before_user_command&nbsp;FOR&nbsp;gs_wcl_alv.<br/>
&nbsp;&nbsp;SET&nbsp;HANDLER&nbsp;ls_gr_event_handler-&gt;handle_onf4&nbsp;FOR&nbsp;gs_wcl_alv.<br/>
<br/>
<br/>
ENDFORM.                    " EVENTLOAD<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;ADJUST_EDITTABLES<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_adjust_edittables .<br/>
&nbsp;&nbsp;DATA&nbsp;ls_lds_stylerow&nbsp;TYPE&nbsp;lvc_s_styl.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_ldt_styletab&nbsp;TYPE&nbsp;lvc_t_styl.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;loop&nbsp;&nbsp;at&nbsp;gdt_data&nbsp;into&nbsp;gds_data.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;clear:&nbsp;&nbsp;lds_stylerow.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;clear:&nbsp;&nbsp;ldt_styletab.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;gds_data-class&nbsp;is&nbsp;not&nbsp;initial.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lds_stylerow-fieldname&nbsp;=&nbsp;'class'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lds_stylerow-style&nbsp;=&nbsp;cl_gui_alv_grid=&gt;mc_style_disabled&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;insert&nbsp;lds_stylerow&nbsp;into&nbsp;table&nbsp;ldt_styletab.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;endif.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;gds_data-cellstyles&nbsp;=&nbsp;&nbsp;ldt_styletab&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;modify&nbsp;gdt_data&nbsp;from&nbsp;gds_data.<br/>
*&nbsp;&nbsp;endloop.<br/>
</div>
<div class="code">
ENDFORM.                    " ADJUST_EDITTABLES<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;REFURBISH_ALV<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刷新ALV<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_refurbish_alv.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_adjust_edittables&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;调整可更改的单元格<br/>
</div>
<div class="codeComment">
*----------------------------刷新ALV----------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;gs_gds_stable-row&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_gds_stable-col&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gs_wcl_alv-&gt;refresh_table_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_stable&nbsp;=&nbsp;gs_gds_stable<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;finished&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
ENDFORM.                    "refurbish_ALV<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;HANDLE_DATA_CHANGED<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数据更新事件<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;IR_DATA_CHANGED&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_handle_data_changed USING p_ir_data_changed<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_alv_changed_data_protocol.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_mod_cell&nbsp;TYPE&nbsp;lvc_s_modi,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_mod_cell&nbsp;TYPE&nbsp;lvc_t_modi,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_value&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_value.<br/>
<br/>
ENDFORM.                    "HANDLE_DATA_CHANGED<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;handle_data_CHANGED_finished<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;IR_DATA_CHANGED&nbsp;&nbsp;BOM层级<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_handle_data_changed_finish USING p_e_modified    TYPE char01<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_et_good_cells&nbsp;TYPE&nbsp;lvc_t_modi.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_es_good_cells&nbsp;TYPE&nbsp;lvc_s_modi.<br/>
<br/>
ENDFORM.                    "handle_data_CHANGED_finished<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;HANDLE_DOUBLE_CLICK<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;双击事件<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;I_ROW&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;I_COLUMN&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;IS_ROW_NO&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_handle_double_click USING p_gdt_row   TYPE lvc_s_row<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_gdt_column&nbsp;&nbsp;TYPE&nbsp;lvc_s_col<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_is_row_no&nbsp;TYPE&nbsp;lvc_s_roid.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_stylerow&nbsp;TYPE&nbsp;lvc_s_styl.<br/>
&nbsp;&nbsp;IF&nbsp;p_gdt_column-fieldname&nbsp;=&nbsp;'BELNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;&nbsp;INDEX&nbsp;p_is_row_no-row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;AND&nbsp;gt_data-belnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BLN'&nbsp;FIELD&nbsp;gt_data-belnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BUK'&nbsp;FIELD&nbsp;gt_data-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'GJR'&nbsp;FIELD&nbsp;gt_data-gjahr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'FB03'&nbsp;WITH&nbsp;AUTHORITY-CHECK&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;p_gdt_column-fieldname&nbsp;=&nbsp;'REBZG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;&nbsp;INDEX&nbsp;p_is_row_no-row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;AND&nbsp;gt_data-rebzg&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BLN'&nbsp;FIELD&nbsp;gt_data-rebzg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'BUK'&nbsp;FIELD&nbsp;gt_data-bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'GJR'&nbsp;FIELD&nbsp;gt_data-gjahr1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'FB03'&nbsp;WITH&nbsp;AUTHORITY-CHECK&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.                    "handle_double_click<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;HANDLE_BEFORE_USER_COMMAND<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按钮事件<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_E_UCOMM&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_handle_before_user_command  USING    p_e_ucomm.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;p_e_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;REFRESH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_refurbish_alv.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"刷新ALV<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDFORM.                    " HANDLE_BEFORE_USER_COMMAND<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;HANDLE_ONF4<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_handle_onf4 USING  p_e_fieldname TYPE lvc_fname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_es_row_no&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_roid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_er_event_data&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_alv_event_data.<br/>
<br/>
<br/>
ENDFORM.                    "HANDLE_ONF4<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;BDC部分的定义<br/>
</div>
<div class="code">
DATA: gt_bdcdata1    TYPE TABLE OF bdcdata WITH HEADER LINE ,        "BDC 数据内表<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BDCDATA&nbsp;TYPE&nbsp;BDCDATA,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_gdt_bdcmsg1&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bdcmsgcoll,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"BDC&nbsp;消息内表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_bdcmsg1&nbsp;TYPE&nbsp;bdcmsgcoll,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_gdt_return&nbsp;&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;bapiret2,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"文字消息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_return&nbsp;&nbsp;LIKE&nbsp;bapiret2,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"文字消息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_gds_options&nbsp;TYPE&nbsp;ctu_params.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;选择界面<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK d2 WITH FRAME TITLE TEXT-002.<br/>
PARAMETERS: p_01 RADIOBUTTON GROUP g1 USER-COMMAND fcode DEFAULT 'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_02&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1.<br/>
SELECTION-SCREEN END OF BLOCK d2.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK dl WITH FRAME TITLE TEXT-001.<br/>
SELECT-OPTIONS:  s_bukrs FOR bseg-bukrs  .<br/>
</div>
<div class="codeComment">
*PARAMETERS:&nbsp;&nbsp;S_MBLNR(50)&nbsp;TYPE&nbsp;&nbsp;C.<br/>
</div>
<div class="code">
SELECT-OPTIONS: s_lifnr FOR bsik-lifnr  MODIF ID gys.<br/>
SELECT-OPTIONS: s_kunnr FOR bsid-kunnr  MODIF ID kh.<br/>
PARAMETERS : p_budat TYPE  sy-datum  .<br/>
</div>
<div class="codeComment">
*SELECT-OPTIONS:&nbsp;s_umskz&nbsp;FOR&nbsp;bsid-umskz&nbsp;&nbsp;NO&nbsp;INTERVALS.<br/>
*SELECT-OPTIONS:&nbsp;s_waers&nbsp;FOR&nbsp;bsid-waers&nbsp;&nbsp;NO&nbsp;INTERVALS.<br/>
*PARAMETERS:&nbsp;&nbsp;p_01&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1,&nbsp;p_02&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1.<br/>
</div>
<div class="code">
SELECTION-SCREEN END OF BLOCK dl.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK c WITH FRAME TITLE TEXT-003.<br/>
<br/>
PARAMETERS:<br/>
&nbsp;&nbsp;p_moda&nbsp;TYPE&nbsp;c&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;gr2,<br/>
&nbsp;&nbsp;p_modn&nbsp;TYPE&nbsp;c&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;gr2&nbsp;&nbsp;DEFAULT&nbsp;'X',<br/>
&nbsp;&nbsp;p_mode&nbsp;TYPE&nbsp;c&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;gr2.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK c.<br/>
<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;INITIALIZATION<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
INITIALIZATION.<br/>
<br/>
</div>
<div class="codeComment">
**&amp;--------------------------------------------------------------------*<br/>
**&nbsp;AT&nbsp;SELECTION-SCREEN&nbsp;OUTPUT<br/>
**&amp;--------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN OUTPUT.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_01&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'GYS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_02&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'KH'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;AT&nbsp;SELECTION-SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
&nbsp;&nbsp;CASE&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'UC1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ONLI'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"F8<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;space.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"回车<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;START-OF-SELECTION<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'BUKRS'&nbsp;''&nbsp;&nbsp;'公司代码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'KOART'&nbsp;''&nbsp;&nbsp;'账户类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'KOTXT'&nbsp;''&nbsp;&nbsp;'账户类型'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'LIFNR'&nbsp;''&nbsp;&nbsp;'供应商编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'KUNNR'&nbsp;''&nbsp;&nbsp;'客户编码'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'BUDAT'&nbsp;''&nbsp;&nbsp;'过账日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZG'&nbsp;''&nbsp;&nbsp;'原凭证编号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZZ'&nbsp;''&nbsp;&nbsp;'原凭证编号行项目'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'BELNR'&nbsp;''&nbsp;&nbsp;'清账凭证'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'DMBTR'&nbsp;''&nbsp;&nbsp;'剩余金额'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'REBZG1'&nbsp;''&nbsp;&nbsp;'参考发票号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'IS_SUCCESS'&nbsp;''&nbsp;'是否成功'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
*&nbsp;&nbsp;df_set_fieldcat&nbsp;'ZMSGTX'&nbsp;''&nbsp;'消息'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_check_run.<br/>
&nbsp;&nbsp;IF&nbsp;p_budat&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_budat&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;IF&nbsp;sy-batch&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WRITE:/&nbsp;gv_txt2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WRITE:&nbsp;/&nbsp;'公司:',gt_data-bukrs,'账户类型:',gt_data-koart,'供应商编码:',gt_data-lifnr,'客户编码:',gt_data-kunnr,'过账日期:',gt_data-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'原凭证编号:',gt_data-rebzg,'原凭证编号行项目:',gt_data-rebzz,'清账凭证:',gt_data-belnr,'剩余金额:',gt_data-dmbtr,'参考发票号:',gt_data-rebzg1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'是否成功:',gt_data-is_success,&nbsp;'消息:',gt_data-zmsgtx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;9000.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;END-OF-SELECTION<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
END-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_9000&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_9000 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STANDARD'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'9000'.<br/>
</div>
<div class="code">
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;DISPLAY_ALV&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE display_alv OUTPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_display_alv.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_9000&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE user_command_9000 INPUT.<br/>
&nbsp;&nbsp;gs_save_code&nbsp;=&nbsp;gs_ok_code.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gs_ok_code.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;gs_save_code.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'BACK'&nbsp;OR&nbsp;'&amp;F03'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;'0'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'EXIT'&nbsp;OR&nbsp;'CANCLE'&nbsp;OR&nbsp;'&amp;F15'&nbsp;OR&nbsp;'&amp;F12'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gs_wcl_alv-&gt;set_function_code<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c_ucomm&nbsp;=&nbsp;gs_save_code.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDMODULE.<br/>
<br/>
include <a href="zfico013b_f01.html">zfico013b_f01</a>.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户清账<br/>
*&nbsp;P_02&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商清账<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;P_MODA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;显示步骤<br/>
*&nbsp;P_MODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;显示错误<br/>
*&nbsp;P_MODN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;不显示<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户编码<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商编码<br/>
*&nbsp;S_WAERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;货币<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;OO<br/>
*000&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户清账<br/>
*&nbsp;P_02&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商清账<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;P_MODA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;显示步骤<br/>
*&nbsp;P_MODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;显示错误<br/>
*&nbsp;P_MODN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;不显示<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户编码<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商编码<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;OO<br/>
*000&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>