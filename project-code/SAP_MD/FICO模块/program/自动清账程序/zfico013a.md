<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO013A</h2>
<h3> Description: 自动清账程序02</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO013A<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;清账程序<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO013A.<br/>
<br/>
include <a href="zfir013a_top.html">zfir013a_top</a>.<br/>
<br/>
include <a href="zfir013a_c01.html">zfir013a_c01</a>.<br/>
<br/>
include <a href="zfir013a_sel.html">zfir013a_sel</a>.<br/>
<br/>
include <a href="zfir013a_f01.html">zfir013a_f01</a>.<br/>
<br/>
include <a href="zfir013a_v01.html">zfir013a_v01</a>.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户清账<br/>
*&nbsp;P2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商清账<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;S_BUKRS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户编码<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商编码<br/>
*&nbsp;S_UMSKZ&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*YPE<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;请勾选需要核销的数据！<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;MESSTAB-MSGID<br/>
*MES<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;OO<br/>
*000&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户清账<br/>
*&nbsp;P2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商清账<br/>
*&nbsp;P_BUDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;S_BUKRS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户编码<br/>
*&nbsp;S_LIFNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商编码<br/>
*&nbsp;S_UMSKZ&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;请勾选需要核销的数据！<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;OO<br/>
*000&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>