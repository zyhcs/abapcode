<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFIR013A_SEL</h2>
<h3> Description: Include ZFIR013A_SEL</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFIR013A_SEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
<br/>
</div>
<div class="code">
TABLES : bseg.<br/>
TYPE-POOLS: slis.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE TEXT-002.<br/>
PARAMETERS: p1 RADIOBUTTON GROUP g1 USER-COMMAND mx DEFAULT 'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;g1.<br/>
SELECTION-SCREEN END OF BLOCK b2.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
SELECT-OPTIONS: s_bukrs FOR bseg-bukrs  .<br/>
SELECT-OPTIONS: s_lifnr FOR bseg-lifnr  MODIF ID c1.<br/>
SELECT-OPTIONS: s_kunnr FOR bseg-kunnr  MODIF ID c2.<br/>
SELECT-OPTIONS: s_umskz FOR bseg-umskz .<br/>
PARAMETERS : p_budat TYPE  sy-datum DEFAULT sy-datum .<br/>
<br/>
SELECTION-SCREEN END OF BLOCK b1.<br/>
<br/>
INITIALIZATION .<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'C1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'C2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
START-OF-SELECTION.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_run.&nbsp;&nbsp;"检验是否有用户在使用此程序<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_alv_display&nbsp;.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>