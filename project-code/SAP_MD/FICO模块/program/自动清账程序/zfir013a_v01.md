<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFIR013A_V01</h2>
<h3> Description: Include ZFIR013A_V01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFIR013A_V01<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_ALV_DISPLAY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_alv_display .<br/>
<br/>
&nbsp;&nbsp;gs_layout-zebra&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-sel_mode&nbsp;=&nbsp;'D'.<br/>
&nbsp;&nbsp;gs_layout-cwidth_opt&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fieldcat_init.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'ST'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'FRM_USER_COMMAND&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_grid_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_title<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_event<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_tab.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_fieldcat_init .<br/>
</div>
<div class="codeComment">
***************************&nbsp;列位置&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;列字段&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;列描述&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;主键&nbsp;&nbsp;&nbsp;&nbsp;复选框&nbsp;&nbsp;&nbsp;&nbsp;编辑可能<br/>
***************************&nbsp;col_pos&nbsp;&nbsp;fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;seltext_l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;outputlen&nbsp;&nbsp;&nbsp;&nbsp;emphasize&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;decimals&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;edit1<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;p1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;gs_fieldcat&nbsp;USING&nbsp;lv_pos&nbsp;&nbsp;&nbsp;'KUNNR'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'客户编码'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'10'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;gs_fieldcat&nbsp;USING&nbsp;lv_pos&nbsp;&nbsp;&nbsp;'NAME2'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'客户名称'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'35'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;gs_fieldcat&nbsp;USING&nbsp;lv_pos&nbsp;&nbsp;&nbsp;'LIFNR'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'供应商编码'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'10'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;gs_fieldcat&nbsp;USING&nbsp;lv_pos&nbsp;&nbsp;&nbsp;'NAME1'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'供应商名称'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'35'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM .<br/>
<br/>
FORM gs_fieldcat USING  col_pos  fieldname seltext_l    outputlen  emphasize  sum  decimals  key key1  edit1.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;gs_fieldcat.<br/>
&nbsp;&nbsp;gs_fieldcat-col_pos&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;col_pos.&nbsp;&nbsp;&nbsp;"列位置<br/>
&nbsp;&nbsp;gs_fieldcat-fieldname&nbsp;=&nbsp;fieldname.&nbsp;&nbsp;"列字段<br/>
&nbsp;&nbsp;gs_fieldcat-coltext&nbsp;&nbsp;&nbsp;=&nbsp;seltext_l.&nbsp;&nbsp;"列描述<br/>
&nbsp;&nbsp;gs_fieldcat-outputlen&nbsp;=&nbsp;outputlen.&nbsp;&nbsp;"长度<br/>
&nbsp;&nbsp;gs_fieldcat-emphasize&nbsp;=&nbsp;emphasize.&nbsp;&nbsp;"颜色<br/>
&nbsp;&nbsp;gs_fieldcat-do_sum&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sum.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"汇总<br/>
&nbsp;&nbsp;gs_fieldcat-decimals&nbsp;&nbsp;=&nbsp;decimals.&nbsp;&nbsp;&nbsp;"小数<br/>
&nbsp;&nbsp;gs_fieldcat-key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;key.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设置关键字<br/>
&nbsp;&nbsp;gs_fieldcat-checkbox&nbsp;&nbsp;=&nbsp;key1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"复选框<br/>
&nbsp;&nbsp;gs_fieldcat-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;edit1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"编辑可能<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;gs_fieldcat&nbsp;TO&nbsp;gt_fieldcat.<br/>
&nbsp;&nbsp;lv_pos&nbsp;=&nbsp;lv_pos&nbsp;+&nbsp;1.<br/>
<br/>
ENDFORM.<br/>
<br/>
FORM frm_user_command USING i_ucomm TYPE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
&nbsp;&nbsp;CASE&nbsp;i_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_head-zhxje&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_tab&nbsp;INTO&nbsp;ls_tab&nbsp;INDEX&nbsp;is_selfield-tabindex&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_append_data_p1&nbsp;USING&nbsp;ls_tab-kunnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;0100.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;p2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_tab&nbsp;INTO&nbsp;ls_tab&nbsp;INDEX&nbsp;is_selfield-tabindex&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_append_data_p2&nbsp;USING&nbsp;ls_tab-lifnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;SCREEN&nbsp;0100.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
&nbsp;&nbsp;ENDCASE&nbsp;.<br/>
ENDFORM .<br/>
<br/>
FORM st USING rt_extab TYPE slis_t_extab.   "状态栏/工具栏<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'ZSTAT001'.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_APPEND_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;GS_TAB_KUNNR<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_append_data_p1  USING pv_kunnr TYPE kunnr .<br/>
&nbsp;&nbsp;CLEAR&nbsp;pt_tab&nbsp;.<br/>
&nbsp;&nbsp;SELECT&nbsp;bsid~belnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~buzei<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~gjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~dmbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~umskz<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~zuonr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~budat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~blart<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~shkzg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~sgtxt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~hkont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsid~waers<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kna1~name1&nbsp;AS&nbsp;name2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;pt_tab&nbsp;FROM&nbsp;bsid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;kna1&nbsp;ON&nbsp;bsid~kunnr&nbsp;=&nbsp;kna1~kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bsid~kunnr&nbsp;IN&nbsp;s_kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;bsid~bukrs&nbsp;IN&nbsp;s_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;(&nbsp;bsid~umskz&nbsp;IN&nbsp;s_umskz&nbsp;OR&nbsp;bsid~umskz&nbsp;EQ&nbsp;''&nbsp;).<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gt_h,gt_s&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;pt_tab&nbsp;INTO&nbsp;gs_tab&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;pv_kunnr&nbsp;AND&nbsp;shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_tab&nbsp;TO&nbsp;gs_h&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_h&nbsp;TO&nbsp;gt_h&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gs_h,gs_tab&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;pt_tab&nbsp;INTO&nbsp;gs_tab&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;pv_kunnr&nbsp;AND&nbsp;shkzg&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_tab&nbsp;TO&nbsp;gs_s&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_s&nbsp;TO&nbsp;gt_s&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gs_s,gs_tab&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;gt_h&nbsp;BY&nbsp;bukrs&nbsp;gjahr&nbsp;belnr&nbsp;buzei&nbsp;.<br/>
&nbsp;&nbsp;SORT&nbsp;gt_s&nbsp;BY&nbsp;bukrs&nbsp;gjahr&nbsp;belnr&nbsp;buzei&nbsp;.<br/>
ENDFORM.<br/>
<br/>
FORM frm_append_data_p2  USING pv_lifnr TYPE lifnr .<br/>
&nbsp;&nbsp;CLEAR&nbsp;pt_tab&nbsp;.<br/>
&nbsp;&nbsp;SELECT&nbsp;bsik~belnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~buzei<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~gjahr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~lifnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~wrbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~dmbtr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~umskz<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~zuonr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~budat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~blart<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~shkzg<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~sgtxt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~hkont<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~waers<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsik~XREF3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfa1~name1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;pt_tab&nbsp;FROM&nbsp;bsik<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;lfa1&nbsp;ON&nbsp;bsik~lifnr&nbsp;=&nbsp;lfa1~lifnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bsik~lifnr&nbsp;IN&nbsp;s_lifnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;bsik~bukrs&nbsp;IN&nbsp;s_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;(&nbsp;bsik~umskz&nbsp;IN&nbsp;s_umskz&nbsp;OR&nbsp;bsik~umskz&nbsp;EQ&nbsp;''&nbsp;).<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gt_h,gt_s&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;pt_tab&nbsp;INTO&nbsp;gs_tab&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;pv_lifnr&nbsp;AND&nbsp;shkzg&nbsp;=&nbsp;'H'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_tab&nbsp;TO&nbsp;gs_h&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_h&nbsp;TO&nbsp;gt_h&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gs_h,gs_tab&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;pt_tab&nbsp;INTO&nbsp;gs_tab&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;pv_lifnr&nbsp;AND&nbsp;shkzg&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_tab&nbsp;TO&nbsp;gs_s&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_s&nbsp;TO&nbsp;gt_s&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gs_s,gs_tab&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;gt_h&nbsp;BY&nbsp;bukrs&nbsp;gjahr&nbsp;belnr&nbsp;buzei&nbsp;.<br/>
&nbsp;&nbsp;SORT&nbsp;gt_s&nbsp;BY&nbsp;bukrs&nbsp;gjahr&nbsp;belnr&nbsp;buzei&nbsp;.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>