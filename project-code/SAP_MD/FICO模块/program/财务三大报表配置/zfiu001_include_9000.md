<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFIU001_INCLUDE_9000</h2>
<h3> Description: Include ZFIU001_INCLUDE_9000</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
***INCLUDE&nbsp;ZFIR001_INCLUDE_9000.<br/>
*----------------------------------------------------------------------*<br/>
<br/>
*&amp;SPWIZARD:&nbsp;DECLARATION&nbsp;OF&nbsp;TABLECONTROL&nbsp;'TC01'&nbsp;ITSELF<br/>
</div>
<div class="code">
CONTROLS: tbcc TYPE TABLEVIEW USING SCREEN 9000.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;TC_CHANGE_TC_ATTR&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE tc_change_tc_attr OUTPUT .<br/>
&nbsp;&nbsp;"设置table&nbsp;control的起始行高<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;gt_tbcc&nbsp;LINES&nbsp;tbcc-lines&nbsp;.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;OUTPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TBCC'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MOVE&nbsp;ITAB&nbsp;TO&nbsp;DYNPRO<br/>
</div>
<div class="code">
MODULE tbcc_move OUTPUT .<br/>
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_tbcc&nbsp;TO&nbsp;zfit001&nbsp;.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;SPWIZARD:&nbsp;INPUT&nbsp;MODULE&nbsp;FOR&nbsp;TC&nbsp;'TBCC'.&nbsp;DO&nbsp;NOT&nbsp;CHANGE&nbsp;THIS&nbsp;LINE!<br/>
*&amp;SPWIZARD:&nbsp;MODIFY&nbsp;TABLE<br/>
</div>
<div class="code">
MODULE tbcc_modify INPUT.<br/>
<br/>
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;zfit001&nbsp;TO&nbsp;gs_tbcc&nbsp;.<br/>
&nbsp;&nbsp;MODIFY&nbsp;gt_tbcc&nbsp;FROM&nbsp;gs_tbcc&nbsp;INDEX&nbsp;tbcc-current_line&nbsp;.<br/>
<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;TBCC_MARK&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE tbcc_mark INPUT.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_tbcc&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_tbcc&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;tbcc-line_sel_mode&nbsp;=&nbsp;1&nbsp;AND&nbsp;gs_tbcc-slbox&nbsp;=&nbsp;'X'.&nbsp;"当设置table&nbsp;control为&nbsp;行单选时&nbsp;&nbsp;需要把已选中的行清除掉<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_tbcc&nbsp;INTO&nbsp;ls_tbcc&nbsp;WHERE&nbsp;slbox&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_tbcc-slbox&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_tbcc&nbsp;FROM&nbsp;ls_tbcc&nbsp;TRANSPORTING&nbsp;slbox.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;MODIFY&nbsp;gt_tbcc&nbsp;FROM&nbsp;gs_tbcc&nbsp;INDEX&nbsp;tbcc-current_line&nbsp;TRANSPORTING&nbsp;slbox&nbsp;.<br/>
<br/>
ENDMODULE.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;STATUS_9000&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_9000 OUTPUT.<br/>
<br/>
&nbsp;&nbsp;DATA:f_code&nbsp;LIKE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;sy-ucomm&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;fc_code&nbsp;&lt;&gt;&nbsp;'NEW'&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"修改&nbsp;&nbsp;&nbsp;根据功能码&nbsp;&nbsp;动态的来确定工具栏功能按钮<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;f_code[]&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;'TCADD'&nbsp;TO&nbsp;f_code&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;'UPLOAD'&nbsp;TO&nbsp;f_code&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;f_code[]&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;'MODIFY'&nbsp;TO&nbsp;f_code&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;'NEW'&nbsp;TO&nbsp;f_code&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;'TCDEL'&nbsp;TO&nbsp;f_code&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI9000'&nbsp;EXCLUDING&nbsp;f_code[]&nbsp;.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TITLE9000'.<br/>
ENDMODULE.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;INITIAL_SCREEN&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE initial_screen OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_col&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;tbcc-cols.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gt_tbcc[]&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"初始化table&nbsp;control为全灰<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-name(8)&nbsp;=&nbsp;'ZFIT001-'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-active&nbsp;=&nbsp;'0'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;tbcc-cols&nbsp;INTO&nbsp;ls_col&nbsp;."隐藏tablecontrol&nbsp;公司代码列&nbsp;&nbsp;因为所有公司公用一套<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ztype-low&nbsp;=&nbsp;'CF'&nbsp;.&nbsp;&nbsp;"根据报表类型决定显示table&nbsp;contro哪些列<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_col-screen-name&nbsp;=&nbsp;'ZFIT001-FRFAREA'&nbsp;OR&nbsp;ls_col-screen-name&nbsp;=&nbsp;'ZFIT001-TRFAREA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;ls_col-screen-name&nbsp;=&nbsp;'ZFIT001-FSAKNR'&nbsp;OR&nbsp;ls_col-screen-name&nbsp;=&nbsp;'ZFIT001-TSAKNR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_col-vislength&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;tbcc-cols&nbsp;FROM&nbsp;ls_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_col-screen-name&nbsp;=&nbsp;'ZFIT001-RSTGR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_col-vislength&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;tbcc-cols&nbsp;FROM&nbsp;ls_col.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;fc_code&nbsp;&lt;&gt;&nbsp;'NEW'&nbsp;.&nbsp;&nbsp;&nbsp;"修改功能下&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;控制修改状态下&nbsp;&nbsp;主键字段不可修改<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'G2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;mdf_flag&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ELSE&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"新增功能<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;screen-group1&nbsp;=&nbsp;'G1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;screen-input&nbsp;=&nbsp;'1'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;SCREEN&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>