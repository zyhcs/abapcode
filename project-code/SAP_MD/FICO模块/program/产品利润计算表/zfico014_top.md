<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO014_TOP</h2>
<h3> Description: Include ZFICO014_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO014_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;TABLES:t001,acdoca,kna1,mara.<br/>
&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;mara-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maktx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;makt-maktx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wgbez&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t023t-wgbez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;butxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;t001-butxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cpfl(10)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_fklmg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-fklmg,&nbsp;"本期数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-meins,&nbsp;"数量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_netwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_netwr_yf&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期运费<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_wavwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-wavwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_tssl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tvpod-lgmng_diff,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_tsje&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期途损金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bn_TSSL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tvpod-lgmng_diff,&nbsp;"本年途损数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bn_TSJE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年途损金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_xsfy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期销售费用<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_xsfysum&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期销售费用总额--处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_glf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期管理费用及财务费用<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_glfsum&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期管理费用及财务费用总额-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_sj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期税金及附加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_sjsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期税金及附加总额-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_yf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期研发<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_yfsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期研发-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljsl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-fklmg,&nbsp;"本年累计数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljxsje&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计销售金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljxsje_yf&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计销售金额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljxscb&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计销售成本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljxsfy&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计销售费用<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_ljxsfysum&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计销售费用总额-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_glf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计管理费用及财务费用<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_glfsum&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计管理费用及财务费用总额-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_sj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计税金及附加<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_sjsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年累计税金及附加总额-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_yf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年研发<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_yfsum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年研发-处理尾差<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_cycb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年差异成本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_cycb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期差异成本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bn_sjcb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本年实际成本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bq_sjcb&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,&nbsp;"本期实际成本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_data.<br/>
<br/>
&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_vbrk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrk-vbeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-posnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fklmg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-fklmg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-netwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wavwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-wavwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs_ana&nbsp;LIKE&nbsp;vbrp-bukrs_ana,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrp-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fkdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrk-fkdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fkart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;vbrk-fkart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_vbrk.<br/>
<br/>
&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_tvpod,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tvpod-vbeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;posnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;tvpod-posnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lgmng_diff&nbsp;LIKE&nbsp;tvpod-lgmng_diff,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_tvpod.<br/>
<br/>
&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_objek,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objek&nbsp;LIKE&nbsp;ausp-objek,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_objek.<br/>
<br/>
&nbsp;TYPES:BEGIN&nbsp;OF&nbsp;ty_acdoct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ryear&nbsp;&nbsp;LIKE&nbsp;acdoct-ryear,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rpmax&nbsp;&nbsp;LIKE&nbsp;acdoct-rpmax,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;&nbsp;LIKE&nbsp;acdoct-drcrk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbukrs&nbsp;LIKE&nbsp;acdoct-rbukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;LIKE&nbsp;acdoct-racct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;LIKE&nbsp;acdoct-rfarea,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt&nbsp;&nbsp;LIKE&nbsp;acdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl01&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl02&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl02,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl03&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl03,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl04&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl04,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl05&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl05,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl06&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl06,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl07&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl07,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl08&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl08,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl09&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl09,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl10&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl11&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl11,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl12&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl13&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl13,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl14&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl14,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl15&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl15,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl16&nbsp;&nbsp;LIKE&nbsp;acdoct-hsl16,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_acdoct.<br/>
<br/>
&nbsp;DATA:gt_data&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;ty_data,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gw_data&nbsp;TYPE&nbsp;ty_data.<br/>
<br/>
&nbsp;RANGES:&nbsp;rs_fkdat&nbsp;FOR&nbsp;vbrk-fkdat.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;显示用到的全局参数<br/>
*&nbsp;定义FIELDCAT&nbsp;内表和工作区<br/>
</div>
<div class="code">
&nbsp;DATA:&nbsp;gt_fieldcat&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gw_fieldcat&nbsp;TYPE&nbsp;lvc_s_fcat.<br/>
</div>
<div class="codeComment">
*&nbsp;定义布局<br/>
</div>
<div class="code">
&nbsp;DATA:&nbsp;gw_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
</div>
<div class="codeComment">
*&nbsp;设置字段排序<br/>
</div>
<div class="code">
&nbsp;DATA:&nbsp;gt_sort&nbsp;TYPE&nbsp;lvc_t_sort,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gw_sort&nbsp;TYPE&nbsp;lvc_s_sort.<br/>
</div>
<div class="codeComment">
*&nbsp;定义ALV&nbsp;对象,ALV回调的时候用，一般不用<br/>
</div>
<div class="code">
&nbsp;DATA:&nbsp;gr_grid&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;为FIELDCAT&nbsp;定义宏&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;其中&amp;1表示占位符<br/>
*&nbsp;以下含有“是否”的值范围为空和X<br/>
</div>
<div class="code">
&nbsp;DEFINE&nbsp;de_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gw_FIELDCAT.<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;1.&nbsp;"字段名<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-scrtext_l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段长描述<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-scrtext_m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段中描述<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-scrtext_s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;"字段短描述<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;3.&nbsp;"主键，蓝底显示，默认冻结列<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-no_zero&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;4.&nbsp;"不显示0值<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;"是否编辑<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-ref_field&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;"参考字段&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-ref_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;"参考表&nbsp;&nbsp;&nbsp;&amp;6&nbsp;&amp;7&nbsp;一起使用<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-fix_column&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;8.&nbsp;"冻结列<br/>
&nbsp;&nbsp;&nbsp;gw_FIELDCAT-convexit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;9.&nbsp;"转换例程<br/>
&nbsp;&nbsp;&nbsp;APPEND&nbsp;gw_FIELDCAT&nbsp;TO&nbsp;gt_fieldcat.<br/>
&nbsp;END-OF-DEFINITION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>