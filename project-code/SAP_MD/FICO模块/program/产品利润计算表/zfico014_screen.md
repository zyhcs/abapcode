<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO014_SCREEN</h2>
<h3> Description: Include ZFICO014_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO014_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:p_BURKS&nbsp;like&nbsp;T001-bukrs&nbsp;OBLIGATORY,&nbsp;&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_GJAHR&nbsp;like&nbsp;ACDOCA-GJAHR&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_POPER&nbsp;like&nbsp;ACDOCA-POPER&nbsp;OBLIGATORY.<br/>
SELECT-OPTIONS: s_MATNR  for MARA-MATNR.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_MATKL&nbsp;&nbsp;for&nbsp;MARA-MATKL.<br/>
</div>
<div class="code">
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;b1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>