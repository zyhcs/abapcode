<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO014</h2>
<h3> Description: 产品利润计算表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO014<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO014.<br/>
<br/>
include <a href="zfico014_top.html">ZFICO014_top</a>.<br/>
<br/>
include <a href="zfico014_screen.html">ZFICO014_screen</a>.<br/>
<br/>
include <a href="zfico014_f01.html">ZFICO014_f01</a>.<br/>
<br/>
include <a href="zfico014_o01.html">ZFICO014_o01</a>.<br/>
<br/>
START-OF-SELECTION.<br/>
"MESSAGE i022(M8) WITH 'AAAAA'." RAISING INVALID_POSTING_PERIOD.<br/>
&nbsp;&nbsp;PERFORM&nbsp;CHECK_AUTH.<br/>
</div>
<div class="codeComment">
*取数<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_pub_data.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_get_data.<br/>
</div>
<div class="codeComment">
*alv&nbsp;显示<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;FRM_ALV_DISPLAY.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;O01&nbsp;公司描述<br/>
*&nbsp;O02&nbsp;本期数量(基本计量单位)<br/>
*&nbsp;O03&nbsp;本期销售金额<br/>
*&nbsp;O04&nbsp;本期销售成本<br/>
*&nbsp;O05&nbsp;本期途损数量<br/>
*&nbsp;O06&nbsp;本期途损金额<br/>
*&nbsp;O07&nbsp;本期销售费用<br/>
*&nbsp;O08&nbsp;本期管理费用及财务费用<br/>
*&nbsp;O09&nbsp;本期税金及附加<br/>
*&nbsp;O10&nbsp;本年累计数量<br/>
*&nbsp;O11&nbsp;本年累计销售金额<br/>
*&nbsp;O12&nbsp;本年累计销售成本<br/>
*&nbsp;O13&nbsp;本年累计销售费用<br/>
*&nbsp;O14&nbsp;本年累计管理费用及财务费用<br/>
*&nbsp;O15&nbsp;本年累计税金及附加<br/>
*&nbsp;O16&nbsp;物料<br/>
*&nbsp;O17&nbsp;物料描述<br/>
*&nbsp;O18&nbsp;物料分类<br/>
*&nbsp;O19&nbsp;分类描述<br/>
*&nbsp;O20&nbsp;数量单位<br/>
*&nbsp;O21&nbsp;产品分类<br/>
*&nbsp;O22&nbsp;本年途损数量<br/>
*&nbsp;O23&nbsp;本年途损金额<br/>
*&nbsp;O24&nbsp;本期实际成本<br/>
*&nbsp;O25&nbsp;本年实际成本<br/>
*&nbsp;O26&nbsp;本期运费<br/>
*&nbsp;O27&nbsp;本期研发费用<br/>
*&nbsp;O28&nbsp;本年运费<br/>
*&nbsp;O29&nbsp;本年研发费用<br/>
*&nbsp;W01&nbsp;没有查询到相关数据!<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BURKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年度<br/>
*&nbsp;P_POPER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;期间<br/>
*&nbsp;S_MATKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料组<br/>
*&nbsp;S_MATNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>