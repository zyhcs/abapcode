<table class="outerTable">
<tr>
<td><h2>Table: ZBV_KO_MA_LST_V</h2>
<h3>Description: 为视图生成的表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>AUFNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUKRS</td>
<td>3</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>GJPER</td>
<td>4</td>
<td>&nbsp;</td>
<td>JAHRPER</td>
<td>JAHRPER</td>
<td>NUMC</td>
<td>7</td>
<td>&nbsp;</td>
<td>期间/年度</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>AUART</td>
<td>5</td>
<td>&nbsp;</td>
<td>AUFART</td>
<td>AUFART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>订单类型</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MATNR</td>
<td>6</td>
<td>&nbsp;</td>
<td>CO_MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>订单的物料编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>KTEXT</td>
<td>7</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>IDAT2</td>
<td>8</td>
<td>&nbsp;</td>
<td>AUFIDAT2</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>技术完成日期</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>HSL_CHK</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>10</td>
<td>HSL_CP_I</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>11</td>
<td>HSL_CP_O</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>12</td>
<td>HSL_CP_W</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>