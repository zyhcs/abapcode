<table class="outerTable">
<tr>
<td><h2>Table: ZSTR_KO_A001_ALV</h2>
<h3>Description: KO A001 Output</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>AUFNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MATNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>KTEXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>AUFTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>HSL_CP_I</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZHSLCPI</td>
<td>ZHSLCPI</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>本期投入</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>HSL_CP_W</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZHSLCPW</td>
<td>ZHSLCPW</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>在制变化</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>HSL_CP_O</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZHSLCPO</td>
<td>ZHSLCPO</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>本期产出</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>HSL_CHK</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZHSLCHK</td>
<td>ZHSLCHK</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>余额检查</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>