<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO026</h2>
<h3> Description: 生产订单投入产出平衡检查表报表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;zfico026<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfico026.<br/>
TABLES: t001,bkpf,ska1,aufk.<br/>
DATA: gt_koma  TYPE TABLE OF  zbv_ko_ma_lst_v,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_sumcp&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;&nbsp;zbv_ko_sum_v.<br/>
<br/>
DATA: gt_output TYPE TABLE OF zstr_ko_a001_alv.<br/>
<br/>
<br/>
FIELD-SYMBOLS: &lt;fs_koma&gt;   TYPE zbv_ko_ma_lst_v,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_sumcp&gt;&nbsp;&nbsp;TYPE&nbsp;zbv_ko_sum_v,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_output&gt;&nbsp;TYPE&nbsp;zstr_ko_a001_alv.<br/>
<br/>
DATA: gr_alv       TYPE REF TO cl_salv_table,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gr_functions<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_salv_functions_list.<br/>
<br/>
DATA: lr_layout TYPE REF TO cl_salv_layout,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_key&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;salv_s_layout_key.<br/>
SELECT-OPTIONS: s_bukrs FOR bkpf-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_aufnr&nbsp;FOR&nbsp;aufk-saknr.<br/>
<br/>
PARAMETERS: p_gjper LIKE acdoca-fiscyearper .<br/>
<br/>
START-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*&nbsp;select&nbsp;data&nbsp;in&nbsp;internal&nbsp;table<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;@gt_koma<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;&nbsp;zbv_ko_ma_lst_v(&nbsp;p_gjper&nbsp;=&nbsp;@p_gjper&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;bukrs&nbsp;IN&nbsp;@s_bukrs<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;aufnr&nbsp;IN&nbsp;@s_aufnr&nbsp;.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_koma&nbsp;ASSIGNING&nbsp;&lt;fs_koma&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;INITIAL&nbsp;LINE&nbsp;TO&nbsp;gt_output&nbsp;ASSIGNING&nbsp;&lt;fs_output&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;&lt;fs_koma&gt;&nbsp;TO&nbsp;&lt;fs_output&gt;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;create&nbsp;the&nbsp;ALV&nbsp;object<br/>
</div>
<div class="code">
&nbsp;&nbsp;cl_salv_table=&gt;factory(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_salv_table&nbsp;&nbsp;&nbsp;=&nbsp;gr_alv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_output<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;).<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;set&nbsp;default&nbsp;set&nbsp;of&nbsp;generic&nbsp;functions<br/>
</div>
<div class="code">
&nbsp;&nbsp;gr_functions&nbsp;=&nbsp;gr_alv-&gt;get_functions(&nbsp;).<br/>
&nbsp;&nbsp;gr_functions-&gt;set_all(&nbsp;).<br/>
</div>
<div class="codeComment">
*&nbsp;display&nbsp;it!<br/>
</div>
<div class="code">
&nbsp;&nbsp;gr_alv-&gt;display(&nbsp;).<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_GJPER&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_AUFNR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;S_BUKRS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>