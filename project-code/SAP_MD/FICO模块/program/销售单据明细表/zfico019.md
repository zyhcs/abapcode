<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO019</h2>
<h3> Description: 销售单据明细表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO019<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO019.<br/>
<br/>
include <a href="zfico019_top.html">ZFICO019_top</a>.<br/>
<br/>
include <a href="zfico019_screen.html">ZFICO019_screen</a>.<br/>
<br/>
include <a href="zfico019_f01.html">ZFICO019_f01</a>.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;AT&nbsp;SELECTION-SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
</div>
<div class="codeComment">
*&nbsp;权限检查<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_input_check.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;at&nbsp;selection-screen&nbsp;output<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN OUTPUT.<br/>
</div>
<div class="codeComment">
*&nbsp;设置选择界面属性<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_set_screen_output.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;start-of-selection<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*&nbsp;检查必输字段<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_check_required.<br/>
</div>
<div class="codeComment">
*&nbsp;读取数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_retrieve_data.<br/>
</div>
<div class="codeComment">
*&nbsp;处理数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_manipulate_data.<br/>
</div>
<div class="codeComment">
*&nbsp;使用ALV显示数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_display_data.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;T01&nbsp;选择条件<br/>
*&nbsp;T02&nbsp;未拒绝<br/>
*&nbsp;T03&nbsp;部分拒绝<br/>
*&nbsp;T04&nbsp;全部拒绝<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;RD_DN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按发货单查询<br/>
*&nbsp;RD_SO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;按销售订单查询<br/>
*&nbsp;S_AUART&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;订单类型<br/>
*&nbsp;S_CHARG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;批次<br/>
*&nbsp;S_ERDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创建日期<br/>
*&nbsp;S_ERNAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创建者<br/>
*&nbsp;S_FKDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开票日期<br/>
*&nbsp;S_GBSTK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销售订单总体状态<br/>
*&nbsp;S_KUNNG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;送达方<br/>
*&nbsp;S_KUNNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客户编号<br/>
*&nbsp;S_LBELN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交货单号<br/>
*&nbsp;S_LFART&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交货单类型<br/>
*&nbsp;S_LGORT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;库存地点<br/>
*&nbsp;S_MATKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料组<br/>
*&nbsp;S_MATNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料编号<br/>
*&nbsp;S_VBELN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销售订单号<br/>
*&nbsp;S_VKGRP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销售组<br/>
*&nbsp;S_VKORG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销售组织<br/>
*&nbsp;S_VTWEG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分销渠道<br/>
*&nbsp;S_WADAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;过账日期<br/>
*&nbsp;S_WBSTK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交货过账状态<br/>
*&nbsp;S_WERKS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交货工厂<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;00<br/>
*055&nbsp;&nbsp;&nbsp;填写所有必填字段<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;COM_CLEAR<br/>
*018&nbsp;&nbsp;&nbsp;您没有权限<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;EDEREG_INV<br/>
*174&nbsp;&nbsp;&nbsp;无法找到满足选择标准的数据<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>