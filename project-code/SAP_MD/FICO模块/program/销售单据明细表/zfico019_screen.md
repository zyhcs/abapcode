<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO019_SCREEN</h2>
<h3> Description: Include ZFICO019_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO019_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;SELECTION-SCREEN<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk_1 WITH FRAME TITLE TEXT-t01.<br/>
SELECT-OPTIONS:s_vkorg FOR vbak-vkorg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_vtweg&nbsp;FOR&nbsp;vbak-vtweg&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_vkgrp&nbsp;FOR&nbsp;vbak-vkgrp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kunnr&nbsp;FOR&nbsp;likp-kunnr&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_vbeln&nbsp;FOR&nbsp;vbak-vbeln&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lbeln&nbsp;FOR&nbsp;likp-vbeln&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_auart&nbsp;FOR&nbsp;vbak-auart&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_gbstk&nbsp;FOR&nbsp;vbak-gbstk&nbsp;,"MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lfart&nbsp;FOR&nbsp;likp-lfart&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lbstk&nbsp;FOR&nbsp;likp-gbstk&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kunng&nbsp;FOR&nbsp;likp-kunnr&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ernam&nbsp;FOR&nbsp;vbak-ernam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_erdat&nbsp;FOR&nbsp;vbak-erdat,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kodat&nbsp;FOR&nbsp;likp-kodat&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_wadat&nbsp;FOR&nbsp;likp-wadat_ist&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matnr&nbsp;FOR&nbsp;vbap-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matkl&nbsp;FOR&nbsp;vbap-matkl&nbsp;MODIF&nbsp;ID&nbsp;m1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_werks&nbsp;FOR&nbsp;vbap-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lgort&nbsp;FOR&nbsp;vbap-lgort&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_charg&nbsp;FOR&nbsp;lips-charg&nbsp;MODIF&nbsp;ID&nbsp;m2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_wbstk&nbsp;FOR&nbsp;likp-wbstk,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_fkdat&nbsp;FOR&nbsp;vbrk-fkdat.<br/>
SELECTION-SCREEN END OF BLOCK blk_1.<br/>
PARAMETERS:rd_so RADIOBUTTON GROUP grp1 USER-COMMAND cmd DEFAULT 'X',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rd_dn&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>