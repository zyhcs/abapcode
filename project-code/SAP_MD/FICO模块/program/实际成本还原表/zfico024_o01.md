<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO024_O01</h2>
<h3> Description: Include ZFICO024_O01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO024_O01<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_BULID_LAYOUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;设置ALV的布局<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_BULID_LAYOUT .<br/>
&nbsp;&nbsp;GW_LAYOUT-ZEBRA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;斑马条&nbsp;使ALV界面呈现颜色交替<br/>
&nbsp;&nbsp;GW_LAYOUT-CWIDTH_OPT&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;自动优化列宽<br/>
&nbsp;&nbsp;GW_LAYOUT-SEL_MODE&nbsp;&nbsp;&nbsp;=&nbsp;'A'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;选择模式，“A”在最左端有选择按钮<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_BULID_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;使用宏来填充FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_BULID_FIELDCAT .<br/>
&nbsp;&nbsp;REFRESH&nbsp;GT_FIELDCAT&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BURKS'&nbsp;TEXT-O01&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'BUTXT'&nbsp;TEXT-O02&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'XM'&nbsp;&nbsp;&nbsp;&nbsp;TEXT-O03&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'KOSTL'&nbsp;TEXT-O04&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'KTEXT'&nbsp;TEXT-O05&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON1'&nbsp;TEXT-O06&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON2'&nbsp;TEXT-O07&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON3'&nbsp;TEXT-O08&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON4'&nbsp;TEXT-O09&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON5'&nbsp;TEXT-O10&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON6'&nbsp;TEXT-O11&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON7'&nbsp;TEXT-O12&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON8'&nbsp;TEXT-O13&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON9'&nbsp;TEXT-O14&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON10'&nbsp;TEXT-O15&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON11'&nbsp;TEXT-O16&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'MON12'&nbsp;TEXT-O17&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;DE_FIELDCAT&nbsp;'LJ'&nbsp;TEXT-O18&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;''&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_ALV_DISPLAY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FRM_ALV_DISPLAY .<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_BULID_LAYOUT&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_BULID_FIELDCAT&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_BUFFER_ACTIVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALLBACK_PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;SY-REPID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"I_CALLBACK_PF_STATUS_SET&nbsp;=&nbsp;'FRM_SET_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"I_CALLBACK_USER_COMMAND&nbsp;&nbsp;=&nbsp;'FRM_USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IS_LAYOUT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GW_LAYOUT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_FIELDCAT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_FIELDCAT[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IT_SORT_LVC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_SORT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SAVE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T_OUTTAB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;GT_DATA[].<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>