<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO024_SCREEN</h2>
<h3> Description: Include ZFICO024_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO024_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B2&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;&nbsp;PARAMETERS:P_BUKRS&nbsp;LIKE&nbsp;T001-BUKRS&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P_GJAHR&nbsp;LIKE&nbsp;ACDOCA-GJAHR&nbsp;OBLIGATORY.<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B2.<br/>
<br/>
&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;B1&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-002.<br/>
<br/>
&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;LINE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;5.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;rd1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1&nbsp;DEFAULT&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;8(10)&nbsp;TEXTCR1.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;20.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;rd2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;22(10)&nbsp;TEXTCR2.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;34.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;rd3&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;36(12)&nbsp;TEXTCR3.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;POSITION&nbsp;50.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARAMETERS:&nbsp;rd4&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rad1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;COMMENT&nbsp;52(10)&nbsp;TEXTCR4.<br/>
&nbsp;&nbsp;&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;LINE.<br/>
<br/>
<br/>
&nbsp;SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;B1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>