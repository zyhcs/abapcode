<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO018</h2>
<h3>Description: 实际成本还原报配置</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BURKS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZTYPE</td>
<td>3</td>
<td>X</td>
<td>ZE_ZTYPE</td>
<td>ZDM_ZTYPE</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>报表</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZROW</td>
<td>4</td>
<td>X</td>
<td>ZROW</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>行号(报表中的行号)</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZPRO</td>
<td>5</td>
<td>X</td>
<td>ZE_ZPRO</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>项目</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>KOSTL</td>
<td>6</td>
<td>X</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>SAKNR</td>
<td>7</td>
<td>X</td>
<td>SAKNR</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MATNR</td>
<td>8</td>
<td>X</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZTYPETXT</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZE_ZTYPETXT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>25</td>
<td>&nbsp;</td>
<td>报表描述</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZPROTXT</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZE_ZPROTXT</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>25</td>
<td>&nbsp;</td>
<td>项目描述</td>
</tr>
<tr class="cell">
<td>11</td>
<td>KTEXT</td>
<td>11</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td>12</td>
<td>TXT50</td>
<td>12</td>
<td>&nbsp;</td>
<td>TXT50_SKAT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>总账科目长文本</td>
</tr>
<tr class="cell">
<td>13</td>
<td>MAKTX</td>
<td>13</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>