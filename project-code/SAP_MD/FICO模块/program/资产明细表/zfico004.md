<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO004</h2>
<h3> Description: 资产明细表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO004<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO004.<br/>
include <a href="zfico004_top.html">zfico004_top</a>.<br/>
<br/>
include <a href="zfico004_sel.html">zfico004_sel</a>.<br/>
<br/>
include <a href="zfico004_f01.html">zfico004_f01</a>.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_AFABE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;折旧范围<br/>
*&nbsp;P_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;财年<br/>
*&nbsp;P_PERAF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;折旧期间<br/>
*&nbsp;R_B1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资产价值明细表<br/>
*&nbsp;R_B2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;含未入账资产清单<br/>
*&nbsp;S_AFASL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;折旧码<br/>
*&nbsp;S_ANLKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资产分类<br/>
*&nbsp;S_ANLN1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资产<br/>
*&nbsp;S_KOSTL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成本中心<br/>
*&nbsp;S_ORD43&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;使用人(非员工号)<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;00<br/>
*398&nbsp;&nbsp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;&nbsp;&amp;<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>