<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO004_SEL</h2>
<h3> Description: Include ZFICO004_SEL</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO004_SEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;选择屏幕<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK bk1.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;p_bukrs&nbsp;FOR&nbsp;anla-bukrs&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_gjahr&nbsp;TYPE&nbsp;anlc-gjahr&nbsp;DEFAULT&nbsp;sy-datum+0(4)&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_peraf&nbsp;TYPE&nbsp;anlp-peraf&nbsp;DEFAULT&nbsp;sy-datum+4(2)&nbsp;OBLIGATORY.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;s_anln1&nbsp;FOR&nbsp;anla-anln1,&nbsp;"主资产号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_anlkl&nbsp;FOR&nbsp;anla-anlkl,&nbsp;"资产分类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"s_invnr&nbsp;FOR&nbsp;anla-invnr,"存放地点&nbsp;&nbsp;存货号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_kostl&nbsp;FOR&nbsp;anlz-kostl,&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_afasl&nbsp;FOR&nbsp;anlb-afasl,&nbsp;"折旧码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ord43&nbsp;FOR&nbsp;anla-ord43.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_afabe&nbsp;TYPE&nbsp;anlc-afabe&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;'01'.&nbsp;"实际折旧范围<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;r_b1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rb1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_b2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;rb1.<br/>
SELECTION-SCREEN END OF BLOCK bk1.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;AT&nbsp;SELECTION&nbsp;SERCCN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
&nbsp;&nbsp;PERFORM&nbsp;authority_check.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;START-OF-SELECTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;p_peraf&nbsp;=&nbsp;'013'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;p_gjahr&nbsp;&nbsp;'12'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'01'&nbsp;INTO&nbsp;wlv_date.&nbsp;&nbsp;&nbsp;&nbsp;"屏选年度期间第一天，期间为13<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;p_gjahr&nbsp;&nbsp;p_peraf+1(2)&nbsp;'01'&nbsp;INTO&nbsp;wlv_date.&nbsp;&nbsp;&nbsp;&nbsp;"屏选年度期间第一天<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;p_gjahr&nbsp;&nbsp;p_peraf+1(2)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;wlv_date_ym.&nbsp;"屏选年度期间<br/>
<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;p_gjahr&nbsp;'01'&nbsp;'01'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;wlv_first."屏选年度第一天<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'LAST_DAY_OF_MONTHS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;day_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_date<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;last_day_of_month&nbsp;=&nbsp;wlv_month<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;day_in_no_date&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;PERFORM&nbsp;get_data.<br/>
&nbsp;&nbsp;PERFORM&nbsp;process_data.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;END-OF-SELECTION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
END-OF-SELECTION.<br/>
&nbsp;&nbsp;IF&nbsp;wt_outtab[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="codeComment">
***********************************************************************<br/>
**&nbsp;S4DK900831&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ABAP03&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FICO_固定资产接口修改_20190225<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-tcode&nbsp;=&nbsp;'SE38'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR&nbsp;sy-tcode&nbsp;=&nbsp;'ZFICO034'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;display_data.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FREE&nbsp;MEMORY&nbsp;ID&nbsp;'ZFI_FIXEDASSET_TAB'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;wt_outtab[]&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'ZFI_FIXEDASSET_TAB'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
**********************************************************************<br/>
</div>
<div class="code">
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;i398(00)&nbsp;WITH&nbsp;'没有查到数据！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;STOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>