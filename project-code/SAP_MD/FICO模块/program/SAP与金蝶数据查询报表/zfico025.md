<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO025</h2>
<h3> Description: SAP与金蝶数据查询报表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO025<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT ZFICO025.<br/>
<br/>
include <a href="zfico025_data.html">ZFICO025_data</a>.<br/>
include <a href="zfico025_form.html">ZFICO025_form</a>.<br/>
<br/>
INITIALIZATION."初始化事件<br/>
<br/>
AT SELECTION-SCREEN OUTPUT. "选择屏幕输出处理事件<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;frm_modify_screen.<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.   "权限检查<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;frm_check.<br/>
<br/>
</div>
<div class="code">
START-OF-SELECTION.   "取数<br/>
&nbsp;&nbsp;IF&nbsp;p_jsap&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data015.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;p_sapj&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data016.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
END-OF-SELECTION.     "alv展示<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_display.<br/>
<br/>
</div>
<div class="codeComment">
*Text&nbsp;elements<br/>
*----------------------------------------------------------<br/>
*&nbsp;001&nbsp;选择条件<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_JSAP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金蝶传SAP<br/>
*&nbsp;P_R1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;传输成功信息<br/>
*&nbsp;P_R2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;传输失败信息<br/>
*&nbsp;P_SAPJ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAP传金蝶<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计年度<br/>
*&nbsp;S_MONAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会计期间<br/>
*&nbsp;S_ZKBE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金蝶凭证编号<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>