<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO006</h2>
<h3>Description: 会计凭证推送EAS汇总表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>HZID</td>
<td>2</td>
<td>X</td>
<td>ZEHZID</td>
<td>ZDHZID</td>
<td>NUMC</td>
<td>20</td>
<td>&nbsp;</td>
<td>会计凭证汇总ID</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>HZITEM</td>
<td>3</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>INT2</td>
<td>5</td>
<td>&nbsp;</td>
<td>汇总行号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BLART</td>
<td>4</td>
<td>&nbsp;</td>
<td>BLART</td>
<td>BLART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>凭证类型</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>RYEAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>RYEAR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>POPER</td>
<td>6</td>
<td>&nbsp;</td>
<td>POPER</td>
<td>POPER</td>
<td>NUMC</td>
<td>3</td>
<td>&nbsp;</td>
<td>过账期间</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>RBUKRS</td>
<td>7</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>RACCT</td>
<td>8</td>
<td>&nbsp;</td>
<td>RACCT</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>科目号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZKSAKNR</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZEKSAKNR</td>
<td>ZDKSAKNR</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶末级科目</td>
</tr>
<tr class="cell">
<td>10</td>
<td>DRCRK</td>
<td>10</td>
<td>&nbsp;</td>
<td>SHKZG</td>
<td>SHKZG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>借/贷标识</td>
</tr>
<tr class="cell">
<td>11</td>
<td>RWCUR</td>
<td>11</td>
<td>&nbsp;</td>
<td>RWCUR</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>原事务货币的货币码</td>
</tr>
<tr class="cell">
<td>12</td>
<td>HSL</td>
<td>12</td>
<td>&nbsp;</td>
<td>FINS_VHCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以公司代码货币计的金额</td>
</tr>
<tr class="cell">
<td>13</td>
<td>WSL</td>
<td>13</td>
<td>&nbsp;</td>
<td>FINS_VWCUR12</td>
<td>WERTV12</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以交易货币计的金额</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MATNR</td>
<td>14</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>15</td>
<td>AUFNR</td>
<td>15</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>16</td>
<td>RCNTR</td>
<td>16</td>
<td>&nbsp;</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td>17</td>
<td>KUNNR</td>
<td>17</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>18</td>
<td>LIFNR</td>
<td>18</td>
<td>&nbsp;</td>
<td>LIFNR</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商或债权人的帐号</td>
</tr>
<tr class="cell">
<td>19</td>
<td>MWSKZ</td>
<td>19</td>
<td>&nbsp;</td>
<td>MWSKZ</td>
<td>MWSKZ</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>销售/购买税代码</td>
</tr>
<tr class="cell">
<td>20</td>
<td>HBKID</td>
<td>20</td>
<td>&nbsp;</td>
<td>HBKID</td>
<td>HBKID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>开户行简短代码</td>
</tr>
<tr class="cell">
<td>21</td>
<td>HKTID</td>
<td>21</td>
<td>&nbsp;</td>
<td>HKTID</td>
<td>HKTID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>帐户细目的代码</td>
</tr>
<tr class="cell">
<td>22</td>
<td>RASSC</td>
<td>22</td>
<td>&nbsp;</td>
<td>RASSC</td>
<td>RCOMP</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>贸易合作伙伴的公司标识</td>
</tr>
<tr class="cell">
<td>23</td>
<td>RSTGR</td>
<td>23</td>
<td>&nbsp;</td>
<td>RSTGR</td>
<td>RSTGR</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>付款原因代码</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZCPLB</td>
<td>24</td>
<td>&nbsp;</td>
<td>ZE_CPLB</td>
<td>ZD_CPLB</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>EAS产品类别明细</td>
</tr>
<tr class="cell">
<td>25</td>
<td>SENDED</td>
<td>25</td>
<td>&nbsp;</td>
<td>FLAG</td>
<td>FLAG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>一般标记</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>H</td>
<td>&nbsp;</td>
<td>贷方</td>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>S</td>
<td>&nbsp;</td>
<td>借方</td>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>X</td>
<td>&nbsp;</td>
<td>标志已设置（事件引发）</td>
</tr>
<tr class="cell">
<td>FLAG</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>未设置标记</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>