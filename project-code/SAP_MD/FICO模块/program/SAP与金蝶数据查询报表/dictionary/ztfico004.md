<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO004</h2>
<h3>Description: 辅助核算自建表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>SAKNR</td>
<td>2</td>
<td>X</td>
<td>SAKNR</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZCSAKNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZECSAKNR</td>
<td>ZDCSAKNR</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>国家一级科目</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>TXT50</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZETXT</td>
<td>ZDTXT</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>科目描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZKSAKNR</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZEKSAKNR</td>
<td>ZDKSAKNR</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶末级科目</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZKTXT50</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZEKTXT</td>
<td>ZDKTXT</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>金蝶末级科目描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZKFL</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZEKFL</td>
<td>ZDKFL</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶辅助核算分类</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZKFLMS</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZEKFLMS</td>
<td>ZDKFLMS</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>金蝶辅助核算分类描述</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZKFUXM</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZEKFUXM</td>
<td>ZDKFUXM</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>辅助核算项目</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZKFUTEXT</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZEKFUTEXT</td>
<td>ZDKFUTEXT</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>辅助核算描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>