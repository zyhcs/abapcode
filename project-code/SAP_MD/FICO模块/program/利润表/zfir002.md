<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFIR002</h2>
<h3> Description: 利润表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFIR002<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
*****************************************************************<br/>
*&nbsp;&nbsp;System&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;ERP项目<br/>
*&nbsp;&nbsp;Module&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;FICO<br/>
*&nbsp;&nbsp;Program&nbsp;ID&nbsp;&nbsp;:&nbsp;&nbsp;ZFIR002<br/>
*&nbsp;&nbsp;Program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;ZFIR002<br/>
*&nbsp;&nbsp;Author&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;LIWS<br/>
*&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;20180520<br/>
*&nbsp;&nbsp;Description&nbsp;:&nbsp;&nbsp;利润表<br/>
*****************************************************************<br/>
*&nbsp;&nbsp;Modified&nbsp;Recorder&nbsp;:<br/>
*&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C#NO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Author&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Content<br/>
*&nbsp;&nbsp;-----------&nbsp;&nbsp;-------&nbsp;&nbsp;&nbsp;&nbsp;------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------<br/>
*&nbsp;&nbsp;修改日期&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C票或变更文档ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改者&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改内容<br/>
*****************************************************************<br/>
*-----------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfir002 .<br/>
<br/>
TABLES: faglflext.<br/>
TABLES: sscrfields .<br/>
TYPE-POOLS: icon .<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------------<br/>
*---TYPES<br/>
*-----------------------------------------------------------------------------<br/>
</div>
<div class="code">
TYPES: BEGIN OF typ_fag,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbukrs&nbsp;LIKE&nbsp;faglflext-rbukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ryear&nbsp;&nbsp;LIKE&nbsp;faglflext-ryear,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rpmax&nbsp;&nbsp;LIKE&nbsp;faglflext-rpmax,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;racct&nbsp;&nbsp;LIKE&nbsp;faglflext-racct,&nbsp;&nbsp;"科目号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rfarea&nbsp;LIKE&nbsp;faglflext-rfarea,&nbsp;"功能范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;LIKE&nbsp;faglflext-rtcur,&nbsp;&nbsp;"货币码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drcrk&nbsp;&nbsp;LIKE&nbsp;faglflext-drcrk,&nbsp;&nbsp;"借贷标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt&nbsp;&nbsp;LIKE&nbsp;faglflext-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl01&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl02&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl02,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl03&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl03,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl04&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl04,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl05&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl05,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl06&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl06,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl07&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl07,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl08&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl08,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl09&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl09,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl10&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl11&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl11,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl12&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl13&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl13,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl14&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl14,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl15&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl15,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsl16&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl16,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_fag&nbsp;.<br/>
<br/>
TYPES: BEGIN OF typ_dmbtr ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcolumn&nbsp;TYPE&nbsp;zfit001-zcolumn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit001-zrow,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nmhsl&nbsp;&nbsp;&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;&nbsp;"当期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nyhsl&nbsp;&nbsp;&nbsp;LIKE&nbsp;faglflext-hsl01,&nbsp;&nbsp;&nbsp;"本年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmhsl&nbsp;&nbsp;&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;"上年同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lyhsl&nbsp;&nbsp;&nbsp;LIKE&nbsp;faglflext-hslvt,&nbsp;&nbsp;&nbsp;"上年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_dmbtr&nbsp;.<br/>
<br/>
TYPES: BEGIN OF typ_dmbtr1 ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ztext&nbsp;TYPE&nbsp;zfit001-ztext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nmhsl&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;"当期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nyhsl&nbsp;LIKE&nbsp;faglflext-hsl01,&nbsp;"本年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmhsl&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;"上年同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lyhsl&nbsp;LIKE&nbsp;faglflext-hslvt,&nbsp;"上年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;slbox&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;typ_dmbtr1&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*DATA:&nbsp;BEGIN&nbsp;OF&nbsp;gs_fit003,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcolumn&nbsp;LIKE&nbsp;&nbsp;zfit003-zcolumn,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;zfit003-zrow,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ztext&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;zfit003-ztext,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tsl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;&nbsp;zfit003-tsl,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gs_fit003,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_fit003&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;gs_fit003.<br/>
*-----------------------------------------------------------------------*<br/>
</div>
<div class="code">
"数据定义<br/>
DATA: gt_fag TYPE STANDARD TABLE OF typ_fag,     "<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fag&nbsp;TYPE&nbsp;typ_fag.<br/>
<br/>
DATA: gt_fag1 TYPE STANDARD TABLE OF typ_fag,     "<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fag1&nbsp;TYPE&nbsp;typ_fag.<br/>
<br/>
DATA: gt_fit001 TYPE STANDARD TABLE OF zfit001,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fit001&nbsp;TYPE&nbsp;zfit001.<br/>
FIELD-SYMBOLS: &lt;gs_fit001&gt; TYPE zfit001.<br/>
<br/>
DATA: gt_dmbtr TYPE STANDARD TABLE OF typ_dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr&nbsp;TYPE&nbsp;typ_dmbtr.<br/>
<br/>
DATA: gt_dmbtr1 TYPE STANDARD TABLE OF typ_dmbtr1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr1&nbsp;TYPE&nbsp;typ_dmbtr1.<br/>
<br/>
DATA: gt_ZFIT003 LIKE TABLE OF zfit003 WITH HEADER LINE.  "add by frank20180718<br/>
<br/>
DATA:BEGIN OF it_zfit13 OCCURS 0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ztype&nbsp;&nbsp;LIKE&nbsp;zfit001-ztype,&nbsp;&nbsp;"IS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;LIKE&nbsp;zfit001-zrow,&nbsp;&nbsp;"行<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;znmhsl&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;&nbsp;"当期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;znyhsl&nbsp;LIKE&nbsp;faglflext-hsl01,&nbsp;&nbsp;&nbsp;"本年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zlmhsl&nbsp;TYPE&nbsp;faglflext-hsl01,&nbsp;"上年同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zlyhsl&nbsp;LIKE&nbsp;faglflext-hslvt,&nbsp;&nbsp;&nbsp;"上年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;it_zfit13.<br/>
<br/>
DATA: tp_tcode LIKE sy-tcode .<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*ALV&nbsp;相关参数<br/>
<br/>
</div>
<div class="code">
DATA: gs_fieldcatalog TYPE lvc_s_fcat,     "显示数据列内表工作区域<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_fieldcatalog&nbsp;TYPE&nbsp;lvc_t_fcat,&nbsp;&nbsp;"显示数据列内表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_alv_sort&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_t_sort&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_layout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_pos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
DATA: gt_header TYPE STANDARD TABLE OF slis_listheader,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_header&nbsp;TYPE&nbsp;slis_listheader.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------<br/>
</div>
<div class="code">
DATA:<br/>
&nbsp;&nbsp;g_excel&nbsp;&nbsp;TYPE&nbsp;ole2_object,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"EXCEL&nbsp;object<br/>
&nbsp;&nbsp;g_books&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;g_book&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"book<br/>
&nbsp;&nbsp;g_sheets&nbsp;TYPE&nbsp;ole2_object,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"sheet<br/>
&nbsp;&nbsp;g_cell&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"cell<br/>
&nbsp;&nbsp;g_font&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;g_rows&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;g_cols&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"columns<br/>
<br/>
DATA: g_butxt     TYPE t001-butxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;g_excelname&nbsp;TYPE&nbsp;rlgrap-filename.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;选择屏幕<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS&nbsp;p_bukrs&nbsp;&nbsp;&nbsp;LIKE&nbsp;faglflext-rbukrs&nbsp;OBLIGATORY&nbsp;MEMORY&nbsp;ID&nbsp;buk&nbsp;.&nbsp;&nbsp;&nbsp;"&nbsp;公司代码<br/>
&nbsp;&nbsp;PARAMETERS&nbsp;p_ryear&nbsp;&nbsp;&nbsp;LIKE&nbsp;faglflext-ryear&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;sy-datum+0(4)."&nbsp;会计年度<br/>
&nbsp;&nbsp;PARAMETERS&nbsp;p_rpmax&nbsp;&nbsp;&nbsp;LIKE&nbsp;faglflext-rpmax&nbsp;OBLIGATORY&nbsp;DEFAULT&nbsp;sy-datum+4(2)&nbsp;.&nbsp;"&nbsp;会计期间<br/>
<br/>
SELECTION-SCREEN END OF BLOCK blk1.<br/>
<br/>
SELECTION-SCREEN FUNCTION KEY 1.<br/>
</div>
<div class="codeComment">
*SELECTION-SCREEN&nbsp;FUNCTION&nbsp;KEY&nbsp;2.<br/>
</div>
<div class="code">
DATA: functxt TYPE smp_dyntxt.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------------<br/>
*---事件处理<br/>
*-----------------------------------------------------------------------------<br/>
</div>
<div class="code">
INITIALIZATION .<br/>
</div>
<div class="codeComment">
***改变按钮名称<br/>
</div>
<div class="code">
&nbsp;&nbsp;functxt-icon_text&nbsp;&nbsp;=&nbsp;'配置数据维护'&nbsp;.<br/>
&nbsp;&nbsp;functxt-icon_id&nbsp;&nbsp;&nbsp;=&nbsp;icon_toggle_display_change.<br/>
&nbsp;&nbsp;sscrfields-functxt_01&nbsp;=&nbsp;functxt&nbsp;.<br/>
&nbsp;&nbsp;sscrfields-ucomm&nbsp;=&nbsp;'FC01'&nbsp;.<br/>
<br/>
&nbsp;&nbsp;functxt-icon_text&nbsp;&nbsp;=&nbsp;'期初数据维护'&nbsp;.<br/>
&nbsp;&nbsp;functxt-icon_id&nbsp;&nbsp;&nbsp;=&nbsp;icon_toggle_display_change.<br/>
&nbsp;&nbsp;sscrfields-functxt_02&nbsp;=&nbsp;functxt&nbsp;.<br/>
&nbsp;&nbsp;sscrfields-ucomm&nbsp;=&nbsp;'FC02'&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;tp_tcode&nbsp;.<br/>
&nbsp;&nbsp;tp_tcode&nbsp;=&nbsp;sy-tcode&nbsp;.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;initial_screen&nbsp;.<br/>
<br/>
AT SELECTION-SCREEN .<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-ucomm&nbsp;=&nbsp;'FC01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'ZTYPE'&nbsp;FIELD&nbsp;'IS'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'ZFIU001'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*<br/>
*&nbsp;&nbsp;IF&nbsp;SY-UCOMM&nbsp;=&nbsp;'FC02'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'ZTYPE'&nbsp;FIELD&nbsp;'IS'&nbsp;.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'ZFIU002'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;&nbsp;.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
START-OF-SELECTION .<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_year&nbsp;TYPE&nbsp;&nbsp;faglflext-ryear&nbsp;.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;tp_year&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"用于取上年同期<br/>
&nbsp;&nbsp;tp_year&nbsp;=&nbsp;p_ryear&nbsp;-&nbsp;1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;check_authority&nbsp;.&nbsp;&nbsp;"权限校验<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_get_data&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;gt_fit001[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;sub_deal_data&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'没有利润表对应配置数据!!'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
END-OF-SELECTION .<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_create_data&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_show_data&nbsp;.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;INITIAL_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM initial_screen .<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;CHECK_INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM check_authority .<br/>
<br/>
&nbsp;&nbsp;AUTHORITY-CHECK&nbsp;OBJECT&nbsp;'F_BKPF_BUK'<br/>
&nbsp;&nbsp;ID&nbsp;'BUKRS'&nbsp;FIELD&nbsp;p_bukrs<br/>
&nbsp;&nbsp;ID&nbsp;'ACTVT'&nbsp;FIELD&nbsp;'03'.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;Implement&nbsp;a&nbsp;suitable&nbsp;exception&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'没有查看该公司的权限'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SUB_GET_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM sub_get_data .<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_cond&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_field&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;5.<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;tp_perio&nbsp;TYPE&nbsp;char3&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_monat&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;2&nbsp;VALUE&nbsp;'00'.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;tp_perio&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;p_rpmax&nbsp;=&nbsp;12&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;tp_perio&nbsp;=&nbsp;p_rpmax&nbsp;+&nbsp;4&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;tp_perio&nbsp;=&nbsp;p_rpmax&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;tp_cond,tp_monat&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"整合查询条件<br/>
&nbsp;&nbsp;DO&nbsp;tp_perio&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;tp_monat&nbsp;=&nbsp;tp_monat&nbsp;+&nbsp;1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SHIFT&nbsp;tp_monat&nbsp;RIGHT&nbsp;DELETING&nbsp;TRAILING&nbsp;space&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OVERLAY&nbsp;tp_monat&nbsp;WITH&nbsp;'00'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;tp_field&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'HSL'&nbsp;tp_monat&nbsp;INTO&nbsp;&nbsp;tp_field&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;tp_cond&nbsp;tp_field&nbsp;INTO&nbsp;tp_cond&nbsp;SEPARATED&nbsp;BY&nbsp;space&nbsp;.<br/>
&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;'RBUKRS'&nbsp;'RYEAR'&nbsp;'RPMAX'&nbsp;'RACCT'&nbsp;'RTCUR'&nbsp;'DRCRK'&nbsp;'RFAREA'&nbsp;'HSLVT'<br/>
&nbsp;&nbsp;tp_cond&nbsp;INTO&nbsp;tp_cond&nbsp;SEPARATED&nbsp;BY&nbsp;space&nbsp;.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;(tp_cond)&nbsp;&nbsp;&nbsp;"查询发生额数据<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gt_fag<br/>
&nbsp;&nbsp;FROM&nbsp;faglflext<br/>
&nbsp;&nbsp;WHERE&nbsp;rbukrs&nbsp;=&nbsp;p_bukrs<br/>
&nbsp;&nbsp;AND&nbsp;(&nbsp;ryear&nbsp;&nbsp;=&nbsp;p_ryear&nbsp;OR&nbsp;ryear&nbsp;=&nbsp;tp_year&nbsp;)<br/>
&nbsp;&nbsp;AND&nbsp;rldnr&nbsp;=&nbsp;'0L'.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;*<br/>
&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gt_fit001<br/>
&nbsp;&nbsp;FROM&nbsp;zfit001<br/>
&nbsp;&nbsp;WHERE&nbsp;ztype&nbsp;=&nbsp;'IS'<br/>
&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;langu&nbsp;=&nbsp;sy-langu&nbsp;.&nbsp;&nbsp;"只获取IS类<br/>
<br/>
&nbsp;&nbsp;SORT&nbsp;gt_fit001&nbsp;BY&nbsp;ztype&nbsp;zcolumn&nbsp;zrow&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:gt_ZFIT003,gt_ZFIT003[].<br/>
&nbsp;&nbsp;SELECT&nbsp;*&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gt_ZFIT003<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zfit003&nbsp;WHERE&nbsp;rbukrs&nbsp;=&nbsp;&nbsp;p_bukrs.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;"获取17年6月份之前的数据<br/>
*&nbsp;&nbsp;SELECT&nbsp;&nbsp;zcolumn<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tsl<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gt_fit003<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;zfit003<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_bukrs<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;gjahr&nbsp;=&nbsp;p_ryear<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;MONAT&nbsp;=&nbsp;P_RPMAX<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ztype&nbsp;=&nbsp;'IS'.<br/>
</div>
<div class="code">
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SUB_DEAL_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM sub_deal_data .<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS:&nbsp;&lt;hsl&gt;&nbsp;TYPE&nbsp;any&nbsp;.<br/>
&nbsp;&nbsp;RANGES:&nbsp;r_saknr&nbsp;FOR&nbsp;faglflext-racct.<br/>
&nbsp;&nbsp;RANGES:&nbsp;r_rfarea&nbsp;FOR&nbsp;zfit001-frfarea&nbsp;.<br/>
&nbsp;&nbsp;RANGES:&nbsp;r_drcrk&nbsp;FOR&nbsp;faglflext-drcrk&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gt_dmbtr[],it_zfit13,it_zfit13[].<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_fit001&nbsp;ASSIGNING&nbsp;&lt;gs_fit001&gt;&nbsp;."根据配置表数据&nbsp;&nbsp;把取出的发生额加以汇总&nbsp;&nbsp;得到报表项数据<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;&lt;gs_fit001&gt;&nbsp;TO&nbsp;gs_dmbtr&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-formula&nbsp;=&nbsp;''&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"不是通过公式计算<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;(&nbsp;(&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;OR&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;)&nbsp;AND<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&lt;gs_fit001&gt;-zjfbs&nbsp;&lt;&gt;&nbsp;''&nbsp;OR&nbsp;&lt;gs_fit001&gt;-zdfbs&nbsp;&lt;&gt;&nbsp;''&nbsp;)&nbsp;)&nbsp;&nbsp;.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------------<br/>
*---组织整合条件<br/>
*-----------------------------------------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:r_saknr[],r_drcrk[],r_rfarea[]&nbsp;.&nbsp;&nbsp;&nbsp;"资产负债表没有功能范围<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*“科目范围<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;r_saknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;=&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_saknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;=&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_saknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_saknr-high&nbsp;=&nbsp;&lt;gs_fit001&gt;-tsaknr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_saknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*"功能范围<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;r_rfarea.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;=&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea(3)&nbsp;=&nbsp;'IEQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;=&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea(3)&nbsp;=&nbsp;'IEQ'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_rfarea-high&nbsp;=&nbsp;&lt;gs_fit001&gt;-trfarea.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*”借贷标识<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:r_drcrk&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-zjfbs&nbsp;=&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_drcrk(3)&nbsp;=&nbsp;'IEQ'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_drcrk-low&nbsp;=&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_drcrk&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-zdfbs&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_drcrk(3)&nbsp;=&nbsp;'IEQ'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r_drcrk-low&nbsp;=&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;r_drcrk&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ryear&nbsp;=&nbsp;'2017'&nbsp;AND&nbsp;p_rpmax&nbsp;LT&nbsp;'6'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_fit003&nbsp;INTO&nbsp;gs_fit003&nbsp;WHERE&nbsp;zcolumn&nbsp;=&nbsp;&lt;gs_fit001&gt;-zcolumn&nbsp;AND&nbsp;zrow&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;gs_fit003-tsl&nbsp;TO&nbsp;gs_dmbtr-nmhsl&nbsp;.&nbsp;&nbsp;"当期<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&nbsp;gs_fit003-tsl&nbsp;TO&nbsp;gs_dmbtr-nyhsl&nbsp;.&nbsp;&nbsp;"当年累计<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_ryear&nbsp;=&nbsp;'2017'&nbsp;AND&nbsp;p_rpmax&nbsp;&gt;=&nbsp;'6'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_fit003&nbsp;INTO&nbsp;gs_fit003&nbsp;WHERE&nbsp;zcolumn&nbsp;=&nbsp;&lt;gs_fit001&gt;-zcolumn&nbsp;AND&nbsp;zrow&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&nbsp;gs_fit003-tsl&nbsp;TO&nbsp;gs_dmbtr-nyhsl&nbsp;.&nbsp;&nbsp;"当年累计<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_fag&nbsp;INTO&nbsp;gs_fag&nbsp;WHERE&nbsp;racct&nbsp;IN&nbsp;r_saknr&nbsp;AND&nbsp;rfarea&nbsp;IN&nbsp;r_rfarea&nbsp;AND&nbsp;drcrk&nbsp;IN&nbsp;r_drcrk&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gs_fag-ryear&nbsp;=&nbsp;p_ryear&nbsp;.&nbsp;&nbsp;"当年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;16&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;(&nbsp;sy-index&nbsp;+&nbsp;8&nbsp;)&nbsp;OF&nbsp;STRUCTURE&nbsp;gs_fag&nbsp;TO&nbsp;&lt;hsl&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_rpmax&nbsp;=&nbsp;12&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-index&nbsp;&gt;=&nbsp;12&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-nmhsl&nbsp;.&nbsp;&nbsp;"当期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-index&nbsp;=&nbsp;p_rpmax&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-nmhsl&nbsp;."当期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-nyhsl&nbsp;.&nbsp;&nbsp;"当年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.&nbsp;&nbsp;"上年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;16&nbsp;TIMES.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;(&nbsp;sy-index&nbsp;+&nbsp;8&nbsp;)&nbsp;OF&nbsp;STRUCTURE&nbsp;gs_fag&nbsp;TO&nbsp;&lt;hsl&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&nbsp;p_rpmax&nbsp;=&nbsp;12&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-index&nbsp;&gt;=&nbsp;12&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-lmhsl&nbsp;.&nbsp;"上年同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-index&nbsp;=&nbsp;p_rpmax&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-lmhsl&nbsp;."上年同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&lt;hsl&gt;&nbsp;TO&nbsp;gs_dmbtr-lyhsl&nbsp;.&nbsp;"上年累计<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-reversal&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-lmhsl&nbsp;=&nbsp;0&nbsp;-&nbsp;gs_dmbtr-lmhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nmhsl&nbsp;=&nbsp;0&nbsp;-&nbsp;gs_dmbtr-nmhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nyhsl&nbsp;=&nbsp;0&nbsp;-&nbsp;gs_dmbtr-nyhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-lyhsl&nbsp;=&nbsp;0&nbsp;-&nbsp;gs_dmbtr-lyhsl.&nbsp;&nbsp;"add&nbsp;by&nbsp;zhangcs&nbsp;20180709<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;gs_dmbtr&nbsp;INTO&nbsp;gt_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;gs_dmbtr&nbsp;INTO&nbsp;gt_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公式计算项<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;caculate_results&nbsp;CHANGING&nbsp;&lt;gs_fit001&gt;-formula&nbsp;gs_dmbtr-lmhsl&nbsp;"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nmhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nyhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-lyhsl.&nbsp;&nbsp;&nbsp;"有计算公式的行没有分科目号段照成多行的问题&nbsp;&nbsp;&nbsp;modify&nbsp;by&nbsp;frank&nbsp;20180709<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLLECT&nbsp;gs_dmbtr&nbsp;INTO&nbsp;gt_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"add&nbsp;by&nbsp;frank&nbsp;20180718<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-ztype&nbsp;=&nbsp;&lt;gs_fit001&gt;-ztype.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-zrow&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-bukrs&nbsp;IS&nbsp;INITIAL.&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;ZHANGCS&nbsp;20180925<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;gs_fit001&gt;-bukrs&nbsp;=&nbsp;p_bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_ZFIT003&nbsp;WHERE&nbsp;rbukrs&nbsp;=&nbsp;&lt;gs_fit001&gt;-bukrs&nbsp;AND&nbsp;ztype&nbsp;=&nbsp;&lt;gs_fit001&gt;-ztype&nbsp;AND&nbsp;zrow&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;AND&nbsp;ryear&nbsp;=&nbsp;p_ryear.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gt_ZFIT003-rpmax&nbsp;=&nbsp;p_rpmax.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-znmhsl&nbsp;=&nbsp;gt_ZFIT003-hslvt.&nbsp;"本月同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-znyhsl&nbsp;=&nbsp;it_zfit13-znyhsl&nbsp;+&nbsp;gt_ZFIT003-hslvt.&nbsp;&nbsp;"累加,同年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_ZFIT003&nbsp;WHERE&nbsp;rbukrs&nbsp;=&nbsp;&lt;gs_fit001&gt;-bukrs&nbsp;AND&nbsp;ztype&nbsp;=&nbsp;&lt;gs_fit001&gt;-ztype&nbsp;AND&nbsp;zrow&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;AND&nbsp;ryear&nbsp;=&nbsp;tp_year.&nbsp;&nbsp;"上一年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gt_ZFIT003-rpmax&nbsp;=&nbsp;p_rpmax.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-zlmhsl&nbsp;=&nbsp;gt_ZFIT003-hslvt.&nbsp;"本月同期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_zfit13-zlyhsl&nbsp;=&nbsp;it_zfit13-zlyhsl&nbsp;+&nbsp;gt_ZFIT003-hslvt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;it_zfit13.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;it_zfit13.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;HEZZ&nbsp;AT&nbsp;20170628&nbsp;再次计算公式，避免一些公式取后面的值失败<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;&nbsp;gt_fit001&nbsp;ASSIGNING&nbsp;&lt;gs_fit001&gt;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-formula&nbsp;NE&nbsp;''&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"公式计算项<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS:&nbsp;&lt;f_dmbtr&gt;&nbsp;TYPE&nbsp;typ_dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_dmbtr&nbsp;ASSIGNING&nbsp;&lt;f_dmbtr&gt;&nbsp;WITH&nbsp;KEY&nbsp;zcolumn&nbsp;=&nbsp;&lt;gs_fit001&gt;-zcolumn<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;gs_fit001&gt;-zrow.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;&lt;f_dmbtr&gt;-lmhsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-nmhsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-nyhsl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-lyhsl.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;caculate_results&nbsp;CHANGING&nbsp;&lt;gs_fit001&gt;-formula&nbsp;&lt;f_dmbtr&gt;-lmhsl&nbsp;"gs_dmbtr-lyhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-nmhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-nyhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;f_dmbtr&gt;-lyhsl&nbsp;.&nbsp;&nbsp;&nbsp;"有计算公式的行没有分科目号段照成多行的问题<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;"ADD&nbsp;BY&nbsp;FRANK&nbsp;20180718<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_dmbtr&nbsp;INTO&nbsp;gs_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;it_zfit13&nbsp;WITH&nbsp;KEY&nbsp;zrow&nbsp;=&nbsp;gs_dmbtr-zrow.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_zfit13-znmhsl&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nmhsl&nbsp;=&nbsp;gs_dmbtr-nmhsl&nbsp;+&nbsp;it_zfit13-znmhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_zfit13-znyhsl&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-nyhsl&nbsp;=&nbsp;gs_dmbtr-nyhsl&nbsp;+&nbsp;it_zfit13-znyhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_zfit13-zlmhsl&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-lmhsl&nbsp;=&nbsp;gs_dmbtr-lmhsl&nbsp;+&nbsp;it_zfit13-zlmhsl.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_zfit13-zlyhsl&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr-lyhsl&nbsp;=&nbsp;gs_dmbtr-lyhsl&nbsp;+&nbsp;it_zfit13-zlyhsl.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_dmbtr&nbsp;FROM&nbsp;gs_dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_dmbtr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;CACULATE_RESULTS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GS_PZB01_FORMULA&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM caculate_results CHANGING p_formula p_lmhsl  p_nmhsl p_nyhsl p_lyhsl ."p_lyhsl<br/>
&nbsp;&nbsp;DATA:&nbsp;wa_dmbtr&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;formula_len&nbsp;TYPE&nbsp;&nbsp;i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;last&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_char&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_row&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;3.<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_lmhsl&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_lyhsl&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_nmhsl&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_nyhsl&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_value&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_value&nbsp;TYPE&nbsp;f&nbsp;.<br/>
<br/>
&nbsp;&nbsp;formula_len&nbsp;=&nbsp;strlen(&nbsp;p_formula&nbsp;)&nbsp;.&nbsp;&nbsp;&nbsp;"计算公式长度<br/>
<br/>
&nbsp;&nbsp;CLEAR:tp_lmhsl,tp_lyhsl,tp_nmhsl,tp_nyhsl,tp_char,l_row&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CONDENSE&nbsp;p_formula&nbsp;NO-GAPS&nbsp;.<br/>
&nbsp;&nbsp;DO&nbsp;formula_len&nbsp;TIMES.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;last&nbsp;=&nbsp;sy-index&nbsp;-&nbsp;1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;tp_char&nbsp;=&nbsp;p_formula+last(1)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;tp_char&nbsp;CO&nbsp;'0123456789'&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"为数字<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;l_row&nbsp;tp_char&nbsp;INTO&nbsp;l_row&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;tp_char&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;tp_char&nbsp;CO&nbsp;'+-*/'&nbsp;OR&nbsp;sy-index&nbsp;=&nbsp;formula_len&nbsp;."为计算符号<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SHIFT&nbsp;l_row&nbsp;RIGHT&nbsp;DELETING&nbsp;TRAILING&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OVERLAY&nbsp;l_row&nbsp;WITH&nbsp;'000'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;wa_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_dmbtr&nbsp;INTO&nbsp;wa_dmbtr&nbsp;WITH&nbsp;KEY&nbsp;zrow&nbsp;=&nbsp;l_row&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:tp_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_value&nbsp;=&nbsp;wa_dmbtr-lmhsl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'&nbsp;&nbsp;&nbsp;"负数负号前置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;tp_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;tp_lmhsl&nbsp;'('&nbsp;tp_value&nbsp;')'&nbsp;&nbsp;tp_char&nbsp;INTO&nbsp;tp_lmhsl&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:tp_value&nbsp;.&nbsp;&nbsp;"modify&nbsp;by&nbsp;zhangcs&nbsp;20180709<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_value&nbsp;=&nbsp;wa_dmbtr-lyhsl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'&nbsp;&nbsp;&nbsp;"负数负号前置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;tp_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;tp_lyhsl&nbsp;'('&nbsp;tp_value&nbsp;')'&nbsp;&nbsp;tp_char&nbsp;INTO&nbsp;tp_lyhsl&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:tp_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_value&nbsp;=&nbsp;wa_dmbtr-nmhsl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'&nbsp;&nbsp;&nbsp;"负数负号前置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;tp_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;tp_nmhsl&nbsp;'('&nbsp;tp_value&nbsp;')'&nbsp;&nbsp;tp_char&nbsp;INTO&nbsp;tp_nmhsl&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:tp_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tp_value&nbsp;=&nbsp;wa_dmbtr-nyhsl&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'&nbsp;&nbsp;&nbsp;"负数负号前置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;tp_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;tp_nyhsl&nbsp;'('&nbsp;tp_value&nbsp;')'&nbsp;&nbsp;tp_char&nbsp;INTO&nbsp;tp_nyhsl&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_row&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;tp_lmhsl&nbsp;&lt;&gt;&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CHECK_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_lmhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'EVAL_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_lmhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;&nbsp;&nbsp;=&nbsp;l_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_lmhsl&nbsp;=&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;tp_lyhsl&nbsp;&lt;&gt;&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CHECK_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_lyhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'EVAL_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_lyhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;&nbsp;&nbsp;=&nbsp;l_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_lyhsl&nbsp;=&nbsp;l_value&nbsp;.&nbsp;&nbsp;"modify&nbsp;by&nbsp;frank&nbsp;20180709<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;tp_nmhsl&nbsp;&lt;&gt;&nbsp;''&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CHECK_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_nmhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'EVAL_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_nmhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;&nbsp;&nbsp;=&nbsp;l_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_nmhsl&nbsp;=&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;tp_nyhsl&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CHECK_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_nyhsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'EVAL_FORMULA'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formula&nbsp;=&nbsp;tp_nyhsl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;&nbsp;&nbsp;=&nbsp;l_value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_nyhsl&nbsp;=&nbsp;l_value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;OUTPUT_EXCEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM output_excel .<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;get_excel_modle.<br/>
&nbsp;&nbsp;CHECK&nbsp;g_excelname&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;PERFORM&nbsp;create_oleobj.<br/>
&nbsp;&nbsp;PERFORM&nbsp;set_exceldata.<br/>
&nbsp;&nbsp;PERFORM&nbsp;excel_quit&nbsp;.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;GET_EXCEL_MODLE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM get_excel_modle .<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_objdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;wwwdatatab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_mime&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;w3mime,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fullpath&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'D:\',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'D:\',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_destination&nbsp;LIKE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_objnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-subrc,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_errtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_dest&nbsp;LIKE&nbsp;sapb-sappfad.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;g_butxt.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;butxt<br/>
&nbsp;&nbsp;INTO&nbsp;g_butxt<br/>
&nbsp;&nbsp;FROM&nbsp;t001<br/>
&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_bukrs&nbsp;.<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;g_butxt&nbsp;'_利润表'&nbsp;INTO&nbsp;l_filename.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;"调用保存对话框<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'XLS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'D:\'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;AND&nbsp;l_fullpath&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_destination&nbsp;=&nbsp;l_fullpath.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;relid&nbsp;objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;l_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;srtf2&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;relid&nbsp;=&nbsp;'MI'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;objid&nbsp;=&nbsp;'ZFIR002'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDSELECT..<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;l_destination<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_rc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;temp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_dest.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_rc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'模板文件：'&nbsp;l_objdata+2(13)&nbsp;'下载失败'&nbsp;INTO&nbsp;l_errtxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;l_errtxt&nbsp;TYPE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;g_excelname&nbsp;=&nbsp;l_destination.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;CREATE_OLEOBJ<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM create_oleobj .<br/>
<br/>
&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;g_excel&nbsp;'EXCEL.APPLICATION'.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;&nbsp;'VISIBLE'&nbsp;=&nbsp;1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"不显示窗口<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;&nbsp;'SCREENUPDATING'&nbsp;=&nbsp;0.&nbsp;&nbsp;"关闭刷新,增加填数据的速度<br/>
<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;'WORKBOOKS'&nbsp;=&nbsp;g_books.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_books&nbsp;'OPEN'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#1&nbsp;=&nbsp;g_excelname.<br/>
<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;'WORKSHEETS'&nbsp;=&nbsp;g_sheets&nbsp;.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_excel&nbsp;'WORKSHEETS'&nbsp;=&nbsp;g_sheets<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;#1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_sheets&nbsp;'ACTIVATE'.<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;'ACTIVESHEET'&nbsp;=&nbsp;g_sheets.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SET_EXCELDATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM set_exceldata .<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;tp_date&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;20,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;today&nbsp;&nbsp;&nbsp;LIKE&nbsp;sy-datum.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_row&nbsp;&nbsp;TYPE&nbsp;i&nbsp;.<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS:&lt;data&gt;&nbsp;TYPE&nbsp;any&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_str&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;TYPES:&nbsp;BEGIN&nbsp;OF&nbsp;t_senderline,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;line(4096)&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;t_senderline&nbsp;.<br/>
&nbsp;&nbsp;TYPES:t_sender&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t_senderline&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;it_excel_tab&nbsp;&nbsp;TYPE&nbsp;t_sender,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_excel_line&nbsp;TYPE&nbsp;t_senderline.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_separator&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;CLASS&nbsp;cl_abap_char_utilities&nbsp;DEFINITION&nbsp;LOAD.<br/>
&nbsp;&nbsp;l_separator&nbsp;=&nbsp;cl_abap_char_utilities=&gt;horizontal_tab.<br/>
<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;'编制单位：'&nbsp;g_butxt&nbsp;INTO&nbsp;g_butxt.<br/>
&nbsp;&nbsp;PERFORM&nbsp;fill_cell&nbsp;USING&nbsp;3&nbsp;1&nbsp;g_butxt&nbsp;''&nbsp;''&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;'所属期：'&nbsp;p_ryear&nbsp;'-'&nbsp;p_rpmax+1(2)&nbsp;INTO&nbsp;tp_date.<br/>
&nbsp;&nbsp;PERFORM&nbsp;fill_cell&nbsp;USING&nbsp;3&nbsp;2&nbsp;tp_date&nbsp;''&nbsp;''&nbsp;.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_dmbtr1&nbsp;INTO&nbsp;gs_dmbtr1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ws_excel_line&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DO&nbsp;3&nbsp;TIMES.&nbsp;&nbsp;"modify&nbsp;by&nbsp;zhangcs&nbsp;20180709<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-index&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;(&nbsp;sy-index&nbsp;+&nbsp;2&nbsp;)&nbsp;OF&nbsp;STRUCTURE&nbsp;gs_dmbtr1&nbsp;TO&nbsp;&lt;data&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;(&nbsp;sy-index&nbsp;+&nbsp;1&nbsp;)&nbsp;OF&nbsp;STRUCTURE&nbsp;gs_dmbtr1&nbsp;TO&nbsp;&lt;data&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_str&nbsp;=&nbsp;&lt;data&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"金额字段负号前置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;l_str.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ws_excel_line&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ws_excel_line&nbsp;l_str&nbsp;INTO&nbsp;ws_excel_line&nbsp;SEPARATED&nbsp;BY&nbsp;l_separator&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_excel_line&nbsp;=&nbsp;l_str.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_str&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDDO.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ws_excel_line&nbsp;TO&nbsp;it_excel_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_count&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;lv_count&nbsp;=&nbsp;lines(&nbsp;gt_dmbtr1[]&nbsp;)&nbsp;+&nbsp;4.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_clipboard_to_excel&nbsp;TABLES&nbsp;it_excel_tab&nbsp;USING&nbsp;5&nbsp;3&nbsp;lv_count&nbsp;5&nbsp;."7&nbsp;&nbsp;"输出到excel<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;EXCEL_QUIT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM excel_quit .<br/>
<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;'SCREENUPDATING'&nbsp;=&nbsp;1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"打开刷新<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;g_excel&nbsp;'ActiveWorkbook'&nbsp;=&nbsp;g_books.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_books&nbsp;'SAVE'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_excel&nbsp;'QUIT'.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;g_books.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;g_excel.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FILL_CELL<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
FORM fill_cell  USING    a b c d e.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_excel&nbsp;'CELLS'&nbsp;&nbsp;=&nbsp;g_cell&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单元格位置<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;#1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;a<br/>
&nbsp;&nbsp;&nbsp;&nbsp;#2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;b.<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_cell&nbsp;'VALUE'&nbsp;=&nbsp;c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单元格内容<br/>
<br/>
&nbsp;&nbsp;GET&nbsp;PROPERTY&nbsp;OF&nbsp;g_cell&nbsp;'FONT'&nbsp;=&nbsp;g_font.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_cell&nbsp;'FONT'&nbsp;=&nbsp;g_font.<br/>
</div>
<div class="code">
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_font&nbsp;'BOLD'&nbsp;=&nbsp;d&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"字体是否加粗<br/>
&nbsp;&nbsp;SET&nbsp;PROPERTY&nbsp;OF&nbsp;g_font&nbsp;'SIZE'&nbsp;=&nbsp;e&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"字体大小<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;g_cell&nbsp;.<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT&nbsp;g_font&nbsp;.<br/>
ENDFORM.                    " fill_cell<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CLIPBOARD_TO_EXCEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CLIPBOARD_TO_EXCEL<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_clipboard_to_excel TABLES p_excel_table USING s_row s_col e_row e_col .<br/>
<br/>
&nbsp;&nbsp;TYPES:&nbsp;BEGIN&nbsp;OF&nbsp;t_senderline,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;line(4096)&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;t_senderline&nbsp;.<br/>
&nbsp;&nbsp;TYPES:t_sender&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;t_senderline&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;it_excel_tab&nbsp;&nbsp;TYPE&nbsp;t_sender,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_excel_line&nbsp;TYPE&nbsp;t_senderline.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;l_range&nbsp;&nbsp;&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_cell_01&nbsp;TYPE&nbsp;ole2_object,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_cell_02&nbsp;TYPE&nbsp;ole2_object.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_rc&nbsp;TYPE&nbsp;i.<br/>
<br/>
&nbsp;&nbsp;CLEAR:it_excel_tab[]&nbsp;.<br/>
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;p_excel_table[]&nbsp;TO&nbsp;it_excel_tab[]&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;clipboard_export<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;it_excel_tab[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_rc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_sheets&nbsp;'Cells'&nbsp;=&nbsp;l_cell_01<br/>
&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;s_row&nbsp;#2&nbsp;=&nbsp;s_col.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_sheets&nbsp;'Cells'&nbsp;=&nbsp;l_cell_02<br/>
&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;e_row&nbsp;#2&nbsp;=&nbsp;e_col.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_sheets&nbsp;'Range'&nbsp;=&nbsp;l_range<br/>
&nbsp;&nbsp;EXPORTING&nbsp;#1&nbsp;=&nbsp;l_cell_01&nbsp;#2&nbsp;=&nbsp;l_cell_02.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;l_cell_01&nbsp;'SELECT'.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;l_range&nbsp;'SELECT'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;OF&nbsp;g_sheets&nbsp;'PASTE'.<br/>
<br/>
&nbsp;&nbsp;FREE&nbsp;OBJECT:&nbsp;l_range,&nbsp;l_cell_01,l_cell_02.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SUB_SHOW_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM sub_show_data .<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;sub_create_data&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_fieldcat&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_set_layout&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'ALV_PF_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'ALV_USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_top_of_page&nbsp;&nbsp;&nbsp;=&nbsp;'ALV_TOP_OF_PAGE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_sort_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_alv_sort[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcatalog[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_dmbtr1[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_SET_FIELDCAT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_fieldcat .<br/>
<br/>
&nbsp;&nbsp;DEFINE&nbsp;bulid_fieldlog.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;g_pos&nbsp;=&nbsp;g_pos&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-col_pos&nbsp;=&nbsp;g_pos.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-fieldname&nbsp;=&nbsp;&amp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-coltext&nbsp;=&nbsp;&amp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-outputlen&nbsp;=&nbsp;&amp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-ref_table&nbsp;&nbsp;=&nbsp;&amp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-ref_field&nbsp;=&nbsp;&amp;5.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-edit&nbsp;=&nbsp;&amp;6.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-key&nbsp;=&nbsp;&amp;7.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-checkbox&nbsp;=&nbsp;&amp;8&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-lzero&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-no_zero&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;g_pos&nbsp;=&nbsp;1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-just&nbsp;=&nbsp;'L'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcatalog-just&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_fieldcatalog&nbsp;TO&nbsp;gt_fieldcatalog.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_fieldcatalog.<br/>
&nbsp;&nbsp;END-OF-DEFINITION.<br/>
<br/>
&nbsp;&nbsp;FREE&nbsp;:&nbsp;gt_fieldcatalog.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;g_pos.<br/>
&nbsp;&nbsp;bulid_fieldlog:<br/>
&nbsp;&nbsp;'ZTEXT'&nbsp;&nbsp;&nbsp;&nbsp;'项目'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;,<br/>
&nbsp;&nbsp;'NMHSL'&nbsp;&nbsp;&nbsp;&nbsp;'本月金额'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;,<br/>
&nbsp;&nbsp;'NYHSL'&nbsp;&nbsp;&nbsp;&nbsp;'本年金额'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;,<br/>
&nbsp;&nbsp;'LYHSL'&nbsp;&nbsp;&nbsp;&nbsp;'上年金额'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;''&nbsp;.&nbsp;&nbsp;"modify&nbsp;by&nbsp;zhangcs&nbsp;20180709<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_SET_LAYOUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_set_layout .<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gs_layout.<br/>
&nbsp;&nbsp;gs_layout-box_fname&nbsp;=&nbsp;'SLBOX'.<br/>
&nbsp;&nbsp;gs_layout-zebra&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-sel_mode&nbsp;=&nbsp;'B'&nbsp;."'B'.<br/>
&nbsp;&nbsp;gs_layout-cwidth_opt&nbsp;=&nbsp;'X'.&nbsp;&nbsp;"优化列宽<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;gs_layout-KEYHOT&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;gs_layout-stylefname&nbsp;=&nbsp;'CELLTAB'.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
FORM alv_top_of_page.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_text&nbsp;TYPE&nbsp;string&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;g_butxt.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;butxt<br/>
&nbsp;&nbsp;INTO&nbsp;g_butxt<br/>
&nbsp;&nbsp;FROM&nbsp;t001<br/>
&nbsp;&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_bukrs&nbsp;.<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;g_butxt&nbsp;'--利润表'&nbsp;INTO&nbsp;l_text&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gs_header,gt_header[]&nbsp;.<br/>
&nbsp;&nbsp;gs_header-typ&nbsp;=&nbsp;'H'&nbsp;.<br/>
&nbsp;&nbsp;gs_header-info&nbsp;=&nbsp;l_text&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;gs_header&nbsp;TO&nbsp;gt_header&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CONCATENATE&nbsp;'所属期：'&nbsp;p_ryear&nbsp;'-'&nbsp;p_rpmax+1(2)&nbsp;INTO&nbsp;l_text&nbsp;.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gs_header&nbsp;.<br/>
&nbsp;&nbsp;gs_header-typ&nbsp;=&nbsp;'S'&nbsp;.<br/>
&nbsp;&nbsp;gs_header-info&nbsp;=&nbsp;l_text&nbsp;.<br/>
&nbsp;&nbsp;APPEND&nbsp;gs_header&nbsp;TO&nbsp;gt_header&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_COMMENTARY_WRITE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_list_commentary&nbsp;=&nbsp;gt_header[]<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_logo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'ENJOYSAP_LOGO'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_END_OF_LIST_GRID&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_ALV_FORM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
ENDFORM .<br/>
<br/>
FORM alv_user_command USING p_ucomm LIKE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rs_selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_name&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;lr_grid&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_GLOBALS_FROM_SLVC_FULLSCR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_grid&nbsp;=&nbsp;lr_grid.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lr_grid-&gt;check_changed_data.<br/>
<br/>
&nbsp;&nbsp;rs_selfield-refresh&nbsp;=&nbsp;'X'.&nbsp;&nbsp;"自动刷新<br/>
&nbsp;&nbsp;rs_selfield-col_stable&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;rs_selfield-row_stable&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;p_ucomm&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'D_LOAD'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;output_excel&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SEND'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_send_eas.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;IC1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"双击触发的事件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;sub_rstgr_data&nbsp;USING&nbsp;rs_selfield&nbsp;.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDFORM.                    "USER_COMMAND_ALV<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;set_pf_status<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AVL&nbsp;STATUS<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM alv_pf_status USING rt_extab TYPE slis_t_extab.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'GUI01'&nbsp;.<br/>
ENDFORM.                    "set_pf_status<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SUB_CREATE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM sub_create_data .<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gt_dmbtr1&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_dmbtr&nbsp;INTO&nbsp;gs_dmbtr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_dmbtr1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;gs_dmbtr&nbsp;TO&nbsp;gs_dmbtr1&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_fit001&nbsp;INTO&nbsp;gs_fit001&nbsp;WITH&nbsp;KEY&nbsp;zcolumn&nbsp;=&nbsp;gs_dmbtr-zcolumn&nbsp;zrow&nbsp;=&nbsp;gs_dmbtr-zrow&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_dmbtr1-ztext&nbsp;=&nbsp;gs_fit001-ztext&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_dmbtr1&nbsp;TO&nbsp;gt_dmbtr1&nbsp;.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
<br/>
<br/>
FORM sub_rstgr_data USING rs_selfield TYPE slis_selfield  .<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;gs_dmbtr1&nbsp;.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_dmbtr1&nbsp;INTO&nbsp;gs_dmbtr1&nbsp;INDEX&nbsp;rs_selfield-tabindex&nbsp;.<br/>
&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;"仅在双击本期金额的时候下钻<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;rs_selfield-fieldname&nbsp;=&nbsp;'NYHSL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;zi&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;zi&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;:&nbsp;ztextd&nbsp;TYPE&nbsp;&nbsp;zfit001-ztext.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ztextd&nbsp;=&nbsp;gs_dmbtr1-ztext.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ztextd&nbsp;&lt;&gt;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RANGES:&nbsp;s_racct&nbsp;FOR&nbsp;faglflext-racct.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:s_racct[]&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RANGES:&nbsp;s_rfarea&nbsp;FOR&nbsp;faglflext-rfarea.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;s_rfarea.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_fit001&nbsp;INTO&nbsp;&lt;gs_fit001&gt;&nbsp;WITH&nbsp;KEY&nbsp;ztext&nbsp;=&nbsp;ztextd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_fit001&nbsp;ASSIGNING&nbsp;&lt;gs_fit001&gt;&nbsp;WHERE&nbsp;&nbsp;ztext&nbsp;=&nbsp;ztextd&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;=&nbsp;''&nbsp;.&nbsp;&nbsp;"根据配置整合科目范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-sign&nbsp;=&nbsp;'I'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_racct&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;=&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-option&nbsp;=&nbsp;'EQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_racct&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-tsaknr&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-fsaknr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_racct-high&nbsp;=&nbsp;&lt;gs_fit001&gt;-tsaknr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_racct&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;=&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea(3)&nbsp;=&nbsp;'IEQ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;=&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea(3)&nbsp;=&nbsp;'IEQ'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;AND&nbsp;&lt;gs_fit001&gt;-trfarea&nbsp;&lt;&gt;&nbsp;''&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-sign&nbsp;=&nbsp;'I'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-option&nbsp;=&nbsp;'BT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-low&nbsp;=&nbsp;&lt;gs_fit001&gt;-frfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_rfarea-high&nbsp;=&nbsp;&lt;gs_fit001&gt;-trfarea.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;s_rfarea&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;(&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&gt;=&nbsp;'001'&nbsp;AND&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&lt;=&nbsp;'003'&nbsp;)&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&gt;=&nbsp;'015'&nbsp;AND&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&lt;=&nbsp;'021')&nbsp;OR&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;=&nbsp;'024'&nbsp;OR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&gt;=&nbsp;'026'&nbsp;AND&nbsp;&lt;gs_fit001&gt;-zrow&nbsp;&lt;=&nbsp;'035')&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zi&nbsp;=&nbsp;-1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;s_racct&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"将参数写入内存，并调用事务<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;p_bukrs&nbsp;=&nbsp;p_bukrs&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'BUKRS'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;s_racct&nbsp;=&nbsp;s_racct[]&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'RACCT'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;s_rfarea&nbsp;=&nbsp;s_rfarea[]&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'RFAREA'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;p_ryear&nbsp;=&nbsp;p_ryear&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'RYEAR'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;p_rpmax&nbsp;=&nbsp;p_rpmax&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'RPMAX'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT&nbsp;zi&nbsp;=&nbsp;zi&nbsp;TO&nbsp;MEMORY&nbsp;ID&nbsp;'ZI'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUBMIT&nbsp;zfir004_1&nbsp;AND&nbsp;RETURN.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'ZFIR004_1'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN&nbsp;.<br/>
<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_send_eas<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;发送EAS接口<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_send_eas .<br/>
&nbsp;&nbsp;DATA:ls_output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_fico065_income_statement_r,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header&nbsp;TYPE&nbsp;zdt_fico065_income_statement_1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_fico065_income_statement_r,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_header&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_fico065_income_statement_3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_data_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_fico065_income_stateme_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_fico065_income_statement_2.<br/>
&nbsp;&nbsp;DATA:fico065_income_stat&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zco_si_erp_fico065_income_stat.<br/>
&nbsp;&nbsp;DATA:lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype.<br/>
</div>
<div class="codeComment">
*&nbsp;初始化日志<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZCO_SI_ERP_FICO065_INCOME_STAT'&nbsp;).<br/>
</div>
<div class="codeComment">
*&nbsp;判断接口状态<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-receiver&nbsp;=&nbsp;'EAS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-bustyp&nbsp;=&nbsp;'FICO-65'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_message_header-send_user&nbsp;=&nbsp;sy-uname&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;抬头数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkbukrs&nbsp;INTO&nbsp;ls_data_header-bukrs&nbsp;FROM&nbsp;ztfico001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;p_bukrs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_data_header-gjahr&nbsp;=&nbsp;p_ryear.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_data_header-monat&nbsp;=&nbsp;p_rpmax.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_OUTPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;ls_data_header-monat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_data_header-monat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:lv_index&nbsp;TYPE&nbsp;i.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;行项目数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_dmbtr1&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;fs_alv&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_data_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_index&nbsp;=&nbsp;lv_index&nbsp;+&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-zindex&nbsp;=&nbsp;lv_index.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-ztext&nbsp;=&nbsp;&lt;fs_alv&gt;-ztext.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_data_item-ztext&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslby&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;&lt;fs_alv&gt;-nyhsl&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslbn&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;&lt;fs_alv&gt;-lmhsl&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslsn&nbsp;=&nbsp;zcl_assist01=&gt;digital_to_string(&nbsp;&lt;fs_alv&gt;-lyhsl&nbsp;).<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslby&nbsp;=&nbsp;&lt;fs_alv&gt;-nyhsl.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslbn&nbsp;=&nbsp;&lt;fs_alv&gt;-lmhsl.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_data_item-hslsn&nbsp;=&nbsp;&lt;fs_alv&gt;-lyhsl.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_data_item&nbsp;TO&nbsp;lt_data_item&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-header&nbsp;=&nbsp;ls_data_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-items&nbsp;=&nbsp;lt_data_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_fico065_income_statement_re-message_header&nbsp;=&nbsp;ls_message_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_output-mt_fico065_income_statement_re-zdata&nbsp;=&nbsp;ls_zdata.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;fico065_income_stat&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;fico065_income_stat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;fico065_income_stat-&gt;si_erp_fico065_income_statemen<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_output.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'发送金蝶成功！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;fico065_income_stat&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'发送金蝶成功!'&nbsp;TYPE&nbsp;'S'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'发送金蝶失败!'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;|{&nbsp;p_bukrs&nbsp;}|<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;|{&nbsp;p_ryear&nbsp;}|<br/>
&nbsp;&nbsp;key3&nbsp;=&nbsp;|{&nbsp;p_rpmax&nbsp;}|&nbsp;).<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUKRS&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;P_RPMAX&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
*&nbsp;P_RYEAR&nbsp;D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;没有利润表对应配置数据!!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>