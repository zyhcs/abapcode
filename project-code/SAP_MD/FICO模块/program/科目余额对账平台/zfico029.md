<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO029</h2>
<h3> Description: 科目余额对账平台</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO029<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
REPORT zfico029.<br/>
<br/>
<br/>
include <a href="zfico029_data.html">ZFICO029_data</a>.<br/>
<br/>
include <a href="zfico029_form.html">ZFICO029_form</a>.<br/>
<br/>
<br/>
INITIALIZATION.<br/>
<br/>
START-OF-SELECTION.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
<br/>
end-of-SELECTION.<br/>
&nbsp;&nbsp;IF&nbsp;p_r1&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_display.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_display_mx.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;P_GJAHR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年度<br/>
*&nbsp;P_R1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;汇总对账<br/>
*&nbsp;P_R2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;明细对账<br/>
*&nbsp;P_RPMAX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;期间<br/>
*&nbsp;S_HKONT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总账科目<br/>
*&nbsp;S_YJKM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一级科目<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>