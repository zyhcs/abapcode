<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO029_DATA</h2>
<h3> Description: Include ZFICO029_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO029_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES:ztfico027,zfit0002.<br/>
<br/>
DATA:BEGIN OF gs_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yjkm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-fmisn1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yjkmms&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-fmist1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;monat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-monat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt50&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;skat-txt50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-rtcur,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ryear&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-ryear,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqcye&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zSBQye&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zHBQye&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbqze&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbydmbtr&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbpdmbtr&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsdmbtr&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zhdmbtr&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zshjd&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqmjd&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;znccy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqccy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zscy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zhcy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zshcy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqmcy&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;gs_alv.<br/>
<br/>
DATA:gt_alv LIKE TABLE OF gs_alv.<br/>
<br/>
DATA:BEGIN OF ws_alv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ryear&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-ryear,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yjkm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-fmisn1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yjkmms&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-fmist1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hkont&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zfit0002-hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;monat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bkpf-monat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txt50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;skat-txt50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rtcur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-rtcur,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rcntr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-rcntr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ltext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;cskt-ltext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;aufk-ktext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-kunnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name_org1&nbsp;TYPE&nbsp;but000-name_org1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-aufnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hbkid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-hbkid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hktid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-hktid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rstgr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-rstgr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xref3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zacdoct-xref3,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hslvt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoct-hslvt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqcye&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zSBQye&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zHBQye&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbqze&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbydmbtr&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zbpdmbtr&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsdmbtr&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zhdmbtr&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zshjd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqmjd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;znccy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqccy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zscy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zhcy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zshcy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zqmcy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;hslxx12,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ws_alv.<br/>
<br/>
DATA:wt_alv LIKE TABLE OF ws_alv.<br/>
<br/>
<br/>
DATA: wt_fieldcat   TYPE lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
<br/>
DATA: gt_fieldcat   TYPE lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK blk1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:p_bukrs&nbsp;LIKE&nbsp;ztfico027-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_gjahr&nbsp;LIKE&nbsp;ztfico027-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_rpmax&nbsp;LIKE&nbsp;ztfico027-monat.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:s_hkont&nbsp;FOR&nbsp;ztfico027-hkont,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_yjkm&nbsp;FOR&nbsp;zfit0002-fmisn1.<br/>
&nbsp;&nbsp;PARAMETERS:p_r1&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_r2&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;grp1.<br/>
SELECTION-SCREEN END OF BLOCK blk1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>