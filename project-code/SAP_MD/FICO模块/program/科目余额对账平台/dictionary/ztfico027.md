<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO027</h2>
<h3>Description: EAS科目余额表存储表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>GJAHR</td>
<td>3</td>
<td>X</td>
<td>GJAHR</td>
<td>GJAHR</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>会计年度</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MONAT</td>
<td>4</td>
<td>X</td>
<td>MONAT</td>
<td>MONAT</td>
<td>NUMC</td>
<td>2</td>
<td>&nbsp;</td>
<td>会计期间</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BUZEI</td>
<td>5</td>
<td>X</td>
<td>ZBUZEI</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>行项目</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>WAERS</td>
<td>6</td>
<td>&nbsp;</td>
<td>WAERS</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>货币码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZSFQC</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZSFQC</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>是否期初</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>SHKZG</td>
<td>8</td>
<td>&nbsp;</td>
<td>SHKZG</td>
<td>SHKZG</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>借/贷标识</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZBYSHKZG</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZBYSHKZG</td>
<td>CHAR2</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>年初借贷标识</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZBYDMBTR</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZBYDMBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>年初本币金额</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZBYWRBTR</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZBYWRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>年初货币金额</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZBPSHKZG</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZBPSHKZG</td>
<td>CHAR2</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>期初借贷标识</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZBPDMBTR</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZBPDMBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>期初本币金额</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZBPWRBTR</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZBPWRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>期初货币金额</td>
</tr>
<tr class="cell">
<td>15</td>
<td>HKONT</td>
<td>15</td>
<td>&nbsp;</td>
<td>HKONT</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总账科目</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZYHZH</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZYHZH</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>银行账号</td>
</tr>
<tr class="cell">
<td>17</td>
<td>MWSKZ</td>
<td>17</td>
<td>&nbsp;</td>
<td>MWSKZ</td>
<td>MWSKZ</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>销售/购买税代码</td>
</tr>
<tr class="cell">
<td>18</td>
<td>KUNNR</td>
<td>18</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>19</td>
<td>LIFNR</td>
<td>19</td>
<td>&nbsp;</td>
<td>LIFNR</td>
<td>LIFNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>供应商或债权人的帐号</td>
</tr>
<tr class="cell">
<td>20</td>
<td>UMSKZ</td>
<td>20</td>
<td>&nbsp;</td>
<td>UMSKZ</td>
<td>UMSKZ</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>特殊总账标识</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZSDMBTR</td>
<td>21</td>
<td>&nbsp;</td>
<td>ZSDMBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>借方本币</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZSWRBTR</td>
<td>22</td>
<td>&nbsp;</td>
<td>ZSWRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>借方货币</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZHDMBTR</td>
<td>23</td>
<td>&nbsp;</td>
<td>ZHDMBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>贷方本币</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZHWRBTR</td>
<td>24</td>
<td>&nbsp;</td>
<td>ZHWRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>贷方货币</td>
</tr>
<tr class="cell">
<td>25</td>
<td>DMBTR</td>
<td>25</td>
<td>&nbsp;</td>
<td>DMBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>以本币计的金额</td>
</tr>
<tr class="cell">
<td>26</td>
<td>WRBTR</td>
<td>26</td>
<td>&nbsp;</td>
<td>WRBTR</td>
<td>AFLE13D2O16N_TO_23D2O30N</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>凭证货币金额</td>
</tr>
<tr class="cell">
<td>27</td>
<td>KOSTL</td>
<td>27</td>
<td>&nbsp;</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td>28</td>
<td>AUFNR</td>
<td>28</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td>29</td>
<td>MATERIAL_LONG</td>
<td>29</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>30</td>
<td>QUANTITY</td>
<td>30</td>
<td>&nbsp;</td>
<td>MENGE_D</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>31</td>
<td>BASE_UOM</td>
<td>31</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>32</td>
<td>ZFBDT</td>
<td>32</td>
<td>&nbsp;</td>
<td>ZFBDT</td>
<td>ZFBDT_DATS8</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基准日期</td>
</tr>
<tr class="cell">
<td>33</td>
<td>PERSON</td>
<td>33</td>
<td>&nbsp;</td>
<td>ZPERSON</td>
<td>NUMC8</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>人员</td>
</tr>
<tr class="cell">
<td>34</td>
<td>HBKID</td>
<td>34</td>
<td>&nbsp;</td>
<td>HBKID</td>
<td>HBKID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>开户行简短代码</td>
</tr>
<tr class="cell">
<td>35</td>
<td>HKTID</td>
<td>35</td>
<td>&nbsp;</td>
<td>HKTID</td>
<td>HKTID</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>帐户细目的代码</td>
</tr>
<tr class="cell">
<td>36</td>
<td>SGTXT</td>
<td>36</td>
<td>&nbsp;</td>
<td>SGTXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>项目文本</td>
</tr>
<tr class="cell">
<td>37</td>
<td>RSTGR</td>
<td>37</td>
<td>&nbsp;</td>
<td>RSTGR</td>
<td>RSTGR</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>付款原因代码</td>
</tr>
<tr class="cell">
<td>38</td>
<td>XNEGP</td>
<td>38</td>
<td>&nbsp;</td>
<td>XNEGP</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>标识：负过账</td>
</tr>
<tr class="cell">
<td>39</td>
<td>REF_KEY_2</td>
<td>39</td>
<td>&nbsp;</td>
<td>XREF2</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>业务伙伴参考码</td>
</tr>
<tr class="cell">
<td>40</td>
<td>REF_KEY_3</td>
<td>40</td>
<td>&nbsp;</td>
<td>XREF3</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>行项目的参考码</td>
</tr>
<tr class="cell">
<td>41</td>
<td>ZUONR</td>
<td>41</td>
<td>&nbsp;</td>
<td>ZUONR</td>
<td>CHAR18</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>分配</td>
</tr>
<tr class="cell">
<td>42</td>
<td>PRCTR</td>
<td>42</td>
<td>&nbsp;</td>
<td>PRCTR</td>
<td>PRCTR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>利润中心</td>
</tr>
<tr class="cell">
<td>43</td>
<td>VBUND</td>
<td>43</td>
<td>&nbsp;</td>
<td>VBUND</td>
<td>RCOMP</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>公司标识</td>
</tr>
<tr class="cell">
<td>44</td>
<td>ZJDKM</td>
<td>44</td>
<td>&nbsp;</td>
<td>ZJDKM</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>金蝶科目号</td>
</tr>
<tr class="cell">
<td>45</td>
<td>ZJDKH</td>
<td>45</td>
<td>&nbsp;</td>
<td>ZJDKH</td>
<td>CHAR50</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶客户</td>
</tr>
<tr class="cell">
<td>46</td>
<td>ZJDGYS</td>
<td>46</td>
<td>&nbsp;</td>
<td>ZJDGYS</td>
<td>CHAR50</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶供应商</td>
</tr>
<tr class="cell">
<td>47</td>
<td>ZKRSTGR</td>
<td>47</td>
<td>&nbsp;</td>
<td>ZEKRSTGR</td>
<td>ZDKRSTGR</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>金蝶现金流量码</td>
</tr>
<tr class="cell">
<td>48</td>
<td>ZJDCBZX</td>
<td>48</td>
<td>&nbsp;</td>
<td>ZJDCBZX</td>
<td>CHAR50</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶成本中心</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>H</td>
<td>&nbsp;</td>
<td>贷方</td>
</tr>
<tr class="cell">
<td>SHKZG</td>
<td>S</td>
<td>&nbsp;</td>
<td>借方</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>