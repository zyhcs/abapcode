<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO022_TOP</h2>
<h3> Description: Include ZFICO022_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO022_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
TABLES:ekko,ekpo,ekbe,qave,mara.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;采购订单信息<br/>
</div>
<div class="code">
TYPES:BEGIN OF ty_po,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-ebelp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ekorg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ekorg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ekgrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ekgrp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aedat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-aedat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bsart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bstyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bstyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pstyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-pstyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;batxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t161t-batxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ptext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t163y-ptext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char120,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txz01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-txz01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-groes,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eknam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t024-eknam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ernam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ernam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;purps&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-purps,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;purps_t&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umrez&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-umrez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umren&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-umren,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmein&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-lmein,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_p&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-menge,&nbsp;"采购订单数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-meins,&nbsp;"采购订单单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;textc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char80,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;banfn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-banfn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bnfpo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-bnfpo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;badat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-badat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-lfdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ameng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;knttp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-knttp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brtwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-brtwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bprme&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-bprme,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,&nbsp;"净值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,&nbsp;"净价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-mwskz,&nbsp;"税码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;peinh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-peinh,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-waers,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retpo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-retpo,&nbsp;&nbsp;&nbsp;"退货信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zisweb&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-zisweb,&nbsp;"是否网采<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_netpr&nbsp;&nbsp;TYPE&nbsp;ekpo-js_netpr,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_peinh&nbsp;&nbsp;TYPE&nbsp;ekpo-js_peinh,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_bprme&nbsp;&nbsp;TYPE&nbsp;ekpo-js_bprme,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl_txt&nbsp;TYPE&nbsp;t023t-wgbez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_po,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;采购到货日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_eket,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;TYPE&nbsp;ekko-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;TYPE&nbsp;ekpo-ebelp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eindt&nbsp;TYPE&nbsp;eket-eindt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_eket,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;采购相关的收货/发票<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_ekbe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-ebelp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zekkn&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-zekkn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buzei&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-buzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_io&nbsp;TYPE&nbsp;ekbe-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bldat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-bldat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-charg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mseg-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_p&nbsp;&nbsp;TYPE&nbsp;ekpo-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins2&nbsp;&nbsp;&nbsp;TYPE&nbsp;mseg-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-mwskz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfbnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-lfbnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfpos&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-lfpos,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shkzg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-shkzg,&nbsp;&nbsp;&nbsp;"zzl&nbsp;20190921&nbsp;add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwart&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-bwart,&nbsp;&nbsp;&nbsp;"zzl&nbsp;20191206&nbsp;add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;btext&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t156t-btext,&nbsp;&nbsp;&nbsp;"zzl&nbsp;20210106&nbsp;add<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_m&nbsp;&nbsp;TYPE&nbsp;matdoc-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-dmbtr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;erfmg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-erfmg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;erfme&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-erfme,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;licha&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-licha,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsdat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-hsdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ar_budat&nbsp;TYPE&nbsp;matdoc-budat,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sjahr&nbsp;&nbsp;TYPE&nbsp;ekbe-sjahr,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smbln&nbsp;&nbsp;TYPE&nbsp;ekbe-smbln,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smblp&nbsp;&nbsp;TYPE&nbsp;ekbe-smblp,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbstat&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-rbstat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xblnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-xblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vgart&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-vgart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stblg&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-stblg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stjah&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-stjah,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char120,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_ekbe,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;检验批信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_qals,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prueflos&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-prueflos,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stat35&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-stat35,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vdatum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vdatum,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vcode,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmenge01&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-lmenge01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mengeneinh&nbsp;TYPE&nbsp;qals-mengeneinh,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-mjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-mblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zeile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qals-zeile,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_qals,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料质检状态<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_qmat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;TYPE&nbsp;qmat-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;TYPE&nbsp;qmat-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;art&nbsp;&nbsp;&nbsp;TYPE&nbsp;qmat-art,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aktiv&nbsp;TYPE&nbsp;qmat-aktiv,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_qmat,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;输出清单<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BEGIN&nbsp;OF&nbsp;ty_list,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ebeln,&nbsp;"采购订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ebelp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-ebelp,&nbsp;"行项目<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bsart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bsart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;batxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t161t-batxt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pstyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-pstyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ptext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t163y-ptext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;belnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bwart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-bwart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;btext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t156t-btext,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buzei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-buzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-bukrs,&nbsp;"公司代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ekorg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ekorg,&nbsp;"采购组织<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ekgrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ekgrp,&nbsp;"采购组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aedat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-aedat,&nbsp;"采购凭证日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lifnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-lifnr,&nbsp;"供应商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char120,&nbsp;&nbsp;&nbsp;"供应商名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;purps&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-purps,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;purps_t&nbsp;&nbsp;&nbsp;TYPE&nbsp;lfa1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-matnr,&nbsp;"物料<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txz01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-txz01,&nbsp;"物料描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-matkl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matkl_txt&nbsp;TYPE&nbsp;t023t-wgbez,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mara-groes,&nbsp;"规格型号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-werks,&nbsp;"工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matqc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char10,&nbsp;"是否需要采购质检<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eknam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t024-eknam,&nbsp;"采购组名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ernam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-ernam,&nbsp;"创建者<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;acdoca-msl,&nbsp;"采购基本单位数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msehl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"基本计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_p&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-menge,&nbsp;"采购订单数量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-meins,&nbsp;"采购订单单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins3&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"采购订单单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-netwr,&nbsp;"净值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;p&nbsp;LENGTH&nbsp;16&nbsp;DECIMALS&nbsp;4,&nbsp;"净价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-mwskz,&nbsp;"税额<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eindt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eket-eindt,&nbsp;"预计到货时间<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;textc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char80,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"创建者名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;banfn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-banfn,&nbsp;"PR<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bnfpo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-bnfpo,&nbsp;"PR&nbsp;item<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;badat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-badat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-lfdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ameng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eban-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseh2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"PR基本计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brtwr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-brtwr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bprme&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-bprme,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseh5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"订单价格单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;peinh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-peinh,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;netpr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;p&nbsp;DECIMALS&nbsp;4,&nbsp;"&nbsp;ekpo-netpr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-waers,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;licha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-licha,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hsdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mch1-hsdat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-wrbtr,&nbsp;&nbsp;"发票金额（不含税）<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rate(8)&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"税率<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bldat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-bldat,&nbsp;&nbsp;"到货日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ar_budat&nbsp;&nbsp;TYPE&nbsp;matdoc-budat,&nbsp;&nbsp;"到货日期&nbsp;20210519到货日期从BLDAT改为BUDAT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;charg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-charg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmeng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;erfme&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-erfme,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseh6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"到货单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_m&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseh3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"到货基本计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dmbtr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-dmbtr,&nbsp;"到货金额（不含税）<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dhje_hs&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-dmbtr,&nbsp;"到货金额（含税）<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_netpr&nbsp;&nbsp;TYPE&nbsp;ekpo-js_netpr,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_peinh&nbsp;&nbsp;TYPE&nbsp;ekpo-js_peinh,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_bprme&nbsp;&nbsp;TYPE&nbsp;ekpo-js_bprme,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;js_price&nbsp;&nbsp;TYPE&nbsp;p&nbsp;LENGTH&nbsp;13&nbsp;DECIMALS&nbsp;4,&nbsp;"寄售含税单价<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prueflos&nbsp;&nbsp;TYPE&nbsp;qals-prueflos,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vdatum&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;qave-vdatum,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;valid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char30,&nbsp;"是否合格<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lmenge01&nbsp;&nbsp;TYPE&nbsp;qals-lmenge01,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mseh4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006a-mseh6,&nbsp;"合格计量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;budat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rjahr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-gjahr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zeile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-buzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_io&nbsp;&nbsp;TYPE&nbsp;ekbe-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rbstat&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-rbstat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;xblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-xblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lfbnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-lfbnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rkdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekbe-bldat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rlifn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;rbkp-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char120,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retpo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekpo-retpo,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"退货信息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zisweb&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ekko-zisweb,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"是否网采<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flag2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_list.<br/>
TYPES:BEGIN OF ty_t007v,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aland&nbsp;TYPE&nbsp;t007v-aland,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mwskz&nbsp;TYPE&nbsp;t007v-mwskz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kbetr&nbsp;TYPE&nbsp;t007v-kbetr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t007v.<br/>
<br/>
TYPES:BEGIN OF ty_cancel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mblnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-mblnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;urzei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-urzei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cancellation_type&nbsp;TYPE&nbsp;matdoc-cancellation_type,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cancelled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;matdoc-cancelled,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_cancel.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;DEFINE&nbsp;VARIABLES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA:gt_po     TYPE STANDARD TABLE OF ty_po,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_eket&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_eket,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_mdoc&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_ekbe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_mdoc2&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_ekbe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_voice&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_ekbe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_qals&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_qals,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_qmat&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_qmat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_cancel&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_cancel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_list&nbsp;&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_list.&nbsp;"输出内表<br/>
DATA:gr_ekorg TYPE RANGE OF ekorg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gr_ekgrp&nbsp;TYPE&nbsp;RANGE&nbsp;OF&nbsp;ekgrp.<br/>
DATA:gt_t007v TYPE TABLE OF ty_t007v.<br/>
</div>
<div class="codeComment">
*&nbsp;Define&nbsp;ALV&nbsp;attribute<br/>
</div>
<div class="code">
DATA:go_grid     TYPE REF TO cl_gui_alv_grid.<br/>
DATA:gs_layout   TYPE lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_fieldcat&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_variant&nbsp;&nbsp;TYPE&nbsp;disvariant,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_fieldcat&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;gt_fieldcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_exclude&nbsp;&nbsp;TYPE&nbsp;slis_t_extab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_repid&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;syrepid.<br/>
RANGES:r_belnr FOR matdoc-belnr.<br/>
RANGES:r_belnr2 FOR matdoc-belnr.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;SELECTION-SCREEN<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK blk_1 WITH FRAME TITLE TEXT-t01.<br/>
&nbsp;&nbsp;SELECT-OPTIONS:s_ekorg&nbsp;FOR&nbsp;ekko-ekorg&nbsp;OBLIGATORY,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ekgrp&nbsp;FOR&nbsp;ekko-ekgrp&nbsp;OBLIGATORY,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matnr&nbsp;FOR&nbsp;ekpo-matnr,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_matnr&nbsp;FOR&nbsp;mara-matnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_bsart&nbsp;FOR&nbsp;ekko-bsart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_werks&nbsp;FOR&nbsp;ekpo-werks,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_budat&nbsp;FOR&nbsp;ekbe-budat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ebeln&nbsp;FOR&nbsp;ekko-ebeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_lifnr&nbsp;FOR&nbsp;ekko-lifnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_aedat&nbsp;FOR&nbsp;ekko-aedat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_ernam&nbsp;FOR&nbsp;ekko-ernam,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_bldat&nbsp;FOR&nbsp;ekbe-bldat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_belnr&nbsp;for&nbsp;ekbe-belnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s_vatum&nbsp;FOR&nbsp;qave-vdatum.<br/>
SELECTION-SCREEN END OF BLOCK blk_1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>