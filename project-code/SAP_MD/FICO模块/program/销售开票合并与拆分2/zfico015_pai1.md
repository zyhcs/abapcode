<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015_PAI1</h2>
<h3> Description: Include ZFICO015_PAI</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO015_PAI<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;USER_COMMAND_0100&nbsp;&nbsp;INPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE user_command_0100 INPUT.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_stable&nbsp;TYPE&nbsp;lvc_s_stbl.<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_valid&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA&nbsp;l_index&nbsp;TYPE&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_celltab&nbsp;TYPE&nbsp;lvc_s_styl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_celltab&nbsp;TYPE&nbsp;lvc_t_styl.<br/>
&nbsp;&nbsp;ok_code&nbsp;=&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;save_ok&nbsp;=&nbsp;ok_code.<br/>
&nbsp;&nbsp;CLEAR&nbsp;ok_code.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gv_grid-&gt;check_changed_data.&nbsp;"获取alv变更数据<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gv_grid_matnr-&gt;check_changed_data.&nbsp;"获取alv变更数据<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;save_ok.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F03'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;TO&nbsp;SCREEN&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'&amp;F12'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;PROGRAM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SAL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;checkbox&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-checkbox&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_alv&nbsp;FROM&nbsp;gs_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alv&nbsp;INTO&nbsp;gs_alv&nbsp;WHERE&nbsp;checkbox&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gs_alv-checkbox&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;gt_alv&nbsp;FROM&nbsp;gs_alv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZXY'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_zxy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZCFTM'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_zcftm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZHBCF'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_zhbcf.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gv_grid-&gt;refresh_table_display(&nbsp;).<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gv_grid_matnr-&gt;refresh_table_display(&nbsp;).<br/>
<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>