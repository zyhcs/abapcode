<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015_PBO1</h2>
<h3> Description: Include ZFICO015_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZFICO015_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Module&nbsp;STATUS_0100&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE status_0100 OUTPUT.<br/>
&nbsp;&nbsp;DATA:&nbsp;BEGIN&nbsp;OF&nbsp;ex_tab&nbsp;OCCURS&nbsp;0,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fcode&nbsp;LIKE&nbsp;sy-ucomm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ex_tab.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;R_A&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ex_tab-fcode&nbsp;=&nbsp;'DELETE'.&nbsp;APPEND&nbsp;ex_tab.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ex_tab-fcode&nbsp;=&nbsp;'SALH'.&nbsp;APPEND&nbsp;ex_tab.<br/>
*&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ex_tab-fcode&nbsp;=&nbsp;'INSERT'.&nbsp;APPEND&nbsp;ex_tab.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ex_tab-fcode&nbsp;=&nbsp;'IMP'.&nbsp;APPEND&nbsp;ex_tab."20210603&nbsp;导入模板<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'ZFICO015_100'&nbsp;EXCLUDING&nbsp;ex_tab.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TITLE01'.<br/>
&nbsp;&nbsp;IF&nbsp;GV_CUSTOM_CONTAINER&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_CREATE_AND_INIT_ALV.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;GV_CUSTOM_CONTAINER_MATNR&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_SHOW_MATNR.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>