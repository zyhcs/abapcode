<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007C</h2>
<h3>Description: 开票不含税限额</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUTXT</td>
<td>3</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZMAXLIMIT</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZMAXLIMIT</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>专票发票限额（不含税）</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>PMAXLIMIT</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZPMAXLIMIT</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>普票（不含税）</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>