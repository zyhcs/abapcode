<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007M</h2>
<h3>Description: 特许客户开票差异表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZJSNO</td>
<td>2</td>
<td>X</td>
<td>ZJSNO</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>发票申请号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VBELN</td>
<td>3</td>
<td>X</td>
<td>VBELN_VF</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZHJBHSJE</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZHJBHSJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>开票金额</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZYSJIE</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZYSJIE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>原始金额</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZCYJE</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZCYJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>差异金额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>