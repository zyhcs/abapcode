<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007T</h2>
<h3>Description: 金税认证信息配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>CLIENT_ID</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZCKID</td>
<td>ZMD_ZCKID</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>客户ID</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>CLIENT_SECRET</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZCLST</td>
<td>ZMD_ZCLST</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>认证MD</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ENCRYPT_KEY</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZKEY</td>
<td>ZMD_ZKEY</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>进入key</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>