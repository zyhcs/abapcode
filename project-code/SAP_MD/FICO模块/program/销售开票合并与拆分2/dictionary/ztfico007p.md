<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007P</h2>
<h3>Description: 金票开具发送抬头表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZJSNO</td>
<td>2</td>
<td>X</td>
<td>ZJSNO</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>发票申请号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MABER</td>
<td>3</td>
<td>&nbsp;</td>
<td>MABER</td>
<td>MABER</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>催款范围</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZJSKPLX</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZJSKPLX</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>开票类型</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZJSQDBZ</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZJSQDBZ</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>清单标志</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZJSYFPDM</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZJSYFPDM</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>原发票代码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZJSYFPHM</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZJSYFPHM</td>
<td>CHAR8</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>原发票号码</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>BUKRS</td>
<td>8</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>BUTXT</td>
<td>9</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZXSFSBH</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZXSFSBH</td>
<td>CHAR18</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>销售方识别号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZXFDZ</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZXFDZ</td>
<td>ZMD_ZXFDZ</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销方地址电话</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZXFDH</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZXFDH</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销方电话</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZXFYH</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZXFYH</td>
<td>ZMD_ZXFYH</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>开户行及账号</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZSPF</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZSPF</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>收票方</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZSPFNAME</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZSPFNAME</td>
<td>ZMD_ZSPFNAME</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>收票方名称</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZGMFSBH</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZGMFSBH</td>
<td>ZMD_ZGMFSBH</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>购买方纳税人识别码</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZGMFDZDH</td>
<td>17</td>
<td>&nbsp;</td>
<td>ZGMFDZDH</td>
<td>ZMD_ZGMFDZDH</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>购买方地址电话</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZBANKN</td>
<td>18</td>
<td>&nbsp;</td>
<td>ZBANKN</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>纳税人开户行银行账号</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZMARK</td>
<td>19</td>
<td>&nbsp;</td>
<td>ZMARK</td>
<td>ZMD_ZMARK</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>发票备注</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZKPR</td>
<td>20</td>
<td>&nbsp;</td>
<td>ZKPR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>开票人</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZSKR</td>
<td>21</td>
<td>&nbsp;</td>
<td>ZSKR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>收款人</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZFHR</td>
<td>22</td>
<td>&nbsp;</td>
<td>ZFHR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>复核人</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZJSHJJE</td>
<td>23</td>
<td>&nbsp;</td>
<td>ZJSHJJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>价税合计</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZHJBHSJE</td>
<td>24</td>
<td>&nbsp;</td>
<td>ZHJBHSJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>开票金额</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZHJSE</td>
<td>25</td>
<td>&nbsp;</td>
<td>ZHJSE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>合计税额</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZJSHSBZ</td>
<td>26</td>
<td>&nbsp;</td>
<td>ZJSHSBZ</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>含税标识</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZDJBZ</td>
<td>27</td>
<td>&nbsp;</td>
<td>CHAR1</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>单字符标记</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZKPRQ</td>
<td>28</td>
<td>&nbsp;</td>
<td>ZKPRQ</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>开票日期</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZSQSM</td>
<td>29</td>
<td>&nbsp;</td>
<td>ZSQSM</td>
<td>ZMD_ZSQSM</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>申请说明</td>
</tr>
<tr class="cell">
<td>30</td>
<td>ZXXBLX</td>
<td>30</td>
<td>&nbsp;</td>
<td>ZXXBLX</td>
<td>ZMD_ZXXBLX</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>信息表类型</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>0</td>
<td>&nbsp;</td>
<td>购方发起已抵扣</td>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>1</td>
<td>&nbsp;</td>
<td>购方发起未抵扣</td>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>2</td>
<td>&nbsp;</td>
<td>销方</td>
</tr>
<tr class="cell">
<td>ZMD_ZXXBLX</td>
<td>0</td>
<td>&nbsp;</td>
<td>正常</td>
</tr>
<tr class="cell">
<td>ZMD_ZXXBLX</td>
<td>1</td>
<td>&nbsp;</td>
<td>逾期（仅销方开具）</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>