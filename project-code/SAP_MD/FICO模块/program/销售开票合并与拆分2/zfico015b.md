<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZFICO015B</h2>
<h3> Description: 销售开票合并与拆分(2位小数特殊客户）</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Report&nbsp;ZFICO015<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
report zfico015b.<br/>
<br/>
<br/>
include <a href="zfico015_data1.html">ZFICO015_DATA1</a>.<br/>
</div>
<div class="codeComment">
*include&nbsp;ZFICO015_data.<br/>
</div>
<div class="code">
include <a href="zfico015_from1.html">ZFICO015_FROM1</a>.<br/>
</div>
<div class="codeComment">
*include&nbsp;ZFICO015_from.<br/>
</div>
<div class="code">
include <a href="zfico015_pbo1.html">ZFICO015_PBO1</a>.<br/>
</div>
<div class="codeComment">
*include&nbsp;zfico015_pbo.<br/>
</div>
<div class="code">
include <a href="zfico015_pai1.html">ZFICO015_PAI1</a>.<br/>
</div>
<div class="codeComment">
*include&nbsp;zfico015_pai.<br/>
<br/>
<br/>
</div>
<div class="code">
initialization.<br/>
<br/>
at selection-screen.<br/>
&nbsp;&nbsp;perform&nbsp;frm_check_qx.<br/>
<br/>
start-of-selection.<br/>
&nbsp;&nbsp;if&nbsp;p_r1&nbsp;=&nbsp;'X'.&nbsp;&nbsp;"创建部分<br/>
&nbsp;&nbsp;&nbsp;&nbsp;perform&nbsp;frm_get_data.<br/>
&nbsp;&nbsp;elseif&nbsp;p_r2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;perform&nbsp;frm_modify_data.<br/>
&nbsp;&nbsp;else.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;perform&nbsp;frm_cx_data.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
END-OF-SELECTION.<br/>
&nbsp;&nbsp;if&nbsp;p_r1&nbsp;=&nbsp;'X'&nbsp;or&nbsp;p_r2&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;screen&nbsp;'100'.<br/>
&nbsp;&nbsp;elseif&nbsp;p_r3&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;perform&nbsp;frm_cx_display.<br/>
&nbsp;&nbsp;endif.<br/>
<br/>
</div>
<div class="codeComment">
*GUI&nbsp;Texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;TITILE01&nbsp;--&gt;&nbsp;金税发票生成<br/>
<br/>
<br/>
*Selection&nbsp;texts<br/>
*----------------------------------------------------------<br/>
*&nbsp;P_R1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;创建<br/>
*&nbsp;P_R2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改<br/>
*&nbsp;P_R3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;查询<br/>
*&nbsp;S_BUKRS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公司代码<br/>
*&nbsp;S_FKART&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开票类型<br/>
*&nbsp;S_FKDAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开票日期<br/>
*&nbsp;S_MABER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金税发票种类<br/>
*&nbsp;S_MATKL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;产品组<br/>
*&nbsp;S_MATNR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物料编码<br/>
*&nbsp;S_PARSD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;售达方<br/>
*&nbsp;S_PARSP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收票方<br/>
*&nbsp;S_PAYEE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开票人<br/>
*&nbsp;S_VBELN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;开票凭证<br/>
*&nbsp;S_VKORG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销售组织<br/>
*&nbsp;S_VTWEG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分销渠道<br/>
*&nbsp;S_ZFPHM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金税发票号<br/>
*&nbsp;S_ZHPXX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;红字发票申请号<br/>
*&nbsp;S_ZJSNO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金税发票申请号<br/>
*&nbsp;S_ZSTAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发票申请状态<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;Hard&nbsp;coded<br/>
*&nbsp;&nbsp;&nbsp;请单行下移<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>