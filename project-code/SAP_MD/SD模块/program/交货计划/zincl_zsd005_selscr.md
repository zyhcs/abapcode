<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZSD005_SELSCR</h2>
<h3> Description: Include ZINCL_ZSD005_SELSCR</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZSD005_SELSCR<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
"超期天数统计，crt_dat创建日期，被比较日期（一般是当天）<br/>
FORM frm_get_factorydats USING creat_day TYPE sy-datum crut_day TYPE sy-datum CHANGING ov_days type i.<br/>
&nbsp;&nbsp;DATA&nbsp;itab&nbsp;LIKE&nbsp;casdayattr&nbsp;OCCURS&nbsp;0&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;DATA&nbsp;l_line&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DAY_ATTRIBUTES_GET'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date_from&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;creat_day<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date_to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;crut_day<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;day_attributes&nbsp;=&nbsp;itab.<br/>
<br/>
&nbsp;&nbsp;DELETE&nbsp;itab&nbsp;WHERE&nbsp;weekday&nbsp;=&nbsp;6&nbsp;.<br/>
&nbsp;&nbsp;DELETE&nbsp;itab&nbsp;WHERE&nbsp;weekday&nbsp;=&nbsp;7&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DESCRIBE&nbsp;TABLE&nbsp;itab&nbsp;LINES&nbsp;l_line.<br/>
&nbsp;&nbsp;ov_days&nbsp;=&nbsp;l_line.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM frm_buidlmessage USING i_return TYPE bapiret2 CHANGING e_message TYPE any .<br/>
&nbsp;&nbsp;DATA&nbsp;ls_message&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MESSAGE_TEXT_BUILD'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-id<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-message_v1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-message_v2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-message_v3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_return-message_v4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message_text_output&nbsp;=&nbsp;ls_message.<br/>
&nbsp;&nbsp;IF&nbsp;e_message&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;ls_message&nbsp;TO&nbsp;e_message&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;e_message&nbsp;'；'&nbsp;ls_message&nbsp;INTO&nbsp;e_message&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>