<table class="outerTable">
<tr>
<td><h2>Table:ZPIDT_ERP_SD027_DELIVERYP_TAB1</h2>
<h3>Description:Proxy Table Type (generated)</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Name of table type</th>
<th>Name of row type for table types</th>
<th>Category of table type (range or general table type)</th>
<th>Elem. type of LOW and HIGH components of a Ranges type</th>
<th>Type of Object Referenced</th>
<th>Initial Line Number for Table Types</th>
<th>Description</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZPIDT_ERP_SD027_DELIVERYP_TAB1</td>
<td>ZPIDT_ERP_SD027_DELIVERYPLAN_5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>00000</td>
<td>Proxy Table Type (generated)</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>