<table class="outerTable">
<tr>
<td><h2>Table: ZTSD035L</h2>
<h3>Description: SAP发送开门生活交货单数据日志</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BSTKD</td>
<td>4</td>
<td>&nbsp;</td>
<td>BSTKD</td>
<td>BSTKD</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>客户参考</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VBELN</td>
<td>2</td>
<td>X</td>
<td>VBELN_VL</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>交货</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>WADAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>WADAK</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>计划货物移动日期</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZRQDT</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>发送开门数据日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZRQMG</td>
<td>8</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZRQNM</td>
<td>9</td>
<td>&nbsp;</td>
<td>SYST_UNAME</td>
<td>SYCHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>ABAP 系统字段：当前用户的名称</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZRQTM</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>发送开门数据时间</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZRQTP</td>
<td>7</td>
<td>&nbsp;</td>
<td>BAPI_MTYPE</td>
<td>SYCHAR01</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型: S 成功,E 错误,W 警告,I 信息,A 中断</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>