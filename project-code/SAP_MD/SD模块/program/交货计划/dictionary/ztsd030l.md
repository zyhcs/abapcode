<table class="outerTable">
<tr>
<td><h2>Table: ZTSD030L</h2>
<h3>Description: 开门生活发送SAP采购数据日志表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ARKTX</td>
<td>19</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>AUART</td>
<td>4</td>
<td>&nbsp;</td>
<td>AUART</td>
<td>AUART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售凭证类型</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AUGRU</td>
<td>12</td>
<td>&nbsp;</td>
<td>AUGRU</td>
<td>AUGRU</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>订单原因（业务交易原因）</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BSTKD</td>
<td>2</td>
<td>X</td>
<td>BSTKD</td>
<td>BSTKD</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>客户参考</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>DZLSCH</td>
<td>13</td>
<td>&nbsp;</td>
<td>DZLSCH</td>
<td>ZLSCH</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>付款方式</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>KALSM</td>
<td>15</td>
<td>&nbsp;</td>
<td>KALSMASD</td>
<td>KALSM</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>定价中的定价过程</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>KBETR</td>
<td>21</td>
<td>&nbsp;</td>
<td>KBETR</td>
<td>WERTV6</td>
<td>CURR</td>
<td>11</td>
<td>&nbsp;</td>
<td>条件金额或百分比</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>KUNAG</td>
<td>9</td>
<td>&nbsp;</td>
<td>KUNAG</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>售达方</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>KUNNR</td>
<td>10</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>10</td>
<td>KWMENG</td>
<td>22</td>
<td>&nbsp;</td>
<td>KWMENG</td>
<td>MENG15</td>
<td>QUAN</td>
<td>15</td>
<td>&nbsp;</td>
<td>以销售单位表示的累计订购数量</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MATNR</td>
<td>18</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>13</td>
<td>POSNR</td>
<td>3</td>
<td>X</td>
<td>EBELP</td>
<td>EBELP</td>
<td>NUMC</td>
<td>5</td>
<td>&nbsp;</td>
<td>采购凭证的项目编号</td>
</tr>
<tr class="cell">
<td>14</td>
<td>PSTYV</td>
<td>23</td>
<td>&nbsp;</td>
<td>PSTYV</td>
<td>PSTYV</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售凭证项目类别</td>
</tr>
<tr class="cell">
<td>15</td>
<td>SPART</td>
<td>7</td>
<td>&nbsp;</td>
<td>SPART</td>
<td>SPART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>产品组</td>
</tr>
<tr class="cell">
<td>16</td>
<td>VBELN</td>
<td>17</td>
<td>&nbsp;</td>
<td>VBELN_VA</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>销售凭证</td>
</tr>
<tr class="cell">
<td>17</td>
<td>VKBUR</td>
<td>8</td>
<td>&nbsp;</td>
<td>VKBUR</td>
<td>VKBUR</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售办事处</td>
</tr>
<tr class="cell">
<td>18</td>
<td>VKORG</td>
<td>5</td>
<td>&nbsp;</td>
<td>VKORG</td>
<td>VKORG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td>19</td>
<td>VRKME</td>
<td>20</td>
<td>&nbsp;</td>
<td>VRKME</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>销售单位</td>
</tr>
<tr class="cell">
<td>20</td>
<td>VSART</td>
<td>14</td>
<td>&nbsp;</td>
<td>VSARTTR</td>
<td>VERSART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>装运类型</td>
</tr>
<tr class="cell">
<td>21</td>
<td>VSBED</td>
<td>33</td>
<td>&nbsp;</td>
<td>VSBED</td>
<td>VSBED</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>装运条件</td>
</tr>
<tr class="cell">
<td>22</td>
<td>VTWEG</td>
<td>6</td>
<td>&nbsp;</td>
<td>VTWEG</td>
<td>VTWEG</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>分销渠道</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZQYGS</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>区域公司</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZRQDT</td>
<td>28</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>发送开门数据日期</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZRQMG</td>
<td>31</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZRQNM</td>
<td>32</td>
<td>&nbsp;</td>
<td>SYST_UNAME</td>
<td>SYCHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>ABAP 系统字段：当前用户的名称</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZRQTM</td>
<td>29</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>发送开门数据时间</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZRQTP</td>
<td>30</td>
<td>&nbsp;</td>
<td>BAPI_MTYPE</td>
<td>SYCHAR01</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型: S 成功,E 错误,W 警告,I 信息,A 中断</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZRSDT</td>
<td>24</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>接收开门数据日期</td>
</tr>
<tr class="cell">
<td>30</td>
<td>ZRSMG</td>
<td>27</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td>31</td>
<td>ZRSTM</td>
<td>25</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>接收开门数据时间</td>
</tr>
<tr class="cell">
<td>32</td>
<td>ZRSTP</td>
<td>26</td>
<td>&nbsp;</td>
<td>BAPI_MTYPE</td>
<td>SYCHAR01</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>消息类型: S 成功,E 错误,W 警告,I 信息,A 中断</td>
</tr>
<tr class="cell">
<td>33</td>
<td>ZSHDZ</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>