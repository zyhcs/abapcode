<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO007G</h2>
<h3>Description: 发票拆分合并_抬头</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUKRS</td>
<td>3</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUTXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>FKDAT</td>
<td>8</td>
<td>&nbsp;</td>
<td>FKDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>开票日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>KUNAG_ANA</td>
<td>13</td>
<td>&nbsp;</td>
<td>KUNAG</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>售达方</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>KUNAG_NAME</td>
<td>14</td>
<td>&nbsp;</td>
<td>KUNAG_NAME</td>
<td>CHAR35</td>
<td>CHAR</td>
<td>35</td>
<td>&nbsp;</td>
<td>售达方名称</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MABER</td>
<td>21</td>
<td>&nbsp;</td>
<td>MABER</td>
<td>MABER</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>催款范围</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>TEXT1</td>
<td>22</td>
<td>&nbsp;</td>
<td>TEXT1</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>文本</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>VKORG</td>
<td>5</td>
<td>&nbsp;</td>
<td>VKORG</td>
<td>VKORG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td>10</td>
<td>VTWEG</td>
<td>7</td>
<td>&nbsp;</td>
<td>VTWEG</td>
<td>VTWEG</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>分销渠道</td>
</tr>
<tr class="cell">
<td>11</td>
<td>VTXTK</td>
<td>6</td>
<td>&nbsp;</td>
<td>VTXTK</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>12</td>
<td>WAERK</td>
<td>27</td>
<td>&nbsp;</td>
<td>WAERK</td>
<td>WAERS</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>销售和分销凭证货币</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZFHR</td>
<td>29</td>
<td>&nbsp;</td>
<td>ZFHR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>复核人</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZFPDM</td>
<td>37</td>
<td>&nbsp;</td>
<td>ZFPDM</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>金税发票代码</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZFPHM</td>
<td>38</td>
<td>&nbsp;</td>
<td>ZFPHM</td>
<td>CHAR8</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>金税发票号码</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZFPQQRQ</td>
<td>20</td>
<td>&nbsp;</td>
<td>ZFPQQRQ</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>发票请求日期</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZGMFDZDH</td>
<td>19</td>
<td>&nbsp;</td>
<td>ZGMFDZDH</td>
<td>ZMD_ZGMFDZDH</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>购买方地址电话</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZGMFSBH</td>
<td>17</td>
<td>&nbsp;</td>
<td>ZGMFSBH</td>
<td>ZMD_ZGMFSBH</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>购买方纳税人识别码</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZGMFYHZH</td>
<td>18</td>
<td>&nbsp;</td>
<td>ZGMFYHZH</td>
<td>ZMD_ZGMFYHZH</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>购买方银行账号</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZHCFPDM</td>
<td>40</td>
<td>&nbsp;</td>
<td>ZHCFPDM</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>红冲发票代码</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZHCFPH</td>
<td>39</td>
<td>&nbsp;</td>
<td>ZHCFPH</td>
<td>CHAR8</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>红冲发票号码</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZHJBHSJE</td>
<td>25</td>
<td>&nbsp;</td>
<td>ZHJBHSJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>开票金额</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZHJSE</td>
<td>26</td>
<td>&nbsp;</td>
<td>ZHJSE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>合计税额</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZHPRQ</td>
<td>43</td>
<td>&nbsp;</td>
<td>ZHPRQ</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>红票日期</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZHPXX</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZHPXX</td>
<td>CHAR255</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>红字发票申请号</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZHZSQH</td>
<td>41</td>
<td>&nbsp;</td>
<td>ZHZSQH</td>
<td>CHAR40</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>红字申请号</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZJSHJJE</td>
<td>24</td>
<td>&nbsp;</td>
<td>ZJSHJJE</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>价税合计</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZJSHSBZ</td>
<td>23</td>
<td>&nbsp;</td>
<td>ZJSHSBZ</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>含税标识</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZJSKPLX</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZJSKPLX</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>开票类型</td>
</tr>
<tr class="cell">
<td>30</td>
<td>ZJSNO</td>
<td>2</td>
<td>X</td>
<td>ZJSNO</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>发票申请号</td>
</tr>
<tr class="cell">
<td>31</td>
<td>ZJSQDBZ</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZJSQDBZ</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>清单标志</td>
</tr>
<tr class="cell">
<td>32</td>
<td>ZKPR</td>
<td>30</td>
<td>&nbsp;</td>
<td>ZKPR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>开票人</td>
</tr>
<tr class="cell">
<td>33</td>
<td>ZKPRQ</td>
<td>42</td>
<td>&nbsp;</td>
<td>ZKPRQ</td>
<td>DATS</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>开票日期</td>
</tr>
<tr class="cell">
<td>34</td>
<td>ZMARK</td>
<td>35</td>
<td>&nbsp;</td>
<td>ZMARK</td>
<td>ZMD_ZMARK</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>发票备注</td>
</tr>
<tr class="cell">
<td>35</td>
<td>ZPPHZXX</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZPPHZXX</td>
<td>CHAR255</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>普票负数发票申请信息</td>
</tr>
<tr class="cell">
<td>36</td>
<td>ZSKR</td>
<td>28</td>
<td>&nbsp;</td>
<td>ZSKR</td>
<td>CHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>收款人</td>
</tr>
<tr class="cell">
<td>37</td>
<td>ZSPF</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZSPF</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>收票方</td>
</tr>
<tr class="cell">
<td>38</td>
<td>ZSPFNAME</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZSPFNAME</td>
<td>ZMD_ZSPFNAME</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>收票方名称</td>
</tr>
<tr class="cell">
<td>39</td>
<td>ZSQSM</td>
<td>44</td>
<td>&nbsp;</td>
<td>ZSQSM</td>
<td>ZMD_ZSQSM</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>申请说明</td>
</tr>
<tr class="cell">
<td>40</td>
<td>ZSTATUS</td>
<td>36</td>
<td>&nbsp;</td>
<td>ZSTATUS</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>发票状态</td>
</tr>
<tr class="cell">
<td>41</td>
<td>ZXFDH</td>
<td>34</td>
<td>&nbsp;</td>
<td>ZXFDH</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销方电话</td>
</tr>
<tr class="cell">
<td>42</td>
<td>ZXFDZ</td>
<td>33</td>
<td>&nbsp;</td>
<td>ZXFDZ</td>
<td>ZMD_ZXFDZ</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销方地址电话</td>
</tr>
<tr class="cell">
<td>43</td>
<td>ZXFYH</td>
<td>32</td>
<td>&nbsp;</td>
<td>ZXFYH</td>
<td>ZMD_ZXFYH</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>开户行及账号</td>
</tr>
<tr class="cell">
<td>44</td>
<td>ZXSFSBH</td>
<td>31</td>
<td>&nbsp;</td>
<td>ZXSFSBH</td>
<td>CHAR18</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>销售方识别号</td>
</tr>
<tr class="cell">
<td>45</td>
<td>ZXXBLX</td>
<td>45</td>
<td>&nbsp;</td>
<td>ZXXBLX</td>
<td>ZMD_ZXXBLX</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>信息表类型</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>0</td>
<td>&nbsp;</td>
<td>购方发起已抵扣</td>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>1</td>
<td>&nbsp;</td>
<td>购方发起未抵扣</td>
</tr>
<tr class="cell">
<td>ZMD_ZSQSM</td>
<td>2</td>
<td>&nbsp;</td>
<td>销方</td>
</tr>
<tr class="cell">
<td>ZMD_ZXXBLX</td>
<td>0</td>
<td>&nbsp;</td>
<td>正常</td>
</tr>
<tr class="cell">
<td>ZMD_ZXXBLX</td>
<td>1</td>
<td>&nbsp;</td>
<td>逾期（仅销方开具）</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>