<table class="outerTable">
<tr>
<td><h2>Table: ZTSD004</h2>
<h3>Description: 物流APP回传的运费信息</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>DEDUCTIONS</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>扣款额</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>FCONVERSIONFACTOR</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DEC</td>
<td>6</td>
<td>&nbsp;</td>
<td>折算系数</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>FREIGHT</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>汽车运费</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>FSTATEMENTDATE</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>APP对账单日期</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>GUID</td>
<td>2</td>
<td>X</td>
<td>SYSUUID_C32</td>
<td>SYSUUID_C32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 32 Characters (Hexadecimal Encoded)</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MEINS</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>UNIT</td>
<td>3</td>
<td>&nbsp;</td>
<td>单位</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>POSNR</td>
<td>4</td>
<td>&nbsp;</td>
<td>POSNR_VL</td>
<td>POSNR</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>交货项目</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>REASON</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>备注</td>
</tr>
<tr class="cell">
<td>10</td>
<td>UNIT_PRICE</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际运费单价</td>
</tr>
<tr class="cell">
<td>11</td>
<td>VBELN</td>
<td>3</td>
<td>&nbsp;</td>
<td>VBELN_VL</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>交货</td>
</tr>
<tr class="cell">
<td>12</td>
<td>WAERS</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CUKY</td>
<td>5</td>
<td>&nbsp;</td>
<td>货币</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZCSRQ</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>传输日期</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZDWSL</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>吨数</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZKKJE</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>折算后金额(含税）</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZSDJ</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>折算后单价(含税)</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>