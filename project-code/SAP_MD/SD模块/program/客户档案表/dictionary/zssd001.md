<table class="outerTable">
<tr>
<td><h2>Table: ZSSD001</h2>
<h3>Description: 客户档案表结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ACCNAME</td>
<td>44</td>
<td>&nbsp;</td>
<td>BU_BANKACCNAME</td>
<td>BU_BKCCNAME</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>银行帐户的名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>AKONT</td>
<td>24</td>
<td>&nbsp;</td>
<td>AKONT</td>
<td>SAKNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>总帐中的统驭科目</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AKONT_TXT</td>
<td>25</td>
<td>&nbsp;</td>
<td>TXT20_SKAT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>总帐科目名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BANKL</td>
<td>56</td>
<td>&nbsp;</td>
<td>BU_BANKK</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>银行代码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BANKN</td>
<td>46</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>24</td>
<td>&nbsp;</td>
<td>银行账号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BZIRK</td>
<td>19</td>
<td>&nbsp;</td>
<td>BZIRK</td>
<td>BZIRK</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>销售地区</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>BZIRK_TXT</td>
<td>20</td>
<td>&nbsp;</td>
<td>BZTXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>区名</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>CAP_INCR_A</td>
<td>37</td>
<td>&nbsp;</td>
<td>BP_CA_IN_A</td>
<td>WERTV8</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>上次资本增长量</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>CHDAT</td>
<td>51</td>
<td>&nbsp;</td>
<td>CRDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>10</td>
<td>CORP_NAME</td>
<td>41</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>法人代表姓名</td>
</tr>
<tr class="cell">
<td>11</td>
<td>CRDAT</td>
<td>50</td>
<td>&nbsp;</td>
<td>CRDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>12</td>
<td>CREDIT_LIMIT</td>
<td>34</td>
<td>&nbsp;</td>
<td>UKM_CREDIT_LIMIT</td>
<td>UKM_CREDIT_LIMIT</td>
<td>CURR</td>
<td>15</td>
<td>&nbsp;</td>
<td>信用额度</td>
</tr>
<tr class="cell">
<td>13</td>
<td>CREDIT_SGMNT</td>
<td>33</td>
<td>&nbsp;</td>
<td>UKM_CREDIT_SGMNT</td>
<td>UKM_CREDIT_SGMNT</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>信用段</td>
</tr>
<tr class="cell">
<td>14</td>
<td>FOUND_DAT</td>
<td>36</td>
<td>&nbsp;</td>
<td>BU_FOUND_DAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>组织成立日期</td>
</tr>
<tr class="cell">
<td>15</td>
<td>KATR1</td>
<td>27</td>
<td>&nbsp;</td>
<td>KATR1</td>
<td>ATTR1</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>属性1</td>
</tr>
<tr class="cell">
<td>16</td>
<td>KATR1_TXT</td>
<td>28</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>17</td>
<td>KATR2</td>
<td>29</td>
<td>&nbsp;</td>
<td>KATR2</td>
<td>ATTR2</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>属性2</td>
</tr>
<tr class="cell">
<td>18</td>
<td>KATR2_TXT</td>
<td>30</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>19</td>
<td>KATR3</td>
<td>26</td>
<td>&nbsp;</td>
<td>KATR3</td>
<td>ATTR3</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>属性3</td>
</tr>
<tr class="cell">
<td>20</td>
<td>KOINH</td>
<td>45</td>
<td>&nbsp;</td>
<td>BU_KOINH</td>
<td>BU_KOINH</td>
<td>CHAR</td>
<td>60</td>
<td>X</td>
<td>账户持有人姓名</td>
</tr>
<tr class="cell">
<td>21</td>
<td>KTGRD</td>
<td>22</td>
<td>&nbsp;</td>
<td>KTGRD</td>
<td>KTGRD</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>此客户的账户分配组</td>
</tr>
<tr class="cell">
<td>22</td>
<td>KTGRD_TXT</td>
<td>23</td>
<td>&nbsp;</td>
<td>BEZEI20</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>23</td>
<td>KTOKD</td>
<td>3</td>
<td>&nbsp;</td>
<td>KTOKD</td>
<td>KTOKD</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>客户帐户组</td>
</tr>
<tr class="cell">
<td>24</td>
<td>KTOKD_TXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>TXT30_077T</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>科目组名称</td>
</tr>
<tr class="cell">
<td>25</td>
<td>KUNNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td>26</td>
<td>KVGR1</td>
<td>5</td>
<td>&nbsp;</td>
<td>KVGR1</td>
<td>KVGR1</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户组 1</td>
</tr>
<tr class="cell">
<td>27</td>
<td>KVGR1_TXT</td>
<td>6</td>
<td>&nbsp;</td>
<td>BEZEI20</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>28</td>
<td>KVGR2</td>
<td>7</td>
<td>&nbsp;</td>
<td>KVGR2</td>
<td>KVGR2</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户组 2</td>
</tr>
<tr class="cell">
<td>29</td>
<td>KVGR2_TXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>BEZEI20</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>30</td>
<td>KVGR3</td>
<td>9</td>
<td>&nbsp;</td>
<td>KVGR3</td>
<td>KVGR3</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户组 3</td>
</tr>
<tr class="cell">
<td>31</td>
<td>KVGR3_TXT</td>
<td>10</td>
<td>&nbsp;</td>
<td>BEZEI20</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>32</td>
<td>ORT01</td>
<td>32</td>
<td>&nbsp;</td>
<td>ORT01</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>城市</td>
</tr>
<tr class="cell">
<td>33</td>
<td>PAR_NAME</td>
<td>42</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>经办人姓名</td>
</tr>
<tr class="cell">
<td>34</td>
<td>PAR_TEL</td>
<td>43</td>
<td>&nbsp;</td>
<td>AD_TLNMBR</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>电话号码: 拨区号 + 号码</td>
</tr>
<tr class="cell">
<td>35</td>
<td>PODKZ</td>
<td>21</td>
<td>&nbsp;</td>
<td>PODKZ</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>与 POD 处理相关</td>
</tr>
<tr class="cell">
<td>36</td>
<td>REGION</td>
<td>31</td>
<td>&nbsp;</td>
<td>REGIO</td>
<td>REGIO</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>地区（省/自治区/直辖市、市、县）</td>
</tr>
<tr class="cell">
<td>37</td>
<td>RES_ADDR</td>
<td>35</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>注册地址</td>
</tr>
<tr class="cell">
<td>38</td>
<td>SMTP_ADDR</td>
<td>40</td>
<td>&nbsp;</td>
<td>AD_SMTPADR</td>
<td>AD_SMTPADR</td>
<td>CHAR</td>
<td>241</td>
<td>X</td>
<td>电子邮件地址</td>
</tr>
<tr class="cell">
<td>39</td>
<td>SPART</td>
<td>15</td>
<td>&nbsp;</td>
<td>SPART</td>
<td>SPART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>产品组</td>
</tr>
<tr class="cell">
<td>40</td>
<td>SPART_TXT</td>
<td>16</td>
<td>&nbsp;</td>
<td>VTXTK</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>41</td>
<td>TAXNUMXL</td>
<td>47</td>
<td>&nbsp;</td>
<td>BPTAXNUMXL</td>
<td>BPTAXNUMXL</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>业务伙伴税号</td>
</tr>
<tr class="cell">
<td>42</td>
<td>TEL_NUMBER</td>
<td>39</td>
<td>&nbsp;</td>
<td>TELF1</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>第一个电话号</td>
</tr>
<tr class="cell">
<td>43</td>
<td>UNW_REMARK</td>
<td>38</td>
<td>&nbsp;</td>
<td>BP_UNW_REMARK</td>
<td>TEXT35</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>不受欢迎的注释</td>
</tr>
<tr class="cell">
<td>44</td>
<td>VKBUR</td>
<td>17</td>
<td>&nbsp;</td>
<td>VKBUR</td>
<td>VKBUR</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售办事处</td>
</tr>
<tr class="cell">
<td>45</td>
<td>VKBUR_TXT</td>
<td>18</td>
<td>&nbsp;</td>
<td>BEZEI20</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td>46</td>
<td>VKORG</td>
<td>11</td>
<td>&nbsp;</td>
<td>VKORG</td>
<td>VKORG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td>47</td>
<td>VKORG_TXT</td>
<td>12</td>
<td>&nbsp;</td>
<td>VTXTK</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>48</td>
<td>VTWEG</td>
<td>13</td>
<td>&nbsp;</td>
<td>VTWEG</td>
<td>VTWEG</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>分销渠道</td>
</tr>
<tr class="cell">
<td>49</td>
<td>VTWEG_TXT</td>
<td>14</td>
<td>&nbsp;</td>
<td>VTXTK</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>50</td>
<td>ZKUNNR</td>
<td>48</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>销售业务员</td>
</tr>
<tr class="cell">
<td>51</td>
<td>ZKUNNR_TEL</td>
<td>49</td>
<td>&nbsp;</td>
<td>AD_TLNMBR</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>电话号码: 拨区号 + 号码</td>
</tr>
<tr class="cell">
<td>52</td>
<td>ZNAME</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>客户名称</td>
</tr>
<tr class="cell">
<td>53</td>
<td>ZSDF</td>
<td>52</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>送达方编码</td>
</tr>
<tr class="cell">
<td>54</td>
<td>ZSDF_TXT</td>
<td>53</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>送达方描述</td>
</tr>
<tr class="cell">
<td>55</td>
<td>ZSPF</td>
<td>54</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>收票方编码</td>
</tr>
<tr class="cell">
<td>56</td>
<td>ZSPF_TXT</td>
<td>55</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>收票方描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>