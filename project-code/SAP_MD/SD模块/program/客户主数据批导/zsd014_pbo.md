<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD014_PBO</h2>
<h3> Description: Include ZSD014_PBO</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD014_PBO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;STATUS_9000&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE STATUS_9000 OUTPUT.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STATUS_9001'.<br/>
&nbsp;&nbsp;SET&nbsp;TITLEBAR&nbsp;'TITLE_9000'.<br/>
ENDMODULE.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Module&nbsp;&nbsp;ALV_LIST&nbsp;&nbsp;OUTPUT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
MODULE ALV_LIST OUTPUT.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FRM_CREATE_ALV.<br/>
ENDMODULE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>