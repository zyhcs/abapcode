<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD014_SCREEN</h2>
<h3> Description: Include ZSD014_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD014_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_file&nbsp;&nbsp;&nbsp;LIKE&nbsp;rlgrap-filename.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK b1.<br/>
<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE TEXT-002.<br/>
<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_rb1&nbsp;TYPE&nbsp;c&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;sg1&nbsp;USER-COMMAND&nbsp;CCC&nbsp;DEFAULT&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_rb2&nbsp;TYPE&nbsp;c&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;sg1&nbsp;.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_rb3&nbsp;TYPE&nbsp;c&nbsp;&nbsp;RADIOBUTTON&nbsp;GROUP&nbsp;sg1&nbsp;.<br/>
<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;LINE.<br/>
*SELECTION-SCREEN&nbsp;POSITION&nbsp;1.<br/>
*PARAMETERS:&nbsp;P_A1&nbsp;AS&nbsp;CHECKBOX&nbsp;DEFAULT&nbsp;'X'.<br/>
*SELECTION-SCREEN&nbsp;COMMENT&nbsp;3(15)&nbsp;TEXT-003."基本视图<br/>
*<br/>
*SELECTION-SCREEN&nbsp;POSITION&nbsp;24.<br/>
*PARAMETERS:&nbsp;P_A2&nbsp;AS&nbsp;CHECKBOX.<br/>
*SELECTION-SCREEN&nbsp;COMMENT&nbsp;26(15)&nbsp;TEXT-004."销售视图<br/>
*<br/>
*SELECTION-SCREEN&nbsp;POSITION&nbsp;46.<br/>
*PARAMETERS:&nbsp;P_A3&nbsp;AS&nbsp;CHECKBOX.<br/>
*SELECTION-SCREEN&nbsp;COMMENT&nbsp;48(15)&nbsp;TEXT-005."公司视图<br/>
*<br/>
*SELECTION-SCREEN&nbsp;POSITION&nbsp;68.<br/>
*PARAMETERS:&nbsp;P_A4&nbsp;AS&nbsp;CHECKBOX.<br/>
*SELECTION-SCREEN&nbsp;COMMENT&nbsp;70(15)&nbsp;TEXT-006."信贷视图<br/>
<br/>
*SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;LINE.<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN END OF BLOCK b2.<br/>
<br/>
</div>
<div class="codeComment">
*SELECTION-SCREEN&nbsp;BEGIN&nbsp;OF&nbsp;BLOCK&nbsp;b3&nbsp;WITH&nbsp;FRAME&nbsp;TITLE&nbsp;TEXT-003.<br/>
*&nbsp;&nbsp;PARAMETERS:&nbsp;p_c1&nbsp;AS&nbsp;CHECKBOX&nbsp;&nbsp;DEFAULT&nbsp;'X'&nbsp;MODIF&nbsp;ID&nbsp;MD1,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_c2&nbsp;AS&nbsp;CHECKBOX&nbsp;MODIF&nbsp;ID&nbsp;MD1.<br/>
*SELECTION-SCREEN&nbsp;END&nbsp;OF&nbsp;BLOCK&nbsp;b3.<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN FUNCTION KEY 1.<br/>
SELECTION-SCREEN FUNCTION KEY 2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>