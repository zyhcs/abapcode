<table class="outerTable">
<tr>
<td><h2>Table: ZTSD009A</h2>
<h3>Description: 到站港数据</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>KUNNR</td>
<td>3</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VKORG</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZCJR</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>创建人</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZCJSJ</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>创建时间</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZDZG</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>到站港编码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZDZGD</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>到站港描述</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZENDC</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>最后修改时间</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZHCZYX</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>火车专用线</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZLXR</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>联系人</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZLXRT</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>联系人电话</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZSHR</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>收货人名称/公司</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZXGR</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>修改人</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZZJHM</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>证件号</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>