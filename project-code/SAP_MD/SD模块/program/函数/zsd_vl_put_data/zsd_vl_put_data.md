<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZSD_VL_PUT_DATA</h2>
<h3> Description: 将交货单数据存入函数组</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zsd_vl_put_data.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IS_LIKP) TYPE  LIKP OPTIONAL<br/>
*"     REFERENCE(IS_LIKPD) TYPE  LIKPD OPTIONAL<br/>
*"     REFERENCE(IT_YLIKP) TYPE  SHP_YLIKP_T OPTIONAL<br/>
*"     REFERENCE(IT_XVBUK) TYPE  SHP_VL10_VBUK_T OPTIONAL<br/>
*"     REFERENCE(IT_YVBUK) TYPE  SHP_VL10_VBUK_T OPTIONAL<br/>
*"     REFERENCE(IT_XVBUP) TYPE  SHP_VL10_VBUP_T OPTIONAL<br/>
*"     REFERENCE(IT_YVBUP) TYPE  SHP_VL10_VBUP_T OPTIONAL<br/>
*"     REFERENCE(IT_XLIPS) TYPE  SHP_LIPS_T OPTIONAL<br/>
*"     REFERENCE(IT_YLIPS) TYPE  SHP_LIPS_T OPTIONAL<br/>
*"     REFERENCE(IT_XVBPA) TYPE  SHP_VBPAVB_T OPTIONAL<br/>
*"     REFERENCE(IT_YVBPA) TYPE  SHP_VBPAVB_T OPTIONAL<br/>
*"     REFERENCE(IT_XVBFA) TYPE  SHP_VL10_VBFA_T OPTIONAL<br/>
*"     REFERENCE(IT_YVBFA) TYPE  SHP_VL10_VBFA_T OPTIONAL<br/>
*"     REFERENCE(IT_XVBADR) TYPE  SHP_SADRVB_T OPTIONAL<br/>
*"     REFERENCE(IS_V50AGL) TYPE  V50AGL OPTIONAL<br/>
*"     REFERENCE(IF_TRTYP) TYPE  TRTYP OPTIONAL<br/>
*"     REFERENCE(IF_TCODE) TYPE  LE_SHP_TCODE OPTIONAL<br/>
*"  CHANGING<br/>
*"     REFERENCE(CS_V50AGL_CUST) TYPE  V50AGL_CUST<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zsd_vl_put_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;trtyp&nbsp;=&nbsp;if_trtyp.<br/>
&nbsp;&nbsp;tcode&nbsp;=&nbsp;if_tcode.<br/>
&nbsp;&nbsp;gs_likp&nbsp;=&nbsp;is_likp.<br/>
&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;is_likp&nbsp;TO&nbsp;zsds001.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>