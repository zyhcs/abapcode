<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_INTERFACE_00001050</h2>
<h3> Description: Schnittstellenbeschreibung zum Event 00001050</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZFM_INTERFACE_00001050.<br/>
</div>
<div class="codeComment">
*"--------------------------------------------------------------------<br/>
*"*"局部接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_XVBUP) LIKE  OFIWA-XVBUP DEFAULT 'X'<br/>
*"  TABLES<br/>
*"      T_BKP1 STRUCTURE  BKP1<br/>
*"      T_BKPF STRUCTURE  BKPF<br/>
*"      T_BSEC STRUCTURE  BSEC<br/>
*"      T_BSED STRUCTURE  BSED<br/>
*"      T_BSEG STRUCTURE  BSEG<br/>
*"      T_BSET STRUCTURE  BSET<br/>
*"      T_BSEU STRUCTURE  BSEU<br/>
*"--------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zfm_interface_00001050.html">Global data declarations</a></div><br/>
</div>
<div class="code">
BREAK ITL_WEIXP.<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>