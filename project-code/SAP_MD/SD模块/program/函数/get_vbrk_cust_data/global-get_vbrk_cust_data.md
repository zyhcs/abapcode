<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG012TOP</h2>
<h3> Description: 获取发票自定义字段</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zsdg012                      MESSAGE-ID zsd001.<br/>
<br/>
DATA gs_cvbrk TYPE zsvbrk.<br/>
DATA gv_active TYPE flag.<br/>
DATA gs_vbrk TYPE vbrk.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZSDG012D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>