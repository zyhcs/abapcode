<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD033_SALESPRICEAPPROVAL</h2>
<h3> Description: 销售价格创建</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd033_salespriceapproval .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(INPUT) TYPE  ZSDS002<br/>
*"  EXPORTING<br/>
*"     REFERENCE(MSGTYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(MESSAGE) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_sd033_salespriceapproval.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bapicondct&nbsp;&nbsp;TYPE&nbsp;bapicondct,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bapicondhd&nbsp;&nbsp;TYPE&nbsp;bapicondhd,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bapicondit&nbsp;&nbsp;TYPE&nbsp;bapicondit,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_bapiret2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiret2,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapicondqs&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicondqs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapicondvs&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicondvs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapiknumhs&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiknumhs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_mem_initial&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;cnd_mem_initial,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_bapiret2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiret2.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*初始化<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondct.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondhd.<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondit.<br/>
&nbsp;&nbsp;IF&nbsp;input-cond_type&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;input-cond_type&nbsp;=&nbsp;'ZR01'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;input-cond_usage&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;input-cond_usage&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;input-applicatio&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;input-applicatio&nbsp;=&nbsp;'V'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;input-kpein&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;input-kpein&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_kumza&nbsp;TYPE&nbsp;umrez.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_kumne&nbsp;TYPE&nbsp;umren.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_main_meins&nbsp;TYPE&nbsp;meins.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;zcl_assist01=&gt;exchang_main_meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;input-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins_in&nbsp;=&nbsp;input-meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;menge_in&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;meins&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_main_meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umrez&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_kumza<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;umren&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_kumne<br/>
</div>
<div class="codeComment">
*    RECEIVING<br/>
*     menge_ex =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*补零<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;input-kunnr&nbsp;=&nbsp;|{&nbsp;input-kunnr&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;input-matnr&nbsp;=&nbsp;|{&nbsp;input-matnr&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*检查数据<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;meins&nbsp;FROM&nbsp;mara&nbsp;INTO&nbsp;input-meins<br/>
&nbsp;&nbsp;&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;input-matnr.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e001(zsd001)&nbsp;WITH&nbsp;input-matnr&nbsp;INTO&nbsp;&nbsp;message&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;tvko<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vkorg&nbsp;=&nbsp;input-vkorg.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|销售组织{&nbsp;input-vkorg&nbsp;}不存在!|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;input-vtweg&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;tvtw<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vtweg&nbsp;=&nbsp;input-vtweg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|分销渠道{&nbsp;input-vtweg&nbsp;}不存在!|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;input-kunnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;kna1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;input-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|客户{&nbsp;input-kunnr&nbsp;}不存在!|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;input-vsart&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UP&nbsp;TO&nbsp;1&nbsp;ROWS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;t173<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vsart&nbsp;=&nbsp;input-vsart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;|装运类型{&nbsp;input-vsart&nbsp;}不存在!|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DATE_CHECK_PLAUSIBILITY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;input-datab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plausibility_check_failed&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'有效起始日期错误,格式YYYYMMDD.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DATE_CHECK_PLAUSIBILITY'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;input-datbi<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;plausibility_check_failed&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'有效结束日期错误,格式YYYYMMDD.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;IF&nbsp;input-datab&nbsp;&gt;&nbsp;input-datbi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'有效起始日期不能大于有效结束日期.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*获取条件表主键<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lt_fieldname&nbsp;TYPE&nbsp;tt_fieldname.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_tabname&nbsp;TYPE&nbsp;tabname.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_varkey&nbsp;TYPE&nbsp;char100.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_varkey_string&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;lv_tabname&nbsp;=&nbsp;|{&nbsp;input-cond_usage&nbsp;}{&nbsp;input-table_no&nbsp;}|.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_table_key&nbsp;TABLES&nbsp;lt_fieldname&nbsp;USING&nbsp;lv_tabname&nbsp;.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_fieldname&nbsp;INTO&nbsp;DATA(lv_fieldname).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;lv_fieldname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'MANDT'&nbsp;OR&nbsp;'KAPPL'&nbsp;OR&nbsp;'KSCHL'&nbsp;OR&nbsp;'KFRST'&nbsp;OR&nbsp;'DATBI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;lv_fieldname&nbsp;OF&nbsp;STRUCTURE&nbsp;input&nbsp;TO&nbsp;FIELD-SYMBOL(&lt;lv_field&gt;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e003(zsd001)&nbsp;WITH&nbsp;lv_fieldname&nbsp;INTO&nbsp;message&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;lv_field&gt;&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e004(zsd001)&nbsp;WITH&nbsp;lv_fieldname&nbsp;INTO&nbsp;message..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;lv_varkey_string&nbsp;&lt;lv_field&gt;&nbsp;INTO&nbsp;lv_varkey_string&nbsp;RESPECTING&nbsp;BLANKS.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;lv_varkey&nbsp;=&nbsp;lv_varkey_string.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*拼接sql<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lv_sqlcond&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_build_sqlcond&nbsp;TABLES&nbsp;lt_fieldname&nbsp;CHANGING&nbsp;lv_sqlcond.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*获取相关价格条件<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lv_source)&nbsp;=&nbsp;|{&nbsp;lv_tabname&nbsp;}&nbsp;AS&nbsp;A&nbsp;INNER&nbsp;JOIN&nbsp;KONH&nbsp;AS&nbsp;B&nbsp;ON&nbsp;A~KNUMH&nbsp;=&nbsp;B~KNUMH&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;KONP&nbsp;AS&nbsp;C&nbsp;ON&nbsp;B~KNUMH&nbsp;=&nbsp;C~KNUMH&nbsp;|.<br/>
</div>
<div class="codeComment">
*   lv_source = |{ lv_source } INNER JOIN KONP AS C ON B~KNUMH = C~KNUMH |.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lt_konp&nbsp;TYPE&nbsp;tt_konp.<br/>
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b~kvewe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b~kotabnr,<br/>
</div>
<div class="codeComment">
*     b~kappl,<br/>
*     b~kschl,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b~datab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b~datbi,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c~*<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;(lv_source)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;(lv_sqlcond)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;@lt_konp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e009(zsd001)&nbsp;WITH&nbsp;lv_fieldname&nbsp;INTO&nbsp;message&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_operation&nbsp;TYPE&nbsp;msgfn.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_konp2&nbsp;TYPE&nbsp;ts_konp.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_konp&nbsp;INTO&nbsp;DATA(ls_konp).<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*删除原来的条件价格<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;lv_operation&nbsp;=&nbsp;'003'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_bapidata&nbsp;&nbsp;USING&nbsp;&nbsp;lv_varkey&nbsp;ls_konp&nbsp;lv_operation<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;msgtype&nbsp;message.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*调整相关的历史条件价格有效起止日期<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_konp-knumh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_operation&nbsp;=&nbsp;'009'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_konp-datab&nbsp;&lt;&nbsp;&nbsp;input-datab&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_konp2&nbsp;=&nbsp;ls_konp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_new_date&nbsp;USING&nbsp;input-datab&nbsp;'-1'&nbsp;CHANGING&nbsp;ls_konp2-datbi&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_bapidata&nbsp;&nbsp;USING&nbsp;&nbsp;lv_varkey&nbsp;ls_konp2&nbsp;lv_operation<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;msgtype&nbsp;message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;msgtype&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_konp-datbi&nbsp;&gt;&nbsp;input-datbi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_konp2&nbsp;=&nbsp;ls_konp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_new_date&nbsp;USING&nbsp;&nbsp;input-datbi&nbsp;'+1'&nbsp;CHANGING&nbsp;ls_konp2-datab&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_fill_bapidata&nbsp;&nbsp;USING&nbsp;&nbsp;lv_varkey&nbsp;ls_konp2&nbsp;lv_operation<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;msgtype&nbsp;message.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;msgtype&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*新增条件价格赋值<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;ls_konp.<br/>
&nbsp;&nbsp;ls_konp-kappl&nbsp;=&nbsp;input-applicatio..<br/>
&nbsp;&nbsp;ls_konp-kvewe&nbsp;=&nbsp;input-cond_usage..<br/>
&nbsp;&nbsp;ls_konp-kotabnr&nbsp;=&nbsp;input-table_no&nbsp;.<br/>
&nbsp;&nbsp;ls_konp-kschl&nbsp;=&nbsp;input-cond_type.<br/>
&nbsp;&nbsp;ls_konp-datab&nbsp;=&nbsp;input-datab.<br/>
&nbsp;&nbsp;ls_konp-datbi&nbsp;=&nbsp;input-datbi.<br/>
&nbsp;&nbsp;ls_konp-kopos&nbsp;=&nbsp;'01'.<br/>
&nbsp;&nbsp;ls_konp-stfkz&nbsp;=&nbsp;'A'.<br/>
&nbsp;&nbsp;ls_konp-krech&nbsp;=&nbsp;'C'.<br/>
&nbsp;&nbsp;ls_konp-kumza&nbsp;=&nbsp;lv_kumza.<br/>
&nbsp;&nbsp;ls_konp-kumne&nbsp;=&nbsp;lv_kumne.<br/>
&nbsp;&nbsp;ls_konp-kwaeh&nbsp;=&nbsp;input-konwa..<br/>
&nbsp;&nbsp;ls_konp-zaehk_ind&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;ls_konp-kbetr&nbsp;=&nbsp;input-kbetr.<br/>
&nbsp;&nbsp;ls_konp-konwa&nbsp;=&nbsp;input-konwa.<br/>
<br/>
&nbsp;&nbsp;ls_konp-kpein&nbsp;=&nbsp;input-kpein.<br/>
&nbsp;&nbsp;ls_konp-kmein&nbsp;=&nbsp;input-meins.<br/>
&nbsp;&nbsp;ls_konp-meins&nbsp;=&nbsp;lv_main_meins.<br/>
&nbsp;&nbsp;lv_operation&nbsp;=&nbsp;'009'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_fill_bapidata&nbsp;&nbsp;USING&nbsp;&nbsp;lv_varkey&nbsp;ls_konp&nbsp;lv_operation<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;msgtype&nbsp;message.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*call bapi<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_PRICES_CONDITIONS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pi_initialmode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pi_physical_deletion&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ti_bapicondct&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_bapicondct[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ti_bapicondhd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_bapicondhd[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ti_bapicondit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_bapicondit[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ti_bapicondqs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapicondqs[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ti_bapicondvs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapicondvs[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to_bapiret2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapiret2[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to_bapiknumhs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_bapiknumhs[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to_mem_initial&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_mem_initial[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_msg&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_bapiret2&nbsp;INTO&nbsp;DATA(ls_return)&nbsp;WHERE&nbsp;type&nbsp;CO&nbsp;'AEX'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MESSAGE_TEXT_BUILD'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-id<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-message_v1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-message_v2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-message_v3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_return-message_v4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message_text_output&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;message&nbsp;lv_msg&nbsp;INTO&nbsp;message&nbsp;SEPARATED&nbsp;BY&nbsp;';'.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
</div>
<div class="codeComment">
*         EXPORTING<br/>
*           WAIT          =<br/>
*         IMPORTING<br/>
*           RETURN        =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;message&nbsp;=&nbsp;'创建成功!'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'<br/>
</div>
<div class="codeComment">
*         IMPORTING<br/>
*           RETURN        =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 获取条件编号失败!<br/>
* 002 缺少条件价格字段:<br/>
* 003 字段不能为空!<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZSD001<br/>
*001   物料号&amp;1不存在!<br/>
*002   获取条件编号失败!<br/>
*003   缺少条件价格字段&amp;1!<br/>
*004   字段&amp;1不能为空!<br/>
*009   获取历史条件价格时出现异常!<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>