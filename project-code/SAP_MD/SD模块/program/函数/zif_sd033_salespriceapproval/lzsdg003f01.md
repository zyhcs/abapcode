<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG003F01</h2>
<h3> Description: Include LZSDG003F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
***INCLUDE&nbsp;LZSDG003F01.<br/>
*----------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_GET_TABLE_KEY<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LT_FIELDNAME<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_table_key  TABLES   ct_fieldname TYPE STANDARD TABLE  USING uv_tabname.<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;TABLE&nbsp;ct_fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;dd03l<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;tabname&nbsp;=&nbsp;uv_tabname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;as4local&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;keyflag&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ORDER&nbsp;BY&nbsp;position.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_BUILD_SQLCOND<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LT_FIELDNAME<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;cv_sqlcond<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_build_sqlcond  TABLES   it_fieldname TYPE tt_fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;cv_sqlcond.<br/>
<br/>
&nbsp;&nbsp;cv_sqlcond&nbsp;=&nbsp;|A~KAPPL&nbsp;=&nbsp;@INPUT-APPLICATIO&nbsp;AND&nbsp;A~KSCHL&nbsp;=&nbsp;@INPUT-COND_TYPE&nbsp;|.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_fieldname&nbsp;INTO&nbsp;DATA(lv_fieldname).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;lv_fieldname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'MANDT'&nbsp;OR&nbsp;'KAPPL'&nbsp;OR&nbsp;'KSCHL'&nbsp;OR&nbsp;'KFRST'&nbsp;OR&nbsp;'DATBI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;cv_sqlcond&nbsp;=&nbsp;|{&nbsp;cv_sqlcond&nbsp;}&nbsp;AND&nbsp;A~{&nbsp;lv_fieldname&nbsp;}&nbsp;=&nbsp;@INPUT-{&nbsp;lv_fieldname&nbsp;}&nbsp;|.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;cv_sqlcond&nbsp;=&nbsp;|{&nbsp;cv_sqlcond&nbsp;}&nbsp;AND&nbsp;A~DATBI&nbsp;&gt;=&nbsp;@INPUT-DATAB&nbsp;AND&nbsp;A~DATAB&nbsp;&lt;=&nbsp;@INPUT-DATBI&nbsp;|.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;cv_sqlcond&nbsp;=&nbsp;|{&nbsp;cv_sqlcond&nbsp;}&nbsp;AND&nbsp;C~LOEVM_KO&nbsp;&lt;&gt;&nbsp;'X'&nbsp;|.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_process_old_data<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;gt_bapicondct<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LT__BAPICONDHD<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LT__BAPICONDIT<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;INPUT<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LV_VARKEY<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LS_KONP<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_fill_bapidata   USING     uv_varkey<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE(us_konp)&nbsp;TYPE&nbsp;ts_konp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uv_operation&nbsp;TYPE&nbsp;msgfn<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;cv_code<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cv_msg.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*获取条件号<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;us_konp-knumh&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_con_no&nbsp;CHANGING&nbsp;us_konp-knumh.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;us_konp-knumh&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cv_code&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e002(zsd001)&nbsp;INTO&nbsp;cv_msg&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondct.<br/>
&nbsp;&nbsp;gt_bapicondct-operation&nbsp;&nbsp;=&nbsp;uv_operation.<br/>
&nbsp;&nbsp;gt_bapicondct-cond_no&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-knumh.<br/>
&nbsp;&nbsp;gt_bapicondct-varkey&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;uv_varkey.<br/>
&nbsp;&nbsp;gt_bapicondct-applicatio&nbsp;=&nbsp;us_konp-kappl.<br/>
&nbsp;&nbsp;gt_bapicondct-cond_usage&nbsp;=&nbsp;us_konp-kvewe.<br/>
&nbsp;&nbsp;gt_bapicondct-table_no&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-kotabnr&nbsp;.<br/>
&nbsp;&nbsp;gt_bapicondct-cond_type&nbsp;&nbsp;=&nbsp;us_konp-kschl.<br/>
&nbsp;&nbsp;gt_bapicondct-valid_from&nbsp;=&nbsp;us_konp-datab.<br/>
&nbsp;&nbsp;gt_bapicondct-valid_to&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-datbi.<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;gt_bapicondct&nbsp;.<br/>
<br/>
&nbsp;&nbsp;"&nbsp;KONH<br/>
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondhd.<br/>
&nbsp;&nbsp;gt_bapicondhd-operation&nbsp;&nbsp;=&nbsp;uv_operation.<br/>
&nbsp;&nbsp;gt_bapicondhd-cond_no&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-knumh.<br/>
&nbsp;&nbsp;gt_bapicondhd-varkey&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;uv_varkey.<br/>
&nbsp;&nbsp;gt_bapicondhd-applicatio&nbsp;=&nbsp;us_konp-kappl.<br/>
&nbsp;&nbsp;gt_bapicondhd-cond_usage&nbsp;=&nbsp;us_konp-kvewe.<br/>
&nbsp;&nbsp;gt_bapicondhd-table_no&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-kotabnr&nbsp;.<br/>
&nbsp;&nbsp;gt_bapicondhd-cond_type&nbsp;&nbsp;=&nbsp;us_konp-kschl.<br/>
&nbsp;&nbsp;gt_bapicondhd-valid_from&nbsp;=&nbsp;us_konp-datab.<br/>
&nbsp;&nbsp;gt_bapicondhd-valid_to&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-datbi.<br/>
&nbsp;&nbsp;gt_bapicondhd-creat_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;gt_bapicondhd-created_by&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;APPEND&nbsp;gt_bapicondhd&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;KONP&nbsp;tabel<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;gt_bapicondit.<br/>
&nbsp;&nbsp;gt_bapicondit-operation&nbsp;&nbsp;=&nbsp;uv_operation.<br/>
&nbsp;&nbsp;gt_bapicondit-cond_no&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-knumh..<br/>
&nbsp;&nbsp;gt_bapicondit-cond_count&nbsp;=&nbsp;us_konp-kopos.<br/>
&nbsp;&nbsp;gt_bapicondit-applicatio&nbsp;=&nbsp;us_konp-kappl.<br/>
&nbsp;&nbsp;gt_bapicondit-cond_type&nbsp;&nbsp;=&nbsp;us_konp-kschl.<br/>
&nbsp;&nbsp;gt_bapicondit-scaletype&nbsp;&nbsp;=&nbsp;us_konp-stfkz.<br/>
&nbsp;&nbsp;gt_bapicondit-calctypcon&nbsp;&nbsp;=&nbsp;us_konp-krech.<br/>
&nbsp;&nbsp;gt_bapicondit-numconvert&nbsp;&nbsp;=&nbsp;us_konp-kumza.<br/>
&nbsp;&nbsp;gt_bapicondit-denominato&nbsp;&nbsp;=&nbsp;us_konp-kumne.<br/>
&nbsp;&nbsp;gt_bapicondit-condcurren&nbsp;=&nbsp;us_konp-kwaeh.<br/>
&nbsp;&nbsp;gt_bapicondit-conditidx&nbsp;&nbsp;=&nbsp;us_konp-zaehk_ind.<br/>
&nbsp;&nbsp;gt_bapicondit-cond_value&nbsp;=&nbsp;us_konp-kbetr.<br/>
&nbsp;&nbsp;gt_bapicondit-condcurr&nbsp;&nbsp;&nbsp;=&nbsp;us_konp-konwa.<br/>
&nbsp;&nbsp;gt_bapicondit-cond_p_unt&nbsp;=&nbsp;us_konp-kpein.<br/>
&nbsp;&nbsp;gt_bapicondit-cond_unit&nbsp;&nbsp;=&nbsp;us_konp-kmein.<br/>
&nbsp;&nbsp;gt_bapicondit-base_uom&nbsp;&nbsp;=&nbsp;us_konp-meins.<br/>
&nbsp;&nbsp;APPEND&nbsp;gt_bapicondit&nbsp;.<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_get_new_date<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;US_INPUT_DATAB<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;P_<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;US_KONP_DATBI<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_new_date  USING   uv_date TYPE d<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VALUE(uv_i)&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;cv_date&nbsp;TYPE&nbsp;d.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'FIMA_DATE_CREATE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_date&nbsp;=&nbsp;uv_date<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_FLG_END_OF_MONTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_YEARS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_MONTHS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_days&nbsp;=&nbsp;uv_i<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_CALENDAR_DAYS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SET_LAST_DAY_OF_MONTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_date&nbsp;=&nbsp;cv_date<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_FLG_END_OF_MONTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_DAYS_OF_I_DATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;FRM_GET_CON_NO<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;INPUT_KNUMH<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_con_no  CHANGING    cv_knumh TYPE konh-knumh.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'NUMBER_GET_NEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nr_range_nr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'01'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'KONH'<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'1'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SUBOBJECT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOYEAR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'0000'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IGNORE_BUFFER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;cv_knumh<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;QUANTITY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURNCODE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interval_not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;number_range_not_intern&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object_not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;quantity_is_0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;quantity_is_not_1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interval_overflow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buffer_overflow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>