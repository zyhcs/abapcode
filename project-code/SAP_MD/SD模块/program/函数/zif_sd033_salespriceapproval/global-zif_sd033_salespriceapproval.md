<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG003TOP</h2>
<h3> Description: 销售价格创建</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zsdg003.                      "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZSDG003D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
<div class="code">
TYPES tt_fieldname TYPE TABLE OF fieldname.<br/>
TYPES: BEGIN OF ts_konp ,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kvewe&nbsp;&nbsp;&nbsp;TYPE&nbsp;konh-kvewe,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kotabnr&nbsp;TYPE&nbsp;konh-kotabnr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;datab&nbsp;&nbsp;&nbsp;TYPE&nbsp;konh-datab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;datbi&nbsp;&nbsp;&nbsp;TYPE&nbsp;konh-datbi.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INCLUDE&nbsp;STRUCTURE&nbsp;konp.<br/>
TYPES END OF ts_konp.<br/>
TYPES tt_konp TYPE TABLE OF ts_konp.<br/>
DATA:gt_bapicondct TYPE TABLE OF bapicondct WITH HEADER LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_bapicondhd&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicondhd&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_bapicondit&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicondit&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>