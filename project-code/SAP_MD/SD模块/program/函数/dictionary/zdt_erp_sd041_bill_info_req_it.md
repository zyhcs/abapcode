<table class="outerTable">
<tr>
<td><h2>Table: ZDT_ERP_SD041_BILL_INFO_REQ_IT</h2>
<h3>Description: </h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>CONTROLLER</td>
<td>1</td>
<td>&nbsp;</td>
<td>PRXCTRLTAB</td>
<td>&nbsp;</td>
<td>TTYP</td>
<td>0</td>
<td>&nbsp;</td>
<td>Control Flags for Fields of a Structure</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>FKIMG</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>G_LFIMG</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>KBETR</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>KWERT</td>
<td>26</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MAKTX</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>MATNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>POSNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>VRKME</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>10</td>
<td>VSART</td>
<td>19</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>11</td>
<td>WADAT_IST</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZBSTKD</td>
<td>18</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZCJDJ</td>
<td>22</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZCJJE</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZCJSL</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZCJYY</td>
<td>24</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZCPHM</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZDZG</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZFPBZ</td>
<td>29</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZFYDH</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZGDJECJ</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZJHDD</td>
<td>21</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZJZXH1</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZJZXH2</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZKPDW</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZKPSL</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZXSSL</td>
<td>27</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZYZFY</td>
<td>28</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZZKHZ</td>
<td>25</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>STRG</td>
<td>0</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>