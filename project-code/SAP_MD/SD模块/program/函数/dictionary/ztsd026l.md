<table class="outerTable">
<tr>
<td><h2>Table: ZTSD026L</h2>
<h3>Description: 销售订单超信调接口日志</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>RQDAT</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>SAP发送到OA日期</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>RQNAM</td>
<td>5</td>
<td>&nbsp;</td>
<td>SYST_UNAME</td>
<td>SYCHAR12</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>ABAP 系统字段：当前用户的名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>RQTIM</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>SAP发送到OA时间</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>RSDAT</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>审批日期</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>RSMSG</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>审批消息</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>RSRLT</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>审批结果</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>RSSPR</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>审批人</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>RSTIM</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>审批时间</td>
</tr>
<tr class="cell">
<td>10</td>
<td>VBELN</td>
<td>2</td>
<td>X</td>
<td>VBELN_VA</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>销售凭证</td>
</tr>
<tr class="cell">
<td>11</td>
<td>WORKFLOWID</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>10</td>
<td>&nbsp;</td>
<td>流程号</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>