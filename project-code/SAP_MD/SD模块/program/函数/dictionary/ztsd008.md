<table class="outerTable">
<tr>
<td><h2>Table: ZTSD008</h2>
<h3>Description: SAP与OA产品组编码映射</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>SPART</td>
<td>2</td>
<td>X</td>
<td>SPART</td>
<td>SPART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>产品组</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VTEXT</td>
<td>3</td>
<td>&nbsp;</td>
<td>VTXTK</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZOACPZ</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZOACPZ</td>
<td>ZD_ZOACPZ</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>OA产品组编码</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZOACPZMS</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZE_ZOACPZMS</td>
<td>ZD_ZOACPZMS</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>OA产品组描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>