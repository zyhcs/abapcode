<table class="outerTable">
<tr>
<td><h2>Table: ZTSD006</h2>
<h3>Description: EAS回传应收单审核状态</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>STATUS</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZE_STATUS</td>
<td>ZDM_STATUS</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>状态标识</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>VBELN</td>
<td>2</td>
<td>X</td>
<td>VBELN_VF</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>ZDM_STATUS</td>
<td>D</td>
<td>&nbsp;</td>
<td>可以冲销</td>
</tr>
<tr class="cell">
<td>ZDM_STATUS</td>
<td>B</td>
<td>&nbsp;</td>
<td>不可冲销</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>