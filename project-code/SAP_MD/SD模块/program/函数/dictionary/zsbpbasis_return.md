<table class="outerTable">
<tr>
<td><h2>Table: ZSBPBASIS_RETURN</h2>
<h3>Description: 客户主数据基本视图接口返回结果结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>.INCLUDE</td>
<td>1</td>
<td>&nbsp;</td>
<td>ZSBPBASIS</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>0</td>
<td>&nbsp;</td>
<td>客户主数据基本视图接口结构</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ANRED</td>
<td>15</td>
<td>&nbsp;</td>
<td>AD_TITLE</td>
<td>AD_TITLE</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>地址关键字的表格</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BU_GROUP</td>
<td>6</td>
<td>&nbsp;</td>
<td>BU_GROUP</td>
<td>BU_GROUP</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>业务伙伴分组</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BU_TYPE</td>
<td>8</td>
<td>&nbsp;</td>
<td>BU_TYPE</td>
<td>BU_TYPE</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>业务伙伴类别</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>COUNTRY</td>
<td>13</td>
<td>&nbsp;</td>
<td>LAND1</td>
<td>LAND1</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>国家/地区代码</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>FAX_NUMBER</td>
<td>20</td>
<td>&nbsp;</td>
<td>AD_FXNMBR1</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>第一个传真号: 区号 + 编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>KTOKD</td>
<td>11</td>
<td>&nbsp;</td>
<td>KTOKD</td>
<td>KTOKD</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>客户帐户组</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>KUNNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>LANGU</td>
<td>12</td>
<td>&nbsp;</td>
<td>LANGU</td>
<td>SPRAS</td>
<td>LANG</td>
<td>1</td>
<td>X</td>
<td>语言代码</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MESSAGE</td>
<td>22</td>
<td>&nbsp;</td>
<td>BAPI_MSG</td>
<td>TEXT220</td>
<td>CHAR</td>
<td>220</td>
<td>&nbsp;</td>
<td>消息文本</td>
</tr>
<tr class="cell">
<td>11</td>
<td>MOB_NUMBER</td>
<td>18</td>
<td>&nbsp;</td>
<td>AD_MBNMBR1</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>第一个移动电话号码：区号 + 电话号码</td>
</tr>
<tr class="cell">
<td>12</td>
<td>NAME1</td>
<td>4</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>13</td>
<td>NAME2</td>
<td>5</td>
<td>&nbsp;</td>
<td>NAME2</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称 2</td>
</tr>
<tr class="cell">
<td>14</td>
<td>PARTNER</td>
<td>2</td>
<td>&nbsp;</td>
<td>BU_PARTNER</td>
<td>BU_PARTNER</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>业务伙伴编号</td>
</tr>
<tr class="cell">
<td>15</td>
<td>REGION</td>
<td>14</td>
<td>&nbsp;</td>
<td>REGIO</td>
<td>REGIO</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>地区（省/自治区/直辖市、市、县）</td>
</tr>
<tr class="cell">
<td>16</td>
<td>SMTP_ADDR</td>
<td>19</td>
<td>&nbsp;</td>
<td>AD_SMTPADR</td>
<td>AD_SMTPADR</td>
<td>CHAR</td>
<td>241</td>
<td>X</td>
<td>电子邮件地址</td>
</tr>
<tr class="cell">
<td>17</td>
<td>SORTL</td>
<td>9</td>
<td>&nbsp;</td>
<td>BU_SORT1</td>
<td>BU_SORT1</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>业务伙伴的搜索词 1</td>
</tr>
<tr class="cell">
<td>18</td>
<td>STATUS</td>
<td>21</td>
<td>&nbsp;</td>
<td>ZE_PROSTATUS</td>
<td>ZD_PROSTATUS</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>处理状态</td>
</tr>
<tr class="cell">
<td>19</td>
<td>STCD5</td>
<td>7</td>
<td>&nbsp;</td>
<td>STCD5</td>
<td>CHAR60</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>税号 5</td>
</tr>
<tr class="cell">
<td>20</td>
<td>TEL_NUMBER</td>
<td>17</td>
<td>&nbsp;</td>
<td>AD_TLNMBR1</td>
<td>CHAR30</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>第一个电话号码：区号 + 号码</td>
</tr>
<tr class="cell">
<td>21</td>
<td>VBUND</td>
<td>16</td>
<td>&nbsp;</td>
<td>RASSC</td>
<td>RCOMP</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>贸易合作伙伴的公司标识</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZTYPE</td>
<td>10</td>
<td>&nbsp;</td>
<td>BU_ID_TYPE</td>
<td>BU_ID_TYPE</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>标识类型</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>BU_TYPE</td>
<td>1</td>
<td>&nbsp;</td>
<td>人员</td>
</tr>
<tr class="cell">
<td>BU_TYPE</td>
<td>2</td>
<td>&nbsp;</td>
<td>组织</td>
</tr>
<tr class="cell">
<td>BU_TYPE</td>
<td>3</td>
<td>&nbsp;</td>
<td>组</td>
</tr>
<tr class="cell">
<td>ZD_PROSTATUS</td>
<td>F</td>
<td>&nbsp;</td>
<td>处理失败</td>
</tr>
<tr class="cell">
<td>ZD_PROSTATUS</td>
<td>S</td>
<td>&nbsp;</td>
<td>处理成功</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>