<table class="outerTable">
<tr>
<td><h2>Table: ZSDS001</h2>
<h3>Description: 交货单抬头增强字段</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZCPHM</td>
<td>1</td>
<td>&nbsp;</td>
<td>ZESDCPHM</td>
<td>CHAR10</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>车牌号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZCSRQ</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZESDCSRQ</td>
<td>ZDSDCSRQ</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>传输日期</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZCYRT</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZESDCYRT</td>
<td>CHAR15</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>承运人电话</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZCYRY</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZESDCYRY</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>承运人</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZCYWL</td>
<td>2</td>
<td>&nbsp;</td>
<td>ZESDCYWL</td>
<td>CHAR40</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>承运物流</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZJZXH1</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZESDJZXH1</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>集装箱号1</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZJZXH2</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZESDJZXH2</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>集装箱号2</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZJZXHZL1</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZESDJZXHZL1</td>
<td>ZDSDJZXHZL1</td>
<td>QUAN</td>
<td>5</td>
<td>&nbsp;</td>
<td>集装箱号1重量</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZJZXHZL2</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZESDJZXHZL2</td>
<td>ZDSDJZXHZL2</td>
<td>QUAN</td>
<td>5</td>
<td>&nbsp;</td>
<td>集装箱号2重量</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZZDR</td>
<td>10</td>
<td>&nbsp;</td>
<td>ZESDZDR</td>
<td>ZDSDZDR</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>制单人</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>