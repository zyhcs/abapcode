<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD036_CUSTOMERSAVE</h2>
<h3> Description: 客户主数据对象号保存</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd036_customersave.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_KUNNR) TYPE  KNA1-KUNNR<br/>
*"     VALUE(I_UUID) TYPE  SYSUUID_C32 OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_sd036_customersave.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:ls_data&nbsp;TYPE&nbsp;ztsd005.<br/>
&nbsp;&nbsp;IF&nbsp;i_uuid&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;i_uuid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
* 创建<br/>
</div>
<div class="code">
&nbsp;&nbsp;ls_data-kunnr&nbsp;=&nbsp;i_kunnr.<br/>
&nbsp;&nbsp;ls_data-guid&nbsp;&nbsp;=&nbsp;i_uuid.<br/>
&nbsp;&nbsp;ls_data-zz_crt_usr&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;ls_data-zz_crt_dat&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;ls_data-zz_crt_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;MODIFY&nbsp;ztsd005&nbsp;FROM&nbsp;ls_data&nbsp;.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>