<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD041B_INV_APL_BACK_RES</h2>
<h3> Description: 销售开票申请状态审批回传接口</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd041b_inv_apl_back_res.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_INPUT) TYPE  ZMT_ERP_SD041B_INV_APL_BACK_R1<br/>
*"  EXPORTING<br/>
*"     VALUE(E_OUTPUT) TYPE  ZMT_ERP_SD041B_INV_APL_BACK_RE<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_sd041b_inv_apl_back_res.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:ls_header&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041b_inv_apl_back_r6.<br/>
&nbsp;&nbsp;DATA:ls_message_header_rs&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041b_inv_apl_back_r2.<br/>
&nbsp;&nbsp;DATA:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_vbeln&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbrk-vbeln.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lt_return&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapireturn1&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_messtab&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bdcmsgcoll&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_success&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapivbrksuccess&nbsp;WITH&nbsp;HEADER&nbsp;LINE.<br/>
&nbsp;&nbsp;DATA:lv_ret_msg&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA:lv_vbeln_001&nbsp;&nbsp;&nbsp;TYPE&nbsp;bdcdata-fval.<br/>
&nbsp;&nbsp;DATA:lv_bill_doc&nbsp;TYPE&nbsp;&nbsp;bapivbrksuccess-bill_doc.<br/>
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'SI_OA2ERP_SD041B_InvAplBack_Sync_IN'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ls_header&nbsp;=&nbsp;i_input-mt_erp_sd041b_inv_apl_back_req-zdata-header.<br/>
&nbsp;&nbsp;lv_vbeln&nbsp;=&nbsp;ls_header-vbeln.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;lv_vbeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_vbeln.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'&nbsp;.<br/>
</div>
<div class="codeComment">
*   同意<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header-zspbs&nbsp;=&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'同意审批，生成会计凭证'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vbeln_001&nbsp;=&nbsp;lv_vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. zif_sd041b_vf02_interface="" zif_sd041b_vf02_interface.html"="">'ZIF_SD041B_VF02_INTERFACE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ctu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'N'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'L'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln_001&nbsp;=&nbsp;lv_vbeln_001<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;messtab&nbsp;&nbsp;&nbsp;=&nbsp;lt_messtab.<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
* 生成会计凭证<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-header_status&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_messtab&nbsp;WHERE&nbsp;msgtyp&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;lv_ret_msg&nbsp;lt_messtab-msgv1&nbsp;INTO&nbsp;lv_ret_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lv_ret_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-header_status&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="codeComment">
*   不同意<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'拒绝审批，进行发票冲销'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bill_doc&nbsp;=&nbsp;lv_vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_BILLINGDOC_CANCEL1'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;billingdocument&nbsp;=&nbsp;lv_bill_doc<br/>
</div>
<div class="codeComment">
*         TESTRUN         =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_commit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;billingdate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-datum&nbsp;"取消开票日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;success&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_success.<br/>
</div>
<div class="codeComment">
* 冲销失败<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_return&nbsp;TRANSPORTING&nbsp;NO&nbsp;FIELDS&nbsp;WITH&nbsp;KEY&nbsp;type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
</div>
<div class="codeComment">
* 回滚事务<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-header_status&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_return.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;lv_ret_msg&nbsp;lt_return-message&nbsp;INTO&nbsp;lv_ret_msg&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lv_ret_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
</div>
<div class="codeComment">
* 提交事务<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 获取成功信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_success&nbsp;&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-header_status&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-vbeln&nbsp;=&nbsp;lt_success-bill_doc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-zdata-header-header_text&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ls_message_header_rs-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;ls_message_header_rs-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;ls_message_header_rs-bustyp&nbsp;=&nbsp;'SD041B'.<br/>
&nbsp;&nbsp;ls_message_header_rs-message_status&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;ls_message_header_rs-message_text&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;ls_message_header_rs-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;ls_message_header_rs-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;ls_message_header_rs-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;ls_message_header_rs-send_user&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;e_output-mt_erp_sd041b_inv_apl_back_res-message_header&nbsp;=&nbsp;ls_message_header_rs.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当ERP为接收方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lo_log-&gt;set_proxy_receiver_msgid(&nbsp;)&nbsp;.<br/>
<br/>
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;lv_vbeln&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>