<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZSD_VL_ITEM_PUT_DATA</h2>
<h3> Description: 将交货单行项目数据存入函数组</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zsd_vl_item_put_data.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IS_LIPS) TYPE  LIPS<br/>
*"     REFERENCE(IF_TRTYP) TYPE  TRTYP<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zsd_vl_item_put_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;trtyp&nbsp;=&nbsp;if_trtyp.<br/>
</div>
<div class="codeComment">
*  tcode = if_tcode.<br/>
</div>
<div class="code">
&nbsp;&nbsp;gs_lips&nbsp;=&nbsp;is_lips.<br/>
&nbsp;&nbsp;zzdr&nbsp;=&nbsp;is_lips-zzdr.<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;wbstk&nbsp;FROM&nbsp;likp&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;@is_lips-vbeln&nbsp;INTO&nbsp;@v_wbstk.<br/>
</div>
<div class="codeComment">
*  MOVE-CORRESPONDING is_likp-zzdr TO zzdr.<br/>
<br/>
<br/>
</div>
<div class="code">
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>