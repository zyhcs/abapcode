<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG_002TOP</h2>
<h3> Description: 将交货单行项目数据存入函数组</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zsdg_002.                     "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZSDG_002D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
</div>
<div class="code">
DATA:gs_lips TYPE lips,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trtyp&nbsp;&nbsp;&nbsp;TYPE&nbsp;trtyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzdr&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lips-zzdr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;v_wbstk&nbsp;TYPE&nbsp;likp-wbstk.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>