<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZFM_CREATE_CUSTOMER_BASIS</h2>
<h3> Description: 客户主数据基本数据创建</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZFM_CREATE_CUSTOMER_BASIS.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IS_INPUT) TYPE  ZSBPBASIS<br/>
*"  EXPORTING<br/>
*"     REFERENCE(ES_RETURN) TYPE  ZSBPBASIS_RETURN<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zfm_create_customer_basis.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;@DATA(LS_BUT000)<br/>
&nbsp;&nbsp;FROM&nbsp;BUT000<br/>
&nbsp;&nbsp;WHERE&nbsp;PARTNER&nbsp;=&nbsp;@IS_INPUT-PARTNER.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;EQ&nbsp;0.<br/>
<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"创建客户主数据(基本视图)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;FRM_CUSTOMER_CREATE&nbsp;USING&nbsp;IS_INPUT&nbsp;CHANGING&nbsp;ES_RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>