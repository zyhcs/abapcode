<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZFG_BPTOP</h2>
<h3> Description: 客户主数据基本数据创建</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL ZFG_BP.                       "MESSAGE-ID ..<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZFG_BPD...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
<br/>
<br/>
</div>
<div class="code">
CONSTANTS: CONS_S      TYPE CHAR1 VALUE 'S',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONS_F&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;CHAR1&nbsp;VALUE&nbsp;'F',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONS_TASK_M&nbsp;TYPE&nbsp;BUS_EI_OBJECT_TASK&nbsp;VALUE&nbsp;'M',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONS_TASK_U&nbsp;TYPE&nbsp;BUS_EI_OBJECT_TASK&nbsp;VALUE&nbsp;'U',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONS_TASK_D&nbsp;TYPE&nbsp;BUS_EI_OBJECT_TASK&nbsp;VALUE&nbsp;'D',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONS_TASK_I&nbsp;TYPE&nbsp;BUS_EI_OBJECT_TASK&nbsp;VALUE&nbsp;'I'.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>