<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD030_RES</h2>
<h3> Description: 开门生活发送采购信息到SAP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd030_res.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IT_INPUT) TYPE  ZPIMT_ERP_SD030_PURCHASEORDER1<br/>
*"  EXPORTING<br/>
*"     REFERENCE(OT_OUTPUT) TYPE  ZPIMT_ERP_SD030_PURCHASEORDER<br/>
*"     VALUE(OS_RETURN) TYPE  BAPIRET2<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_sd030_res.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lv_mstype&nbsp;TYPE&nbsp;bapi_mtype&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_msg&nbsp;TYPE&nbsp;bapi_msg.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_guid&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_return&nbsp;&nbsp;TYPE&nbsp;&nbsp;bapiret2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;TYPE&nbsp;c<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;ls_header&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseorder7.<br/>
<br/>
&nbsp;&nbsp;DATA:lt_items&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseo_tab1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item&nbsp;&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseorder5.<br/>
<br/>
&nbsp;&nbsp;DATA:os_item&nbsp;&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseorder4,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ot_items&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseor_tab.<br/>
&nbsp;&nbsp;DATA&nbsp;:&nbsp;os_header&nbsp;TYPE&nbsp;zpidt_erp_sd030_purchaseorder6.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_log&nbsp;TYPE&nbsp;ztsd030l,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_log&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ztsd030l.<br/>
&nbsp;&nbsp;DATA:lv_zyyyhg&nbsp;TYPE&nbsp;&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_atwrt&nbsp;&nbsp;TYPE&nbsp;&nbsp;ausp-atwrt.<br/>
&nbsp;&nbsp;DATA:lv_bili&nbsp;&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;IF&nbsp;it_input&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'没有接收到数据'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;lv_guid&nbsp;=&nbsp;it_input-mt_erp_sd030_purchaseorder_req-message_header-messageid.<br/>
<br/>
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_guid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'SI_ERP_SD030_PURCHASEORDER_SYN_IN'&nbsp;).<br/>
<br/>
</div>
<div class="codeComment">
* 检查接口是否启用<br/>
* --------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'接口未启用!'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header&nbsp;=&nbsp;it_input-mt_erp_sd030_purchaseorder_req-zdata-header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_items&nbsp;=&nbsp;it_input-mt_erp_sd030_purchaseorder_req-zdata-items.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;lt_items[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'没有接收到数据!'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;lv_vbeln&nbsp;TYPE&nbsp;vbeln_va."销售凭证<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"由于开门生活并没有销售单号，如果是做退货处理的话，要去日志表找以前创建的销售订单记录，看是否创建过<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vbeln&nbsp;INTO&nbsp;lv_vbeln&nbsp;FROM&nbsp;ztsd030l&nbsp;WHERE&nbsp;bstkd&nbsp;=&nbsp;ls_header-bstkd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_vbeln&nbsp;IS&nbsp;INITIAL&nbsp;AND&nbsp;ls_header-auart&nbsp;=&nbsp;'ZRE1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'采购订单'&nbsp;&amp;&amp;&nbsp;ls_header-bstkd&nbsp;&amp;&amp;&nbsp;'没有找到对应的销售订单进行退货处理！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;"把接口数据转成SAP结构数据<br/>
&nbsp;&nbsp;DATA&nbsp;lv_dzieme&nbsp;TYPE&nbsp;meins.&nbsp;"内部单位<br/>
&nbsp;&nbsp;DATA&nbsp;lv_pr&nbsp;TYPE&nbsp;i&nbsp;VALUE&nbsp;0.<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ATINN_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;'ZYYYHG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_items&nbsp;INTO&nbsp;ls_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_dzieme,lv_atwrt,lv_bili.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-matnr&nbsp;&nbsp;=&nbsp;ls_item-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kwmeng&nbsp;&nbsp;=&nbsp;ls_item-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kbetr&nbsp;=&nbsp;ls_item-kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;atwrt&nbsp;INTO&nbsp;lv_atwrt&nbsp;FROM&nbsp;ausp&nbsp;WHERE&nbsp;objek&nbsp;=&nbsp;ls_log-matnr&nbsp;AND&nbsp;atinn&nbsp;=&nbsp;lv_zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vrkme&nbsp;INTO&nbsp;lv_dzieme&nbsp;FROM&nbsp;ztsd011&nbsp;WHERE&nbsp;zomsdw&nbsp;=&nbsp;ls_item-vrkme&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"如果单位异常，抓错，<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;=&nbsp;'单位'&nbsp;&amp;&amp;&nbsp;ls_item-vrkme&nbsp;&amp;&amp;&nbsp;'异常'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vrkme&nbsp;=&nbsp;lv_dzieme.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_CUNIT_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-vrkme<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-vrkme<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unit_not_found&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"开门发过来的单位不确定是什么，做转换并且把数量也做转换<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;=&nbsp;'盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_menge&nbsp;TYPE&nbsp;ekpo-menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_menge&nbsp;=&nbsp;ls_log-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MD_CONVERT_MATERIAL_UNIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_in_me&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-vrkme<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_out_me&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'KG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_in_application&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_log-kwmeng&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bili&nbsp;=&nbsp;lv_menge&nbsp;/&nbsp;ls_log-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kwmeng&nbsp;=&nbsp;lv_menge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_bili&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kbetr&nbsp;=&nbsp;ls_log-kbetr&nbsp;/&nbsp;lv_bili.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vrkme&nbsp;=&nbsp;'KG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;=&nbsp;'SAP没有单位：'&nbsp;&amp;&amp;&nbsp;ls_item-vrkme&nbsp;&amp;&amp;&nbsp;'与KG的转换关系'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_pr&nbsp;=&nbsp;lv_pr&nbsp;+&nbsp;10.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-auart&nbsp;&nbsp;=&nbsp;ls_header-auart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vkorg&nbsp;&nbsp;=&nbsp;ls_header-vkorg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vtweg&nbsp;&nbsp;=&nbsp;ls_header-vtweg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-spart&nbsp;&nbsp;=&nbsp;ls_header-spart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vkbur&nbsp;&nbsp;=&nbsp;ls_header-vkbur.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kunag&nbsp;&nbsp;=&nbsp;ls_header-kunag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kunnr&nbsp;&nbsp;=&nbsp;ls_header-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-zshdz&nbsp;&nbsp;=&nbsp;ls_header-zshdz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vsbed&nbsp;&nbsp;=&nbsp;ls_header-vsbed.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-augru&nbsp;&nbsp;=&nbsp;ls_header-augru.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header-zlsch&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;ls_header-zlsch&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-dzlsch&nbsp;=&nbsp;'K'.&nbsp;&nbsp;"先货后款<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-dzlsch&nbsp;=&nbsp;ls_header-zlsch.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vsart&nbsp;&nbsp;=&nbsp;ls_header-vsart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-kalsm&nbsp;&nbsp;=&nbsp;ls_header-kalsm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-vbeln&nbsp;&nbsp;=&nbsp;lv_vbeln.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"无论开门是什么业务，都不会传销售订单过来<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-zqygs&nbsp;=&nbsp;ls_header-zqygs.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-bstkd&nbsp;&nbsp;=&nbsp;ls_header-bstkd.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-posnr&nbsp;&nbsp;=&nbsp;lv_pr.&nbsp;&nbsp;"ls_item-posnr.<br/>
</div>
<div class="codeComment">
*      ls_log-kbetr  = ls_item-kbetr.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-arktx&nbsp;&nbsp;=&nbsp;ls_item-arktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-pstyv&nbsp;&nbsp;=&nbsp;ls_item-pstyv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-zrsdt&nbsp;&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_log-zrstm&nbsp;&nbsp;=&nbsp;sy-uzeit.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_log&nbsp;TO&nbsp;lt_log.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_log,ls_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;"执行业务，<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"按照接口设计，开门每次调接口只发送一个抬头数据，<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"传输订单类型为：ZOR1常规订单、ZRE1退货订单、ZSF1买赠订单、ZSO1样品订单。<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;ls_header_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapisdhd1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapisdhd1x,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_return2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiret2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_return2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiret2&nbsp;WITH&nbsp;HEADER&nbsp;&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapisditm&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapisditmx&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_partners&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiparnr&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_in&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapischdl&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_inx&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapischdlx&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicond&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapicondx&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bape_vbak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bape_vbak,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bape_vbakx&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bape_vbakx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bape_vbap&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bape_vbap,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_bape_vbapx&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bape_vbapx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_extensionin&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiparex&nbsp;WITH&nbsp;HEADER&nbsp;LINE,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_ordertext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapisdtext.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;ev_salesdocument&nbsp;TYPE&nbsp;bapivbeln-vbeln.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_log&gt;&nbsp;TYPE&nbsp;ztsd030l.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_updateflag&nbsp;TYPE&nbsp;c&nbsp;VALUE&nbsp;'I'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_log&nbsp;INTO&nbsp;ls_log&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-doc_type&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-auart.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;销售凭证类型&nbsp;&nbsp;&nbsp;”固定<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-sales_org&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-vkorg.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;销售机构<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-distr_chan&nbsp;&nbsp;=&nbsp;ls_log-vtweg.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;分销渠道<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-division&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-spart.&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;产品组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-sales_off&nbsp;&nbsp;=&nbsp;ls_log-vkbur.&nbsp;&nbsp;&nbsp;"销售办事处<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-purch_no_c&nbsp;&nbsp;&nbsp;=&nbsp;ls_log-bstkd.&nbsp;&nbsp;&nbsp;"采购订单<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-ship_type&nbsp;&nbsp;=&nbsp;ls_log-vsart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-ship_cond&nbsp;&nbsp;=&nbsp;ls_log-vsbed.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_in-pymt_meth&nbsp;&nbsp;=&nbsp;ls_log-dzlsch.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-doc_type&nbsp;&nbsp;&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-sales_org&nbsp;&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-distr_chan&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-division&nbsp;&nbsp;&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-sales_off&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-purch_no_c&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-ship_type&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-ship_cond&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-updateflag&nbsp;=&nbsp;lv_updateflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_header_inx-pymt_meth&nbsp;=&nbsp;abap_true.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;售达方<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_partners-partn_role&nbsp;=&nbsp;'AG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lt_partners-partn_numb&nbsp;=&nbsp;|{&nbsp;ls_log-kunag&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_partners.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_partners.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_log&nbsp;ASSIGNING&nbsp;&lt;fs_log&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-purch_no_s&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_log&gt;-bstkd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-material&nbsp;=&nbsp;|{&nbsp;&lt;fs_log&gt;-matnr&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-target_qty&nbsp;=&nbsp;&lt;fs_log&gt;-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-pymt_meth&nbsp;=&nbsp;&lt;fs_log&gt;-dzlsch.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-item_categ&nbsp;=&nbsp;&lt;fs_log&gt;-pstyv.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_in-target_qu&nbsp;=&nbsp;&lt;fs_log&gt;-vrkme.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_items_in.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_items_in.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-purch_no_s&nbsp;&nbsp;&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-material&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-target_qty&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-target_qu&nbsp;&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-item_categ&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-updateflag&nbsp;=&nbsp;lv_updateflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items_inx-pymt_meth&nbsp;=&nbsp;abap_true.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_items_inx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_items_inx.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"计划行数据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_in-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_in-req_qty&nbsp;=&nbsp;&lt;fs_log&gt;-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_in-req_date&nbsp;=&nbsp;sy-datum.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"在接口配置信息里没有这个字段，需要问下业务顾问是否默认为当前日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_schedules_in.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_schedules_in.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_inx-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_inx-updateflag&nbsp;=&nbsp;lv_updateflag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_inx-req_qty&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_schedules_inx-req_date&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lt_schedules_inx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_schedules_inx.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_log&gt;-kbetr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.&nbsp;&nbsp;&nbsp;"itm_number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-cond_type&nbsp;&nbsp;=&nbsp;&nbsp;'ZR01'&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"定价条件<br/>
</div>
<div class="codeComment">
*      IF ls_header-auart = 'ZRE1'.<br/>
*        lt_conditons_in-cond_type  = ''.<br/>
*      ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-cond_value&nbsp;=&nbsp;&lt;fs_log&gt;-kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-currency&nbsp;&nbsp;&nbsp;=&nbsp;'CNY'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-cond_unit&nbsp;&nbsp;=&nbsp;&lt;fs_log&gt;-vrkme.&nbsp;&nbsp;&nbsp;"条件单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_in-cond_p_unt&nbsp;=&nbsp;&nbsp;1&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;&nbsp;lt_conditons_in.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_conditons_in.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-itm_number&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-cond_type&nbsp;&nbsp;=&nbsp;&nbsp;'ZR01'&nbsp;.<br/>
</div>
<div class="codeComment">
*      IF ls_header-auart = 'ZRE1'.<br/>
*        lt_conditons_inx-cond_type  = ''.<br/>
*      ENDIF.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-cond_value&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-currency&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-cond_unit&nbsp;&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_conditons_inx-cond_p_unt&nbsp;=&nbsp;&nbsp;'X'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;&nbsp;lt_conditons_inx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lt_conditons_inx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_header-auart&nbsp;=&nbsp;'ZRE1'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"退货<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ev_salesdocument&nbsp;=&nbsp;ls_log-vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_CUSTOMERRETURN_CREATE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*         SALESDOCUMENTIN      = ls_header-vbeln<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_header_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_header_inx&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_inx<br/>
</div>
<div class="codeComment">
*         SENDER               =<br/>
*         BINARY_RELATIONSHIPTYPE       =<br/>
*         INT_NUMBER_ASSIGNMENT         =<br/>
*         BEHAVE_WHEN_ERROR    =<br/>
*         LOGIC_SWITCH         =<br/>
*         TESTRUN              =<br/>
*         CONVERT              = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;salesdocument&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ev_salesdocument<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_items_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_items_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_items_inx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_items_inx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_partners&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_partners<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_schedules_in&nbsp;&nbsp;=&nbsp;lt_schedules_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_schedules_inx&nbsp;=&nbsp;lt_schedules_inx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return_conditions_in&nbsp;=&nbsp;lt_conditons_in<br/>
</div>
<div class="codeComment">
*         RETURN_CFGS_REF      =<br/>
*         RETURN_CFGS_INST     =<br/>
*         RETURN_CFGS_PART_OF  =<br/>
*         RETURN_CFGS_VALUE    =<br/>
*         RETURN_CFGS_BLOB     =<br/>
*         RETURN_CFGS_VK       =<br/>
*         RETURN_CFGS_REFINST  =<br/>
*         RETURN_TEXT          =<br/>
*         RETURN_KEYS          =<br/>
*         EXTENSIONIN          =<br/>
*         PARTNERADDRESSES     =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;ls_header-auart&nbsp;=&nbsp;'ZOR1'&nbsp;OR&nbsp;ls_header-auart&nbsp;=&nbsp;'ZSF1'&nbsp;OR&nbsp;ls_header-auart&nbsp;=&nbsp;'ZSO1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_SALESORDER_CREATEFROMDAT2'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*         SALESDOCUMENTIN      =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_header_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_header_inx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_inx<br/>
</div>
<div class="codeComment">
*         SENDER               =<br/>
*         BINARY_RELATIONSHIPTYPE       =<br/>
*         INT_NUMBER_ASSIGNMENT         =<br/>
*         BEHAVE_WHEN_ERROR    =<br/>
*         LOGIC_SWITCH         =<br/>
*         TESTRUN              =<br/>
*         CONVERT              = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;salesdocument&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ev_salesdocument<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_items_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_items_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_items_inx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_items_inx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_partners&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_partners<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_schedules_in&nbsp;&nbsp;&nbsp;=&nbsp;lt_schedules_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_schedules_inx&nbsp;&nbsp;=&nbsp;lt_schedules_inx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_conditions_in&nbsp;&nbsp;=&nbsp;lt_conditons_in<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_conditions_inx&nbsp;=&nbsp;lt_conditons_inx<br/>
</div>
<div class="codeComment">
*         ORDER_CFGS_REF       =<br/>
*         ORDER_CFGS_INST      =<br/>
*         ORDER_CFGS_PART_OF   =<br/>
*         ORDER_CFGS_VALUE     =<br/>
*         ORDER_CFGS_BLOB      =<br/>
*         ORDER_CFGS_VK        =<br/>
*         ORDER_CFGS_REFINST   =<br/>
*         ORDER_CCARD          =<br/>
*         ORDER_TEXT           =<br/>
*         ORDER_KEYS           =<br/>
*         EXTENSIONIN          =<br/>
*         PARTNERADDRESSES     =<br/>
*         EXTENSIONEX          =<br/>
*         NFMETALLITMS         =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"创建成功后，保存长文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ev_salesdocument&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA:ls_header_obj&nbsp;TYPE&nbsp;thead&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA:lt_ltxts&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;tline,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ltxt&nbsp;&nbsp;TYPE&nbsp;tline.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_header_obj&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdobject&nbsp;=&nbsp;'VBBK'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'0001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdspras&nbsp;&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdname&nbsp;&nbsp;&nbsp;=&nbsp;ev_salesdocument.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_log&nbsp;INTO&nbsp;ls_log&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ltxt-tdformat&nbsp;=&nbsp;'*'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ltxt-tdline&nbsp;=&nbsp;ls_log-zshdz.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_ltxt&nbsp;TO&nbsp;lt_ltxts.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_log&nbsp;,ls_ltxt.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SAVE_TEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*           CLIENT          = SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_obj<br/>
</div>
<div class="codeComment">
*           INSERT          = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;savemode_direct&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*           OWNER_SPECIFIED = ' '<br/>
*           LOCAL_CAT       = ' '<br/>
*           KEEP_LAST_CHANGED       = ' '<br/>
*     IMPORTING<br/>
*           FUNCTION        =<br/>
*           NEWHEADER       =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_ltxts<br/>
</div>
<div class="codeComment">
*     EXCEPTIONS<br/>
*           ID              = 1<br/>
*           LANGUAGE        = 2<br/>
*           NAME            = 3<br/>
*           OBJECT          = 4<br/>
*           OTHERS          = 5<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
* Implement suitable error handling here<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"将区域公司写入抬头文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_header_obj,lt_ltxts,ls_ltxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdobject&nbsp;=&nbsp;'VBBK'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'0012'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdspras&nbsp;&nbsp;=&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header_obj-tdname&nbsp;&nbsp;&nbsp;=&nbsp;ev_salesdocument.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_log&nbsp;INTO&nbsp;ls_log&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ltxt-tdformat&nbsp;=&nbsp;'*'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_ltxt-tdline&nbsp;=&nbsp;ls_log-zqygs.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_ltxt&nbsp;TO&nbsp;lt_ltxts.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_log&nbsp;,ls_ltxt.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SAVE_TEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*           CLIENT          = SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_header_obj<br/>
</div>
<div class="codeComment">
*           INSERT          = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;savemode_direct&nbsp;=&nbsp;'X'<br/>
</div>
<div class="codeComment">
*           OWNER_SPECIFIED = ' '<br/>
*           LOCAL_CAT       = ' '<br/>
*           KEEP_LAST_CHANGED       = ' '<br/>
*     IMPORTING<br/>
*           FUNCTION        =<br/>
*           NEWHEADER       =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_ltxts<br/>
</div>
<div class="codeComment">
*     EXCEPTIONS<br/>
*           ID              = 1<br/>
*           LANGUAGE        = 2<br/>
*           NAME            = 3<br/>
*           OBJECT          = 4<br/>
*           OTHERS          = 5<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
* Implement suitable error handling here<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"获取消息<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;&nbsp;lt_return2&nbsp;&nbsp;INTO&nbsp;ls_return2&nbsp;WHERE&nbsp;type&nbsp;CA&nbsp;'AE'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_m&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MESSAGE_TEXT_BUILD'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-id<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-message_v1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-message_v2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-message_v3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgv4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_return2-message_v4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message_text_output&nbsp;=&nbsp;lv_m.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lv_msg&nbsp;&amp;&amp;&nbsp;lv_m.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_log&nbsp;ASSIGNING&nbsp;&lt;fs_log&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_log&gt;-zrstp&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_log&gt;-zrsmg&nbsp;&nbsp;=&nbsp;ls_header-auart&nbsp;&amp;&amp;&nbsp;'业务执行成功。'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_log&gt;-vbeln&nbsp;=&nbsp;ev_salesdocument.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_key&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_status&nbsp;=&nbsp;&lt;fs_log&gt;-zrstp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_text&nbsp;=&nbsp;&lt;fs_log&gt;-zrsmg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;os_item&nbsp;TO&nbsp;ot_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;os_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-header_status&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-header_text&nbsp;=&nbsp;ls_header-auart&nbsp;&amp;&amp;&nbsp;'业务执行成功。'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-vbeln&nbsp;=&nbsp;ev_salesdocument.<br/>
</div>
<div class="codeComment">
*    ls_return-type = 'S' .<br/>
*    ls_return-message   = ls_header-auart &amp;&amp; '业务执行成功。'.<br/>
*    APPEND ls_return TO lt_return  .<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'&nbsp;.<br/>
</div>
<div class="codeComment">
* " 获取错误信息<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_log&nbsp;ASSIGNING&nbsp;&lt;fs_log&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_log&gt;-zrstp&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_log&gt;-zrsmg&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_key&nbsp;=&nbsp;&lt;fs_log&gt;-posnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_status&nbsp;=&nbsp;&lt;fs_log&gt;-zrstp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;os_item-item_text&nbsp;=&nbsp;&lt;fs_log&gt;-zrsmg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;os_item&nbsp;TO&nbsp;ot_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;os_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-header_status&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;os_header-header_text&nbsp;=&nbsp;lv_msg.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"将创建成功的数据保存到日志表<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lt_log_mod&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ztsd030l.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_log&nbsp;ASSIGNING&nbsp;&lt;fs_log&gt;&nbsp;WHERE&nbsp;zrstp&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;&lt;fs_log&gt;&nbsp;TO&nbsp;lt_log_mod.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lt_log_mod[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;ztsd030l&nbsp;FROM&nbsp;TABLE&nbsp;lt_log_mod.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_mstype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_mstype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;os_return-type&nbsp;=&nbsp;lv_mstype.<br/>
&nbsp;&nbsp;os_return-message&nbsp;=&nbsp;lv_msg.<br/>
<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-message_header_rs-message_status&nbsp;=&nbsp;os_header-header_status.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-message_header_rs-message_text&nbsp;=&nbsp;os_header-header_text.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-message_header_rs-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-message_header_rs-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-message_header_rs-send_user&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-zdata-header&nbsp;=&nbsp;os_header.<br/>
&nbsp;&nbsp;ot_output-mt_erp_sd030_purchaseorder_res-zdata-items&nbsp;=&nbsp;ot_items.<br/>
<br/>
</div>
<div class="codeComment">
*当ERP为接收方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;"LO_LOG-&gt;set_proxy_in_msgid(&nbsp;)&nbsp;.<br/>
&nbsp;&nbsp;lo_log-&gt;set_proxy_receiver_msgid(&nbsp;)&nbsp;.<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*当接口出现错误时,获取函数传入传出参数,以便重推<br/>
*--------------------------------------------------------------------*<br/>
*  IF LS_RETURN-TYPE = 'E'.<br/>
*    FIELD-SYMBOLS &lt;PARAS&gt; .<br/>
*    LO_LOG-&gt;GET_FUNC_PARAS( IMPORTING PARAS = DATA(LT_PARAS) ).<br/>
*    LOOP AT LT_PARAS  ASSIGNING  FIELD-SYMBOL(&lt;LS_PARAS&gt;) .<br/>
*      ASSIGN (&lt;LS_PARAS&gt;-PARAMETER) TO &lt;PARAS&gt;.<br/>
*      CHECK SY-SUBRC = 0.<br/>
*      &lt;LS_PARAS&gt;-VALUE = REF #( &lt;PARAS&gt; ).<br/>
*    ENDLOOP.<br/>
*    LO_LOG-&gt;SET_FUNC_MSG(<br/>
*        EXPORTING<br/>
*          PARAS  = LT_PARAS<br/>
*          REPUSH = 'X' ).<br/>
*  ENDIF.<br/>
<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_guid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_mstype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;''&nbsp;).&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>