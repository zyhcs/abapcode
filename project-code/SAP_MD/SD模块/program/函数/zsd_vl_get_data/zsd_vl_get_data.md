<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZSD_VL_GET_DATA</h2>
<h3> Description: 从函数组获取交货单数据</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZSD_VL_GET_DATA.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_ZSDS001) TYPE  ZSDS001<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<br/>
<div class="codeComment">*       <a href="global-zsd_vl_get_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
e_zsds001 = zsds001.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>