<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG_001TOP</h2>
<h3> Description: 从函数组获取交货单数据</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION-POOL zsdg_001.                     "MESSAGE-ID ..<br/>
<br/>
DATA:gs_likp        TYPE likp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_likpd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likpd,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_ylikp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_ylikp_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xvbuk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbuk_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_yvbuk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbuk_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xvbup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbup_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_yvbup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbup_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xlips	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_lips_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_ylips&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_lips_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xvbpa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vbpavb_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_yvbpa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vbpavb_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xvbfa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbfa_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_yvbfa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_vl10_vbfa_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_xvbadr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;shp_sadrvb_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_v50agl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;v50agl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;trtyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;trtyp,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tcode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;le_shp_tcode,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cs_v50agl_cust&nbsp;TYPE&nbsp;v50agl_cust.<br/>
TABLES zsds001.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;INCLUDE&nbsp;LZSDG_001D...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Local&nbsp;class&nbsp;definition<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>