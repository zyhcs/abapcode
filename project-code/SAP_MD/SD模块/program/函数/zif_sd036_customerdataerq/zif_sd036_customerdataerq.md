<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD036_CUSTOMERDATAERQ</h2>
<h3> Description: 客户主数据发送外围系统</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd036_customerdataerq.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_KUNNR) TYPE  KNA1-KUNNR<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_MSG_TYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(E_MSG) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_sd036_customerdataerq.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:customerdataerq&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zco_si_erp_sd036_customerdata1.<br/>
&nbsp;&nbsp;DATA:lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_erp_sd036_customerdata_re1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd036_customerdata_re5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header&nbsp;TYPE&nbsp;zdt_erp_sd036_customerdata_re4.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_kna1&nbsp;TYPE&nbsp;kna1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_lfbk&nbsp;TYPE&nbsp;lfbk.<br/>
&nbsp;&nbsp;DATA:lv_unw_remark&nbsp;TYPE&nbsp;bp001-unw_remark,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;adrct-remark,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bezei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t005u-bezei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vbund&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bp001-vbund,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bukrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztfico001-bukrs,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_smtp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;adr6-smtp_addr.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_zkhfl&nbsp;TYPE&nbsp;ztsd007-zkhfl.<br/>
</div>
<div class="codeComment">
* 获取UUID<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;lv_uuid.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
</div>
<div class="codeComment">
* 初始化日志<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_SD036_CUSTOMERDATAERQ'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'.<br/>
</div>
<div class="codeComment">
* 发送信息抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-receiver&nbsp;=&nbsp;'EAS'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-bustyp&nbsp;=&nbsp;'SD-036'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_user&nbsp;=&nbsp;sy-uname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_kunnr&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;ls_kna1&nbsp;FROM&nbsp;kna1&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;i_kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;*&nbsp;INTO&nbsp;ls_lfbk&nbsp;FROM&nbsp;lfbk&nbsp;WHERE&nbsp;lifnr&nbsp;=&nbsp;i_kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;unw_remark&nbsp;INTO&nbsp;lv_unw_remark&nbsp;FROM&nbsp;bp001&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;i_kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;remark&nbsp;INTO&nbsp;lv_remark&nbsp;FROM&nbsp;adrct&nbsp;WHERE&nbsp;addrnumber&nbsp;=&nbsp;ls_kna1-adrnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;smtp_addr&nbsp;INTO&nbsp;lv_smtp&nbsp;FROM&nbsp;adr6&nbsp;WHERE&nbsp;addrnumber&nbsp;=&nbsp;ls_kna1-adrnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkhfl&nbsp;INTO&nbsp;lv_zkhfl&nbsp;FROM&nbsp;ztsd007&nbsp;WHERE&nbsp;ort01&nbsp;=&nbsp;ls_kna1-ort01.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bezei&nbsp;INTO&nbsp;lv_bezei&nbsp;FROM&nbsp;t005u&nbsp;WHERE&nbsp;bland&nbsp;=&nbsp;ls_kna1-regio&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'&nbsp;AND&nbsp;land1&nbsp;=&nbsp;'CN'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-guid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_uuid&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;ls_kna1-kunnr&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-kunnr&nbsp;NO-GAPS.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vbund&nbsp;INTO&nbsp;lv_vbund&nbsp;FROM&nbsp;bp001&nbsp;WHERE&nbsp;partner&nbsp;=&nbsp;i_kunnr&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_bukrs&nbsp;=&nbsp;&nbsp;lv_vbund+2(4).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zkbukrs&nbsp;INTO&nbsp;ls_zdata_header-vbund&nbsp;FROM&nbsp;ztfico001&nbsp;WHERE&nbsp;bukrs&nbsp;=&nbsp;lv_bukrs&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_kna1-name1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ort01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zkhfl&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_kna1-stras&nbsp;=&nbsp;'香港'&nbsp;or&nbsp;ls_kna1-stras&nbsp;=&nbsp;'330'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bezei&nbsp;=&nbsp;'香港'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-stras&nbsp;=&nbsp;'香港'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bezei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_bezei&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-stras&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_kna1-stras&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_kna1-ktokd&nbsp;=&nbsp;'Z001'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ktokk&nbsp;=&nbsp;'Y'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-ktokk&nbsp;=&nbsp;'N'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*      ls_zdata_header-ktokk      = ls_kna1-ktokd  .<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-nodel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_kna1-nodel&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-stcd5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_kna1-stcd5&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-unw_remark&nbsp;=&nbsp;lv_unw_remark&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-tel_number&nbsp;=&nbsp;ls_kna1-telf1&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-fax_number&nbsp;=&nbsp;ls_kna1-telfx&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-smtp_addr&nbsp;&nbsp;=&nbsp;lv_smtp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_remark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-bankl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_lfbk-bankl&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_lfbk-bankn&nbsp;ls_lfbk-bkref&nbsp;INTO&nbsp;ls_zdata_header-zbank.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd036_customerdata_req-message_header&nbsp;=&nbsp;ls_msg_header.<br/>
</div>
<div class="codeComment">
*      APPEND ls_zdata_header TO ls_out-mt_erp_sd036_customerdata_req-zdata-header.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd036_customerdata_req-zdata-header&nbsp;=&nbsp;ls_zdata_header.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*   发送数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;customerdataerq&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;customerdataerq.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;customerdataerq-&gt;si_erp_sd036_customerdata_asyn<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;'发送成功！'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;customerdataerq&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 记录状态<br/>
</div>
<div class="code">
&nbsp;&nbsp;e_msg_type&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;e_msg&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
<br/>
</div>
<div class="codeComment">
*  记录日志<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;i_kunnr&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>