<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function GET_HEAD_ACTIVE_TAB_PAGE</h2>
<h3> Description: 获取自定义屏幕</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION get_head_active_tab_page .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(ACTIVE) TYPE  FLAG<br/>
*"  EXPORTING<br/>
*"     REFERENCE(HEAD_CAPTION) TYPE  CHAR40<br/>
*"     REFERENCE(HEAD_PROGRAM) TYPE  SYREPID<br/>
*"     REFERENCE(HEAD_DYNPRO) TYPE  SYDYNNR<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-get_head_active_tab_page.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;gv_active&nbsp;=&nbsp;active.<br/>
&nbsp;&nbsp;head_caption&nbsp;=&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;head_dynpro&nbsp;=&nbsp;'9001'.<br/>
&nbsp;&nbsp;head_program&nbsp;=&nbsp;sy-repid.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 自定义字段<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZSD001<br/>
*021   收票方&amp;1不存在或者未非分配给客户&amp;2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>