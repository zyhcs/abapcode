<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: LZSDG004F01</h2>
<h3> Description: Include LZSDG004F01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
***INCLUDE&nbsp;LZSDG004F01.<br/>
*----------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_meins_to_dun<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LS_TAB_MATNR<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;&nbsp;LV_MEINS<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--&nbsp;LS_TAB_KWMENG<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_meins_to_dun  USING   p_kpein<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_meins<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;p_kwmeng<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_kbetr.<br/>
<br/>
&nbsp;&nbsp;DATA:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_in_me&nbsp;&nbsp;LIKE&nbsp;&nbsp;mara-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_out_me&nbsp;LIKE&nbsp;&nbsp;mara-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;i_menge&nbsp;&nbsp;LIKE&nbsp;&nbsp;ekpo-menge,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_menge&nbsp;&nbsp;LIKE&nbsp;&nbsp;ekpo-menge.<br/>
<br/>
&nbsp;&nbsp;i_in_me&nbsp;=&nbsp;p_meins.<br/>
&nbsp;&nbsp;i_out_me&nbsp;=&nbsp;'TO'.<br/>
&nbsp;&nbsp;i_menge&nbsp;=&nbsp;p_kwmeng.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'MD_CONVERT_MATERIAL_UNIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_matnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_matnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_in_me&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_in_me<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_out_me&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_out_me<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_menge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;e_menge<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_in_application&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;e_menge&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_kpein&nbsp;=&nbsp;'1000'&nbsp;AND&nbsp;i_in_me&nbsp;=&nbsp;'KG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_kbetr&nbsp;=&nbsp;p_kbetr&nbsp;*&nbsp;(&nbsp;p_kwmeng&nbsp;/&nbsp;e_menge&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_kwmeng&nbsp;=&nbsp;e_menge.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>