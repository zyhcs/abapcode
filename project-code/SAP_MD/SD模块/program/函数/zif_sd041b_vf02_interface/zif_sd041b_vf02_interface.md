<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD041B_VF02_INTERFACE</h2>
<h3> Description: OA开票申请状态回传凭证生成</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZIF_SD041B_VF02_INTERFACE.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(CTU) LIKE  APQI-PUTACTIVE DEFAULT 'X'<br/>
*"     VALUE(MODE) LIKE  APQI-PUTACTIVE DEFAULT 'N'<br/>
*"     VALUE(UPDATE) LIKE  APQI-PUTACTIVE DEFAULT 'L'<br/>
*"     VALUE(GROUP) LIKE  APQI-GROUPID OPTIONAL<br/>
*"     VALUE(USER) LIKE  APQI-USERID OPTIONAL<br/>
*"     VALUE(KEEP) LIKE  APQI-QERASE OPTIONAL<br/>
*"     VALUE(HOLDDATE) LIKE  APQI-STARTDATE OPTIONAL<br/>
*"     VALUE(NODATA) LIKE  APQI-PUTACTIVE DEFAULT '/'<br/>
*"     VALUE(VBELN_001) LIKE  BDCDATA-FVAL DEFAULT '0000000000'<br/>
*"  EXPORTING<br/>
*"     VALUE(SUBRC) LIKE  SYST-SUBRC<br/>
*"  TABLES<br/>
*"      MESSTAB STRUCTURE  BDCMSGCOLL OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_sd041b_vf02_interface.html">Global data declarations</a></div><br/>
</div>
<div class="code">
subrc = 0.<br/>
<br/>
perform bdc_nodata      using NODATA.<br/>
<br/>
perform open_group      using GROUP USER KEEP HOLDDATE CTU.<br/>
<br/>
perform bdc_dynpro      using 'SAPMV60A' '0101'.<br/>
perform bdc_field       using 'BDC_CURSOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VBRK-VBELN'.<br/>
perform bdc_field       using 'BDC_OKCODE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'=CONTI'.<br/>
perform bdc_field       using 'VBRK-VBELN'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VBELN_001.<br/>
perform bdc_dynpro      using 'SAPMV60A' '0104'.<br/>
perform bdc_field       using 'BDC_CURSOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VBRK-FKART'.<br/>
perform bdc_field       using 'BDC_OKCODE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'=FKFR'.<br/>
perform bdc_dynpro      using 'SAPMV60A' '0104'.<br/>
perform bdc_field       using 'BDC_CURSOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'VBRK-FKART'.<br/>
perform bdc_field       using 'BDC_OKCODE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'=BACK'.<br/>
perform bdc_transaction tables messtab<br/>
using                         'VF02'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CTU<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPDATE.<br/>
if sy-subrc &lt;&gt; 0.<br/>
&nbsp;&nbsp;subrc&nbsp;=&nbsp;sy-subrc.<br/>
&nbsp;&nbsp;exit.<br/>
endif.<br/>
<br/>
perform close_group using     CTU.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
INCLUDE BDCRECXY .<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>