<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function SET_VBRK_CUST_DATA</h2>
<h3> Description: 设置自定义字段</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION SET_VBRK_CUST_DATA .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(VBRK) TYPE  VBRK<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-set_vbrk_cust_data.html">Global data declarations</a></div><br/>
</div>
<div class="code">
CHECK gv_active = 'X'.<br/>
gs_vbrk = vbrk.<br/>
MOVE-CORRESPONDING gs_vbrk to gs_cvbrk.<br/>
<br/>
ENDFUNCTION.<br/>
<br/>
</div>
<div class="codeComment">
*Text elements<br/>
*----------------------------------------------------------<br/>
* 001 自定义字段<br/>
<br/>
<br/>
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
* Message class: ZSD001<br/>
*021   收票方&amp;1不存在或者未非分配给客户&amp;2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>