<table class="outerTable">
<tr>
<td><h2>Table: ZSDS_001</h2>
<h3>Description: 销售发货通知单打印参考结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ATWRT_D</td>
<td>10</td>
<td>&nbsp;</td>
<td>ATWRT</td>
<td>ATWRT</td>
<td>CHAR</td>
<td>70</td>
<td>X</td>
<td>特性值</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ATWRT_J</td>
<td>12</td>
<td>&nbsp;</td>
<td>BEZEI40</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ATWRT_L</td>
<td>11</td>
<td>&nbsp;</td>
<td>ATWRT</td>
<td>ATWRT</td>
<td>CHAR</td>
<td>70</td>
<td>X</td>
<td>特性值</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>BEZEI</td>
<td>5</td>
<td>&nbsp;</td>
<td>VERSARTBEZ</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>装运类型描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>BSTKD</td>
<td>29</td>
<td>&nbsp;</td>
<td>BSTKD</td>
<td>BSTKD</td>
<td>CHAR</td>
<td>35</td>
<td>X</td>
<td>客户参考</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BUTXT</td>
<td>21</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>LFIMG</td>
<td>14</td>
<td>&nbsp;</td>
<td>LFIMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际已交货量（按销售单位）</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>LGOBE</td>
<td>15</td>
<td>&nbsp;</td>
<td>LGOBE</td>
<td>TEXT16</td>
<td>CHAR</td>
<td>16</td>
<td>X</td>
<td>仓储地点的描述</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>MAKTX</td>
<td>7</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>10</td>
<td>NAME1</td>
<td>3</td>
<td>&nbsp;</td>
<td>CHAR100</td>
<td>CHAR100</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>字符100</td>
</tr>
<tr class="cell">
<td>11</td>
<td>NAME3</td>
<td>6</td>
<td>&nbsp;</td>
<td>AD_NAME2_P</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>人员出生时的名字</td>
</tr>
<tr class="cell">
<td>12</td>
<td>VBELN</td>
<td>1</td>
<td>&nbsp;</td>
<td>VBELN_VL</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>交货</td>
</tr>
<tr class="cell">
<td>13</td>
<td>WADAT</td>
<td>2</td>
<td>&nbsp;</td>
<td>WADAT_IST</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>实际货物移动日期</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZBH</td>
<td>22</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>质控编号</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZBZ</td>
<td>24</td>
<td>&nbsp;</td>
<td>TXT60</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>文本 (60)</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZCPHM</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>车牌号</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZCYWL</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>承运物流</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZDATE</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>打印时间</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZDYCS</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>INT4</td>
<td>10</td>
<td>&nbsp;</td>
<td>打印次数</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZDZGD</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>到港站描述</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZJHSLD</td>
<td>13</td>
<td>&nbsp;</td>
<td>LFIMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际已交货量（按销售单位）</td>
</tr>
<tr class="cell">
<td>22</td>
<td>ZJHSLJ</td>
<td>18</td>
<td>&nbsp;</td>
<td>LFIMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际已交货量（按销售单位）</td>
</tr>
<tr class="cell">
<td>23</td>
<td>ZJHSLJ2</td>
<td>23</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量(件)</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ZJZXH1</td>
<td>25</td>
<td>&nbsp;</td>
<td>ZESDJZXH1</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>集装箱号1</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ZJZXH2</td>
<td>26</td>
<td>&nbsp;</td>
<td>ZESDJZXH2</td>
<td>CHAR20</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>集装箱号2</td>
</tr>
<tr class="cell">
<td>26</td>
<td>ZJZXHZL1</td>
<td>27</td>
<td>&nbsp;</td>
<td>ZESDJZXHZL1</td>
<td>ZDSDJZXHZL1</td>
<td>QUAN</td>
<td>5</td>
<td>&nbsp;</td>
<td>集装箱号1重量</td>
</tr>
<tr class="cell">
<td>27</td>
<td>ZJZXHZL2</td>
<td>28</td>
<td>&nbsp;</td>
<td>ZESDJZXHZL2</td>
<td>ZDSDJZXHZL2</td>
<td>QUAN</td>
<td>5</td>
<td>&nbsp;</td>
<td>集装箱号2重量</td>
</tr>
<tr class="cell">
<td>28</td>
<td>ZLXR</td>
<td>19</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>联系人</td>
</tr>
<tr class="cell">
<td>29</td>
<td>ZLXRT</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>15</td>
<td>&nbsp;</td>
<td>联系电话</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>