<table class="outerTable">
<tr>
<td><h2>Table: ZSSD026B</h2>
<h3>Description: 调用销售订单超信接口传入数据结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ARKTX</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BZIRK</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>销售区域</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BZIRK_TXT</td>
<td>16</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>70</td>
<td>&nbsp;</td>
<td>销售区域描述</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>CREDIT_LIMIT</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>信贷风险总额</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>KUNNR</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>KWERT</td>
<td>8</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>订单总金额</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>KWMENG</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>订单数量</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>MATNR</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编码</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>NAME_ORG1</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>35</td>
<td>&nbsp;</td>
<td>客户名称</td>
</tr>
<tr class="cell">
<td>10</td>
<td>POSNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>行号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>VBELN</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>销售凭证编号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>VKORG</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td>13</td>
<td>WAERK</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>60</td>
<td>&nbsp;</td>
<td>货币</td>
</tr>
<tr class="cell">
<td>14</td>
<td>WORKFLOWID</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>10</td>
<td>&nbsp;</td>
<td>流程ID</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZKUNNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>销售业务员</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZKUNNR_TXT</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>销售业务员描述</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZKWERT</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CURR</td>
<td>13</td>
<td>&nbsp;</td>
<td>超信金额</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>