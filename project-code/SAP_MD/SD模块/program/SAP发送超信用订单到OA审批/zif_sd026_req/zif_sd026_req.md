<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD026_REQ</h2>
<h3> Description: 发送超信用数据到OA审批</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd026_req .<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(IT_TAB) TYPE  ZTSD026B<br/>
*"  EXPORTING<br/>
*"     REFERENCE(OS_RETURN) TYPE  BAPIRET2<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-zif_sd026_req.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;li_proxy&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zpsdco_si_erp_sd026_exceedcred.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_uuid&nbsp;TYPE&nbsp;sysuuid_c32.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_tab&nbsp;TYPE&nbsp;zssd026b.<br/>
<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;&nbsp;&nbsp;TYPE&nbsp;zpsdmt_erp_sd026_exceedcredit1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_input&nbsp;TYPE&nbsp;zpsdmt_erp_sd026_exceedcreditr.<br/>
&nbsp;&nbsp;DATA:ls_header&nbsp;TYPE&nbsp;zpsddt_erp_sd026_exceedcredit7.<br/>
&nbsp;&nbsp;DATA:ls_item&nbsp;&nbsp;TYPE&nbsp;zpsddt_erp_sd026_exceedcredit5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_items&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;zpsddt_erp_sd026_exceedcredit5.<br/>
</div>
<div class="codeComment">
*        lt_item type ZPSDDT_ERP_SD026_EXCEEDCREDIT5<br/>
</div>
<div class="code">
&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_message&nbsp;TYPE&nbsp;bapi_msg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;TYPE&nbsp;bapi_mtype.<br/>
&nbsp;&nbsp;DATA:lv_key(50)&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_bpext&nbsp;TYPE&nbsp;but000-bpext.&nbsp;&nbsp;&nbsp;"OA系统人员编号，用于SEND_USER,<br/>
&nbsp;&nbsp;DATA:lv_kbetr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kbetr.<br/>
&nbsp;&nbsp;DATA:lv_meins&nbsp;&nbsp;TYPE&nbsp;meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_atwrt&nbsp;&nbsp;TYPE&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zyyyhg&nbsp;TYPE&nbsp;&nbsp;ausp-atinn&nbsp;,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"ZYYYHG,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kpein&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kpein.<br/>
&nbsp;&nbsp;lv_uuid&nbsp;=&nbsp;cl_system_uuid=&gt;create_uuid_c32_static(&nbsp;).<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_SD026_REQ'&nbsp;).<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
<br/>
*检查接口是否启用<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"构建报文，并调PO类发送数据<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;it_tab&nbsp;INTO&nbsp;ls_tab&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-vbeln&nbsp;=&nbsp;|{&nbsp;ls_tab-vbeln&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"销售组织名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vtext&nbsp;INTO&nbsp;ls_header-vkorg&nbsp;FROM&nbsp;tvkot&nbsp;WHERE&nbsp;vkorg&nbsp;=&nbsp;ls_tab-vkorg.<br/>
</div>
<div class="codeComment">
*      ls_header-vkorg = ls_tab-vkorg.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-kunnr&nbsp;=&nbsp;|{&nbsp;ls_tab-kunnr&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-name_org1&nbsp;=&nbsp;ls_tab-name_org1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;t~bztxt&nbsp;INTO&nbsp;ls_header-bzirk&nbsp;FROM&nbsp;t171t&nbsp;AS&nbsp;t&nbsp;INNER&nbsp;JOIN&nbsp;vbkd&nbsp;AS&nbsp;vd&nbsp;ON&nbsp;t~bzirk&nbsp;=&nbsp;vd~bzirk<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;vbak&nbsp;AS&nbsp;vk&nbsp;ON&nbsp;vd~vbeln&nbsp;=&nbsp;vk~vbeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;&nbsp;vk~vbeln&nbsp;=&nbsp;ls_tab-vbeln&nbsp;AND&nbsp;t~spras&nbsp;=&nbsp;'1'.<br/>
</div>
<div class="codeComment">
*      ls_header-bzirk = ls_tab-bzirk_txt.   "OA不关注编码，传描述<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-zkunnr&nbsp;=&nbsp;ls_tab-zkunnr_txt.&nbsp;&nbsp;&nbsp;"OA不关注编码，传描述<br/>
</div>
<div class="codeComment">
*      ls_header-kwert = ls_tab-kwert.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-credit_limit&nbsp;=&nbsp;ls_tab-credit_limit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-workflowid&nbsp;=&nbsp;&nbsp;zcl_eas_assist=&gt;get_workflowid(&nbsp;'SD026'&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-zkwert&nbsp;=&nbsp;ls_tab-zkwert.<br/>
</div>
<div class="codeComment">
* 获取OA工号<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bu~bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lv_bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000&nbsp;AS&nbsp;bu&nbsp;INNER&nbsp;JOIN&nbsp;vbpa&nbsp;AS&nbsp;vp&nbsp;ON&nbsp;bu~partner&nbsp;=&nbsp;vp~kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vp~vbeln&nbsp;=&nbsp;ls_tab-vbeln&nbsp;AND&nbsp;vp~parvw&nbsp;=&nbsp;'ZA'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_to&nbsp;TYPE&nbsp;zssd026b-kwmeng.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_tab&nbsp;INTO&nbsp;ls_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_atwrt,lv_meins,lv_kbetr,lv_kpein.<br/>
</div>
<div class="codeComment">
* 获取单位<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vrkme&nbsp;INTO&nbsp;lv_meins&nbsp;FROM&nbsp;vbap&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;ls_tab-vbeln&nbsp;AND&nbsp;posnr&nbsp;=&nbsp;ls_tab-posnr.<br/>
</div>
<div class="codeComment">
* 获取单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;pr~kbetr,pr~kpein&nbsp;INTO&nbsp;(&nbsp;@lv_kbetr,@lv_kpein&nbsp;)&nbsp;FROM&nbsp;prcd_elements&nbsp;AS&nbsp;pr&nbsp;INNER&nbsp;JOIN&nbsp;vbak&nbsp;AS&nbsp;v&nbsp;ON&nbsp;pr~knumv&nbsp;=&nbsp;v~knumv<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;v~vbeln&nbsp;=&nbsp;@ls_tab-vbeln&nbsp;AND&nbsp;pr~kschl&nbsp;=&nbsp;'ZR01'&nbsp;AND&nbsp;pr~kposn&nbsp;=&nbsp;@ls_tab-posnr.<br/>
</div>
<div class="codeComment">
* 获取物料类型<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ATINN_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;'ZYYYHG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;atwrt&nbsp;INTO&nbsp;lv_atwrt&nbsp;FROM&nbsp;ausp&nbsp;WHERE&nbsp;objek&nbsp;=&nbsp;ls_tab-matnr&nbsp;AND&nbsp;atinn&nbsp;=&nbsp;lv_zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;=&nbsp;'日用盐'&nbsp;.<br/>
</div>
<div class="codeComment">
* 单位转换<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_meins_to_dun&nbsp;USING&nbsp;lv_kpein&nbsp;ls_tab-matnr&nbsp;lv_meins&nbsp;CHANGING&nbsp;ls_tab-kwmeng&nbsp;lv_kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-vrkme&nbsp;=&nbsp;'吨'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;mseh6&nbsp;INTO&nbsp;ls_item-vrkme&nbsp;FROM&nbsp;t006a&nbsp;WHERE&nbsp;msehi&nbsp;=&nbsp;lv_meins&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
* 单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. znumber_point_format="" znumber_point_format.html"="">'ZNUMBER_POINT_FORMAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_data&nbsp;&nbsp;&nbsp;=&nbsp;lv_kbetr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_num&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;ls_item-zxsje.<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
* 抬头总金额<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_header-kwert&nbsp;=&nbsp;ls_header-kwert&nbsp;+&nbsp;lv_kbetr&nbsp;*&nbsp;ls_tab-kwmeng.<br/>
</div>
<div class="codeComment">
* 行项目总金额<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-zhsje&nbsp;=&nbsp;lv_kbetr&nbsp;*&nbsp;ls_tab-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. znumber_point_format="" znumber_point_format.html"="">'ZNUMBER_POINT_FORMAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_data&nbsp;&nbsp;&nbsp;=&nbsp;ls_item-zhsje<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_num&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;ls_item-zhsje.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-posnr&nbsp;=&nbsp;ls_tab-posnr.<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*        ls_item-vrkme = lv_meins.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-matnr&nbsp;=&nbsp;ls_tab-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-kwmeng&nbsp;=&nbsp;ls_tab-kwmeng.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-arktx&nbsp;=&nbsp;ls_tab-arktx.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-waerk&nbsp;=&nbsp;ls_tab-waerk.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_item&nbsp;TO&nbsp;lt_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;ls_item,lv_to.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-zdata-header&nbsp;=&nbsp;ls_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-zdata-items&nbsp;=&nbsp;lt_items.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-bustyp&nbsp;=&nbsp;'SD026'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-messageid&nbsp;=&nbsp;ls_header-vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd026_exceedcreditreque-message_header-send_user&nbsp;=&nbsp;lv_bpext.&nbsp;&nbsp;&nbsp;"对方系统的OA人员编号<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;li_proxy&nbsp;.<br/>
</div>
<div class="codeComment">
*调用同步接口<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;li_proxy-&gt;si_erp_sd026_exceedcreditreque<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;ls_input.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_ai_system_fault&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;"处理代理类结果<br/>
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_input&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;ls_input-mt_erp_sd026_exceedcreditreque-message_header_rs-message_status.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;ls_input-mt_erp_sd026_exceedcreditreque-message_header_rs-message_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;lv_msgtype&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;ls_input-mt_erp_sd026_exceedcreditreque-zdata-header-header_status.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;ls_input-mt_erp_sd026_exceedcreditreque-zdata-header-header_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;IS&nbsp;INITIAL&nbsp;OR&nbsp;lv_msgtype&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;=&nbsp;'S'&nbsp;AND&nbsp;lv_message&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;=&nbsp;'数据推送成功'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msgtype&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;&nbsp;&nbsp;=&nbsp;'未收到OA系统返回的数据'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;os_return-type&nbsp;=&nbsp;lv_msgtype.<br/>
&nbsp;&nbsp;os_return-message&nbsp;=&nbsp;lv_message.<br/>
<br/>
</div>
<div class="codeComment">
*当接口出现错误时,获取函数传入传出参数,以便重推<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;lv_msgtype&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;paras&gt;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;get_func_paras(&nbsp;IMPORTING&nbsp;paras&nbsp;=&nbsp;DATA(lt_paras)&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_paras&nbsp;&nbsp;ASSIGNING&nbsp;&nbsp;FIELD-SYMBOL(&lt;ls_paras&gt;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;(&lt;ls_paras&gt;-parameter)&nbsp;TO&nbsp;&lt;paras&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_paras&gt;-value&nbsp;=&nbsp;REF&nbsp;#(&nbsp;&lt;paras&gt;&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_func_msg(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;paras&nbsp;&nbsp;=&nbsp;lt_paras<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;repush&nbsp;=&nbsp;'X'&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*--------------------------------------------------------------------*<br/>
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msgtype<br/>
&nbsp;&nbsp;msg&nbsp;=&nbsp;lv_message<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;ls_tab-vbeln&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>