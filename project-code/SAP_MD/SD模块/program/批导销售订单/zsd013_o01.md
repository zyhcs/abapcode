<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD013_O01</h2>
<h3> Description: Include ZSD013_O01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD013_O01<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_ALV_UPLOAD<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_alv_upload .<br/>
<br/>
&nbsp;&nbsp;DATA:lv_grid_title&nbsp;&nbsp;TYPE&nbsp;lvc_title.<br/>
</div>
<div class="codeComment">
*&nbsp;fieldcate&nbsp;设定<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;fieldcate_set&nbsp;CHANGING&nbsp;gt_fieldcat_lvc.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;页面布局设定<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR:gs_layout.<br/>
&nbsp;&nbsp;gs_layout-cwidth_opt&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"自动优化宽度<br/>
&nbsp;&nbsp;gs_layout-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-no_rowmark&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;gs_layout-smalltitle&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;lv_grid_title&nbsp;=&nbsp;|文件导入总条目数:{&nbsp;lines(&nbsp;gt_excel&nbsp;)&nbsp;}|.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;调用显示函数显示数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_fieldcat_lvc[]<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_layout<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'FRM_STATUS_SET'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'FRM_USER_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_event<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_grid_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_grid_title<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_excel<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
<br/>
ENDFORM.                    " FRM_ALV_UPLOAD<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FIELDCATE_SET<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_GT_FIELDCAT_LVC&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM fieldcate_set  CHANGING pt_fieldcat_lvc LIKE gt_fieldcat_lvc.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_fieldcat_lvc&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;pt_fieldcat_lvc.<br/>
&nbsp;&nbsp;REFRESH&nbsp;pt_fieldcat_lvc.<br/>
&nbsp;&nbsp;DEFINE&nbsp;add_fieldcat.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-fieldname&nbsp;=&nbsp;&amp;1.&nbsp;&nbsp;"字段名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-icon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;2.&nbsp;&nbsp;"图标<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-ref_table&nbsp;=&nbsp;&amp;3.&nbsp;&nbsp;"表名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-ref_field&nbsp;=&nbsp;&amp;4.&nbsp;&nbsp;"字段名<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-reptext&nbsp;&nbsp;&nbsp;=&nbsp;&amp;5.&nbsp;&nbsp;"文本字段<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-scrtext_l&nbsp;=&nbsp;&amp;5.&nbsp;&nbsp;"长文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-scrtext_m&nbsp;=&nbsp;&amp;5.&nbsp;&nbsp;"中文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-scrtext_s&nbsp;=&nbsp;&amp;5.&nbsp;&nbsp;"短文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&amp;6.&nbsp;&nbsp;"可编辑<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_fieldcat_lvc-checkbox&nbsp;&nbsp;=&nbsp;&amp;7.&nbsp;&nbsp;"选择框<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_fieldcat_lvc&nbsp;TO&nbsp;pt_fieldcat_lvc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_fieldcat_lvc.<br/>
&nbsp;&nbsp;END-OF-DEFINITION.<br/>
<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'BOX_SEL'&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c00&nbsp;&nbsp;&nbsp;'X'&nbsp;&nbsp;&nbsp;&nbsp;'X'.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ICON'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c01&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ZMESSAGE'&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c03&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VBELN'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c04&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'POSNR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c05&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'SCBS'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c37&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'AUART'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c06&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VKORG'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c07&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VTWEG'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c08&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'SPART'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c09&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VKBUR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c10&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KUNNR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c11&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KUNNR_TXT'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c12&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KUNWE'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c13&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KUNWE_TXT'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c14&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'SDABW'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c47&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VSART'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c15&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ZLSCH'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c16&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'XSYWY'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c17&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ADDRESS'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c18&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'SHLXR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c19&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'TELE'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c20&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'BZ'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c21&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'QYGS'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c22&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'BSTKD'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c23&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ABLAD'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c24&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VSBED'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c41&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'IHREZ'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c42&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ZTCH'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c44&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ZTSJ'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c45&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ZTDH'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c46&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KVGR5'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c25&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'MATNR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c26&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'MAKTX'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c27&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KWMENG'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c28&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'DJDW'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c38&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'VRKME'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c29&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'WERKS'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c30&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'ROUTE'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c43&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'LGORT'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c31&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'KSCHL'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c32&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'NETPR'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c33&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'MVGR2'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c34&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'JB'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c35&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'WAERK'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c36&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'YWRQ'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c39&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'CKRQ'&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;TEXT-c40&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''.<br/>
&nbsp;&nbsp;add_fieldcat&nbsp;'BNAME'&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''&nbsp;&nbsp;''&nbsp;&nbsp;&nbsp;'金蝶单据号'&nbsp;''&nbsp;&nbsp;''&nbsp;.<br/>
ENDFORM.                    " FIELDCATE_SET<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;frm_status_set<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;PT_EXTAB&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_status_set USING pt_extab TYPE slis_t_extab.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_extab&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;pt_extab.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'STANDARD_FULLSCREEN'.<br/>
ENDFORM.                 "FRM_STATUS_SET<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;frm_user_command<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;PV_UCOMM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;PS_SELFIELD&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_user_command USING pv_ucomm TYPE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ps_selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
</div>
<div class="codeComment">
*&nbsp;刷新ALV的显示<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_GLOBALS_FROM_SLVC_FULLSCR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_grid&nbsp;=&nbsp;gcl_grid.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gcl_grid-&gt;check_changed_data.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;pv_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'IMPT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;import_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SELECT_ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_select_all.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'DESELECT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_deselect_all.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
<br/>
&nbsp;&nbsp;ps_selfield-refresh&nbsp;=&nbsp;'X'.<br/>
<br/>
ENDFORM.                 "FRM_USER_COMMAND<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>