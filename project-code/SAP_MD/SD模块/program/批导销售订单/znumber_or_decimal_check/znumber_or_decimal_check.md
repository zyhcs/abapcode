<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZNUMBER_OR_DECIMAL_CHECK</h2>
<h3> Description: 数字或小数检查</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZNUMBER_OR_DECIMAL_CHECK.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     REFERENCE(I_STR) TYPE  STRING<br/>
*"  EXPORTING<br/>
*"     REFERENCE(E_FLAG) TYPE  CHAR01<br/>
*"----------------------------------------------------------------------<br/>
<br/>
<div class="codeComment">*       <a href="global-znumber_or_decimal_check.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;I_STR2&nbsp;TYPE&nbsp;STRING.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;E_FLAG.<br/>
<br/>
&nbsp;&nbsp;I_STR2&nbsp;=&nbsp;I_STR.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;I_STR2&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;E_FLAG&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;SHIFT&nbsp;I_STR2&nbsp;RIGHT&nbsp;DELETING&nbsp;TRAILING&nbsp;'-'.<br/>
&nbsp;&nbsp;SHIFT&nbsp;I_STR2&nbsp;LEFT&nbsp;DELETING&nbsp;LEADING&nbsp;'-'.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;I_STR2&nbsp;CS&nbsp;'.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SHIFT&nbsp;I_STR2&nbsp;RIGHT&nbsp;DELETING&nbsp;TRAILING&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;REPLACE&nbsp;FIRST&nbsp;OCCURRENCE&nbsp;OF&nbsp;'.'&nbsp;IN&nbsp;I_STR2&nbsp;WITH&nbsp;''.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;CONDENSE&nbsp;I_STR2&nbsp;NO-GAPS.<br/>
<br/>
&nbsp;&nbsp;DATA:MATCHER&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;CL_ABAP_MATCHER.<br/>
<br/>
&nbsp;&nbsp;MATCHER&nbsp;=&nbsp;CL_ABAP_MATCHER=&gt;CREATE(&nbsp;PATTERN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'\d+'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;I_STR2&nbsp;).&nbsp;&nbsp;E_FLAG&nbsp;=&nbsp;MATCHER-&gt;MATCH(&nbsp;).<br/>
<br/>
<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>