<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD013_SCREEN</h2>
<h3> Description: Include ZSD013_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD013_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK B1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;P_FILE&nbsp;&nbsp;LIKE&nbsp;RLGRAP-FILENAME&nbsp;MEMORY&nbsp;ID&nbsp;G1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"上传路径<br/>
SELECTION-SCREEN END OF BLOCK B1.<br/>
<br/>
SELECTION-SCREEN FUNCTION KEY 1.  "下载模板必须按钮<br/>
<br/>
INITIALIZATION.<br/>
&nbsp;&nbsp;GV_BUTTON1-ICON_ID&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ICON_EXPORT.<br/>
&nbsp;&nbsp;GV_BUTTON1-ICON_TEXT&nbsp;&nbsp;=&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;GV_BUTTON1-QUICKINFO&nbsp;&nbsp;=&nbsp;TEXT-001.<br/>
&nbsp;&nbsp;SSCRFIELDS-FUNCTXT_01&nbsp;=&nbsp;GV_BUTTON1.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;screen_display_control.&nbsp;&nbsp;&nbsp;"修改屏幕属性<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
&nbsp;&nbsp;PERFORM&nbsp;FUNCTION_BUTTON."按钮功能<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR P_FILE.<br/>
&nbsp;&nbsp;PERFORM&nbsp;GET_FILENAME.&nbsp;&nbsp;"搜索帮助<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FUNCTION_BUTTON<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM FUNCTION_BUTTON .<br/>
&nbsp;&nbsp;DATA:&nbsp;LV_OBJID&nbsp;TYPE&nbsp;WWWDATA-OBJID,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_NAME&nbsp;&nbsp;TYPE&nbsp;STRING.<br/>
&nbsp;&nbsp;CASE&nbsp;SSCRFIELDS-UCOMM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'FC01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_OBJID&nbsp;=&nbsp;'ZSD013'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;TEXT-F01&nbsp;SY-DATUM&nbsp;SY-UZEIT&nbsp;INTO&nbsp;LV_NAME.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;="zfm_download_template zfm_download_template.html"="">'ZFM_DOWNLOAD_TEMPLATE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IM_OBJID&nbsp;=&nbsp;LV_OBJID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IM_NAME&nbsp;&nbsp;=&nbsp;LV_NAME.<br/>
</a&nbsp;href&nbsp;="zfm_download_template></div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'FC02'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ZM4FM_DISPLAY_DBTABLE_DATA'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_tablename&nbsp;=&nbsp;'ZM4TPPI008_LOG'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
ENDFORM.                    " FUNCTION_BUTTON<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;GET_FILENAME<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM GET_FILENAME .<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'KD_GET_FILENAME_ON_F4'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STATIC&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FILE_NAME&nbsp;=&nbsp;P_FILE.<br/>
&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.                    " GET_FILENAME<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>