<table class="outerTable">
<tr>
<td><h2>Table: ZSVBRK</h2>
<h3>Description: vbrk附加字段</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ZZSPF</td>
<td>1</td>
<td>&nbsp;</td>
<td>ZESPF</td>
<td>BU_PARTNER</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>收票方</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>