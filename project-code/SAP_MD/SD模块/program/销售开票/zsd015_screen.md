<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD015_SCREEN</h2>
<h3> Description: Include ZSD015_SCREEN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD015_SCREEN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
SELECTION-SCREEN BEGIN OF BLOCK BLK1 WITH FRAME TITLE TEXT-T01.<br/>
&nbsp;&nbsp;PARAMETERS:P_VKORG&nbsp;LIKE&nbsp;LIKP-VKORG&nbsp;OBLIGATORY.&nbsp;&nbsp;"销售组织<br/>
&nbsp;&nbsp;SELECT-OPTIONS:&nbsp;S_KUNNR&nbsp;FOR&nbsp;LIKP-KUNNR,&nbsp;"客户编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_VBELN&nbsp;FOR&nbsp;VBAK-VBELN,&nbsp;&nbsp;"销售订单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ZJHDH&nbsp;FOR&nbsp;LIKP-VBELN,&nbsp;&nbsp;&nbsp;"交货单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_MATNR&nbsp;FOR&nbsp;LIPS-MATNR,&nbsp;"物料编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_WADAT&nbsp;FOR&nbsp;LIKP-WADAT_IST,"发运日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_VKBUR&nbsp;FOR&nbsp;LIKP-VKBUR,&nbsp;&nbsp;&nbsp;"销售办公室<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_VSART&nbsp;FOR&nbsp;LIKP-VSART,&nbsp;&nbsp;&nbsp;&nbsp;"交货方式<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_LGORT&nbsp;FOR&nbsp;LIPS-LGORT,&nbsp;&nbsp;&nbsp;&nbsp;"存储位置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ZCPHM&nbsp;FOR&nbsp;LIKP-ZCPHM,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"车号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ZJZXH1&nbsp;FOR&nbsp;LIKP-ZJZXH1,&nbsp;&nbsp;"集装箱号1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S_ywy&nbsp;&nbsp;FOR&nbsp;KUAGV-KUNNR.&nbsp;&nbsp;&nbsp;"销售业务员<br/>
SELECTION-SCREEN END OF BLOCK BLK1.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>