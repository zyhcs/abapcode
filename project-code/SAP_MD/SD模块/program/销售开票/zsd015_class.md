<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD015_CLASS</h2>
<h3> Description: Include ZSD015_O01</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD015_O01<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZQMIF001_CLASS<br/>
*&amp;---------------------------------------------------------------------*<br/>
*------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;CLASS&nbsp;lcl_event_handler&nbsp;DEFINITION<br/>
*------------------------------------------------------------------*<br/>
</div>
<div class="code">
CLASS lcl_event_handler DEFINITION.<br/>
&nbsp;&nbsp;PUBLIC&nbsp;SECTION.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLASS-METHODS:<br/>
</div>
<div class="codeComment">
*&nbsp;-------------------------------------------------------------<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_hotspot_click&nbsp;FOR&nbsp;EVENT&nbsp;hotspot_click&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_row_id&nbsp;e_column_id&nbsp;es_row_no,<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_onf4&nbsp;FOR&nbsp;EVENT&nbsp;onf4&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_fieldname&nbsp;e_fieldvalue&nbsp;es_row_no&nbsp;er_event_data&nbsp;et_bad_cells&nbsp;e_display,<br/>
</div>
<div class="codeComment">
*&nbsp;用户事件监听<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HANDLE_DATA_CHANGED&nbsp;FOR&nbsp;EVENT&nbsp;DATA_CHANGED&nbsp;OF&nbsp;CL_GUI_ALV_GRID<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;ER_DATA_CHANGED<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_ONF4<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_ONF4_BEFORE<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_ONF4_AFTER<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E_UCOMM,<br/>
*--·数据更新后事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_data_changed_finished&nbsp;&nbsp;FOR&nbsp;EVENT&nbsp;data_changed_finished&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_modified<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;et_good_cells<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sender,<br/>
</div>
<div class="codeComment">
*&nbsp;工具条管理<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_toolbar&nbsp;FOR&nbsp;EVENT&nbsp;toolbar&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_object&nbsp;e_interactive,<br/>
</div>
<div class="codeComment">
*&nbsp;管理菜单<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_menu_button&nbsp;FOR&nbsp;EVENT&nbsp;menu_button&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_object&nbsp;e_ucomm,<br/>
</div>
<div class="codeComment">
*&nbsp;用户事件监听<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;handle_user_command&nbsp;FOR&nbsp;EVENT&nbsp;user_command&nbsp;OF&nbsp;cl_gui_alv_grid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING&nbsp;e_ucomm&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;-------------------------------------------------------------<br/>
</div>
<div class="code">
ENDCLASS. "lcl_event_handler DEFINITION<br/>
<br/>
</div>
<div class="codeComment">
*-----------------------------------------------------------------*<br/>
*&nbsp;CLASS&nbsp;lcl_event_handler&nbsp;IMPLEMENTATION<br/>
*-----------------------------------------------------------------*<br/>
</div>
<div class="code">
CLASS lcl_event_handler IMPLEMENTATION.<br/>
</div>
<div class="codeComment">
*&nbsp;-----------------------------------------------------------------<br/>
*&nbsp;热点点击控制<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_hotspot_click.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;lv_message&nbsp;TYPE&nbsp;char100&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA:&nbsp;lv_row_id&nbsp;TYPE&nbsp;char5,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_field&nbsp;&nbsp;TYPE&nbsp;lvc_fname.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_row_id&nbsp;=&nbsp;es_row_no-row_id&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_field&nbsp;=&nbsp;e_column_id-fieldname&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;lv_field.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'VBELN_VF'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;INTO&nbsp;gw_data&nbsp;INDEX&nbsp;lv_row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gw_data-vbeln_vf&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'VF'&nbsp;FIELD&nbsp;gw_data-vbeln_vf.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'VF03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZXSPZ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;INTO&nbsp;gw_data&nbsp;INDEX&nbsp;lv_row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;gw_data-vbtyp&nbsp;=&nbsp;'J'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'VL'&nbsp;FIELD&nbsp;gw_data-zxspz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'VL03N'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'AUN'&nbsp;FIELD&nbsp;gw_data-zxspz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'VA03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'KUNNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;INTO&nbsp;gw_data&nbsp;INDEX&nbsp;lv_row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'KUN'&nbsp;FIELD&nbsp;gw_data-kunnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'XD03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'MATNR'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;gt_data&nbsp;INTO&nbsp;gw_data&nbsp;INDEX&nbsp;lv_row_id.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SET&nbsp;PARAMETER&nbsp;ID&nbsp;'MAT'&nbsp;FIELD&nbsp;gw_data-matnr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;TRANSACTION&nbsp;'MM03'&nbsp;AND&nbsp;SKIP&nbsp;FIRST&nbsp;SCREEN.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"HANDLE_HOTSPOT_CLICK<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Handle&nbsp;F4<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_onf4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;process_event_onf4&nbsp;USING&nbsp;e_fieldname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_fieldvalue<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;es_row_no<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;er_event_data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;et_bad_cells<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_display.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;er_event_data-&gt;m_event_handled&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_refresh_display&nbsp;CHANGING&nbsp;gr_alvgrid.<br/>
&nbsp;&nbsp;ENDMETHOD.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;METHOD&nbsp;HANDLE_DATA_CHANGED.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;SY-SUBRC&nbsp;=&nbsp;0.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;ENDMETHOD.<br/>
<br/>
<br/>
*--·数据更新后事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_data_changed_finished.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;&nbsp;frm_data_changed_finished&nbsp;USING&nbsp;e_modified&nbsp;et_good_cells&nbsp;sender&nbsp;."G_REFRESH.<br/>
&nbsp;&nbsp;ENDMETHOD.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;工具条增加功能按钮<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_toolbar.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_toolbar.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-function&nbsp;&nbsp;=&nbsp;'SELECT_ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-icon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;icon_select_all.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-quickinfo&nbsp;=&nbsp;'全选'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'全选'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-butn_type&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-disabled&nbsp;&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_toolbar&nbsp;TO&nbsp;e_object-&gt;mt_toolbar.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_toolbar.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-function&nbsp;&nbsp;=&nbsp;'DESELECT_ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-icon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;icon_deselect_all.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-quickinfo&nbsp;=&nbsp;'反选'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'反选'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-butn_type&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-disabled&nbsp;&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_toolbar&nbsp;TO&nbsp;e_object-&gt;mt_toolbar.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;gs_toolbar.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-function&nbsp;&nbsp;=&nbsp;'CREAT_VF'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-icon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&nbsp;icon_resubmission.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-quickinfo&nbsp;=&nbsp;'提交开票申请'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'提交开票申请'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-butn_type&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_toolbar-disabled&nbsp;&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_toolbar&nbsp;TO&nbsp;e_object-&gt;mt_toolbar.<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"HANDLE_TOOLBAR<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;管理菜单<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_menu_button.<br/>
<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"HANDLE_MENU_BUTTON<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;响应用户点击事件<br/>
</div>
<div class="code">
&nbsp;&nbsp;METHOD&nbsp;handle_user_command.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;gr_alvgrid-&gt;check_changed_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_cfw=&gt;flush.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;e_ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'CREAT_VF'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_creat_vf03.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'SELECT_ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_select_data&nbsp;USING&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'DESELECT_ALL'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_select_data&nbsp;USING&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_refresh_display&nbsp;USING&nbsp;gr_alvgrid.<br/>
&nbsp;&nbsp;ENDMETHOD.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"HANDLE_USER_COMMAND<br/>
ENDCLASS.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>