<table class="outerTable">
<tr>
<td><h2>Table: ZTSD007</h2>
<h3>Description: SAP客户主数据与EAS城市编码映射</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ORT01</td>
<td>2</td>
<td>X</td>
<td>ORT01</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>城市</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZKHFL</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZE_ZKHFL</td>
<td>ZD_ZKHFL</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>EAS客户分类</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZKHFLMS</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZE_ZKHFLMS</td>
<td>ZD_ZKHFLMS</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>EAS客户分类描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>