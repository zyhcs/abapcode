<table class="outerTable">
<tr>
<td><h2>Table: ZTFICO001</h2>
<h3>Description: 组织架构自建表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>BUKRS</td>
<td>2</td>
<td>X</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>BUTXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>BUTXT</td>
<td>TEXT25</td>
<td>CHAR</td>
<td>25</td>
<td>X</td>
<td>公司代码或公司的名称</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>EKORG</td>
<td>3</td>
<td>X</td>
<td>EKORG</td>
<td>EKORG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>采购组织</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>EKOTX</td>
<td>7</td>
<td>&nbsp;</td>
<td>EKOTX</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>采购组织描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZKBUKRS</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZEKBUKRS</td>
<td>ZDKBUKRS</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶公司代码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZKBUTXT</td>
<td>6</td>
<td>&nbsp;</td>
<td>ZEKBUTXT</td>
<td>ZDKBUTXT</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶公司描述</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZKEKORG</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZEKEKORG</td>
<td>ZDKEKORG</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>金蝶采购组织</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZKEKOTX</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZEKEKOTX</td>
<td>ZDKEKOTX</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>金蝶采购组描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>