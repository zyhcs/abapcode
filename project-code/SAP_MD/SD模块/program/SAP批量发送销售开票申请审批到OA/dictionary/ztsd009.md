<table class="outerTable">
<tr>
<td><h2>Table: ZTSD009</h2>
<h3>Description: 销售开票申请审批状态记录表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>VBELN</td>
<td>2</td>
<td>X</td>
<td>VBELN_VF</td>
<td>VBELN</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>开票凭证</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZPUSH</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZE_ZPUSH</td>
<td>ZD_ZPUSH</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>推送标识</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>