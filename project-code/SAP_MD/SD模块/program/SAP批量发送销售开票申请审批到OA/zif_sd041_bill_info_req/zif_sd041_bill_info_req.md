<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_SD041_BILL_INFO_REQ</h2>
<h3> Description: SAP发送开发信息给OA审批</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zif_sd041_bill_info_req.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(I_VBELN) TYPE  VBRK-VBELN OPTIONAL<br/>
*"     VALUE(I_VBRK) TYPE  VBRKVB OPTIONAL<br/>
*"  EXPORTING<br/>
*"     VALUE(E_MSG_TYPE) TYPE  BAPI_MTYPE<br/>
*"     VALUE(E_MSG) TYPE  BAPI_MSG<br/>
*"  TABLES<br/>
*"      I_VBRP STRUCTURE  VBRPVB OPTIONAL<br/>
*"----------------------------------------------------------------------<br/>
* 定义接口变量<br/>
<div class="codeComment">*       <a href="global-zif_sd041_bill_info_req.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:bill_info_req&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;zco_si_erp2oa_sd041_bill_info.<br/>
&nbsp;&nbsp;DATA:ls_out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_erp_sd041_bill_info_req,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_in&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zmt_erp_sd041_bill_info_res,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041_bill_info_req_me,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041_bill_info_req_zd,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header&nbsp;TYPE&nbsp;zdt_erp_sd041_bill_info_req_he,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_items&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041_bill_info_r_tab1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item&nbsp;&nbsp;&nbsp;TYPE&nbsp;zdt_erp_sd041_bill_info_req_it.<br/>
<br/>
&nbsp;&nbsp;DATA:lv_uuid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sysuuid_c32,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_key(50)&nbsp;&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;TYPE&nbsp;bapi_mtype,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapi_msg.<br/>
&nbsp;&nbsp;DATA:ls_vbrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbrpvb.<br/>
&nbsp;&nbsp;DATA:lv_bezei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;tvkbt-bezei,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vkbur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbak-vkbur,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbak-vbeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_wadat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-wadat_ist,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_lfimg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lips-lfimg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vsart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-vsart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fkimg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;vbrp-fkimg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kbetr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kbetr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_shuilv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kbetr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kwert&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kwert,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_yunza&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kwert,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hszje&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kwert,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_cjje1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kwert,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_cjje2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;prcd_elements-kwert,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_lfimg_diff&nbsp;TYPE&nbsp;tvpod-lfimg_diff,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_grund&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;tvpod-grund,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_meins&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lips-meins,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_zoacpz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;ztsd008-zoacpz,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_name1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kna1-name1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_xsywy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kna1-name1.<br/>
&nbsp;&nbsp;DATA:lv_bili&nbsp;&nbsp;TYPE&nbsp;lips-lfimg,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_atwrt&nbsp;TYPE&nbsp;ausp-atwrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_auart&nbsp;TYPE&nbsp;vbak-auart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_atinn&nbsp;TYPE&nbsp;ausp-atinn,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kumza&nbsp;TYPE&nbsp;prcd_elements-kumza,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kumne&nbsp;TYPE&nbsp;prcd_elements-kumne,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kpein&nbsp;TYPE&nbsp;prcd_elements-kpein.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_bpext&nbsp;TYPE&nbsp;but000-bpext.&nbsp;&nbsp;&nbsp;"OA系统人员编号，用于SEND_USER,<br/>
&nbsp;&nbsp;DATA:lv_flag1&nbsp;TYPE&nbsp;c,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_flag2&nbsp;TYPE&nbsp;c.<br/>
&nbsp;&nbsp;DATA:lv_str&nbsp;&nbsp;TYPE&nbsp;string&nbsp;VALUE&nbsp;'0.000',<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_cjsl&nbsp;TYPE&nbsp;string.<br/>
</div>
<div class="codeComment">
* 获取UUID<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_system_uuid=&gt;if_system_uuid_static~create_uuid_c32<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uuid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_uuid_error&nbsp;.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
</div>
<div class="codeComment">
* 初始化日志<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA(lo_log)&nbsp;=&nbsp;NEW&nbsp;zcl_if_log(&nbsp;id&nbsp;=&nbsp;lv_uuid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;interface&nbsp;=&nbsp;'ZIF_SD041_BILL_INFO_REQ'&nbsp;).<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lo_log-&gt;check_active(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'接口未启用.'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;lv_msg_type&nbsp;&lt;&gt;&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_auart.<br/>
</div>
<div class="codeComment">
* 发送信息抬头<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;i_vbrp&nbsp;INTO&nbsp;ls_vbrp&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lv_vbeln&nbsp;=&nbsp;ls_vbrp-aubel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bu~bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;lv_bpext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000&nbsp;AS&nbsp;bu&nbsp;INNER&nbsp;JOIN&nbsp;vbpa&nbsp;AS&nbsp;vp&nbsp;ON&nbsp;bu~partner&nbsp;=&nbsp;vp~kunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vp~vbeln&nbsp;=&nbsp;lv_vbeln&nbsp;AND&nbsp;vp~parvw&nbsp;=&nbsp;'ZA'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;auart&nbsp;INTO&nbsp;lv_auart&nbsp;FROM&nbsp;vbak&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;&nbsp;lv_vbeln.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-sender&nbsp;=&nbsp;'ERP'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-receiver&nbsp;=&nbsp;'OA'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-bustyp&nbsp;=&nbsp;'SD041'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-messageid&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_date&nbsp;=&nbsp;sy-datum.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_time&nbsp;=&nbsp;sy-uzeit.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_msg_header-send_user&nbsp;=&nbsp;lv_bpext.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-workflowid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;zcl_eas_assist=&gt;get_workflowid(&nbsp;'SD041'&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-vbeln&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;i_vbrk-vbeln&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;.<br/>
</div>
<div class="codeComment">
* 销售组织<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vtext&nbsp;INTO&nbsp;ls_zdata_header-vkorg&nbsp;FROM&nbsp;tvkot&nbsp;WHERE&nbsp;vkorg&nbsp;=&nbsp;i_vbrk-vkorg.<br/>
</div>
<div class="codeComment">
*    ls_zdata_header-vkorg     = i_vbrk-vkorg.<br/>
*    ls_zdata_header-maber     = ls_vbrp-msr_refund_code.<br/>
* 产品组<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zoacpz&nbsp;INTO&nbsp;lv_zoacpz&nbsp;FROM&nbsp;ztsd008&nbsp;WHERE&nbsp;spart&nbsp;=&nbsp;i_vbrk-spart.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-spart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_zoacpz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;vkbur&nbsp;INTO&nbsp;lv_vkbur&nbsp;FROM&nbsp;vbak&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;lv_vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bezei&nbsp;INTO&nbsp;lv_bezei&nbsp;FROM&nbsp;tvkbt&nbsp;WHERE&nbsp;vkbur&nbsp;=&nbsp;lv_vkbur&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'.<br/>
</div>
<div class="codeComment">
* 销售办公室<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-vkbur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_bezei.<br/>
</div>
<div class="codeComment">
* 客户编号<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-kunnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;i_vbrk-kunag&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;concat(&nbsp;name1,name2&nbsp;)&nbsp;AS&nbsp;name1&nbsp;&nbsp;FROM&nbsp;kna1&nbsp;WHERE&nbsp;kunnr&nbsp;=&nbsp;@i_vbrk-kunag&nbsp;INTO&nbsp;@lv_name1.<br/>
</div>
<div class="codeComment">
* 客户名称<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-name_org1&nbsp;=&nbsp;lv_name1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kna1~name1&nbsp;INTO&nbsp;lv_xsywy&nbsp;FROM&nbsp;kna1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;vbpa&nbsp;ON&nbsp;kna1~kunnr&nbsp;=&nbsp;vbpa~kunnr&nbsp;WHERE&nbsp;vbpa~vbeln&nbsp;=&nbsp;lv_vbeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;vbpa~parvw&nbsp;=&nbsp;'ZA'.<br/>
</div>
<div class="codeComment">
* 销售业务员<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrk-vkorg&nbsp;EQ&nbsp;'6000'&nbsp;OR&nbsp;i_vbrk-vkorg&nbsp;EQ&nbsp;'6200'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;but~bpext&nbsp;INTO&nbsp;ls_zdata_header-zkunnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;but000&nbsp;AS&nbsp;but&nbsp;INNER&nbsp;JOIN&nbsp;knvp&nbsp;AS&nbsp;k&nbsp;ON&nbsp;but~partner&nbsp;=&nbsp;k~kunn2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;k~parvw&nbsp;=&nbsp;'ZA'&nbsp;AND&nbsp;k~kunnr&nbsp;=&nbsp;i_vbrk-kunag.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zkunnr&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_xsywy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*   区域公司<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_text&nbsp;USING&nbsp;'0012'&nbsp;'VBBK'&nbsp;lv_vbeln&nbsp;CHANGING&nbsp;ls_zdata_header-zqygs.<br/>
</div>
<div class="codeComment">
* 结算方式<br/>
*    SELECT SINGLE bstkd INTO ls_zdata_header-bstkd FROM vbkd WHERE vbeln = lv_vbeln.<br/>
** 产品类别<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ATINN_INPUT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;'ZYYYHG'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;lv_atinn.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;atwrt&nbsp;INTO&nbsp;ls_zdata_header-zyyyhg&nbsp;FROM&nbsp;ausp&nbsp;WHERE&nbsp;objek&nbsp;=&nbsp;ls_vbrp-matnr&nbsp;AND&nbsp;atinn&nbsp;=&nbsp;lv_atinn.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;ls_zdata_header-zyyyhg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'无水硫酸钠'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'纯碱'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'氯化铵'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'3'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'日用盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'4'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'氯碱'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zyyyhg&nbsp;=&nbsp;'5'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;i_vbrp&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrp-fkimg&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTINUE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_atwrt,lv_kwert,lv_kumza,lv_kumne,lv_kpein,lv_grund,lv_vsart,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_shuilv,lv_cjje1,lv_cjje2,lv_flag1,lv_flag2,lv_kbetr,lv_cjsl,lv_vbeln<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_vbeln&nbsp;=&nbsp;i_vbrp-aubel.<br/>
</div>
<div class="codeComment">
* 发票类型<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CASE&nbsp;i_vbrp-msr_refund_code.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maber&nbsp;=&nbsp;'专用发票'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maber&nbsp;=&nbsp;'普通发票'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'51'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maber&nbsp;=&nbsp;'电子发票'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'61'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maber&nbsp;=&nbsp;'电子专用发票'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'ZZ'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-maber&nbsp;=&nbsp;'不开票'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDCASE.<br/>
</div>
<div class="codeComment">
* 行项目数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-posnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;i_vbrp-posnr&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;.<br/>
</div>
<div class="codeComment">
* 获取物料类型<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;atwrt&nbsp;INTO&nbsp;lv_atwrt&nbsp;FROM&nbsp;ausp&nbsp;WHERE&nbsp;objek&nbsp;=&nbsp;i_vbrp-matnr&nbsp;AND&nbsp;atinn&nbsp;=&nbsp;lv_atinn.<br/>
</div>
<div class="codeComment">
* 开票单位<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;&lt;&gt;&nbsp;'日用盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zkpdw&nbsp;=&nbsp;'吨'.<br/>
</div>
<div class="codeComment">
* 出库单位<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-vrkme&nbsp;=&nbsp;'吨'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;mseh6&nbsp;INTO&nbsp;ls_zdata_item-zkpdw&nbsp;FROM&nbsp;t006a&nbsp;WHERE&nbsp;msehi&nbsp;=&nbsp;i_vbrp-vrkme&nbsp;AND&nbsp;spras&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-vrkme&nbsp;=&nbsp;ls_zdata_item-zkpdw.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
**   区域公司<br/>
*      PERFORM frm_get_text USING '0012' 'VBBK' lv_vbeln CHANGING ls_zdata_item-zqygs.<br/>
*   备注<br/>
*      PERFORM frm_get_text USING '0005' 'VBBK' lv_vbeln CHANGING ls_zdata_item-zfpbz.<br/>
* 交货地点<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_text&nbsp;USING&nbsp;'0001'&nbsp;'VBBK'&nbsp;lv_vbeln&nbsp;CHANGING&nbsp;ls_zdata_item-zjhdd.<br/>
</div>
<div class="codeComment">
* 冲减原因<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_text&nbsp;USING&nbsp;'0010'&nbsp;'VBBK'&nbsp;i_vbrp-vbeln&nbsp;CHANGING&nbsp;ls_zdata_item-zcjyy.<br/>
</div>
<div class="codeComment">
* 物料编码<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-matnr&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_vbrp-matnr.<br/>
</div>
<div class="codeComment">
* 发运单号<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zfydh&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;|{&nbsp;i_vbrp-vgbel&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;|{&nbsp;i_vbrp-vgpos&nbsp;ALPHA&nbsp;=&nbsp;OUT&nbsp;}|&nbsp;.<br/>
</div>
<div class="codeComment">
* 运费票制<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;bstkd&nbsp;INTO&nbsp;ls_zdata_item-zbstkd&nbsp;FROM&nbsp;vbkd&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;lv_vbeln.<br/>
</div>
<div class="codeComment">
*      ls_zdata_item-zbstkd    = ls_zdata_header-bstkd.<br/>
* 物料名称<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;maktx&nbsp;INTO&nbsp;ls_zdata_item-maktx&nbsp;FROM&nbsp;makt&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;i_vbrp-matnr.<br/>
</div>
<div class="codeComment">
* 发运日期<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;wadat_ist&nbsp;INTO&nbsp;lv_wadat&nbsp;FROM&nbsp;likp&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;i_vbrp-vgbel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_int_to_ext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datint&nbsp;&nbsp;&nbsp;=&nbsp;lv_wadat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datext&nbsp;&nbsp;&nbsp;=&nbsp;ls_zdata_item-wadat_ist.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_abap_datfm_format_unknown.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
</div>
<div class="codeComment">
* 运输方式<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;t173t~bezei,likp~vsart&nbsp;INTO&nbsp;(&nbsp;@ls_zdata_item-vsart&nbsp;,@lv_vsart&nbsp;)&nbsp;FROM&nbsp;t173t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;likp&nbsp;ON&nbsp;t173t~vsart&nbsp;=&nbsp;likp~vsart&nbsp;WHERE&nbsp;likp~vbeln&nbsp;=&nbsp;@i_vbrp-vgbel<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;t173t~spras&nbsp;=&nbsp;'1'.<br/>
</div>
<div class="codeComment">
* 到站港<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_vsart&nbsp;=&nbsp;'06'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kna1~name1&nbsp;INTO&nbsp;ls_zdata_item-zdzg&nbsp;FROM&nbsp;kna1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;likp&nbsp;ON&nbsp;kna1~kunnr&nbsp;=&nbsp;likp~kunnr&nbsp;WHERE&nbsp;likp~vbeln&nbsp;=&nbsp;i_vbrp-vgbel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SPLIT&nbsp;ls_zdata_item-zdzg&nbsp;AT&nbsp;'-'&nbsp;INTO&nbsp;ls_zdata_item-zdzg&nbsp;DATA(lv_zdzg).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ablad&nbsp;INTO&nbsp;ls_zdata_item-zdzg&nbsp;FROM&nbsp;likp&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;i_vbrp-vgbel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 车牌号<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;zcphm&nbsp;zjzxh1&nbsp;zjzxh2&nbsp;INTO&nbsp;(ls_zdata_item-zcphm,ls_zdata_item-zjzxh1,ls_zdata_item-zjzxh2)&nbsp;FROM&nbsp;likp&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;i_vbrp-vgbel.<br/>
<br/>
</div>
<div class="codeComment">
* 出库数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_lfimg,lv_meins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;lfimg&nbsp;meins&nbsp;INTO&nbsp;(lv_lfimg,lv_meins)&nbsp;FROM&nbsp;lips&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;i_vbrp-vgbel&nbsp;AND&nbsp;posnr&nbsp;=&nbsp;i_vbrp-vgpos.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;&lt;&gt;&nbsp;'日用盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_meins_trans&nbsp;USING&nbsp;'TO'&nbsp;lv_meins&nbsp;i_vbrp-matnr&nbsp;CHANGING&nbsp;lv_lfimg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-g_lfimg&nbsp;=&nbsp;lv_lfimg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_meins.<br/>
</div>
<div class="codeComment">
* 冲减数量<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:lv_lfimg_diff.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;lfimg_diff&nbsp;meins&nbsp;grund&nbsp;INTO&nbsp;(lv_lfimg_diff,lv_meins,lv_grund)&nbsp;FROM&nbsp;tvpod&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;i_vbrp-vgbel&nbsp;AND&nbsp;posnr&nbsp;=&nbsp;i_vbrp-vgpos&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_lfimg_diff&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;&lt;&gt;&nbsp;'日用盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_meins_trans&nbsp;USING&nbsp;'TO'&nbsp;lv_meins&nbsp;i_vbrp-matnr&nbsp;CHANGING&nbsp;lv_lfimg_diff.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zcjsl&nbsp;=&nbsp;lv_lfimg_diff.<br/>
</div>
<div class="codeComment">
*      lv_cjsl = lv_lfimg_diff.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_grund&nbsp;=&nbsp;'DFG1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;'-'&nbsp;ls_zdata_item-zcjsl&nbsp;INTO&nbsp;ls_zdata_item-zcjsl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 固定金额冲减<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kwert&nbsp;&nbsp;INTO&nbsp;lv_cjje1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'ZR89'.<br/>
</div>
<div class="codeComment">
* 冲减金额固定<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_cjje1&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zgdjecj&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zgdjecj&nbsp;=&nbsp;lv_cjje1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-zgdjecj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-zgdjecj&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-zgdjecj&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_item-zgdjecj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 冲减金额，单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kbetr&nbsp;kwert&nbsp;&nbsp;INTO&nbsp;(lv_kbetr,lv_cjje2)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'ZR99'.<br/>
</div>
<div class="codeComment">
* 冲减单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_kbetr&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zcjdj&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zcjdj&nbsp;=&nbsp;lv_kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-zcjdj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. znumber_point_format="" znumber_point_format.html"="">'ZNUMBER_POINT_FORMAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_data&nbsp;&nbsp;&nbsp;=&nbsp;ls_zdata_item-zcjdj<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_num&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;ls_zdata_item-zcjdj.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
* 冲减金额<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_cjje2&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zcjje&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zcjje&nbsp;=&nbsp;lv_cjje2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-zcjje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-zcjje&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-zcjje&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_item-zcjje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
* 开票数量(吨)<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_fkimg&nbsp;=&nbsp;i_vbrp-fkimg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_atwrt&nbsp;&lt;&gt;&nbsp;'日用盐'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_meins_trans&nbsp;USING&nbsp;'TO'&nbsp;i_vbrp-meins&nbsp;i_vbrp-matnr&nbsp;CHANGING&nbsp;lv_fkimg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-fkimg&nbsp;=&nbsp;lv_fkimg.<br/>
</div>
<div class="codeComment">
* 单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_auart&nbsp;=&nbsp;'ZSO2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kbetr&nbsp;kumza&nbsp;kumne&nbsp;kpein&nbsp;INTO&nbsp;(lv_kbetr,lv_kumza,lv_kumne,lv_kpein)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'ZPXX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kbetr&nbsp;kumza&nbsp;kumne&nbsp;kpein&nbsp;INTO&nbsp;(lv_kbetr,lv_kumza,lv_kumne,lv_kpein)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'ZR05'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kbetr&nbsp;=&nbsp;(&nbsp;lv_kumza&nbsp;*&nbsp;lv_kbetr&nbsp;)&nbsp;/&nbsp;(&nbsp;lv_kpein&nbsp;*&nbsp;lv_kumne&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrp-vrkme&nbsp;=&nbsp;'KG'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_kbetr&nbsp;=&nbsp;lv_kbetr&nbsp;*&nbsp;1000&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 金额<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrp-kzwi1&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-kwert&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-kwert&nbsp;=&nbsp;i_vbrp-kzwi1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_hszje&nbsp;=&nbsp;lv_hszje&nbsp;+&nbsp;i_vbrp-kzwi1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-kwert.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-kwert&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-kwert&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_item-kwert.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
* 折旧汇总金额<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrp-kzwi3&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zzkhz&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zzkhz&nbsp;=&nbsp;i_vbrp-kzwi3.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-zzkhz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-zzkhz&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-zzkhz&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_item-zzkhz.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
* 单价<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_kbetr&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-kbetr&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-kbetr&nbsp;=&nbsp;lv_kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. znumber_point_format="" znumber_point_format.html"="">'ZNUMBER_POINT_FORMAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_data&nbsp;&nbsp;&nbsp;=&nbsp;ls_zdata_item-kbetr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_num&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;=&nbsp;ls_zdata_item-kbetr.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
* 税率<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kbetr&nbsp;INTO&nbsp;lv_shuilv&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'MWSI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_shuilv&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kbetr&nbsp;INTO&nbsp;lv_shuilv&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'MWST'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;<a&nbsp;href&nbsp;=".. znumber_point_format="" znumber_point_format.html"="">'ZNUMBER_POINT_FORMAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_shuilv<br/>
</a&nbsp;href&nbsp;="..></div>
<div class="codeComment">
*         I_NUM          =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_flag_integer&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_zdata_item-zxssl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-zxssl&nbsp;'%'&nbsp;INTO&nbsp;ls_zdata_item-zxssl.<br/>
</div>
<div class="codeComment">
* 运杂费<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;kwert&nbsp;INTO&nbsp;lv_yunza&nbsp;FROM&nbsp;prcd_elements&nbsp;WHERE&nbsp;knumv&nbsp;=&nbsp;i_vbrk-knumv&nbsp;AND&nbsp;kposn&nbsp;=&nbsp;i_vbrp-posnr&nbsp;AND&nbsp;kschl&nbsp;=&nbsp;'ZF00'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_yunza&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zyzfy&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_item-zyzfy&nbsp;=&nbsp;lv_yunza.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_item-zyzfy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_item-zyzfy&nbsp;NO-GAPS.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_item-zyzfy&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_item-zyzfy.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_zdata_item&nbsp;TO&nbsp;ls_zdata_items.<br/>
<br/>
</div>
<div class="codeComment">
* 审批流程<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;i_vbrk-vkorg&nbsp;=&nbsp;'6000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_zdata_header-zsplc&nbsp;&lt;&gt;&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_zdata_item-zcjdj&nbsp;=&nbsp;lv_str&nbsp;AND&nbsp;ls_zdata_item-zgdjecj&nbsp;=&nbsp;lv_str<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;ls_zdata_item-zzkhz&nbsp;=&nbsp;lv_str&nbsp;AND&nbsp;lv_lfimg_diff&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zsplc&nbsp;=&nbsp;'1'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zsplc&nbsp;=&nbsp;'2'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lv_hszje&nbsp;=&nbsp;'0'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zhszje&nbsp;=&nbsp;'0.000'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CLOI_PUT_SIGN_IN_FRONT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;value&nbsp;=&nbsp;ls_zdata_header-zhszje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata_header-zhszje&nbsp;=&nbsp;lv_hszje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONDENSE&nbsp;ls_zdata_header-zhszje&nbsp;NO-GAPS&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;ls_zdata_header-zhszje&nbsp;'0'&nbsp;INTO&nbsp;ls_zdata_header-zhszje.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-header&nbsp;=&nbsp;ls_zdata_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_zdata-items&nbsp;=&nbsp;ls_zdata_items.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd041_bill_info_req-message_header&nbsp;=&nbsp;ls_msg_header.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out-mt_erp_sd041_bill_info_req-zdata&nbsp;=&nbsp;ls_zdata.<br/>
<br/>
</div>
<div class="codeComment">
*  发送数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;bill_info_req&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREATE&nbsp;OBJECT&nbsp;bill_info_req.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;bill_info_req-&gt;si_erp2oa_sd041_bill_info_sync<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;ls_out<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;ls_in.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_in-mt_erp_sd041_bill_info_res-zdata-header-header_status&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;ls_in-mt_erp_sd041_bill_info_res-zdata-header-header_text.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg&nbsp;=&nbsp;lo_error-&gt;get_text(&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_msg_type&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lo_log-&gt;set_rpoxy_sender_msgid(&nbsp;proxy&nbsp;=&nbsp;bill_info_req&nbsp;).<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;e_msg_type&nbsp;=&nbsp;lv_msg_type.<br/>
&nbsp;&nbsp;e_msg&nbsp;&nbsp;=&nbsp;lv_msg.<br/>
<br/>
&nbsp;&nbsp;lv_key&nbsp;=&nbsp;lv_uuid.<br/>
&nbsp;&nbsp;lo_log-&gt;save_log(<br/>
&nbsp;&nbsp;mtype&nbsp;=&nbsp;lv_msg_type<br/>
&nbsp;&nbsp;msg&nbsp;&nbsp;=&nbsp;lv_msg<br/>
&nbsp;&nbsp;key1&nbsp;=&nbsp;lv_key<br/>
&nbsp;&nbsp;key2&nbsp;=&nbsp;i_vbrk-vbeln&nbsp;).<br/>
&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>