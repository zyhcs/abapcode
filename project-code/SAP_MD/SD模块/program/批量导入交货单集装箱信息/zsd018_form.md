<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD018_FORM</h2>
<h3> Description: Include ZSD018_FORM</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD018_FORM<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;download<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM download .<br/>
&nbsp;&nbsp;DATA:&nbsp;l_filename&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fullpath&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;CHECK&nbsp;sy-ucomm&nbsp;=&nbsp;'FC01'.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;sy-ucomm.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'FC01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename&nbsp;=&nbsp;TEXT-003.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname&nbsp;=&nbsp;'ZSD018_IMPORT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;OTHERS.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Get&nbsp;the&nbsp;file&nbsp;path<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;get_fullpath&nbsp;CHANGING&nbsp;l_fullpath&nbsp;l_path&nbsp;l_filename.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;l_fullpath&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'找不到模板'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;download_template&nbsp;USING&nbsp;l_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_tempname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;GET_FULLPATH<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_FULLPATH&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_PATH&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_L_FILENAME&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM get_fullpath  CHANGING p_fullpath TYPE string<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_path&nbsp;TYPE&nbsp;string<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_name&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA:&nbsp;l_init_path&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_filename&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_fullpath&nbsp;&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;l_filename&nbsp;=&nbsp;p_name.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---用户选择名称、路径<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'XLS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prompt_on_overwrite&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_fullpath&nbsp;=&nbsp;l_fullpath.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_path.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;p_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_filename.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;DOWNLOAD_TEMPLATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_L_FULLPATH&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_L_FILENAME&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_L_TEMPNAME&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_L_PATH&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM download_template  USING    p_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_tempname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p_path.<br/>
&nbsp;&nbsp;DATA:&nbsp;lwa_objdata&nbsp;&nbsp;&nbsp;LIKE&nbsp;wwwdatatab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_destination&nbsp;LIKE&nbsp;rlgrap-filename,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_objnam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_errtxt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_filename&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_result,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_subrc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sy-subrc.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---查找文件是否存在。<br/>
</div>
<div class="code">
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;relid&nbsp;objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;lwa_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;srtf2&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;relid&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'MI'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;&nbsp;objid&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_tempname.<br/>
<br/>
&nbsp;&nbsp;l_destination&nbsp;&nbsp;&nbsp;=&nbsp;p_fullpath.<br/>
<br/>
&nbsp;&nbsp;CLEAR:&nbsp;l_subrc.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---下载模版。<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lwa_objdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;l_destination<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_subrc.<br/>
&nbsp;&nbsp;IF&nbsp;l_subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'下载模板失败'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;f4_help_file<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM f4_help_file .<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'F4_FILENAME'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_name&nbsp;=&nbsp;p_file.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_MAIN<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_main .<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;p_file&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'请输入文件路径'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;STOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;upload_file.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Check&nbsp;&amp;&nbsp;Prepare&nbsp;Data<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;check_prepare_data.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;display.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;save_data.<br/>
**<br/>
<br/>
*<br/>
*&nbsp;&nbsp;ELSEIF&nbsp;p_chaxun&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_display_chaxun.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;UPLOAD_FILE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM upload_file .<br/>
&nbsp;&nbsp;DATA:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lit_intern&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;alsmex_tabline,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lwa_intern&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;lit_intern,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lwa_import&nbsp;LIKE&nbsp;LINE&nbsp;OF&nbsp;it_import,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_temp&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;any.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ALSM_EXCEL_TO_INTERNAL_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;p_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;10<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;65536<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lit_intern<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inconsistent_parameters&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;upload_ole&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;File&nbsp;cannot&nbsp;be&nbsp;Uploaded<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;'数据导入错误'&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LEAVE&nbsp;LIST-PROCESSING.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;get&nbsp;file&nbsp;data<br/>
</div>
<div class="code">
&nbsp;&nbsp;SORT&nbsp;lit_intern&nbsp;BY&nbsp;row&nbsp;col.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lit_intern&nbsp;INTO&nbsp;lwa_intern.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;l_col&nbsp;=&nbsp;lwa_intern-col.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.&nbsp;&nbsp;&nbsp;"Import&nbsp;new&nbsp;data<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;l_col&nbsp;OF&nbsp;STRUCTURE&nbsp;lwa_import&nbsp;TO&nbsp;&lt;fs_temp&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_temp&gt;&nbsp;IS&nbsp;ASSIGNED.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_temp&gt;&nbsp;=&nbsp;lwa_intern-value.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UNASSIGN&nbsp;&lt;fs_temp&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lwa_import-zrow&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lwa_import-zrow&nbsp;=&nbsp;lwa_intern-row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;lwa_import&nbsp;TO&nbsp;it_import.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:&nbsp;lwa_import.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDAT.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;CHECK_PREPARE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM check_prepare_data .<br/>
<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_import&nbsp;INTO&nbsp;DATA(ls_import).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_import-vbeln&nbsp;=&nbsp;|{&nbsp;ls_import-vbeln&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SINGLE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COUNT(*)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;likp<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;vbeln&nbsp;=&nbsp;ls_import-vbeln.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_import-msg&nbsp;=&nbsp;'交货单不存在,请检查'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_import-mark&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_import-light&nbsp;=&nbsp;icon_red_light.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_import-light&nbsp;=&nbsp;icon_yellow_light.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;it_import&nbsp;FROM&nbsp;ls_import.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_import.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA:ls_import&nbsp;TYPE&nbsp;t_import.<br/>
*&nbsp;&nbsp;DATA:l_error&nbsp;TYPE&nbsp;char10.<br/>
*&nbsp;&nbsp;DATA:lt_zctewmmarbq&nbsp;LIKE&nbsp;TABLE&nbsp;OF&nbsp;zctewmmarbq.<br/>
*&nbsp;&nbsp;DATA:n&nbsp;TYPE&nbsp;n&nbsp;LENGTH&nbsp;13.<br/>
*&nbsp;&nbsp;DATA:l_type&nbsp;TYPE&nbsp;dd01v-datatype.<br/>
*&nbsp;&nbsp;DATA:l_xiangshu&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;13.<br/>
*&nbsp;&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;it_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;*&nbsp;FROM&nbsp;zctewmmarbq&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_zctewmmarbq<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;it_import<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;werks&nbsp;=&nbsp;it_import-werks<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;matnr&nbsp;=&nbsp;it_import-matnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_import&nbsp;ASSIGNING&nbsp;FIELD-SYMBOL(&lt;fs_import&gt;).<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_ALPHA_INPUT'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;=&nbsp;&lt;fs_import&gt;-matnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;=&nbsp;&lt;fs_import&gt;-matnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;matnr&nbsp;INTO&nbsp;@DATA(lv_matnr)&nbsp;FROM&nbsp;mara&nbsp;WHERE&nbsp;matnr&nbsp;=&nbsp;@&lt;fs_import&gt;-matnr.&nbsp;"检查物料编码<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s135(zcewm01)&nbsp;INTO&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-msg&nbsp;=&nbsp;&lt;fs_import&gt;-msg&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-mark&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-light&nbsp;=&nbsp;icon_red_light.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检查工厂<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_import&gt;-werks&nbsp;&lt;&gt;&nbsp;'6500'&nbsp;AND&nbsp;&lt;fs_import&gt;-werks&nbsp;&lt;&gt;&nbsp;'6541'&nbsp;AND&nbsp;&lt;fs_import&gt;-werks&nbsp;&lt;&gt;&nbsp;'6605'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s143(zcewm01)&nbsp;INTO&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-msg&nbsp;=&nbsp;&lt;fs_import&gt;-msg&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-mark&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-light&nbsp;=&nbsp;icon_red_light.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检查箱数<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;l_xiangshu&nbsp;=&nbsp;&lt;fs_import&gt;-zzpalqu.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'NUMERIC_CHECK'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;string_in&nbsp;&nbsp;=&nbsp;l_xiangshu<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;string_out&nbsp;=&nbsp;n<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;htype&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;l_type.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;l_type&nbsp;=&nbsp;'CHAR'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s145(zcewm01)&nbsp;INTO&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-msg&nbsp;=&nbsp;&lt;fs_import&gt;-msg&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-mark&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-light&nbsp;=&nbsp;icon_red_light.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"检查单位是否为CV<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_import&gt;-meins&nbsp;&lt;&gt;&nbsp;'CS'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s144(zcewm01)&nbsp;INTO&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-msg&nbsp;=&nbsp;&lt;fs_import&gt;-msg&nbsp;&amp;&amp;&nbsp;'/'&nbsp;&amp;&amp;&nbsp;l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-mark&nbsp;=&nbsp;'X'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-light&nbsp;=&nbsp;icon_red_light.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:l_error.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'CONVERSION_EXIT_CUNIT_OUTPUT'&nbsp;&nbsp;"单位转换<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_import&gt;-meins<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-langu<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LONG_TEXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_import&gt;-meins<br/>
**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SHORT_TEXT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unit_not_found&nbsp;=&nbsp;1<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
**&nbsp;Implement&nbsp;suitable&nbsp;error&nbsp;handling&nbsp;here<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;&lt;fs_import&gt;-mark&nbsp;=&nbsp;''.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SORT&nbsp;lt_zctewmmarbq&nbsp;BY&nbsp;werks&nbsp;matnr.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_zctewmmarbq&nbsp;INTO&nbsp;DATA(ls_zctewmmarbq)<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;KEY&nbsp;werks&nbsp;=&nbsp;&lt;fs_import&gt;-werks<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;matnr&nbsp;=&nbsp;&lt;fs_import&gt;-matnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BINARY&nbsp;SEARCH.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s142(zcewm01)&nbsp;INTO&nbsp;&lt;fs_import&gt;-msg.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s141(zcewm01)&nbsp;INTO&nbsp;&lt;fs_import&gt;-msg.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;fs_import&gt;-light&nbsp;=&nbsp;icon_green_light.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOVE-CORRESPONDING&nbsp;&lt;fs_import&gt;&nbsp;TO&nbsp;gs_zctewmmarbq.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_zctewmmarbq&nbsp;TO&nbsp;gt_zctewmmarbq.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
*&nbsp;&nbsp;ENDIF.<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;SAVE_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM save_data .<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA:&nbsp;lcl_root&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cx_root.<br/>
*&nbsp;&nbsp;TRY.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;p_import&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;zctewmmarbq&nbsp;FROM&nbsp;TABLE&nbsp;gt_zctewmmarbq.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK&nbsp;AND&nbsp;WAIT.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;lcl_root.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ROLLBACK&nbsp;WORK.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s064(zcewm01)&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
*&nbsp;&nbsp;ENDTRY.<br/>
*&nbsp;&nbsp;MESSAGE&nbsp;s065(zcewm01).<br/>
</div>
<div class="code">
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;display<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM display .<br/>
&nbsp;&nbsp;SORT&nbsp;it_import&nbsp;BY&nbsp;mark.<br/>
&nbsp;&nbsp;ws_layout_lvc-zebra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ws_layout_lvc-cwidth_opt&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ws_layout_lvc-box_fname&nbsp;=&nbsp;'SEL'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'LIGHT'&nbsp;'信号灯'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'MSG'&nbsp;'消息'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'VBELN'&nbsp;'交货单号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZCPHM'&nbsp;'车牌号'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZCYWL'&nbsp;'承运物流'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZCYRY'&nbsp;'承运人'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZCYRT'&nbsp;'承运人电话'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZJZXH1'&nbsp;'集装箱号1'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZJZXHZL1'&nbsp;'集装箱号1重量'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZCSRQ'&nbsp;'传输日期'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'ZZDR'&nbsp;'制单人'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
&nbsp;&nbsp;PERFORM&nbsp;add_fieldcat&nbsp;&nbsp;USING&nbsp;'SDABW'&nbsp;'特殊处理标识'&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''&nbsp;''.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'REUSE_ALV_GRID_DISPLAY_LVC'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_program&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-repid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_pf_status_set&nbsp;=&nbsp;'SET_DATA_STATUS'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_callback_user_command&nbsp;&nbsp;=&nbsp;'DATA_COMMAND'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_layout_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ws_layout_lvc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it_fieldcat_lvc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wt_fieldcat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t_outtab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;it_import.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM add_fieldcat USING wlv_s1  wlv_s2 wlv_s3 wlv_s4 wlv_s5 wlv_s6 wlv_s7 wlv_s8 wlv_s9 wlv_s10.<br/>
&nbsp;&nbsp;DATA&nbsp;wls_fieldcat&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_fcat.<br/>
&nbsp;&nbsp;wls_fieldcat-fieldname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s1.<br/>
&nbsp;&nbsp;wls_fieldcat-coltext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s2.<br/>
&nbsp;&nbsp;wls_fieldcat-scrtext_l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s2.<br/>
&nbsp;&nbsp;wls_fieldcat-scrtext_m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s2.<br/>
&nbsp;&nbsp;wls_fieldcat-scrtext_s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s2.<br/>
&nbsp;&nbsp;wls_fieldcat-tooltip&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s2.<br/>
&nbsp;&nbsp;wls_fieldcat-fix_column&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s3.<br/>
&nbsp;&nbsp;wls_fieldcat-edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s4.<br/>
&nbsp;&nbsp;wls_fieldcat-hotspot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s5.<br/>
&nbsp;&nbsp;wls_fieldcat-lzero&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s6.<br/>
&nbsp;&nbsp;wls_fieldcat-ref_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s7.<br/>
&nbsp;&nbsp;wls_fieldcat-ref_field&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s8.<br/>
&nbsp;&nbsp;wls_fieldcat-convexit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s9.<br/>
&nbsp;&nbsp;wls_fieldcat-just&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;wlv_s10.<br/>
<br/>
&nbsp;&nbsp;APPEND&nbsp;wls_fieldcat&nbsp;TO&nbsp;wt_fieldcat.<br/>
&nbsp;&nbsp;CLEAR&nbsp;wls_fieldcat.<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM set_data_status USING rt_extab TYPE slis_t_extab.<br/>
&nbsp;&nbsp;SET&nbsp;PF-STATUS&nbsp;'ZSD018_100'.<br/>
ENDFORM.<br/>
<br/>
<br/>
FORM data_command USING p_ucomm LIKE sy-ucomm<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pwa_selfield&nbsp;TYPE&nbsp;slis_selfield.<br/>
&nbsp;&nbsp;DATA:&nbsp;lo_guid&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_gui_alv_grid.<br/>
&nbsp;&nbsp;DATA:&nbsp;stbl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;lvc_s_stbl.<br/>
<br/>
&nbsp;&nbsp;stbl-row&nbsp;=&nbsp;'X'."&nbsp;基于行的稳定刷新<br/>
&nbsp;&nbsp;stbl-col&nbsp;=&nbsp;'X'."&nbsp;基于列稳定刷新<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'GET_GLOBALS_FROM_SLVC_FULLSCR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_grid&nbsp;=&nbsp;lo_guid.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lo_guid-&gt;check_changed_data.<br/>
<br/>
&nbsp;&nbsp;DATA:lwa_mard&nbsp;TYPE&nbsp;mard.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;lo_guid-&gt;refresh_table_display<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_stable&nbsp;=&nbsp;stbl.<br/>
<br/>
&nbsp;&nbsp;pwa_selfield-refresh&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;pwa_selfield-col_stable&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;pwa_selfield-row_stable&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;CASE&nbsp;p_ucomm.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHEN&nbsp;'IMPORT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_IMPORT.<br/>
&nbsp;&nbsp;ENDCASE.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_IMPORT<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_IMPORT .<br/>
&nbsp;&nbsp;DATA&nbsp;header_data&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiobdlvhdrchg.<br/>
&nbsp;&nbsp;DATA&nbsp;header_control&nbsp;TYPE&nbsp;bapiobdlvhdrctrlchg.<br/>
&nbsp;&nbsp;DATA&nbsp;delivery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;bapiobdlvhdrchg-deliv_numb.<br/>
&nbsp;&nbsp;DATA&nbsp;techn_control&nbsp;&nbsp;TYPE&nbsp;bapidlvcontrol.<br/>
&nbsp;&nbsp;DATA&nbsp;item_data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiobdlvitemchg&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;item_control&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiobdlvitemctrlchg&nbsp;.&nbsp;"交货单行项目<br/>
&nbsp;&nbsp;DATA&nbsp;extension2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiext&nbsp;.&nbsp;"增强字段<br/>
&nbsp;&nbsp;DATA&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;bapiret2&nbsp;.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;delivry&nbsp;TYPE&nbsp;bapiobdlvhdrchg-deliv_numb.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_import&nbsp;INTO&nbsp;DATA(ls_item).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;delivery&nbsp;=&nbsp;|{&nbsp;ls_item-vbeln&nbsp;ALPHA&nbsp;=&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header_data-deliv_numb&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;delivery.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header_data-unit_of_wt&nbsp;=&nbsp;'KG'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header_control-deliv_numb&nbsp;=&nbsp;delivery.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;header_control-gross_wt_flg&nbsp;=&nbsp;'X'.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;techn_control-upd_ind&nbsp;=&nbsp;'U'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_item-zcsrq&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_datext&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;10.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_datint&nbsp;TYPE&nbsp;d.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_datext&nbsp;=&nbsp;ls_item-zcsrq.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_abap_datfm=&gt;conv_date_ext_to_int<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datext&nbsp;&nbsp;&nbsp;=&nbsp;lv_datext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;im_datfmdes&nbsp;=&nbsp;'6'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datint&nbsp;&nbsp;&nbsp;=&nbsp;lv_datint<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ex_datfmused&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_root&nbsp;INTO&nbsp;DATA(lo_error).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-msg&nbsp;=&nbsp;ls_item-msg&nbsp;&amp;&amp;&nbsp;'传输日期格式错误,要求格式(YYYY-MM-DD)'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXIT.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDTRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;extension2&nbsp;=&nbsp;VALUE&nbsp;#(&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZCPHM'&nbsp;&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zcphm&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZCYWL'&nbsp;&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zcywl&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZCYRY'&nbsp;&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zcyry&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZCYRT'&nbsp;&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zcyrt&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZJZXH1'&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zjzxh1&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZJZXHZL1'&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zjzxhzl1&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZCSRQ'&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;lv_datint&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'ZZDR'&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-zzdr&nbsp;)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;field&nbsp;=&nbsp;'SDABW'&nbsp;&nbsp;param&nbsp;=&nbsp;'LIKP'&nbsp;value&nbsp;=&nbsp;ls_item-sdabw&nbsp;&nbsp;)&nbsp;&nbsp;).<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_OUTB_DELIVERY_CHANGE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header_data&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;header_data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;header_control&nbsp;=&nbsp;header_control<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;delivery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;delivery<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;techn_control&nbsp;&nbsp;=&nbsp;techn_control<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;item_data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;item_data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;item_control&nbsp;&nbsp;&nbsp;=&nbsp;item_control<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;extension2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;extension2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;return.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_msg&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA&nbsp;lv_msg2&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_msg2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;return&nbsp;INTO&nbsp;DATA(ls_return)&nbsp;WHERE&nbsp;type&nbsp;CO&nbsp;'AEX'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;ls_return-id&nbsp;TYPE&nbsp;ls_return-type&nbsp;NUMBER&nbsp;ls_return-number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;ls_return-message_v1&nbsp;ls_return-message_v2&nbsp;ls_return-message_v3&nbsp;ls_return-message_v4&nbsp;INTO&nbsp;lv_msg.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-msg&nbsp;=&nbsp;ls_item-msg&nbsp;&amp;&amp;&nbsp;lv_msg.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_ROLLBACK'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-mark&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-light&nbsp;=&nbsp;icon_red_light.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-mark&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-light&nbsp;=&nbsp;icon_GREEN_light.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_item-msg&nbsp;=&nbsp;'导入修改成功'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;it_import&nbsp;FROM&nbsp;ls_item.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR:ls_item.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>