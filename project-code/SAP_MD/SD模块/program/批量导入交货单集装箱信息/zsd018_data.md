<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZSD018_DATA</h2>
<h3> Description: Include ZSD018_DATA</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZSD018_DATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
TYPE-POOLS: icon.<br/>
TABLES:sscrfields.<br/>
<br/>
<br/>
TYPES: BEGIN OF t_import,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vbeln&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-vbeln,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcphm&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zcphm,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcywl&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zcywl,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcyry&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zcyry,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcyrt&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zcyrt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjzxh1&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zjzxh1,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjzxh2&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zjzxh2,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjzxhzl1&nbsp;TYPE&nbsp;likp-zjzxhzl1,<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zjzxhzl2&nbsp;TYPE&nbsp;likp-zjzxhzl2,<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zcsrq&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zcsrq,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zzdr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-zzdr,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sdabw&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;likp-sdabw,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char50,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mark&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;char1,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zrow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;kcd_ex_row_n,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;light&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;10,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;sel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;t_import.<br/>
<br/>
DATA: it_import TYPE STANDARD TABLE OF t_import.<br/>
<br/>
DATA: wt_fieldcat   TYPE lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ws_layout_lvc&nbsp;TYPE&nbsp;lvc_s_layo.<br/>
<br/>
SELECTION-SCREEN FUNCTION KEY 1.<br/>
<br/>
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.<br/>
<br/>
&nbsp;&nbsp;PARAMETERS:&nbsp;p_file&nbsp;TYPE&nbsp;localfile.<br/>
<br/>
SELECTION-SCREEN END OF BLOCK b1.<br/>
<br/>
INITIALIZATION.<br/>
&nbsp;&nbsp;sscrfields-functxt_01&nbsp;=&nbsp;TEXT-002.<br/>
<br/>
AT SELECTION-SCREEN OUTPUT.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;PERFORM&nbsp;selection_sreen_pbo.<br/>
<br/>
</div>
<div class="code">
AT SELECTION-SCREEN.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;download.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"上传模板关键代码<br/>
<br/>
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.  "文件路径搜索帮助<br/>
&nbsp;&nbsp;PERFORM&nbsp;f4_help_file.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>