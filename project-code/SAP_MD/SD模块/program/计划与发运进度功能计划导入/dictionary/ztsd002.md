<table class="outerTable">
<tr>
<td><h2>Table: ZTSD002</h2>
<h3>Description: 计划与发运数据导入表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>KUNNR</td>
<td>3</td>
<td>X</td>
<td>KUNNR</td>
<td>KUNNR</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>客户编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>MAKTX</td>
<td>8</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>MATNR</td>
<td>4</td>
<td>X</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>MATYP</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>物料分类标准</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>PLNMG</td>
<td>11</td>
<td>&nbsp;</td>
<td>PLNMG</td>
<td>MENG13</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>计划数量</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>VKBUR</td>
<td>13</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销售办事处</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>VKORG</td>
<td>2</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>销售组织</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZCUST_NAME</td>
<td>7</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>客户名称</td>
</tr>
<tr class="cell">
<td>10</td>
<td>ZPLAN_DATE</td>
<td>5</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>计划日期</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZPLAN_DATE_TYPE</td>
<td>6</td>
<td>X</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>计划日期类型</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZSALE_PERSON</td>
<td>12</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>销售业务员</td>
</tr>
<tr class="cell">
<td>13</td>
<td>ZTRANS_TYPE</td>
<td>10</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>运输方式</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>