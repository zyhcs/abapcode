<table class="outerTable">
<tr>
<td><h2>Table: ZSPM006</h2>
<h3>Description: 设备状况统计月报</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>SWERK</td>
<td>1</td>
<td>&nbsp;</td>
<td>SWERK</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>维护工厂</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>INGRP</td>
<td>2</td>
<td>&nbsp;</td>
<td>INGRP</td>
<td>INGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户服务和工厂维护的计划员组</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>INNAM</td>
<td>3</td>
<td>&nbsp;</td>
<td>INNAM</td>
<td>TEXT18</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>维护计划员组的名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZBREAKDOWN_CNT</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>故障次数</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZEQ_PERF_RATE</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>动设备完好率</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>ZMAIN_EQ_PERF_RATE</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>主要动设备完好率</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>