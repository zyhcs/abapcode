<table class="outerTable">
<tr>
<td><h2>Table: ZSPM005</h2>
<h3>Description: 工单打印结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>NAME1</td>
<td>1</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>EQUNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>EQUNR</td>
<td>EQUNR</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>设备号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>EQKTX</td>
<td>3</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>AUFNR</td>
<td>4</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>AUFNR_TXT</td>
<td>5</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>QMNUM</td>
<td>6</td>
<td>&nbsp;</td>
<td>QMNUM</td>
<td>QMNUM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>通知编号</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>TPLNR</td>
<td>7</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>PLTXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>WHGZZX_TXT</td>
<td>9</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>100</td>
<td>&nbsp;</td>
<td>维护工作中心</td>
</tr>
<tr class="cell">
<td>10</td>
<td>AUART_TXT</td>
<td>10</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ILART</td>
<td>11</td>
<td>&nbsp;</td>
<td>ILA</td>
<td>ILART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>维护作业类型</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ILATX</td>
<td>12</td>
<td>&nbsp;</td>
<td>ILATX</td>
<td>TXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>维护作业类型描述</td>
</tr>
<tr class="cell">
<td>13</td>
<td>GSTRP</td>
<td>13</td>
<td>&nbsp;</td>
<td>PM_ORDGSTRP</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基本开始日期</td>
</tr>
<tr class="cell">
<td>14</td>
<td>GLTRP</td>
<td>14</td>
<td>&nbsp;</td>
<td>CO_GLTRP</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基本完成日期</td>
</tr>
<tr class="cell">
<td>15</td>
<td>AUFNR_DETL</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>工单详细描述</td>
</tr>
<tr class="cell">
<td>16</td>
<td>VORNR</td>
<td>16</td>
<td>&nbsp;</td>
<td>VORNR</td>
<td>VORNR</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>操作/活动编号</td>
</tr>
<tr class="cell">
<td>17</td>
<td>STEUS</td>
<td>17</td>
<td>&nbsp;</td>
<td>STEUS</td>
<td>STEUS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>控制码</td>
</tr>
<tr class="cell">
<td>18</td>
<td>LTXA1</td>
<td>18</td>
<td>&nbsp;</td>
<td>LTXA1</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>工序短文本</td>
</tr>
<tr class="cell">
<td>19</td>
<td>AUART</td>
<td>19</td>
<td>&nbsp;</td>
<td>AUART</td>
<td>AUART</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>销售凭证类型</td>
</tr>
<tr class="cell">
<td>20</td>
<td>QMNUM_DETL</td>
<td>20</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>200</td>
<td>&nbsp;</td>
<td>通知单长文本</td>
</tr>
<tr class="cell">
<td>21</td>
<td>SGDW</td>
<td>21</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>