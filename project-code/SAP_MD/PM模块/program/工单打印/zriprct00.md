<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZRIPRCT00</h2>
<h3> Description: 工单打印</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
REPORT zriprct00 .<br/>
</div>
<div class="codeComment">
************************************************************************<br/>
*&nbsp;Print&nbsp;Drive&nbsp;ABAP&nbsp;for&nbsp;CONTROL&nbsp;TICKET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;Standard&nbsp;FORM&nbsp;is&nbsp;&nbsp;PM_COMMOM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;ABAP&nbsp;STEPS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;1:&nbsp;IMPORT&nbsp;data&nbsp;to&nbsp;print.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;2:&nbsp;Parse&nbsp;of&nbsp;Data&nbsp;to&nbsp;print&nbsp;FORM.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The&nbsp;external&nbsp;text&nbsp;tables&nbsp;will&nbsp;be&nbsp;read&nbsp;as&nbsp;necessary.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;3:&nbsp;Save&nbsp;Print-Protocol&nbsp;records&nbsp;in&nbsp;PMPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
************************************************************************<br/>
*$*$&nbsp;&nbsp;D&nbsp;A&nbsp;T&nbsp;A&nbsp;&nbsp;&nbsp;&nbsp;S&nbsp;E&nbsp;C&nbsp;T&nbsp;I&nbsp;O&nbsp;N&nbsp;&nbsp;&nbsp;&nbsp;I&nbsp;N&nbsp;C&nbsp;L&nbsp;U&nbsp;D&nbsp;E&nbsp;S&nbsp;---------------------*<br/>
</div>
<div class="code">
INCLUDE riprid01.                      " General DATA and TABLE struct.<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;定义结构与表<br/>
*----------------------------------------------------------------------*<br/>
<br/>
<br/>
<br/>
</div>
<div class="code">
DATA gs_head TYPE zspm005.<br/>
DATA gt_tab TYPE TABLE OF zspm005.<br/>
DATA gw_tab TYPE zspm005.<br/>
<br/>
</div>
<div class="codeComment">
*------------------*<br/>
</div>
<div class="code">
START-OF-SELECTION.<br/>
</div>
<div class="codeComment">
*------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;print_paper.&nbsp;&nbsp;"can&nbsp;be&nbsp;started&nbsp;via&nbsp;SUBMIT&nbsp;or&nbsp;PERFORM&nbsp;PRINT_PAPER<br/>
&nbsp;&nbsp;PERFORM&nbsp;print_paper_pdf.&nbsp;&nbsp;"can&nbsp;be&nbsp;started&nbsp;via&nbsp;SUBMIT&nbsp;or&nbsp;PERFORM&nbsp;PRINT_PAPER_PDF<br/>
<br/>
</div>
<div class="codeComment">
*$*$&nbsp;................&nbsp;M&nbsp;A&nbsp;I&nbsp;N&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F&nbsp;O&nbsp;R&nbsp;M&nbsp;.............................*<br/>
*...&nbsp;DATA&nbsp;STRUCTURE:&nbsp;..................................................*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;CAUFVD&nbsp;(AFIH&nbsp;AUFK&nbsp;AFKO&nbsp;plus&nbsp;other&nbsp;dialog&nbsp;fields:&nbsp;ORDER&nbsp;HEADER)&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--&nbsp;AFVGD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(AFVC&nbsp;AFVV&nbsp;plus&nbsp;dialog&nbsp;fields)&nbsp;Order&nbsp;operatns&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|--&nbsp;The&nbsp;sub&nbsp;operations&nbsp;also&nbsp;stored&nbsp;AFVGD&nbsp;and&nbsp;are&nbsp;pre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sorted.&nbsp;The&nbsp;SUMNR&nbsp;fields&nbsp;distinguishes&nbsp;Main&nbsp;operaitons*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;sub&nbsp;operations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--&nbsp;RESBD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Materials&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--&nbsp;RIPW0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Object&nbsp;list&nbsp;dialog&nbsp;area<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;------VIQMEL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;First&nbsp;Notification&nbsp;from&nbsp;Object&nbsp;list<br/>
*...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--&nbsp;IHPAD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Partners&nbsp;to&nbsp;Orders&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*...<br/>
*......................................................................*<br/>
<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORM&nbsp;PRINT_PAPER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Main&nbsp;driving&nbsp;Form&nbsp;behind&nbsp;the&nbsp;Printing&nbsp;of&nbsp;Papers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All&nbsp;information&nbsp;is&nbsp;imported&nbsp;from&nbsp;MEMORY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;FORM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name&nbsp;of&nbsp;SAPSCRIPT&nbsp;form&nbsp;to&nbsp;use.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;WWORKPAPER&nbsp;&nbsp;Print&nbsp;options&nbsp;for&nbsp;SAPSCRIPT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Structure&nbsp;command&nbsp;to&nbsp;define&nbsp;wworkpaper&nbsp;so&nbsp;the&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;individual&nbsp;fields&nbsp;can&nbsp;be&nbsp;addressed.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;DATA&nbsp;STRUCTURES&nbsp;&nbsp;&nbsp;&nbsp;See&nbsp;form&nbsp;DATA_IMPORT&nbsp;INCLUDE&nbsp;RIPRID01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*$*$&nbsp;-&nbsp;&nbsp;&nbsp;P&nbsp;&nbsp;R&nbsp;&nbsp;I&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;T&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&nbsp;&nbsp;A&nbsp;&nbsp;P&nbsp;&nbsp;E&nbsp;&nbsp;R<br/>
</div>
<div class="code">
FORM print_paper.                      " This form name must be used !!!<br/>
</div>
<div class="codeComment">
*$*$&nbsp;-&nbsp;&nbsp;STARTED&nbsp;BY&nbsp;EXTERNAL&nbsp;PERFORM<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;order_data_import.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;See&nbsp;INCLUDE&nbsp;RIPRIf02<br/>
&nbsp;&nbsp;PERFORM&nbsp;main_print.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Print&nbsp;the&nbsp;PAPER&nbsp;now<br/>
ENDFORM.                    "PRINT_PAPER<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORM&nbsp;PRINT_PAPER_PDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Main&nbsp;driving&nbsp;Form&nbsp;behind&nbsp;the&nbsp;Printing&nbsp;of&nbsp;Papers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All&nbsp;information&nbsp;is&nbsp;imported&nbsp;from&nbsp;MEMORY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;FORM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name&nbsp;of&nbsp;SAPSCRIPT&nbsp;form&nbsp;to&nbsp;use.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;WWORKPAPER&nbsp;&nbsp;Print&nbsp;options&nbsp;for&nbsp;SAPSCRIPT.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Structure&nbsp;command&nbsp;to&nbsp;define&nbsp;wworkpaper&nbsp;so&nbsp;the&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;individual&nbsp;fields&nbsp;can&nbsp;be&nbsp;addressed.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;DATA&nbsp;STRUCTURES&nbsp;&nbsp;&nbsp;&nbsp;See&nbsp;form&nbsp;ORDER_DATA_IMPORT&nbsp;INCLUDE&nbsp;RIPRIF02&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM print_paper_pdf.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;order_data_import.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;See&nbsp;INCLUDE&nbsp;RIPRIF02<br/>
&nbsp;&nbsp;PERFORM&nbsp;main_print_pdf.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Print&nbsp;the&nbsp;PAPER&nbsp;now<br/>
ENDFORM.                    "PRINT_PAPER_PDF<br/>
<br/>
</div>
<div class="codeComment">
*$*$&nbsp;MAIN&nbsp;PRINT&nbsp;SECTION&nbsp;CONTROLLED&nbsp;HERE................................<br/>
*...&nbsp;If&nbsp;you&nbsp;are&nbsp;making&nbsp;changes&nbsp;to&nbsp;Print&nbsp;ABAPS,&nbsp;(Naturally&nbsp;a&nbsp;copied<br/>
*...&nbsp;version)&nbsp;here&nbsp;is&nbsp;the&nbsp;place&nbsp;you&nbsp;can&nbsp;alter&nbsp;the&nbsp;logic&nbsp;and<br/>
*...&nbsp;and&nbsp;data&nbsp;supplied&nbsp;to&nbsp;the&nbsp;form.&nbsp;&nbsp;&nbsp;You&nbsp;should&nbsp;not&nbsp;alter&nbsp;logic<br/>
*...&nbsp;before&nbsp;this&nbsp;point&nbsp;if&nbsp;you&nbsp;wish&nbsp;it&nbsp;to&nbsp;operate&nbsp;successfully<br/>
*...&nbsp;with&nbsp;the&nbsp;standard&nbsp;transactions.&nbsp;Form&nbsp;PRINT_PAPER&nbsp;must&nbsp;exist&nbsp;!!<br/>
*...&nbsp;However&nbsp;if&nbsp;you&nbsp;wish&nbsp;the&nbsp;PRINT&nbsp;LOG&nbsp;to&nbsp;work&nbsp;you&nbsp;must&nbsp;take<br/>
*...&nbsp;care&nbsp;to&nbsp;make&nbsp;sure&nbsp;the&nbsp;LOG&nbsp;records&nbsp;are&nbsp;written&nbsp;to&nbsp;PMPL.<br/>
*......................................................................<br/>
<br/>
</div>
<div class="code">
FORM main_print.<br/>
</div>
<div class="codeComment">
*...&nbsp;Workpaper&nbsp;is&nbsp;controlled&nbsp;at&nbsp;a&nbsp;HEADER&nbsp;LEVEL&nbsp;&nbsp;(ORDERS)<br/>
*...&nbsp;ONLY&nbsp;THOSE&nbsp;OPERATIONS&nbsp;WITH&nbsp;A&nbsp;VALID&nbsp;CONTROL&nbsp;KEY&nbsp;FOR&nbsp;PRINT&nbsp;will<br/>
*...&nbsp;be&nbsp;LISTED.<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:&nbsp;lf_formname&nbsp;TYPE&nbsp;tdsfname.<br/>
&nbsp;&nbsp;DATA:&nbsp;lf_fm_name&nbsp;TYPE&nbsp;rs38l_fnam.<br/>
<br/>
&nbsp;&nbsp;lf_formname&nbsp;=&nbsp;'ZFPM001'.<br/>
&nbsp;&nbsp;"调用SMARTFORM程序生成函数.<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SSF_FUNCTION_MODULE_NAME'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lf_formname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fm_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lf_fm_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_form&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;no_function_module&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_output_info&nbsp;TYPE&nbsp;ssfcrescl.<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_control&nbsp;TYPE&nbsp;ssfctrlop.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_out_option&nbsp;TYPE&nbsp;ssfcompop.<br/>
<br/>
&nbsp;&nbsp;ls_out_option-tddest&nbsp;=&nbsp;'LP01'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"指定默认打印机名，一般为四位字符<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;ls_out_option-tdnoprev&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"隐藏报表预览功能，默认为空<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;device&nbsp;&lt;&gt;&nbsp;'PREVIEW'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_out_option-tdimmed&nbsp;=&nbsp;'X'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"是否勾选快速打印项<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_control-no_dialog&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_control-preview&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_control-no_dialog&nbsp;=&nbsp;'X'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ls_out_option-tdarmod&nbsp;=&nbsp;1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"确认本地设置是否有效，必选项<br/>
</div>
<div class="codeComment">
*不显示对话框直接预览<br/>
<br/>
<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_get_data.<br/>
<br/>
<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;lf_fm_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ARCHIVE_INDEX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ARCHIVE_INDEX_TAB&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ARCHIVE_PARAMETERS&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;control_parameters&nbsp;=&nbsp;ls_control<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAIL_APPL_OBJ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAIL_RECIPIENT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAIL_SENDER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output_options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_out_option<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_settings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;''<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_head<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOCUMENT_OUTPUT_INFO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;job_output_info&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_output_info<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JOB_OUTPUT_OPTIONS&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;itab1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_tab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;formatting_error&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;internal_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;send_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_canceled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;TEXT-001&nbsp;TYPE&nbsp;'S'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
</div>
<div class="code">
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
ENDFORM.                    "MAIN_PRINT<br/>
<br/>
</div>
<div class="codeComment">
*$*$&nbsp;MAIN&nbsp;PRINT&nbsp;PDF&nbsp;SECTION&nbsp;CONTROLLED&nbsp;HERE................................<br/>
*...&nbsp;If&nbsp;you&nbsp;are&nbsp;making&nbsp;changes&nbsp;to&nbsp;Print&nbsp;ABAPS,&nbsp;(Naturally&nbsp;a&nbsp;copied<br/>
*...&nbsp;version)&nbsp;here&nbsp;is&nbsp;the&nbsp;place&nbsp;you&nbsp;can&nbsp;alter&nbsp;the&nbsp;logic&nbsp;and<br/>
*...&nbsp;and&nbsp;data&nbsp;supplied&nbsp;to&nbsp;the&nbsp;form.&nbsp;&nbsp;&nbsp;You&nbsp;should&nbsp;not&nbsp;alter&nbsp;logic<br/>
*...&nbsp;before&nbsp;this&nbsp;point&nbsp;if&nbsp;you&nbsp;wish&nbsp;it&nbsp;to&nbsp;operate&nbsp;successfully<br/>
*...&nbsp;with&nbsp;the&nbsp;standard&nbsp;transactions<br/>
*...&nbsp;However&nbsp;if&nbsp;you&nbsp;wish&nbsp;the&nbsp;PRINT&nbsp;LOG&nbsp;to&nbsp;work&nbsp;you&nbsp;must&nbsp;take<br/>
*...&nbsp;care&nbsp;to&nbsp;make&nbsp;sure&nbsp;the&nbsp;LOG&nbsp;records&nbsp;are&nbsp;written&nbsp;to&nbsp;PMPP.<br/>
*......................................................................<br/>
</div>
<div class="code">
FORM main_print_pdf.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;set_gv_arc_type_aufk&nbsp;USING&nbsp;caufvd-auart.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"n766146<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;open_form_pdf&nbsp;&nbsp;USING&nbsp;gv_arc_type_aufk&nbsp;&nbsp;"Archive&nbsp;link&nbsp;for&nbsp;order<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;caufvd-aufnr"order&nbsp;number&nbsp;as&nbsp;key<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"New&nbsp;form&nbsp;for&nbsp;each&nbsp;Order<br/>
&nbsp;&nbsp;PERFORM&nbsp;lock_and_set&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Enque&nbsp;and&nbsp;determine&nbsp;copy&nbsp;number<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USING&nbsp;c_header_order.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;open&nbsp;for&nbsp;Header&nbsp;level<br/>
&nbsp;&nbsp;PERFORM&nbsp;set_title.<br/>
&nbsp;&nbsp;PERFORM&nbsp;title_page_pdf..<br/>
&nbsp;&nbsp;PERFORM&nbsp;read_order_text_tables.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Read&nbsp;tables&nbsp;for&nbsp;CAUFVD<br/>
&nbsp;&nbsp;PERFORM&nbsp;order_header_detail_pdf.&nbsp;"&nbsp;Now&nbsp;print&nbsp;the&nbsp;order&nbsp;header&nbsp;see&nbsp;f02<br/>
&nbsp;&nbsp;PERFORM&nbsp;permits_pdf&nbsp;USING&nbsp;caufvd-objnr.&nbsp;&nbsp;"special&nbsp;permits<br/>
&nbsp;&nbsp;PERFORM&nbsp;partner_details_pdf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;prints&nbsp;partner&nbsp;details<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES&nbsp;order_ihpad_tab.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"<br/>
&nbsp;&nbsp;PERFORM&nbsp;tech_object_partner_pdf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;partner&nbsp;address&nbsp;equi&nbsp;/&nbsp;F.Locat<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USING&nbsp;caufvd-equnr&nbsp;&nbsp;caufvd-tplnr.<br/>
&nbsp;&nbsp;PERFORM&nbsp;print_classification_pdf&nbsp;USING&nbsp;caufvd-equnr&nbsp;'EQUI'&nbsp;'EQUNR'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;print_classification_pdf&nbsp;USING&nbsp;caufvd-tplnr&nbsp;'IFLOT'&nbsp;'TPLNR'.<br/>
&nbsp;&nbsp;PERFORM&nbsp;order_operations_pdf.&nbsp;&nbsp;"&nbsp;Operation&nbsp;details&nbsp;and&nbsp;reservations<br/>
&nbsp;&nbsp;PERFORM&nbsp;object_list_pdf&nbsp;USING&nbsp;yes.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;object&nbsp;list&nbsp;with&nbsp;new&nbsp;page&nbsp;true<br/>
&nbsp;&nbsp;PERFORM&nbsp;print_pdf.<br/>
&nbsp;&nbsp;PERFORM&nbsp;close_form_pdf.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Close&nbsp;the&nbsp;form.<br/>
&nbsp;&nbsp;PERFORM&nbsp;unlock_and_log.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Dequeue&nbsp;and&nbsp;Log&nbsp;print<br/>
<br/>
ENDFORM.                    "MAIN_PRINT_PDF<br/>
</div>
<div class="codeComment">
*$*$&nbsp;&nbsp;&nbsp;F&nbsp;O&nbsp;R&nbsp;M&nbsp;&nbsp;&nbsp;&nbsp;R&nbsp;O&nbsp;U&nbsp;T&nbsp;I&nbsp;N&nbsp;E&nbsp;S&nbsp;-------------------------------------*<br/>
*...&nbsp;&nbsp;&nbsp;Includes&nbsp;for&nbsp;General&nbsp;and&nbsp;Sepcific&nbsp;form&nbsp;routines<br/>
</div>
<div class="code">
INCLUDE riprif01.                      " General PRINT routines<br/>
INCLUDE riprif02.                      " General PRINT routines ORDERS<br/>
INCLUDE riprif1a.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;ORDER_OPERATIONS_PDF<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Notification&nbsp;Details&nbsp;are&nbsp;printed&nbsp;here.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Text&nbsp;ELEMENT&nbsp;used&nbsp;&nbsp;&nbsp;&nbsp;OPERATION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM order_operations_pdf.<br/>
&nbsp;&nbsp;iafvgd&nbsp;=&nbsp;space.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;iafvgd&nbsp;WHERE&nbsp;aufpl&nbsp;=&nbsp;caufvd-aufpl.&nbsp;"loop&nbsp;on&nbsp;operations<br/>
&nbsp;&nbsp;&nbsp;&nbsp;afvgd&nbsp;=&nbsp;iafvgd.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Set&nbsp;workarea&nbsp;for&nbsp;SAPSCRIPT<br/>
</div>
<div class="codeComment">
*...&nbsp;for&nbsp;each&nbsp;operation<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;check_print_status&nbsp;USING&nbsp;afvgd-objnr<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wworkpaper-pm_delta_p<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;rc&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;op_entries&nbsp;&gt;&nbsp;0.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;single&nbsp;operation&nbsp;print&nbsp;active<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;op_print_tab&nbsp;WHERE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flg_sel&nbsp;=&nbsp;'X'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;vornr&nbsp;&nbsp;&nbsp;=&nbsp;afvgd-vornr&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;was&nbsp;the&nbsp;operation&nbsp;selected<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;&nbsp;uvorn&nbsp;&nbsp;&nbsp;=&nbsp;afvgd-uvorn.&nbsp;&nbsp;&nbsp;"&nbsp;for&nbsp;print&nbsp;???<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;syst-subrc&nbsp;=&nbsp;0.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;should&nbsp;this&nbsp;op&nbsp;be&nbsp;printed<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;read_op_text_tables.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"operation&nbsp;text&nbsp;tables<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;T430&nbsp;is&nbsp;now&nbsp;available<br/>
&nbsp;&nbsp;&nbsp;&nbsp;iafvgd-flg_frd&nbsp;=&nbsp;afvgd-flg_frd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHECK&nbsp;t430-vrgd&nbsp;=&nbsp;yes.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;jump&nbsp;to&nbsp;next&nbsp;operation&nbsp;when&nbsp;oper&nbsp;not&nbsp;marked&nbsp;for&nbsp;print<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_afvgd&nbsp;=&nbsp;iafvgd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_afvgd&nbsp;TO&nbsp;gt_afvgd.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_rcr01-arbid&nbsp;=&nbsp;afvgd-arbid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_rcr01-arbpl&nbsp;=&nbsp;rcr01-arbpl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_rcr01-ktext&nbsp;=&nbsp;rcr01-ktext.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_rcr01-werks&nbsp;=&nbsp;rcr01-werks.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gs_rcr01&nbsp;TO&nbsp;gt_rcr01.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;gs_rcr01.<br/>
</div>
<div class="codeComment">
*...&nbsp;check&nbsp;that&nbsp;the&nbsp;operation&nbsp;should&nbsp;be&nbsp;printed&nbsp;based&nbsp;on&nbsp;the<br/>
*...&nbsp;control&nbsp;key.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;print_operation_text_pdf.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"&nbsp;Longtext&nbsp;to&nbsp;operation<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
ENDFORM.                    "ORDER_OPERATIONS_PDF<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;PRINT_PDF<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM print_pdf .<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_sess_tzone&nbsp;&nbsp;TYPE&nbsp;&nbsp;ttzz-tzone.<br/>
<br/>
</div>
<div class="codeComment">
*---&nbsp;Get&nbsp;time&nbsp;zone&nbsp;value&nbsp;for&nbsp;the&nbsp;user<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;get_timezone&nbsp;CHANGING&nbsp;lv_sess_tzone.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;NOT&nbsp;gv_fm_name&nbsp;IS&nbsp;INITIAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;gv_fm_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/1bcdwb/docparams&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_docparams&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"1938408<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_header&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;caufvd<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object_connection&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;riwo1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_operation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_afvgd<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;task_list&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;rcr01<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title_page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_wiprt<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;order_type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;t003p<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;priority&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;t356_t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;local_account&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gs_iloa<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object_list&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_ripw0<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;revision&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;t352r<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;permits&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_t357g<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;permit_segment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_ihgns<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;partner_info&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_ihpad1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class_object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_api_kssk<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class_value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_api_val<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;notification_head_longtext&nbsp;=&nbsp;lt_lines1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;material_longtext&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_opr_text<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;work_center&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gt_rcr01<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sess_tzone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_sess_tzone<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/1bcdwb/formoutput&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gv_fpformoutput<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;usage_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;system_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;internal_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;ID&nbsp;sy-msgid&nbsp;TYPE&nbsp;sy-msgty&nbsp;NUMBER&nbsp;sy-msgno<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WITH&nbsp;sy-msgv1&nbsp;sy-msgv2&nbsp;sy-msgv3&nbsp;sy-msgv4.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;REFRESH&nbsp;:&nbsp;gt_tline1,gt_qkat_head,&nbsp;gt_qkat,&nbsp;lt_lines1,gt_afvgd,gt_ihpad1,gt_opr_text,gt_api_val,gt_ihgns,gt_ripw0,gt_api_kssk,gt_rcr01,gt_t357g.<br/>
&nbsp;&nbsp;CLEAR&nbsp;:&nbsp;gs_wiprt,&nbsp;gs_viqmel,&nbsp;gs_tq80_t,&nbsp;gs_to24i,gs_wqmfe,&nbsp;gs_makt,gs_iloa.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gv_pdf_paper_selected&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;BADI&nbsp;gb_riprif01_pdf_paper_print-&gt;set_pdf_paper_result<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_pdfdata&nbsp;&nbsp;&nbsp;=&nbsp;gv_fpformoutput-pdf<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iv_form_name&nbsp;=&nbsp;t390_t-papertext.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.                    " PRINT_PDF<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;get_timezone<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;Check&nbsp;the&nbsp;time&nbsp;zone&nbsp;switch&nbsp;and&nbsp;customization&nbsp;status&nbsp;and&nbsp;get&nbsp;the&nbsp;reference&nbsp;time&nbsp;zone<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;CV_SESS_TZONE&nbsp;&nbsp;reference&nbsp;time&nbsp;zone&nbsp;for&nbsp;the&nbsp;user<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM get_timezone CHANGING cv_sess_tzone TYPE ttzz-tzone.<br/>
</div>
<div class="codeComment">
*---&nbsp;declaration&nbsp;for&nbsp;time&nbsp;zone&nbsp;support<br/>
</div>
<div class="code">
&nbsp;&nbsp;TYPE-POOLS:&nbsp;tzs1.<br/>
&nbsp;&nbsp;DATA:&nbsp;gb_badi_time_zone&nbsp;&nbsp;&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;badi_eam_tz_generic_core,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_time_zone_active&nbsp;TYPE&nbsp;tz_d_active.<br/>
<br/>
&nbsp;&nbsp;BREAK-POINT&nbsp;ID&nbsp;eam_tzs_gen.<br/>
</div>
<div class="codeComment">
*---&nbsp;at&nbsp;first&nbsp;check&nbsp;if&nbsp;BAdI&nbsp;is&nbsp;implemented<br/>
</div>
<div class="code">
&nbsp;&nbsp;TRY.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GET&nbsp;BADI&nbsp;gb_badi_time_zone.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CATCH&nbsp;cx_badi_not_implemented.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gv_time_zone_active&nbsp;=&nbsp;abap_false.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDTRY.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;cl_badi_query=&gt;number_of_implementations(&nbsp;gb_badi_time_zone&nbsp;)&nbsp;=&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gv_time_zone_active&nbsp;=&nbsp;abap_false.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
**---&nbsp;check&nbsp;if&nbsp;customizing&nbsp;is&nbsp;active<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'TZS1_CHECK_CUSTOMIZING'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is_application&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;tzs1_appl-pm_woc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;es_activated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;gv_time_zone_active<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;application_not_valid&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gv_time_zone_active&nbsp;=&nbsp;abap_false.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*---&nbsp;For&nbsp;time&nbsp;zone&nbsp;support,&nbsp;get&nbsp;the&nbsp;reference&nbsp;time&nbsp;zone&nbsp;of&nbsp;the&nbsp;user<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;gv_time_zone_active&nbsp;=&nbsp;abap_true.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'TZS1_GET_SESSION_TZON'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_sess_tzone&nbsp;=&nbsp;cv_sess_tzone.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM.                    "get_timezone<br/>
</div>
<div class="codeComment">
*.......................................................................<br/>
*$*$&nbsp;G&nbsp;E&nbsp;N&nbsp;E&nbsp;R&nbsp;A&nbsp;L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F&nbsp;O&nbsp;R&nbsp;M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;R&nbsp;O&nbsp;U&nbsp;T&nbsp;I&nbsp;N&nbsp;E&nbsp;S&nbsp;....................*<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;Form&nbsp;frm_get_data<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_get_data .<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;给表头赋值<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR&nbsp;gs_head.<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;name1&nbsp;FROM&nbsp;t001w&nbsp;WHERE&nbsp;t001w~werks&nbsp;=&nbsp;@caufvd-swerk&nbsp;INTO&nbsp;@gs_head-name1&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单位<br/>
&nbsp;&nbsp;gs_head-equnr&nbsp;=&nbsp;caufvd-equnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设备号<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;eqktx&nbsp;FROM&nbsp;eqkt&nbsp;WHERE&nbsp;spras&nbsp;=&nbsp;@sy-langu&nbsp;AND&nbsp;eqkt~equnr&nbsp;=&nbsp;@caufvd-equnr&nbsp;INTO&nbsp;@gs_head-eqktx.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"设备描述<br/>
&nbsp;&nbsp;gs_head-aufnr&nbsp;=&nbsp;caufvd-aufnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工单<br/>
&nbsp;&nbsp;gs_head-aufnr_txt&nbsp;=&nbsp;caufvd-ktext.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工单描述<br/>
&nbsp;&nbsp;gs_head-qmnum&nbsp;=&nbsp;caufvd-qmnum.&nbsp;"通知单号<br/>
&nbsp;&nbsp;gs_head-tplnr&nbsp;=&nbsp;caufvd-tplnr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"功能位置编码<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;pltxt&nbsp;FROM&nbsp;iflotx&nbsp;WHERE&nbsp;iflotx~spras&nbsp;=&nbsp;@sy-langu&nbsp;AND&nbsp;iflotx~tplnr&nbsp;=&nbsp;@caufvd-tplnr&nbsp;INTO&nbsp;@gs_head-pltxt.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"功能位置描述<br/>
&nbsp;&nbsp;gs_head-auart&nbsp;=&nbsp;caufvd-auart.&nbsp;"工单类型<br/>
&nbsp;&nbsp;gs_head-ilart&nbsp;=&nbsp;caufvd-ilart.&nbsp;"pm作业类型<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ilatx&nbsp;FROM&nbsp;t353i_t&nbsp;WHERE&nbsp;t353i_t~spras&nbsp;=&nbsp;@sy-langu&nbsp;AND&nbsp;t353i_t~ilart&nbsp;=&nbsp;@caufvd-ilart&nbsp;INTO&nbsp;@gs_head-ilatx.&nbsp;"pm作业类型描述<br/>
&nbsp;&nbsp;gs_head-gstrp&nbsp;=&nbsp;caufvd-gstrp.&nbsp;"计划开始时间<br/>
&nbsp;&nbsp;gs_head-gltrp&nbsp;=&nbsp;caufvd-gltrs.&nbsp;"计划结束时间<br/>
<br/>
&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;crtx~ktext<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;aufk<br/>
&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;crhd<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;aufk~vaplz&nbsp;=&nbsp;crhd~arbpl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;aufk~wawrk&nbsp;=&nbsp;crhd~werks<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;aufk~aufnr&nbsp;=&nbsp;@caufvd-aufnr<br/>
&nbsp;&nbsp;&nbsp;INNER&nbsp;JOIN&nbsp;crtx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;crhd~objty&nbsp;=&nbsp;crtx~objty<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;crhd~objid&nbsp;=&nbsp;crtx~objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;crtx~spras&nbsp;=&nbsp;@sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;@gs_head-whgzzx_txt.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"维护工作中心<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;如果工单长文本为空，则直接获取工单描述<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lv_name1&nbsp;&nbsp;&nbsp;TYPE&nbsp;thead-tdname.<br/>
&nbsp;&nbsp;DATA&nbsp;lv_name2&nbsp;&nbsp;&nbsp;TYPE&nbsp;thead-tdname.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;lv_name1,lv_name2.<br/>
<br/>
&nbsp;&nbsp;lv_name1&nbsp;=&nbsp;caufvd-aufnr.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_ltxt&nbsp;USING&nbsp;'LTXT'&nbsp;lv_name1&nbsp;'AUFK'&nbsp;CHANGING&nbsp;gs_head-aufnr_detl.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;gs_head-aufnr_detl&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gs_head-aufnr_detl&nbsp;=&nbsp;gs_head-aufnr_txt.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;获取通知单长文本<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;lv_name2&nbsp;=&nbsp;caufvd-qmnum.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_get_ltxt&nbsp;USING&nbsp;'LTXT'&nbsp;lv_name2&nbsp;'QMEL'&nbsp;CHANGING&nbsp;gs_head-qmnum_detl.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;获取工序数据<br/>
*----------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;lw_iafvgd&nbsp;LIKE&nbsp;iafvgd.<br/>
<br/>
</div>
<div class="codeComment">
*----------------------------------------------------------------------*<br/>
*&nbsp;默认工序，安全检查<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
CLEAR gt_tab.<br/>
<br/>
&nbsp;&nbsp;gw_tab-vornr&nbsp;=&nbsp;'0000'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工序单号<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;gw_tab-steus&nbsp;=&nbsp;lw_iafvgd-steus.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"控制码<br/>
</div>
<div class="code">
&nbsp;&nbsp;gw_tab-ltxa1&nbsp;=&nbsp;'安全检查'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工序描述<br/>
&nbsp;&nbsp;APPEND&nbsp;gw_tab&nbsp;TO&nbsp;gt_tab.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;iafvgd&nbsp;INTO&nbsp;lw_iafvgd.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;iafvgd&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;gw_tab-vornr&nbsp;=&nbsp;lw_iafvgd-vornr.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工序单号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gw_tab-steus&nbsp;=&nbsp;lw_iafvgd-steus.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"控制码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;gw_tab-ltxa1&nbsp;=&nbsp;lw_iafvgd-ltxa1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"工序描述<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lw_iafvgd-steus&nbsp;=&nbsp;'PM01'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;ktext&nbsp;FROM&nbsp;cskt&nbsp;WHERE&nbsp;spras&nbsp;=&nbsp;@sy-langu&nbsp;AND&nbsp;cskt~kostl&nbsp;=&nbsp;@lw_iafvgd-arbpl&nbsp;INTO&nbsp;@gw_tab-sgdw.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSEIF&nbsp;lw_iafvgd-steus&nbsp;=&nbsp;'PM03'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT&nbsp;SINGLE&nbsp;name1&nbsp;FROM&nbsp;lfa1&nbsp;WHERE&nbsp;spras&nbsp;=&nbsp;@sy-langu&nbsp;AND&nbsp;lfa1~lifnr&nbsp;=&nbsp;@lw_iafvgd-lifnr&nbsp;INTO&nbsp;@gw_tab-sgdw.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;gw_tab&nbsp;TO&nbsp;gt_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;APPEND&nbsp;INITIAL&nbsp;LINE&nbsp;TO<br/>
<br/>
*&nbsp;&nbsp;SELECT&nbsp;t001w~name1,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"单位<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aufnr,<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;auart,&nbsp;"工单类型<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr,&nbsp;"设备编号<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext,&nbsp;"工单描述<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qmnum,&nbsp;"通知单号<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr,&nbsp;"功能位置描述编码<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;innam,&nbsp;"设备名称暂定<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ilart,&nbsp;"PM作业类型<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gstrp,&nbsp;"计划开始时间<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gltrs&nbsp;"计划结束时间<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;gs_head<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;caufvd<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;LEFT&nbsp;JOIN&nbsp;t001w<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON&nbsp;t001w~werks&nbsp;=&nbsp;caufvd~swerk.<br/>
<br/>
</div>
<div class="code">
ENDFORM.<br/>
<br/>
DATA: lp_id     TYPE thead-tdid,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_name&nbsp;&nbsp;&nbsp;TYPE&nbsp;thead-tdname,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_object&nbsp;TYPE&nbsp;thead-tdobject,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lp_text&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;200.<br/>
<br/>
FORM frm_get_ltxt USING lp_id lp_name lp_object CHANGING lp_text.<br/>
&nbsp;&nbsp;DATA&nbsp;lt_itab&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;tline.<br/>
&nbsp;&nbsp;DATA&nbsp;lw_itab&nbsp;TYPE&nbsp;tline.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;获取长文本数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'READ_TEXT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLIENT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;SY-MANDT<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lp_id<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;sy-langu<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lp_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lp_object<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ARCHIVE_HANDLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;0<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOCAL_CAT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;IMPORTING<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HEADER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OLD_LINE_COUNTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lines&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lt_itab<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;language&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_found&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;object&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reference_check&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;6<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_access_to_archive&nbsp;=&nbsp;7<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;8.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;&lt;&gt;&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;lp_text&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;lt_itab&nbsp;INTO&nbsp;lw_itab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;lw_itab-tdline&nbsp;CP&nbsp;'*UTC*'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELETE&nbsp;lt_itab&nbsp;INDEX&nbsp;sy-tabix.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;lp_text&nbsp;cl_abap_char_utilities=&gt;newline&nbsp;lw_itab-tdline&nbsp;INTO&nbsp;lp_text.&nbsp;"将长文本换行输出到ALV<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*Messages<br/>
*----------------------------------------------------------<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;ID<br/>
*001&nbsp;&nbsp;&nbsp;PDF&nbsp;车间加工单&nbsp;&amp;1&nbsp;&amp;2：归档时出错<br/>
*002&nbsp;&nbsp;&nbsp;PDF&nbsp;车间加工单&nbsp;&amp;1&nbsp;&amp;2：传真时出错<br/>
*650&nbsp;&nbsp;&nbsp;从内存装入错：程序删除<br/>
*<br/>
*&nbsp;Message&nbsp;class:&nbsp;IW<br/>
*264&nbsp;&nbsp;&nbsp;打印日志已满，因此无法打印车间加工单&nbsp;&amp;<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>