<table class="outerTable">
<tr>
<td><h2>Table: ZSS_ASSET_OUT</h2>
<h3>Description: 资产价值(原值+净值+折旧方法+已提折旧)</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>AFABE</td>
<td>4</td>
<td>&nbsp;</td>
<td>AFABE_D</td>
<td>AFABE</td>
<td>NUMC</td>
<td>2</td>
<td>&nbsp;</td>
<td>实际折旧范围</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>AFASL</td>
<td>7</td>
<td>&nbsp;</td>
<td>AFASL</td>
<td>AFASL</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>折旧码</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AFATXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>AFATXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>外部折旧码名称</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ANLN1</td>
<td>2</td>
<td>&nbsp;</td>
<td>ANLN1</td>
<td>ANLN1</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>主要资产编号</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ANLN2</td>
<td>3</td>
<td>&nbsp;</td>
<td>ANLN2</td>
<td>ANLN2</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>资产子编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BUKRS</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZJZ</td>
<td>6</td>
<td>&nbsp;</td>
<td>KANSW</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>累积购置和生产费用</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZYTZJ</td>
<td>9</td>
<td>&nbsp;</td>
<td>NAFAG</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>记帐在当前年的正常折旧</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZYZ</td>
<td>5</td>
<td>&nbsp;</td>
<td>KANSW</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>累积购置和生产费用</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>