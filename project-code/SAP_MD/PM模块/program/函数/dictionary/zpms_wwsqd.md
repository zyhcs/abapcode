<table class="outerTable">
<tr>
<td><h2>Table: ZPMS_WWSQD</h2>
<h3>Description: OA获取ERP委外申请关联通知单数据表结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ARBPL</td>
<td>24</td>
<td>&nbsp;</td>
<td>ARBPL</td>
<td>ARBPL</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>工作中心</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>AUSBS</td>
<td>32</td>
<td>&nbsp;</td>
<td>AUSBS</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>故障结束（日期）</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AUSVN</td>
<td>31</td>
<td>&nbsp;</td>
<td>AUSVN</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>故障开始（日期）</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>AUZTB</td>
<td>34</td>
<td>&nbsp;</td>
<td>AUZTB</td>
<td>UHRZT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>故障结束（时间）</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>AUZTV</td>
<td>33</td>
<td>&nbsp;</td>
<td>AUZTV</td>
<td>UHRZT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>故障开始（时间）</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>EQTXT</td>
<td>18</td>
<td>&nbsp;</td>
<td>KTX01</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>技术对象描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>EQUNR</td>
<td>17</td>
<td>&nbsp;</td>
<td>EQUNR</td>
<td>EQUNR</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>设备号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ILOAN</td>
<td>5</td>
<td>&nbsp;</td>
<td>ILOAN</td>
<td>ILOAN</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>技术对象的位置和科目分配</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>INGRP</td>
<td>20</td>
<td>&nbsp;</td>
<td>INGRP</td>
<td>INGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户服务和工厂维护的计划员组</td>
</tr>
<tr class="cell">
<td>10</td>
<td>INNAM</td>
<td>21</td>
<td>&nbsp;</td>
<td>INNAM</td>
<td>TEXT18</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>维护计划员组的名称</td>
</tr>
<tr class="cell">
<td>11</td>
<td>IWERK</td>
<td>22</td>
<td>&nbsp;</td>
<td>IWERK</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>维护计划工厂</td>
</tr>
<tr class="cell">
<td>12</td>
<td>KTEXT</td>
<td>25</td>
<td>&nbsp;</td>
<td>CR_KTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>短描述</td>
</tr>
<tr class="cell">
<td>13</td>
<td>LTRMN</td>
<td>28</td>
<td>&nbsp;</td>
<td>LTRMN</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>要求的结束日期</td>
</tr>
<tr class="cell">
<td>14</td>
<td>LTRUR</td>
<td>29</td>
<td>&nbsp;</td>
<td>LTRUR</td>
<td>UZEIT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>要求的结束时间</td>
</tr>
<tr class="cell">
<td>15</td>
<td>MSAUS</td>
<td>30</td>
<td>&nbsp;</td>
<td>MSAUS</td>
<td>XFELD</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>停机指示器</td>
</tr>
<tr class="cell">
<td>16</td>
<td>MZEIT</td>
<td>37</td>
<td>&nbsp;</td>
<td>MZEIT</td>
<td>UHRZT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>通知时间</td>
</tr>
<tr class="cell">
<td>17</td>
<td>QMART</td>
<td>2</td>
<td>&nbsp;</td>
<td>QMART</td>
<td>QMART</td>
<td>CHAR</td>
<td>2</td>
<td>&nbsp;</td>
<td>通知类型</td>
</tr>
<tr class="cell">
<td>18</td>
<td>QMARTX</td>
<td>3</td>
<td>&nbsp;</td>
<td>QMARTX</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>通知类型文本</td>
</tr>
<tr class="cell">
<td>19</td>
<td>QMDAT</td>
<td>36</td>
<td>&nbsp;</td>
<td>QMDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>通知日期</td>
</tr>
<tr class="cell">
<td>20</td>
<td>QMNAM</td>
<td>35</td>
<td>&nbsp;</td>
<td>QMNAM</td>
<td>USNAM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>通知报告人的姓名</td>
</tr>
<tr class="cell">
<td>21</td>
<td>QMNUM</td>
<td>1</td>
<td>&nbsp;</td>
<td>QMNUM</td>
<td>QMNUM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>通知编号</td>
</tr>
<tr class="cell">
<td>22</td>
<td>QMTXT</td>
<td>4</td>
<td>&nbsp;</td>
<td>QMTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>短文本</td>
</tr>
<tr class="cell">
<td>23</td>
<td>STRMN</td>
<td>26</td>
<td>&nbsp;</td>
<td>STRMN</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>要求的开始日期</td>
</tr>
<tr class="cell">
<td>24</td>
<td>STRUR</td>
<td>27</td>
<td>&nbsp;</td>
<td>STRUR</td>
<td>UZEIT</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>要求的开始时间</td>
</tr>
<tr class="cell">
<td>25</td>
<td>TPLNR</td>
<td>6</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>26</td>
<td>TPLNR_01</td>
<td>7</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>27</td>
<td>TPLNR_02</td>
<td>9</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>28</td>
<td>TPLNR_03</td>
<td>11</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>29</td>
<td>TPLNR_04</td>
<td>13</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>30</td>
<td>TPLNR_05</td>
<td>15</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>31</td>
<td>ZJHGC</td>
<td>23</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>32</td>
<td>ZKTX01_01</td>
<td>8</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>33</td>
<td>ZKTX01_02</td>
<td>10</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>34</td>
<td>ZKTX01_03</td>
<td>12</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>35</td>
<td>ZKTX01_04</td>
<td>14</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>36</td>
<td>ZKTX01_05</td>
<td>16</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>37</td>
<td>ZXMCWB</td>
<td>19</td>
<td>&nbsp;</td>
<td>ZE_ZXMCWB</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>255</td>
<td>&nbsp;</td>
<td>文本行</td>
</tr>
</table>
</td>
</tr>
<br/>
<table class="outerTable">
<tr>
<td><h2>Fixed Domain Values </h2>
</td></tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Domain Name</th>
<th>Value Low</th>
<th>Value High</th>
<th>Text</th>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>X</td>
<td>&nbsp;</td>
<td>是</td>
</tr>
<tr class="cell">
<td>XFELD</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>否</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>
</table>