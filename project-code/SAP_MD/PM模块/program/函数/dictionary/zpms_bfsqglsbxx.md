<table class="outerTable">
<tr>
<td><h2>Table: ZPMS_BFSQGLSBXX</h2>
<h3>Description: 报废申请关联设备台账接口表类型</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ABCKZ</td>
<td>15</td>
<td>&nbsp;</td>
<td>ABCKZ</td>
<td>ABCKZ</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>技术对象的 ABC 标识</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ABCTX</td>
<td>16</td>
<td>&nbsp;</td>
<td>ABCTX</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>技术对象的 ABC 标识文本</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>AKTIV</td>
<td>30</td>
<td>&nbsp;</td>
<td>AKTIVD</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>资产资本化日期</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ANLKL</td>
<td>28</td>
<td>&nbsp;</td>
<td>ANLKL</td>
<td>ANLKL</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>资产类</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ANLNR</td>
<td>26</td>
<td>&nbsp;</td>
<td>ANLN1</td>
<td>ANLN1</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>主要资产编号</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>BEGRU</td>
<td>7</td>
<td>&nbsp;</td>
<td>IAUTG</td>
<td>IAUTG</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>特种设备</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>EARTX</td>
<td>6</td>
<td>&nbsp;</td>
<td>EARTX</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>对象文本文本</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>EQART</td>
<td>5</td>
<td>&nbsp;</td>
<td>EQART</td>
<td>EQART</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>技术对象类型</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>EQKTX</td>
<td>3</td>
<td>&nbsp;</td>
<td>KTX01</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>技术对象描述</td>
</tr>
<tr class="cell">
<td>10</td>
<td>EQUNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>EQUNR</td>
<td>EQUNR</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>设备号</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ERNAM</td>
<td>13</td>
<td>&nbsp;</td>
<td>BEBER</td>
<td>BEBER</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>厂区</td>
</tr>
<tr class="cell">
<td>12</td>
<td>FING</td>
<td>14</td>
<td>&nbsp;</td>
<td>NAME1</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称</td>
</tr>
<tr class="cell">
<td>13</td>
<td>GEWRK</td>
<td>21</td>
<td>&nbsp;</td>
<td>CR_OBJID</td>
<td>CR_OBJID</td>
<td>NUMC</td>
<td>8</td>
<td>&nbsp;</td>
<td>资源的对象标识</td>
</tr>
<tr class="cell">
<td>14</td>
<td>GEWRK_T</td>
<td>22</td>
<td>&nbsp;</td>
<td>GEWRK</td>
<td>ARBPL</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>维护任务的工作中心</td>
</tr>
<tr class="cell">
<td>15</td>
<td>HERST</td>
<td>9</td>
<td>&nbsp;</td>
<td>HERST</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>资产制造商</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ILOAN</td>
<td>41</td>
<td>&nbsp;</td>
<td>ILOAN</td>
<td>ILOAN</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>技术对象的位置和科目分配</td>
</tr>
<tr class="cell">
<td>17</td>
<td>INBDT</td>
<td>8</td>
<td>&nbsp;</td>
<td>ILOM_DATAB</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>技术对象的开始日期</td>
</tr>
<tr class="cell">
<td>18</td>
<td>INGRP</td>
<td>19</td>
<td>&nbsp;</td>
<td>INGRP</td>
<td>INGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户服务和工厂维护的计划员组</td>
</tr>
<tr class="cell">
<td>19</td>
<td>INNAM</td>
<td>20</td>
<td>&nbsp;</td>
<td>INNAM</td>
<td>TEXT18</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>维护计划员组的名称</td>
</tr>
<tr class="cell">
<td>20</td>
<td>IWERK</td>
<td>40</td>
<td>&nbsp;</td>
<td>IWERK</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>维护计划工厂</td>
</tr>
<tr class="cell">
<td>21</td>
<td>KOSTL</td>
<td>24</td>
<td>&nbsp;</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td>22</td>
<td>KTEXT</td>
<td>25</td>
<td>&nbsp;</td>
<td>KTEXT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>一般姓名</td>
</tr>
<tr class="cell">
<td>23</td>
<td>MEINS</td>
<td>37</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>24</td>
<td>MENGE</td>
<td>36</td>
<td>&nbsp;</td>
<td>AM_MENGE</td>
<td>MENG13V</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>数量</td>
</tr>
<tr class="cell">
<td>25</td>
<td>MSGRP</td>
<td>11</td>
<td>&nbsp;</td>
<td>RAUMNR</td>
<td>CHAR8</td>
<td>CHAR</td>
<td>8</td>
<td>&nbsp;</td>
<td>设备负责人</td>
</tr>
<tr class="cell">
<td>26</td>
<td>OBJNR</td>
<td>2</td>
<td>&nbsp;</td>
<td>J_OBJNR</td>
<td>J_OBJNR</td>
<td>CHAR</td>
<td>22</td>
<td>&nbsp;</td>
<td>对象号</td>
</tr>
<tr class="cell">
<td>27</td>
<td>PLTXT</td>
<td>18</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td>28</td>
<td>STAT</td>
<td>4</td>
<td>&nbsp;</td>
<td>J_STATUS</td>
<td>J_STATUS</td>
<td>CHAR</td>
<td>5</td>
<td>&nbsp;</td>
<td>对象状态</td>
</tr>
<tr class="cell">
<td>29</td>
<td>TPLNR</td>
<td>17</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td>30</td>
<td>TXK20</td>
<td>29</td>
<td>&nbsp;</td>
<td>TXT20_ANKT</td>
<td>TEXT20</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>资产类别名的短文本</td>
</tr>
<tr class="cell">
<td>31</td>
<td>TXT50</td>
<td>27</td>
<td>&nbsp;</td>
<td>TXT50</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>文本</td>
</tr>
<tr class="cell">
<td>32</td>
<td>TXT_GEWRK</td>
<td>23</td>
<td>&nbsp;</td>
<td>AP_KTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>工作中心短文本</td>
</tr>
<tr class="cell">
<td>33</td>
<td>TYPBZ</td>
<td>10</td>
<td>&nbsp;</td>
<td>TYPBZ</td>
<td>TYPBZ</td>
<td>CHAR</td>
<td>20</td>
<td>X</td>
<td>制造商型号</td>
</tr>
<tr class="cell">
<td>34</td>
<td>WERK</td>
<td>12</td>
<td>&nbsp;</td>
<td>WERKS_D</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>工厂</td>
</tr>
<tr class="cell">
<td>35</td>
<td>ZJZ</td>
<td>35</td>
<td>&nbsp;</td>
<td>WRTMA</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>手工净值税价值</td>
</tr>
<tr class="cell">
<td>36</td>
<td>ZKOSTL</td>
<td>32</td>
<td>&nbsp;</td>
<td>KOSTL</td>
<td>KOSTL</td>
<td>CHAR</td>
<td>10</td>
<td>&nbsp;</td>
<td>成本中心</td>
</tr>
<tr class="cell">
<td>37</td>
<td>ZLTEXT</td>
<td>33</td>
<td>&nbsp;</td>
<td>LTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>长文本</td>
</tr>
<tr class="cell">
<td>38</td>
<td>ZUGDT</td>
<td>31</td>
<td>&nbsp;</td>
<td>DZUGDAT</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>第一次记帐资产的资产起用日</td>
</tr>
<tr class="cell">
<td>39</td>
<td>ZYTZJ</td>
<td>39</td>
<td>&nbsp;</td>
<td>WRTMA</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>手工净值税价值</td>
</tr>
<tr class="cell">
<td>40</td>
<td>ZYZ</td>
<td>34</td>
<td>&nbsp;</td>
<td>WRTMA</td>
<td>AFLE13D2O16S_TO_23D2O31S</td>
<td>CURR</td>
<td>23</td>
<td>&nbsp;</td>
<td>手工净值税价值</td>
</tr>
<tr class="cell">
<td>41</td>
<td>ZZJFF</td>
<td>38</td>
<td>&nbsp;</td>
<td>AFATXT</td>
<td>TEXT50</td>
<td>CHAR</td>
<td>50</td>
<td>X</td>
<td>外部折旧码名称</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>