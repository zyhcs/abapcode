<table class="outerTable">
<tr>
<td><h2>Table: ZSS_ASSET</h2>
<h3>Description: 资产传入结构</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>ANLN1</td>
<td>2</td>
<td>&nbsp;</td>
<td>ANLN1</td>
<td>ANLN1</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>主要资产编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ANLN2</td>
<td>3</td>
<td>&nbsp;</td>
<td>ANLN2</td>
<td>ANLN2</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>资产子编号</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>BUKRS</td>
<td>1</td>
<td>&nbsp;</td>
<td>BUKRS</td>
<td>BUKRS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>公司代码</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>