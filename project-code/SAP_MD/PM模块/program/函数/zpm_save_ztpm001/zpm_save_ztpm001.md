<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZPM_SAVE_ZTPM001</h2>
<h3> Description: 保存设备加油记录表</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION zpm_save_ztpm001.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"更新函数模块：<br/>
*"<br/>
*"*"本地接口：<br/>
*"  TABLES<br/>
*"      IT_DATA STRUCTURE  ZTPM001<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zpm_save_ztpm001.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;CHECK&nbsp;it_data[]&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;it_data&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;DELETE&nbsp;FROM&nbsp;ztpm001&nbsp;WHERE&nbsp;jyjlid&nbsp;=&nbsp;it_data-jyjlid.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;it_data.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;it_data-zz_sel&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MODIFY&nbsp;it_data.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;INSERT&nbsp;ztpm001&nbsp;FROM&nbsp;TABLE&nbsp;it_data[]&nbsp;.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>