<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for function ZIF_PM022_NARR_SYNC</h2>
<h3> Description: OA通知单审批状态回传ERP接口函数</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="code">
FUNCTION ZIF_PM022_NARR_SYNC.<br/>
</div>
<div class="codeComment">
*"----------------------------------------------------------------------<br/>
*"*"本地接口：<br/>
*"  IMPORTING<br/>
*"     VALUE(IV_QMNUM) TYPE  QMNUM<br/>
*"     VALUE(IV_RESULT) TYPE  C<br/>
*"     VALUE(IV_GUID) TYPE  SYSUUID_C32<br/>
*"  EXPORTING<br/>
*"     REFERENCE(OT_TYPE) TYPE  BAPI_MTYPE<br/>
*"     REFERENCE(OT_MESSAGE) TYPE  BAPI_MSG<br/>
*"----------------------------------------------------------------------<br/>
<div class="codeComment">*       <a href="global-zif_pm022_narr_sync.html">Global data declarations</a></div><br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA:IV_NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LIKE&nbsp;BAPI2080_NOTHDRE-NOTIF_NO,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LS_USR_STATUS&nbsp;LIKE&nbsp;BAPI2080_NOTUSRSTATI,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_RETURN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;TABLE&nbsp;OF&nbsp;BAPIRET2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OS_RETURN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;BAPIRET2,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OV_USERSTATUS&nbsp;TYPE&nbsp;CO_ASTTX,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_KEY1(50)&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_KEY2(20)&nbsp;TYPE&nbsp;C,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LV_KEY3(20)&nbsp;TYPE&nbsp;C.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;DATA(LO_LOG)&nbsp;=&nbsp;NEW&nbsp;ZCL_IF_LOG(&nbsp;ID&nbsp;=&nbsp;IV_GUID<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTERFACE&nbsp;=&nbsp;'SI_OA2ERP_PM022_NARR_SYNC_IN'&nbsp;).<br/>
</div>
<div class="codeComment">
* 检查接口是否启用<br/>
* --------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;LO_LOG-&gt;CHECK_ACTIVE(&nbsp;)&nbsp;&lt;&gt;&nbsp;'X'..<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;&nbsp;=&nbsp;'E'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;&nbsp;=&nbsp;'接口未启用.'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'<br/>
&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;IV_QMNUM<br/>
&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;IV_NUMBER.<br/>
&nbsp;&nbsp;IF&nbsp;IV_RESULT&nbsp;EQ&nbsp;'R'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LS_USR_STATUS-STATUS_INT&nbsp;=&nbsp;'E0007'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LS_USR_STATUS-STATUS_EXT&nbsp;=&nbsp;'审批通过'.<br/>
&nbsp;&nbsp;ELSEIF&nbsp;IV_RESULT&nbsp;EQ&nbsp;'N'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LS_USR_STATUS-STATUS_INT&nbsp;=&nbsp;'E0010'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LS_USR_STATUS-STATUS_EXT&nbsp;=&nbsp;'驳回'.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;=&nbsp;'审批状态不对'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;OT_TYPE&nbsp;NE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_ALM_NOTIF_CHANGEUSRSTAT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;IV_NUMBER<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USR_STATUS&nbsp;=&nbsp;LS_USR_STATUS<br/>
</div>
<div class="codeComment">
*       SET_INACTIVE       = ' '<br/>
*       TESTRUN    = ' '<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
</div>
<div class="codeComment">
*       SYSTEMSTATUS       =<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USERSTATUS&nbsp;=&nbsp;OV_USERSTATUS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;OT_RETURN.<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;OT_RETURN&nbsp;INTO&nbsp;OS_RETURN&nbsp;WHERE&nbsp;TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;=&nbsp;OT_MESSAGE&nbsp;&amp;&amp;&nbsp;OS_RETURN-MESSAGE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OT_TYPE&nbsp;NE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OV_USERSTATUS&nbsp;EQ&nbsp;'20'&nbsp;OR&nbsp;OV_USERSTATUS&nbsp;EQ&nbsp;'30'&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REFRESH&nbsp;OT_RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_ALM_NOTIF_SAVE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NUMBER&nbsp;=&nbsp;IV_NUMBER<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN&nbsp;=&nbsp;OT_RETURN.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;OT_RETURN&nbsp;INTO&nbsp;OS_RETURN&nbsp;WHERE&nbsp;TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;=&nbsp;OT_MESSAGE&nbsp;&amp;&amp;&nbsp;OS_RETURN-MESSAGE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDLOOP.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;OT_TYPE&nbsp;NE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'BAPI_TRANSACTION_COMMIT'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;=&nbsp;'S'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;=&nbsp;'执行成功'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_TYPE&nbsp;=&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OT_MESSAGE&nbsp;=&nbsp;'执行失败'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*当ERP为接收方时的,获取系统保存的MSGID<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;"LO_LOG-&gt;set_proxy_in_msgid(&nbsp;)&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LO_LOG-&gt;SET_PROXY_RECEIVER_MSGID(&nbsp;)&nbsp;.<br/>
</div>
<div class="codeComment">
*保存日志<br/>
*--------------------------------------------------------------------*<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;LV_KEY1&nbsp;=&nbsp;iV_GUID.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LV_KEY2&nbsp;=&nbsp;IV_QMNUM.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;LO_LOG-&gt;SAVE_LOG(<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MTYPE&nbsp;=&nbsp;OT_TYPE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MSG&nbsp;=&nbsp;OT_MESSAGE<br/>
&nbsp;&nbsp;&nbsp;&nbsp;KEY1&nbsp;=&nbsp;LV_KEY1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;KEY2&nbsp;=&nbsp;LV_KEY2&nbsp;).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;COMMIT&nbsp;WORK.<br/>
<br/>
ENDFUNCTION.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>