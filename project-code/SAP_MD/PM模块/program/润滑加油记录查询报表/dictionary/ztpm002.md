<table class="outerTable">
<tr>
<td><h2>Table: ZTPM002</h2>
<h3>Description: IW21自定义加油类型配置表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>ZJYLX</td>
<td>2</td>
<td>X</td>
<td>ZEJYLX</td>
<td>ZDJYLX</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>加油类型</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZJYLXMS</td>
<td>3</td>
<td>&nbsp;</td>
<td>ZEJYLXMS</td>
<td>ZDJYLXMS</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>加油类型描述</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>