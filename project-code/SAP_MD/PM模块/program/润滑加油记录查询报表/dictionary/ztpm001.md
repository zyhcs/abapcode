<table class="outerTable">
<tr>
<td><h2>Table: ZTPM001</h2>
<h3>Description: IW21自定义屏幕增强加油记录表</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>MANDT</td>
<td>1</td>
<td>X</td>
<td>MANDT</td>
<td>MANDT</td>
<td>CLNT</td>
<td>3</td>
<td>&nbsp;</td>
<td>集团</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>JYJLID</td>
<td>2</td>
<td>X</td>
<td>SYSUUID_C32</td>
<td>SYSUUID_C32</td>
<td>CHAR</td>
<td>32</td>
<td>&nbsp;</td>
<td>16 Byte UUID in 32 Characters (Hexadecimal Encoded)</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>ZHXM</td>
<td>3</td>
<td>X</td>
<td>ZEHXM</td>
<td>ZDHXM</td>
<td>NUMC</td>
<td>6</td>
<td>&nbsp;</td>
<td>加油记录行项目</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>ZJYLX</td>
<td>4</td>
<td>&nbsp;</td>
<td>ZEJYLX</td>
<td>ZDJYLX</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>加油类型</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>ZYP</td>
<td>5</td>
<td>&nbsp;</td>
<td>ZEYP</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>油品</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>MAKTX</td>
<td>6</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>ZYPPP</td>
<td>7</td>
<td>&nbsp;</td>
<td>ZEYPPP</td>
<td>ZDYPPP</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>油品品牌</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>ZYPXH</td>
<td>8</td>
<td>&nbsp;</td>
<td>ZEYPXH</td>
<td>ZDYPXH</td>
<td>CHAR</td>
<td>50</td>
<td>&nbsp;</td>
<td>油品型号</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>ZJYL</td>
<td>9</td>
<td>&nbsp;</td>
<td>ZEJYL</td>
<td>ZDJYL</td>
<td>QUAN</td>
<td>10</td>
<td>&nbsp;</td>
<td>加油量</td>
</tr>
<tr class="cell">
<td>10</td>
<td>MEINS</td>
<td>10</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>11</td>
<td>ZJYRQ</td>
<td>11</td>
<td>&nbsp;</td>
<td>ZEJYRQ</td>
<td>ZDJYRQ</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>实际加油日期</td>
</tr>
<tr class="cell">
<td>12</td>
<td>ZJYR</td>
<td>12</td>
<td>&nbsp;</td>
<td>ZEJYR</td>
<td>ZDJYR</td>
<td>CHAR</td>
<td>6</td>
<td>&nbsp;</td>
<td>加油人</td>
</tr>
<tr class="cell">
<td>13</td>
<td>.INCLUDE</td>
<td>13</td>
<td>&nbsp;</td>
<td>ZSCOMMON</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>0</td>
<td>&nbsp;</td>
<td>自定义表统一附加结构</td>
</tr>
<tr class="cell">
<td>14</td>
<td>ZZ_CRT_USR</td>
<td>14</td>
<td>&nbsp;</td>
<td>ZE_CRT_USR</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>创建者</td>
</tr>
<tr class="cell">
<td>15</td>
<td>ZZ_CRT_DAT</td>
<td>15</td>
<td>&nbsp;</td>
<td>ZE_CRT_DAT</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>创建日期</td>
</tr>
<tr class="cell">
<td>16</td>
<td>ZZ_CRT_TIME</td>
<td>16</td>
<td>&nbsp;</td>
<td>ZE_CRT_TIME</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>创建时间</td>
</tr>
<tr class="cell">
<td>17</td>
<td>ZZ_UPD_USR</td>
<td>17</td>
<td>&nbsp;</td>
<td>ZE_UPD_USR</td>
<td>XUBNAME</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>变更者</td>
</tr>
<tr class="cell">
<td>18</td>
<td>ZZ_UPD_DAT</td>
<td>18</td>
<td>&nbsp;</td>
<td>ZE_UPD_DAT</td>
<td>&nbsp;</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>变更日期</td>
</tr>
<tr class="cell">
<td>19</td>
<td>ZZ_UPD_TIME</td>
<td>19</td>
<td>&nbsp;</td>
<td>ZE_UPD_TIME</td>
<td>&nbsp;</td>
<td>TIMS</td>
<td>6</td>
<td>&nbsp;</td>
<td>变更时间</td>
</tr>
<tr class="cell">
<td>20</td>
<td>ZZ_DEL</td>
<td>20</td>
<td>&nbsp;</td>
<td>ZE_DEL</td>
<td>CHAR1</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>删除标记</td>
</tr>
<tr class="cell">
<td>21</td>
<td>ZZ_SEL</td>
<td>21</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>1</td>
<td>&nbsp;</td>
<td>选择标识</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>