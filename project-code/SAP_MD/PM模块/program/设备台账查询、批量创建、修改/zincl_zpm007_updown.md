<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPM007_UPDOWN</h2>
<h3> Description: Include ZINCL_ZPM007_UPDOWN</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPM007_UPDOWN<br/>
*&amp;---------------------------------------------------------------------*<br/>
</div>
<div class="code">
DATA c_excel TYPE string VALUE 'Excel文件(*.XLS)|*.XLS|Excel文件(*.xlsx)|*.xlsx|全部文件 (*.*)|*.*|'.<br/>
DATA c_txt TYPE string VALUE 'TXT文件(*.TXT)|*.TXT|全部文件 (*.*)|*.*|' .<br/>
DATA gt_alsmex TYPE issr_alsmex_tabline.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILE_SAVETEMPLET<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_file_savetemplet .<br/>
&nbsp;&nbsp;DATA&nbsp;ls_file&nbsp;TYPE&nbsp;localfile.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_file_save_dialog&nbsp;USING&nbsp;space&nbsp;&nbsp;&nbsp;c_excel&nbsp;space&nbsp;CHANGING&nbsp;ls_file.<br/>
&nbsp;&nbsp;CHECK&nbsp;ls_file&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_download_template&nbsp;USING&nbsp;c_template&nbsp;ls_file.<br/>
<br/>
ENDFORM.<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_SELDATACHECK<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_seldatacheck USING p_file TYPE any.<br/>
&nbsp;&nbsp;IF&nbsp;p_file&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e000&nbsp;WITH&nbsp;'请选择导入文件!'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_file_exist&nbsp;USING&nbsp;p_file&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e000&nbsp;WITH&nbsp;'文件不存在,请检查!'&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_FILE_SAVE_DIALOG<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;p1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;p2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_file_save_dialog USING i_dfname TYPE any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_filter&nbsp;TYPE&nbsp;any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_overwrite&nbsp;TYPE&nbsp;flag<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;e_fullpath&nbsp;TYPE&nbsp;clike.<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'保存文件'.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_default_file_name&nbsp;TYPE&nbsp;string&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fullpath&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;&nbsp;VALUE&nbsp;'C:\*\*\Desktop\'&nbsp;."默认桌面<br/>
&nbsp;&nbsp;DATA&nbsp;ls_user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i&nbsp;.<br/>
<br/>
&nbsp;&nbsp;IF&nbsp;i_dfname&nbsp;&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;i_dfname&nbsp;TO&nbsp;ls_default_file_name&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CONCATENATE&nbsp;sy-title&nbsp;'_'&nbsp;sy-datum&nbsp;&nbsp;INTO&nbsp;ls_default_file_name&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_save_dialog<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_window_title<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_extension&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;space<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_file_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_default_file_name<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prompt_on_overwrite&nbsp;&nbsp;=&nbsp;i_overwrite<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_path<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fullpath&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_fullpath<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_user_action<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;4.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;Clear&nbsp;fullpath&nbsp;if&nbsp;cancel<br/>
</div>
<div class="code">
&nbsp;&nbsp;IF&nbsp;ls_user_action&nbsp;=&nbsp;cl_gui_frontend_services=&gt;action_cancel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;e_fullpath.<br/>
&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;e_fullpath&nbsp;=&nbsp;ls_fullpath.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CHECK_FILE_EXIST<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;检查模板是否存在<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_FILENAME&nbsp;&nbsp;&nbsp;文件名<br/>
*&nbsp;&nbsp;&lt;--&nbsp;&nbsp;E_EXIST&nbsp;&nbsp;是否存在<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_check_file_exist USING i_filename TYPE any .<br/>
<br/>
&nbsp;&nbsp;DATA&nbsp;ls_file&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_exist&nbsp;&nbsp;TYPE&nbsp;abap_bool.<br/>
&nbsp;&nbsp;ls_file&nbsp;=&nbsp;i_filename.<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_exist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RECEIVING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;result&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_exist<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wrong_parameter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not_supported_by_gui&nbsp;=&nbsp;4<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;5.<br/>
&nbsp;&nbsp;IF&nbsp;ls_exist&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;8&nbsp;TO&nbsp;sy-subrc&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;0&nbsp;TO&nbsp;sy-subrc&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
ENDFORM. "FRM_CHECK_FILE_EXIST<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_DOWNLOAD_TEMPLATE<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下载模版<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_OBJID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;模版对象名称<br/>
*&nbsp;&nbsp;--&gt;&nbsp;&nbsp;I_FILENAME&nbsp;&nbsp;下载文件名<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_download_template USING i_objid TYPE wwwdatatab-objid<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_file&nbsp;&nbsp;&nbsp;TYPE&nbsp;any&nbsp;.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA&nbsp;LS_FILENAME&nbsp;TYPE&nbsp;STRING&nbsp;&nbsp;.<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_exist&nbsp;TYPE&nbsp;abap_bool.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_wwwdata&nbsp;TYPE&nbsp;wwwdatatab.<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;DATA&nbsp;LT_MINE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;W3MIME&nbsp;OCCURS&nbsp;10&nbsp;.<br/>
*&nbsp;&nbsp;DATA&nbsp;LS_TITLE&nbsp;TYPE&nbsp;STRING&nbsp;.<br/>
*<br/>
</div>
<div class="code">
&nbsp;&nbsp;DATA&nbsp;ls_rc&nbsp;&nbsp;&nbsp;TYPE&nbsp;sy-subrc.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_path&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_fullpath&nbsp;TYPE&nbsp;string.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;判断模版文件是否存在<br/>
</div>
<div class="code">
&nbsp;&nbsp;PERFORM&nbsp;frm_check_file_exist&nbsp;USING&nbsp;i_file&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.&nbsp;"已存在<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'模版'&nbsp;i_file&nbsp;'已存在'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'W'.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_wwwdata-relid&nbsp;=&nbsp;'MI'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_wwwdata-objid&nbsp;=&nbsp;i_objid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'DOWNLOAD_WEB_OBJECT'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_wwwdata<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination&nbsp;=&nbsp;i_file<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_rc.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_rc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;e001(00)&nbsp;WITH&nbsp;'下载模版失败!'&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'成功下载模版,下载目录:'&nbsp;i_file&nbsp;&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
ENDFORM. " download_template<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_GETPATH<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--P_P_FILE&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_getpath USING i_filter TYPE any<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;e_fullpath&nbsp;TYPE&nbsp;any&nbsp;.<br/>
<br/>
&nbsp;&nbsp;"""""""""""""""''function<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;call&nbsp;function&nbsp;'F4_FILENAME'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;exporting<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;program_name&nbsp;&nbsp;=&nbsp;syst-cprog<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dynpro_number&nbsp;=&nbsp;syst-dynnr<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;field_name&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;importing<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;I_FILE.<br/>
</div>
<div class="code">
&nbsp;&nbsp;"""""""""""""""""""""""""""""""""""""<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;f_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;filetable&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;&nbsp;ls_initname&nbsp;TYPE&nbsp;string.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_user_action&nbsp;TYPE&nbsp;i&nbsp;.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_wtitle&nbsp;TYPE&nbsp;string&nbsp;VALUE&nbsp;'文件选择'&nbsp;.<br/>
<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;f_table&gt;&nbsp;TYPE&nbsp;file_table&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;&nbsp;i_filter&nbsp;IS&nbsp;INITIAL&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MOVE&nbsp;c_excel&nbsp;TO&nbsp;i_filter&nbsp;.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;concatenate&nbsp;&nbsp;'XXXX'&nbsp;&nbsp;'-'&nbsp;SY-DATUM&nbsp;SY-UZEIT'.xls'&nbsp;into&nbsp;&nbsp;L_INITNAME.<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_frontend_services=&gt;file_open_dialog&nbsp;"打开<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;call&nbsp;method&nbsp;cl_gui_frontend_services=&gt;file_save_dialog&nbsp;"保存<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;window_title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'选择文件'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;default_filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_initname<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_filter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filter<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;initial_directory&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'C:\*\*\Desktop\'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;multiselection&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'&nbsp;'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CHANGING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_table&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;f_table<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;rc<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user_action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;ls_user_action<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;file_open_dialog_failed&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;error_no_gui&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
<br/>
&nbsp;&nbsp;CALL&nbsp;METHOD&nbsp;cl_gui_cfw=&gt;flush<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_error&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cntl_system_error&nbsp;=&nbsp;2.<br/>
&nbsp;&nbsp;IF&nbsp;ls_user_action&nbsp;EQ&nbsp;cl_gui_frontend_services=&gt;action_cancel.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;e_fullpath&nbsp;.<br/>
&nbsp;&nbsp;ELSE&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;f_table&nbsp;ASSIGNING&nbsp;&lt;f_table&gt;&nbsp;INDEX&nbsp;1.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e_fullpath&nbsp;=&nbsp;&lt;f_table&gt;-filename.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
ENDFORM.<br/>
<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_EXCEL_TO_ITAB<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXCEL上传到sap<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;--ET_UPLOAD&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_excel_to_tabline USING i_filename TYPE clike<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sheet_name&nbsp;&nbsp;&nbsp;TYPE&nbsp;clike<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_sheet_number&nbsp;TYPE&nbsp;i<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHANGING&nbsp;et_upload&nbsp;TYPE&nbsp;issr_alsmex_tabline.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;0.&nbsp;清除之前的数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CLEAR:et_upload,et_upload[].<br/>
<br/>
&nbsp;&nbsp;PERFORM&nbsp;frm_check_file_exist&nbsp;USING&nbsp;i_filename&nbsp;.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'上传文件'&nbsp;i_filename&nbsp;'不存在!'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;2.&nbsp;显示进度<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'SAPGUI_PROGRESS_INDICATOR'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text&nbsp;=&nbsp;'文件上传中,&nbsp;请稍等......'.<br/>
<br/>
</div>
<div class="codeComment">
*&nbsp;3.&nbsp;读取上传文件内的数据<br/>
</div>
<div class="code">
&nbsp;&nbsp;CALL&nbsp;FUNCTION&nbsp;'ALSM_EXCEL_TO_INTERNAL_TABLE'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXPORTING<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;filename&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_filename<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_begin_col<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_begin_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_begin_row<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_col&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_end_col<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i_end_row&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;i_end_row<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SHNAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;'Sheet1'<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I_SHEET_NUMBER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;I_SHEET_NUMBER<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;TABLES<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;intern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;et_upload<br/>
&nbsp;&nbsp;&nbsp;&nbsp;EXCEPTIONS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inconsistent_parameters&nbsp;=&nbsp;1<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;upload_ole&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;2<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OTHERS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;3.<br/>
&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;NE&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;MESSAGE&nbsp;s001(00)&nbsp;WITH&nbsp;'文件'&nbsp;i_filename&nbsp;'上传不成功,请检查!'&nbsp;DISPLAY&nbsp;LIKE&nbsp;'E'.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM. " FRM_EXCEL_TO_ITAB<br/>
<br/>
</div>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form&nbsp;&nbsp;FRM_CONVERTDATA<br/>
*&amp;---------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&gt;P_GT_OUT&nbsp;&nbsp;text<br/>
*----------------------------------------------------------------------*<br/>
</div>
<div class="code">
FORM frm_convertdata TABLES it_data TYPE STANDARD TABLE .<br/>
&nbsp;&nbsp;"为&nbsp;&lt;...&gt;&nbsp;插入正确的名称.<br/>
&nbsp;&nbsp;DATA&nbsp;cl_descr&nbsp;TYPE&nbsp;REF&nbsp;TO&nbsp;cl_abap_structdescr.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_comp&gt;&nbsp;TYPE&nbsp;abap_compdescr.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_alsmex&gt;&nbsp;TYPE&nbsp;alsmex_tabline&nbsp;.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;ls_field&gt;&nbsp;&nbsp;TYPE&nbsp;any.<br/>
&nbsp;&nbsp;cl_descr&nbsp;?=&nbsp;cl_abap_typedescr=&gt;describe_by_data(&nbsp;it_data&nbsp;)."&nbsp;内表数据结构<br/>
&nbsp;&nbsp;DELETE&nbsp;gt_alsmex&nbsp;WHERE&nbsp;value&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;CLEAR&nbsp;it_data.<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_alsmex&nbsp;ASSIGNING&nbsp;&lt;ls_alsmex&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;cl_descr-&gt;components&nbsp;ASSIGNING&nbsp;&lt;ls_comp&gt;&nbsp;INDEX&nbsp;&lt;ls_alsmex&gt;-col&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ASSIGN&nbsp;COMPONENT&nbsp;&lt;ls_comp&gt;-name&nbsp;OF&nbsp;STRUCTURE&nbsp;it_data&nbsp;TO&nbsp;&lt;ls_field&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ls_field&gt;&nbsp;=&nbsp;&lt;ls_alsmex&gt;-value&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AT&nbsp;END&nbsp;OF&nbsp;row.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;it_data&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;it_data&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ENDAT&nbsp;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF&nbsp;.<br/>
<br/>
&nbsp;&nbsp;ENDLOOP&nbsp;.<br/>
ENDFORM.<br/>
<br/>
"将上传数据转换为指定类型数据，上传数据都未STring，需要处理<br/>
FORM frm_trans_updata_to_tab.<br/>
&nbsp;&nbsp;IF&nbsp;gt_upload[]&nbsp;IS&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;RETURN.<br/>
&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;CLEAR:gt_tab.<br/>
&nbsp;&nbsp;FIELD-SYMBOLS&nbsp;&lt;fs_up&gt;&nbsp;TYPE&nbsp;ty_upload.<br/>
&nbsp;&nbsp;DATA&nbsp;ls_tab&nbsp;TYPE&nbsp;ty_tab.<br/>
<br/>
&nbsp;&nbsp;DATA:&nbsp;lv_tmp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_tmp2&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lv_message&nbsp;TYPE&nbsp;bapi_msg.<br/>
<br/>
&nbsp;&nbsp;"反查工作中心编码的对象号<br/>
&nbsp;&nbsp;DATA:&nbsp;ls_arbpl&nbsp;TYPE&nbsp;ty_crtx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_arbpl&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_crtx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_crtx&nbsp;&nbsp;TYPE&nbsp;ty_crtx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lt_crtx&nbsp;&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_crtx.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_upload&nbsp;ASSIGNING&nbsp;&lt;fs_up&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_arbpl-arbpl&nbsp;=&nbsp;&lt;fs_up&gt;-arbpl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_arbpl&nbsp;TO&nbsp;lt_arbpl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_arbpl.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
&nbsp;&nbsp;SELECT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objid&nbsp;"资源的对象标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objty&nbsp;""CIM&nbsp;资源的对象类型.A"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arbpl&nbsp;&nbsp;"工作中心编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FROM&nbsp;crhd<br/>
&nbsp;&nbsp;&nbsp;&nbsp;INTO&nbsp;CORRESPONDING&nbsp;FIELDS&nbsp;OF&nbsp;TABLE&nbsp;lt_crtx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;FOR&nbsp;ALL&nbsp;ENTRIES&nbsp;IN&nbsp;lt_arbpl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;WHERE&nbsp;arbpl&nbsp;=&nbsp;lt_arbpl-arbpl<br/>
&nbsp;&nbsp;&nbsp;&nbsp;AND&nbsp;crhd~objty&nbsp;=&nbsp;'A'<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.<br/>
<br/>
&nbsp;&nbsp;LOOP&nbsp;AT&nbsp;gt_upload&nbsp;ASSIGNING&nbsp;&lt;fs_up&gt;.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-equnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-equnr.&nbsp;&nbsp;"设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"设备类别<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-eqtyp&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-eqtyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-eqtyp_txt&nbsp;=&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-eqfnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-eqfnr.&nbsp;&nbsp;"原设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-equnr_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-equnr_txt.&nbsp;&nbsp;"设备描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-stat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-stat.&nbsp;&nbsp;"设备状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-begru&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-begru.&nbsp;&nbsp;"是否特种设备<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;"技术对象类型<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-eqart&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-eqart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-eqart_txt&nbsp;=&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-brgew&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-brgew.&nbsp;&nbsp;"重量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"重量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-gewei&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-gewei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"重量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-gewei_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"重量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-groes.&nbsp;&nbsp;"大小/尺寸<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-inbdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-inbdt.&nbsp;&nbsp;"开始日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-erdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-inbdt.&nbsp;&nbsp;"创建日期，这个需要跟业务顾问研究一下<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-answt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-answt.&nbsp;&nbsp;"购置价值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;ls_tab-answt&nbsp;IS&nbsp;NOT&nbsp;INITIAL.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-waers&nbsp;=&nbsp;'CNY'.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ELSE.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-waers&nbsp;=&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-ansdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-ansdt.&nbsp;&nbsp;"购置日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-herst&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-herst.&nbsp;&nbsp;"制造商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"制造商国家<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-herld&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-herld&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"制造商国家<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-herld_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"制造商国家<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-typbz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-typbz.&nbsp;&nbsp;"型号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-mapar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-mapar.&nbsp;&nbsp;"制造商零件号<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zeqsq&nbsp;type&nbsp;string,&nbsp;&nbsp;"设备系列号<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-serge&nbsp;=&nbsp;&lt;fs_up&gt;-serge.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-baujj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-baujj.&nbsp;&nbsp;"制造年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-baumm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-baumm.&nbsp;&nbsp;"制造月<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"维护工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-swerk&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-swerk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"维护工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-swerk_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"维护工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-msgrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-msgrp.&nbsp;&nbsp;"设备负责人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"工厂区域<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-beber&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-beber&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"工厂区域<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-beber_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"工厂区域<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-abckz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-abckz.&nbsp;&nbsp;"ABC标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-kostl&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-kostl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-kostl&nbsp;=&nbsp;|{&nbsp;ls_tab-kostl&nbsp;ALPHA&nbsp;=&nbsp;&nbsp;IN&nbsp;}|.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-kostl_txt&nbsp;&nbsp;=&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"计划员组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-ingrp&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-ingrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"计划员组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-ingrp_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"计划员组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"主工作中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-arbpl&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-arbpl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"主工作中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-arbpl_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"主工作中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;READ&nbsp;TABLE&nbsp;lt_crtx&nbsp;INTO&nbsp;ls_crtx&nbsp;WITH&nbsp;KEY&nbsp;arbpl&nbsp;=&nbsp;ls_tab-arbpl.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IF&nbsp;sy-subrc&nbsp;EQ&nbsp;0.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-gewrk&nbsp;=&nbsp;ls_crtx-objid.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ENDIF.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"功能位置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-tplnr&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-tplnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"功能位置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-tplnr_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"功能位置描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-anlnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;&lt;fs_up&gt;-anlnr.&nbsp;&nbsp;"资产卡片<br/>
&nbsp;&nbsp;&nbsp;&nbsp;"设备分类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;PERFORM&nbsp;frm_get_attr&nbsp;USING&nbsp;&lt;fs_up&gt;-sklsob&nbsp;CHANGING&nbsp;lv_tmp&nbsp;lv_tmp2.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-sklsob&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp.&nbsp;&nbsp;"设备分类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-kschl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=&nbsp;lv_tmp2.&nbsp;&nbsp;"类别名称<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;ls_tab-zinnm&nbsp;=&nbsp;&lt;fs_up&gt;-zinnm.&nbsp;"填写人<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;APPEND&nbsp;ls_tab&nbsp;TO&nbsp;gt_tab.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;CLEAR&nbsp;ls_tab.<br/>
&nbsp;&nbsp;ENDLOOP.<br/>
<br/>
ENDFORM.<br/>
<br/>
"在上传的文件中，不少字段都是“属性值_属性描述”的形式，在此取属性值<br/>
FORM frm_get_attr USING iv_upvle TYPE string CHANGING ov_attr TYPE string ov_attr2 TYPE string.<br/>
&nbsp;&nbsp;CLEAR:&nbsp;ov_attr,ov_attr2.<br/>
&nbsp;&nbsp;IF&nbsp;iv_upvle&nbsp;IS&nbsp;NOT&nbsp;INITIAL&nbsp;OR&nbsp;iv_upvle&nbsp;&lt;&gt;&nbsp;''.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;SPLIT&nbsp;iv_upvle&nbsp;AT&nbsp;'_'&nbsp;INTO&nbsp;ov_attr&nbsp;ov_attr2.<br/>
&nbsp;&nbsp;ENDIF.<br/>
ENDFORM.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>