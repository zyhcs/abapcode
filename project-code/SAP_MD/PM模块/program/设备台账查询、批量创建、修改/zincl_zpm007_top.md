<table class="outerTable">
<tr class="normalBoldLarge">
<td><h2>Code listing for: ZINCL_ZPM007_TOP</h2>
<h3> Description: Include ZINCL_ZPM007_TOP</h3></td>
</tr>
<tr>
<td>
<table class="innerTable">
<tr>
<td>
<div class="codeComment">
*&amp;---------------------------------------------------------------------*<br/>
*&amp;&nbsp;包含&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZINCL_ZPM007_TOP<br/>
*&amp;---------------------------------------------------------------------*<br/>
<br/>
</div>
<div class="code">
DATA c_template TYPE w3objid VALUE 'ZPM007' . "模板名称<br/>
<br/>
DATA:gds_alv_layout_lvc   TYPE lvc_s_layo,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gdt_alv_fieldcat_lvc&nbsp;TYPE&nbsp;lvc_t_fcat,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gds_alv_fieldcat_lvc&nbsp;TYPE&nbsp;lvc_s_fcat.<br/>
<br/>
DATA:g_grid TYPE REF TO cl_gui_alv_grid.<br/>
<br/>
TYPES:BEGIN OF ty_upload,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zinnm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"填写人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqtyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备类别,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqfnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"原设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr_txt&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;begru&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"是否特种设备<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"技术对象类型,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brgew&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"重量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gewei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"重量单位,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"大小/尺寸<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inbdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"开始日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;answt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"购置价值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ansdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"购置日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herst&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"制造商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herld&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"制造商国家,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;typbz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"型号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mapar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"制造商零件号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;serge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备系列号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;baujj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"制造年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;baumm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"制造月<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;swerk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"维护工厂,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备负责人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beber&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"工厂区域,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abckz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"ABC标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"成本中心,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ingrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"计划员组,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arbpl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"主工作中心,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"功能位置,下划线split<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr_txt&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"功能位置描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"资产卡片<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sklsob&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"设备分类,下划线split<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kschl&nbsp;TYPE&nbsp;string,&nbsp;&nbsp;"类别名称<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_upload.<br/>
<br/>
DATA gt_upload TYPE STANDARD TABLE OF ty_upload.<br/>
<br/>
TYPES:BEGIN OF ty_tab,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-equnr,&nbsp;&nbsp;"设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;eqkt-eqktx,&nbsp;&nbsp;&nbsp;&nbsp;"设备描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;datbi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-datbi,&nbsp;&nbsp;&nbsp;&nbsp;""有效截至日期99991231"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqlfn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-eqlfn,&nbsp;&nbsp;&nbsp;""同一天的设备使用期段连号取最大值对应的记录"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iloan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-iloan,&nbsp;&nbsp;&nbsp;&nbsp;"技术对象的位置和科目分配<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gewrk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-gewrk,&nbsp;&nbsp;"工作中心的对象标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arbpl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;crhd-arbpl,&nbsp;&nbsp;"工作中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arbpl_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;crtx-ktext,&nbsp;&nbsp;&nbsp;&nbsp;"工作中心描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ingrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-ingrp,&nbsp;&nbsp;&nbsp;&nbsp;"计划员组(客户服务和工厂维护的计划员组)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ingrp_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t024i-innam,&nbsp;&nbsp;&nbsp;&nbsp;"计划员组描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iwerk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-iwerk,&nbsp;&nbsp;&nbsp;&nbsp;"维护计划工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mapar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equz-mapar,&nbsp;&nbsp;"制造商零件号<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqtyp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-eqtyp,&nbsp;&nbsp;"设备种类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqtyp_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t370u-typtx,&nbsp;&nbsp;&nbsp;"设备种类描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-objnr,&nbsp;&nbsp;&nbsp;&nbsp;"对象号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;jest-stat,&nbsp;&nbsp;&nbsp;"对象状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zssts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;j_stext,&nbsp;&nbsp;&nbsp;"设备系统状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zusts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;asttx,&nbsp;&nbsp;&nbsp;"设备用户状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sklsob&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;mcipmis-sklsob,&nbsp;&nbsp;&nbsp;&nbsp;"设备分类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kschl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;swor-kschl,&nbsp;&nbsp;&nbsp;"设备分类描述<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;erdat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-erdat,&nbsp;&nbsp;&nbsp;"创建日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;begru&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-begru,&nbsp;&nbsp;"是否特种设备(技术对象授权组)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;begru_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t370b_t-begtx,&nbsp;&nbsp;&nbsp;"技术对象授权组描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqart&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-eqart,&nbsp;&nbsp;"技术对象类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqart_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t370k_t-eartx,&nbsp;&nbsp;&nbsp;"技术对象类型描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;brgew&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-brgew,&nbsp;&nbsp;"重量<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gewei&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-gewei,&nbsp;&nbsp;"重量单位<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gewei_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t006t-txdim,&nbsp;&nbsp;&nbsp;"重量单位描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;groes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-groes,&nbsp;&nbsp;"大小/尺寸<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inbdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-inbdt,&nbsp;&nbsp;"开始日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;answt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-answt,&nbsp;&nbsp;"购置价值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;waers&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-waers,&nbsp;&nbsp;"购置价值<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ansdt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-ansdt,&nbsp;&nbsp;"购置日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;serge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-serge,&nbsp;&nbsp;"制造商系列号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herst&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-herst,&nbsp;&nbsp;"制造商<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herld&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-herld,&nbsp;&nbsp;"制造商国家<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;herld_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t005t-landx,&nbsp;&nbsp;&nbsp;"国家描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;typbz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-typbz,&nbsp;&nbsp;"型号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;baujj&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-baujj,&nbsp;&nbsp;"制造年<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;baumm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;equi-baumm,&nbsp;&nbsp;"制造月<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-tplnr,&nbsp;&nbsp;&nbsp;"功能位置<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iflotx-pltxt,&nbsp;&nbsp;&nbsp;&nbsp;"功能位置描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msgrp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-msgrp,&nbsp;&nbsp;"设备负责人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;swerk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-swerk,&nbsp;&nbsp;&nbsp;&nbsp;"维护工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;swerk_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t001w-name1,&nbsp;&nbsp;"维护工厂描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqfnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-eqfnr,&nbsp;&nbsp;&nbsp;&nbsp;"分类字段(原设备号)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beber&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-beber,&nbsp;&nbsp;"工厂区域<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beber_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t357-fing,&nbsp;&nbsp;&nbsp;"工区描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abckz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-abckz,&nbsp;&nbsp;"ABC标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abckz_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;t370c_t-abctx,&nbsp;&nbsp;"ABC标识描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;cskt-kostl,&nbsp;&nbsp;"成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kokrs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-kokrs,&nbsp;&nbsp;"控制范围，800<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl_txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;cskt-ktext,&nbsp;&nbsp;&nbsp;&nbsp;"控制范围描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;anlnr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;iloa-anlnr,&nbsp;&nbsp;"资产卡片<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zinnm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;40,&nbsp;&nbsp;"填写人<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zsel,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ztype&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zicon(4)&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zmessage(200)&nbsp;,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_tab.<br/>
<br/>
DATA gt_tab TYPE STANDARD TABLE OF ty_tab.<br/>
<br/>
"设备短文本<br/>
TYPES:BEGIN OF ty_eqkt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;equnr&nbsp;TYPE&nbsp;&nbsp;equnr,&nbsp;&nbsp;"设备号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqktx	TYPE&nbsp;ktx01,&nbsp;&nbsp;&nbsp;"技术对象描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_eqkt.<br/>
DATA gt_eqkt TYPE STANDARD TABLE OF ty_eqkt.<br/>
<br/>
"工作中心或生产资源/工具文本<br/>
TYPES:BEGIN OF ty_crtx,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objid&nbsp;TYPE&nbsp;&nbsp;cr_objid,&nbsp;"资源的对象标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objty&nbsp;TYPE&nbsp;&nbsp;cr_objty,&nbsp;""CIM&nbsp;资源的对象类型.A"<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;arbpl&nbsp;TYPE&nbsp;arbpl,&nbsp;"工作中心编码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;TYPE&nbsp;&nbsp;cr_ktext,&nbsp;"短描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_crtx.<br/>
DATA gt_crtx TYPE STANDARD TABLE OF ty_crtx.<br/>
<br/>
"单独对象状态<br/>
TYPES:BEGIN OF ty_jest,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objnr&nbsp;TYPE&nbsp;&nbsp;j_objnr,&nbsp;"&nbsp;&nbsp;对象号<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;stat&nbsp;&nbsp;TYPE&nbsp;&nbsp;j_status,&nbsp;"&nbsp;对象状态<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_jest.<br/>
DATA gt_jest TYPE STANDARD TABLE OF ty_jest.<br/>
<br/>
"维护工厂<br/>
TYPES:BEGIN OF ty_t001w,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;TYPE&nbsp;&nbsp;werks_d,&nbsp;"&nbsp;&nbsp;工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name1&nbsp;TYPE&nbsp;&nbsp;name1,&nbsp;"&nbsp;&nbsp;名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t001w.<br/>
DATA gt_t001w TYPE STANDARD TABLE OF ty_t001w.<br/>
<br/>
"成本中心<br/>
TYPES:BEGIN OF ty_cskt,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;datbi&nbsp;TYPE&nbsp;&nbsp;datbi,&nbsp;"&nbsp;&nbsp;有效截至日期<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kokrs&nbsp;TYPE&nbsp;&nbsp;kokrs,&nbsp;"&nbsp;&nbsp;控制范围<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kostl&nbsp;TYPE&nbsp;&nbsp;kostl,&nbsp;"&nbsp;&nbsp;成本中心<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ktext&nbsp;TYPE&nbsp;&nbsp;ktext,&nbsp;"&nbsp;&nbsp;KTEXT<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_cskt.<br/>
DATA gt_cskt TYPE STANDARD TABLE OF ty_cskt.<br/>
<br/>
"功能位置<br/>
TYPES:BEGIN OF ty_iflot,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objnr&nbsp;TYPE&nbsp;&nbsp;j_objnr,&nbsp;"&nbsp;&nbsp;对象号,iflot<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tplnr&nbsp;TYPE&nbsp;&nbsp;tplnr,&nbsp;"&nbsp;&nbsp;功能位置,,iflot,iflotx<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码,iflotx<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pltxt&nbsp;TYPE&nbsp;&nbsp;pltxt,&nbsp;"&nbsp;&nbsp;功能位置描述,iflotx<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_iflot.<br/>
DATA gt_iflot TYPE STANDARD TABLE OF ty_iflot.<br/>
</div>
<div class="codeComment">
*DATA&nbsp;gt_iflotx&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;ty_iflot.<br/>
<br/>
</div>
<div class="code">
"设备分类<br/>
TYPES:BEGIN OF ty_mcipmis,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;objnr&nbsp;&nbsp;TYPE&nbsp;&nbsp;qmobjnr,&nbsp;"&nbsp;&nbsp;状态管理的对象号,MCIPMIS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sklsob&nbsp;TYPE	sklsob,&nbsp;"	PMIS:对象分类,MCIPMIS<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class&nbsp;&nbsp;TYPE&nbsp;&nbsp;klasse_d,&nbsp;"&nbsp;分类编号,KLAH<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clint&nbsp;&nbsp;TYPE&nbsp;&nbsp;clint,&nbsp;"&nbsp;&nbsp;内部分类号,KLAH<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kschl&nbsp;&nbsp;TYPE&nbsp;text40,&nbsp;&nbsp;"分类描述，SWOR<br/>
</div>
<div class="codeComment">
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;imerk&nbsp;&nbsp;TYPE&nbsp;&nbsp;atinn,&nbsp;"&nbsp;&nbsp;内部特性,ksml<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atinn&nbsp;&nbsp;TYPE&nbsp;&nbsp;atinn,&nbsp;"&nbsp;&nbsp;内部特性,cabnt<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码,cabnt<br/>
*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;atbez&nbsp;&nbsp;TYPE&nbsp;&nbsp;atbez,&nbsp;"&nbsp;&nbsp;特性描述,cabnt<br/>
</div>
<div class="code">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_mcipmis.<br/>
DATA gt_mcipmis TYPE STANDARD TABLE OF ty_mcipmis.<br/>
<br/>
"维护计划员组<br/>
TYPES:BEGIN OF ty_t024i,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ingrp&nbsp;TYPE&nbsp;ingrp,&nbsp;"&nbsp;&nbsp;客户服务和工厂维护的计划员组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iwerk&nbsp;TYPE&nbsp;iwerk,&nbsp;"&nbsp;&nbsp;IWERK<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;innam&nbsp;TYPE&nbsp;innam,&nbsp;"&nbsp;&nbsp;维护计划员组的名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t024i.<br/>
DATA gt_t024i TYPE STANDARD TABLE OF ty_t024i.<br/>
<br/>
"设备种类<br/>
TYPES:BEGIN OF ty_t370u,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqtyp&nbsp;TYPE&nbsp;&nbsp;eqtyp,&nbsp;"&nbsp;&nbsp;设备种类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;typtx&nbsp;TYPE&nbsp;&nbsp;etytx,&nbsp;"&nbsp;&nbsp;设备种类描述<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t370u.<br/>
DATA gt_t370u TYPE STANDARD TABLE OF ty_t370u.<br/>
<br/>
"是否特种设备<br/>
TYPES:BEGIN OF ty_t370b_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;begru&nbsp;TYPE&nbsp;&nbsp;iautg,&nbsp;"&nbsp;&nbsp;技术对象授权组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;begtx&nbsp;TYPE&nbsp;&nbsp;iautx,&nbsp;"&nbsp;&nbsp;数据对象授权组的文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t370b_t.<br/>
DATA gt_t370b_t TYPE STANDARD TABLE OF ty_t370b_t.<br/>
<br/>
"技术对象类型<br/>
TYPES:BEGIN OF ty_t370k_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eqart&nbsp;TYPE&nbsp;&nbsp;eqart,&nbsp;"&nbsp;&nbsp;技术对象类型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eartx&nbsp;TYPE&nbsp;&nbsp;eartx,&nbsp;"&nbsp;&nbsp;对象文本文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t370k_t.<br/>
DATA gt_t370k_t TYPE STANDARD TABLE OF ty_t370k_t.<br/>
<br/>
"重量单位<br/>
TYPES:BEGIN OF ty_t006,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;msehi&nbsp;TYPE&nbsp;&nbsp;msehi,&nbsp;"&nbsp;&nbsp;度量单位.t006<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dimid&nbsp;TYPE&nbsp;&nbsp;dimid,&nbsp;"&nbsp;&nbsp;维度键值.t006,t006t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码,t006t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;txdim&nbsp;TYPE&nbsp;&nbsp;txdim,&nbsp;"&nbsp;&nbsp;量纲文本,t006t<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t006.<br/>
DATA gt_t006 TYPE STANDARD TABLE OF ty_t006.<br/>
<br/>
"国家<br/>
TYPES:BEGIN OF ty_t005t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;land1&nbsp;TYPE&nbsp;&nbsp;land1,&nbsp;"&nbsp;&nbsp;国家/地区代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;landx&nbsp;TYPE&nbsp;&nbsp;landx,&nbsp;"&nbsp;&nbsp;国家名称<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t005t.<br/>
DATA gt_t005t TYPE STANDARD TABLE OF ty_t005t.<br/>
<br/>
"工厂区域<br/>
TYPES:BEGIN OF ty_t357,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;werks&nbsp;TYPE&nbsp;&nbsp;werks_d,&nbsp;"&nbsp;&nbsp;工厂<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beber&nbsp;TYPE&nbsp;&nbsp;beber,&nbsp;"&nbsp;&nbsp;厂区<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fing&nbsp;&nbsp;TYPE&nbsp;&nbsp;fing,&nbsp;"&nbsp;负责公司范围的员工组<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t357.<br/>
DATA gt_t357 TYPE STANDARD TABLE OF ty_t357.<br/>
<br/>
"技术对象的 ABC 标识<br/>
TYPES:BEGIN OF ty_t370c_t,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abckz&nbsp;TYPE&nbsp;&nbsp;abckz,&nbsp;"&nbsp;&nbsp;技术对象的&nbsp;ABC&nbsp;标识<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spras&nbsp;TYPE&nbsp;&nbsp;spras,&nbsp;"&nbsp;&nbsp;语言代码<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;abctx&nbsp;TYPE&nbsp;&nbsp;abctx,&nbsp;"&nbsp;&nbsp;技术对象的&nbsp;ABC&nbsp;标识文本<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_t370c_t.<br/>
DATA gt_t370c_t TYPE STANDARD TABLE OF ty_t370c_t.<br/>
<br/>
DATA: gt_s_ststs TYPE STANDARD TABLE OF bapi_itob_status,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gt_u_ststs&nbsp;TYPE&nbsp;STANDARD&nbsp;TABLE&nbsp;OF&nbsp;bapi_itob_status.<br/>
<br/>
TYPES:BEGIN OF ty_split,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lntxt&nbsp;TYPE&nbsp;c&nbsp;LENGTH&nbsp;220,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END&nbsp;OF&nbsp;ty_split.<br/>
<br/>
DATA gt_return TYPE STANDARD TABLE OF bapiret2.<br/>
</div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>