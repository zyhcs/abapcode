<table class="outerTable">
<tr>
<td><h2>Table: ZSPM001</h2>
<h3>Description: 备品备件消耗量</h3></td>
</tr>
<tr>
<td><!--This is where our main table begins  -->
<table class="innerTable">
<tr>
<th>Row</th>
<th>Field name</th>
<th>Position</th>
<th>Key</th>
<th>Data element</th>
<th>Domain</th>
<th>Datatype</th>
<th>Length</th>
<th>Lowercase</th>
<th>Domain text</th>
</tr>
<tr class="cell">
<td> 1</td>
<td>AUFNR</td>
<td>1</td>
<td>&nbsp;</td>
<td>AUFNR</td>
<td>AUFNR</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>订单编号</td>
</tr>
<tr class="cell">
<td> 2</td>
<td>KTEXT</td>
<td>2</td>
<td>&nbsp;</td>
<td>AUFTEXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>描述</td>
</tr>
<tr class="cell">
<td> 3</td>
<td>EQUNR</td>
<td>3</td>
<td>&nbsp;</td>
<td>EQUNR</td>
<td>EQUNR</td>
<td>CHAR</td>
<td>18</td>
<td>&nbsp;</td>
<td>设备号</td>
</tr>
<tr class="cell">
<td> 4</td>
<td>EQKTX</td>
<td>4</td>
<td>&nbsp;</td>
<td>KTX01</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>技术对象描述</td>
</tr>
<tr class="cell">
<td> 5</td>
<td>TPLNR</td>
<td>5</td>
<td>&nbsp;</td>
<td>TPLNR</td>
<td>TPLNR</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>功能位置</td>
</tr>
<tr class="cell">
<td> 6</td>
<td>PLTXT</td>
<td>6</td>
<td>&nbsp;</td>
<td>PLTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>功能位置描述</td>
</tr>
<tr class="cell">
<td> 7</td>
<td>QMNUM</td>
<td>7</td>
<td>&nbsp;</td>
<td>QMNUM</td>
<td>QMNUM</td>
<td>CHAR</td>
<td>12</td>
<td>&nbsp;</td>
<td>通知编号</td>
</tr>
<tr class="cell">
<td> 8</td>
<td>QMTXT</td>
<td>8</td>
<td>&nbsp;</td>
<td>QMTXT</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>短文本</td>
</tr>
<tr class="cell">
<td> 9</td>
<td>GSTRP</td>
<td>9</td>
<td>&nbsp;</td>
<td>CO_GSTRP</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基本开始日期</td>
</tr>
<tr class="cell">
<td>10</td>
<td>GLTRP</td>
<td>10</td>
<td>&nbsp;</td>
<td>CO_GLTRP</td>
<td>DATUM</td>
<td>DATS</td>
<td>8</td>
<td>&nbsp;</td>
<td>基本完成日期</td>
</tr>
<tr class="cell">
<td>11</td>
<td>RSPOS</td>
<td>11</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>NUMC</td>
<td>4</td>
<td>&nbsp;</td>
<td>预留项目编号</td>
</tr>
<tr class="cell">
<td>12</td>
<td>MATNR</td>
<td>12</td>
<td>&nbsp;</td>
<td>MATNR</td>
<td>MATNR</td>
<td>CHAR</td>
<td>40</td>
<td>&nbsp;</td>
<td>物料编号</td>
</tr>
<tr class="cell">
<td>13</td>
<td>MAKTX</td>
<td>13</td>
<td>&nbsp;</td>
<td>MAKTX</td>
<td>TEXT40</td>
<td>CHAR</td>
<td>40</td>
<td>X</td>
<td>物料描述</td>
</tr>
<tr class="cell">
<td>14</td>
<td>MENGE</td>
<td>14</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>计划数量</td>
</tr>
<tr class="cell">
<td>15</td>
<td>DENMNG</td>
<td>15</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>QUAN</td>
<td>13</td>
<td>&nbsp;</td>
<td>实际数量</td>
</tr>
<tr class="cell">
<td>16</td>
<td>MEINS</td>
<td>16</td>
<td>&nbsp;</td>
<td>MEINS</td>
<td>MEINS</td>
<td>UNIT</td>
<td>3</td>
<td>X</td>
<td>基本计量单位</td>
</tr>
<tr class="cell">
<td>17</td>
<td>EBELN</td>
<td>17</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>20</td>
<td>&nbsp;</td>
<td>领料单号</td>
</tr>
<tr class="cell">
<td>18</td>
<td>INGPR</td>
<td>18</td>
<td>&nbsp;</td>
<td>INGRP</td>
<td>INGRP</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>客户服务和工厂维护的计划员组</td>
</tr>
<tr class="cell">
<td>19</td>
<td>INNAM</td>
<td>19</td>
<td>&nbsp;</td>
<td>INNAM</td>
<td>TEXT18</td>
<td>CHAR</td>
<td>18</td>
<td>X</td>
<td>维护计划员组的名称</td>
</tr>
<tr class="cell">
<td>20</td>
<td>SWERK</td>
<td>20</td>
<td>&nbsp;</td>
<td>SWERK</td>
<td>WERKS</td>
<td>CHAR</td>
<td>4</td>
<td>&nbsp;</td>
<td>维护工厂</td>
</tr>
<tr class="cell">
<td>21</td>
<td>NAME2</td>
<td>21</td>
<td>&nbsp;</td>
<td>NAME2</td>
<td>TEXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>名称 2</td>
</tr>
<tr class="cell">
<td>22</td>
<td>BEBER</td>
<td>22</td>
<td>&nbsp;</td>
<td>BEBER</td>
<td>BEBER</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>厂区</td>
</tr>
<tr class="cell">
<td>23</td>
<td>FING</td>
<td>23</td>
<td>&nbsp;</td>
<td>FING</td>
<td>TEXT14</td>
<td>CHAR</td>
<td>14</td>
<td>X</td>
<td>负责公司范围的员工组</td>
</tr>
<tr class="cell">
<td>24</td>
<td>ILART</td>
<td>24</td>
<td>&nbsp;</td>
<td>ILA</td>
<td>ILART</td>
<td>CHAR</td>
<td>3</td>
<td>&nbsp;</td>
<td>维护作业类型</td>
</tr>
<tr class="cell">
<td>25</td>
<td>ILATX</td>
<td>25</td>
<td>&nbsp;</td>
<td>ILATX</td>
<td>TXT30</td>
<td>CHAR</td>
<td>30</td>
<td>X</td>
<td>维护作业类型描述</td>
</tr>
<tr class="cell">
<td>26</td>
<td>TXT04_USR</td>
<td>26</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>CHAR</td>
<td>30</td>
<td>&nbsp;</td>
<td>工单系统状态</td>
</tr>
</table>
</td>
</tr>
<br/>
<tr>
<td class="footer">Extracted by Mass Download version 1.5.5 - E.G.Mellodew. 1998-2021. Sap Release 755</td>
</tr>
</table>