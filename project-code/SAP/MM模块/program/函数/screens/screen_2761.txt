****************************************************************																																
*   This file was generated by Direct Download Enterprise.     *																																
*   Please do not change it manually.                          *																																
****************************************************************																																
%_DYNPRO																																
SAPLZMMG04																																
2761																																
755																																
                40																																
%_HEADER																																
SAPLZMMG04                              2761I0000DATE 14 83192 37  0  0 15 86  0G 1                              20210615205601																																
%_DESCRIPTION																																
Storage data: general data																																
%_FIELDS																																
RMSCH-RAHMENT2	0	CHAR	 83	30	00	00	30	00	  1	  2		  0	  0	  0		 14	R				  0	  0	101							基本信息		
MARA-MEINS	3	CHAR	 20	20	00	00	30	00	  2	  3		  0	  0	  0		  0					  0	  0								基本计量单位	                                                                                                                                                                                                                                                        X	
MARA-MEINS	C	UNIT	  3	E0	02	84	00	08	  2	 24		  0	  0	  0		  0				CUNIT	  6	  0				F01				___	X                                      00	
T006A-MSEHT	C	CHAR	 10	A0	02	80	31	00	  2	 31		  0	  0	  0		  0					 20	  0				T01				__________	                                       00	
MARC-AUSME	3	CHAR	 20	20	00	00	30	00	  2	 43		  0	  0	  0		  0					  0	  0								发货单位	                                                                                                                                                                                                                                                        X	
MARC-AUSME	C	UNIT	  3	E0	02	84	00	08	  2	 64		  0	  0	  0		  0				CUNIT	  6	  0								___	X                                      00	
MARD-LGPBE	3	CHAR	 20	20	00	00	30	00	  3	  3		  0	  0	  0		  0					  0	  0								库存仓位	                                                                                                                                                                                                                                                        X	
MARD-LGPBE	C	CHAR	 10	E0	00	80	00	00	  3	 24		  0	  0	  0		  0					 20	  0								__________	                                       00	
MARD-LWMKB	3	CHAR	 20	30	00	00	30	00	  3	 43		  0	  0	  0		  0					  0	  0								领货范围	                                                                                                                                                                                                                                                        X	
MARD-LWMKB	C	CHAR	  3	A0	00	80	00	08	  3	 64		  0	  0	  0		  0					  6	  0								___	                                       00	
MARA-TEMPB	3	CHAR	 20	20	00	00	30	00	  4	  3		  0	  0	  0		  0					  0	  0								温度条件	                                                                                                                                                                                                                                                        X	
MARA-TEMPB	C	CHAR	  2	E0	00	84	00	08	  4	 24		  0	  0	  0		  0					  4	  0								__	X                                      00	
MARA-RAUBE	3	CHAR	 20	20	00	00	30	00	  4	 43		  0	  0	  0		  0					  0	  0								存储条件	                                                                                                                                                                                                                                                        X	
MARA-RAUBE	C	CHAR	  2	E0	00	84	00	08	  4	 64		  0	  0	  0		  0					  4	  0								__	X                                      00	
MARA-BEHVO	3	CHAR	 20	20	00	00	30	00	  5	  3		  0	  0	  0		  0					  0	  0								集装箱需求	                                                                                                                                                                                                                                                        X	
MARA-BEHVO	C	CHAR	  2	E0	00	84	00	08	  5	 24		  0	  0	  0		  0					  4	  0								__	X                                      00	
MARA-STOFF	3	CHAR	 20	20	00	00	30	00	  5	 43		  0	  0	  0		  0			STO		  0	  0								危险物料号	                                                                                                                                                                                                                                                        X	
MARA-STOFF	C	CHAR	 40	A1	00	84	00	08	  5	 64		  0	  0	  0		 18			STO	MATN3	 80	  0								________________________________________	X                                      00	
MARC-ABCIN	3	CHAR	 20	20	00	00	30	00	  6	  3		  0	  0	  0		  0					  0	  0								周期盘点库存盘点标识	                                                                                                                                                                                                                                                        X	
MARC-ABCIN	C	CHAR	  1	A0	00	84	00	08	  6	 24		  0	  0	  0		  0					  2	  0								_	X                                      00	
MARA-WESCH	3	CHAR	 20	20	00	00	30	00	  6	 43		  0	  0	  0		  0					  0	  0								收货单据数	                                                                                                                                                                                                                                                        X	
MARA-WESCH	P	QUAN	 17	A0	00	80	00	00	  6	 64		  0	  0	  0		  0					 13	  3		MARA-MEINS						_.___.___.___,___	                                       00	
MARC-CCFIX	C	CHAR	  1	A0	00	81	00	08	  7	 24		  0	  0	  0		  0	C				  2	  0	102							_	X                                      00	
MARC-CCFIX	1	CHAR	 10	30	00	01	30	00	  7	 26		  0	  0	  0		  0	C				  0	  0	102							CC被固定		
MARA-XGCHP	C	CHAR	  1	A0	00	81	00	08	  8	 24		  0	  0	  0		  0	C				  2	  0	103							_	X                                      00	
MARA-XGCHP	3	CHAR	 20	30	00	00	30	00	  8	 26		  0	  0	  0		  0	C				  0	  0	103							需要批准的批次记录		
MARA-ETIAR	3	CHAR	 20	20	00	00	30	00	  9	  3		  0	  0	  0		  0					  0	  0								标号类型	                                                                                                                                                                                                                                                        X	
MARA-ETIAR	C	CHAR	  2	A0	00	84	00	08	  9	 24		  0	  0	  0		  0					  4	  0								__	X                                      00	
MARA-ETIFO	0	CHAR	  8	30	00	00	30	00	  9	 54		  0	  0	  0		  0					  0	  0								标签格式	                                                                                                                                                                                                                                                        X	
MARA-ETIFO	C	CHAR	  2	A0	00	84	00	08	  9	 64		  0	  0	  0		  0					  4	  0								__	X                                      00	
MARA-XCHPF	C	CHAR	  1	A0	00	81	00	08	 10	 24		  0	  0	  0		  0	C				  2	  0	104							_	X                                      00	
MARA-XCHPF	0	CHAR	 23	30	00	01	30	00	 10	 26		  0	  0	  0		  0	C				  0	  0	104							批次管理		
MARC-UCHKZ	1	CHAR	 10	30	00	01	30	00	 10	 53		  0	  0	  0		  0					  0	  0								OB管理	                                                                                                                                                                                                                                                        X	
MARC-UCHKZ	C	CHAR	  1	A0	00	81	40	08	 10	 64		  0	  0	  0		  0					  2	  0								_	X                                      00	
MARC-XCHPF	C	CHAR	  1	A0	00	81	00	08	 11	 24		  0	  0	  0		  0	C				  2	  0	105							_	X                                      00	
MARC-XCHPF	0	CHAR	 23	30	00	01	30	00	 11	 26		  0	  0	  0		  0	C				  0	  0	105							批次管理（工厂）		
MARC-UCMAT	0	CHAR	 20	30	00	04	30	00	 12	  3		  0	  0	  0		  0					  0	  0								OB 参考物料	                                                                                                                                                                                                                                                        X	
MARC-UCMAT	C	CHAR	 40	A1	00	84	40	08	 12	 24		  0	  0	  0		  0				MATN1	 80	  0								________________________________________	X                                      00	
MARC-ROTATION_DATE	2	CHAR	 15	30	00	01	30	00	 13	  3		  0	  0	  0		  0					  0	  0								库存入库/出库	                                                                                                                                                                                                                                                        X	
MARC-ROTATION_DATE	C	CHAR	  1	A0	00	81	40	08	 13	 24		  0	  0	  0		  0					  2	  0								_	X                                      00	
	0	CHAR	 20	80	10	00	00	00	255	  1	O	  0	  0	  0		  0					  0	  0								____________________		
%_FLOWLOGIC																																
PROCESS BEFORE OUTPUT.																																
*                      Verarbeitung vor der Ausgabe																																
																																
  MODULE INIT_SUB.																																
  MODULE GET_DATEN_SUB.																																
  MODULE FELDAUSWAHL.																																
  MODULE SONDERFAUS.																																
  MODULE SONFAUSW_IN_FGRUPPEN.																																
  MODULE FAUSW_BEZEICHNUNGEN.       "ch zu 3.0F																																
  MODULE FELDHISTORIE.        " Aenderungsdienst																																
  MODULE BILDSTATUS.          " Nach Feldauswahl vor Vorlagehand.																																
  MODULE ZUSREF_VORSCHLAGEN_B.																																
  MODULE REFDATEN_VORSCHLAGEN.																																
  MODULE ZUSREF_VORSCHLAGEN_A.																																
  MODULE BEZEICHNUNGEN_LESEN.																																
  MODULE SET_DATEN_SUB.																																
																																
																																
PROCESS AFTER INPUT.																																
*                      Verarbeitung nach der Eingabe																																
  MODULE GET_DATEN_SUB.																																
																																
  FIELD: MARD-LGPBE,																																
         MARA-TEMPB,																																
         MARA-RAUBE,																																
         MARA-BEHVO,																																
         MARA-WESCH,																																
         MARC-ABCIN,																																
         MARC-CCFIX.																																
																																
  FIELD: MARC-ROTATION_DATE.          "TF 4.7																																
																																
  CHAIN.																																
     FIELD MARA-MEINS.																																
           MODULE MARA-MEINS.																																
  ENDCHAIN.																																
  CHAIN.																																
     FIELD MARA-MEINS.																																
     FIELD MARC-AUSME.																																
           MODULE MARC-AUSME.																																
  ENDCHAIN.																																
  CHAIN.																																
     FIELD MARA-STOFF.																																
           MODULE MARA-STOFF.																																
  ENDCHAIN.																																
  CHAIN.																																
     FIELD MARA-ETIFO.																																
     FIELD MARA-ETIAR.																																
           MODULE MARA-ETIFO.																																
  ENDCHAIN.																																
  CHAIN.																																
																																
       FIELD MARA-XCHPF																																
           MODULE MARA_XCHPF ON REQUEST.																																
       FIELD MARC-XCHPF.																																
           MODULE MARC-XCHPF.																																
           MODULE MARC-XCHAR.																																
  ENDCHAIN.																																
  CHAIN.                                               "4.0A  BE/190897																																
     FIELD MARA-XCHPF.                                 "4.0A  BE/190897																																
     FIELD MARA-XGCHP.                                 "4.0A  BE/190897																																
           MODULE MARA-XGCHP.                          "4.0A  BE/190897																																
  ENDCHAIN.                                            "4.0A  BE/190897																																
  chain.                                               "TF 4.7																																
    field marc-uchkz.                                  "TF 4.7																																
    field marc-ucmat.                                  "TF 4.7																																
          module marc-uchkz.                           "TF 4.7																																
  endchain.                                            "TF 4.7																																
  CHAIN.                                               "4.0A  BE/140897																																
     FIELD MARD-LWMKB.                                 "4.0A  BE/140897																																
           MODULE MARD-LWMKB.                          "4.0A  BE/140897																																
  ENDCHAIN.                                            "4.0A  BE/140897																																
*																																
  MODULE SET_DATEN_SUB.																																
*																																
PROCESS ON VALUE-REQUEST.																																
* Selbstprogrammierte Eingabehilfen																																
     FIELD MARA-MEINS  MODULE MEKFM_HELP.																																
     FIELD MARC-AUSME  MODULE MEKFM_HELP.																																
     FIELD MARD-LWMKB  MODULE MARD_LWMKB_HELP.         "4.0A  BE/061197																																
     FIELD MARC-ABCIN  MODULE MARC_ABCIN_HELP.         "4.0C/ch																																
