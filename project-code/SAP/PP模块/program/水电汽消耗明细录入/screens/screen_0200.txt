****************************************************************																																
*   This file was generated by Direct Download Enterprise.     *																																
*   Please do not change it manually.                          *																																
****************************************************************																																
%_DYNPRO																																
ZPP010A																																
0200																																
755																																
                40																																
%_HEADER																																
ZPP010A                                 0200 0200     23165192 37  0  0 27165  0G 1                              20210702114155																																
%_DESCRIPTION																																
电																																
%_FIELDS																																
LBL01		CHAR	165	00	00	08	30	00	  1	  2		  0	  0	  0		  3	R				  0	  0	102							水电消耗明细表录入		
LBL02		CHAR	  4	00	00	08	30	00	  2	  4		  0	  0	  0		  0					  0	  0								工厂	                                                                                                                                                                                                                                                        X	
GW_ZPPT015-WERKS		CHAR	  4	80	00	88	30	00	  2	  9		  0	  0	  0		  0					  0	  0								____		
TXT_NAME		CHAR	 31	80	00	88	31	00	  2	 14		  0	  0	  0		  0					  0	  0								_______________________________		
LBL03		CHAR	  8	00	00	08	30	00	  2	 58		  0	  0	  0		  0					  0	  0								生产日期		
GW_ZPPT015-ZPDATE		DATS	 10	80	00	88	30	00	  2	 67		  0	  0	  0		  0					  0	  0								__________		
LBL04		CHAR	  4	00	00	08	30	00	  2	 93		  0	  0	  0		  0					  0	  0								班次		
GW_ZPPT015-ZBANCI		CHAR	  8	80	00	88	30	00	  2	 98		  0	  0	  0		  0					  0	  0								________		
LBL05		CHAR	  4	00	00	08	30	00	  2	121		  0	  0	  0		  0					  0	  0								值别		
GW_ZPPT015-ZZHIBIE		CHAR	  6	80	00	A8	00	00	  2	126		  0	  0	  0		  9					  0	  0								?_____	 DL K	
LBL06		CHAR	165	00	00	08	30	00	  4	  2		  0	  0	  0		 20	R				  0	  0	103							电消耗明细录入		
TC_DXHMXB			157	F8	F8	00	00	00	  5	  4	E	101	  1	  1		 18	T				  5	 15										
GS_ZPPT015-ZYDDW		CHAR	  8	00	00	08	30	80	  1	  1	T	101	  1	  1		 33					  0	  0								用电单位		
GS_ZPPT015-ZYDL		CHAR	 13	00	00	08	30	80	  1	  2	T	101	  1	  1		 45					  0	  0								用电量（KWH）		
GS_ZPPT015-ZBEIZHU		CHAR	  4	00	00	08	30	80	  1	  3	T	101	  1	  1		 69					  0	  0								备注		
GS_ZPPT015-SELT		CHAR	  1	80	00	88	00	20	  1	  0	T	101	  1	  1		  1	C				  0	  0	104									
GS_ZPPT015-ZYDDW		CHAR	 10	80	00	88	30	00	  1	  1	T	101	  1	  1		 33					  0	  0								__________		
GS_ZPPT015-ZYDL		DEC	 17	80	00	88	00	00	  1	  2	T	101	  1	  1		 45					  0	  0								_________________		
GS_ZPPT015-ZBEIZHU		CHAR	 50	80	00	88	00	00	  1	  3	T	101	  1	  1		 69					  0	  0								__________________________________________________		
OK_CODE		CHAR	 20	80	10	08	00	00	255	  1	O	  0	  0	  0		  0					  0	  0								____________________		
%_FLOWLOGIC																																
PROCESS BEFORE OUTPUT.																																
  MODULE FRM_INIT_DDATA.																																
*&SPWIZARD: PBO FLOW LOGIC FOR TABLECONTROL 'TC_DXHMXB'																																
  MODULE TC_DXHMXB_CHANGE_TC_ATTR.																																
*&SPWIZARD: MODULE TC_DXHMXB_CHANGE_COL_ATTR.																																
  LOOP AT   GT_ZPPT015																																
       INTO GS_ZPPT015																																
       WITH CONTROL TC_DXHMXB																																
       CURSOR TC_DXHMXB-CURRENT_LINE.																																
*&SPWIZARD:   MODULE TC_DXHMXB_CHANGE_FIELD_ATTR																																
  ENDLOOP.																																
																																
  MODULE STATUS_0100.																																
  MODULE MODIFY_SCREEN .																																
*																																
PROCESS AFTER INPUT.																																
*&SPWIZARD: PAI FLOW LOGIC FOR TABLECONTROL 'TC_DXHMXB'																																
  LOOP AT GT_ZPPT015.																																
    CHAIN.																																
      FIELD GS_ZPPT015-ZYDDW.																																
      FIELD GS_ZPPT015-ZYDL.																																
      FIELD GS_ZPPT015-ZBEIZHU.																																
      MODULE TC_DXHMXB_MODIFY ON CHAIN-REQUEST.																																
    ENDCHAIN.																																
    FIELD GS_ZPPT015-SELT																																
      MODULE TC_DXHMXB_MARK ON REQUEST.																																
  ENDLOOP.																																
*&SPWIZARD: MODULE TC_DXHMXB_CHANGE_TC_ATTR.																																
*&SPWIZARD: MODULE TC_DXHMXB_CHANGE_COL_ATTR.																																
																																
  MODULE EXIT_PROGGAM AT EXIT-COMMAND.																																
  MODULE USER_COMMAND_0100.																																
